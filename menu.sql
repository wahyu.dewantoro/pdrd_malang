/*
SQLyog Community v10.51 
MySQL - 5.5.58-cll : Database - k4094426_ptsms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`k4094426_ptsms` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `k4094426_ptsms`;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `kode_menu` int(3) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(100) DEFAULT NULL,
  `link_menu` varchar(100) DEFAULT NULL,
  `parent_menu` int(3) DEFAULT NULL,
  `sort_menu` int(3) DEFAULT NULL,
  `icon_menu` varchar(100) DEFAULT NULL,
  `active` char(1) DEFAULT NULL,
  PRIMARY KEY (`kode_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`kode_menu`,`nama_menu`,`link_menu`,`parent_menu`,`sort_menu`,`icon_menu`,`active`) values (1,'Setting','#',0,100,'fa fa-cogs','1'),(2,'Menu','Setting/Msmenu',1,1,'','1'),(3,'Grup','Setting/Group',1,2,NULL,'1'),(4,'privilege','Setting/Sistem/privilege',1,3,NULL,'1'),(5,'Pengguna','Setting/pengguna',1,4,'fa fa-users','1'),(6,'Referensi','#',0,3,'fa fa-book','1'),(7,'Referensi Supplier','Pembelian/ref_supplier',6,1,'fa fa-barcode','1'),(8,'Referensi Lokasi Kayu','Pembelian/ref_lokasi_kayu',6,2,'fa fa-ban','1'),(9,'Referensi Panjang Kayu','Pembelian/ref_panjang_kayu',6,3,'fa fa-ban','1'),(10,'Transaksi','#',0,1,'fa fa-tasks','1'),(11,'Manajemen Harga Kayu','Pembelian/man_harga_kayu',10,1,'fa fa-ban','1'),(12,'Transaksi DUKB','Pembelian/man_dukb',10,2,'fa fa-ban','1'),(13,'Transaksi BAP ','Pembelian/man_bap',10,3,'fa fa-bank','1'),(14,'Referensi Grader','Pembelian/ref_grader',6,4,'fa fa-ban','1'),(15,'Referensi Harga Kayu','Pembelian/man_harga_kayu',6,5,'fa fa-ban','1'),(17,'Validasi DUKB','Pembelian/man_dukb/validasi',10,4,'fa fa-ban','1'),(18,'Laporan','#',0,2,'fa fa-book','1'),(19,'Laporan Pembayaran','Pembelian/lap_pembayaran',18,1,'fa fa-book','1'),(20,'Rekap Pembayaran Harian','Pembelian/rp_harian',18,2,'fa fa-book','1'),(21,'Rekap Pembayaran','Pembelian/rp',18,2,'fa fa-book','1'),(22,'Rekap Pembayaran Supplier','Pembelian/rp_sup',18,3,'fa fa-book','1'),(23,'Referensi Supplier','Gudang/ref_supplier',6,7,'fa fa-book','1'),(24,'DUKB Tervalidasi','Pembelian/man_dukb/dukb_tervalidasi',10,5,'fa fa-book','1'),(25,'Laporan Depo Supplier','Pembelian/lap_depo_sup',18,4,'fa fa-book','1'),(26,'Laporan Berdasarkan Wilayah','Pembelian/lap_wil',18,5,'fa fa-book','1'),(27,'Referensi Barang','Gudang/ref_barang',6,8,'fa fa-bank','1'),(28,'Referensi Kategori','Gudang/ref_kategori',6,9,'fa fa-book','1'),(29,'Referensi Harga Barang Stock','Gudang/Ref_barang/Harga_stock',6,10,'fa fa-book','1'),(30,'Transaksi Barang Masuk','Gudang/Tr_barang_masuk',10,6,'fa fa-book','1'),(31,'Transaksi Barang Keluar','Gudang/tr_barang_keluar',10,7,'fa fa-book','1'),(32,'Referensi Unit','gudang/ref_unit',6,7,'fa fa-book','1'),(33,'Laporan  Pengeluaran Barang per Divisi','Gudang/laporan/lap_per_divisi',18,7,'fa fa-book','1'),(34,'Laporan Pengeluaran Barang per Kategori ','Gudang/laporan/lap_stock_jb',18,8,'fa fa-book','1'),(35,'Laporan Pengeluaran Barang per Bulan','Gudang/laporan/lap_per_bulan',18,9,'fa fa-book','1'),(36,'Mutasi Barang','Gudang/tr_mutasi_barang',10,11,'fa fa-book','1'),(37,'Laporan Persediaan Stock Barang','Gudang/ref_barang/stock_barang_gudang',18,11,'fa fa-book','1'),(38,'Laporan Gudang','#',0,3,'fa fa-book','1'),(39,'Laporan Pengeluaran Barang Per Divisi','Gudang/laporan/lap_per_divisi',38,1,'fa fa-book','1'),(40,'Laporan Pengeluaran Barang Per Kategori','Gudang/laporan/lap_stock_jb',38,2,'fa fa-book','1'),(41,'Laporan Pengeluaran Barang Per Bulan','Gudang/laporan/lap_per_bulan',38,3,'fa fa-book','1'),(42,'Stock Barang Gudang','Gudang/ref_barang/stock_barang_gudang',38,4,'fa fa-book','1'),(43,'Laporan Kayu','#',0,2,'fa fa-book','1'),(44,'Laporan Pembayaran','Pembelian/lap_pembayaran',43,1,'fa fa-book','1'),(45,'Rekap Pembayaran Harian','Pembelian/rp_harian',43,2,'fa fa-book','1'),(46,'Rekap Pembayaran','Pembelian/rp',43,3,'fa fa-book','1'),(47,'Laporan Depo Supplier','Pembelian/lap_depo_sup',43,4,'fa fa-bank','1'),(48,'Laporan Berdasarkan Wilayah','Pembelian/lap_wil',43,5,'fa fa-bank','1'),(49,'Laporan Riwayat Barang','Gudang/laporan/lap_rb',18,5,'fa fa-book','1'),(50,'Laporan Barang Tidak Terpakai','Gudang/laporan/lap_btt',18,6,'fa fa-book','1'),(51,'Laporan Penerimaan','Pembelian/lap_penerimaan',18,6,'fa fa-book','1'),(52,'Rekap Barang Masuk','Gudang/rekap_bm',18,12,'fa fa-book','1'),(53,'Referensi Divisi','Gudang/ref_divisi',6,11,'fa fa-book','1'),(54,'Referensi Alokasi Pemakaian','Gudang/ref_alok_p',6,9,'fa fa-book','1'),(55,'Referensi Alokasi Biaya','Gudang/ref_alok_b',6,10,'fa fa-bank','1'),(56,'Rekap Laporan Persediaan Stock Barang ','Gudang/rekap_lpsb',18,13,'fa fa-bank','1'),(57,'Rekap Pemakaian Barang','Gudang/rekap_pb',18,15,'fa fa-book','1'),(58,'Laporan Rekapitulasi  Biaya Pemakaian Barang','Gudang/lap_rek_bpb',18,16,'fa fa-book','1'),(59,'Referensi Sub Divisi','Gudang/Ref_sub_div',6,12,'fa fa-bar-chart','1'),(60,'Laporan Riwayat Barang','Gudang/laporan/lap_rb',38,1,'fa fa-book','1'),(61,'Laporan Barang Tidak Terpakai','Gudang/laporan/lap_btt',38,1,'fa fa-book','1'),(62,'Rekap Barang Masuk','gudang/rekap_bm',38,7,'fa fa-book','1'),(63,'Rekap Laporan Persediaan Stock Barang','Gudang/rekap_lpsb',38,8,'fa fa-book','1'),(64,'Rekap Pemakaian Barang','Gudang/rekap_pb',38,9,'','1'),(65,'Laporan Rekapitulasi Biaya Pemakaian Barang','Gudang/lap_rek_bpb',38,10,'fa fa-book','1'),(66,'a','a',10,1,'fa fa-book','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
