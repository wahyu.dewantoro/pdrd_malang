

            <h1>
              Form Aktivasi Formulir Pengajuan
            </h1>
          </section>
        <section class="content">
            <div class="x_panel">
                <div class="box-body">
        <form class="form-horizontal" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" name="randform"> 
        <div class="form-group">
            <label class="control-label col-md-2" for="varchar">NPWPD <small>*</small></label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="NPWPD" id="NPWPD"  value="<?php echo $NPWPD; ?>" readonly=""/>
            <?php echo form_error('NPWPD') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama WP <small>*</small></label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="NAMA_WP" id="NAMA_WP"  value="<?php echo $NAMA_WP; ?>" readonly=""/>
            <?php echo form_error('NAMA_WP') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Tempat Usaha <small>*</small></label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="NAMA_USAHA" id="NAMA_USAHA"  value="<?php echo $NAMA_USAHA; ?>" readonly=""/>

            <input type="hidden" class="form-control" name="ID_USAHA" id="ID_USAHA"  value="<?php echo $ID_USAHA; ?>" readonly=""/>

            <?php echo form_error('ID_USAHA') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="timestamp">Email Tempat Usaha <small>*</small></label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="EMAIL_TEMPAT_USAHA" id="EMAIL_TEMPAT_USAHA"  value="<?php echo $EMAIL_TEMPAT_USAHA?>" />
            <?php echo form_error('EMAIL_TEMPAT_USAHA') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="timestamp">Kode Aktivasi <small>*</small></label>
            <div class="col-md-2">
            <input type="number" class="form-control" name="KODE_AKTIVASI" id="KODE_AKTIVASI"  value="" readonly="" />
            </div>
            <div class="col-md-3">
                <button type="button" class="btn btn-sm btn-warning" onClick="randomString();"><i class="fa fa-refresh"></i> Generate</button> 
            </div>
        </div>
       <div class="form-group">
        <div class="col-md-4 col-md-offset-2">
       <div class="btn-group">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> <?php echo $button ?></button> 
        <a href="<?php echo site_url('master/pokok/formulir_pengajuan') ?>" class="btn  btn-sm btn-default"><i class="fa fa-close"></i> Batal</a>
          </div >
         </div >
       </div >
    </form>
                </div>
            </div>
  <script type="text/javascript">
      function randomString() {
    // alert('tes');
  var chars = "0123456789";
  var string_length = 4;
  var randomstring = '';
  for (var i=0; i<string_length; i++) {
    var rnum = Math.floor(Math.random() * chars.length);
    randomstring += chars.substring(rnum,rnum+1);
  }
  
  //alert(randomstring);
  
  document.randform.KODE_AKTIVASI.value = randomstring;
}
  </script>