

            <h1>
              Form Formulir Pengajuan
            </h1>
          </section>
        <section class="content">
            <div class="x_panel">
                <div class="box-body">
        <form class="form-horizontal" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"> 
        <div class="form-group">
            <label class="control-label col-md-2" for="varchar">NPWPD <small>*</small></label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="NPWPD" id="NPWPD"  value="<?php echo $NPWPD; ?>" onchange="NamaUsaha(this.value);"/>
            <?php echo form_error('NPWPD') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama WP <small>*</small></label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="NAMA_WP" id="NAMA_WP"  value="<?php echo $NAMA_WP; ?>" />
            <?php echo form_error('NAMA_WP') ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="varchar">Nama Tempat Usaha <small>*</small></label>
            <div class="col-md-4">
            <select  style="font-size:12px" id="NAMA_USAHA" required name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" <?php echo $disable ?>>
                          <?php foreach($jenis as $jns){ ?>
                          <option <?php if($jns->NAMA_USAHA==$NAMA_USAHA){echo "selected";}?> value="<?php echo $jns->NAMA_USAHA?>"><?php echo $jns->NAMA_USAHA ?></option>
                          <?php } ?> 
                        </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2" for="timestamp">Email Tempat Usaha <small>*</small></label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="EMAIL_TEMPAT_USAHA" id="EMAIL_TEMPAT_USAHA"  value="<?php echo $EMAIL_TEMPAT_USAHA; ?>" />
            <?php echo form_error('EMAIL_TEMPAT_USAHA') ?>
            </div>
        </div>
       <div class="form-group">
        <div class="col-md-4 col-md-offset-2">
       <div class="btn-group">
        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> <?php echo $button ?></button> 
        <a href="<?php echo site_url('master/pokok/formulir_pengajuan') ?>" class="btn  btn-sm btn-default"><i class="fa fa-close"></i> Batal</a>
          </div >
         </div >
       </div >
    </form>
                </div>
            </div>
  <script type="text/javascript">
      function NamaUsaha(id){
                //alert(id);
                var npwpd=id;
                $.ajax({
        url: "<?php echo base_url().'master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
              }
              
            }
          });
        
        $.ajax({
           type: "POST",
           url: "<?php echo base_url().'master/pokok/get_nama_usaha'?>",
           data: { npwp: npwpd  },
           cache: false,
           success: function(msga){
                  //alert(jp);
                  $("#NAMA_USAHA").html(msga);
                }
              }); 
                
    }     
  </script>