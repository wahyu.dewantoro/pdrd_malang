<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 
$autoload['packages'] = array();
 
$autoload['libraries'] = array('database', 'session', 'xmlrpc','pagination','Template','Menu');
 
$autoload['drivers'] = array('session');
 
$autoload['helper'] = array('url', 'file','html','form','tgl_indo','acakan','akses');
 
$autoload['config'] = array();

 
$autoload['language'] = array();
 
$autoload['model'] = array();
