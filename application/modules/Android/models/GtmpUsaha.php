<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GtmpUsaha extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gTmpUsaha($npwp,$value){
		// if ($value == 1) {
		// 	$tmpUsaha = "AND tempat_usaha = 1";
		// } else if ($value == 2) {
		// 	$tmpUsaha = "AND tempat_usaha = 2";
		// } else {
			$tmpUsaha = "";
		// }
		$sql = "SELECT * FROM TEMPAT_USAHA WHERE NPWPD='$npwp' AND ID_OP IS NOT NULL $tmpUsaha ORDER BY NAMA_USAHA DESC";
		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listTmpUsaha"] = array();
			//create an array
			foreach ($result as $row) {
				$emparray["id"] = $row["ID_INC"];
				$emparray["nmUsaha"] = $row["NAMA_USAHA"];
				$emparray["almtUsaha"] = $row["ALAMAT_USAHA"];
				$emparray["npwpd"] = $row["NPWPD"];
				$emparray["kodeKec"] = $row["KODEKEC"];
				$emparray["kodeKel"] = $row["KODEKEL"];
				$emparray["jnsPajak"] = $row["JENIS_PAJAK"];
				$emparray["idOP"] = $row["ID_OP"];
				$emparray["rekening"] = $row["REKENING"];
				$emparray["jmlKamarMeja"] = $row["JUMLAH_KAMAR_MEJA"];
				array_push($response["listTmpUsaha"], $emparray);
			}
			echo json_encode($response);
			
			//close the db connection
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}