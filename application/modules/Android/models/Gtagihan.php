<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gtagihan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gTagihan($npwp,$idOp,$tahun,$tmpUsaha){
		// $sql = "SELECT SP.ID_INC,T.KODE_BILING,T.MASA_PAJAK||'/'||T.TAHUN_PAJAK AS MAS_TAH
		// ,DATE_BAYAR,T.TAGIHAN+T.DENDA AS TOTAL_TAGIHAN FROM TAGIHAN T 
		// LEFT JOIN SPTPD_PEMBAYARAN SP ON SP.KODE_BILING = T.KODE_BILING
		// JOIN OBJEK_PAJAK OP ON T.ID_OP = OP.ID_OP 
		// WHERE NPWPD = '$npwp' AND T.ID_OP = '$idOp' AND T.TAHUN_PAJAK = '$tahun' 
		// AND TEMPAT_USAHA_ID = '$tmpUsaha' ORDER BY DATE_INSERT DESC";
		$sql = "SELECT SP.ID_INC,T.KODE_BILING,T.MASA_PAJAK||'/'||T.TAHUN_PAJAK AS MAS_TAH
		,DATE_BAYAR,T.TAGIHAN+T.DENDA AS TOTAL_TAGIHAN,VIRTUAL_ACCOUNT FROM TAGIHAN T 
		LEFT JOIN SPTPD_PEMBAYARAN SP ON SP.KODE_BILING = T.KODE_BILING
		JOIN OBJEK_PAJAK OP ON T.ID_OP = OP.ID_OP 
		WHERE NPWPD = '$npwp' AND T.TAHUN_PAJAK = '$tahun' 
		AND TEMPAT_USAHA_ID = '$tmpUsaha' ORDER BY DATE_INSERT DESC";

		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listTagihan"] = array();
			//create an array
			foreach ($result as $row) {
				$emparray["id"] = $row["ID_INC"];
				$emparray["kdBiling"] = $row["KODE_BILING"];
				$emparray["masTah"] = $row["MAS_TAH"];
				$emparray["dateBayar"] = $row["DATE_BAYAR"];
				$emparray["ttlTagihan"] = $row["TOTAL_TAGIHAN"];
				$emparray["noVa"] = $row["VIRTUAL_ACCOUNT"];
				array_push($response["listTagihan"], $emparray);
			}
			echo json_encode($response);
			
			//close the db connection
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}