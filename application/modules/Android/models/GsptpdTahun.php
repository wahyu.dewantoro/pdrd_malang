<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GsptpdTahun extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gSptpdTahun($npwp,$golongan){
		if ($golongan == 1) {
			$sql = "SELECT TAHUN_PAJAK FROM SPTPD_HOTEL WHERE NPWPD = '$npwp' GROUP BY TAHUN_PAJAK ORDER BY TAHUN_PAJAK DESC";
		}else if ($golongan == 2) {
			$sql = "SELECT TAHUN_PAJAK FROM SPTPD_RESTORAN WHERE NPWPD = '$npwp' GROUP BY TAHUN_PAJAK ORDER BY TAHUN_PAJAK DESC";
		}else if ($golongan == 3) {
			$sql = "SELECT TAHUN_PAJAK FROM SPTPD_HIBURAN WHERE NPWPD = '$npwp' GROUP BY TAHUN_PAJAK ORDER BY TAHUN_PAJAK DESC";
		}else if ($golongan == 7) {
			$sql = "SELECT TAHUN_PAJAK FROM SPTPD_PARKIR WHERE NPWPD = '$npwp' GROUP BY TAHUN_PAJAK ORDER BY TAHUN_PAJAK DESC";
		}else {
			$response["success"] = 0;
			$response["message"] = "Layanan Belum Tersedia";

			echo json_encode($response);
		}

		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listTahun"] = array();
			$arrayTahun = array();
			//create an array
			foreach ($result as $row) {
				$tahunPajak = $row["TAHUN_PAJAK"];
				if (!in_array($tahunPajak, $arrayTahun)) {
					$emparray["tahun"] = $tahunPajak;
					array_push($arrayTahun, $tahunPajak);
					array_push($response["listTahun"], $emparray);
				}
			}
			echo json_encode($response);
			
			//close the db connection
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}