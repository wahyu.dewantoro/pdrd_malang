<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ggolongan extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gGolongan($npwp){
		$sql = "SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI FROM OBJEK_PAJAK 
			WHERE ID_GOL=142 OR ID_GOL=143 OR ID_GOL=145
			OR ID_GOL=147 OR ID_GOL=148 OR ID_GOL=149
			OR ID_GOL=151 OR ID_GOL=172 OR ID_GOL=174 OR ID_GOL=175 
			OR ID_GOL=176 OR ID_GOL=184 OR ID_GOL=185 OR ID_GOL=186
			OR ID_GOL=187 OR ID_GOL=193 OR ID_GOL=240 OR ID_GOL=493 OR ID_GOL=494 
			OR ID_GOL=495 OR ID_GOL=496 OR ID_GOL=497 OR ID_GOL=498
			OR ID_GOL=499 OR ID_GOL=500 OR ID_GOL=501 OR ID_GOL=502 OR ID_GOL=503
			OR ID_GOL=504 OR ID_GOL=505 OR ID_GOL=506";
		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listGolongan"] = array();
			//create an array
			foreach ($result as $row) {
				$emparray["id"] = $row["ID_OP"];
				$emparray["idGol"] = $row["ID_GOL"];
				$emparray["masa"] = $row["MASA"];
				$emparray["persen"] = $row["PERSEN"];
				$emparray["deskripsi"] = $row["DESKRIPSI"];
				array_push($response["listGolongan"], $emparray);
			}
			echo json_encode($response);
			
			//close the db connection
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}