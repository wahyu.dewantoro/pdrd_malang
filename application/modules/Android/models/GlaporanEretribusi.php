<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GlaporanEretribusi extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gLaporanEretribusi($value,$bulan,$tahun){
		if ($value == 0) {
			$sql = "SELECT ID_TR,WAKTU,NOMINAL_TRANSAKSI,TIPE_KENDARAAN FROM TRANSAKSI_RETRIBUSI WHERE TIPE_KENDARAAN = '$tahun'";
		}
		
		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listLaporanEretribusi"] = array();
			//create an array
			foreach ($result as $row) {
				$emparray["id"] = $row["ID_TR"];
				$emparray["waktu"] = $row["WAKTU"];
				$emparray["nominal"] = $row["NOMINAL_TRANSAKSI"];
				$emparray["tipe"] = $row["TIPE_KENDARAAN"];
				array_push($response["listLaporanEretribusi"], $emparray);
			}
			echo json_encode($response);
			
			//close the db connection
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}