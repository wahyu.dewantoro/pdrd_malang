<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gsptpd extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gSptpd($npwp,$golongan,$tahun,$tmpUsaha){
		if ($golongan == 1) {
			$sql = "SELECT ID_INC,MASA_PAJAK||'/'||TAHUN_PAJAK AS MAS_TAH,PAJAK_TERUTANG,DPP,PERSEN AS PAJAK,TGL_INSERT,
				FILE_TRANSAKSI,KODE_BILING,NO_VA FROM SPTPD_HOTEL SH JOIN OBJEK_PAJAK OP ON SH.ID_OP = OP.ID_OP 
				WHERE NPWPD = '$npwp' AND TAHUN_PAJAK = '$tahun' AND ID_USAHA = '$tmpUsaha' ORDER BY TGL_INSERT DESC";
		}else if ($golongan == 2) {
			$sql = "SELECT ID_INC,MASA_PAJAK||'/'||TAHUN_PAJAK AS MAS_TAH,PAJAK_TERUTANG,DPP,PERSEN AS PAJAK,TGL_INSERT,
			FILE_TRANSAKSI,KODE_BILING,NO_VA FROM SPTPD_RESTORAN SR JOIN OBJEK_PAJAK OP ON SR.ID_OP = OP.ID_OP 
			WHERE NPWPD = '$npwp' AND TAHUN_PAJAK = '$tahun' AND ID_USAHA = '$tmpUsaha' ORDER BY TGL_INSERT DESC";
		}else if ($golongan == 3) {
			$sql = "SELECT ID_INC,MASA_PAJAK||'/'||TAHUN_PAJAK AS MAS_TAH,PAJAK_TERUTANG,DPP,PERSEN AS PAJAK,TGL_INSERT,
			FILE_TRANSAKSI,KODE_BILING,NO_VA FROM SPTPD_HIBURAN SH JOIN OBJEK_PAJAK OP ON SH.ID_OP = OP.ID_OP 
			WHERE NPWPD = '$npwp' AND TAHUN_PAJAK = '$tahun' AND ID_TEMPAT_USAHA = '$tmpUsaha' ORDER BY TGL_INSERT DESC";
		}else if ($golongan == 7) {
			$sql = "SELECT ID_INC,MASA_PAJAK||'/'||TAHUN_PAJAK AS MAS_TAH,PAJAK_TERUTANG,DPP,PERSEN AS PAJAK,TGL_INSERT,
			FILE_TRANSAKSI,KODE_BILING,NO_VA FROM SPTPD_PARKIR SP JOIN OBJEK_PAJAK OP ON SP.ID_OP = OP.ID_OP 
			WHERE NPWPD = '$npwp' AND TAHUN_PAJAK = '$tahun' AND ID_TEMPAT_USAHA = '$tmpUsaha' ORDER BY TGL_INSERT DESC";
		}

		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listSptpd"] = array();
			$response["listTahun"] = array();
			foreach ($result as $row) {
				$emparray["id"] = $row["ID_INC"];
				$emparray["masTah"] = $row["MAS_TAH"];
				$emparray["dpp"] = $row["DPP"];
				$emparray["pjkTerutang"] = $row["PAJAK_TERUTANG"];
				$emparray["pajak"] = $row["PAJAK"];
				$emparray["tglInsert"] = $row["TGL_INSERT"];
				$emparray["idTmpUsaha"] = 0;
				$emparray["fileTransaksi"] = $row["FILE_TRANSAKSI"];
				$emparray["kodeBiling"] = $row["KODE_BILING"];
				$emparray["noVa"] = $row["NO_VA"];
				array_push($response["listSptpd"], $emparray);
			}
			echo json_encode($response);
			
			//close the db connection
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}