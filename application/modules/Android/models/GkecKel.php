<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GkecKel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gKecKel(){
		$tmpUsaha = "";
		$sql = "SELECT KODEKEC,NAMAKEC FROM KECAMATAN ORDER BY NAMAKEC DESC";
		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listKec"] = array();
			//create an array
			foreach ($result as $row) {
				$emparray["kdKec"] = $row["KODEKEC"];
				$emparray["nmKec"] = $row["NAMAKEC"];
				array_push($response["listKec"], $emparray);
			}
			
			$sql = "SELECT KODEKELURAHAN,NAMAKELURAHAN,KODEKEC FROM KELURAHAN ORDER BY NAMAKELURAHAN DESC";
			$result = $this->db->query($sql)->result_array();
			if (count($result) > 0) {
				$response["success"] = 1;
				$response["message"] = "Berhasil.";
				$response["listKel"] = array();
				//create an array
				foreach ($result as $row) {
					$emparray["kdKel"] = $row["KODEKELURAHAN"];
					$emparray["nmKel"] = $row["NAMAKELURAHAN"];
					$emparray["kdKec"] = $row["KODEKEC"];
					array_push($response["listKel"], $emparray);
				}
				echo json_encode($response);
				
				//close the db connection
				
			} else {
				$response["success"] = 0;
				$response["message"] = "Tidak ada data yang ditemukan";

				echo json_encode($response);
			}
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}