<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class AndroidJson extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('master/Mpokok');
    }

    public function pLogin() {
        $usrnm = $_POST['usrnm'];
        $pwd = $_POST['pwd'];
        $kdAktivasi = $_POST['kdAktivasi'];
        
        $this->load->model('Plogin');
        $this->Plogin->pLogin($usrnm,$pwd,$kdAktivasi);
    }

    public function gTmpUsaha() {
        $npwp = $_GET['npwp'];
        $value = $_GET['value'];
        
        $this->load->model('GtmpUsaha');
        $this->GtmpUsaha->gTmpUsaha($npwp,$value);
    }

    public function gGolongan() {
        $value = $_GET['value'];
        
        $this->load->model('Ggolongan');
        $this->Ggolongan->gGolongan($value);
    }

    public function gSptpdTahun() {
        $npwp = $_GET['npwp'];
        $golongan = $_GET['golongan'];
        
        $this->load->model('GsptpdTahun');
        $this->GsptpdTahun->gSptpdTahun($npwp,$golongan);
    }

    public function gSptpd() {
        $npwp = $_GET['npwp'];
        $golongan = $_GET['golongan'];
        $tahun = $_GET['tahun'];
        $tmpUsaha = $_GET['tmpUsaha'];
        
        $this->load->model('Gsptpd');
        $this->Gsptpd->gSptpd($npwp,$golongan,$tahun,$tmpUsaha);
    }

    public function gTagihanTahun() {
        $npwp = $_GET['npwp'];
        $idTmpUsaha = $_GET['idTmpUsaha'];
        
        $this->load->model('GtagihanTahun');
        $this->GtagihanTahun->gTagihanTahun($npwp,$idTmpUsaha);
    }

    public function gTagihan() {
        $npwp = $_GET['npwp'];
        $idOp = $_GET['idOp'];
        $tahun = $_GET['tahun'];
        $tmpUsaha = $_GET['tmpUsaha'];
        
        $this->load->model('Gtagihan');
        $this->Gtagihan->gTagihan($npwp,$idOp,$tahun,$tmpUsaha);
    }

    public function gStatusUser() {
        $npwp = $_GET['npwp'];
        $value = $_GET['value'];
        
        $this->load->model('GstatusUser');
        $this->GstatusUser->gStatusUser($npwp);
    }

    public function gKecKel() {
        $value = $_GET['value'];
        
        $this->load->model('GkecKel');
        $this->GkecKel->gKecKel();
    }

    public function pUTmpUsaha() {
        $idInc = $_POST['idInc'];
        $nmUsaha = $_POST['nmUsaha'];
        $almtUsaha = $_POST['almtUsaha'];
        $kdKel = $_POST['kdKel'];
        $kdKec = $_POST['kdKec'];

        $this->load->model('PuTmpUsaha');
        $this->PuTmpUsaha->pUTmpUsaha($idInc,$nmUsaha,$almtUsaha,$kdKel,$kdKec);
    }

    public function pUEmail() {
        $npwp = $_POST['npwp'];
        $email = $_POST['email'];

        $this->load->model('PuEmail');
        $this->PuEmail->pUEmail($npwp,$email);
    }

    public function pLupaPass() {
        $email = $_POST['email'];
        $sql = "SELECT NPWP FROM WAJIB_PAJAK WHERE EMAIL='$email'";
		$result = $this->db->query($sql)->result_array();
        if (count($result) > 0) {
            foreach ($result as $row) {
                $NPWP = $row["NPWP"];
                $sql1 = "SELECT PASSWORD FROM MS_PENGGUNA WHERE NIP='$NPWP'";
                $result1 = $this->db->query($sql1)->result_array();
                if (count($result1) > 0) {
                    foreach ($result1 as $row1) {
                        $PASSWORD = $row1["PASSWORD"];
                        $pass_baru = $this->password_generate('5');
                        $sql2 = "UPDATE MS_PENGGUNA SET PASSWORD = '$pass_baru' WHERE NIP='$NPWP'";
                        $result2 = $this->db->query($sql2);
                        if ($result2 == true) {
                            $this->terima_pengajuan($email,$pass_baru,$NPWP);
                        } else {
                            $response["success"] = 0;
                            $response["message"] = "Gagal kirim email tujuan.";
                            echo json_encode($response);
                        }
                    }
                } else {
                    $response["success"] = 0;
                    $response["message"] = "Data pengguna tidak ditemukan(2).";
                    echo json_encode($response);
                }
            }
        } else {
            $response["success"] = 0;
            $response["message"] = "Data pengguna tidak ditemukan(1).";
            echo json_encode($response);
        }
    }

    public function pUTelp() {
        $npwp = $_POST['npwp'];
        $telp = $_POST['telp'];

        $this->load->model('PuTelp');
        $this->PuTelp->pUTelp($npwp,$telp);
    }

    public function pUpdateSptpdBerkas() {
        $idSptpd = $_POST['idSptpd'];
        $jmhFile = $_POST['jmlFile'];
        $jnsPajak = $_POST['jnsPajak'];
        $brksFile = $_POST['brksFile'];

        $filePathAbsolute = "";
        $sukses = "";
        $uploadedFiles = "";
        for ($i = 0; $i < $jmhFile; $i++) {
            
            $iterasiUploadFile = $i+1;
            if (isset($_FILES["uploaded_file$iterasiUploadFile"]["name"])) {
                if ($i == 0) {
                    $uploadedFiles .= "uploaded_file$iterasiUploadFile";
                } else {
                    $uploadedFiles .= ","."uploaded_file$iterasiUploadFile";
                }
            } else { 
                $sukses .= "fail1";
                return $sukses;
			}
        }

        $valuesString = "$idSptpd|$uploadedFiles|$jnsPajak|$brksFile";

        $this->load->model('PupdateSptpdBerkas');
        $this->PupdateSptpdBerkas->pUpdateSptpdBerkas($valuesString);
    }

    public function pInsertSptpd() {
        $jnsPajak = $_POST['jnsPajak'];
        $msPajak = $_POST['msPajak'];
        $thnPajak = $_POST['thnPajak'];
        $nmWp = $_POST['nmWp'];
        $idUsaha = $_POST['idUsaha'];
        $almtWp = $_POST['almtWp'];
        $nmUsaha = $_POST['nmUsaha'];
        $almtUsaha = $_POST['almtUsaha'];
        $npwpd = $_POST['npwpd'];
        $tglPenerimaan = $_POST['tglPenerimaan'];
        $berlakuMulai = $_POST['berlakuMulai'];
        $kodeKec = $_POST['kodeKec'];
        $kodeKel = $_POST['kodeKel'];
        $idOp = $_POST['idOp'];
        $dsrPengenaan = $_POST['dsrPengenaan'];
        if (empty($dsrPengenaan)) {
            $dsrPengenaan = "-";
        }
        $jnsEntrian = $_POST['jnsEntrian'];
        $jmhFile = $_POST['jmlFile'];
        $dpp = $_POST['dpp'];
        $npwpInsert = $_POST['npwpInsert'];
        $ipInsert = $_POST['ipInsert'];
        $tglInsert = $_POST['tglInsert'];
        $pjkTerutang = $_POST['pjkTerutang'];
        $masa = $_POST['masa'];
        $pajak = $_POST['pajak'];
        $status = $_POST['status'];
        $device = $_POST['device'];
        $statusPengiriman = $_POST['statusPengiriman'];

        $filePathAbsolute = "";
        $sukses = "";
        $uploadedFiles = "";
        for ($i = 0; $i < $jmhFile; $i++) {
            
            $iterasiUploadFile = $i+1;
            if (isset($_FILES["uploaded_file$iterasiUploadFile"]["name"])) {
                if ($i == 0) {
                    $uploadedFiles .= "uploaded_file$iterasiUploadFile";
                } else {
                    $uploadedFiles .= ","."uploaded_file$iterasiUploadFile";
                }
            } else { 
                $sukses .= "fail1";
                return $sukses;
			}
        }
        
        $rek=$this->db->query("select  getNoRek('$idOp')REK from dual")->row();
        $rek=$rek->REK;
        
        $kb=$this->Mpokok->getSqIdBiling();
        $bank=$this->db->query("select getnorek_bank('$idOp')BANK from dual")->row();
        $queryKdBilling=$this->db->query("SELECT ('$bank->BANK'||TO_CHAR(SYSDATE, 'yymmdd')||'$kb')ASDF FROM dual")->row();
        $queryKdBilling=$queryKdBilling->ASDF;

        $va=$this->Mpokok->getVa($idOp);

        $noform=$this->Mpokok->getFormulir();

        $exp=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed from dual")->row();
        $ed=$exp->ED;
        
        $valuesString = "$jnsPajak|$noform|$msPajak|$thnPajak|$nmWp|$almtWp
        |$idUsaha|$nmUsaha|$almtUsaha|$npwpd|$tglPenerimaan|$berlakuMulai
        |$kodeKec|$kodeKel|$idOp|$dsrPengenaan|$jnsEntrian|$dpp|$npwpInsert
        |$ipInsert|$tglInsert|$pjkTerutang|$masa|$pajak|$status|$rek|$queryKdBilling
        |$device|$statusPengiriman|$uploadedFiles|$va|$ed";
        
        $this->load->model('PinsertSptpd');
        $this->PinsertSptpd->pInsertSptpd($valuesString);
    }

    function terima_pengajuan($email="",$pass_baru,$npwp){
        
        $tujuan=$email;
        //$tujuan='adypabbsi@gmail.com';
        $ci                  = get_instance();
        $ci->load->library('email');
        $config['protocol']  = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "malangbapendakab@gmail.com"; 
        $config['smtp_pass'] = "B4p3nd4km";
        $config['charset']   = "utf-8";
        $config['mailtype']  = "html";
        $config['newline']   = "\r\n";          
        $ci->email->initialize($config);            
        $ci->email->from('Bapenda Kab. Malang', 'PDRD.KAB.MALANG');
        $list                = array($tujuan);
        $ci->email->to($list);
        $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
        $ci->email->subject('Permintaan Reset Password');
        // $ci->email->message('Sesuai permintaan saudara untuk melakukan reset password, maka kami kirimkan data sebagaui berikut,
        // user anda '.$npwp.', password baru '.$pass_baru.'. Demikian harap digunakan sebaik-baiknya.');
        $messageFromHtml = $this->messageHtml1($npwp,$pass_baru,$email);
        $ci->email->message($messageFromHtml);
        $ci->email->send();
        $response["success"] = 1;
        $response["message"] = "Password baru user $npwp telah dikirim.";
        echo json_encode($response);
    }

    function messageHtml($npwp,$pass_baru,$email) {
        return
        '<html>
            <body>
            <canvas id="myCanvas" width="504" height="48"
            style="border:1px solid #d3d3d3;">
            Your browser does not support the canvas element.
            </canvas>
            <br/>
            <br/>
            Kepada Yth.<br/>
            Pemilik Email '.$email.'<br/>
            di<br/>
            Tempat<br/>
            <br/>
            Anda melakukan pengajuan <b>Reset Password</b> Aplikasi Wajib Pajak Priority Bapenda Kab. Malang, maka kami kirimkan data sebagai berikut:<br/>
            <br/>
                &emsp;&emsp;<b>Username	: '.$npwp.'</b><br/>
                &emsp;&emsp;<b>Password baru	: '.$pass_baru.'</b><br/>
            <br/>
            Demikian Informasi ini kami sampaikan, mohon digunakan sebaik-baiknya<br/>
            <br/>
            Hormat Kami,<br/>
            <br/>
            Admin<br/>
            <br/>
            Email ini dihasilkan secara otomatis, mohon untuk tidak membalas email ini.<br/>
            
            <script>
                var canvas = document.getElementById("myCanvas");
                canvas.width = window.innerWidth;
                var ctx = canvas.getContext("2d");
                ctx.font = "18px Arial";
                ctx.fillStyle = "#0055aa";
                ctx.fillRect(0,0,canvas.width, canvas.height);
                ctx.fillStyle = "white";
                ctx.textAlign = "center";
                ctx.fillText("BADAN PENDAPATAN KAB. MALANG", canvas.width/2, canvas.height/2);
            </script>
            
            </body>
        </html>';
    }

    function messageHtml1($npwp,$pass_baru,$email) {
        return
        '<html>
            <head>
                <style>
                    #top {
                        width: 100%;
                        padding: 1px 0;
                        background-color: #0055aa;
                    }
                    h3 {color: white; text-align:center;}
                </style>
            </head>
            <body>
                <div id="top" ><h3>BADAN PENDAPATAN KAB. MALANG</h3></div>
                <br/>
                <br/>
                Kepada Yth.<br/>
                Pemilik Email '.$email.'<br/>
                di<br/>
                Tempat<br/>
                <br/>
                Anda melakukan pengajuan <b>Reset Password</b> Aplikasi Wajib Pajak Priority Bapenda Kab. Malang, maka kami kirimkan data sebagai berikut:<br/>
                <br/>
                    &emsp;&emsp;<b>Username	: '.$npwp.'</b><br/>
                    &emsp;&emsp;<b>Password baru	: '.$pass_baru.'</b><br/>
                <br/>
                Demikian Informasi ini kami sampaikan, mohon digunakan sebaik-baiknya<br/>
                <br/>
                Hormat Kami,<br/>
                <br/>
                Admin<br/>
                <br/>
                Email ini dihasilkan secara otomatis, mohon untuk tidak membalas email ini.<br/>
            </body>
        </html>';
    }

    function password_generate($chars) {
        $data = '1234567890';
        return substr(str_shuffle($data), 0, $chars);
    }

}
