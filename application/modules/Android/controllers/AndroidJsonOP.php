<?php
	/**
	 *
	 */
	class AndroidJsonOP extends CI_Controller
	{

	    function __construct()
	    {
	        parent::__construct();

	        $this->load->model('Wp/Mtempat_usaha');
	        $this->load->model('Sptpd_hotel/Msptpd_hotel');
	        $this->load->model('Sptpd_restoran/Msptpd_restoran');
	        $this->load->model('Sptpdhiburan/Mhiburan');
	        //$this->load->model('Sptpdreklame/Mreklame');
	        $this->load->model('Sptpd_ppj/Msptpd_ppj');
	        //$this->load->model('Sptpd_mblbm/Msptpd_mblbm');
	        //$this->load->model('Sptpd_parkir/Msptpd_parkir');
	        $this->load->model('Sptpdair/Mair');
	    }

		public function index($value='')
		{
			$response = array();
			$countfiles = count($_FILES['foto']['name']);
			$foto;
					//uploading file and storing it to database as well
			for($i=0;$i<$countfiles;$i++){
				$foto.=$_FILES['foto']['name'][$i];

			}

			$response['error'] = false;
			$response['message'] = "Sukses".$foto;
			echo json_encode($response);
		}

		function Auth()
		{
			$result = array();
			$u = $_POST['username'];
			$p = $_POST['password'];

			$hasil = $this->db->query("SELECT * FROM MS_PENGGUNA WHERE username='$u' AND password='$p'")->result();

			if (count($hasil) == '1') {
				foreach ($hasil as $hasil) {
					# code...
					array_push($result, array(
						'pesan'       =>'1',
						'id_inc'      => $hasil->ID_INC,
						'nip'         => $hasil->NIP,
						'nama'        => $hasil->NAMA,
						'unit_upt_id' => $hasil->UNIT_UPT_ID,
						'ms_role_id'  => $hasil->MS_ROLE_ID,
					));

				}
			}
			else {

				array_push($result, array(
					'pesan'=>'0',
				));
			}

			echo json_encode(array("result"=>$result));
		}

		function cekData()
		{
			$result = array();

			$hasil = $this->db->query("SELECT A.ID_INC, NAMA_USAHA, ALAMAT_USAHA, NAMA_PAJAK,A.JENIS_PAJAK, A.KODEKEC, A.KODEKEL, A.ID_OP, DESKRIPSI, NAMAKELURAHAN, NAMAKEC, NPWPD, NAMA AS NAMA_WP FROM TEMPAT_USAHA A JOIN JENIS_PAJAK B ON A.JENIS_PAJAK = B.ID_INC LEFT JOIN OBJEK_PAJAK C ON A.ID_OP = C.ID_OP JOIN KECAMATAN D ON A.KODEKEC = D.KODEKEC JOIN WAJIB_PAJAK F ON A.NPWPD = F.NPWP JOIN KELURAHAN E ON A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC ORDER BY A.ID_INC ASC")->result();


			array_push($result, array(
					'jml_data'   => count($hasil),

				));


			echo json_encode(array("result"=>$result));
		}

		function SinkronPemutakhiran()
		{
			$result = array();

			$hasil = $this->db->query("SELECT A.ID_INC, NAMA_USAHA, ALAMAT_USAHA, NAMA_PAJAK,A.JENIS_PAJAK, A.KODEKEC, A.KODEKEL, A.ID_OP, DESKRIPSI, NAMAKELURAHAN, NAMAKEC, NPWPD, NAMA AS NAMA_WP FROM TEMPAT_USAHA A JOIN JENIS_PAJAK B ON A.JENIS_PAJAK = B.ID_INC LEFT JOIN OBJEK_PAJAK C ON A.ID_OP = C.ID_OP JOIN KECAMATAN D ON A.KODEKEC = D.KODEKEC JOIN WAJIB_PAJAK F ON A.NPWPD = F.NPWP JOIN KELURAHAN E ON A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC ORDER BY A.ID_INC ASC")->result();

			foreach ($hasil as $hasil) {
				if($hasil->ID_OP==''){
					$id_op = "0";
				}else{
					$id_op = $hasil->ID_OP;
				}

				if($hasil->DESKRIPSI==''){
					$gol = "";
				}else{
					$gol = $hasil->DESKRIPSI;
				}
				array_push($result, array(
					'nama_usaha'   => $hasil->NAMA_USAHA,
					'id_inc'       => $hasil->ID_INC,
					'alamat_usaha' => $hasil->ALAMAT_USAHA,
					'npwpd'        => $hasil->NPWPD,
					'namawp'       => $hasil->NAMA_WP,
					'kodekec'      => $hasil->KODEKEC,
					'kodekel'      => $hasil->KODEKEL,
					'namakec'      => $hasil->NAMAKEC,
					'namakel'      => $hasil->NAMAKELURAHAN,
					'jenis_pajak'  => $hasil->JENIS_PAJAK,
					'namapajak'    => $hasil->NAMA_PAJAK,
					'id_op'        => $id_op,
					'golongan'     => $gol,

				));
			}

			echo json_encode(array("result"=>$result));
		}

		function getJenisPajak()
		{
			$result = array();

			$query = $this->Mtempat_usaha->ListJenisPajak();

			foreach ($query as $a) {

				array_push($result, array(
					'kd_jenis_pajak'=>$a->ID_INC,
					'nama_pajak'=>$a->NAMA_PAJAK
				));

			}

			echo json_encode(array("result"=>$result));
		}

		function getGolongan()
		{
			$result = array();
			$ID_INC = $_POST['id_inc'];

			if ($ID_INC=='01') {
	        	$query=$this->Msptpd_hotel->getGol();
	        }
	        //RESTORAN
	        if ($ID_INC=='02') {
	        	$query=$this->Msptpd_restoran->getGol();;
	        }
	        //HIBURAN
	        if ($ID_INC=='03') {
	        	$query=$this->Mhiburan->getGol();
	        }
	        //PPJ
	        if ($ID_INC=='05') {
	        	$query=$this->Msptpd_ppj->getGol();
	        }
	        //Parkir
	        if ($ID_INC=='07') {
	        	$query=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM OBJEK_PAJAK WHERE ID_OP=456 ")->result();
	        }
	        if ($ID_INC=='08') {
	       		$query=$this->Mair->Gol();
	        }
	        if ($ID_INC=='09') {
	        	$query=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM OBJEK_PAJAK WHERE ID_OP=459")->result();
	        }

	        foreach ($query as $a) {
	        	array_push($result, array(
					'id_op'=>$a->ID_OP,
					'deskripsi'=>$a->DESKRIPSI,
				));
	        }

	        echo json_encode(array("result"=>$result));
		}

		function TambahData()
		{
			$pesan=0;
			$FOTO='';
			$NAMA_USAHA   = $_POST['nama_usaha'];
			$ALAMAT_USAHA = $_POST['alamat_usaha'];
			$NPWPD        = $_POST['npwpd'];
			$NM_KECAMATAN = $_POST['nm_kecamatan'];
			$NM_KELURAHAN = $_POST['nm_kelurahan'];
			$JENIS_PAJAK  = $_POST['jenis_pajak'];
			$ID_OP        = $_POST['id_op'];
			$DATE_INSERT  = date('m/d/Y H:i:s', strtotime($_POST['date_insert']));
			$LOKASI       = $_POST['lokasi'];
			$USERNAME     = $_POST['username'];

            $ImageCount = count($_FILES['foto']['name']);
            for($i=0; $i< $ImageCount; $i++)
    		{
				$tmp_file  =$_FILES['foto']['tmp_name'][$i];
				$name_file =$_FILES['foto']['name'][$i];

            	if($name_file!=''){
                    $explode  = explode('.',$name_file);
                    $extensi  = $explode[count($explode)-1];
                    $img_name_op=uniqid().'.'.$extensi;
                    $nf='upload/objek_pajak/'.$img_name_op;
                    if(move_uploaded_file($tmp_file,$nf)){
                       	$pesan=1;
                       	$FOTO .= $img_name_op.',';
                   	}else{
                       	$pesan=0;
                    }

                 }
    		}


    		if($pesan==1){

	    		$datax = array(
					'NAMA_USAHA'         => $NAMA_USAHA,
					'ALAMAT_USAHA'       => $ALAMAT_USAHA,
					'NPWPD'              => $NPWPD,
					'NM_KECAMATAN'       => $NM_KECAMATAN,
					'NM_KELURAHAN'       => $NM_KELURAHAN,
					'JENIS_PAJAK'        => $JENIS_PAJAK,
					'ID_OP'              => $ID_OP,
					'LOKASI'             => $LOKASI,
					'STATUS_USAHA_AKTIF' => '0',
					'USERNAME'           => $USERNAME,
					'FOTO'               => rtrim($FOTO,","),
	    		);


	  			$this->db->set('DATE_INSERT',"to_date('$DATE_INSERT','MM/DD/YYYY HH24:MI:SS ')",false);
	    		$query = $this->db->insert('MOBILE_OBJEK_PAJAK', $datax);

				if($query){
					echo "sukses";
				}else{
				 	//echo "gagal";
				}
    		}else{
    			//echo "gagal";
    		}



		}

		function TambahData2()
		{
			$pesan=0;
			$FOTO='';
			$NAMA_USAHA   = $_GET['nama_usaha'];
			$ALAMAT_USAHA = $_GET['alamat_usaha'];
			$NPWPD        = $_GET['npwpd'];
			$NM_KECAMATAN = $_GET['nm_kecamatan'];
			$NM_KELURAHAN = $_GET['nm_kelurahan'];
			$JENIS_PAJAK  = $_GET['jenis_pajak'];
			$ID_OP        = $_GET['id_op'];
			$DATE_INSERT  = date('m/d/Y H:i:s', strtotime($_GET['date_insert']));
			$LOKASI       = $_GET['lokasi'];
			$USERNAME     = $_GET['username'];

            $ImageCount = count($_FILES['foto']['name']);
            for($i=0; $i< $ImageCount; $i++)
    		{
				$tmp_file  =$_FILES['foto']['tmp_name'][$i];
				$name_file =$_FILES['foto']['name'][$i];

            	if($name_file!=''){
                    $explode  = explode('.',$name_file);
                    $extensi  = $explode[count($explode)-1];
                    $img_name_op=uniqid().'.'.$extensi;
                    $nf='upload/objek_pajak/'.$img_name_op;
                    if(move_uploaded_file($tmp_file,$nf)){
                       	$pesan=1;
                       	$FOTO .= $img_name_op.',';
                   	}else{
                       	$pesan=0;
                    }

                 }
    		}


    		if($pesan==1){

	    		$datax = array(
					'NAMA_USAHA'         => $NAMA_USAHA,
					'ALAMAT_USAHA'       => $ALAMAT_USAHA,
					'NPWPD'              => $NPWPD,
					'NM_KECAMATAN'       => $NM_KECAMATAN,
					'NM_KELURAHAN'       => $NM_KELURAHAN,
					'JENIS_PAJAK'        => $JENIS_PAJAK,
					'ID_OP'              => $ID_OP,
					'LOKASI'             => $LOKASI,
					'STATUS_USAHA_AKTIF' => '0',
					'USERNAME'           => $USERNAME,
					'FOTO'               => rtrim($FOTO,","),
	    		);


	  			$this->db->set('DATE_INSERT',"to_date('$DATE_INSERT','MM/DD/YYYY HH24:MI:SS ')",false);
	    		$query = $this->db->insert('MOBILE_OBJEK_PAJAK', $datax);

				if($query){
					echo "sukses";
				}else{
				 	echo "gagal";
				}
    		}else{
    			echo "gagal";
    		}



		}

		function getDataOP($value='')
		{
			$query = $this->db->query('SELECT * FROM MOBILE_OBJEK_PAJAK')->result_array();
			echo var_dump($query);
		}

		function deleteDataOP($value='')
		{
			$query = $this->db->query('DELETE FROM MOBILE_OBJEK_PAJAK');

			if($query){
				echo "sukses hapus";
			}else{
				echo "gagal";
			}
		}

	}
?>