<?php
	/**
	 *
	 */
	class AndroidJsonOPBaru extends CI_Controller
	{

	    function __construct()
	    {
	        parent::__construct();

	        $this->load->model('Android/MAndroidJsonOP');
	    }

		public function index($value='')
		{
			$response = array();
			$countfiles = count($_FILES['foto']['name']);
			$foto;
					//uploading file and storing it to database as well
			for($i=0;$i<$countfiles;$i++){
				$foto.=$_FILES['foto']['name'][$i];

			}

			$response['error'] = false;
			$response['message'] = "Sukses".$foto;
			echo json_encode($response);
		}

		//fix
		function Auth()
		{
			$result = array();
			$u = $_POST['username'];
			$p = $_POST['password'];

			$hasil = $this->db->query("SELECT * FROM PDRD2.MS_PENGGUNA WHERE USERNAME='$u' AND PASSWORD='$p' AND MS_ROLE_ID='4'")->result();

			if (count($hasil) == '1') {
				foreach ($hasil as $hasil) {
					# code...
					array_push($result, array(
						'pesan'       =>'1',
						'id_inc'      => $hasil->ID_INC,
						'nip'         => $hasil->NIP,
						'nama'        => $hasil->NAMA,
						'unit_upt_id' => $hasil->UNIT_UPT_ID,
						'ms_role_id'  => $hasil->MS_ROLE_ID,
					));

				}
			}
			else {

				array_push($result, array(
					'pesan'=>'0',
				));
			}

			echo json_encode(array("result"=>$result));
		}

		//fix
		function cekData()
		{
			$result = array();

			$hasil = $this->db->query("SELECT A.ID_INC, NAMA_USAHA, ALAMAT_USAHA, NAMA_PAJAK,A.JENIS_PAJAK, A.KODEKEC, A.KODEKEL, A.ID_OP, DESKRIPSI, NAMAKELURAHAN, NAMAKEC, NPWPD, NAMA AS NAMA_WP FROM PDRD2.TEMPAT_USAHA A JOIN PDRD2.JENIS_PAJAK B ON A.JENIS_PAJAK = B.ID_INC LEFT JOIN PDRD2.OBJEK_PAJAK C ON A.ID_OP = C.ID_OP JOIN PDRD2.KECAMATAN D ON A.KODEKEC = D.KODEKEC JOIN PDRD2.WAJIB_PAJAK F ON A.NPWPD = F.NPWP JOIN PDRD2.KELURAHAN E ON A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC ORDER BY A.ID_INC ASC")->result();


			array_push($result, array(
					'jml_data'   => count($hasil),

				));


			echo json_encode(array("result"=>$result));
		}

		//fix
		function SinkronPemutakhiran()
		{
			$result = array();

			$hasil = $this->db->query("SELECT A.ID_INC, NAMA_USAHA, ALAMAT_USAHA, NAMA_PAJAK,A.JENIS_PAJAK, A.KODEKEC, A.KODEKEL, A.ID_OP, DESKRIPSI, NAMAKELURAHAN, NAMAKEC, NPWPD, NAMA AS NAMA_WP FROM PDRD2.TEMPAT_USAHA A JOIN PDRD2.JENIS_PAJAK B ON A.JENIS_PAJAK = B.ID_INC LEFT JOIN PDRD2.OBJEK_PAJAK C ON A.ID_OP = C.ID_OP JOIN PDRD2.KECAMATAN D ON A.KODEKEC = D.KODEKEC JOIN PDRD2.WAJIB_PAJAK F ON A.NPWPD = F.NPWP JOIN PDRD2.KELURAHAN E ON A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC ORDER BY A.ID_INC ASC")->result();

			foreach ($hasil as $hasil) {
				if($hasil->ID_OP==''){
					$id_op = "0";
				}else{
					$id_op = $hasil->ID_OP;
				}

				if($hasil->DESKRIPSI==''){
					$gol = "";
				}else{
					$gol = $hasil->DESKRIPSI;
				}
				array_push($result, array(
					'nama_usaha'   => $hasil->NAMA_USAHA,
					'id_inc'       => $hasil->ID_INC,
					'alamat_usaha' => $hasil->ALAMAT_USAHA,
					'npwpd'        => $hasil->NPWPD,
					'namawp'       => $hasil->NAMA_WP,
					'kodekec'      => $hasil->KODEKEC,
					'kodekel'      => $hasil->KODEKEL,
					'namakec'      => $hasil->NAMAKEC,
					'namakel'      => $hasil->NAMAKELURAHAN,
					'jenis_pajak'  => $hasil->JENIS_PAJAK,
					'namapajak'    => $hasil->NAMA_PAJAK,
					'id_op'        => $id_op,
					'golongan'     => $gol,

				));
			}

			echo json_encode(array("result"=>$result));
		}

		//fix
		function getJenisPajak()
		{
			$result = array();

			$query = $this->MAndroidJsonOP->ListJenisPajak();

			foreach ($query as $a) {

				array_push($result, array(
					'kd_jenis_pajak'=>$a->ID_INC,
					'nama_pajak'=>$a->NAMA_PAJAK
				));

			}

			echo json_encode(array("result"=>$result));
		}

		//fix
		function getGolongan()
		{
			$result = array();
			$ID_INC = $_POST['id_inc'];

			if ($ID_INC=='01') {
	        	$query=$this->MAndroidJsonOP->getGolHotel();
	        }
	        //RESTORAN
	        if ($ID_INC=='02') {
	        	$query=$this->MAndroidJsonOP->getGolRestoran();;
	        }
	        //HIBURAN
	        if ($ID_INC=='03') {
	        	$query=$this->MAndroidJsonOP->getGolHiburan();
	        }
	        //PPJ
	        if ($ID_INC=='05') {
	        	$query=$this->MAndroidJsonOP->getGolPpj();
	        }
	        //Parkir
	        if ($ID_INC=='07') {
	        	$query=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM PDRD2.OBJEK_PAJAK WHERE ID_OP=456 ")->result();
	        }
	        if ($ID_INC=='08') {
	       		$query=$this->MAndroidJsonOP->getGolAir();
	        }
	        if ($ID_INC=='09') {
	        	$query=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM PDRD2.OBJEK_PAJAK WHERE ID_OP=459")->result();
	        }

	        foreach ($query as $a) {
	        	array_push($result, array(
					'id_op'=>$a->ID_OP,
					'deskripsi'=>$a->DESKRIPSI,
				));
	        }

	        echo json_encode(array("result"=>$result));
		}

		function TambahData()
		{
			$msg = "";
			$error = "";
			$status = "";
			try {        
				$pesan=0;
				$FOTO='';
				$NAMA_USAHA   = $_POST['nama_usaha'];
				$ALAMAT_USAHA = $_POST['alamat_usaha'];
				$NPWPD        = $_POST['npwpd'];
				$NM_KECAMATAN = $_POST['nm_kecamatan'];
				$NM_KELURAHAN = $_POST['nm_kelurahan'];
				$JENIS_PAJAK  = $_POST['jenis_pajak'];
				$ID_OP        = $_POST['id_op'];
				$DATE_INSERT  = date('m/d/Y H:i:s', strtotime($_POST['date_insert']));
				$LOKASI       = $_POST['lokasi'];
				$USERNAME     = $_POST['username'];
				$ID_INC       = $_POST['id_inc'];

				$data_log = array(
					'USERNAME'       => $USERNAME,
					'SDK'            => '0',
					'MODEL'          => '0',
					'INFO'           => $NAMA_USAHA.','.$ALAMAT_USAHA.','.$NPWPD.','.$NM_KECAMATAN.','.$NM_KELURAHAN.','.$JENIS_PAJAK.','.$ID_OP.','.$DATE_INSERT.','.$LOKASI.','.$USERNAME.','.$ID_INC,
					'ACTIVITY_CLASS' => 'TambahDataPHP'
				);

				$this->db->trans_begin();

				$this->db->set('DATE_INSERT',"to_date('$DATE_INSERT','MM/DD/YYYY HH24:MI:SS ')",false);
				$query = $this->db->insert('PDRD2.LOG_ERROR', $data_log);
				if ($this->db->trans_status() === FALSE)
				{
				        $this->db->trans_rollback();
				}
				else
				{
				        $this->db->trans_commit();
				}

				if(isset($_FILES['foto']['name'])){
					$ImageCount = count($_FILES['foto']['name']);

					try {  
						for($i=0; $i< $ImageCount; $i++)
			    		{
							$tmp_file  =$_FILES['foto']['tmp_name'][$i];
							$name_file =$_FILES['foto']['name'][$i];

			            	if($name_file!=''){
			                    $explode  = explode('.',$name_file);
			                    $extensi  = $explode[count($explode)-1];
			                    $img_name_op=uniqid().'.'.$extensi;
			                    $nf='upload/objek_pajak/'.$img_name_op;

			                    if(move_uploaded_file($tmp_file,$nf)){
			                       	$pesan=1;
			                       	$FOTO .= $img_name_op.',';
			                   	}else{
			                       	$pesan=0;
			                    }

			                 }
			    		}
					} catch (\Exception $ex) {
            			$msg = $ex->getMessage();
            			$status.=$msg;
        			} 

		    		if($pesan==1){
		    			try{
		    				$datax = array(
								'NAMA_USAHA'         => $NAMA_USAHA,
								'ALAMAT_USAHA'       => $ALAMAT_USAHA,
								'NPWPD'              => $NPWPD,
								'NM_KECAMATAN'       => $NM_KECAMATAN,
								'NM_KELURAHAN'       => $NM_KELURAHAN,
								'JENIS_PAJAK'        => $JENIS_PAJAK,
								'ID_OP'              => $ID_OP,
								'LOKASI'             => $LOKASI,
								'STATUS_USAHA_AKTIF' => '0',
								'USERNAME'           => $USERNAME,
								'ID_INC_USERNAME'    => $ID_INC,
								'FOTO'               => rtrim($FOTO,',')
			    			);


				  			$this->db->set('DATE_INSERT',"to_date('$DATE_INSERT','MM/DD/YYYY HH24:MI:SS ')",false);
				    		$query = $this->db->insert('PDRD2.MOBILE_OBJEK_PAJAK', $datax);

							if($query){
								$status= "sukses";
							}else{
								$status.= "Error Data1";
							}
		    			}catch (\Exception $ex) {
            				$msg = $ex->getMessage();
            				$status.=$msg;
        				} 
			    		
		    		}else{
		    			$status.= "Error Data2";
		    		}
				}else{
					try{
						$datax = array(
							'NAMA_USAHA'         => $NAMA_USAHA,
							'ALAMAT_USAHA'       => $ALAMAT_USAHA,
							'NPWPD'              => $NPWPD,
							'NM_KECAMATAN'       => $NM_KECAMATAN,
							'NM_KELURAHAN'       => $NM_KELURAHAN,
							'JENIS_PAJAK'        => $JENIS_PAJAK,
							'ID_OP'              => $ID_OP,
							'LOKASI'             => $LOKASI,
							'STATUS_USAHA_AKTIF' => '0',
							'USERNAME'           => $USERNAME,
							'ID_INC_USERNAME'    => $ID_INC
				    	);


				  		$this->db->set('DATE_INSERT',"to_date('$DATE_INSERT','MM/DD/YYYY HH24:MI:SS ')",false);
				    	$query = $this->db->insert('PDRD2.MOBILE_OBJEK_PAJAK', $datax);

						if($query){
							$status.= "sukses";
						}else{
							$status.= "Error Data3";
						}
					}catch (\Exception $ex) {
            			$msg = $ex->getMessage();
            			$status.=$msg;
        			} 
					
				}
        	} catch (\Exception $e) {
            	$msg = $e->getMessage();
            	$status.=$msg;
        	}			

        	echo $status;

		}

		// function TambahData2()
		// {
		// 	$pesan=0;
		// 	$FOTO='';
		// 	$NAMA_USAHA   = $_GET['nama_usaha'];
		// 	$ALAMAT_USAHA = $_GET['alamat_usaha'];
		// 	$NPWPD        = $_GET['npwpd'];
		// 	$NM_KECAMATAN = $_GET['nm_kecamatan'];
		// 	$NM_KELURAHAN = $_GET['nm_kelurahan'];
		// 	$JENIS_PAJAK  = $_GET['jenis_pajak'];
		// 	$ID_OP        = $_GET['id_op'];
		// 	$DATE_INSERT  = date('m/d/Y H:i:s', strtotime($_GET['date_insert']));
		// 	$LOKASI       = $_GET['lokasi'];
		// 	$USERNAME     = $_GET['username'];

  //           $ImageCount = count($_FILES['foto']['name']);
  //           for($i=0; $i< $ImageCount; $i++)
  //   		{
		// 		$tmp_file  =$_FILES['foto']['tmp_name'][$i];
		// 		$name_file =$_FILES['foto']['name'][$i];

  //           	if($name_file!=''){
  //                   $explode  = explode('.',$name_file);
  //                   $extensi  = $explode[count($explode)-1];
  //                   $img_name_op=uniqid().'.'.$extensi;
  //                   $nf='upload/objek_pajak/'.$img_name_op;
  //                   if(move_uploaded_file($tmp_file,$nf)){
  //                      	$pesan=1;
  //                      	$FOTO .= $img_name_op.',';
  //                  	}else{
  //                      	$pesan=0;
  //                   }

  //                }
  //   		}


  //   		if($pesan==1){

	 //    		$datax = array(
		// 			'NAMA_USAHA'         => $NAMA_USAHA,
		// 			'ALAMAT_USAHA'       => $ALAMAT_USAHA,
		// 			'NPWPD'              => $NPWPD,
		// 			'NM_KECAMATAN'       => $NM_KECAMATAN,
		// 			'NM_KELURAHAN'       => $NM_KELURAHAN,
		// 			'JENIS_PAJAK'        => $JENIS_PAJAK,
		// 			'ID_OP'              => $ID_OP,
		// 			'LOKASI'             => $LOKASI,
		// 			'STATUS_USAHA_AKTIF' => '0',
		// 			'USERNAME'           => $USERNAME,
		// 			'FOTO'               => rtrim($FOTO,','),
	 //    		);


	 //  			$this->db->set('DATE_INSERT',"to_date('$DATE_INSERT','MM/DD/YYYY HH24:MI:SS ')",false);
	 //    		$query = $this->db->insert('MOBILE_OBJEK_PAJAK', $datax);

		// 		if($query){
		// 			echo "sukses";
		// 		}else{
		// 		 	echo "gagal";
		// 		}
  //   		}else{
  //   			echo "gagal";
  //   		}

		// }

		function getDataOP($value='')
		{
			$query = $this->db->query('SELECT * FROM PDRD2.MOBILE_OBJEK_PAJAK')->result_array();
			echo var_dump($query);
		}

		function deleteDataOP($value='')
		{
			$query = $this->db->query('DELETE FROM PDRD2.MOBILE_OBJEK_PAJAK');

			if($query){
				echo "sukses hapus";
			}else{
				echo "gagal";
			}
		}

		function SaveLogError()
		{

			$DATE_INSERT = date('m/d/Y H:i:s');
			$data = array(
				'USERNAME'       => $_POST['username'],
				'SDK'            => $_POST['sdk'],
				'MODEL'          => $_POST['model'],
				'INFO'           => $_POST['info'],
				'ACTIVITY_CLASS' => $_POST['activity_class']
			);

			$this->db->trans_begin();

			$this->db->set('DATE_INSERT',"to_date('$DATE_INSERT','MM/DD/YYYY HH24:MI:SS ')",false);
			$query = $this->db->insert('PDRD2.LOG_ERROR', $data);
			if ($this->db->trans_status() === FALSE)
			{
			        $this->db->trans_rollback();
			        echo "gagal";
			}
			else
			{
			        $this->db->trans_commit();
			        echo "sukses";
			}
		}

	}
?>