<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class AndroidJsonEretribusi extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('master/Mpokok');
    }

    public function pLogin() {
        $usrnm = $_POST['usrnm'];
        $pwd = $_POST['pwd'];
        $kdAktivasi = $_POST['kdAktivasi'];
        
        $this->load->model('Plogin');
        $this->Plogin->pLogin($usrnm,$pwd,$kdAktivasi);
    }

    public function pTransaksi() {
        $nominal = $_POST['nominal'];
        $tipe = $_POST['tipe'];
        $idUser = $_POST['idUser'];
        
        $this->load->model('Ptransaksi');
        $this->Ptransaksi->pTransaksi($nominal,$tipe,$idUser);
    }

    public function gLaporan() {
        $value = $_GET['value'];
        $bulan = $_GET['bulan'];
        $tahun = $_GET['tahun'];
        // echo $value;
        
        $this->load->model('GlaporanEretribusi');
        $this->GlaporanEretribusi->gLaporanEretribusi($value,$bulan,$tahun);
    }

}
