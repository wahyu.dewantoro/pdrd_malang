 <div id="SELISIH">     
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Keperluan <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select  style="font-size:11px" id="KEPERLUAN" name="KEPERLUAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEPERLUAN=="Industri"){echo "selected";} ?> value="Industri|1.5">Industri</option>
      <option <?php if($KEPERLUAN=="Bisnis"){echo "selected";} ?> value="Bisnis|1.5">Bisnis</option>
      <!-- <option <?php if($KEPERLUAN=="Hasil Sendiri"){echo "selected";} ?> value="Hasil Sendiri|1.5">Hasil Sendiri</option> -->
    </select>
  </div>
   <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Tarif/ TDL<sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);'  readonly type="text" id="TARIF" name="TARIF" placeholder="Tarif/ TDL" required="required" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PERHITUNGAN=='Selisih Kwh'){echo $TARIF;} else {echo "";} ?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div >
</div>
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Pemakaian Bulan Ini<sup>*</sup>
  </label>
  <div class="col-md-9 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);'  type="text" id="PEMAKAIAN_BULAN_INI" name="PEMAKAIAN_BULAN_INI" placeholder="Pemakaian Bulan Ini" required="required" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PERHITUNGAN=='Selisih Kwh'){echo $PEMAKAIAN_BULAN_INI;} else {echo "";} ?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div> 
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Pemakaian Bulan Lalu<sup>*</sup>
  </label>
  <div class="col-md-9 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);'  type="text" id="PEMAKAIAN_BULAN_LALU" name="PEMAKAIAN_BULAN_LALU" placeholder="Pemakaian Bulan Lalu" required="required" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PERHITUNGAN=='Selisih Kwh'){echo $PEMAKAIAN_BULAN_LALU;} else {echo "";} ?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div>         
<script>

if('<?php echo $CARA_PERHITUNGAN ?>'=='Selisih Kwh'){

        JUMLAH_PEMAKAIAN.value=getNominal(<?php echo $JUMLAH_PEMAKAIAN ?>);
        DPP.value=getNominal(<?php echo $DPP ?>);
        PAJAK_TERUTANG.value=getNominal(<?php echo $PAJAK_TERUTANG ?>);
}else{
}
    function get() {
      var PEMAKAIAN_BULAN_INI= parseFloat(nominalFormat(document.getElementById("PEMAKAIAN_BULAN_INI").value))||0;
      var PEMAKAIAN_BULAN_LALU= parseFloat(nominalFormat(document.getElementById("PEMAKAIAN_BULAN_LALU").value))||0;
      var TARIF= parseFloat(nominalFormat(document.getElementById("TARIF").value))||0; 
      var PAJAK= parseFloat(document.getElementById("PAJAK").value)/100 ||0; 
      DPP.value=getNominal(Math.round((PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)*TARIF)) ;
      PAJAK_TERUTANG.value=getNominal(Math.round((PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)*TARIF*PAJAK))  ;
      JUMLAH_PEMAKAIAN.value=getNominal(Math.round(PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU))  ;
    }
    
    /* TARIF */
    var TARIF = document.getElementById('TARIF');
    TARIF.addEventListener('keyup', function(e)
    {
      TARIF.value = formatRupiah(this.value);
    });
    /* PEMAKAIAN_BULAN_LALU */
    var PEMAKAIAN_BULAN_LALU = document.getElementById('PEMAKAIAN_BULAN_LALU');
    PEMAKAIAN_BULAN_LALU.addEventListener('keyup', function(e)
    {
      PEMAKAIAN_BULAN_LALU.value = formatRupiah(this.value);
    });
    /* PEMAKAIAN_BULAN_INI */
    var PEMAKAIAN_BULAN_INI = document.getElementById('PEMAKAIAN_BULAN_INI');
    PEMAKAIAN_BULAN_INI.addEventListener('keyup', function(e)
    {
      PEMAKAIAN_BULAN_INI.value = formatRupiah(this.value);
    });
  $('#KEGUNAAN').on('change', function() {

    var exp = this.value.split("|");
    $("#JAM_OPERASI").val(exp[1]);
  })
  $('#KEPERLUAN').on('change', function() {
    var exp = this.value.split("|");
        $("#DAYA_TERPASANG").val(""); 
        $("#FAKTOR_DAYA").val("");
        //$("#TARIF").val("");
        $("#JUMLAH_PEMAKAIAN").val("");
        $("#DPP").val("");
        $("#PAJAK_TERUTANG").val("");
    $("#PAJAK").val(exp[1]);
      if (exp[0]=='Industri') {
          $("#TARIF").val('1672,5');
      } else if (exp[0]=='Bisnis') {
          $("#TARIF").val('1530');
      } else {
          $("#TARIF").val('0');
      }      
  })
</script>

         </div>     