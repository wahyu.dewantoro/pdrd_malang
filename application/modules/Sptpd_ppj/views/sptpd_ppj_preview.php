      <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
    <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
      <a class="btn btn-sm btn-primary" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>  
  <div class="clearfix"></div>   
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >        
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="col-md-6">
                      <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 
                      <input type="hidden" name="MASA_PAJAK" value="<?php echo $MASA_PAJAK;?>">  
                      <input style="font-size: 12px" type="hidden" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                  
                      <input type="hidden" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" <?php echo $disable ?> onchange="NamaUsaha(this.value);" >                  
                      <input type="hidden" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>" <?php echo $disable ?>>
                      <input type="hidden" name="ALAMAT_WP" value="<?php echo $ALAMAT_WP; ?>">

                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="19%"><b>Masa Pajak</b></td>
                            <td>: <?php echo $namabulan[$MASA_PAJAK] ?></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Tahun</b></td>
                            <td>: <?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>NPWPD</b></td>
                            <td>: <?php echo $NPWPD; ?></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Nama WP</b></td>
                            <td>: <?php echo $NAMA_WP; ?></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Alamat WP</b></td>
                            <td>: <?php echo $ALAMAT_WP; ?> </td>
                          </tr>                      
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-6">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="19%"><b>Nama Usaha</b></td>
                            <td>: <?php $exp=explode('|', $NAMA_USAHA);echo $exp[0]; ?></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Almat Usaha</b></td>
                            <td>: <?php echo $ALAMAT_USAHA; ?></td>
                          </tr>
                          <tr>
                            <?php $kec=$this->db->query(" select namakec from kecamatan where kodekec='$KECAMATAN'")->row();?>
                            <td><b>Kecamatan </b></td>
                            <td>: <?php echo $kec->NAMAKEC;?></td>
                          </tr>
                          <tr>
                            <?php $kel=$this->db->query("select namakelurahan from kelurahan where KODEKELURAHAN='$KELURAHAN'  and kodekec='$KECAMATAN'")->row();?>
                            <td><b>Kelurahan</b></td>
                            <td>: <?php echo $kel->NAMAKELURAHAN;?></td>
                          </tr>                       
                        </tbody>
                      </table>
                      <input name="NAMA_USAHA"   value="<?php echo $NAMA_USAHA?>" type="hidden">
                      <input name="ALAMAT_USAHA" value="<?php echo $ALAMAT_USAHA?>" type="hidden">
                      <input type="hidden" value="<?php echo $KECAMATAN?>"  name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12">
                      <input type="hidden" value="<?php echo $KELURAHAN?>"  name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12">  
                    </div> 
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >     
                    <div class="col-md-6">              
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                        <div class="col-md-9">
                          <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"  <?php echo $disable?> placeholder="No Formulir">
                        </div>
                      </div> -->
                      <input type="hidden" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >            
                    <input type="hidden" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required <?php echo $disable?> >
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                        <div class="col-md-9">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar">
                            </i></span>
                            <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                          </div>
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                        <div class="col-md-9">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar">
                            </i></span>
                            <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required <?php echo $disable?> >
                          </div>
                        </div>
                      </div> -->
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($kec as $kec){ ?>
                            <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select  style="font-size:11px" id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php if($button==('Update SPTPD Hotel'||'Form SPTPD Hiburan')){?>
                            <?php foreach($kel as $kel){ ?>
                            <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                            <?php }} ?>                          
                          </select>
                        </div>
                      </div>  -->
                      <?php
                      $persen = "var persen = new Array();\n";
                      $masa = "var masa = new Array();\n";
                      ?>   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN" <?php echo $disable?> name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                            <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                            <?php 
                            $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                            $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <script type="text/javascript">  
                      <?php echo $persen;echo $masa;?>             
                      function changeValue(id){
                        document.getElementById('PAJAK').value = persen[id].persen;
                        document.getElementById('MASA').value = masa[id].masa;
                      }
                    </script>    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Cara Perhitungan <sup>*</sup>
                      </label>
                      <div class="col-md-3">
                        <select  style="font-size:11px" id="CARA_PERHITUNGAN" <?php echo $disable?> name="CARA_PERHITUNGAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <option  <?php if($CARA_PERHITUNGAN=='Formula'){echo "selected";}?> value="Formula">Formula</option>
                          <option  <?php if($CARA_PERHITUNGAN=='Selisih Kwh'){echo "selected";}?> value="Selisih Kwh">Selisih Kwh</option>
                        </select>
                      </div>
                      <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                      <div class="col-md-3">
                        <div class="checkbox">
                          <label>
                            <input <?php echo $disable ?> type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                          </label>
                        </div>
                      </div>
                    </div>   
                    <div id="DATA_PAJAK">
                      <?php if ($CARA_PERHITUNGAN=="Selisih Kwh") { ?>
 <div id="SELISIH">     
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Keperluan <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select <?php echo $disable ?>  style="font-size:11px" id="KEPERLUAN" name="KEPERLUAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEPERLUAN=="Industri|1.5"){echo "selected";} ?> value="Industri|1.5">Industri</option>
      <option <?php if($KEPERLUAN=="Bisnis|1.5"){echo "selected";} ?> value="Bisnis|1.5">Bisnis</option>
      <!-- <option <?php if($KEPERLUAN=="Hasil Sendiri|1.5"){echo "selected";} ?> value="Hasil Sendiri|1.5">Hasil Sendiri</option> -->
    </select>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Tarif/ TDL<sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input <?php echo $disable ?> onchange='get(this);'  readonly type="text" id="TARIF" name="TARIF" placeholder="Tarif/ TDL" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $TARIF?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Pemakaian Bulan Ini<sup>*</sup>
  </label>
  <div class="col-md-9 col-sm-6 col-xs-12">
    <div class="input-group">
      <input <?php echo $disable ?> onchange='get(this);'  type="text" id="PEMAKAIAN_BULAN_INI" name="PEMAKAIAN_BULAN_INI" placeholder="Pemakaian Bulan Ini" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $PEMAKAIAN_BULAN_INI?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div> 
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Pemakaian Bulan Lalu<sup>*</sup>
  </label>
  <div class="col-md-9 col-sm-6 col-xs-12">
    <div class="input-group">
      <input <?php echo $disable ?> onchange='get(this);'  type="text" id="PEMAKAIAN_BULAN_LALU" name="PEMAKAIAN_BULAN_LALU" placeholder="Pemakaian Bulan Lalu" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $PEMAKAIAN_BULAN_LALU?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div>         
<script>

    function get() {
      var PEMAKAIAN_BULAN_INI= parseFloat(nominalFormat(document.getElementById("PEMAKAIAN_BULAN_INI").value))||0;
      var PEMAKAIAN_BULAN_LALU= parseFloat(nominalFormat(document.getElementById("PEMAKAIAN_BULAN_LALU").value))||0;
      var TARIF= parseFloat(nominalFormat(document.getElementById("TARIF").value))||0; 
      var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100 ||0; 
      DPP.value=getNominal((PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)*TARIF)  ;
      PAJAK_TERUTANG.value=getNominal((PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)*TARIF*PAJAK)  ;
      JUMLAH_PEMAKAIAN.value=getNominal(PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)  ;
    }
    
    /* TARIF */
    var TARIF = document.getElementById('TARIF');
    TARIF.addEventListener('keyup', function(e)
    {
      TARIF.value = formatRupiah(this.value);
    });
    /* PEMAKAIAN_BULAN_LALU */
    var PEMAKAIAN_BULAN_LALU = document.getElementById('PEMAKAIAN_BULAN_LALU');
    PEMAKAIAN_BULAN_LALU.addEventListener('keyup', function(e)
    {
      PEMAKAIAN_BULAN_LALU.value = formatRupiah(this.value);
    });
    /* PEMAKAIAN_BULAN_INI */
    var PEMAKAIAN_BULAN_INI = document.getElementById('PEMAKAIAN_BULAN_INI');
    PEMAKAIAN_BULAN_INI.addEventListener('keyup', function(e)
    {
      PEMAKAIAN_BULAN_INI.value = formatRupiah(this.value);
    });
  $('#KEGUNAAN').on('change', function() {

    var exp = this.value.split("|");
    $("#JAM_OPERASI").val(exp[1]);
  })
   $('#KEPERLUAN').on('change', function() {
    var exp = this.value.split("|");
        $("#DAYA_TERPASANG").val(""); 
        $("#FAKTOR_DAYA").val("");
        //$("#TARIF").val("");
        $("#JUMLAH_PEMAKAIAN").val("");
        $("#DPP").val("");
        $("#PAJAK_TERUTANG").val("");
    $("#PAJAK").val(exp[1]);
      if (exp[0]=='Industri') {
          $("#TARIF").val('1672,5');
      } else if (exp[0]=='Bisnis') {
          $("#TARIF").val('1530');
      } else {
          $("#TARIF").val('0');
      }      
  })
</script>

         </div>     
                     <?php } // selisih
elseif ($CARA_PERHITUNGAN=="Formula") { ?>
 <div id="FORMULA">
              
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kegunaan <?php echo $KEGUNAAN; ?> <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select <?php echo $disable ?>  style="font-size:11px" id="KEGUNAAN" name="KEGUNAAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEGUNAAN=="Utama|240"){echo "selected";} ?> value="Utama|240">Utama</option>
      <option <?php if($KEGUNAAN=="Cadangan|120"){echo "selected";} ?> value="Cadangan|120">Cadangan</option>
      <option <?php if($KEGUNAAN=="Darurat|30"){echo "selected";} ?> value="Darurat|30">Darurat</option>
    </select>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Keperluan <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select <?php echo $disable ?>  style="font-size:11px" id="KEPERLUAN" name="KEPERLUAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEPERLUAN=="Industri|1.5"){echo "selected";} ?> value="Industri|1.5">Industri</option>
      <option <?php if($KEPERLUAN=="Bisnis|1.5"){echo "selected";} ?> value="Bisnis|1.5">Bisnis</option>
      <!-- <option <?php if($KEPERLUAN=="Hasil Sendiri|1.5"){echo "selected";} ?> value="Hasil Sendiri|1.5">Hasil Sendiri</option> -->
    </select>
  </div>                      
</div>   
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jam Operasi <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);' readonly="" type="text" id="JAM_OPERASI" name="JAM_OPERASI" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $JAM_OPERASI; ?>" >
      <span class="input-group-addon">Jam</span>
    </div>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tarif / TDL <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <span class="input-group-addon">Rp</span>
      <input <?php echo $disable ?> onchange='get(this);' type="text" id="TARIF" name="TARIF" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $TARIF; ?>" >
    </div>
  </div>
</div>                     
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Daya Terpasang <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input <?php echo $disable ?> onchange='get(this);' type="text" id="DAYA_TERPASANG" name="DAYA_TERPASANG" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $DAYA_TERPASANG; ?>" >
      <span class="input-group-addon">Kva</span>
    </div>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Faktor Daya <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input <?php echo $disable ?> onchange='get(this);' type="text" id="FAKTOR_DAYA" name="FAKTOR_DAYA" required="required"  class="form-control col-md-7 col-xs-12" value="<?php echo $FAKTOR_DAYA; ?>" >
      <span class="input-group-addon">Kva</span>
    </div>
  </div>
</div>  
<script>

    function get() {
      var JAM_OPERASI= parseFloat(nominalFormat(document.getElementById("JAM_OPERASI").value))||0;
      var FAKTOR_DAYA= parseFloat(nominalFormat(document.getElementById("FAKTOR_DAYA").value))||0;
      var DAYA_TERPASANG= parseFloat(nominalFormat(document.getElementById("DAYA_TERPASANG").value))||0; 
      var TARIF= parseFloat(nominalFormat(document.getElementById("TARIF").value))||0; 
      var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100 ||0; 
      DPP.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG*TARIF)  ;
      PAJAK_TERUTANG.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG*TARIF*PAJAK)  ;
      JUMLAH_PEMAKAIAN.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG)  ;
    }
    
    /* TARIF */
    var TARIF = document.getElementById('TARIF');
    TARIF.addEventListener('keyup', function(e)
    {
      TARIF.value = formatRupiah(this.value);
    });
    /* DAYA_TERPASANG */
    var DAYA_TERPASANG = document.getElementById('DAYA_TERPASANG');
    DAYA_TERPASANG.addEventListener('keyup', function(e)
    {
      DAYA_TERPASANG.value = formatRupiah(this.value);
    });
    /* FAKTOR_DAYA */
    var FAKTOR_DAYA = document.getElementById('FAKTOR_DAYA');
    FAKTOR_DAYA.addEventListener('keyup', function(e)
    {
      FAKTOR_DAYA.value = formatRupiah(this.value);
    });
  $('#KEGUNAAN').on('change', function() {

    var exp = this.value.split("|");
    $("#JAM_OPERASI").val(exp[1]);
  })
   $('#KEPERLUAN').on('change', function() {
    var exp = this.value.split("|");
        $("#DAYA_TERPASANG").val(""); 
        $("#FAKTOR_DAYA").val("");
        //$("#TARIF").val("");
        $("#JUMLAH_PEMAKAIAN").val("");
        $("#DPP").val("");
        $("#PAJAK_TERUTANG").val("");
    $("#PAJAK").val(exp[1]);
      if (exp[0]=='Industri') {
          $("#TARIF").val('1672,5');
      } else if (exp[0]=='Bisnis') {
          $("#TARIF").val('1530');
      } else {
          $("#TARIF").val('0');
      }      
  })
</script>

         </div>     

<?php }?>

                    </div>
                    <?php if($disable==''){?>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" onClick="return validasi()">Generate ID Biling</button>
                        <a href="<?php echo site_url('Sptpd_ppj/sptpd_ppj') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                      </div>
                    </div>
                    <?php } ?> 
                  </div>                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                        <div class="col-md-8">
                        <input type="hidden" value="<?php echo $FILE;?>" name="FILE"> <?php echo $FILE;?>
                        </div>
                    </div>          
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                      <div class="col-md-4">
                        <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"  <?php echo $disable?> placeholder="Masa Pajak" >
                      </div>
                      <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                      <div class="col-md-4">
                        <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"  <?php echo $disable?> placeholder="Pajak" onchange='get(this);'>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Pemakaian <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input  type="text" id="JUMLAH_PEMAKAIAN" name="JUMLAH_PEMAKAIAN" required="required" placeholder="Jumlah Pemakaian" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_PEMAKAIAN; ?>" readonly="" <?php echo $disable?>>
                          <span class="input-group-addon">Kwh</span>
                        </div>
                      </div>
                    </div>     
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input <?php echo $disable ?> type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                        </div>
                      </div>  
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                        </div>
                      </div> 
                    </div> 
                  </div>
                </div>
              </div>     
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>



</div>
<script>

function validasi(){
  var npwpd=document.forms["demo-form2"]["NPWPD"].value;
  var number=/^[0-9]+$/; 
  if (npwpd==null || npwpd==""){
    swal("NPWPD Harus di Isi", "", "warning")
    return false;
  };
}
  $("#NPWPD").keyup(function(){
    var npwpd = $("#NPWPD").val();
    $.ajax({
      url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
      type: 'POST',
      data: "npwpd="+npwpd,
      cache: false,
      success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }

            }
          });
  });     
  function getkel(sel)
  {
    var KODEKEC = sel.value;
    $.ajax({
     type: "POST",
     url: "<?php echo base_url().'Master/pokok/getkel'?>",
     data: { KODEKEC: KODEKEC},
     cache: false,
     success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
  }
  $('#CARA_PERHITUNGAN').on('change', function() {
    if(this.value=='Formula'){
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/formula/'.$id;?>'); 
    } else if (this.value=='Selisih Kwh') {
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/selisih/'.$id;?>'); 

    } else {
      $('#FORMULA').remove();
      $('#SELISIH').remove();
    }
  })  
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa  = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
  function ribuan (angka)
  {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }

  function gantiTitikKoma(angka){
    return angka.toString().replace(/\./g,',');
  }
  function nominalFormat(angka){
    return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
  }
  function getNominal(angka){
    var nominal=gantiTitikKoma(angka);
    var indexKoma=nominal.indexOf(',');
    if (indexKoma==-1) {
      return ribuan(angka);
    } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
    }
  }
</script>
<style type="text/css">
  .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    padding: 6px 6px 6px 6px;
  }
  td{
    font-size:12px;
  }
</style>