 <div id="FORMULA">
              
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kegunaan <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select  style="font-size:11px" id="KEGUNAAN" name="KEGUNAAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEGUNAAN=="Utama"){echo "selected";} ?> value="Utama|240">Utama</option>
      <option <?php if($KEGUNAAN=="Cadangan"){echo "selected";} ?> value="Cadangan|120">Cadangan</option>
      <option <?php if($KEGUNAAN=="Darurat"){echo "selected";} ?> value="Darurat|30">Darurat</option>
    </select>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Keperluan <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select  style="font-size:11px" id="KEPERLUAN" name="KEPERLUAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEPERLUAN=="Industri"){echo "selected";} ?> value="Industri|1.5">Industri</option>
      <option <?php if($KEPERLUAN=="Bisnis"){echo "selected";} ?> value="Bisnis|1.5">Bisnis</option>
      <!-- <option <?php if($KEPERLUAN=="Hasil Sendiri"){echo "selected";} ?> value="Hasil Sendiri|1.5">Hasil Sendiri</option> -->
    </select>
  </div>                      
</div>   
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jam Operasi <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);' readonly="" type="text" id="JAM_OPERASI" name="JAM_OPERASI" required="required" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PERHITUNGAN=='Formula'){echo $JAM_OPERASI;} else {echo "";} ?>" >
      <span class="input-group-addon">Jam</span>
    </div>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tarif / TDL <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <span class="input-group-addon">Rp</span>
      <input onchange='get(this);' type="text" id="TARIF" name="TARIF" readonly required="required" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PERHITUNGAN=='Formula'){echo $TARIF;} else {echo "";} ?>" >
    </div>
  </div>
</div>                     
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Daya Terpasang <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);' type="text" id="DAYA_TERPASANG" name="DAYA_TERPASANG" required="required" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PERHITUNGAN=='Formula'){echo $DAYA_TERPASANG;} else {echo "";} ?>" >
      <span class="input-group-addon">Kva</span>
    </div>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Faktor Daya <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);' type="text" id="FAKTOR_DAYA" name="FAKTOR_DAYA" required="required"  class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PERHITUNGAN=='Formula'){echo $FAKTOR_DAYA;} else {echo "";} ?>" >
      <span class="input-group-addon">Kva</span>
    </div>
  </div>
</div> 
<script>

if('<?php echo $CARA_PERHITUNGAN ?>'=='Formula'){

        JUMLAH_PEMAKAIAN.value=getNominal(<?php echo $JUMLAH_PEMAKAIAN ?>);
        DPP.value=getNominal(<?php echo $DPP ?>);
        PAJAK_TERUTANG.value=getNominal(<?php echo $PAJAK_TERUTANG ?>);
}else{
}
    function get() {
      var JAM_OPERASI= parseFloat(nominalFormat(document.getElementById("JAM_OPERASI").value))||0;
      var FAKTOR_DAYA= parseFloat(nominalFormat(document.getElementById("FAKTOR_DAYA").value))||0;
      var DAYA_TERPASANG= parseFloat(nominalFormat(document.getElementById("DAYA_TERPASANG").value))||0; 
      var TARIF= parseFloat(nominalFormat(document.getElementById("TARIF").value))||0; 
      var PAJAK= parseFloat(document.getElementById("PAJAK").value)/100 ||0; 
      JUMLAH_PEMAKAIAN.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG)  ;
      DPP.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG*TARIF)  ;
      PAJAK_TERUTANG.value=getNominal(Math.round(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG*TARIF*PAJAK))  ;
    }
    
    /* TARIF */
    var TARIF = document.getElementById('TARIF');
    TARIF.addEventListener('keyup', function(e)
    {
      TARIF.value = formatRupiah(this.value);
    });
    /* DAYA_TERPASANG */
    var DAYA_TERPASANG = document.getElementById('DAYA_TERPASANG');
    DAYA_TERPASANG.addEventListener('keyup', function(e)
    {
      DAYA_TERPASANG.value = formatRupiah(this.value);
    });
    /* FAKTOR_DAYA */
    var FAKTOR_DAYA = document.getElementById('FAKTOR_DAYA');
    FAKTOR_DAYA.addEventListener('keyup', function(e)
    {
      FAKTOR_DAYA.value = formatRupiah(this.value);
    });
  $('#KEGUNAAN').on('change', function() {

    var exp = this.value.split("|");
    $("#JAM_OPERASI").val(exp[1]);
        $("#DAYA_TERPASANG").val(""); 
        $("#FAKTOR_DAYA").val("");
        //$("#TARIF").val("");
        $("#JUMLAH_PEMAKAIAN").val("");
        $("#DPP").val("");
        $("#PAJAK_TERUTANG").val("");
  })
  $('#KEPERLUAN').on('change', function() {
    var exp = this.value.split("|");
        $("#DAYA_TERPASANG").val(""); 
        $("#FAKTOR_DAYA").val("");
        //$("#TARIF").val("");
        $("#JUMLAH_PEMAKAIAN").val("");
        $("#DPP").val("");
        $("#PAJAK_TERUTANG").val("");
    $("#PAJAK").val(exp[1]);
      if (exp[0]=='Industri') {
          $("#TARIF").val('1672,5');
      } else if (exp[0]=='Bisnis') {
          $("#TARIF").val('1530');
      } else {
          $("#TARIF").val('0');
      }      
  })
</script>

         </div>     