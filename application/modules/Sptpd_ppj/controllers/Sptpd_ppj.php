<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_ppj extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Msptpd_ppj');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->load->model('Master/Mpokok');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
    public function index()
    {
      if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $npwpd=$_GET['NPWPD'];
        } else {
                $bulan='';//date('n');//date('d-m-Y');
                $tahun='';//date('Y');//date('d-m-Y');
                $npwpd='';
        }
        $sess=array(
                'ppj_bulan'=>$bulan,
                'ppj_tahun'=>$tahun,
                'ppj_npwpd'=>$npwpd
         );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','Sptpd_ppj/sptpd_ppj_list',$data);
  }
  public function json() {
    header('Content-Type: application/json');
    echo $this->Msptpd_ppj->json();
}
public function hapus()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $row = $this->Msptpd_ppj->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_ppj->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Supplier Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}
public function table()
{
   $this->load->view('Sptpd_ppj/sptpd_ppj_table');
}	
public function formula($id="")
{
    $res=$this->Msptpd_ppj->get_by_id($id);
    if($res){
        $data=array(
            'JAM_OPERASI'                 => set_value('JAM_OPERASI',number_format($res->JAM_OPERASI,'0','','.')),
            'TARIF'                       => set_value('TARIF',number_format($res->TARIF,'0','','.')),
            'DAYA_TERPASANG'              => set_value('DAYA_TERPASANG',number_format($res->DAYA_TERPASANG,'0','','.')),
            'FAKTOR_DAYA'                 => set_value('FAKTOR_DAYA',number_format($res->FAKTOR_DAYA,'2',',','.')),
            'DPP'                         => set_value('DPP',$res->DPP),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',$res->PAJAK_TERUTANG),
            'CARA_PERHITUNGAN'            => set_value('CARA_PERHITUNGAN',$res->CARA_PERHITUNGAN),
            'MASA'                        => set_value('MASA',$res->MASA),
            'PAJAK'                       => set_value('PAJAK',$res->PAJAK),
            'JUMLAH_PEMAKAIAN'            => set_value('JUMLAH_PEMAKAIAN',$res->JUMLAH_PEMAKAIAN),
            'KEGUNAAN'                    => set_value('KEGUNAAN',$res->KEGUNAAN),
            'KEPERLUAN'                   => set_value('KEPERLUAN',$res->KEPERLUAN),
            'disable'                     => '',
        );    
    }
    else {
                $data=array(
            'JAM_OPERASI'                 => set_value('JAM_OPERASI'),
            'TARIF'                       => set_value('TARIF'),
            'DAYA_TERPASANG'              => set_value('DAYA_TERPASANG'),
            'FAKTOR_DAYA'                 => set_value('FAKTOR_DAYA'),
            'DPP'                         => set_value('DPP'),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG'),
            'CARA_PERHITUNGAN'            => set_value('CARA_PERHITUNGAN'),
            'MASA'                        => set_value('MASA'),
            'PAJAK'                       => set_value('PAJAK'),
            'JUMLAH_PEMAKAIAN'            => set_value('JUMLAH_PEMAKAIAN'),
            'KEGUNAAN'                    => set_value('KEGUNAAN'),
            'KEPERLUAN'                   => set_value('KEPERLUAN'),
            'disable'                     => '',
        ); 
    }
   $this->load->view('Sptpd_ppj/formula_form',$data);
}
public function selisih($id="")
{
    $res=$this->Msptpd_ppj->get_by_id($id);
    if($res){
        $data=array(
            'TARIF'                       => set_value('TARIF',number_format($res->TARIF,'0','','.')),
            'PEMAKAIAN_BULAN_LALU'        => set_value('PEMAKAIAN_BULAN_LALU',number_format($res->PEMAKAIAN_BULAN_LALU,'0','','.')),
            'PEMAKAIAN_BULAN_INI'         => set_value('PEMAKAIAN_BULAN_INI',number_format($res->PEMAKAIAN_BULAN_INI,'0',',','.')),
            'DPP'                         => set_value('DPP',$res->DPP),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',$res->PAJAK_TERUTANG),
            'CARA_PERHITUNGAN'            => set_value('CARA_PERHITUNGAN',$res->CARA_PERHITUNGAN),
            'MASA'                        => set_value('MASA',$res->MASA),
            'PAJAK'                       => set_value('PAJAK',$res->PAJAK),
            'JUMLAH_PEMAKAIAN'            => set_value('JUMLAH_PEMAKAIAN',$res->JUMLAH_PEMAKAIAN),
            'KEGUNAAN'                    => set_value('KEGUNAAN',$res->KEGUNAAN),
            'KEPERLUAN'                   => set_value('KEPERLUAN',$res->KEPERLUAN),
            'disable'                     => '',
        );    
    }
    else {
                $data=array(
            'JAM_OPERASI'                 => set_value('JAM_OPERASI'),
            'TARIF'                       => set_value('TARIF'),
            'DAYA_TERPASANG'              => set_value('DAYA_TERPASANG'),
            'FAKTOR_DAYA'                 => set_value('FAKTOR_DAYA'),
            'DPP'                         => set_value('DPP'),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG'),
            'CARA_PERHITUNGAN'            => set_value('CARA_PERHITUNGAN'),
            'MASA'                        => set_value('MASA'),
            'PAJAK'                       => set_value('PAJAK'),
            'JUMLAH_PEMAKAIAN'            => set_value('JUMLAH_PEMAKAIAN'),
            'KEGUNAAN'                    => set_value('KEGUNAAN'),
            'KEPERLUAN'                   => set_value('KEPERLUAN'),
            'disable'                     => '',
        ); 
    }
   $this->load->view('Sptpd_ppj/selisih_form',$data);
}
public function create() 
{
    if ($this->role==8) {
            $data = array(
        'button'                       => 'Form SPTPD PPJ',
        'action'                       => site_url('Sptpd_hotel/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'tu'                           => $this->Msptpd_ppj->getTu(),

    );           
    $this->template->load('Welcome/halaman','Wp/beranda/beranda_form',$data);
    } else {
    $data = array(
        'button'                       => 'FORM SPTPD PPJ',
        'action'                       => site_url('Sptpd_ppj/preview'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_KAMAR'                 => set_value('JUMLAH_KAMAR'),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'JUMLAH_PEMAKAIAN'             => set_value('JUMLAH_PEMAKAIAN'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'CARA_PERHITUNGAN'             => set_value('CARA_PERHITUNGAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_ppj->getGol(),
        'id'                           =>'0',
        'NAMA_GOLONGAN'                => '',
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => '',
    );           
    $this->template->load('Welcome/halaman','Sptpd_ppj/sptpd_ppj_form',$data);
}
}
public function preview() 
{
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'.'.$extensi;
                        $nf='upload/file_transaksi/ppj/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
    $data = array(
        'button'                       => 'FORM SPTPD PPJ',
        'action'                       => site_url('Sptpd_ppj/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_KAMAR'                 => set_value('JUMLAH_KAMAR'),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'JUMLAH_PEMAKAIAN'             => set_value('JUMLAH_PEMAKAIAN'),
        'KELURAHAN'                    => set_value('KELURAHAN'),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'DAYA_TERPASANG'               => set_value('DAYA_TERPASANG'),
        'KEPERLUAN'                     => set_value('KEPERLUAN',$this->input->post('KEPERLUAN',TRUE)),
        'KEGUNAAN'                     => set_value('KEGUNAAN',$this->input->post('KEGUNAAN',TRUE)),
        'PEMAKAIAN_BULAN_LALU'                     => set_value('PEMAKAIAN_BULAN_LALU',$this->input->post('PEMAKAIAN_BULAN_LALU',TRUE)),
        'PEMAKAIAN_BULAN_INI'                     => set_value('PEMAKAIAN_BULAN_INI',$this->input->post('PEMAKAIAN_BULAN_INI',TRUE)),
        'JAM_OPERASI'                  => set_value('JAM_OPERASI'),
        'TARIF'                        => set_value('TARIF'),
        'FAKTOR_DAYA'                  => set_value('FAKTOR_DAYA'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$this->input->post('GOLONGAN',TRUE)),
        'CARA_PERHITUNGAN'             => set_value('CARA_PERHITUNGAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_ppj->getGol(),
        'id'                           =>'0',
        'FILE'                         => $img_name_ktp,
    );           
    $this->template->load('Welcome/halaman','sptpd_ppj/sptpd_ppj_preview',$data);
}
public function create_action() 
{
    $exp=explode("|",$this->input->post('KEPERLUAN',TRUE));
    $kg=explode("|",$this->input->post('KEGUNAAN',TRUE));
    $nu=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $m=sprintf("%02d", $this->input->post('MASA_PAJAK',TRUE));
    $BERLAKU_MULAI=date('d/'.$m.'/Y');
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $OP=$this->input->post('GOLONGAN',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $va=$this->Mpokok->getVA($OP);
    $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;

    if ($OP=='253') {$pln=1;}else{$pln=0;}
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $nu[0]);
    $this->db->set('ID_USAHA', $nu[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false);
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('CARA_PERHITUNGAN', $this->input->post('CARA_PERHITUNGAN',TRUE));

    $this->db->set('JAM_OPERASI', $this->input->post('JAM_OPERASI',TRUE));
    $this->db->set('KEGUNAAN', $kg[0]);
    $this->db->set('KEPERLUAN', $exp[0]);
    $this->db->set('TARIF',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF'))));
    $this->db->set('DAYA_TERPASANG',str_replace(',', '.', str_replace('.', '',$this->input->post('DAYA_TERPASANG'))));
    $this->db->set('FAKTOR_DAYA',str_replace(',', '.', str_replace('.', '',$this->input->post('FAKTOR_DAYA'))));

    $this->db->set('PEMAKAIAN_BULAN_LALU',str_replace(',', '.', str_replace('.', '',$this->input->post('PEMAKAIAN_BULAN_LALU'))));
    $this->db->set('PEMAKAIAN_BULAN_INI',str_replace(',', '.', str_replace('.', '',$this->input->post('PEMAKAIAN_BULAN_INI'))));

    $this->db->set('JUMLAH_PEMAKAIAN',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_PEMAKAIAN'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));

    $this->db->set('STATUS', 0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('PLN',$pln);
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('FILE_TRANSAKSI',$this->input->post('FILE',TRUE));
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
if ($this->role=='8') {
    $this->db->set('NPWP_INSERT',$this->nip);
} else {
    $this->db->set('NIP_INSERT',$this->nip);
}
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->insert('SPTPD_PPJ');
    $this->Mpokok->registration($va,$nm,$pt,$ed); 
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Tambah SPTPD PPJ", "", "success")
     });

     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpd_ppj'));
    }
    
}
public function delete($id) 
{
    $row = $this->Msptpd_ppj->get_by_id($id);

    if ($row) {
        $this->Msptpd_ppj->delete($id);
        $this->db->query("commit");
        $this->session->set_flashdata('message', '<script>
          $(window).load(function(){
             swal("Berhasil Hapus Supplier", "", "success")
         });

         </script>');
        redirect(site_url('Sptpd/sptpd_ppj'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('Sptpd/sptpd_ppj'));
    }
} 
public function update($id) 
{
    $row = $this->Msptpd_ppj->get_by_id($id);

    if ($row) {
        $data = array(
            'button'              => 'Update SPTPD PPJ',
            'action'              => site_url('Sptpd_ppj/update_action'),
            'disable'             => '',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'KEGUNAAN'            => set_value('KEGUNAAN',$row->KEGUNAAN),
            'KEPERLUAN'           => set_value('KEPERLUAN',$row->KEPERLUAN),
            'JAM_OPERASI'         => set_value('JAM_OPERASI',$row->JAM_OPERASI),
            'DAYA_TERPASANG'      => set_value('DAYA_TERPASANG',number_format($row->DAYA_TERPASANG,'0','','.')),
            'FAKTOR_DAYA'         => set_value('FAKTOR_DAYA',number_format($row->FAKTOR_DAYA,'2',',','.')),
            'CARA_PERHITUNGAN'    => set_value('CARA_PERHITUNGAN',$row->CARA_PERHITUNGAN),
            'JUMLAH_PEMAKAIAN'    => set_value('JUMLAH_PEMAKAIAN',number_format($row->JUMLAH_PEMAKAIAN,'0','','.')),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'TARIF'               => set_value('TARIF',number_format($row->TARIF,'0','','.')),
            'PEMAKAIAN_BULAN_LALU'=> set_value('PEMAKAIAN_BULAN_LALU',number_format($row->PEMAKAIAN_BULAN_LALU,'0','','.')),
            'PEMAKAIAN_BULAN_INI' => set_value('PEMAKAIAN_BULAN_INI',number_format($row->PEMAKAIAN_BULAN_INI,'0','','.')),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_ppj->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_ppj->getGol(),
            'id'                  => $id
        );
        $this->template->load('Welcome/halaman','Sptpd_ppj/sptpd_ppj_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
} 
public function update_action() 
{

    $exp=explode("|",$this->input->post('KEPERLUAN',TRUE));
    $kg=explode("|",$this->input->post('KEGUNAAN',TRUE));
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false);
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('CARA_PERHITUNGAN', $this->input->post('CARA_PERHITUNGAN',TRUE));

    $this->db->set('JAM_OPERASI', $this->input->post('JAM_OPERASI',TRUE));
    $this->db->set('KEGUNAAN', $kg[0]);
    $this->db->set('KEPERLUAN', $exp[0]);
    $this->db->set('TARIF',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF'))));
    $this->db->set('DAYA_TERPASANG',str_replace(',', '.', str_replace('.', '',$this->input->post('DAYA_TERPASANG'))));
    $this->db->set('FAKTOR_DAYA',str_replace(',', '.', str_replace('.', '',$this->input->post('FAKTOR_DAYA'))));

    $this->db->set('PEMAKAIAN_BULAN_LALU',str_replace(',', '.', str_replace('.', '',$this->input->post('PEMAKAIAN_BULAN_LALU'))));
    $this->db->set('PEMAKAIAN_BULAN_INI',str_replace(',', '.', str_replace('.', '',$this->input->post('PEMAKAIAN_BULAN_INI'))));

    $this->db->set('JUMLAH_PEMAKAIAN',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_PEMAKAIAN'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));

    $this->db->set('STATUS', 0);

    $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
    $this->db->update('SPTPD_PPJ');
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Update SPTPD PPJ", "", "success")
     });

     </script>');
    redirect(site_url('Sptpd_ppj'));
}  
public function detail($id) 
{
    $id=rapikan($id);
    $row = $this->Msptpd_ppj->get_by_id($id);

    if ($row) {
        $NAMA_GOLONGAN = $this->Mpokok->getNamaGolongan($row->ID_OP);
        $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($row->KODEKEC);
        $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($row->KODEKEL,$row->KODEKEC);
        $data = array(
            'button'              => 'Detail SPTPD PPJ',
            'action'              => site_url('Sptpd_ppj/update_action'),
            'disable'             => 'disabled',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'KEGUNAAN'            => set_value('KEGUNAAN',$row->KEGUNAAN),
            'KEPERLUAN'           => set_value('KEPERLUAN',$row->KEPERLUAN),
            'JAM_OPERASI'         => set_value('JAM_OPERASI',$row->JAM_OPERASI),
            'DAYA_TERPASANG'      => set_value('DAYA_TERPASANG',number_format($row->DAYA_TERPASANG,'0','','.')),
            'FAKTOR_DAYA'         => set_value('FAKTOR_DAYA',number_format($row->FAKTOR_DAYA,'2',',','.')),
            'CARA_PERHITUNGAN'    => set_value('CARA_PERHITUNGAN',$row->CARA_PERHITUNGAN),
            'JUMLAH_PEMAKAIAN'    => set_value('JUMLAH_PEMAKAIAN',number_format($row->JUMLAH_PEMAKAIAN,'0','','.')),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'TARIF'               => set_value('TARIF',number_format($row->TARIF,'0','','.')),
            'PEMAKAIAN_BULAN_LALU'=> set_value('PEMAKAIAN_BULAN_LALU',number_format($row->PEMAKAIAN_BULAN_LALU,'0','','.')),
            'PEMAKAIAN_BULAN_INI' => set_value('PEMAKAIAN_BULAN_INI',number_format($row->PEMAKAIAN_BULAN_INI,'0','','.')),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_ppj->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_ppj->getGol(),
            'id'                  => $id,
            'NAMA_GOLONGAN'       => $NAMA_GOLONGAN->DESKRIPSI,
            'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
            'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN,
        );
        $this->template->load('Welcome/halaman','Sptpd_ppj/sptpd_ppj_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
}
function pdf_kode_biling_ppj($kode_biling=""){
            $kode_biling=rapikan($kode_biling);
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Sptpd_ppj/sptpd_ppj_pdf',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */