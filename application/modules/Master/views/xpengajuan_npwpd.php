

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Formulir Pengajuan NPWPD</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form role="form" method="post" class="f1" enctype="multipart/form-data" action="<?php echo base_url('Master/pokok/proses_pengajuan_npwpd');?>">
                        <div class="x_title garis">
                        </div>
                         <fieldset>
                                <!-- <h4>Social media profiles:</h4> -->
                                <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>JENIS NPWP</h4> </label>
                                  <div class="col-md-8">
                                    <select name="JENIS_WP" class="form-control chosen col-md-7 col-xs-12" required>
                                      <option value="">Pilih Jenis NPWP</option>
                                      <option value="1">Perorangan</option>
                                      <option value="2">Badan </option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>NAMA USAHA/BADAN</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="NAMA_USAHA" class="form-control col-md-7 col-xs-12" placeholder="Nama Usaha">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-xs-12"> <h4>ALAMAT USAHA/BADAN</h4></label>
                                    <div class="col-md-8">
                                    <textarea name="ALAMAT_USAHA" placeholder="Alamat..." class="f1-about-yourself form-control" ></textarea>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>KECAMATAN</h4> </label>
                                  <div class="col-md-8">
                                    <select onchange="getkel1(this);"  style="font-size:12px" name="KECAMATAN_USAHA" class="form-control chosen col-md-7 col-xs-12">
                                      <option value="">Pilih</option>
                                      <?php foreach($kec1 as $keca){ ?>
                                      <option value="<?php echo $keca->KODEKEC?>"><?php echo $keca->NAMAKEC ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>KELURAHAN</h4> </label>
                                  <div class="col-md-8">
                                    <select  style="font-size:12px" id="KELURAHAN1" name="KELURAHAN_USAHA"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option>                       
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>BIDANG USAHA</h4> </label>
                                  <div class="col-md-8">
                                    <select  onchange="getgol(this);"  style="font-size:12px"  name="JENIS_USAHA"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option>
                                      <?php foreach($jenis as $jns){ ?>
                                      <option value="<?php echo $jns->ID_INC?>"><?php echo $jns->NAMA_PAJAK ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>GOLONGAN</h4> </label>
                                  <div class="col-md-8">
                                    <select  style="font-size:11px" id="GOLONGAN" name="GOLONGAN"  class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option>                                      
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </fieldset> 
                             <marquee direction="right" style="font-size:14px;color:red;">Wajib Diisi dengan Benar dan Sesuai EYD</marquee>                          
<div class="x_title garis"></div>
                             <fieldset>
                            <!-- <h4>Pendaftaran Wajib Pajak</h4><br/> -->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>NAMA PEMILIK </h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="NAMA_WP" class="form-control col-md-7 col-xs-12" placeholder="Nama Wajib Pajak">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>JABATAN</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="JABATAN" class="form-control col-md-7 col-xs-12" placeholder="JABATAN">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-xs-12"> <h4>ALAMAT</h4></label>
                                    <div class="col-md-8">
                                    <textarea name="ALAMAT_WP" placeholder="Alamat..." class="f1-about-yourself form-control" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>TELEPON</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="TELEPON" class="form-control col-md-7 col-xs-12" placeholder="Telepon">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>EMAIL</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" name="EMAIL" class="form-control col-md-7 col-xs-12" placeholder="Email">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>KECAMATAN</h4> </label>
                                  <div class="col-md-8">
                                    <select onchange="getkel(this);"  style="font-size:12px" name="KECAMATAN" class="form-control chosen col-md-7 col-xs-12">
                                      <option value="">Pilih</option>
                                      <?php foreach($kec as $kec){ ?>
                                      <option value="<?php echo $kec->KODEKEC.'|'.$kec->KODE_UPT?>"><?php echo $kec->NAMAKEC ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>KELURAHAN</h4> </label>
                                  <div class="col-md-8">
                                    <select  style="font-size:12px" id="KELURAHAN" name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option>                       
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>JENIS BUKTI DIRI</h4> </label>
                                  <div class="col-md-8">
                                    <select  style="font-size:12px" name="JENIS_BUKTI" class="form-control chosen col-md-7 col-xs-12">
                                      <option value="0">Tidak diketahui</option>
                                      <option value="1">SIM</option>
                                      <option value="2">KTP</option>
                                      <option value="3">PASPORT</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>NO BUKTI DIRI</h4> </label>
                                  <div class="col-md-8">
                                    <input max="15" type="text" required name="NO_BUKTi" class="form-control col-md-7 col-xs-12" placeholder="NO BUKTI DIRI">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>FOTO BUKTI DIRI <font color="red" style='font-size: 9px;'>*Max(2MB)</font></h4></label>
                                  <div class="col-md-8">
                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" name="file_ktp" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>FILE PENDAFTARAN <font color="red" style='font-size: 9px;'>*Max(2MB)</font></h4></label>
                                  <div class="col-md-8">
                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" id="my_file_field1" name="file_daftar" class="form-control col-md-7 col-xs-12" onchange="ab(this);">
                                  </div>
                                </div>                             
                              </div>
                            </fieldset>
                             <marquee direction="right" style="font-size:14px;color:red;">Wajib Diisi dengan Benar dan Sesuai EYD</marquee>
<div class="x_title garis"></div>
                            <!-- <fieldset>
                                <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>PASSWORD LOGIN</h4> </label>
                                  <div class="col-md-8">
                                    <input type="password"  name="PASSWORD1" class="form-control col-md-7 col-xs-12" placeholder="Password">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-xs-12"> <h4>TULIS ULANG PASSWORD LOGIN</h4></label>
                                    <div class="col-md-8">
                                      <input type="password"  id="pass2" name="PASSWORD1" class="form-control col-md-7 col-xs-12" placeholder="Password">
                                    </div>-->
                                </div>
                                 <div class="f1-buttons">
                                    <button type="" class="btn btn-success" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')" > Selesai <span class="fa fa-check"></span></button>
                                </div>
                              </div>
                              <div class="col-md-5">
                              </div>
                            </fieldset> 
                      
                      </form>
                  </div>
                </div>
              </div>
            </div>
   
<script type="text/javascript">
  function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Auth/Auth/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
</script>

<script type="text/javascript">
  function getkel1(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Auth/Auth/getkel1'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN1").html(msga);
          }
        });    
    }
    function getgol(sel)
    {
      var ID_INC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getgol'?>",
       data: { ID_INC: ID_INC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#GOLONGAN").html(msga);
          }
        });
    
    }
    function aa(sel){
      var ext = $('#my_file_field').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
          alert('Format File Tidak Didukung!');
          $('#my_file_field').val('');
      }
    }
    function ab(sel){
      var ext = $('#my_file_field1').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
          alert('Format File Tidak Didukung!');
          $('#my_file_field1').val('');
      }
    }
    var uploadField = document.getElementById("my_file_field");

uploadField.onchange = function() {
    if(this.files[0].size > 2500000){
       alert("File KTP  Terlalu Besar!");
       this.value = "";
    };
};
   var uploadField = document.getElementById("my_file_field1");

uploadField.onchange = function() {
    if(this.files[0].size > 2500000){
       alert("File Pendaftaran  Terlalu Besar!");
       this.value = "";
    };
};
</script>
<style type="text/css">
  .garis{
    border-bottom: 3px solid #436ED5;
  }
  .col-md-55, .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{
    padding-bottom: 7px;
  }
  .x_content h4 {
    font-size: 13.5px;
    font-weight: bold;
   } 
</style>
