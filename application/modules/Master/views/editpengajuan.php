

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Data Pengajuan Wajib Pajak</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form role="form" method="post" enctype="multipart/form-data" class="f1" action="<?php echo base_url('Master/pokok/edit_wp_temp_action');?>">
                        <input type='hidden' value='<?= $ID_INC_WP_TEMP?>' name='ID_INC_WP_TEMP'>
                             <fieldset>
                            <!-- <h4>Pendaftaran Wajib Pajak</h4><br/> -->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>JENIS NPWP</h4> </label>
                                  <div class="col-md-8">
                                    <select name="JENISWP" class="form-control chosen col-md-7 col-xs-12">>
                                      <option value="">Pilih Jenis NPWP</option>
                                      <option <?php if ($JENISWP==1) {echo "selected";} ?> value="1">Perorangan</option>
                                      <option <?php if ($JENISWP==2) {echo "selected";} ?> value="2">Badan </option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>NPWP </h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="NPWP" class="form-control col-md-7 col-xs-12" readonly value="<?= $NPWP?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>NAMA </h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="NAMA" class="form-control col-md-7 col-xs-12" value="<?= $NAMA?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>JABATAN</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="JABATAN" class="form-control col-md-7 col-xs-12" value="<?= $JABATAN?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-xs-12"> <h4>ALAMAT</h4></label>
                                    <div class="col-md-8">
                                    <textarea name="ALAMAT_WP" placeholder="Alamat..." class="f1-about-yourself form-control" ><?= $ALAMAT?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>TELEPON</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" required name="TELEPON" class="form-control col-md-7 col-xs-12" value="<?= $NO_TELP?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>EMAIL</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" name="EMAIL" class="form-control col-md-7 col-xs-12" value="<?= $EMAIL?>">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>KECAMATAN</h4> </label>
                                  <div class="col-md-8">
                                    <select onchange="getkel(this);"  style="font-size:12px" name="KECAMATAN" class="form-control chosen col-md-7 col-xs-12">
                                      <option value="">Pilih</option>
                                      <?php foreach($kec as $kec){ ?>
                                      <option <?php if ($KECAMATAN==$kec->KODEKEC) {echo "selected";} ?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>KELURAHAN</h4> </label>
                                  <div class="col-md-8">
                                    <select  style="font-size:12px" id="KELURAHAN" name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option> 
                                      <?php foreach($kel as $kel){ ?>
                                      <option <?php if ($KECAMATAN==$kel->KODEKEC) {echo "selected";} ?> value="<?php echo $kel->KODEKEC?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                                      <?php } ?>                      
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>JENIS BUKTI DIRI</h4> </label>
                                  <div class="col-md-8">
                                    <select  style="font-size:12px" name="JENIS_BUKTI" class="form-control chosen col-md-7 col-xs-12">
                                      <option <?php if ($BUKTIDIRI==0) {echo "selected";} ?> value="0">Tidak diketahui</option>
                                      <option <?php if ($BUKTIDIRI==1) {echo "selected";} ?> value="1">SIM</option>
                                      <option <?php if ($BUKTIDIRI==2) {echo "selected";} ?> value="2">KTP</option>
                                      <option <?php if ($BUKTIDIRI==3) {echo "selected";} ?> value="3">PASPORT</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>NO BUKTI DIRI</h4> </label>
                                  <div class="col-md-8">
                                    <input max="15" type="text" required name="NO_BUKTi" class="form-control col-md-7 col-xs-12" value="<?= $NO_BUKTI_DIRI?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>FOTO BUKTI DIRI</h4> </label>
                                  <div class="col-md-8">
                                    <input type="file" name="file_ktp" class="form-control col-md-7 col-xs-12" value='aaaaaaa'>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>FILE PENDAFTARAN</h4> </label>
                                  <div class="col-md-8">
                                    <input type="file" name="file_daftar" class="form-control col-md-7 col-xs-12">
                                  </div>
                                </div>                             
                              </div>
                            </fieldset>
<div class="x_title garis"></div>
                                </div>
                                 <div class="f1-buttons">
                                    <button type="" class="btn btn-success" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')" > Selesai <span class="fa fa-check"></span></button>
                                </div>
                              </div>
                              <div class="col-md-5">
                              </div>
                            </fieldset> 
                      
                      </form>
                  </div>
                </div>
              </div>
            </div>
   
<script type="text/javascript">
  function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Auth/Auth/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
</script>

<script type="text/javascript">
  function getkel1(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Auth/Auth/getkel1'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN1").html(msga);
          }
        });    
    }
    function getgol(sel)
    {
      var ID_INC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getgol'?>",
       data: { ID_INC: ID_INC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#GOLONGAN").html(msga);
          }
        });
    
    }
</script>
<style type="text/css">
  .garis{
    border-bottom: 3px solid #436ED5;
  }
  .col-md-55, .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{
    padding-bottom: 7px;
  }
  .x_content h4 {
    font-size: 13.5px;
    font-weight: bold;
   } 
</style>
