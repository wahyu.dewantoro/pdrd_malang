   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<?php echo $this->session->flashdata('message')?><br>
<div class="page-title">
 <div class="title_left">
  <h3>Laporan Perforasi Karcis</h3>
</div>
<div class="  pull-right">
        <?php echo anchor(site_url('Master/pokok/tambah_perforasi'), '<i class="fa fa-plus"></i> Tambah Perforasi Baru', 'class="btn btn-primary btn-sm"'); 
            ?>
      </div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Master/pokok/perforasi'?>">
                <div class="form-group">
                    <input type="text" name="tgl1"  required="required" class="form-control col-md-3 col-xs-12 tanggal" value="<?php if ($tgl1==null) {echo $def1;} else {echo $tgl1;}?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2"  required="required" class="form-control col-md-3 col-xs-12 tanggal" value="<?php if ($tgl2==null) {echo $def2;} else {echo $tgl2;}?>" >
                </div>
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control col-md-8 col-xs-12" value="<?php echo $this->session->userdata('npwpd'); ?>" placeholder="NPWPD/NAMA WP/USAHA">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Master/pokok/perforasi'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th width="5%">Tgl Perforasi</th>
              <th>Nomor Perforasi</th>
              <th>NPWPD</th>
              <th>Nama Pemohon</th>
              <th>Nama Tempat Usaha</th>
         <!--      <th>No Perf</th>
              <th>Jenis Karcis dan No urut</th>
              <th>No Seri</th>
              <th>Nominal</th>
              <th>Jml Blok</th>
              <th>Isi/Blok</th>
              <th>jml Lembar</th>
              <th>jml Pendapatan</th> -->
              
              <th>Detail</th>
              <th>Import</th>
              <th>Cetak</th>

            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($perforasi as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td align="center"><?= $rk->TGL?></td>
                <td><?= $rk->NO_PERMOHONAN?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td align="center"><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk->ID_INC?>"><i class="fa fa-search"></i> </a></td>
                <td align="center"><a href="<?php echo base_url('Master/Pokok/import/'.$rk->ID_INC);?>"><span class="fa fa-file"></span></a></td>
                <td align="center"><a href="<?php echo base_url('Pdf/Pdf/bap_perforasi/'.$rk->ID_INC);?>">[BAP]</a>
                    <a href="<?php echo base_url('Pdf/Pdf/permohonan_perforasi/'.$rk->ID_INC);?>">[PERMOHONAN]</a></td>
                <td align="center"><a href="<?php echo base_url('Master/pokok/hapus_perforasi/'.$rk->ID_INC);?>" onclick="return confirm('Pastikan Data Perforasi belum pernah dilaporkan');"><i class="fa fa-trash"></i>
                </td>
                <td><a href="<?php echo base_url('Master/pokok/edit_perforasi/'.$rk->ID_INC)?>"><i class="fa fa-edit"></i> </a></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  th{
    text-align: center;
  }
</style>
              <?php foreach ($perforasi as $rk)  { ?>
                        <div class="modal fade" id="myModal<?= $rk->ID_INC?>" role="dialog">
                          <div class="modal-dialog"> 
                            <?php $ID=$rk->ID_INC;?>   
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">DETAIL LAPORAN PERFORASI </h4>
                              </div>
                              <div class="modal-body">
                               <div class="table-responsive">
                              <table tyle="width: 100%;" class="table table-striped table-bordered">
                                <thead>
                                  <tr>
                                    <th class="text-center" width="3%">No</th>
                                    <!-- <th class="text-center">Masa Pajak</th>
                                    <th class="text-center">Tahun Pajak</th> -->
                                    <th>Jenis Karcis</th>
                                    <th>No urut</th>
                                    <th>No Seri</th>
                                    <th>Hrg / Lembar</th>
                                    <th>Jml Blok</th>
                                    <th>Isi/Blok</th>
                                    <th>jml Lembar</th>
                                    <th>Pajak %</th>
                                    <th>Nilai Pajak</th>
                                    <th>Catatan</th>
                                  </tr>
                                </thead>
                                  <tbody>
                                  <?php $no=1;$tot=0;$data1=$this->db->query("SELECT * FROM perforasi where no_permohonan='$ID' ")->result(); 
                                  foreach ($data1 as $rk_d)  { ?>                                  
                                    <tr>
                                      <td align="center"><?= $no?></td>
                                      <td align="center"><?= $rk_d->JENIS_KARCIS?></td>
                                      <td align="center"><?= $rk_d->NO_URUT_KARCIS?></td>
                                      <td align="center"><?= $rk_d->NOMOR_SERI?></td>
                                      <td align="right"><?=  number_format($rk_d->NILAI_LEMBAR,'0','','.')?></td>
                                      <td align="right"><?=  number_format($rk_d->JML_BLOK,'0','','.')?></td>
                                      <td align="right"><?=  number_format($rk_d->ISI_LEMBAR,'0','','.')?></td>
                                      <td align="right"><?=  number_format($QTT=$rk_d->JML_BLOK*$rk_d->ISI_LEMBAR,'0','','.')?></td>
                                      <td align="right"><?=  number_format($rk_d->PERSEN,'0','','.')?></td>
                                      <td align="right"><?=  number_format($QTT*$rk_d->NILAI_LEMBAR*$rk_d->PERSEN/100,'0','','.')?></td>
                                      <td align="center"><?= $rk_d->CATATAN?></td>
                                    </tr>
                                    <?php  $no++;} ?>
                                  </tbody>
                                </table>
                              </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                        <?php }?>
<style type="text/css">
  th{
    text-align: center;
  }
  @media (min-width: 768px) {
  .modal-dialog {
    width: 750px;
    margin: 30px auto;
  }
  
</style>