

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Upload File Transaksi Pajak Self</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form role="form" method="post" class="f1" enctype="multipart/form-data" action="<?php echo base_url('Master/pokok/proses_upload_file');?>">
                        <input type='hidden' value='<?php echo $pajak?>' name='pajak'>
                         <input type='hidden' value='<?php echo $data->KODE_BILING?>' name='kode_biling'>
                             <fieldset>
                            <!-- <h4>Pendaftaran Wajib Pajak</h4><br/> -->
                              <div class="col-md-6">
                                <table class="table table-user-information table-hover">
                                  <tbody>
                                    <tr>
                                      <td>KODE BILING</td>
                                      <td>: <?php echo $data->KODE_BILING; ?></td>
                                    </tr>
                                    <tr>
                                      <td>NPWPD</td>
                                      <td>: <?php echo $data->NPWPD?></td>
                                    </tr>
                                    <tr>
                                      <td>Nama WP</td>
                                      <td>: <?php echo $data->NAMA_WP; ?></td>
                                    </tr>
                                    <tr>
                                      <td>NAMA USAHA</td>
                                      <td>: <?php echo $data->NAMA_USAHA; ?></td>
                                    </tr>
                                    <tr>
                                      <td>PAJAK TERUTANG</td>
                                      <td>: Rp. <?php echo number_format($data->PAJAK_TERUTANG,'0','','.'); ?></td>
                                    </tr>
                                    <tr>
                                      <td>FILE TRANSAKSI</td>
                                      <td>: <a href="<?php echo base_url($lokasi).$data->FILE_TRANSAKSI ?>" target="_blank"><?php echo $data->FILE_TRANSAKSI; ?></a></td>
                                    </tr>                                   
                                  </tbody>
                                </table>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                                    <div class="col-md-8">
                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                                    </div>
                                </div>
                                
                              </div>
                              <div class="col-md-6">
                                
                              </div>
                            </fieldset>
<div class="x_title garis"></div>
                                </div>
                                 <div class="f1-buttons">
                                    <button type="" class="btn btn-success"> Simpan <span class="fa fa-check"></span></button>
                                </div>
                              </div>
                              <div class="col-md-5">
                              </div>
                            </fieldset> 
                      
                      </form>
                  </div>
                </div>
              </div>
            </div>
   
<script type="text/javascript">
  function aa(sel){
      var ext = $('#my_file_field').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
          alert('Format File Tidak Didukung!');
          $('#my_file_field').val('');
      }
    }
    var uploadField = document.getElementById("my_file_field");

uploadField.onchange = function() {
    if(this.files[0].size > 2500000){
       alert("File Terlalu Besar!");
       this.value = "";
    };
};
</script>
<style type="text/css">
  .garis{
    border-bottom: 3px solid #436ED5;
  }
  .col-md-55, .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{
    padding-bottom: 7px;
  }
  .x_content h4 {
    font-size: 13.5px;
    font-weight: bold;
   } 
</style>
