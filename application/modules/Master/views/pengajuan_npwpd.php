   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
<div class="  pull-right">
        <?php echo anchor(site_url('Master/pokok/tambah_pengajuan_npwpd'), '<i class="fa fa-plus"></i> Tambah Pengajuan NPWP Baru', 'class="btn btn-primary btn-sm"'); 
            ?>
      </div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');$session_user=$this->session->userdata('NIP');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Master/pokok/pengajuan_npwpd'?>">
               <div class="form-group">
                    <input type="text" name="tgl1" placeholder="Tanggal 1" required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php echo $tgl1?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2" placeholder="S/d Tanggal 2" required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php echo $tgl2;?>" >
                </div>
                <div class="form-group">
                      <select name="sts"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Status</option>
                            <option <?php if ($sts=='1') {echo "selected";} ?> value="1">Diterima</option>
                            <option <?php if ($sts=='2') {echo "selected";} ?> value="2">Ditolak</option>
                            <option <?php if ($sts=='0') {echo "selected";} ?> value="0">Menunggu</option>
                      </select>
                </div>
                 <div class="form-group">
                      <select name="jwp"  class="form-control select2 col-md-7 col-xs-12" >
                            <option value="">Jenis Wajib Pajak</option>
                            <option value="1" <?php if ($jwp=='1') {echo "selected";} ?>>Perseorangan</option>
                            <option value="2" <?php if ($jwp=='2') {echo "selected";} ?>>Badan</option>
                              
                      </select>
                </div>
                <div class="form-group">
                    <input type="text" name="nama"  class="form-control col-md-6 col-xs-12" value="" placeholder="NAMA/TEMPAT USAHA">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Master/pokok/pengajuan_npwpd'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_rekap_pengajuan'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">JENIS</th>
              <th class="text-center">NAMA WP</th>
              <th class="text-center">JABATAN</th>
              <th class="text-center">ALAMAT</th>
              <th class="text-center">HP</th>
              <th class="text-center">NAMA USAHA</th>
              <th class="text-center">KTP</th>
              <th class="text-center">FORMULIR</th>
              <th class="text-center">TGL DAFTAR</th>
              <th class="text-center">UPT</th> 
              <th class="text-center">STATUS</th>                            
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($pengajuan as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?php if ($rk->NPWP=='') {echo "Pengajuan";} else {echo $rk->NPWP;}?></td>
                <td><?php if ($rk->JENISWP=='1') {
                  echo "Perorangan";
                } else if ($rk->JENISWP=='2') {
                  echo "Badan";
                } else {
                    
                }?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->JABATAN?></td>
                <td><?= $rk->ALAMAT?></td>
                <td><?= $rk->NO_TELP?></td>
                <td><?= $rk->NAMA_BADAN?></td>
                <td><a href="<?php echo base_url('upload/ktp').'/'.$rk->FILE_BUKTI_DIRI ?>" target="_blank"><?= $rk->FILE_BUKTI_DIRI?></a></td>
                <td><a href="<?php echo base_url('upload/form_daftar').'/'.$rk->FILE_FORMULIR ?>" target="_blank"><?= $rk->FILE_FORMULIR?></a></td>
                <td><?= $rk->TGL_DAFTAR?></td>
                <td><?= $rk->NAMA_UNIT?></td>
                <td><?php if ($rk->STATUS==0) {echo "Pengajuan";} else if ($rk->STATUS==2) {echo "Ditolak";} else {echo "Disetujui";}?></td>
                
                <td>
                <?php if ($this->session->userdata('MS_ROLE_ID')==10 OR $this->session->userdata('MS_ROLE_ID')==64) {?>
                   <?php if ($rk->STATUS==1) {
                        # code...
                      } else {?>
                          <a href="<?php echo base_url('Master/pokok/edit_wp_temp/'.$rk->ID_INC_WP_TEMP);?>" class='btn btn-xs btn-info'><span class='fa fa-edit'></span></a>
                      <?php }?>
                <?php    
                } else {?>
                      <?php if ($rk->STATUS==0) {?>
                        <?php if ($session_user=='dedy') {?>
                            <a href="<?php echo base_url('Master/pokok/terima_pengajuan/'.$rk->ID_INC_WP_TEMP);?>" class='btn btn-xs btn-success' onclick="return confirm('Apakah anda yakin menerima pengajuan dari <?= $rk->NAMA?>');"><span class='fa fa-'></span>Terima</a>
                            <a href="<?php echo base_url('Master/pokok/tolak_pengajuan/'.$rk->ID_INC_WP_TEMP);?>" class='btn btn-xs btn-warning' onclick="return confirm('Apakah anda yakin Menolak pengajuan dari <?= $rk->NAMA?>');"><span class='fa fa-'></span>Tolak</a>
                          <?php
                        } else {
                          # code...
                        } ?>
                        <a href="<?php echo base_url('Master/pokok/hapus_pengajuan/'.$rk->ID_INC_WP_TEMP);?>" class='btn btn-xs btn-danger' onclick="return confirm('Apakah anda yakin Menghapus pengajuan dari <?= $rk->NAMA?>');"><span class='fa fa-trash'></span></a>
                      <?php
                      } else {?>
                          <!-- <a href="<?php echo base_url('Master/pokok/terima_pengajuan/'.$rk->ID_INC_WP_TEMP);?>" class='btn btn-xs btn-success' onclick="return confirm('Apakah anda yakin menerima pengajuan dari <?= $rk->NAMA?>');"><span class='fa fa-'></span>Terima</a> -->
                      <?php }?>
                  
                  <?php } ?>
                      <a href="<?php echo base_url('Master/pokok/detail_objek_pajak_temp/'.$rk->ID_INC_WP_TEMP);?>" class='btn btn-xs btn-info'><span class='fa fa-'></span>Detail</a>
                </td>
               
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          </button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
          <marquee direction="right" style="font-size:14px;color:red;">Pengajuan setelah diterima, Form NPWPD baru wajib diserahkan ke dinas maksimal 5 hari kerja </marquee>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

