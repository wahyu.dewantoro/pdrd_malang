   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Pengajuan Pembatalan</h3>
</div>

<div class="pull-right">
  <?php echo anchor('Master/pokok/create_pembatalan','<i class="fa fa-plus"></i> Buat Pengajuan Pembatalan','class="btn btn-sm btn-primary"')?>
</div>

</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Master/pokok/pembatalan_kode_biling'?>">
                <div class="form-group">
                    <input type="text" name="param"  class="form-control1" value="<?php echo $this->session->userdata('param'); ?>" placeholder="KODE BILNG / NPWPD / NAMA WP / USAHA">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($param <> '')  { ?>
                                    <a href="<?php echo site_url('Master/pokok/pembatalan_kode_biling'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center"  width="3%">No</th>
              <th class="text-center" >Kode Biling</th>
              <th class="text-center" >Tgl Pengajuan</th>
              <th class="text-center" >NPWPD</th>
              <th class="text-center" >Nama</th>
              <th class="text-center" >Alamat</th>
              <th class="text-center" >Objek Pajak</th>
              <th class="text-center" >Pajak Terutang</th>
              <th class="text-center" >jenis pajak</th>
              <th class="text-center" >User</th>
              <th class="text-center" >UPT</th>
              <th class="text-center" >Alasan</th>
              <th class="text-center" >Status</th>
              <th class="text-center" >Tgl Batal</th>
              <?php if ($this->session->userdata('MS_ROLE_ID')==9) {
               echo "<th class='text-center' >Aksi</th>";
              } else { 

              }?>
            </tr>
          </thead>
          <tbody>
              <?php if($total_rows>0){  foreach ($manage_kobil as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td align="center"><?= $rk->KODE_BILING?></td>
                <td><?= $rk->TGL_PENGAJUAN?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td align="right"><?=  number_format($rk->PAJAK_TERUTANG,'0','','.')?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->NAMA_UNIT?></td>
                <td><?= $rk->ALASAN?></td>
                <td><?php if ($rk->STATUS_BATAL==0) {
                 echo "Menunggu";
                } else if ($rk->STATUS_BATAL==1) {
                  echo "Disetujui";
                } else {
                  echo "Ditolak";
                }?></td>
                <td><?= $rk->TGL_AKSI_BATAL?></td>
                <?php if ($this->session->userdata('MS_ROLE_ID')==9) {
                  if ($rk->STATUS_BATAL==1) {
                    echo "<td></td>";
                  } else {?>
                    <td><a href="<?php echo base_url('Master/pokok/aprove_persetujuan/'.$rk->KODE_BILING.'/'.$rk->JENIS_PAJAK.'/'.$link);?>" onclick="return confirm('konfirmasi Pembatalan kode biling dari <?= $rk->NAMA_WP?>');">Setujui</a></td>
                  <?php }
               
                } else { 

                }?>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
        </table>
        </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
           <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
.form-control1 {
    display: block;
    width:400px;
    height: 28px;
    padding: 3px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;}
</style>



