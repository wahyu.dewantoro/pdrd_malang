
      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
    <div class="page-title">
     <div class="title_left">
      <h3>Tambah Perforasi</h3>
    </div>
    <div class="pull-right">
      <a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>  
  <div class="clearfix"></div>   
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php  echo base_url('Master/pokok/create_action_perforasi');?>" method="post" enctype="multipart/form-data" >        
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_content" >
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">  
                    </div> 
                  </div>
                </div>
              </div> 

            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Sesuai dengan data pengajuan</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >         
                <div class="col-md-12">

                  <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12">Nomor BAP <sup>*</sup> </label>
                                  <div class="col-md-2">
                                    <input type="text" required name="kode1" class="form-control col-md-2 col-xs-12 ok" maxLength="3" placeholder="tanpa 0 didepan ">
                                  </div>
                                  <div class="col-md-1">
                                    <input type="text" required name="kode2" class="form-control col-md-2 col-xs-12 ok" maxLength="3" placeholder="tanpa 0 didepan">
                                  </div>
                                  <div class="col-md-2">
                                    <input type="text" required name="kode3" class="form-control col-md-2 col-xs-12 ok"  placeholder="Contoh XII /2019">
                                  </div>
                                </div>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > NPWPD<sup>*</sup>
                      </label>
                      <div class="col-md-3 col-sm-5 col-xs-12">
                        <input type="text"  id="npwp" name="NPWPD" required="required" class="form-control col-md-7 col-xs-12" onchange="NamaUsaha(this.value);">               
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama WP<sup>*</sup>
                      </label>
                      <div class="col-md-3 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_WP" name="NAMA_WP" required="required" class="form-control col-md-7 col-xs-12" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Jenis Karcis<sup>*</sup>
                      </label>
                      <div class="col-md-3 col-sm-6 col-xs-12">
                          <select style="font-size:11px" id="JK" name="JENIS_KARCIS"  placeholder="Kecamatan" onchange="jenis(this.value)" class="form-control select2 col-md-7 ">
                          <option value="">Pilih Jenis Karcis</option>
                          <option value="1">Karcis Masuk</option>
                          <option value="2">Karcis Parkir</option>
                          <option value="3">Karcis Warung</option>
                      </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha<sup>*</sup>
                      </label>
                      <div class="col-md-3 col-sm-6 col-xs-12" id="TU">
                         
                      </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Tanggal<sup>*</sup>
                      </label>
                      <div class="col-md-3 col-sm-6 col-xs-12" >
                         <input type="text" name="tgl"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="" >
                      </select>
                      </div>
                  </div>
                  <table class="table table-striped table-bordered" border="1" with='100%'>
                    <thead>
                      <tr>
                       <!--  <th width="100">Jenis Karcis</th>
                        <th width="100">Tempat Usaha</th>  -->         
                        <th width="50">KODE</th>
                        <th  colspan="2"  width="15%">No Urut</th>
                        <th width="70">Jml Blok</th>
                        <th width="70">Jml lmbr/Blok</th>
                        <th width="100">Nilai /Lmbr</th>
                        <th width="50">Pajak%</th>
                        <th width="100">Nilai Pajak</th>
                        <th width="130">Catatan</th>
                        <th width="10">Aksi</th>
                      </tr>
                    </thead>
                    <tbody id="kontentdiag">
                    <tr>
                        <!-- <td><select style="font-size:11px" id="JK" name="JENIS_KARCIS[]"  placeholder="Kecamatan" onchange="jenis(this.value)" class="form-control select2 col-md-7 ">
                          <option value="">Pilih Jenis Karcis</option>
                          <option value="1">Karcis Hiburan</option>
                          <option value="2">Karcis Parkir</option>
                      </select></td>
                        <td ></td> -->
                        <td><input type="text" id="No_SERI" name="No_SERI[]" required placeholder="Kode" class="form-control col-md-7"></td>
                        <td><input type="text" id="NO_URUT" name="NO_URUT1[]" class="form-control col-md-1"  placeholder="0001">
                        </td>
                        <td><input type="text" id="NO_URUT" name="NO_URUT2[]" class="form-control col-md-1"  placeholder="1000">
                        </td>
                        <td><input onchange="get(this.value)" type="text" id="JUMLAH_BLOK" name="JUMLAH_BLOK[]" required placeholder="Jml Blok" class="form-control col-md-7" ></td>
                        <td><input type="text" onchange="get(this.value)" name="LBR_BLOK[]" id="LBR_BLOK" class="form-control col-md-7"  placeholder=""></td>
                        <td><input type="text" onchange="get(this.value)" id="NILAI" name="NILAI[]" required class="form-control col-md-7"></td>
                        <td><input type="text" onchange="get(this.value)" id="PAJAK" name="PAJAK[]" required class="form-control col-md-7"></td>
                        <td><input type="text" id="JML" readonly required placeholder="Jumlah" class="form-control col-md-7"></td>
                        <td><input type="text" id="CATATAN" name="CATATAN[]" required="required" placeholder="Catatan" class="form-control col-md-7 col-xs-12" ></td>
                        
                        <td><span class="fa fa-trash"></span></td>
                      </tr>
                    </tbody>
                </table>
                <a class="btn btn-xs btn-success" id="tambahdiag"><i class="fa fa-plus"></i> Tambah</a>
                <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">DPP <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="DPP1" id="DPP1" readonly required class="col-md-7 col-xs-12 form-control" >
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Pajak Terutang <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PAJAK_TERUTANG1" onchange='get(this);' id="PENERIMAAN" required class="col-md-7 col-xs-12 form-control" value="" <?php ?> >
                        </div>
                      </div>
                    </div> -->
                    
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                        <a href="<?php echo site_url('Master/pokok/perforasi') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                      </div>
                    </div>
                
                </div>
              </div>
            </div>     
          </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div></div></div>
    <script>
    function alamat(sel)
    {
      var nama = sel;
      //alert(nama);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_alamat_usaha'?>",
       data: { nama: nama},
       cache: false,
       success: function(msga){
            //alert(msga);
             $("#ALAMAT_USAHA").val(msga); 
          }
        });    
    }
      function get(sel) {        
        var jml_blok= parseInt(document.getElementById("JUMLAH_BLOK").value.replace(/\./g,''))|| 0;
        var lbr_blok= parseInt(document.getElementById("LBR_BLOK").value.replace(/\./g,''))|| 0;
        var nilai= parseInt(document.getElementById("NILAI").value.replace(/\./g,''))|| 0;
        var pajak= parseInt(document.getElementById("PAJAK").value.replace(/\./g,''))|| 0;
        var qtt=jml_blok*lbr_blok;
        var sum=jml_blok*lbr_blok*nilai;
        var pjk=sum*pajak/100;
        JML.value= getNominal(pjk);
       // alert(pajak);
        //CATATAN.value=getNominal(qtt*nilai)
     /*   var dpp=hasil*harga;
        var pjk=dpp*pajak;
        DPP.value = getNominal(dpp);
        PAJAK_TERUTANG.value = getNominal(dpp*pajak);
        var P=pjk.toFixed(0);
        JML.value= getNominal(dpp);
        PENERIMAAN.value= getNominal(P);*/
       // alert(nilai);
      }
//JUMLAH_BLOK 
var JUMLAH_BLOK = document.getElementById('JUMLAH_BLOK'),
numberPattern = /^[0-9]+$/;

JUMLAH_BLOK.addEventListener('keyup', function(e) {
  JUMLAH_BLOK.value = formatRupiah(JUMLAH_BLOK.value);
});
//LBR_BLOK 
var LBR_BLOK = document.getElementById('LBR_BLOK'),
numberPattern = /^[0-9]+$/;

LBR_BLOK.addEventListener('keyup', function(e) {
  LBR_BLOK.value = formatRupiah(LBR_BLOK.value);
});
//NILAI 
var NILAI = document.getElementById('NILAI'),
numberPattern = /^[0-9]+$/;
var PAJAK = document.getElementById('PAJAK'),
numberPattern = /^[0-9]+$/;

NILAI.addEventListener('keyup', function(e) {
  NILAI.value = formatRupiah(NILAI.value);
});
 /* DPP */

    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
  var x=1;
  $("#tambahdiag").click(function(){
    x++;
                $.get("karcis/"+x, function (data) {
                    $("#kontentdiag").append(data);
                });
 });

    $('#kontentdiag').on('click', '.remove_project_file', function(e) {
      x--;
    e.preventDefault();
    $(this).parents(".barisdiag").remove();
  });

    function NamaUsaha(id){
                //alert(id);
                
                var npwpd=id;
                var jp='01';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);
                $('select option').each(function(){$(this).removeAttr('selected');}); 
                
              }else{
                alert('NPWPD Tidak ditemukan');
                $("#NAMA_WP").val(null);   
              }
              
            }
          });                
    } 
    function jenis(id){
        var x = document.getElementById("npwp").value;
    if (x=='') { alert('Masukan NPWP Dulu');} else{
        //if (id==1) {;
        //document.getElementById("demo").innerHTML = x;
                  //alert(x);
          $.ajax({
            url: "<?php echo base_url().'Master/pokok/getUsaha';?>",
            type: 'POST',
            data: "x="+x,
            cache: false,
            success: function(msg){
              if (msg=='') {
                  alert('Tempat Usaha Belum terdaftar');
                //$('select option').each(function(){$(this).removeAttr('selected');}); 
              } else{
                  $("#TU").html(msg);
                }
              }
              });
      //}
                 /*$("#TU").html('');else if (id==2){
        $("#TU").html('Parkir');
      }else {
        $("#TU").html('');
      }*/
    };
  }
</script>


