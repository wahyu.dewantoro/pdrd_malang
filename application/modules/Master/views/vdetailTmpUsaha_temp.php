<div class="page-title">
     <div class="title_left">
        <h3>Detail Pengajuan Objek Pajak</h3>
      </div>
      <div class="pull-right">
      <?php echo anchor('Master/pokok/pengajuan_npwpd','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?>
    </div>
</div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="col-md-6"> 
                      <table class="table borderless" style="border: none !important">
                       <tbody>                 
                         <tr>
                           <td width="30"><b>NPWPD</b></td>
                           <td width="5"><b>:</b></td>
                           <td><b>
                              <?php if ($wp->STATUS==0) {echo "Pengajuan";} else if ($wp->STATUS==2) {echo "Ditolak";} else {echo $wp->NPWP;}?>
                         </tr>
                         <tr>
                           <td><b> Nama Pemilik</b></td>
                           <td><b>:</b></td>
                           <td><b> <?php echo $wp->NAMA;?></b></td>
                         </tr>
                         <tr>
                           <td width="30"><b>Jabatan</b></td>
                           <td width="5"><b>:</b></td>
                           <td><b> <?php echo $wp->JABATAN;?></b></td>
                         </tr>  
                         <tr>
                           <td><b>Alamat</b></td>
                           <td><b>:</b></td>
                           <td><b> <?php echo $wp->ALAMAT; ?></b></td>
                         </tr> 
                         <tr>
                           <td width="30"><b>Kecamatan</b></td>
                           <td width="5"><b>:</b></td>
                           <td><b> <?php echo $wp->NAMAKEC;?></b></td>
                         </tr>
                         <tr>
                           <td><b> Kelurahan</b></td>
                           <td><b>:</b></td>
                           <td><b> <?php echo $wp->NAMAKELURAHAN;?></b></td>
                         </tr>   
                       </tbody>
                      </table>
                    </div>
                    <div class="col-md-6"> 
                      <table class="table borderless" style="border: none !important">
                       <tbody>                 
                         <tr>
                           <td><b>Hp</b></td>
                           <td><b>:</b></td>
                           <td><b> <?php echo $wp->NO_TELP; ?></b></td>
                         </tr>
                         <tr>
                           <td><b>Nomor NIK/SIM</b></td>
                           <td><b>:</b></td>
                           <td><b> <?php echo $wp->NO_BUKTI_DIRI; ?></b></td>
                         </tr>
                         <tr>
                           <td><b>Email</b></td>
                           <td><b>:</b></td>
                           <td><b> <?php echo $wp->EMAIL; ?></b></td>
                         </tr>
                         <tr>
                           <td><b>Tgl Daftar</b></td>
                           <td><b>:</b></td>
                           <td><b> <?php echo $wp->TGL_DAFTAR; ?></b></td>
                         </tr>  
                       </tbody>
                      </table>
                    </div>
                      <div class="x_content">
                        <table id="example2" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th width="5%">No</th>
                              <th>Jenis Pajak</th>
                              <th>Nama Usaha</th>
                              <th>Alamat</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php 
                              if (count($detail_objek_pajak)<=0) {
                                 echo "<tr ><td colspan='4'>belum ada objek pajak</td></tr>";
                               } else {
                                $no=1; foreach($detail_objek_pajak as $rk){?>
                           <tr>
                               <td align="center"><?php echo $no?></td>
                               <td><?php echo $rk->NAMA_PAJAK?></td>
                               <td><?php echo $rk->NAMA_USAHA?></td>
                               <td><?php echo $rk->ALAMAT_USAHA?></td> 
                               <?php if ($this->session->userdata('MS_ROLE_ID')==10) {
                               } else {
                                ?>
                                  <td><a href="<?php echo base_url('Pdf/Pdf/Pdf_pengukuhan_baru/'.$rk->ID_INC);?>">[PENGUKUHAN]</a></td>                           
                                <?php }?>                           
                           </tr>
                           <?php $no++; } }?>
                       </tbody>
                        </table>
                      </div>
                </div>
              </div>
            </div>
