      <div class="page-title">
           <div class="title_left">
                <h3><?php echo $button?></h3>
              </div>
      </div>


            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
              
                  <div class="x_content">
                      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                      <input type="hidden" name="ID_INC" value="<?php echo $ID_INC?>">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">TAHUN PAJAK</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                                  <?php $thnskg = date('Y');
                                  for($i=$thnskg; $i<=$thnskg+2; $i++){ ?>
                                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?>
                            </select>
                        </div>
                      </div> 

                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >UPT
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text"  name="REKENING" value="<?= $REKENING?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">JENIS PAJAK
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text"  name="JENIS_PAJAK" value="<?= $JENIS_PAJAK ?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>                      
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">REKENING
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text"  name="REKENING" value="<?= $REKENING ?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">TARGET
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text"  name="TARGET" value="<?= $TARGET ?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>          
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a href="<?php echo site_url('Master/pokok/mstarget_bapenda') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>



            </div>

