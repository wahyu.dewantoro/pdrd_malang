   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<?php echo $this->session->flashdata('message')?><br>
<div class="page-title">
 <div class="title_left">
  <h3>Edit Perforasi Karcis</h3>
</div>
<div class="pull-right">
  <a href="<?php echo(base_url('Master/pokok/perforasi'))?>" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
</div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <table tyle="width: 100%;" class="table table-striped table-bordered">
                                <thead>
                                  <tr>
                                    <th class="text-center" width="3%">No</th>
                                    <!-- <th class="text-center">Masa Pajak</th>
                                    <th class="text-center">Tahun Pajak</th> -->
                                    <th>Jenis Karcis</th>
                                    <th  width="8%">No urut</th>
                                    <th width="8%">No Seri</th>
                                    <th>Hrg / Lembar</th>
                                    <th>Jml Blok</th>
                                    <th>Isi/Blok</th>
                                    <th>jml Lembar</th>
                                    <th>Pajak %</th>
                                    <th>Nilai Pajak</th>
                                    <th>Catatan</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                  <tbody>
                                  <?php $no=1;$tot=0; foreach ($edit_perforasi as $rk_d)  { ?>                                  
                                    <tr>
                                      <td align="center"><?= $no?></td>
                                      <td align="center"><?= $rk_d->JENIS_KARCIS?></td>
                                      <td align=""><?= $rk_d->NO_URUT_KARCIS?></td>
                                      <td align=""><?= $rk_d->NOMOR_SERI?></td>
                                      <td align="right"><?=  number_format($rk_d->NILAI_LEMBAR,'0','','.')?></td>
                                      <td align="right"><?=  number_format($rk_d->JML_BLOK,'0','','.')?></td>
                                      <td align="right"><?=  number_format($rk_d->ISI_LEMBAR,'0','','.')?></td>
                                      <td align="right"><?=  number_format($QTT=$rk_d->JML_BLOK*$rk_d->ISI_LEMBAR,'0','','.')?></td>
                                      <td align="right"><?=  number_format($rk_d->PERSEN,'0','','.')?></td>
                                      <td align="right"><?=  number_format($QTT*$rk_d->NILAI_LEMBAR*$rk_d->PERSEN/100,'0','','.')?></td>
                                      <td align="center"><?= $rk_d->CATATAN?></td>
                                      <td><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk_d->ID_INC?>">Ubah</i> </a></td>
                                    </tr>

                                     <div class="modal fade" id="myModal<?= $rk_d->ID_INC?>" role="dialog">
                                          <div class="modal-dialog"> 
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Edit Perforasi </h4>
                                              </div>
                                              <form class="form-inline"  method="post" action="<?php echo base_url('Master/pokok/Proses_Edit_Perforasi');?>" enctype="multipart/form-data" >
                                                <input  type="hidden" name="id_inc" required value="<?= $rk_d->ID_INC?>" >
                                                <input  type="hidden" name="no_permohonan" required value="<?= $rk_d->NO_PERMOHONAN?>" >
                                              <div class="modal-body">
                                                <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >No Urut Karcis <sup>*</sup>
                                                  </label>
                                                  <div class="col-md-9 col-sm-7 col-xs-12">
                                                    <input type="text" name="no_urut" required="required" class="form-control col-md-7 col-xs-12" value="<?= $rk_d->NO_URUT_KARCIS?>" >                
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah blok <sup>*</sup>
                                                  </label>
                                                  <div class="col-md-9 col-sm-7 col-xs-12">
                                                    <input type="text" name="jml_blok" required="required" class="form-control col-md-7 col-xs-12" value="<?= $rk_d->JML_BLOK?>" >                
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Per Blok <sup>*</sup>
                                                  </label>
                                                  <div class="col-md-9 col-sm-7 col-xs-12">
                                                    <input type="text" name="jml_isi_perblok" onkeyup="formatangka(this)" required="required" class="form-control col-md-7 col-xs-12" value="<?= $rk_d->ISI_LEMBAR?>" >                
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Harga <sup>*</sup>
                                                  </label>
                                                  <div class="col-md-9 col-sm-7 col-xs-12">
                                                    <input type="text" name="harga" onkeyup="formatangka(this)" required="required" class="form-control col-md-7 col-xs-12" value="<?= $rk_d->NILAI_LEMBAR?>" >                
                                                  </div>
                                                  <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >NO Seri <sup>*</sup>
                                                  </label>
                                                  <div class="col-md-9 col-sm-7 col-xs-12">
                                                    <input type="text" name="seri" required="required" class="form-control col-md-7 col-xs-12" value="<?= $rk_d->NOMOR_SERI?>" >                
                                                  </div>
                                                </div>                                
                                              </div>
                                              <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Catatan <sup>*</sup>
                                                  </label>
                                                  <div class="col-md-9 col-sm-7 col-xs-12">
                                                    <input type="text" name="catatan" required="required" class="form-control col-md-7 col-xs-12" value="<?= $rk_d->CATATAN?>" >                
                                                  </div>
                                                </div>
                                              <div class="modal-footer">
                                                <button type="submit" class="btn btn-success" >Simpan</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </div>
                                            </form>
                                            </div>
                            
                          </div>
                        </div>
                                    <?php  $no++;} ?>
                                  </tbody>
                                </table>
          <div class="float-right">
            
          </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  th{
    text-align: center;
  }
</style>
<style type="text/css">
  th{
    text-align: center;
  }
  @media (min-width: 768px) {
  .modal-dialog {
    width: 750px;
    margin: 30px auto;
  }
  
</style>
<script type="text/javascript">
  function formatangka(objek) {
   a = objek.value;
   b = a.replace(/[^\d]/g,"");
   c = "";
   panjang = b.length;
   j = 0;
   for (i = panjang; i > 0; i--) {
     j = j + 1;
     if (((j % 3) == 1) && (j != 1)) {
       c = b.substr(i-1,1) + "." + c;
     } else {
       c = b.substr(i-1,1) + c;
     }
   }
   objek.value = c;
}
</script>