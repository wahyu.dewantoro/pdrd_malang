<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpokok extends CI_Model {
    public $table = 'TARGET';
    public $id = 'ID_INC';
	 function jsonwp() {
        $this->datatables->select('NPWP,NAMA,ALAMAT_USAHA AS ALAMAT,JENIS,WP_ID,NAMA_USAHA,NAMA_PAJAK');
        $this->datatables->from("V_WAJIB_PAJAK");
        if ($this->session->userdata('INC_USER')=='311') {
            $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Master/pokok/edit_wp/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('Master/pokok/detail_objek_pajak/$1'),'Detail','class="btn btn-xs btn-info"  ').anchor(site_url('Master/pokok/hapus_wp/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger"').'</div>', "acak(NPWP)");
        } else {
            $this->datatables->add_column('action', '<div class="btn-group">'/*.anchor(site_url('Master/pokok/edit_wp/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"')*/.anchor(site_url('Master/pokok/detail_objek_pajak/$1'),'Detail','class="btn btn-xs btn-info"  ').'</div>', "acak(NPWP)");
        }
        return $this->datatables->generate();
    }

    function jsonwp_prioritas() {
        $this->datatables->select('NPWP,NAMA,ALAMAT_USAHA AS ALAMAT,JENIS,WP_ID,NAMA_USAHA,NAMA_PAJAK');
        $this->datatables->from("V_WAJIB_PAJAK_PRIORITAS");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Master/pokok/detail_objek_pajak/$1'),'Detail','class="btn btn-xs btn-info"  ').'</div>', "acak(NPWP)");
        return $this->datatables->generate();
    }

     function jsonwp_user_prioritas() {
        $this->datatables->select('NPWP,NAMA,JENIS,KET_MENU,WP_ID,STATUS_MENU,STATUS_WP');
        $this->datatables->from("V_USER_PRIORITAS");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Master/pokok/detail_objek_pajak/$1'),'Detail','class="btn btn-xs btn-info"  ').'</div>', "acak(NPWP)");
        return $this->datatables->generate();
    }

    function jsonformulir() {
        $this->datatables->select('NPWPD, NAMA_WP, NAMA_TEMPAT_USAHA, EMAIL_TEMPAT_USAHA, KODE_AKTIVASI, TGL_AKTIVASI, ID_TEMPAT_USAHA, ID_DATA');
        $this->datatables->from("V_MOBILE_FORMULIR");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Master/pokok/aktivasi_formulir/$1'),'Aktivasi','class="btn btn-xs btn-success"  ').'</div>', "ID_DATA");
        return $this->datatables->generate();
    }

    function jsonJenisPajak(){
    	$this->datatables->select('ID_INC,NAMA_PAJAK');
    	$this->datatables->from('JENIS_PAJAK');
    	return $this->datatables->generate();
    }

    function jsonJenisHotel(){
    	$this->datatables->select('ID_INC,HOTEL');
    	$this->datatables->from('JENIS_HOTEL');
    	return $this->datatables->generate();	
    }

    function jsonJenisRestoran(){
    	$this->datatables->select('ID_INC,RESTORAN');
    	$this->datatables->from('JENIS_RESTORAN');
    	return $this->datatables->generate();		
    }

    function jsonJenisMineral(){
    	$this->datatables->select('ID_INC,MINERAL');
    	$this->datatables->from('JENIS_MINERAL');
    	return $this->datatables->generate();			
    }

    function listMasapajak(){
        $data=array('1','2','3','4','5','6','7','8','9','10','11','12');
        return $data;
    }
     function getKec(){
        return $this->db->query("SELECT * FROM KECAMATAN WHERE kodekec !='99'")->result();
     }
     function getBank(){
        return $this->db->query("SELECT * FROM MS_BANK")->result();
     }
     function getJabatan($jabatan=""){
        return $this->db->query("SELECT * FROM PEGAWAI WHERE JABATAN='$jabatan'")->row();
     }
    function getIP(){
        $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
    
        return $ipaddress;
    }

    function json_target() {
        //$this->datatables->select("ID_INC,TAHUN_PAJAK,REKENING,JENIS_PAJAK,TARGET");
        $this->datatables->select("ID_INC,TAHUN_PAJAK,UPT,JENIS_PAJAK,TARGET,NAMA_UNIT,NAMA_PAJAK");
        $this->datatables->from("V_MASTER_TARGET_WASMIN");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Master/pokok/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"  ').'</div>', 'acak(ID_INC)');
        return $this->datatables->generate();
    }

    function json_target_bapenda() {
        //$this->datatables->select("ID_INC,TAHUN_PAJAK,REKENING,JENIS_PAJAK,TARGET");
        $this->datatables->select("ID_INC,TAHUN_TARGET,JENIS_PAJAK,TARGET,NAMA_PAJAK,REKENING");
        $this->datatables->from("V_MASTER_TARGET_BAPENDA");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Master/pokok/update_target_bapenda/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"  ').'</div>', 'acak(ID_INC)');
        return $this->datatables->generate();
    }
 
    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    /*function get_by_id_target_bapenda($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }*/
    
  

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
    function getFormulir(){        
        $data=$this->db->query("select max(nomor_formulir)+1 next_val from ms_formulir")->row();
        $FORMULIR = sprintf("%07d", $data->NEXT_VAL);
        return $FORMULIR;
     }
     function getSqIdBiling(){        
        $data=$this->db->query("SELECT S_KODE_BILING.nextval  from dual")->row();
        $kohir = sprintf("%05d", $data->NEXTVAL);
        return $kohir;
     }
      function getSqIdVirtual(){        
        $data=$this->db->query("SELECT LPAD(S_VIRTUAL_ACCOUNT.nextval,6,'0')NEXTVAL  from dual")->row();
        return $data->NEXTVAL;
     }
     function getNoBerkas(){        
        $data=$this->db->query("SELECT S_NO_BERKAS.nextval  from dual")->row();
        $berkas = sprintf("%04d", $data->NEXTVAL);
        return $berkas;
     }
     function getnoformulirwp(){        
        $data=$this->db->query("SELECT S_WAJIB_PAJAK.nextval  from dual")->row();
        $berkas = sprintf("%07d", $data->NEXTVAL);
        return $berkas;
     }

     public function getNamaGolongan($id)
     {
          return $this->db->query("select deskripsi from objek_pajak where id_op = $id")->row();
     }
     public function getVA($OP)
     {
          $data= $this->db->query("select get_norek_va('$OP')nomor_va from dual")->row();
         return $nomor_va=$data->NOMOR_VA;
     }
     public function getNamaKec($id)
     {
          return $this->db->query("select NAMAKEC from KECAMATAN where KODEKEC = $id")->row();
     } 
     public function getNamaKel($kel,$kec)
     {
          return $this->db->query("select namakelurahan from kelurahan where kodekelurahan=$kel and kodekec=$kec")->row();
     } 
     function getKel($id=""){
        return $this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$id'")->result();
     } 

     function total_rows_perforasi($tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
        if ($npwpd!='') {
            $this->db->where("LOWER(NAMA) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%'",null,false);
        }
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->from('NOMOR_PERFORASI A');
        $this->db->join('WAJIB_PAJAK B','A.NPWPD=B.NPWP');
        return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_perforasi($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
        if ($npwpd!='') {
        $this->db->where("LOWER(NAMA_WP_PERF) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%'",null,false);
        }
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->join('WAJIB_PAJAK B','A.NPWPD=B.NPWP');
        $this->db->order_by('A.TGL_PERFORASI', 'DESC');
        $this->db->limit($limit, $start);
        $this->db->select("ID_INC,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,NPWPD,NAMA_WP_PERF NAMA,NOMOR_INC_BLN||' / '||LPAD(NOMOR_INC_THN, 3, '0')||' / '||NOMOR_OBJEK||' / '||NOMOR_BLN_THN NO_PERMOHONAN,NAMA_USAHA");
        //$this->db->select("ID_INC,JENIS_KARCIS,NOMOR_SERI,NILAI_LEMBAR,JML_BLOK,ISI_LEMBAR,PERSEN,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,NPWPD,CATATAN,NAMA,NO_PERMOHONAN");
            return $this->db->get('NOMOR_PERFORASI A')->result();
    }  
    function get_det_wp($npwp){
        return $this->db->query("SELECT NPWP,NAMA,ALAMAT,JABATAN ,email,tgl_daftar,buktidiri,NO_BUKTI_DIRI,NO_TELP,kecamatan,kelurahan,namakec,namakelurahan
                                        FROM WAJIB_PAJAK a
                                        left join kecamatan d on a.kecamatan=D.KODEKEC
                                        left join kelurahan e on a.kelurahan=e.KODEKELURAHAN and A.kecamatan=E.KODEKEC WHERE NPWP='$npwp'")->row();
    } 

    function getNoIncBln(){
        $bln=date('n');        
        $data=$this->db->query("SELECT GetIncNoBlnPerforasi('$bln') inc_bulan from dual")->row();
        return $data->INC_BULAN;
     }
     function getNoIncThn(){        
        $thn=date('Y');        
        $data=$this->db->query("SELECT GetIncNoThnPerforasi('$thn') inc_tahun from dual")->row();
        return $data->INC_TAHUN;
     }
     function getNoObjek($id=""){        
        $data=$this->db->query("SELECT regexp_replace(SUBSTR('$id', 0,13), '[[:space:]]*','') NO_OBJEK FROM dual")->row();
        return $data->NO_OBJEK;
     }
     function getNoRomawi(){        
        $data=$this->db->query("SELECT to_char(sysdate, 'RM/YYYY') NO_ROMAWI FROM dual")->row();
        return $data->NO_ROMAWI;
     }
     function getCatatan(){        
        $data=$this->db->query("select * from ms_catatan where keperluan='kode biling'")->row();
        return $data->CATATAN;
     }

     function total_rows_pengajuan_npwpd($tgl1 = NULL,$tgl2 = NULL,$sts=NULL,$cek=NULL,$jwp=NULL) {
                $this->db->from('V_PENGAJUAN_NPWPD');
                if ($tgl1!=NULL){
                   $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
                   $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
                }
                if ($sts!=NULL){
                    $this->db->where("STATUS","$sts");
                }
                if ($cek!=NULL){
                    $this->db->like("NAMA","%$cek%");
                    $this->db->like("NAMA_BADAN","%$cek%");
                }
                 if ($this->session->userdata('MS_ROLE_ID')==10) {
                    $this->db->where("UPT", $this->session->userdata('UNIT_UPT_ID'));
                }
                if ($this->session->userdata('MS_ROLE_ID')==64) {
                    $this->db->where("UPT", $this->session->userdata('UNIT_UPT_ID'));
                }  
                if ($jwp!=NULL){
                    $this->db->where("JENISWP","$jwp");
                }              
                /*if ($this->upt==2) {
                        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
                    } else {
                         if ($kecamatan==NULL) {
                            $this->db->where("KODE_UPT",$this->upt);            
                        }else {
                            $this->db->where("KODEKEC",$kecamatan);
                        }
                    }*/
                //$this->db->where('TAHUN', $tahun);
                //$this->db->where('BULAN', $bulan);
    /*$this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($this->upt==2) {
        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
    } else {
         if ($kecamatan==NULL) {
            $this->db->where("KODE_UPT",$this->upt);            
        }else {
            $this->db->where("KODEKEC",$kecamatan);
        }
    }
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}*/
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_pengajuan_npwpd($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$sts=NULL ,$nama=NULL,$jwp=NULL) {

    $this->db->select("*");
    if ($tgl1!=NULL){
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
    }
    if ($sts!=NULL){
        $this->db->where("STATUS","$sts");
    }
    if ($nama!=NULL){
        $this->db->like("NAMA","%$nama%");
        $this->db->or_like("NAMA_BADAN","%$nama%");
    }
     if ($this->session->userdata('MS_ROLE_ID')==10) {
        $this->db->where("UPT", $this->session->userdata('UNIT_UPT_ID'));
    }
    if ($this->session->userdata('MS_ROLE_ID')==64) {
                    $this->db->where("UPT", $this->session->userdata('UNIT_UPT_ID'));
                }
    if ($jwp!=NULL){
                    $this->db->where("JENISWP","$jwp");
                }
    //$this->db->where('TAHUN', $tahun);
    //$this->db->where('BULAN', $bulan);
 /*   if ($this->upt==10) {
       
    } else {
        $this->db->where("NPWPD",$npwpd); 
    }*/
   /* if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}*/
    $this->db->order_by('ID_INC_WP_TEMP', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('V_PENGAJUAN_NPWPD')->result();
    }
    function total_rows_manage_kobil(/*$tahun = NULL,$bulan = NULL,*/$param = NULL) {
        $this->db->select("*"); 
        if ($this->session->userdata('MS_ROLE_ID')==10) {
            $this->db->where("UPT", $this->session->userdata('UNIT_UPT_ID'));
            /*if ($param!=null) {
            $this->db->where("STATUS ='0' AND (KODE_BILING LIKE '%$param%' OR LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(NAMA_USAHA) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
            }*/
        }
        if ($param!=null) {
            $this->db->where("STATUS ='0' AND (KODE_BILING LIKE '%$param%' OR LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(NAMA_USAHA) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
        }
        $this->db->from("V_GET_PEMBATALAN_TAGIHAN");
        return $this->db->count_all_results();
        }

    function get_limit_data_manage_kobil($limit, $start = 0, /*$tahun = NULL,$bulan = NULL,*/$param = NULL) {        
        $this->db->select("*");
        if ($this->session->userdata('MS_ROLE_ID')==10) {
            $this->db->where("UPT", $this->session->userdata('UNIT_UPT_ID'));
            /*if ($param!=null) {
            $this->db->where("STATUS ='0' AND (KODE_BILING LIKE '%$param%' OR LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(NAMA_USAHA) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
            }*/
        }
        if ($param!=null) {
            $this->db->where("STATUS ='0' AND (KODE_BILING LIKE '%$param%' OR LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(NAMA_USAHA) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
        }
        //$this->db->order_by('TGL_PENGAJUAN', 'DESC');
        $this->db->order_by('ID_INC', 'DESC');
        $this->db->limit($limit, $start);
        return $this->db->get('V_GET_PEMBATALAN_TAGIHAN')->result();
    }

    function registration($va='',$nama='',$total='',$exp=''){
      $data = array(
            'VirtualAccount'      => $va,
            'Nama' => $nama,
            'TotalTagihan'    => $total,
            'TanggalExp'    => $exp,
            'Berita1'    => '-',
            'Berita2'    => '-',
            'Berita3'    => '-',
            'Berita4'    => '-',
            'Berita5'    => '-',
            'FlagProses' => 1
        );

        $data_string = json_encode($data);
        //$curl = curl_init('http://apps.bankjatim.co.id/Api/Registrasi');
        $curl = curl_init('http://182.23.52.28/Va/Reg');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data

        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using  
        //echo $data_string;
        $v_va = $data["VirtualAccount"];
        $this->db->query("call P_INSERT_LOG('$v_va','$data_string','registrasi1')");
        $content = $data_string;
        $nm_f=$va;
        $filename = $nm_f.".txt"; //the name of our file.
        $strlength = strlen($content); //gets the length of our $content string.
        $create = fopen('log_va/registrasi/'.$filename, "w");
            if( $create == false ){
                //do debugging or logging here
            }else{
                fwrite($create,$content);
                fclose($create);
            }
        curl_close($curl);

       

 /*$data = array(
            'patient_id'      => $username,
            'department_name' => 'ok',
            'patient_type'    => 'b'
    );
    $data_string = json_encode($data);
    $url = 'http://36.89.91.154:8080/pdrd/pembayaran/webservice_ori/posting'; 
    $this->curl->simple_post($url, $data_string, array(CURLOPT_BUFFERSIZE => 10));*/
  }
  function registration_tiga($va='',$nama='',$total='',$exp=''){
      $data = array(
            'VirtualAccount'      => $va,
            'Nama' => $nama,
            'TotalTagihan'    => $total,
            'TanggalExp'    => $exp,
            'Berita1'    => '-',
            'Berita2'    => '-',
            'Berita3'    => '-',
            'Berita4'    => '-',
            'Berita5'    => '-',
            'FlagProses' => 3
        );

        $data_string = json_encode($data);
        $curl = curl_init('http://182.23.52.28/Va/Reg');
       // $curl = curl_init('http://182.23.52.28/Va/Reg');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data

        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using  
        //echo $data_string;
        $v_va = $data["VirtualAccount"];
        $this->db->query("call P_INSERT_LOG('$v_va','$data_string','registrasi1')");
        $content = $data_string;
        $nm_f=$va;
        $filename = $nm_f.".txt"; //the name of our file.
        $strlength = strlen($content); //gets the length of our $content string.
        $create = fopen('log_va/flag3/'.$filename, "w");
            if( $create == false ){
                //do debugging or logging here
            }else{
                fwrite($create,$content);
                fclose($create);
            }
        curl_close($curl);
  }

  function total_rows_manage_log(/*$tahun = NULL,$bulan = NULL,*/$param = NULL) {
        $this->db->select("*"); 
        if ($param!=null) {
            $this->db->where("NO_VA LIKE '%$param%'",null,false);
        }else{
            $this->db->where("NO_VA LIKE ''");
        }
        $this->db->from("LOG");
        return $this->db->count_all_results();
        }

    function get_limit_data_manage_log($limit, $start = 0, /*$tahun = NULL,$bulan = NULL,*/$param = NULL) {        
        $this->db->select("*");
        
        if ($param!=null) {
            $this->db->where("NO_VA LIKE '%$param%'",null,false);
        }else{
            $this->db->where("NO_VA LIKE ''");
        } 
        $this->db->order_by('ID_INC DESC');
        $this->db->limit($limit, $start);
        return $this->db->get('LOG')->result();
    }
    

}

   