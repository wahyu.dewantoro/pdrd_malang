<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pokok extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Mpokok');
        $this->load->model('Sptpd_hotel/Msptpd_hotel');
        $this->load->model('Sptpd_restoran/Msptpd_restoran');
        $this->load->model('Sptpdhiburan/Mhiburan');
        //$this->load->model('Sptpdreklame/Mreklame');
        $this->load->model('Sptpd_ppj/Msptpd_ppj');
        //$this->load->model('Sptpd_mblbm/Msptpd_mblbm');
        //$this->load->model('Sptpd_parkir/Msptpd_parkir');
        $this->load->model('Sptpdair/Mair');
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
 }
 
	public function wajibPajak(){
		// $this->load->view('Auth/formlogin2');
		$this->template->load('Welcome/halaman','vwajibPajak');
	}
	
	 function jsonwp() {
        header('Content-Type: application/json');
        echo $this->Mpokok->jsonwp();
    }

    public function wp_prioritas(){
        // $this->load->view('Auth/formlogin2');
        $this->template->load('Welcome/halaman','vwajibPajak_prioritas');
    }
    
     function jsonwp_prioritas() {
        header('Content-Type: application/json');
        echo $this->Mpokok->jsonwp_prioritas();
    }

    public function user_prioritas(){
        // $this->load->view('Auth/formlogin2');
        $this->template->load('Welcome/halaman','v_user_prioritas');
    }
    
     function jsonwp_user_prioritas() {
        header('Content-Type: application/json');
        echo $this->Mpokok->jsonwp_user_prioritas();
    }

    function jsonformulir() {
        header('Content-Type: application/json');
        echo $this->Mpokok->jsonformulir();
    }

    function jsonJenisPajak(){
    	header('Content-Type: application/json');
        echo $this->Mpokok->jsonJenisPajak();	
    }

    function jenisPajak(){
		$this->template->load('Welcome/halaman','vjenisPajak');	
    }

    function jenisHotel(){
    	$this->template->load('Welcome/halaman','vjenisHotel');
    }

    function jsonJenisHotel(){
    	header('Content-Type: application/json');
        echo $this->Mpokok->jsonJenisHotel();	
    }

    function jsonJenisRestoran(){
    	header('Content-Type: application/json');
        echo $this->Mpokok->jsonJenisRestoran();		
    }

    function jenisRestoran(){
    	$this->template->load('Welcome/halaman','vjenisRestoran');
    }

    function jenisMineral(){
    	$this->template->load('Welcome/halaman','vjenisMineral');
    }

    function jsonJenisMineral(){
    	header('Content-Type: application/json');
        echo $this->Mpokok->jsonJenisMineral();		
    }

    function ceknpwpd(){
        // echo $_POST['npwpd'];
        $var=$_POST['npwpd'];
        $res=$this->db->query("SELECT NPWP,NAMA,ALAMAT 
                                FROM WAJIB_PAJAK 
                                where  NPWP ='$var'")->row();
        // echo $res;
        if($res){
            echo $res->NAMA.'|'.$res->ALAMAT;
        }else{
            echo 0;
        }
    } 
    function cek_ktp(){
        // echo $_POST['npwpd'];
        $var=$_POST['npwpd'];
        $res=$this->db->query("SELECT NPWP,NAMA,ALAMAT 
                                FROM WAJIB_PAJAK 
                                where  NO_BUKTI_DIRI ='$var'")->row();
        // echo $res;
        if($res){
            echo 'Nomor Identitas Sudah Terdaftar dengan Nomor NPWPD '.$res->NPWP;
        }else{
            echo 0;
        }
    }  
    function cektu(){
        // echo $_POST['npwpd'];
        $var=$_POST['id'];
        $res=$this->db->query("SELECT ID_INC, NAMA_USAHA, ALAMAT_USAHA, JENIS_PAJAK,KODEKEC,KODEKEL FROM TEMPAT_USAHA 
WHERE ID_INC='$var'")->row();
        // echo $res;
        if($res){
            echo $res->NAMA_USAHA.'|'.$res->ALAMAT_USAHA.'|'.$res->KODEKEC.'|'.$res->KODEKEL;
        }else{
            echo 0;
        }
    }

    function cektu1(){
        // echo $_POST['npwpd'];
        $var=$_POST['id'];
        $res=$this->db->query("SELECT NAMA_USAHA||'|'||ID_INC NAMA_USAHA, ALAMAT_USAHA, JENIS_PAJAK,KODEKEC,KODEKEL FROM TEMPAT_USAHA 
WHERE ID_INC='$var'")->row();
        // echo $res;
        if($res){
            echo $res->NAMA_USAHA.'"'.$res->ALAMAT_USAHA.'"'.$res->KODEKEC.'"'.$res->KODEKEL;
        }else{
            echo 0;
        }
    } 

    function non_aktif_menu_mobile($id=""){
        $this->db->query("UPDATE MS_PENGGUNA SET STATUS_MENU='0' WHERE NIP='$id'");
        redirect(site_url('Master/pokok/user_prioritas'));
    }

    function aktif_menu_mobile($id=""){
        $this->db->query("UPDATE MS_PENGGUNA SET STATUS_MENU='1' WHERE NIP='$id'");
        redirect(site_url('Master/pokok/user_prioritas'));
    }

    function non_aktif_wp_prioritas($id=""){
        $this->db->query("UPDATE WAJIB_PAJAK SET STATUS_WP='1' WHERE NPWP='$id'");
        redirect(site_url('Master/pokok/user_prioritas'));
    }
    function getkel(){
        $KODEKEC=$_POST['KODEKEC'];
        $data['kc']=$this->db->query("SELECT KODEKELURAHAN,NAMAKELURAHAN FROM KELURAHAN WHERE KODEKEC ='$KODEKEC' order by NAMAKELURAHAN asc")->result();
        $this->load->view('Master/getkel',$data);
    } 
    function getgol(){
        $ID_INC=$_POST['ID_INC'];
        //HOTEL
        if ($ID_INC=='01') {

        $data['gol']=$this->Msptpd_hotel->getGol();
        }
        //RESTORAN
        if ($ID_INC=='02') {

        $data['gol']=$this->Msptpd_restoran->getGol();;
        }
        //HIBURAN
        if ($ID_INC=='03') {

        $data['gol']=$this->Mhiburan->getGol();
        }
        //PPJ
        if ($ID_INC=='05') {

        $data['gol']=$this->Msptpd_ppj->getGol();
        }
        //Parkir
        if ($ID_INC=='07') {

        $data['gol']=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM OBJEK_PAJAK WHERE ID_OP=456 ")->result();
        }
        if ($ID_INC=='08') {

        $data['gol']=$this->Mair->Gol();
        }
        if ($ID_INC=='09') {

        $data['gol']=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM OBJEK_PAJAK WHERE ID_OP=459")->result();
        }
        $this->load->view('Master/getgol',$data);
    } 
    
    function detail_objek_pajak($id=""){
        $id=rapikan($id);
        $data['detail_objek_pajak']=$this->db->query("select b.*,nama_pajak from tempat_usaha b
                                join JENIS_PAJAK a on a.id_inc=b.jenis_pajak 
                                where npwpd='$id'
                                order by a.id_inc")->result();
        $data['wp']=$this->db->query("SELECT NPWP,NAMA,ALAMAT,JABATAN ,email,tgl_daftar,buktidiri,NO_BUKTI_DIRI,NO_TELP,kecamatan,kelurahan,namakec,namakelurahan
                                        FROM WAJIB_PAJAK a
                                        left join kecamatan d on a.kecamatan=D.KODEKEC
                                        left join kelurahan e on a.kelurahan=e.KODEKELURAHAN and A.kecamatan=E.KODEKEC WHERE NPWP='$id'")->row();
        $this->template->load('Welcome/halaman','vdetailTmpUsaha',$data);
    }

    function hapus_wp($id=""){
        $id=rapikan($id);
        $data['detail_objek_pajak']=$this->db->query("select * from tagihan a 
            join jenis_pajak b on a.jenis_pajak=b.id_inc WHERE npwpd='$id'")->result();
        $data['wp']=$this->db->query("SELECT NPWP,NAMA,ALAMAT,JABATAN ,WP_ID,email,tgl_daftar,buktidiri,NO_BUKTI_DIRI,NO_TELP,kecamatan,kelurahan,namakec,namakelurahan
                                        FROM WAJIB_PAJAK a
                                        left join kecamatan d on a.kecamatan=D.KODEKEC
                                        left join kelurahan e on a.kelurahan=e.KODEKELURAHAN and A.kecamatan=E.KODEKEC WHERE NPWP='$id'")->row();
        $this->template->load('Welcome/halaman','vdetail_hapus_wp',$data);
    }

    function aksi_hapus_wp($id=""){
       $this->db->query("update wajib_pajak set status_wp_hapus='1',date_hapus=sysdate where wp_id='$id'");
        
        redirect('master/pokok/wajibPajak');
    }

    ///////////////////////////////////////////////////////////////////////

    public function mstarget()
    {
        $this->template->load('Welcome/halaman','target_list');
    } 
    
    public function json_target() {
        header('Content-Type: application/json');
        echo $this->Mpokok->json_target();
    }
    public function create() 
    {
        $data = array(
            'button'     => 'Tambah Data Target',
            'action'     => site_url('Master/pokok/create_action'),
            //'lparent'    => $this->Mmenu->ListParent(),
            'ID_INC'     => set_value('ID_INC'),
            'TAHUN_PAJAK'=> set_value('TAHUN_PAJAK'),
            'REKENING'   => set_value('REKENING'),
            'JENIS_PAJAK'=> set_value('JENIS_PAJAK'),
            'TARGET'     => set_value('TARGET'),
    );
        $this->template->load('Welcome/halaman','target_form',$data);
    }
    
    public function create_action() 
    {       
            // $data['ID_INC']=$this->input->post('ID_INC');
            $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
            //$data['REKENING']=$this->input->post('REKENING');
            $data['JENIS_PAJAK']=$this->input->post('JENIS_PAJAK');
            $data['TARGET']=$this->input->post('TARGET');
            $this->Mpokok->insert($data); 
            redirect(site_url('Master/pokok/Mstarget'));
 
    }
    
    public function update($id) 
    {
        $id=rapikan($id);
        $row = $this->Mpokok->get_by_id($id);

        if ($row) {
            $data = array(
                'button'      => 'Edit Data',
                'action'      => site_url('Master/pokok/update_action'),
                //'lparent'    =>$this->Mmenu->ListParent(),
                'ID_INC'    => set_value('ID_INC',$row->ID_INC),
                'TAHUN_PAJAK' => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
                //'REKENING' => set_value('REKENING',$row->REKENING),
                'JENIS_PAJAK'    => set_value('JENIS_PAJAK',$row->JENIS_PAJAK),
                'TARGET'    => set_value('TARGET',$row->TARGET),
        );
            $this->template->load('Welcome/halaman','target_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
             redirect(site_url('Master/pokok/Mstarget'));
        }
    }
    
    public function update_action() 
    {
        $data['ID_INC']=$this->input->post('ID_INC');
        $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
        $data['REKENING']=$this->input->post('REKENING');
        $data['JENIS_PAJAK']=$this->input->post('JENIS_PAJAK');
        $data['TARGET']=$this->input->post('TARGET');
        $this->Mpokok->update($this->input->post('ID_INC', TRUE), $data);
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('Master/pokok/Mstarget'));
    }
    
    public function delete($id) 
    {
        $row = $this->Mmenu->get_by_id($id);

        if ($row) {
            $this->Mmenu->delete($id);
            $this->session->set_flashdata('message', ' <button type="button" class="btn btn-danger"> '.$row->nama_menu.' Berhasil Dihapus</button>');
            redirect(site_url('Setting/Mstarget'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Setting/Mstarget'));
        }
    }
    function getrek(){
        $ID_INC=$_POST['ID_INC'];
        $data['rek']=$this->db->query("SELECT REKENING,JENIS_PAJAK,KODE_REKENING_PAJAK,KETERANGAN FROM MS_REKENING WHERE JENIS_PAJAK='$ID_INC'")->result();
        $this->load->view('Master/getrek',$data);
    }
    public function Daftar(){
        // $this->load->view('Auth/formlogin2');
        $data['kec']=$this->db->query("select * from kecamatan where kodekec !='99'")->result();
        $data['kec1']=$this->db->query("select * from kecamatan where kodekec !='99'")->result();
        $data['jenis']=$this->db->query("select * from JENIS_PAJAK order by ID_INC")->result();
        $this->template->load('Welcome/halaman','vdaftar',$data);
    }
    public function daftar_manual(){
        // $this->load->view('Auth/formlogin2');
        $data['kec']=$this->db->query("select * from kecamatan where kodekec !='99'")->result();
        $data['kec1']=$this->db->query("select * from kecamatan where kodekec !='01' AND kodekec !='99'")->result();
        $data['jenis']=$this->db->query("select * from JENIS_PAJAK order by ID_INC")->result();
        $this->template->load('Welcome/halaman','vdaftarmanual',$data);
    }
    function Proses_pendaftaran(){
            $this->db->trans_start();
            $id_wp=$this->Mpokok->getnoformulirwp();
            
            // $wp_id=$this->db->query("select max(no_daftar)no from wajib_pajak")->row();
            
            $ex= explode('|', $this->input->post('KECAMATAN',TRUE));
            $jwp=$this->input->post('JENIS_WP',TRUE);
            $kec=sprintf("%04d", $ex[0]);
           
            $npwp=$jwp.$id_wp.$kec;
                $tmp_file=$_FILES['file_ktp']['tmp_name'];
                $name_file =$_FILES['file_ktp']['name'];

                $tmp_file1=$_FILES['file_daftar']['tmp_name'];
                $name_file1 =$_FILES['file_daftar']['name'];

                    if($name_file!=''){
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp='ID_'.$npwp.'.'.$extensi;
                        $nf='upload/ktp/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                    if($name_file1!=''){
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_form='F_'.$npwp.'.'.$extensi;
                        $nf='upload/form_daftar/'.$img_name_form;
                        move_uploaded_file($tmp_file1,$nf);
                    }
            $this->db->set('WP_ID', $id_wp);
            $this->db->set('NPWP', $npwp);
            $this->db->set('NAMA', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('JENISWP',$jwp);
            $this->db->set('ALAMAT', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('KECAMATAN', $ex[0]);
            $this->db->set('KELURAHAN', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('NO_TELP', $this->input->post('NO_TELP',TRUE));
            $this->db->set('BUKTIDIRI', $this->input->post('JENIS_BUKTI',TRUE));            
            $this->db->set('NO_BUKTI_DIRI', $this->input->post('NO_BUKTi',TRUE));            
            $this->db->set('JABATAN', $this->input->post('JABATAN',TRUE));
            $this->db->set('NAMA_BADAN', $this->input->post('NAMA_USAHA',TRUE));  

            $this->db->set('NO_DAFTAR', $id_wp);
            $this->db->set('TGL_DAFTAR', "sysdate",false);
            $this->db->set('S_VALID', "sysdate",false);
            $this->db->set('STATUS', '1');//ststus 0 menunggu, 1 setuju, 3 ditolak
            $this->db->set('EMAIL', $this->input->post('EMAIL',TRUE));
            $this->db->set('FILE_BUKTI_DIRI', $img_name_ktp);
            $this->db->set('FILE_FORMULIR', $img_name_form);
            $this->db->insert('WAJIB_PAJAK');
           // if ($step1==true) {
                //$this->db->query("INSERT INTO MS_PENGGUNA (NIP ,NAMA,UNIT_UPT_ID,MS_ROLE_ID,USERNAME,PASSWORD) VALUES('"&A5&"','"&D5&"','2','"&B5&"','"&C5&"','1234')");
                $NAMA_USAHA= $this->input->post('NAMA_USAHA',TRUE);
                $ALAMAT_USAHA= $this->input->post('ALAMAT_USAHA',TRUE);
                $KODEKEC= $this->input->post('KECAMATAN_USAHA',TRUE);
                $KODEKEL= $this->input->post('KELURAHAN_USAHA',TRUE);
                $JENIS_PAJAK= $this->input->post('JENIS_USAHA',TRUE);
                $ID_OP= $this->input->post('GOLONGAN',TRUE);
                $NAMA_U=$this->input->post('NAMA_WP',TRUE);
                $this->db->query("INSERT INTO TEMPAT_USAHA (NAMA_USAHA,ALAMAT_USAHA,NPWPD,KODEKEC,KODEKEL,JENIS_PAJAK,ID_OP) 
                                 VALUES('$NAMA_USAHA','$ALAMAT_USAHA','$npwp','$KODEKEC','$KODEKEL','$JENIS_PAJAK','$ID_OP')");
                $this->db->query("insert into ms_pengguna ( NIP, NAMA, UNIT_UPT_ID, MS_ROLE_ID, USERNAME, PASSWORD, TANGGAL_INSERT,STATUS) 
                                  values('$npwp','$NAMA_U','2','8','$npwp','$npwp',sysdate,'1')");
              
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                $tujuan=$this->input->post('EMAIL',TRUE);
                //$tujuan='adypabbsi@gmail.com';
                $ci                  = get_instance();
                $ci->load->library('email');
                $config['protocol']  = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "muhammad.adypranoto@gmail.com"; 
                $config['smtp_pass'] = "12345hikam";
                $config['charset']   = "utf-8";
                $config['mailtype']  = "html";
                $config['newline']   = "\r\n";          
                $ci->email->initialize($config);            
                $ci->email->from('muhammad.adypranoto@gmail.com', 'PDRD.KAB.MALANG');
                $list                = array($tujuan);
                $ci->email->to($list);
                $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                $ci->email->subject('Informasi Pendaftaran');
                $ci->email->message('Terimakasih Bapak/Ibu '.$this->input->post('NAMA_WP',TRUE).' Sudah Mendaftar di aplikasi E-Pajak, tim penetapan akan segera meng verivikasi data anda');
                $ci->email->send();
                echo "<script>window.alert('Pendaftaran WP Baru Berhasil');window.location='".site_url('Master/pokok/wajibPajak')."';</script>";
            } else {
                echo "<script>window.alert('Pendaftaran WP Baru Gagal');window.location='".site_url('Master/pokok/daftar')."';</script>";
            }
            
            
    }
    function Proses_pendaftaran_manual(){
            $kode1=$this->input->post('kode1',TRUE);
            $kode2=$this->input->post('kode2',TRUE);
            $kode3=$this->input->post('kode3',TRUE);
            $wp_id=$this->db->query("select wp_id from wajib_pajak where wp_id='$kode2'")->row();
            if (count($wp_id)<=0) {
                 $this->db->trans_start();
            $ex= explode('|', $this->input->post('KECAMATAN',TRUE));
            $jwp=$this->input->post('JENIS_WP',TRUE);           
            $npwp=$kode1.$kode2.$kode3;
                $tmp_file=$_FILES['file_ktp']['tmp_name'];
                $name_file =$_FILES['file_ktp']['name'];

                $tmp_file1=$_FILES['file_daftar']['tmp_name'];
                $name_file1 =$_FILES['file_daftar']['name'];

                    if($name_file!=''){
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp='ID_'.$npwp.'.'.$extensi;
                        $nf='upload/ktp/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                    if($name_file1!=''){
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_form='F_'.$npwp.'.'.$extensi;
                        $nf='upload/form_daftar/'.$img_name_form;
                        move_uploaded_file($tmp_file1,$nf);
                    }
            $this->db->set('WP_ID', $kode2);
            $this->db->set('NPWP', $npwp);
            $this->db->set('NAMA', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('JENISWP',$jwp);
            $this->db->set('ALAMAT', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('KECAMATAN', $ex[0]);
            $this->db->set('KELURAHAN', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('NO_TELP', $this->input->post('NO_TELP',TRUE));
            $this->db->set('BUKTIDIRI', $this->input->post('JENIS_BUKTI',TRUE));            
            $this->db->set('NO_BUKTI_DIRI', $this->input->post('NO_BUKTi',TRUE));            
            $this->db->set('JABATAN', $this->input->post('JABATAN',TRUE)); 
            $this->db->set('NAMA_BADAN', $this->input->post('NAMA_USAHA',TRUE)); 

            $this->db->set('NO_DAFTAR', $kode2);
            $this->db->set('TGL_DAFTAR', "sysdate",false);
            $this->db->set('S_VALID', "sysdate",false);
            $this->db->set('STATUS', '1');//ststus 0 menunggu, 1 setuju, 3 ditolak
            $this->db->set('EMAIL', $this->input->post('EMAIL',TRUE));
            $this->db->set('FILE_BUKTI_DIRI', $img_name_ktp);
            $this->db->set('FILE_FORMULIR', $img_name_form);
            $this->db->insert('WAJIB_PAJAK');
           // if ($step1==true) {
                //$this->db->query("INSERT INTO MS_PENGGUNA (NIP ,NAMA,UNIT_UPT_ID,MS_ROLE_ID,USERNAME,PASSWORD) VALUES('"&A5&"','"&D5&"','2','"&B5&"','"&C5&"','1234')");
                $NAMA_USAHA= $this->input->post('NAMA_USAHA',TRUE);
                $ALAMAT_USAHA= $this->input->post('ALAMAT_USAHA',TRUE);
                $KODEKEC= $this->input->post('KECAMATAN_USAHA',TRUE);
                $KODEKEL= $this->input->post('KELURAHAN_USAHA',TRUE);
                $JENIS_PAJAK= $this->input->post('JENIS_USAHA',TRUE);
                $ID_OP= $this->input->post('GOLONGAN',TRUE);
                $NAMA_U=$this->input->post('NAMA_WP',TRUE);
                $this->db->query("INSERT INTO TEMPAT_USAHA (NAMA_USAHA,ALAMAT_USAHA,NPWPD,KODEKEC,KODEKEL,JENIS_PAJAK,ID_OP) 
                                 VALUES('$NAMA_USAHA','$ALAMAT_USAHA','$npwp','$KODEKEC','$KODEKEL','$JENIS_PAJAK','$ID_OP')");
                $this->db->query("insert into ms_pengguna ( NIP, NAMA, UNIT_UPT_ID, MS_ROLE_ID, USERNAME, PASSWORD, TANGGAL_INSERT,STATUS) 
                                  values('$npwp','$NAMA_U','2','8','$npwp','$npwp',sysdate,'1')");
              
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                $tujuan=$this->input->post('EMAIL',TRUE);
                //$tujuan='adypabbsi@gmail.com';
                $ci                  = get_instance();
                $ci->load->library('email');
                $config['protocol']  = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "muhammad.adypranoto@gmail.com"; 
                $config['smtp_pass'] = "12345hikam";
                $config['charset']   = "utf-8";
                $config['mailtype']  = "html";
                $config['newline']   = "\r\n";          
                $ci->email->initialize($config);            
                $ci->email->from('muhammad.adypranoto@gmail.com', 'PDRD.KAB.MALANG');
                $list                = array($tujuan);
                $ci->email->to($list);
                $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                $ci->email->subject('Informasi Pendaftaran');
                $ci->email->message('Terimakasih Bapak/Ibu '.$this->input->post('NAMA_WP',TRUE).' Sudah Mendaftar di aplikasi E-Pajak, tim penetapan akan segera meng verivikasi data anda');
                $ci->email->send();
                echo "<script>window.alert('Pendaftaran WP Baru Berhasil');window.location='".site_url('Master/pokok/wajibPajak')."';</script>";
            } else {
                echo "<script>window.alert('Pendaftaran WP Baru Gagal');window.location='".site_url('Master/pokok/daftar')."';</script>";
            }
            } else {
                echo "npwp sudah terdaftar";
            }
            
           
            
            
    }
    public function perforasi(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $npwpd     = urldecode($this->input->get('npwpd', TRUE));
        $start = intval($this->input->get('start'));
        $this->session->set_userdata('npwpd',$npwpd);
        if ($tgl1 <> ''||$tgl2 <> ''||$npwpd) {
            $config['base_url']  = base_url() . 'Master/pokok/perforasi?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&npwpd='.urlencode($npwpd);
            $config['first_url'] = base_url() . 'Master/pokok/perforasi?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&npwpd='.urlencode($npwpd);
        } else {
            $config['base_url']  = base_url() . 'Master/pokok/perforasi';
            $config['first_url'] = base_url() . 'Master/pokok/perforasi';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mpokok->total_rows_perforasi($tgl1,$tgl2,$npwpd);
        $perforasi                   = $this->Mpokok->get_limit_data_perforasi($config['per_page'],  $start, $tgl1,$tgl2,$npwpd);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'perforasi'         => $perforasi,
             'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'npwpd'      => $npwpd,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start
        );

        $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','vperforasi',$data);;
  }
  public function tambah_perforasi(){
        // $this->load->view('Auth/formlogin2');
        $this->template->load('Welcome/halaman','form_perforasi');
    }
    public function karcis($x)
    {
         $data['x']=$x;
         $this->load->view('Master/perforasi_append',$data);
    }

    public function create_action_perforasi(){
        $this->db->trans_start();
        $exp=explode('|',$_POST['NAMA_USAHA']);
        $TANGGAL=$_POST['tgl'];
                    $this->Mpokok->getFormulir();
                    /*$this->db->set('NOMOR_INC_BLN',$this->Mpokok->getNoIncBln());
                    $this->db->set('NOMOR_INC_THN',$this->Mpokok->getNoIncThn());
                    $this->db->set('NOMOR_OBJEK',$this->Mpokok->getNoObjek($exp[1]));
                    $this->db->set('NOMOR_BLN_THN',$this->Mpokok->getNoRomawi());*/
                    $this->db->set('NOMOR_INC_BLN',$_POST['kode1']);
                    $this->db->set('NOMOR_INC_THN',$_POST['kode2']);
                    $this->db->set('NOMOR_OBJEK',$this->Mpokok->getNoObjek($exp[1]));
                    $this->db->set('NOMOR_BLN_THN',$_POST['kode3']);

                    $this->db->set('NPWPD',$_POST['NPWPD']);
                    $this->db->set('NAMA_WP_PERF',$_POST['NAMA_WP']);
                    $this->db->set('TGL_PERFORASI',"to_date('$TANGGAL','dd/mm/yyyy')",false);
                    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
                    $this->db->set('NIP_INSERT',$this->nip);
                    $this->db->set('ID_TEMPAT_USAHA',$exp[0]);
                    $this->db->set('NAMA_USAHA',$exp[1]);
                    $this->db->set('TGL_INPUT', "sysdate",false);
                    $this->db->insert('NOMOR_PERFORASI');
        if(!empty($_POST['No_SERI'])){
                $count=count($_POST['No_SERI']);
                for($i=0;$i<$count;$i++){                    
                    $nomor=$this->db->query("SELECT MAX(ID_INC) NEXT_VAL FROM NOMOR_PERFORASI")->row();
                    $NEXT=$nomor->NEXT_VAL;                    
                    $this->db->set('JENIS_KARCIS',$_POST['JENIS_KARCIS']);
                    $this->db->set('NOMOR_SERI',$_POST['No_SERI'][$i]);
                    $this->db->set('NO_URUT_KARCIS',$_POST['NO_URUT1'][$i].'-'.$_POST['NO_URUT2'][$i]);
                    $this->db->set('NILAI_LEMBAR',str_replace('.', '', $_POST['NILAI'][$i]));
                    $this->db->set('JML_BLOK',str_replace('.', '', $_POST['JUMLAH_BLOK'][$i]));
                    $this->db->set('ISI_LEMBAR',str_replace('.', '', $_POST['LBR_BLOK'][$i]));
                    $this->db->set('PERSEN',$_POST['PAJAK'][$i]);
                    $this->db->set('CATATAN',$_POST['CATATAN'][$i]);
                    $this->db->set('NO_PERMOHONAN',$NEXT);
                   $this->db->insert('PERFORASI');
                   
                }
            }
    $this->db->trans_complete();
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Tambah Perforasi", "", "success")
     });

     </script>');
   redirect(site_url('Master/pokok/perforasi'));
    }
    public function hapus_perforasi($id=""){
        $this->db->query("delete from perforasi where no_permohonan='$id'");
        $this->db->query("delete from nomor_perforasi where id_inc='$id'");
        redirect(site_url('Master/pokok/perforasi'));
    }
    public function edit_perforasi($id=""){
       $data['edit_perforasi']=$this->db->query("SELECT * from perforasi where no_permohonan ='$id'  order by catatan,nomor_seri")->result();
       $this->template->load('Welcome/halaman','edit_perforasi',$data);;
  }
  public function Proses_Edit_Perforasi(){
        $this->db->trans_start();
        $id_inc=$_POST['id_inc'];
        $no_permohonan=$_POST['no_permohonan'];
        $no_urut=$_POST['no_urut'];
        $jml_blok=$_POST['jml_blok'];
        $catatan=$_POST['catatan'];
        $seri=$_POST['seri'];
        $jml_isi_perblok=str_replace('.', '', $_POST['jml_isi_perblok']);
        $harga=str_replace('.', '', $_POST['harga']);
        $this->db->query("update perforasi set NO_URUT_KARCIS='$no_urut',JML_BLOK='$jml_blok',ISI_LEMBAR='$jml_isi_perblok',CATATAN='$catatan',NILAI_LEMBAR='$harga',NOMOR_SERI='$seri' where id_inc='$id_inc'");                    
            $this->db->trans_complete();
            $this->session->set_flashdata('message', '<script>
              $(window).load(function(){
                 swal("Berhasil Edit Perforasi", "", "success")
             });

             </script>');
           redirect(site_url('Master/pokok/edit_perforasi/'.$no_permohonan));
    }

    function getUsaha(){
        $npwp=$_POST['x'];
        $data['tu']=$this->db->query(" SELECT a.*,nama_pajak from tempat_usaha a
          join jenis_pajak b on a.jenis_pajak=b.id_inc WHERE npwpd ='$npwp' ")->result();
        //echo $npwp;
        
       /* foreach($tu as $gol){
            echo "<option value='".$gol->ID_INC."'>".$gol->NAMA_USAHA."</option>";
        };*/
        $this->load->view('Master/getUsaha',$data);
    }
    function ceknoper_hiburan(){
        // echo $_POST['npwpd'];
        $var=$_POST['noper'];
        $res=$this->db->query("SELECT * from v_lookup_perforasi_hiburan where no_permohonan ='$var'")->row();
        $data = array(
                    "NPWPD"   =>  $res->NPWPD,
                    "NAMA_USAHA"   =>  $res->NAMA_USAHA,
                    "NAMA_WP" => $res->NAMA,
                    "ALAMAT_WP" => $res->ALAMAT,
                    "ALAMAT_USAHA" => $res->ALAMAT_USAHA,
                    "DESKRIPSI" => $res->DESKRIPSI,
                    "NAMAKELURAHAN" => $res->NAMAKELURAHAN,
                    "NAMAKEC" => $res->NAMAKEC,

                    "KODEKEC" => $res->KODEKEC,
                    "KODEKEL" => $res->KODEKEL,
                    "ID_OP" => $res->ID_OP,
                    //dataperforasi
                    "NILAI_LEMBAR" => $res->NILAI_LEMBAR,
                    "NO_SERI" => $res->NOMOR_SERI,
                    "NO_URUT_KARCIS" => $res->NO_URUT_KARCIS,
                    "JML_BLOK" => $res->JML_BLOK,
                    "ISI_LEMBAR" => $res->ISI_LEMBAR,
                    "TGL_PERFORASI" => $res->TGL_PERFORASI
                    );                    
                header('Content-Type: application/json');
                echo json_encode($data);
                error_reporting(E_ALL^(E_NOTICE|E_WARNING));
    }
    public function edit_wp($id=""){
        $id=rapikan($id);
        $row = $this->db->query("select * from WAJIB_PAJAK where npwp='$id'")->row();
    if ($row) {
        $data = array(
            'button'              => 'Update Wajib Pajak',
            'action'              => site_url('Master/pokok/update_action'),
            'disable'             => '',
            'WP_ID'               => set_value('WP_ID',$row->WP_ID),
            'NPWP'          => set_value('NPWP',$row->NPWP),
            'JENISWP'         => set_value('JENISWP',$row->JENISWP),
            'JABATAN'               => set_value('JABATAN',$row->JABATAN),
            'ALAMAT'             => set_value('ALAMAT',$row->ALAMAT),
            'NO_TELP'           => set_value('NO_TELP',$row->NO_TELP),
            'KELURAHAN'          => set_value('KELURAHAN',$row->KELURAHAN),
            'KECAMATAN'        => set_value('KECAMATAN',$row->KECAMATAN),
            'NO_BUKTI_DIRI'         => set_value('NO_BUKTI_DIRI',$row->NO_BUKTI_DIRI),
            'BUKTIDIRI'  => set_value('BUKTIDIRI',$row->BUKTIDIRI),
            'EMAIL'  => set_value('EMAIL',$row->EMAIL),
            'NAMA'  => set_value('NAMA',$row->NAMA)

        );
        $data['kec']=$this->db->query("select * from kecamatan order by namakec")->result();
        $data['kel']=$this->db->query("select * from kelurahan where kodekec='$row->KECAMATAN' order by namakelurahan")->result();
         $this->template->load('Welcome/halaman','vwajibPajakEdit',$data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
    }

    public function edit_wp_action(){         
            $this->db->set('JENISWP',$this->input->post('JENISWP',TRUE));
            $this->db->set('ALAMAT', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('KECAMATAN', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KELURAHAN', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('NO_TELP', $this->input->post('TELEPON',TRUE));
            $this->db->set('BUKTIDIRI', $this->input->post('JENIS_BUKTI',TRUE));            
            $this->db->set('NO_BUKTI_DIRI', $this->input->post('NO_BUKTi',TRUE));            
            $this->db->set('JABATAN', $this->input->post('JABATAN',TRUE)); 
            $this->db->set('NAMA', $this->input->post('NAMA',TRUE)); 

            $this->db->set('EMAIL', $this->input->post('EMAIL',TRUE));
            $this->db->where('NPWP',$this->input->post('NPWP', TRUE));
            $this->db->update('WAJIB_PAJAK');
            echo "<script>window.alert('Update Data WP');window.location='".site_url('Master/pokok/wajibPajak')."';</script>";
    }
    function pengajuan_npwpd(){    
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $sts     = urldecode($this->input->get('sts', TRUE));
        $jwp     = urldecode($this->input->get('jwp', TRUE)); 
        $nama     = urldecode($this->input->get('nama', TRUE));       
        $start   = intval($this->input->get('start'));
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Master/pokok/pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&sts='.urlencode($sts).'&jwp='.urlencode($jwp).'&nama='.urlencode($nama);
            $config['first_url'] = base_url() . 'Master/pokok/pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&sts='.urlencode($sts).'&jwp='.urlencode($jwp).'&nama='.urlencode($nama);
            //$cetak= base_url() . 'Excel/Excel/Excel_pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kecamatan='.urlencode($kecamatan);
        } else {
            $tgl2='';
            $tgl1='';
            $sts='';
            $config['base_url']  = base_url() . 'Master/pokok/pengajuan_npwpd';
            $config['first_url'] = base_url() . 'Master/pokok/pengajuan_npwpd';
            //$cetak= base_url() . 'Excel/Excel/Excel_pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mpokok->total_rows_pengajuan_npwpd($tgl1,$tgl2,$sts,$nama,$jwp);
        $pengajuan                 = $this->Mpokok->get_limit_data_pengajuan_npwpd($config['per_page'], $start, $tgl1,$tgl2,$sts,$nama,$jwp);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'sts'        => $sts,
            'jwp'        => $jwp
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Pengajuan NPWPD Baru',
            'pengajuan'        => $pengajuan,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'sts'              => $sts,
            'jwp'              => $jwp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'cetak'            => $cetak
        );
       $data['mp']=$this->Mpokok->listMasapajak();
       $this->template->load('Welcome/halaman','pengajuan_npwpd',$data);
    }
    function lookup_wp(){
        $keyword = $this->input->post('term');
        $data['response'] = 'false'; //Set default response        
        $query = $this->db->query("SELECT a.nama||' || '||alamat||' || '||namakelurahan||' || '||namakec||' || '||nama_badan nama,UTL_MATCH.jaro_winkler_similarity(a.nama,'$keyword') 
                                   FROM wajib_pajak a
                                   left JOIN kecamatan d ON a.kecamatan = D.KODEKEC
                                   left JOIN kelurahan e ON a.kelurahan = e.KODEKELURAHAN AND A.kecamatan = E.KODEKEC
                                   where UTL_MATCH.jaro_winkler_similarity(a.nama,'$keyword')>73")->result(); //Search DB
        if( ! empty($query) )
        {
            $data['response'] = 'true'; //Set response
            $data['message'] = array(); //Create array
            foreach( $query as $row )
            {
                $data['message'][] = array(
                                         'value' => $row->NAMA,
                                        ''
                                     );  //Add a row to array
            }
        }
        if($data!=''){
            echo json_encode($data); //echo json string if ajax request
        }else{
            
        }
    }
    public function tambah_pengajuan_npwpd(){
        // $this->load->view('Auth/formlogin2');
        $data['kec']=$this->db->query("select * from kecamatan where kodekec !='99'")->result();
        $data['kec1']=$this->db->query("select * from kecamatan where kodekec !='99'")->result();
        $data['jenis']=$this->db->query("select * from JENIS_PAJAK order by ID_INC")->result();
        $this->template->load('Welcome/halaman','vpengajuan_npwpd',$data);
    }
    function proses_pengajuan_npwpd(){
            $this->db->trans_start();
            //$id_wp=$this->Mpokok->getnoformulirwp();
            
               
            $ex= explode('|', $this->input->post('KECAMATAN',TRUE));
            
            //$kec=sprintf("%04d", $ex[0]);
            $jwp=$this->input->post('JENIS_WP',TRUE);
            //$npwp=$jwp.$id_wp.$kec;
           // $this->db->set('WP_ID', $id_wp);
            //$this->db->set('NPWP', $npwp);
            $this->db->set('NAMA', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('JENISWP',$jwp);
            $this->db->set('ALAMAT', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('KECAMATAN', $ex[0]);
            $this->db->set('KELURAHAN', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('NO_TELP', $this->input->post('TELEPON',TRUE));
            $this->db->set('BUKTIDIRI', $this->input->post('JENIS_BUKTI',TRUE));            
            $this->db->set('NO_BUKTI_DIRI', $this->input->post('NO_BUKTi',TRUE));            
            $this->db->set('JABATAN', $this->input->post('JABATAN',TRUE)); 
            $this->db->set('NAMA_BADAN', $this->input->post('NAMA_USAHA',TRUE)); 

            //$this->db->set('NO_DAFTAR', $id_wp);
            $this->db->set('TGL_DAFTAR', "sysdate",false);
            $this->db->set('S_VALID', "sysdate",false);
            $this->db->set('STATUS', '0');//ststus 0 menunggu, 1 setuju, 3 ditolak
            $this->db->set('EMAIL', $this->input->post('EMAIL',TRUE));
            $this->db->set('UPT', $this->session->userdata('UNIT_UPT_ID'));
            $this->db->insert('WAJIB_PAJAK_TEMP');
           // if ($step1==true) {
                //$this->db->query("INSERT INTO MS_PENGGUNA (NIP ,NAMA,UNIT_UPT_ID,MS_ROLE_ID,USERNAME,PASSWORD) VALUES('"&A5&"','"&D5&"','2','"&B5&"','"&C5&"','1234')");
                $wp_id=$this->db->query("select max(ID_INC_WP_TEMP)no from WAJIB_PAJAK_temp")->row();
               
                $tmp_file=$_FILES['file_ktp']['tmp_name'];
                $name_file =$_FILES['file_ktp']['name'];

                $tmp_file1=$_FILES['file_daftar']['tmp_name'];
                $name_file1 =$_FILES['file_daftar']['name'];

                    if($name_file!=''){
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp='ID_'.$wp_id->NO.'.'.$extensi;
                        $nf='upload/ktp/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                    if($name_file1!=''){
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_form='F_'.$wp_id->NO.'.'.$extensi;
                        $nf='upload/form_daftar/'.$img_name_form;
                        move_uploaded_file($tmp_file1,$nf);
                    }
                $this->db->query("UPDATE WAJIB_PAJAK_TEMP SET FILE_BUKTI_DIRI='$img_name_ktp',FILE_FORMULIR='$img_name_form' WHERE ID_INC_WP_TEMP='$wp_id->NO'");
                $id_wp=$wp_id->NO;
                $NAMA_USAHA= $this->input->post('NAMA_USAHA',TRUE);
                $ALAMAT_USAHA= $this->input->post('ALAMAT_USAHA',TRUE);
                $KODEKEC= $this->input->post('KECAMATAN_USAHA',TRUE);
                $KODEKEL= $this->input->post('KELURAHAN_USAHA',TRUE);
                $JENIS_PAJAK= $this->input->post('JENIS_USAHA',TRUE);
                $ID_OP= $this->input->post('GOLONGAN',TRUE);

                $this->db->query("INSERT INTO TEMPAT_USAHA_TEMP (NAMA_USAHA,ALAMAT_USAHA,NPWPD,KODEKEC,KODEKEL,JENIS_PAJAK,ID_OP,STATUS,REF_ID_INC_WP_TEMP) 
                                 VALUES('$NAMA_USAHA','$ALAMAT_USAHA','','$KODEKEC','$KODEKEL','$JENIS_PAJAK','$ID_OP','0','$id_wp')");;
              
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                echo "<script>window.alert('Pengajuan WP Baru Berhasil');window.location='".site_url('Master/pokok/pengajuan_npwpd')."';</script>";
            } else {
                echo "<script>window.alert('Pengajuan WP Baru Gagal');window.location='".site_url('Master/pokok/pengajuan_npwpd')."';</script>";
            }
            
            
    }
    function detail_objek_pajak_temp($id=""){
        
        $data['detail_objek_pajak']=$this->db->query("select b.*,nama_pajak from tempat_usaha_temp b
                                join JENIS_PAJAK a on a.id_inc=b.jenis_pajak 
                                where REF_ID_INC_WP_TEMP='$id'
                                order by a.id_inc")->result();
        $data['wp']=$this->db->query("SELECT STATUS,NPWP,NAMA,ALAMAT,JABATAN ,email,tgl_daftar,buktidiri,NO_BUKTI_DIRI,NO_TELP,kecamatan,kelurahan,namakec,namakelurahan
                                        FROM WAJIB_PAJAK_temp a
                                        left join kecamatan d on a.kecamatan=D.KODEKEC
                                        left join kelurahan e on a.kelurahan=e.KODEKELURAHAN and A.kecamatan=E.KODEKEC WHERE ID_INC_WP_TEMP='$id'")->row();
        $this->template->load('Welcome/halaman','vdetailTmpUsaha_temp',$data);
    }
    function terima_pengajuan($id=""){
        $this->db->trans_start();
            $id_wp=$this->Mpokok->getnoformulirwp();
            $cek=$this->db->query("SELECT JENISWP,kecamatan,email,NAMA from WAJIB_PAJAK_TEMP WHERE ID_INC_WP_TEMP='$id'")->row();
            $jwp=$cek->JENISWP;
            $NAMA_U=$cek->NAMA;
            $kec=sprintf("%04d", $cek->KECAMATAN);
            $npwp=$jwp.$id_wp.$kec;         
            
            $this->db->query("INSERT INTO WAJIB_PAJAK (WP_ID,NPWP,NAMA,JENISWP,ALAMAT,KECAMATAN,KELURAHAN,NO_TELP,BUKTIDIRI,NO_BUKTI_DIRI,NAMA_BADAN,
                    NO_DAFTAR,TGL_DAFTAR,S_VALID,STATUS,EMAIL,TGL_APPROVE,FILE_BUKTI_DIRI,FILE_FORMULIR)
            SELECT '$id_wp','$npwp',NAMA,JENISWP,ALAMAT,KECAMATAN,KELURAHAN,NO_TELP,BUKTIDIRI,NO_BUKTI_DIRI,NAMA_BADAN,
                    '$id_wp',TGL_DAFTAR,S_VALID,1,EMAIL,sysdate,FILE_BUKTI_DIRI,FILE_FORMULIR from WAJIB_PAJAK_temp where ID_INC_WP_TEMP='$id'");
          
                //$this->db->query("INSERT INTO MS_PENGGUNA (NIP ,NAMA,UNIT_UPT_ID,MS_ROLE_ID,USERNAME,PASSWORD) VALUES('"&A5&"','"&D5&"','2','"&B5&"','"&C5&"','1234')");
            
                $this->db->query("INSERT INTO TEMPAT_USAHA (NAMA_USAHA,ALAMAT_USAHA,NPWPD,KODEKEC,KODEKEL,JENIS_PAJAK,ID_OP) 
                                select NAMA_USAHA,ALAMAT_USAHA,'$npwp',KODEKEC,KODEKEL,JENIS_PAJAK,ID_OP from tempat_usaha_temp WHERE REF_ID_INC_WP_TEMP='$id'");
                $this->db->query("update WAJIB_PAJAK_TEMP set STATUS='1',npwp='$npwp',TGL_APPROVE=sysdate WHERE ID_INC_WP_TEMP='$id'");
                $this->db->query("update tempat_usaha_temp set STATUS='1',npwpd='$npwp' WHERE REF_ID_INC_WP_TEMP='$id'");
                $this->db->query("insert into ms_pengguna (NIP, NAMA, UNIT_UPT_ID, MS_ROLE_ID, USERNAME, PASSWORD, TANGGAL_INSERT,STATUS) 
                                  values('$npwp','$NAMA_U','2','8','$npwp','$npwp',sysdate,'1')");
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE) {
                $tujuan=$cek->EMAIL;
                //$tujuan='adypabbsi@gmail.com';
                $ci                  = get_instance();
                $ci->load->library('email');
                $config['protocol']  = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "muhammad.adypranoto@gmail.com"; 
                $config['smtp_pass'] = "12345hikam";
                $config['charset']   = "utf-8";
                $config['mailtype']  = "html";
                $config['newline']   = "\r\n";          
                $ci->email->initialize($config);            
                $ci->email->from('muhammad.adypranoto@gmail.com', 'PDRD.KAB.MALANG');
                $list                = array($tujuan);
                $ci->email->to($list);
                $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                $ci->email->subject('Informasi Pendaftaran');
                $ci->email->message('Terimakasih Bapak/Ibu '.$this->input->post('NAMA_WP',TRUE).' Sudah Mendaftar di aplikasi E-Pajak, tim penetapan akan segera meng verivikasi data anda');
                $ci->email->send();
                echo "<script>window.alert('Pendaftaran WP Baru Berhasil');window.location='".site_url('Master/pokok/wajibPajak')."';</script>";
            } 
        echo "<script>window.alert('Pengajuan Berhasil Diterima');window.location='".site_url('Master/pokok/pengajuan_npwpd')."';</script>";
        
    }
    function tolak_pengajuan($id=""){
        $this->db->query("update WAJIB_PAJAK_TEMP set STATUS='2' WHERE ID_INC_WP_TEMP='$id'");
        $this->db->query("update tempat_usaha_temp set STATUS='2' WHERE REF_ID_INC_WP_TEMP='$id'");
         echo "<script>window.alert('Pengajuan Berhasil ditolak');window.location='".site_url('Master/pokok/pengajuan_npwpd')."';</script>";
        
    }
    function hapus_pengajuan($id=""){
        $cek=$this->db->query("SELECT FILE_BUKTI_DIRI,FILE_FORMULIR from WAJIB_PAJAK_TEMP WHERE ID_INC_WP_TEMP='$id'")->row();
        $this->db->query("delete WAJIB_PAJAK_TEMP WHERE ID_INC_WP_TEMP='$id'");
        $this->db->query("delete tempat_usaha_temp WHERE REF_ID_INC_WP_TEMP='$id'");
        $nf        ="upload/ktp/".$cek->FILE_BUKTI_DIRI;
        $nf1        ="upload/form_daftar/".$cek->FILE_FORMULIR;
        @unlink($nf);
        @unlink($nf1);
         echo "<script>window.alert('Pengajuan Berhasil Hapus');window.location='".site_url('Master/pokok/pengajuan_npwpd')."';</script>";
        
    }
    public function edit_wp_temp($id=""){
        //$id=rapikan($id);
        $row = $this->db->query("select * from WAJIB_PAJAK_TEMP where ID_INC_WP_TEMP='$id'")->row();
    if ($row) {
        $data = array(
            'button'              => 'Update Wajib Pajak',
            'action'              => site_url('Master/pokok/update_action'),
            'disable'             => '',
            'WP_ID'               => set_value('WP_ID',$row->WP_ID),
            'NPWP'          => set_value('NPWP',$row->NPWP),
            'JENISWP'         => set_value('JENISWP',$row->JENISWP),
            'JABATAN'               => set_value('JABATAN',$row->JABATAN),
            'ALAMAT'             => set_value('ALAMAT',$row->ALAMAT),
            'NO_TELP'           => set_value('NO_TELP',$row->NO_TELP),
            'KELURAHAN'          => set_value('KELURAHAN',$row->KELURAHAN),
            'KECAMATAN'        => set_value('KECAMATAN',$row->KECAMATAN),
            'NO_BUKTI_DIRI'         => set_value('NO_BUKTI_DIRI',$row->NO_BUKTI_DIRI),
            'BUKTIDIRI'  => set_value('BUKTIDIRI',$row->BUKTIDIRI),
            'EMAIL'  => set_value('EMAIL',$row->EMAIL),
            'NAMA'  => set_value('NAMA',$row->NAMA),
            'ID_INC_WP_TEMP' => set_value('ID_INC_WP_TEMP',$row->ID_INC_WP_TEMP)

        );
        $data['kec']=$this->db->query("select * from kecamatan order by namakec")->result();
        $data['kel']=$this->db->query("select * from kelurahan where kodekec='$row->KECAMATAN' order by namakelurahan")->result();
         $this->template->load('Welcome/halaman','editpengajuan',$data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
    }
    public function edit_wp_temp_action(){
                $wp_id=$this->input->post('ID_INC_WP_TEMP', TRUE); 
                $tmp_file=$_FILES['file_ktp']['tmp_name'];
                $name_file =$_FILES['file_ktp']['name'];

                $tmp_file1=$_FILES['file_daftar']['tmp_name'];
                $name_file1 =$_FILES['file_daftar']['name'];

                    if($name_file!=''){
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp='ID_'.$wp_id.'.'.$extensi;
                        $nf='upload/ktp/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                    if($name_file1!=''){
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_form='F_'.$wp_id.'.'.$extensi;
                        $nf='upload/form_daftar/'.$img_name_form;
                        move_uploaded_file($tmp_file1,$nf);
                    }       
            $this->db->set('JENISWP',$this->input->post('JENISWP',TRUE));
            $this->db->set('ALAMAT', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('KECAMATAN', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KELURAHAN', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('NO_TELP', $this->input->post('TELEPON',TRUE));
            $this->db->set('BUKTIDIRI', $this->input->post('JENIS_BUKTI',TRUE));            
            $this->db->set('NO_BUKTI_DIRI', $this->input->post('NO_BUKTi',TRUE));            
            $this->db->set('JABATAN', $this->input->post('JABATAN',TRUE)); 
            $this->db->set('NAMA', $this->input->post('NAMA',TRUE)); 
            $this->db->set('EMAIL', $this->input->post('EMAIL',TRUE));
            $this->db->set('FILE_BUKTI_DIRI', $img_name_ktp);
            $this->db->set('FILE_FORMULIR', $img_name_form);
            $this->db->where('ID_INC_WP_TEMP',$this->input->post('ID_INC_WP_TEMP', TRUE));
            $this->db->update('WAJIB_PAJAK_TEMP');
            echo "<script>window.alert('Update Data WP');window.location='".site_url('Master/pokok/pengajuan_npwpd')."';</script>";
    }
    public function pembatalan_kode_biling(){
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $param    = urldecode($this->input->get('param', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            /*'tahun'       => $tahun,
            'bulan'       => $bulan,*/
            'param'       => $param,
        );
        $this->session->set_userdata($sess);
        if ($param <> '') {
            $config['base_url']  = base_url() . 'Master/pokok/pembatalan_kode_biling?tahun=' .urlencode($param);
            $config['first_url'] = base_url() . 'Master/pokok/pembatalan_kode_biling?tahun=' .urlencode($param);
        } else {
            $config['base_url']  = base_url() . 'Master/pokok/pembatalan_kode_biling';
            $config['first_url'] = base_url() . 'Master/pokok/pembatalan_kode_biling';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mpokok->total_rows_manage_kobil(/*$tahun,$bulan,*/$param);
        $manage_kobil                = $this->Mpokok->get_limit_data_manage_kobil($config['per_page'], $start /*$tahun,$bulan*/,$param);
        //$data['link']= $config['base_url']  = base_url() . 'Master/pokok/pembatalan_kode_biling?tahun=' .urlencode($param);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Management Kode Biling',
            'manage_kobil'         => $manage_kobil,
         /*   'tahun'             => $tahun,
            'bulan'             => $bulan,*/
            'param'             => $param,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['link']=$config['base_url']  = $config['per_page'];
      $this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','pembatalan_kode_biling',$data);

  }
  function create_pembatalan(){
        if(isset($_POST['kode_biling'])){
            $data['kode_biling']=$_POST['kode_biling'];
            $data['rk']=$this->Mbayar->getKodebiling($_POST['kode_biling']);
        }else{
            $data['kode_biling']=null;
        }
        $this->template->load('Welcome/halaman','form_pembatalan',$data);
    }
public function pengajuan_pembatalan_tagihan()
{
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $KODE_BILING = $this->input->post('Nop');
        $ALASAN = $this->input->post('alasan');
        $NIP=$this->input->post('NIP_INSERT');
        $cek=$this->db->query("select nama from ms_pengguna where nip='$NIP'")->row();
        
        
if ($this->session->userdata('MS_ROLE_ID')=='10' AND $NIP!=$this->nip) {
                $data = array();
                $data = array(
                    "Status"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'13',
                        "ErrorDesc" => "Pengajuan Hanya bisa dilakukan  User ".$cek->NAMA." atau perwakilan dinas"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);        
} else {
        $q=$this->db->query("SELECT KODE_BILING, STATUS FROM TAGIHAN WHERE KODE_BILING='$KODE_BILING'")->row();
        if (!empty($q->KODE_BILING)) {
            if ($q->STATUS==1) {
                // tagihan telah lunas
                $data = array();
                $data = array(
                    "Status"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'13',
                        "ErrorDesc" => "Data Tagihan Telah Lunas"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            } else {
                // belum lunas
                    // sukses
                    $cek=$this->db->query("SELECT count(KODE_BILING)CEK from PEMBATALAN_TAGIHAN where kode_biling='$KODE_BILING'")->row();
                    if ($cek->CEK=='0') {
                        $ip=$this->Mpokok->getIP();
                        $user=$this->nip;
                        $upt=$this->session->userdata('UNIT_UPT_ID');
                        $res=$this->db->query("CALL P_INSERT_PENGAJUAN_PEMBATALAN('$KODE_BILING','$ip','$user','$upt','$ALASAN')");
                        $data = array();
                        if($res=='false'){
                            $data = array();
                            $data = array(
                                "Status"     =>  array(
                                    "IsError"      =>"False",
                                    "ResponseCode" =>'13',
                                    "ErrorDesc"    => "gagal pengajuan"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            
                          
                        }else{
                             $data = array(
                                "Nop"        =>  $q->KODE_BILING,
                                "Status"     =>  array(
                                    "IsError"      =>"False",
                                    "ResponseCode" =>'00',
                                    "ErrorDesc"    => "Berhasil membuat pengajuan"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            
                        }
                    } else {
                        $data = array();
                            $data = array(
                                "Status"     =>  array(
                                    "IsError"      =>"False",
                                    "ResponseCode" =>'13',
                                    "ErrorDesc"    => "gagal pengajuan,Data Sudah Diajukan"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                    }
                    
            }
        }else{
            // data tidak di temukan
            $data = array();
            $data = array(
                "Status"        =>  array(
                    "IsError"=>"True",
                    "ResponseCode"=>'10',
                    "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                )
            );
            header('Content-Type: application/json');
            echo json_encode($data);
        }
}

    }
}
    public function aprove_persetujuan($kode_biling="",$jp="",$link="") {
        $cek=$this->db->query("SELECT STATUS,VIRTUAL_ACCOUNT,NAMA_WP,TAGIHAN+DENDA-POToNGAN TOTAL,TO_CHAR(akhir_berlaku, 'yyyymmdd')ed from tagihan WHERE kode_biling='$kode_biling' or virtual_account='$kode_biling'")->row();
        if ($cek->STATUS=='1') {
                   echo "Kode Biling Sudah Terbayar";
        } else {
            if ($jp=='01') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_hotel where kode_biling='$kode_biling'");
            } else if ($jp=='02') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_restoran where kode_biling='$kode_biling'");
            } else if ($jp=='03') {
                $cek=$this->db->query("select id_inc from sptpd_hiburan where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_hiburan_detail where id_sptpd_hiburan='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_hiburan where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='04') {
              $cek=$this->db->query("select id_inc from sptpd_reklame where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_reklame_detail where sptpd_reklame_id='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_reklame where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='05') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_ppj where kode_biling='$kode_biling'");
            } else if ($jp=='06') {
              $cek=$this->db->query("select id_inc from sptpd_mineral_non_logam where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_mineral_detail where mineral_id='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_mineral_non_logam where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='07') {
              $cek=$this->db->query("select id_inc from sptpd_parkir where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_parkir_karcis where sptpd_parkir_id='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_parkir where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='08') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_air_tanah where kode_biling='$kode_biling'");
            } else {
              # code...
            }
            $this->Mpokok->registration_tiga($cek->VIRTUAL_ACCOUNT,$cek->NAMA_WP,$cek->TOTAL,$cek->ED);
            $this->db->query("UPDATE PEMBATALAN_TAGIHAN set STATUS_BATAL='1' , TGL_AKSI_BATAL=SYSDATE WHERE kode_biling='$kode_biling'");
            redirect(site_url('Master/pokok/pembatalan_kode_biling?start='));
                  
        }
 
    }
    function upload_file($kode_biling="",$pajak=""){
       if ($pajak=='01') {
           $data['pajak']=$pajak;
           $data['data']=$this->db->query("SELECT * FROM sptpd_hotel where kode_biling='$kode_biling'")->row();
           $data['lokasi']='upload/file_transaksi/hotel/';
       } else if ($pajak=='02') {
           $data['pajak']=$pajak;
           $data['data']=$this->db->query("SELECT * FROM sptpd_restoran where kode_biling='$kode_biling'")->row();
           $data['lokasi']='upload/file_transaksi/resto/';
       } else if ($pajak=='03') {
           $data['pajak']=$pajak;
           $data['data']=$this->db->query("SELECT * FROM sptpd_hiburan where kode_biling='$kode_biling'")->row();
           $data['lokasi']='upload/file_transaksi/hiburan/';
       } else if ($pajak=='05') {
           $data['pajak']=$pajak;
           $data['data']=$this->db->query("SELECT * FROM sptpd_ppj where kode_biling='$kode_biling'")->row();
           $data['lokasi']='upload/file_transaksi/ppj/';
       } else if ($pajak=='06') {
           $data['pajak']=$pajak;
           $data['data']=$this->db->query("SELECT * FROM sptpd_mineral_non_logam where kode_biling='$kode_biling'")->row();
           $data['lokasi']='upload/file_transaksi/mblb/';
       } else {
           $data['data']=$this->db->query("SELECT * FROM sptpd_parkir where kode_biling='$kode_biling'")->row();
           $data['lokasi']='upload/file_transaksi/parkir/';
           $data['pajak']=$pajak;
       }
       $this->template->load('Welcome/halaman','upload_file',$data);
    }
    function proses_upload_file(){
       $kode_biling=$this->input->post('kode_biling',TRUE);
       $pajak=$this->input->post('pajak',TRUE);

       if ($pajak=='01') {
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name=$kode_biling.'.'.$extensi;
                        $nf='upload/file_transaksi/hotel/'.$img_name;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
           $this->db->query("UPDATE sptpd_hotel SET FILE_TRANSAKSI='$img_name' where kode_biling='$kode_biling'");
           redirect(site_url('Laporan_sptpd/Laporan_sptpd/laporan_hotel'));
       } else if ($pajak=='02') {
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name=$kode_biling.'.'.$extensi;
                        $nf='upload/file_transaksi/resto/'.$img_name;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
           $this->db->query("UPDATE sptpd_restoran SET FILE_TRANSAKSI='$img_name' where kode_biling='$kode_biling'");
           redirect(site_url('Laporan_sptpd/Laporan_sptpd/laporan_restoran'));
       } else if ($pajak=='03') {
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name=$kode_biling.'.'.$extensi;
                        $nf='upload/file_transaksi/hiburan/'.$img_name;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
           $this->db->query("UPDATE sptpd_hiburan SET FILE_TRANSAKSI='$img_name' where kode_biling='$kode_biling'");
           redirect(site_url('Laporan_sptpd/Laporan_sptpd/laporan_hiburan'));
       } else if ($pajak=='05') {
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name=$kode_biling.'.'.$extensi;
                        $nf='upload/file_transaksi/ppj/'.$img_name;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
           $this->db->query("UPDATE sptpd_ppj SET FILE_TRANSAKSI='$img_name' where kode_biling='$kode_biling'");
           redirect(site_url('Laporan_sptpd/Laporan_sptpd/laporan_ppj'));
       } else if ($pajak=='06') {
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name=$kode_biling.'.'.$extensi;
                        $nf='upload/file_transaksi/mblb/'.$img_name;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
           $this->db->query("UPDATE sptpd_mineral_non_logam SET FILE_TRANSAKSI='$img_name' where kode_biling='$kode_biling'");
           redirect(site_url('Laporan_sptpd/Laporan_sptpd/laporan_galian'));
       } else {
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name=$kode_biling.'.'.$extensi;
                        $nf='upload/file_transaksi/mblb/'.$img_name;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
           $this->db->query("UPDATE sptpd_parkir SET FILE_TRANSAKSI='$img_name' where kode_biling='$kode_biling'");
           redirect(site_url('Laporan_sptpd/Laporan_sptpd/laporan_parkir'));
           
       }
    }

    function Formulir_Pengajuan()
    {
        $this->template->load('Welcome/halaman','formulir_pengajuan/Formulir_pengajuan_list');
    
    }

    public function create_formulir() 
    {
        $data = array(
            'button'             => 'Simpan',
            'action'             => site_url('Master/pokok/create_action_formulir'),
            //'lparent'          => $this->Mmenu->ListParent(),
            'NPWPD'              => set_value('NPWPD'),
            'NAMA_WP'            => set_value('NAMA_WP'),
            'NAMA_USAHA'         => set_value('NAMA_USAHA'),
            'EMAIL_TEMPAT_USAHA' => set_value('EMAIL_TEMPAT_USAHA'),
    );
        $data['jenis']=$this->db->query("SELECT id_inc,nama_usaha from tempat_usaha where npwpd=''")->result();  
        $this->template->load('Welcome/halaman','formulir_pengajuan/Formulir_pengajuan_form',$data);
    }

    function get_nama_usaha(){
        $data1=$_POST['npwp'];
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT id_inc,nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$data1."'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('formulir_pengajuan/get_nama_usaha',$data);
    }

    function create_action_formulir()
    {
        $NPWPD   = $_POST['NPWPD'];
        $NAMA_WP = $_POST['NAMA_WP'];
        $USAHA   = explode("|", $_POST['NAMA_USAHA']);
        $EMAIL   = $_POST['EMAIL_TEMPAT_USAHA'];
        $NAMA_USAHA = $USAHA[0];
        $ID_USAHA = $USAHA[1];



        $data = array(
            'NPWPD'              => $NPWPD,
            'NAMA_WP'            => $NAMA_WP,
            'NAMA_TEMPAT_USAHA'  => $NAMA_USAHA,
            'ID_TEMPAT_USAHA'    => $ID_USAHA,
            'EMAIL_TEMPAT_USAHA' => $EMAIL );

        
        $query = $this->db->insert('MOBILE_T_TEMPAT_USAHA', $data);

        if($query){
            $this->session->set_flashdata('message', '<script>
            $(window).load(function(){
            swal("Berhasil Tambah ", "", "success")
            });
            
            </script>');

             redirect(site_url('Master/pokok/Formulir_Pengajuan'));
        }
    }


    public function aktivasi_formulir($id="") 
    {
        $NPWPD = substr($id, 0, 12);
        $ID_TEMPAT_USAHA = substr($id, 12);

        $query = $this->db->query("SELECT * FROM MOBILE_T_TEMPAT_USAHA WHERE NPWPD = '$NPWPD' AND ID_TEMPAT_USAHA='$ID_TEMPAT_USAHA'")->row();

        $data = array(
            'button'             => 'Aktivasi',
            'action'             => site_url('Master/pokok/create_aktivasi'),
            //'lparent'          => $this->Mmenu->ListParent(),
            'NPWPD'              => set_value('NPWPD', $query->NPWPD),
            'NAMA_WP'            => set_value('NAMA_WP', $query->NAMA_WP),
            'NAMA_USAHA'         => set_value('NAMA_USAHA', $query->NAMA_TEMPAT_USAHA),
            'ID_USAHA'           => set_value('ID_USAHA', $query->ID_TEMPAT_USAHA),
            'EMAIL_TEMPAT_USAHA' => set_value('EMAIL_TEMPAT_USAHA', $query->EMAIL_TEMPAT_USAHA),
    );

        $this->template->load('Welcome/halaman','formulir_pengajuan/Aktivasi_form',$data);
    }

   function create_aktivasi()
   {
        $KODE_AKTIVASI   = $_POST['KODE_AKTIVASI'];
        $EMAIL_TO        = $_POST['EMAIL_TEMPAT_USAHA'];
        $ID_TEMPAT_USAHA = $_POST['ID_USAHA'];
        $NPWPD           = $_POST['NPWPD'];
        $TGL_AKTIVASI = date('Y-m-d H:i:s');

        $cek = $this->db->query("SELECT ID_PENGGUNA FROM MOBILE_T_PENGGUNA WHERE USERNAME='$NPWPD' AND ID_TEMPAT_USAHA='$ID_TEMPAT_USAHA'")->result();

        $data = array(
            'KODE_AKTIVASI'   => $KODE_AKTIVASI,
            'USERNAME'        => $NPWPD,
            'PASSWORD'        => '123',
            'ID_TEMPAT_USAHA' => $ID_TEMPAT_USAHA );

        if(count($cek)>0){
            foreach ($cek as $a) {
                # code...
            }
            $this->db->set('TGL_AKTIVASI',"SYSDATE",false);
            $this->db->set('KODE_AKTIVASI',$KODE_AKTIVASI,false);
            $this->db->where('ID_PENGGUNA', $a->ID_PENGGUNA);
            $query_insert = $this->db->update("MOBILE_T_PENGGUNA");

            $this->db->query("UPDATE MOBILE_T_TEMPAT_USAHA SET EMAIL_TEMPAT_USAHA='$EMAIL_TO' WHERE NPWPD='$NPWPD' AND ID_TEMPAT_USAHA='$ID_TEMPAT_USAHA'");
        }else{
            $this->db->set('TGL_AKTIVASI',"SYSDATE",false);
            $query_insert = $this->db->insert('MOBILE_T_PENGGUNA', $data);

            $this->db->query("UPDATE MOBILE_T_TEMPAT_USAHA SET EMAIL_TEMPAT_USAHA='$EMAIL_TO' WHERE NPWPD='$NPWPD' AND ID_TEMPAT_USAHA='$ID_TEMPAT_USAHA'");
        }
        
        if($query_insert){
        
            $from_email = "hambasahaya26@gmail.com"; 
            $to_email = $EMAIL_TO; 

             $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => 465,
                    'smtp_user' => $from_email,
                    'smtp_pass' => 'Khoirul@6',
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
            );

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");   

             $this->email->from($from_email, 'Bapenda'); 
             $this->email->to($to_email);
             $this->email->subject('Kode Aktivasi Formulir Pengajuan Pemasangan Aplikasi Mobile'); 
             $this->email->message('Kode Aktivasi untuk pemasangan aplikasi mobile : '. $KODE_AKTIVASI); 

             //Send mail 
             if($this->email->send()){
                $this->session->set_flashdata('message', '<script>
                $(window).load(function(){
                swal("Kode Aktivasi Berhasil dibuat ! Silahkan cek email. ", "", "success")
                });
                    
                </script>');

                redirect(site_url('Master/pokok/Formulir_Pengajuan'));
             }else {
                $this->session->set_flashdata('message', '<script>
                $(window).load(function(){
                swal("Kode Aktivasi gagal dibuat", "", "success")
                });
                    
                </script>');

                redirect(site_url('Master/pokok/Formulir_Pengajuan'));
             } 
        }

        
   }


   public function import(){
        $data = array('action' => site_url('Laporan/importaction'), );
        $this->template->load('Welcome/halaman','Master/import_form',$data);
    }
    function importaction(){
        include_once ( APPPATH."/libraries/Excel_reader.php");
        if(!empty($_FILES['fileimport']['name']) && !empty($_FILES['fileimport']['tmp_name'])){ 
             $target = basename($_FILES['fileimport']['name']) ;
             move_uploaded_file($_FILES['fileimport']['tmp_name'], $target);

              chmod($_FILES['fileimport']['name'],0777);
              $data  = new Spreadsheet_Excel_Reader($_FILES['fileimport']['name'],false);
              $baris = $data->rowcount($sheet_index=0);
              $no    =1;
              
                 
              $this->db->query("DELETE FROM rekon_temp WHERE user_import='$this->nip'");
              for($i=2; $i<=$baris; $i++){
                    $this->db->set('CABANG',$data->val($i,1));
                    $this->db->set('KODE_BILING',$data->val($i, 2));
                    $this->db->set('NAMA',$data->val($i, 3));
                    $this->db->set('POKOK',$data->val($i, 4));
                    $this->db->set('DENDA',$data->val($i, 5));
                    $this->db->set('TANGGAL',"to_date('".$data->val($i, 6)."','yyyymmdd')",false );
                    $this->db->set('NO_REF',$data->val($i, 7));
                    $this->db->set('PENGESAHAN',$data->val($i, 8));
                    $this->db->set('USER_BANK',$data->val($i, 9));
                    $this->db->set('JENIS_PAJAK',$data->val($i, 10));
                    $this->db->set('DATE_INSERT',"SYSDATE",false);
                    $this->db->set('USER_IMPORT',$this->nip);
                    $this->db->insert('REKON_TEMP');


            }

            $arr['record']=$baris-1;
            unlink($_FILES['fileimport']['name']);
            //echo $this->db->last_query();
            $this->template->load('Welcome/halaman','Master/import_preview',$arr);

        //}
    }
    }
    function prosesinsertimport($ku=""){
        if(!empty($this->session->userdata('NIP'))){
            $berhasil=0;
            $gagal=0;
           
           $temp = $this->db->query("insert into rekon (CABANG, KODE_BILING, NAMA, POKOK, DENDA, TANGGAL, NO_REF, PENGESAHAN, USER_BANK, JENIS_PAJAK, DATE_INSERT, USER_IMPORT) 
select CABANG, KODE_BILING, NAMA, POKOK, DENDA, TANGGAL, NO_REF, PENGESAHAN, USER_BANK, JENIS_PAJAK, sysdate, USER_IMPORT from rekon_temp where user_import='$this->nip'");
    $notifs="<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> ".$berhasil." Data berhasil di import</div>";
    $this->session->set_flashdata('notifs',$notifs);
 
        $sess=array(
                'gagal'=>$gagal,
                'berhasil'=>$berhasil,
                );
        $this->session->set_userdata($sess);      
            redirect('Pokok/Perforasi');
        }
    }

    public function mstarget_bapenda()
    {
        $this->template->load('Welcome/halaman','target_list_bapenda');
    } 
    
    public function json_target_bapenda() {
        header('Content-Type: application/json');
        echo $this->Mpokok->json_target_bapenda();
    }
       
    public function create_target_bapenda() 
    {
        $data = array(
            'button'     => 'Tambah Data Target',
            'action'     => site_url('Master/pokok/create_action'),
            //'lparent'    => $this->Mmenu->ListParent(),
            'ID_INC'     => set_value('ID_INC'),
            'TAHUN_PAJAK'=> set_value('TAHUN_PAJAK'),
            'REKENING'   => set_value('REKENING'),
            'JENIS_PAJAK'=> set_value('JENIS_PAJAK'),
            'TARGET'     => set_value('TARGET'),
    );
        $this->template->load('Welcome/halaman','target_form',$data);
    }
    
    public function create_action_target_bapenda() 
    {       
            // $data['ID_INC']=$this->input->post('ID_INC');
            $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
            //$data['REKENING']=$this->input->post('REKENING');
            $data['JENIS_PAJAK']=$this->input->post('JENIS_PAJAK');
            $data['TARGET']=$this->input->post('TARGET');
            $this->Mpokok->insert($data); 
            redirect(site_url('Master/pokok/Mstarget'));
 
    }
    
    public function update_target_bapenda($id) 
    {
        $id=rapikan($id);
        $row = $this->db->query("select * from target_bapenda where id_inc='$id'")->row();

        if ($row) {
            $data = array(
                'button'      => 'Edit Data',
                'action'      => site_url('Master/pokok/update_action_target_bapenda'),
                //'lparent'    =>$this->Mmenu->ListParent(),
                'ID_INC'    => set_value('ID_INC',$row->ID_INC),
                'TAHUN_PAJAK' => set_value('TAHUN_TARGET',$row->TAHUN_TARGET),
                'REKENING' => set_value('REKENING',$row->REKENING),
                'JENIS_PAJAK'    => set_value('JENIS_PAJAK',$row->JENIS_PAJAK),
                'TARGET'    => set_value('TARGET',$row->TARGET),
        );
            $this->template->load('Welcome/halaman','target_bapenda_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
             redirect(site_url('Master/pokok/mstarget_bapenda'));
        }
    }
    
    public function update_action_target_bapenda() 
    {
        //$data['ID_INC']=$this->input->post('ID_INC');
        $this->db->set('TAHUN_TARGET', $this->input->post('TAHUN_PAJAK',TRUE));
        $this->db->set('REKENING', $this->input->post('REKENING',TRUE));
        $this->db->set('JENIS_PAJAK', $this->input->post('JENIS_PAJAK',TRUE));
        $this->db->set('TARGET', $this->input->post('TARGET',TRUE));
        $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
        $this->db->update('TARGET_BAPENDA');
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('Master/pokok/mstarget_bapenda'));
        //echo $this->db->last_query();
    }

    public function Log(){
        $param    = urldecode($this->input->get('param', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            /*'tahun'       => $tahun,
            'bulan'       => $bulan,*/
            'param'       => $param,
        );
        $this->session->set_userdata($sess);
        if ($param <> '') {
            $config['base_url']  = base_url() . 'Master/pokok/Log?param=' .urlencode($param);
            $config['first_url'] = base_url() . 'Master/pokok/Log?param=' .urlencode($param);
        } else {
            $config['base_url']  = base_url() . 'Master/pokok/Log';
            $config['first_url'] = base_url() . 'Master/pokok/Log';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mpokok->total_rows_manage_log(/*$tahun,$bulan,*/$param);
        $log                = $this->Mpokok->get_limit_data_manage_log($config['per_page'], $start /*$tahun,$bulan*/,$param);
        //$data['link']= $config['base_url']  = base_url() . 'Master/pokok/pembatalan_kode_biling?tahun=' .urlencode($param);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Log Registrasi dan Payment virtual Account',
            'log'         => $log,
         /*   'tahun'             => $tahun,
            'bulan'             => $bulan,*/
            'param'             => $param,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
      $this->session->set_userdata($sess);
      $this->template->load('Welcome/halaman','log',$data);

  }
  
    
}   

