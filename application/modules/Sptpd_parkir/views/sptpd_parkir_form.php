       <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
      <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
      <a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="" method="post" enctype="multipart/form-data" >
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content" >  
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" />   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option <?php if(!empty($MASA_PAJAK)){ if($mp==$MASA_PAJAK){echo "selected";}} else {if($mp==date("m")){echo "selected";}}?> value="<?php echo $mp?>"><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <input type="text" <?php echo $disable?> id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun Pajak" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>">                
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD  <sup>*</sup> 
                      </label>
                      <div class="col-md-9">
                        <input type="text" id="NPWPD" <?php echo $disable?> name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" onchange="NamaUsaha(this.value);">                  
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <input type="text" id="NAMA_WP" <?php echo $disable?> name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>">                
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Alamat WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <input type="text" id="ALAMAT_WP" rows="3" <?php echo $disable?> class="form-control" placeholder="Alamat" name="ALAMAT_WP" value="<?php echo $ALAMAT_WP; ?>">
                      </div>
                    </div>
                    <?php if ($disable=='disabled') { ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" required id="NAMA_USAHA" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <option  value="<?php echo $NAMA_USAHA?>"><?php echo $NAMA_USAHA ?></option>
                        </select>
                        <!-- <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>> -->               
                      </div>
                    </div>  
                <?php } else { ?>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" required id="NAMA_USAHA" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <?php foreach($jenis as $jns){ ?>
                          <option <?php if($jns->NAMA_USAHA==$NAMA_USAHA){echo "selected";}?> value="<?php echo $jns->NAMA_USAHA?>"><?php echo $jns->NAMA_USAHA ?></option>
                          <?php } ?> 
                        </select>
                        <!-- <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>> -->               
                      </div>
                    </div>                      
                <?php } ?> 
                    <input type="hidden" id="split_tempat_usaha">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="pengenaan(this.value)" style="font-size:11px" id="DASAR_PENGENAAN" <?php echo $disable?> name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <option  <?php if($DASAR_PENGENAAN=='Karcis'){echo "selected";}?> value="Karcis">Karcis</option>
                          <option  <?php if($DASAR_PENGENAAN=='Omzet'){echo "selected";}?> value="Omzet">Omzet</option>
                          <option  <?php if($DASAR_PENGENAAN=='Ketetapan'){echo "selected";}?> value="Ketetapan">Ketetapan</option>
                        </select>
                      </div>
                    </div>            
                  </div>
                </div>
                <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                          <div class="col-md-8">
                          <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                          </div>
                </div>
                <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                          <div class="col-md-8">
                          <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE1" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                          </div>
                </div>
                <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                          <div class="col-md-8">
                          <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE2" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                          </div>
                </div>  
              </div>
              <div class="col-md-6 col-sm-6">
                  <div class="x_title">
                    <h4><i class="fa fa-desktop"></i> Data Wajib Pajak </h4>
                    <div class="clearfix"></div>
                  </div>
                  <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Golongan</b></td>
                          <td><span id="DESKRIPSI"><?php if ($disable=='disabled') {echo "$NAMA_GOLONGAN";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Alamat </b></td>
                          <td><span id="ALAMAT_USAHA"><?php if ($disable=='disabled') {echo "$ALAMAT_USAHA";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kecamatan </b></td>
                          <td><span id="NAMAKEC"><?php if ($disable=='disabled') {echo "$NAMA_KECAMATAN";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kelurahan</b></td>
                          <td><span id="NAMAKELURAHAN"><?php if ($disable=='disabled') {echo "$NAMA_KELURAHAN";} ?></span></td>
                        </tr>                       
                      </tbody>
                    </table>
                    <input type="hidden" id="V_ALAMAT_USAHA" name="ALAMAT_USAHA" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >  
                    <input type="hidden" id="V_KODEKEC" name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="V_KODEKEL" name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >   
                    <input type="hidden" id="GOLONGAN" name="GOLONGAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >   
                    <div id="non_karcis"></div>
                    <?php if ($disable=='disabled' AND $DASAR_PENGENAAN!='Karcis') {?>
                <!-- isiiiiiiiiiiiiiiii -->
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-desktop"></i> Data Parkir  </h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <!-- konten -->
                    <div class="form-group">
                      <div class="col-md-12">
                        <table  width="100%"  border="0" cellspacing="0" cellpadding ="0"   class="table table-bordered" >
                          <thead>
                            <tr>
                              <th width="12%">Jenis</th>
                              <th width="11%">Jml Lembar</th>
                              <th width="16%">Nominal</th>
                              <th width="20%">Penerimaan</th>
                              <th>Catatan</th>
                              <th><i class="fa fa-trash"></i></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $tot=0;foreach($karcis as $karcis){ ?>
                            <tr>
                              <td align="center"><?php echo $karcis->JENIS_PARKIR; ?></td>
                              <td align="right"><?php echo number_format($karcis->JUMLAH_LEMBAR,'0','','.'); ?></td>
                              <td align="right"><?php echo number_format($karcis->NOMINAL,'0','','.'); ?></td>
                              <td align="right"><?php echo number_format($karcis->PENERIMAAN,'0','','.'); ?></td>
                              <td align="center"><?php echo $karcis->CATATAN; ?></td>
                              <td align="center">-</td>
                            </tr>
                            <?php $tot+=$karcis->PENERIMAAN;} ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Karcis <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PENERIMAAN_KARCIS" id="PENERIMAAN_KARCIS" required class="col-md-7 col-xs-12 form-control" value="<?= number_format($tot,'0','','.')?>" <?php echo $disable?> >
                        </div>
                      </div>
                    </div
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Non Karcis <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PENERIMAAN_NON_KARCIS" onchange='get(this);' id="PENERIMAAN_NON_KARCIS" required class="col-md-7 col-xs-12 form-control" value="<?php echo $PENERIMAAN_NON_KARCIS ?>" <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">DPP <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="DPP" id="DPP" readonly required class="col-md-7 col-xs-12 form-control" value="<?php echo $DPP ?>" <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Pajak Terutang <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PAJAK_TERUTANG" id="PAJAK_TERUTANG" required class="col-md-7 col-xs-12 form-control" value="<?php echo $PAJAK_TERUTANG ?>" <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                     <!--  <label class="control-label col-md-3 col-sm-3 col-md-xs-12">SSPD <sup>*</sup></label> -->
                      <div class="col-md-9">
                        <input type="hidden" name="SSPD" id="SSPD" required class="col-md-7 col-xs-12 form-control" value="<?php echo $SSPD ?>" <?php echo $disable?> >
                      </div>
                    </div>
                    <div class="form-group">
                      <!-- <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Rekap Penggunaan BON <sup>*</sup></label> -->
                      <div class="col-md-9">
                        <input type="hidden" name="REKAP_BON" id="REKAP_BON" required class="col-md-7 col-xs-12 form-control" value="<?php echo $REKAP_BON ?>" <?php echo $disable?> >
                      </div>
                    </div>
                    <div class="form-group">
                     <!--  <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Lainya <sup>*</sup></label> -->
                      <div class="col-md-9">
                        <input type="hidden" name="LAINYA" id="LAINYA" required class="col-md-7 col-xs-12 form-control" value="<?php echo $LAINYA ?>" <?php echo $disable?> >
                      </div>
                    </div>
                    <?php if($disable==''){?>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" onClick="return validasi()"><i class="fa fa-save"></i> Simpan</button>
                        <a href="<?php echo site_url('Sptpd_parkir') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                      </div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              <?php } else if ($disable=='disabled') {?>
                <div class="col-md-12">
                        <table  width="100%"  border="0" cellspacing="0" cellpadding ="0"   class="table table-bordered" >
                          <thead>
                            <tr>
                              <th width="12%">KODE KARCIS</th>
                              <th width="11%">Jml Lembar</th>
                              <th width="12%">Nominal</th>
                              <th width="16%">Penerimaan</th>
                              <th width="5%">%</th>
                              <th width="17%">Pajak</th>
                              <th>Catatan</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $tot=0;foreach($karcis as $karcis){ ?>
                            <tr>
                              <td align="center"><?php echo $karcis->JENIS_PARKIR; ?></td>
                              <td align="right"><?php echo number_format($karcis->JUMLAH_LEMBAR,'0','','.'); ?></td>
                              <td align="right"><?php echo number_format($karcis->NOMINAL,'0','','.'); ?></td>
                              <td align="right"><?php echo number_format($karcis->PENERIMAAN,'0','','.'); ?></td>
                              <td align="right"><?php echo $karcis->PERSEN; ?></td>
                              <td align="right"><?php echo number_format($karcis->TAGIHAN_PAJAK,'0','','.'); ?></td>
                              <td align="center"><?php echo $karcis->CATATAN; ?></td>
                            </tr>
                            <?php $tot+=$karcis->PENERIMAAN;} ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Pajak Terutang <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PAJAK_TERUTANG" id="PAJAK_TERUTANG" required class="col-md-7 col-xs-12 form-control" value="<?php echo $PAJAK_TERUTANG ?>" <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
              <?php } else {
                # code...
              };?>
              </div>
              <div class="col-md-12 col-sm-12" id="OPSI">
              <?php error_reporting(E_ALL^(E_NOTICE|E_WARNING));?>
              </div>
             <!--  <div class="col-md-6 col-sm-6" >
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>



</div>

<script>
    function get() {
      var L=parseFloat(nominalFormat(document.getElementById("JUMLAH_LEMBAR").value))|| 0;
      var N=parseFloat(nominalFormat(document.getElementById("NOMINAL").value))|| 0;
      var NON=parseFloat(nominalFormat(document.getElementById("PENERIMAAN_NON_KARCIS").value))|| 0;
      //alert(getNominal(D*H*B*3600/1000));
     /* var P=(L*N).toFixed(0);
      alert(P);
      PENERIMAAN_KARCIS.value= getNominal(P);
      var dpp=parseFloat(P);
      DPP.value= getNominal(dpp);
      PAJAK_TERUTANG.value= getNominal(dpp*0.3);*/
    }
    
    function gantiTitikKoma(angka){
        return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
        return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
      }
    }

  /* JUMLAH_LEMBAR */
  var JUMLAH_LEMBAR = document.getElementById('JUMLAH_LEMBAR');
  JUMLAH_LEMBAR.addEventListener('keyup', function(e)
  {
    JUMLAH_LEMBAR.value = formatRupiah(this.value);
  });
  /* NOMINAL */
  var NOMINAL = document.getElementById('NOMINAL');
  NOMINAL.addEventListener('keyup', function(e)
  {
    NOMINAL.value = formatRupiah(this.value);
  });    
  /* PENERIMAAN_NON_KARCIS */
  var PENERIMAAN_NON_KARCIS = document.getElementById('PENERIMAAN_NON_KARCIS');
  PENERIMAAN_NON_KARCIS.addEventListener('keyup', function(e)
  {
    PENERIMAAN_NON_KARCIS.value = formatRupiah(this.value);
  });
  /* DPP */
  var DPP = document.getElementById('DPP');
  DPP.addEventListener('keyup', function(e)
  {
    DPP.value = formatRupiah(this.value);
  });
  /* PAJAK_TERUTANG */
  var PAJAK_TERUTANG = document.getElementById('PAJAK_TERUTANG');
  PAJAK_TERUTANG.addEventListener('keyup', function(e)
  {
    PAJAK_TERUTANG.value = formatRupiah(this.value);
  });
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
function ribuan (angka)
{
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan  = reverse.match(/\d{1,3}/g);
  ribuan  = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}
  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
    if (NAMA_USAHA==null || NAMA_USAHA==""){
      swal("Nama Usaha Harus di Isi", "", "warning")
      return false;
    };
  }


  
function NamaUsaha(id){
                //alert(id);
                var npwpd=id;
                var jp='07';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
           $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_nama_usaha'?>",
           data: { npwp: npwpd,
                   jp: jp   },
           cache: false,
           success: function(msga){
                 //alert(msga);
                  $("#NAMA_USAHA").html(msga);
                }
              }); 
                
    }     
function alamat(sel)
    {
      if (sel=="") {
        $("#ALAMAT_USAHA").val(""); 
        $("#GOLONGAN").val("");
        $("#MASA").val("");
        $("#PAJAK").val("");
        $("#DESKRIPSI").html("");  
        $("#NAMAKELURAHAN").html("");
        $("#NAMAKEC").html("");    

      } else {
        var nama = sel;
        //alert(nama);
        $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_alamat_usaha'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              //alert(msga);
              $("#split_tempat_usaha").val(nama);
              $("#ALAMAT_USAHA").html(msga);
              $("#V_ALAMAT_USAHA").val(msga);  
            }
          });
         $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_detail_usaha'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              //alert(msga);
              if(msga!=0){
                  var exp = msga.split("|");
                  $("#DESKRIPSI").html(exp[1]);  
                  $("#NAMAKELURAHAN").html(exp[3]);
                  $("#NAMAKEC").html(exp[5]);
                  $("#GOLONGAN").val(exp[0]);
                  $("#V_ID_OP").val(exp[0]);
                  $("#V_KODEKEL").val(exp[2]);
                  $("#V_KODEKEC").val(exp[4]);                  
                  
                 // alert(msga);
                }else{
                  $("#NAMA_WP").val(null);  
                  $("#ALAMAT_WP").val(null); 
                  $("#VNAMA_WP").html("BELUM TERDAFTAR");
                  $("#VALAMAT_WP").html("BELUM TERDAFTAR");
                  
                }
            }
          });   
        } 
           
        

      }




    function pengenaan(aa){
      var pengenaan = aa;
      var nama = document.getElementById("split_tempat_usaha").value;
      var gol =nama.split("|");
      //alert(nama);
      if (pengenaan=='Karcis') {
                $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Sptpd_parkir/Sptpd_parkir/get_data_perforasi'?>",
                     data: { nama: nama},
                     cache: false,
                     success: function(msga){
                            //alert(nama);
                             $("#non_karcis").html('');
                             $('#demo-form2').attr('action', '<?php echo base_url().'Sptpd_parkir/proses_karcis';?>');
                             $("#OPSI").html(msga); 
                          }
                        });
                
      } else if(pengenaan=='Omzet' || pengenaan=='Ketetapan'){
                $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Sptpd_parkir/sptpd_parkir/get_data_non_karcis'?>",
                     data: { nama: nama},
                     cache: false,
                     success: function(msga){
                            //alert(msga);
                             $("#OPSI").html(''); 
                             $('#demo-form2').attr('action', '<?php echo base_url().'Sptpd_parkir/create_action';?>');
                             $("#non_karcis").html(msga);
                            
                          }
                        }); 
                };
                
    }
</script>
