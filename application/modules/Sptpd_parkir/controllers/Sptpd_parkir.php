<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_parkir extends CI_Controller {

    function __construct()
    {
        parent::__construct();
         
        $this->load->model('Msptpd_parkir');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
        $this->load->model('Master/Mpokok');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
	public function index()
	{ 
      if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $npwpd=$_GET['NPWPD'];
                //$dasar_pengenaan=$_POST['DASAR_PENGENAAN'];
        } else {
                $bulan='';//date('n');//date('d-m-Y');
                $tahun='';//date('Y');//date('d-m-Y');
                $npwpd='';
                //$dasar_pengenaan='';
        }
        $sess=array(
                'parkir_bulan'=>$bulan,
                'parkir_tahun'=>$tahun,
                'parkir_npwpd'=>$npwpd
                //'dasar_pengenaan_ppj'=>$dasar_pengenaan
         );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
		$this->template->load('Welcome/halaman','Sptpd_parkir/sptpd_parkir_list',$data);
	}
    public function json() {
        header('Content-Type: application/json');
        echo $this->Msptpd_parkir->json_supplier();
    }
    public function hapus()
    {
            $response = array();
    
    if ($_POST['delete']) {
        
        
        $id = $_POST['delete'];
        $id=rapikan($id);
        $row = $this->Msptpd_parkir->get_by_id($id);
        
        if ($row) {

            $this->db->trans_start();
            // delete karcis
            $this->db->where("SPTPD_PARKIR_ID",$id);
            $this->db->delete("SPTPD_PARKIR_KARCIS");

            // delete PARKIR
            $this->db->where("ID_INC",$id);
            $this->db->delete("SPTPD_PARKIR");

            $this->db->trans_complete();             
            $response['status']  = 'success';
            $response['message'] = 'Data SPTPD Parkir Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
    }
    public function table()
    {
         $data['mp']=$this->Mpokok->listMasapajak();
         $this->load->view('Sptpd_parkir/sptpd_parkir_table',$data);
    }
    public function karcis($x)
    {
         $data['x']=$x;
         $this->load->view('Sptpd_parkir/sptpd_parkir_karcis',$data);
    } 	
    public function create() 
    {
        $data = array(
            'button'                => 'Form SPTPD Parkir',
            'action'                => site_url('Sptpd_parkir/create_action'),
            'mp'                    => $this->Mpokok->listMasapajak(),
            'karcis'                => $this->Msptpd_parkir->getKracis(''),
            'disable'               => '',
            'ID_INC'                => set_value('ID_INC'),
            'MASA_PAJAK'            => set_value('MASA_PAJAK'),
            'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK'),
            'NAMA_WP'               => set_value('NAMA_WP'),
            'ALAMAT_WP'             => set_value('ALAMAT_WP'),
            'NAMA_USAHA'            => set_value('NAMA_USAHA'),
            'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA'),
            'NPWPD'                 => set_value('NPWPD'),
            'PENERIMAAN_KARCIS'     => set_value('PENERIMAAN_KARCIS'),
            'PENERIMAAN_NON_KARCIS' => set_value('PENERIMAAN_NON_KARCIS'),
            'DPP'                   => set_value('DPP'),
            'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG'),
            'SSPD'                  => set_value('SSPD'),
            'REKAP_BON'             => set_value('REKAP_BON'),
            'LAINYA'                => set_value('LAINYA'),
            'DASAR_PENGENAAN'       => set_value('DASAR_PENGENAAN'),
        );
        $this->template->load('Welcome/halaman','Sptpd_parkir/sptpd_parkir_form',$data);
    }
    public function create1() {
        $row=$this->db->query("select nama,npwp,alamat from wajib_pajak where npwp='$this->nip'")->row();
        $data = array(
            'button'                => 'Form SPTPD Parkir',
            'action'                => site_url('Sptpd_parkir/create_action'),
            'mp'                    => $this->Mpokok->listMasapajak(),
            'karcis'                => $this->Msptpd_parkir->getKracis(''),
            'disable'               => '',
            'ID_INC'                => set_value('ID_INC'),
            'MASA_PAJAK'            => set_value('MASA_PAJAK'),
            'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK'),
            'NAMA_WP'               => set_value('NAMA_WP',$row->NAMA),
            'ALAMAT_WP'             => set_value('ALAMAT_WP',$row->ALAMAT),
            'NAMA_USAHA'            => set_value('NAMA_USAHA'),
            'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA'),
            'NPWPD'                 => set_value('NPWPD',$row->NPWP),
            'PENERIMAAN_KARCIS'     => set_value('PENERIMAAN_KARCIS'),
            'PENERIMAAN_NON_KARCIS' => set_value('PENERIMAAN_NON_KARCIS'),
            'DPP'                   => set_value('DPP'),
            'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG'),
            'SSPD'                  => set_value('SSPD'),
            'REKAP_BON'             => set_value('REKAP_BON'),
            'LAINYA'                => set_value('LAINYA'),
            'DASAR_PENGENAAN'       => set_value('DASAR_PENGENAAN'),
            'jenis'                 => $this->db->query("select * from tempat_usaha where npwpd='$this->nip' and jenis_pajak='07'")->result(),
        );
        $this->template->load('Welcome/halaman','Sptpd_parkir/sptpd_parkir_form_wp',$data);
    }
    public function create_action() 
    {
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_1.'.$extensi.',';
                        $nf='upload/file_transaksi/parkir/'.str_replace(',', '', $img_name_ktp);
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                $tmp_file1=$_FILES['FILE1']['tmp_name'];
                $name_file1 =$_FILES['FILE1']['name'];
                if($name_file1!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp1=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_2'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/parkir/'.str_replace(',', '', $img_name_ktp1);
                        move_uploaded_file($tmp_file1,$nf);
                        
                    }
                $tmp_file2=$_FILES['FILE2']['tmp_name'];
                $name_file2 =$_FILES['FILE2']['name'];
                if($name_file2!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file2);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp2=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_3'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/parkir/'.str_replace(',', '', $img_name_ktp2);
                        move_uploaded_file($tmp_file2,$nf);
                        
                }
        $OP=$this->input->post('GOLONGAN');
        $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
        $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
        $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
        $this->db->trans_start();
        $kb=$this->Mpokok->getSqIdBiling();
        $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
        $va=$this->Mpokok->getVA($OP);
        $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
        $nm=$this->input->post('NAMA_WP',TRUE);
        $ed=$expired->ED;
            //  insert hibran
            $this->db->set('ID_TEMPAT_USAHA', $exp[1]);
            $this->db->set('DEVICE','web');
            $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
            $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
            $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
            $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
            $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
            $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
            $this->db->set('NAMA_USAHA',$exp[0]);
            $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
            $this->db->set('NPWPD',$this->input->post('NPWPD'));
            $this->db->set('PENERIMAAN_NON_KARCIS',str_replace('.', '', $this->input->post('PENERIMAAN_NON_KARCIS')));
            $this->db->set('DPP',str_replace('.', '', $this->input->post('DPP')));
            $this->db->set('PAJAK_TERUTANG',str_replace('.', '', $this->input->post('PAJAK_TERUTANG')));
            $this->db->set('SSPD',$this->input->post('SSPD'));
            $this->db->set('REKAP_BON',$this->input->post('REKAP_BON'));
            $this->db->set('LAINYA',$this->input->post('LAINYA'));
            $this->db->set('NIP_INSERT',$this->nip);
            $this->db->set('TGL_INSERT',"sysdate",false);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
            $this->db->set('KODE_REK',$rek->REK);
            $this->db->set('STATUS','0');
            $this->db->set('ID_OP',$OP);
            $this->db->set('TANGGAL_PENERIMAAN',"SYSDATE",false);
            $this->db->set('KODEKEL',$this->input->post('KELURAHAN'));
            $this->db->set('KODEKEC',$this->input->post('KECAMATAN'));
            $this->db->set('FILE_TRANSAKSI',substr_replace($img_name_ktp.$img_name_ktp1.$img_name_ktp2,'','-1'));
         $this->db->set('NO_VA',$va);
              if ($this->role=='8') {
                  $this->db->set('NPWP_INSERT',$this->nip);
              } else {
                  $this->db->set('NIP_INSERT',$this->nip);
              }
            $this->db->insert('SPTPD_PARKIR');
            $this->Mpokok->registration($va,$nm,$pt,$ed);
            // get sptpd_hiburan_id
            $rk=$this->db->query("SELECT MAX(ID_INC) SPTPD_PARKIR_ID
                                    FROM SPTPD_PARKIR
                                    WHERE NIP_INSERT='".$this->nip."' AND IP_INSERT='".$this->Mpokok->getIP()."'")->row();
            // $cek=count();
            if(!empty($_POST['JENIS_PARKIR'])){
                $count=count($_POST['JENIS_PARKIR']);
                for($i=0;$i<$count;$i++){
                    $this->db->set('SPTPD_PARKIR_ID',$rk->SPTPD_PARKIR_ID);
                    $this->db->set('JENIS_PARKIR',$_POST['JENIS_PARKIR'][$i]);
                    $this->db->set('JUMLAH_LEMBAR',str_replace(',', '.', str_replace('.', '',$_POST['JUMLAH_LEMBAR'][$i])));
                    $this->db->set('NOMINAL',str_replace('.', '', $_POST['NOMINAL'][$i]));
                    $this->db->set('PENERIMAAN',str_replace('.', '', $_POST['PENERIMAAN'][$i]));
                    $this->db->set('CATATAN',str_replace('.', '', $_POST['CATATAN'][$i]));
                    $this->db->insert('SPTPD_PARKIR_KARCIS');
                }
            }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE){
            // sukses
            $nt="Berhasil di simpan";
        }else{
            // gagal
            $nt="Gagal di simpan";
        }
        
        $this->session->set_flashdata('message', '<script>
                                              $(window).load(function(){
                                               swal("'.$nt.'", "", "success")
                                              });
                                            </script>');
        if ($this->role=='10') {
            redirect(site_url('Upt/upt'));
        } else {
            redirect(site_url('Sptpd_parkir'));
        }
    }
    public function proses_karcis(){
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_1.'.$extensi.',';
                        $nf='upload/file_transaksi/parkir/'.str_replace(',', '', $img_name_ktp);
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                $tmp_file1=$_FILES['FILE1']['tmp_name'];
                $name_file1 =$_FILES['FILE1']['name'];
                if($name_file1!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp1=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_2'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/parkir/'.str_replace(',', '', $img_name_ktp1);
                        move_uploaded_file($tmp_file1,$nf);
                        
                    }
                $tmp_file2=$_FILES['FILE2']['tmp_name'];
                $name_file2 =$_FILES['FILE2']['name'];
                if($name_file2!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file2);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp2=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_3'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/parkir/'.str_replace(',', '', $img_name_ktp2);
                        move_uploaded_file($tmp_file2,$nf);
                        
                }
        $OP=$this->input->post('GOLONGAN');
        $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
        $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
        $kb=$this->Mpokok->getSqIdBiling();
        $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
        $va=$this->Mpokok->getVA($OP);
        $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
        $nm=$this->input->post('NAMA_WP',TRUE);
        $ed=$expired->ED;
        $this->db->trans_start();
        //  insert hibran
            $pt=str_replace('.', '',$_POST['pajak_terutang']);
            $dpp_k=str_replace('.', '',$_POST['dpp_k']);
        //$this->db->set('KODE_KARCIS', $this->input->post('NPWPD',TRUE));
            $this->db->set('ID_TEMPAT_USAHA', $exp[1]);
            $this->db->set('DEVICE','web');
            $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
            $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
            $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
            $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
            $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
            $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
            $this->db->set('NAMA_USAHA',$exp[0]);
            $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
            $this->db->set('NPWPD',$this->input->post('NPWPD'));
            $this->db->set('PENERIMAAN_NON_KARCIS',str_replace('.', '', $this->input->post('PENERIMAAN_NON_KARCIS')));
            $this->db->set('DPP',array_sum($dpp_k));
            $this->db->set('PAJAK_TERUTANG',array_sum($pt));
            $this->db->set('SSPD',$this->input->post('SSPD'));
            $this->db->set('REKAP_BON',$this->input->post('REKAP_BON'));
            $this->db->set('LAINYA',$this->input->post('LAINYA'));
            $this->db->set('NIP_INSERT',$this->nip);
            $this->db->set('TGL_INSERT',"sysdate",false);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
            $this->db->set('KODE_REK',$rek->REK);
            $this->db->set('STATUS','0');
            $this->db->set('ID_OP',$OP);
            $this->db->set('TANGGAL_PENERIMAAN',"SYSDATE",false);
            $this->db->set('KODEKEL',$this->input->post('KELURAHAN'));
            $this->db->set('KODEKEC',$this->input->post('KECAMATAN'));
            $this->db->set('FILE_TRANSAKSI',substr_replace($img_name_ktp.$img_name_ktp1.$img_name_ktp2,'','-1'));
            $this->db->set('ED_VA',$expired->NOW);
              if ($this->role=='8') {
                  $this->db->set('NPWP_INSERT',$this->nip);
              } else {
                  $this->db->set('NIP_INSERT',$this->nip);
              }
            $this->db->insert('SPTPD_PARKIR');
            $this->Mpokok->registration($va,$nm,$pt,$ed);
           if(!empty($_POST['id_inc'])){
           $nomor=$this->db->query("SELECT MAX(ID_INC) NEXT_VAL FROM SPTPD_PARKIR")->row();
           $NEXT=$nomor->NEXT_VAL;  
           $jumlah= count($_POST['qtt_terjual']); 
                for($i=0; $i < $jumlah; $i++) {
                    $qtt_terjual=str_replace('.', '',$_POST['qtt_terjual'][$i]);
                    $pajak_terutang=str_replace('.', '',$_POST['pajak_terutang'][$i]);

                    $this->db->query("INSERT into SPTPD_PARKIR_KARCIS (sptpd_parkir_id,jenis_parkir,jumlah_lembar,nominal,penerimaan,persen,tagihan_pajak)
                                         select  '$NEXT',nomor_seri,'$qtt_terjual',nilai_lembar,nilai_lembar*'$qtt_terjual',persen,'$pajak_terutang'
                                         from perforasi
                                    where id_inc='".$_POST['id_inc'][$i]."'");
                
            }               
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE){
            // sukses
            $nt="Berhasil di simpan";
        }else{
            // gagal
            $nt="Gagal di simpan";
        }
        //echo $this->db->last_query();
        $this->session->set_flashdata('message', '<script>
                                              $(window).load(function(){
                                               swal("'.$nt.'", "", "success")
                                              });
                                            </script>');
        if ($this->role=='10') {
            redirect(site_url('Upt/upt'));
        } else {
            redirect(site_url('Sptpd_parkir'));
        }
    
}
    public function delete($id) 
    {
        $id=rapikan($id);
        $row = $this->Msptpd_parkir->get_by_id($id);

        if ($row) {
            $this->Msptpd_parkir->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Hapus Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_parkir'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Sptpd/sptpd_parkir'));
        }
    } 
    public function update($id) 
    {
        $id=rapikan($id);
        $row = $this->Msptpd_parkir->get_by_id($id);
        if ($row) {
            $data = array(
                 'button'                => 'Form SPTPD Parkir',
                 'action'                => site_url('Sptpd_parkir/update_action'),
                 'mp'                    => $this->Mpokok->listMasapajak(),
                 'karcis'                => $this->Msptpd_parkir->getKracis($id),
                 'disable'               => '',
                 'ID_INC'                => set_value('ID_INC',$row->ID_INC),
                 'MASA_PAJAK'            => set_value('MASA_PAJAK',$row->MASA_PAJAK),
                 'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
                 'NAMA_WP'               => set_value('NAMA_WP',$row->NAMA_WP),
                 'ALAMAT_WP'             => set_value('ALAMAT_WP',$row->ALAMAT_WP),
                 'NAMA_USAHA'            => set_value('NAMA_USAHA',$row->NAMA_USAHA),
                 'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
                 'NPWPD'                 => set_value('NPWPD',$row->NPWPD),
                 'PENERIMAAN_KARCIS'     => set_value('PENERIMAAN_KARCIS',number_format($row->PENERIMAAN_KARCIS,'0','','.')),
                 'PENERIMAAN_NON_KARCIS' => set_value('PENERIMAAN_NON_KARCIS',number_format($row->PENERIMAAN_NON_KARCIS,'0','','.')),
                 'DPP'                   => set_value('DPP',number_format($row->DPP,'0','','.')),
                 'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
                 'SSPD'                  => set_value('SSPD',$row->SSPD),
                 'REKAP_BON'             => set_value('REKAP_BON',$row->REKAP_BON),
                 'LAINYA'                => set_value('LAINYA',$row->LAINYA),
                 'DASAR_PENGENAAN'       => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
           );

            $this->template->load('Welcome/halaman','Sptpd_parkir/sptpd_parkir_form',$data);

        } else {

            redirect(site_url('Sptpdhiburan/hiburan'));
        }
    } 
        public function update_action() 
    {
         $this->db->trans_start();
            //  insert hibran
            $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
            $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
            $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
            $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
            $this->db->set('NAMA_USAHA',$this->input->post('NAMA_USAHA'));
            $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
            $this->db->set('NPWPD',$this->input->post('NPWPD'));
            $this->db->set('PENERIMAAN_NON_KARCIS',str_replace('.', '', $this->input->post('PENERIMAAN_NON_KARCIS')));
            $this->db->set('DPP',str_replace('.', '', $this->input->post('DPP')));
            $this->db->set('PAJAK_TERUTANG',str_replace('.', '', $this->input->post('PAJAK_TERUTANG')));
            $this->db->set('SSPD',$this->input->post('SSPD'));
            $this->db->set('REKAP_BON',$this->input->post('REKAP_BON'));
            $this->db->set('LAINYA',$this->input->post('LAINYA'));
            $this->db->set('DASAR_PENGENAAN',$this->input->post('DASAR_PENGENAAN'));
            $this->db->where('ID_INC',$this->input->post('ID_INC'));
            $this->db->update('SPTPD_PARKIR');
            
            // hapus karcis lama
            $this->db->query("DELETE FROM SPTPD_PARKIR_KARCIS WHERE SPTPD_PARKIR_ID ='".$this->input->post('ID_INC')."'");

            if(!empty($_POST['JENIS_PARKIR'])){
                $count=count($_POST['JENIS_PARKIR']);
                for($i=0;$i<$count;$i++){
                    $this->db->set('SPTPD_PARKIR_ID',$this->input->post('ID_INC'));
                    $this->db->set('JENIS_PARKIR',$_POST['JENIS_PARKIR'][$i]);
                    $this->db->set('JUMLAH_LEMBAR',$_POST['JUMLAH_LEMBAR'][$i]);
                    $this->db->set('NOMINAL',$_POST['NOMINAL'][$i]);
                    $this->db->set('PENERIMAAN',$_POST['PENERIMAAN'][$i]);
                    $this->db->insert('SPTPD_PARKIR_KARCIS');
                }
            }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE){
            // sukses
            $nt="Berhasil di simpan";
        }else{
            // gagal
            $nt="Gagal di simpan";
        }
        
        $this->session->set_flashdata('message', '<script>
                                              $(window).load(function(){
                                               swal("'.$nt.'", "", "success")
                                              });
                                            </script>');
            redirect(site_url('Sptpd_parkir'));
    }  
             
    public function detail($id) 
    {
        $id=rapikan($id);
        $row = $this->Msptpd_parkir->get_by_id($id);
        if ($row) {
          $NAMA_GOLONGAN = $this->Mpokok->getNamaGolongan($row->ID_OP);
          $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($row->KODEKEC);
          $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($row->KODEKEL,$row->KODEKEC);
            $data = array(
                 'button'                => 'Form SPTPD Parkir',
                 'action'                => site_url('Sptpd_parkir/update_action'),
                 'mp'                    => $this->Mpokok->listMasapajak(),
                 'karcis'                => $this->Msptpd_parkir->getKracis($row->ID_INC),
                 'disable'               => 'disabled',
                 'ID_INC'                => set_value('ID_INC',$row->ID_INC),
                 'MASA_PAJAK'            => set_value('MASA_PAJAK',$row->MASA_PAJAK),
                 'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
                 'NAMA_WP'               => set_value('NAMA_WP',$row->NAMA_WP),
                 'ALAMAT_WP'             => set_value('ALAMAT_WP',$row->ALAMAT_WP),
                 'NAMA_USAHA'            => set_value('NAMA_USAHA',$row->NAMA_USAHA),
                 'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
                 'NPWPD'                 => set_value('NPWPD',$row->NPWPD),
                 'PENERIMAAN_KARCIS'     => set_value('PENERIMAAN_KARCIS',number_format($row->PENERIMAAN_KARCIS,'0','','.')),
                 'PENERIMAAN_NON_KARCIS' => set_value('PENERIMAAN_NON_KARCIS',number_format($row->PENERIMAAN_NON_KARCIS,'0','','.')),
                 'DPP'                   => set_value('DPP',number_format($row->DPP,'0','','.')),
                 'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
                 'SSPD'                  => set_value('SSPD',$row->SSPD),
                 'REKAP_BON'             => set_value('REKAP_BON',$row->REKAP_BON),
                 'LAINYA'                => set_value('LAINYA',$row->LAINYA),
                 'DASAR_PENGENAAN'       => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
                 'NAMA_GOLONGAN'       => $NAMA_GOLONGAN->DESKRIPSI,
                 'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
                 'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN,
           );
            $this->template->load('Welcome/halaman','Sptpd_parkir/sptpd_parkir_form',$data);

        } else {

            redirect(site_url('Sptpdhiburan/hiburan'));
        }
    }
    function pdf_kode_biling_parkir($kode_biling=""){
            $kode_biling=rapikan($kode_biling);
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Sptpd_parkir/sptpd_parkir_pdf',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
             $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }

    function get_data_perforasi(){
            $nama=$_POST['nama'];
            $exp=explode('|', $nama);
           // echo $exp[1]; 
           error_reporting(E_ALL^(E_NOTICE|E_WARNING)); 
            $data['data']=$this->db->query("SELECT * FROM V_CEK_SPTPD_PERFORASI_PARKIR where ID_TEMPAT_USAHA='$exp[1]'")->result();           
            $this->load->view('Sptpd_parkir/v_tabel',$data);
    }  
    function get_data_non_karcis(){
    $data = array(
            'button'                => 'Form SPTPD Parkir',
            'action'                => site_url('Sptpd_parkir/create_action'),
            'mp'                    => $this->Mpokok->listMasapajak(),
            'disable'               => '',
            'ID_INC'                => set_value('ID_INC'),
            'MASA_PAJAK'            => set_value('MASA_PAJAK'),
            'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK'),
            'NAMA_WP'               => set_value('NAMA_WP'),
            'ALAMAT_WP'             => set_value('ALAMAT_WP'),
            'NAMA_USAHA'            => set_value('NAMA_USAHA'),
            'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA'),
            'NPWPD'                 => set_value('NPWPD'),
            'PENERIMAAN_KARCIS'     => set_value('PENERIMAAN_KARCIS'),
            'PENERIMAAN_NON_KARCIS' => set_value('PENERIMAAN_NON_KARCIS'),
            'DPP'                   => set_value('DPP'),
            'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG'),
            'SSPD'                  => set_value('SSPD'),
            'REKAP_BON'             => set_value('REKAP_BON'),
            'LAINYA'                => set_value('LAINYA'),
        );
            $this->load->view('Sptpd_parkir/v_ketetapan_parkir',$data);
    } 
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */