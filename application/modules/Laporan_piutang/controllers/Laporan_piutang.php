<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_piutang extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mlaporan_piutang');
        $this->load->model('Master/Mpokok');
        $this->id_pengguna=$this->session->userdata('INC_USER');
 }
 private function cekAkses($var=null){
        return cek($this->id_pengguna,$var);
}
 function index()
 {
       
       $this->template->load('Welcome/halaman','sptpd_list');
    }
 function laporan_piutang_hotel()
 {      
        $akses =$this->cekAkses(uri_string());
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_hotel';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_hotel';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));              
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $hotel                 = $this->Mlaporan_piutang->get_limit_data_rekap_hotel($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,

            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
        	'title'			   => 'Laporan Piutang Pajak Hotel',
            'hotel'            => $hotel,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
             'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_hotel',$data);
    }
    function laporan_piutang_restoran(){  
        $akses =$this->cekAkses(uri_string());  
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_restoran';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_restoran';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $restoran                 = $this->Mlaporan_piutang->get_limit_data_rekap_restoran($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Pajak Restoran',
            'restoran'            => $restoran,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
             'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_restoran',$data);
    }
     function laporan_piutang_hiburan(){   
        $akses =$this->cekAkses(uri_string()); 
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_hiburan';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_hiburan';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $hiburan                 = $this->Mlaporan_piutang->get_limit_data_rekap_hiburan($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Pajak Hiburan',
            'hiburan'            => $hiburan,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_hiburan',$data);
    }
    function laporan_piutang_reklame(){    
        $akses =$this->cekAkses(uri_string());
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));  
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_reklame';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_reklame';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));  
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $reklame                 = $this->Mlaporan_piutang->get_limit_data_rekap_reklame($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Pajak Reklame',
            'reklame'            => $reklame,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
             'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_reklame',$data);
    }

    function laporan_piutang_ppj(){    
        $akses =$this->cekAkses(uri_string());
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_ppj';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_ppj';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $ppj                 = $this->Mlaporan_piutang->get_limit_data_rekap_ppj($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Pajak PPJ',
            'ppj'            => $ppj,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
             'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_ppj',$data);
    }
    function laporan_piutang_galian(){  
        $akses =$this->cekAkses(uri_string());  
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_galian';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_galian';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));                
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $galian                 = $this->Mlaporan_piutang->get_limit_data_rekap_galian($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Galian C',
            'galian'            => $galian,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_galian',$data);
    }
    function laporan_piutang_parkir(){   
        $akses =$this->cekAkses(uri_string()); 
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));              
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_parkir';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_parkir';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));              
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $parkir                 = $this->Mlaporan_piutang->get_limit_data_rekap_parkir($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Pajak Parkir',
            'parkir'            => $parkir,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
             'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_parkir',$data);
    }
    function laporan_piutang_at(){ 
        $akses =$this->cekAkses(uri_string());  
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_at';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_at';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $at                 = $this->Mlaporan_piutang->get_limit_data_rekap_at($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Pajak Air Tanah',
            'at'            => $at,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_at',$data);
    }
    function laporan_piutang_sb(){   
        $akses =$this->cekAkses(uri_string()); 
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_sb';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_sb';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $sb                 = $this->Mlaporan_piutang->get_limit_data_rekap_sb($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Piutang Pajak Sarang Burung',
            'sb'            => $sb,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_piutang->get_kec();
       $this->template->load('Welcome/halaman','laporan_piutang_sb',$data);
    }
    function getkel(){
        $KODEKEC=$_POST['KODEKEC'];
        $data['kc']=$this->db->query("SELECT KODEKELURAHAN,NAMAKELURAHAN FROM KELURAHAN WHERE KODEKEC ='$KODEKEC' order by NAMAKELURAHAN asc")->result();
        $this->load->view('Laporan_piutang/getkel',$data);
    }
  function laporan_piutang_reklame_detail(){
        $akses =$this->cekAkses(uri_string());    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        //$bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        //$habis_masa=urldecode($this->input->get('habis_masa', TRUE));
        $jns     = urldecode($this->input->get('jns', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&jns='.urlencode($jns));
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&jns='.urlencode($jns));
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&jns='.urlencode($jns));        } else {
            //$bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_piutang/laporan_piutang_reklame_detail';
            $config['first_url'] = base_url() . 'Laporan_piutang/laporan_piutang_reklame_detail';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_piutang_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&jns='.urlencode($jns));        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_piutang->total_rows_rekap_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$jns);
        $reklame                 = $this->Mlaporan_piutang->get_limit_data_rekap_reklame_detail($config['per_page'], $start, $tahun,$kecamatan,$kelurahan,$npwpd,$jns);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            //'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'npwpd'         => $npwpd,
            'jns_reklame'   => $jns
           // 'text'          => $text,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Detail Piutang Pajak Reklame',
            'reklame'            => $reklame,
            'cek'            => $cek,
            //'habis_masa'    =>$habis_masa,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$habis_masa,$jns);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->db->query("SELECT * FROM KECAMATAN WHERE KODEKEC!='01'")->result();
       $this->template->load('Welcome/halaman','laporan_piutang_reklame_detail',$data);
    }

}