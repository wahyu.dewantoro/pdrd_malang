   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan_piutang/laporan_piutang_reklame_detail'?>">
                <input type='hidden' value='1' name='cek'>
                <div class="form-group">
                  <select  name="tahun" required="required" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('h_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                
                <div class="form-group">
                      <select onchange="getkel(this);" name="kecamatan"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('h_kecamatan')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kelurahan"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('h_kelurahan')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control1 col-md-4 col-xs-10" value="<?php echo $this->session->userdata('npwpd');?>" placeholder="nama wp / usaha,teks reklame,npwpd">
                </div>
               <!--  <div class="form-group">
                    <input type="text" name="text"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('text');?>" placeholder="Text Reklame">
                </div> -->
                <!-- <div class="form-group">
                     <input type="checkbox" name="habis_masa" value="1" <?php if ($habis_masa==1) {echo "checked";}?>> Habis masa
                </div> -->
                <div class="form-group">
                      <select  name="jns" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Jenis</option>
                            <option value="2" <?php if($this->session->userdata('jns_reklame')=='2'){echo "selected";}?>>Isidentil</option>
                            <option value="1" <?php if($this->session->userdata('jns_reklame')=='1'){echo "selected";}?>>Permanen</option>
                      </select>
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($cek <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan_piutang/laporan_piutang_reklame_detail'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                     <a href="<?php echo $cetak; ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 100%;"class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Masa</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Lokasi Pasang</th>
              <th class="text-center">KEC</th>
              <th class="text-center">KEL</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">TEKS</th>
              <th class="text-center">P/L</th>
              <th class="text-center">SISI</th>
              <th class="text-center">JUMLAH</th>
              <th class="text-center">TERBIT</th>
              <th class="text-center">AWAL PAJAK</th>
              <th class="text-center">AKHIR PAJAK</th>
              <th class="text-center">KETETAPAN</th>
              <th class="text-center">TGL_PENETAPAN</th>
              <th class="text-center">STATUS</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($reklame as $rk)  { ?>
              <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td><?= $rk->KODE_BILING?></td>
                <td align=""><?= $rk->WAKTU/*$namabulan[$rk->MASA_PAJAK]*/?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->LOKASI_PASANG?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td><?= $rk->TEKS?></td>
                <td><?= number_format($rk->P,1,",",".").' x '.number_format($rk->L,1,",",".")?></td>
                <td align="right"><?=  number_format($rk->S,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH,'0','','.')?></td>
                <td><?= $rk->BERLAKU_MULAI?></td>
                <td><?= $rk->BERLAKU_MULAI?></td>
                <td><?= $rk->AKHIR_PAJAK?></td>
                <td align="right"><?=  number_format($rk->PAJAK_TERUTANG,'0','','.')?></td>
                <td align="center"><?= $rk->TGL_PENETAPAN?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else if ($rk->STATUS=='2'){echo "Belum Ditetapkan";}else{ echo "Belum Bayar";}?></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Total Tagihan : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
    .form-control1 {
    display: block;
    width:253px;
    height: 28px;
    padding: 3px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;}
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

