<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msptpd_mblbm extends CI_Model
{

    public $table = 'SPTPD_MINERAL_NON_LOGAM';
    public $id = 'ID_INC';
    public $order = 'DESC';
    public $KODE_BILING = 'KODE_BILING';

    function __construct()
    {
        parent::__construct();
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }

    // datatables
    function json() {
        $t1 =$this->session->userdata('mlbb_bulan');
        $t2 =$this->session->userdata('mlbb_tahun');
        $cp =$this->session->userdata('mlbb_npwpd');
        if ($this->session->userdata('mlbb_tahun')!='' AND $this->session->userdata('mlbb_bulan')!='' AND $this->session->userdata('mlbb_npwpd')!='') {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND NPWPD LIKE '%$cp%'";
        } else if ($this->session->userdata('mlbb_tahun')!='' AND $this->session->userdata('mlbb_bulan')!='') {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2'";
        } else if ($this->session->userdata('mlbb_tahun')!='' AND $this->session->userdata('mlbb_npwpd')!='') {
            $wh ="TAHUN_PAJAK='$t2' AND NPWPD LIKE '%$cp%'";
        } else if ($this->session->userdata('mlbb_tahun')!='') {
            $wh ="TAHUN_PAJAK='$t2'";
        } else {
            $wh ="TO_CHAR(TGL_INSERT, 'mm')=TO_CHAR(SYSDATE, 'mm') ";
        }
        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,DPP,PAJAK_TERUTANG,STATUS,KODE_BILING,to_char(TGL_BAYAR,'dd/mm/yyyy HH24: MI: SS')  TGL_BAYAR,GETPETUGAS(NIP_INSERT)||getpetugas(NPWP_INSERT)USER_INSERT");
        $this->datatables->from('SPTPD_MINERAL_NON_LOGAM');
        if ($this->role==8) {
            $this->datatables->where("NPWPD='$this->nip'");
        }
        $this->datatables->where($wh);
        $this->db->order_by("TGL_INSERT DESC, TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Sptpd_mblbm/Sptpd_mblbm/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_mblbm/Sptpd_mblbm/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('pembelian/sptpd_hotel/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="delete" data-id="$1" href="javascript:void(0)"').anchor(site_url('Sptpd_mblbm/Sptpd_mblbm/pdf_kode_biling_mblbm/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'acak(KODE_BILING)');
        $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_mblbm/Sptpd_mblbm/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Pdf/pdf/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'acak(KODE_BILING)');
        $this->datatables->add_column('actionc', '<div class="btn-group">'.anchor(site_url('Sptpd_mblbm/Sptpd_mblbm/pdf_kode_biling_mblbm/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'acak(KODE_BILING)');
        return $this->datatables->generate();
    }
 
    // get data by id
    function get_by_id($id)
    {
        return $this->db->query("SELECT ID_INC, MASA_PAJAK, TAHUN_PAJAK, NAMA_WP, ALAMAT_WP, NAMA_USAHA, ALAMAT_USAHA, NPWPD, NO_FORMULIR,TO_CHAR(TANGGAL_PENERIMAAN, 'dd/mm/yyyy') AS TANGGAL_PENERIMAAN, TO_CHAR(BERLAKU_MULAI, 'dd/mm/yyyy') AS BERLAKU_MULAI, KODEKEC, KODEKEL, ID_OP,  JENIS_ENTRIAN, HARGA_JUAL_MINERAL, PENGAMBILAN_MINERAL, MASA, PAJAK, DPP, PAJAK_TERUTANG FROM SPTPD_MINERAL_NON_LOGAM WHERE KODE_BILING='$id'")->row();
    }
  

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->KODE_BILING, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->KODE_BILING, $id);
        $this->db->delete($this->table);
    }


     function getKel($id){
        return $this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$id'")->result();
     }
    function ListJenisMineral(){
        return $this->db->query("SELECT * FROM JENIS_MINERAL ORDER BY ID_INC ASC")->result();
    }
     function getGol(){
        return $this->db->query("SELECT BIDANG,ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF,ANALIS FROM OBJEK_PAJAK WHERE (ID_GOL= 196 OR
ID_GOL= 197 OR
ID_GOL= 199 OR
ID_GOL= 200 OR
ID_GOL= 203 OR
ID_GOL= 204 OR
ID_GOL= 205) AND BIDANG='2' order by id_op")->result();
     }
function getGol_dinas(){
        return $this->db->query("SELECT case WHEN BIDANG = 1 THEN 'Dinas' ELSE 'Upt' END STATUS,ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF,ANALIS,BIDANG FROM OBJEK_PAJAK WHERE (ID_GOL= 196 OR
ID_GOL= 197 OR
ID_GOL= 199 OR
ID_GOL= 200 OR
ID_GOL= 203 OR
ID_GOL= 204 OR
ID_GOL= 205) AND BIDANG='1' OR BIDANG='2' order by bidang,id_op")->result();
     }
    function getTu()
    {
        return $this->db->query("SELECT ID_INC, NAMA_USAHA, ALAMAT_USAHA, JENIS_PAJAK FROM TEMPAT_USAHA WHERE NPWPD='$this->nip' AND JENIS_PAJAK='06' order by JENIS_PAJAK asc")->result();
    }
   /* function jsongalian_detail_temp(){
        $this->datatables->select(" DESKRIPSI,ANALIS_TEMP,VOL_PEKERJAAN AS VOL_PEKERJAAN_TEMP,ID_OP,HARGA_DASAR,PAJAK,VOLUME,I,DPP,PAJAK_TERUTANG,NIP");
        $this->datatables->from('V_MINERAL_TEMP');
        //$this->datatables->join('OBJEK_PAJAK B', 'A.ID_OP = B.ID_OP');
         //$this->datatables->join('KELURAHAN C', 'A.KELURAHAN = C.KODEKELURAHAN AND A.KECAMATAN=C.KODEKEC');
        //$this->datatables->join('OBJEK_PAJAK D', 'A.GOLONGAN = D.ID_OP');
        $this->datatables->where("NIP='00005'");
        //$this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="deletetemp" data-id="$1" href="javascript:void(0)" ').'</div>', 'ID_OP');
        return $this->datatables->generate();
     }*/

     function jsongalian_detail_temp(){
        $this->datatables->select(" DESKRIPSI,ID_INC,ANALIS_TEMP,VOL_PEKERJAAN_TEMP,A.ID_OP,HARGA_DASAR,PAJAK,VOLUME,I,(HARGA_DASAR*VOLUME) AS DPP,PAJAK_TERUTANG,NIP");
        $this->datatables->from('SPTPD_MINERAL_DETAIL_TEMP A');
        $this->datatables->join('OBJEK_PAJAK B', 'A.ID_OP = B.ID_OP');
         //$this->datatables->join('KELURAHAN C', 'A.KELURAHAN = C.KODEKELURAHAN AND A.KECAMATAN=C.KODEKEC');
        //$this->datatables->join('OBJEK_PAJAK D', 'A.GOLONGAN = D.ID_OP');
        $this->datatables->where("NIP='$this->nip'");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="deletetemp" data-id="$1" href="javascript:void(0)" ').'</div>', 'ID_INC');
        return $this->datatables->generate();
     }


}
/*ID_OP=570 OR 
ID_OP=571 OR 
ID_OP=504 OR 
ID_OP=505 OR 
ID_OP=397 OR 
ID_OP=317 OR 
ID_OP=395 OR 
ID_OP=396 OR 
ID_OP=427 OR 
ID_OP=441 OR 
ID_OP=442 OR 
ID_OP=472 */
//OR ID_OP=1215 OR ID_OP=1217 OR ID_OP=1218 OR ID_OP=1219 OR ID_OP=1220 OR ID_OP=1221 OR ID_OP=440 OR ID_OP=439 
/* End of file Mmenu.php */
/* Location: ./application/models/Mmenu.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-08 05:30:09 */
/* http://harviacode.com */

