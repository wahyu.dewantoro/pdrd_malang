  <table id="example" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>No</th>
        <th>Golongan</th>
        <th>Harga Pasar</th>
        <th>Persen</th>
        <th>Analis</th>
        <th>vol Pekerjaan</th>
        <th>Volume(m3)</th>
        <th>Index</th> 
        <th>DPP</th>        
        <th>Pajak Terutang</th>
        <th>Aksi</th>
      </tr>
    </thead>
  </table>
<?php 
$id=$this->session->userdata('NIP');
$row=$this->db->query("SELECT SUM(PAJAK_TERUTANG) AS TOTAL,SUM(VOLUME*HARGA_DASAR) AS DPP FROM SPTPD_MINERAL_DETAIL_TEMP WHERE NIP='$id' GROUP BY  NIP")->row(); ?>
<div class="form-group">
  <label class="control-label col-md-3 col-xs-12 col-md-offset-7">Jumlah Total Pajak Terutang </label>
  <div class="col-md-2 ">
    <div class="input-group">
      <span class="input-group-addon">Rp</span>
      <input type="hidden" name='DPP1' value="<?php if(isset($row->DPP)) {echo number_format($row->DPP,'1',',','.');} else {echo "0";} ?>">
      <input readonly="" name='PAJAK_TERUTANG1' type="text" class="form-control col-md-2 col-xs-12 " style="text-align:right;" value="<?php if(isset($row->TOTAL)) {echo number_format($row->TOTAL,'1',',','.');} else {echo "0";} ?>" >
    </div>
  </div>
</div>
<script type="text/javascript">

    $(document).on('click', '#deletetemp', function(e){
      
      var id = $(this).data('id');
      deltemp(id);
      e.preventDefault();
    });
  function deltemp(id){
    
    swal({
      title: 'Apakah Anda Yakin?',
      text: "Data Akan dihapus Permanen!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!',
      showLoaderOnConfirm: true,
        
      preConfirm: function() {
        return new Promise(function(resolve) {
             
           $.ajax({
            url: '<?php echo base_url()?>Sptpd_mblbm/Sptpd_mblbm/hapustemp',
            type: 'POST',
              data: 'delete='+id,
              dataType: 'json'
           })
           .done(function(response){
            swal('Deleted!', response.message, response.status);
          listgalian();
           })
           .fail(function(){
            swal('Oops...', 'Something went wrong with ajax !', 'error');
           });
        });
        },
      allowOutsideClick: false        
    }); 
    
  }

  $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };
    $.fn.dataTable.ext.errMode = 'throw';
    var t = $("#example").dataTable({
      initComplete: function() {
        var api = this.api();
        $('#mytable_filter input')
        .off('.DT')
        .on('keyup.DT', function(e) {
          if (e.keyCode == 13) {
            api.search(this.value).draw();
          }
        });
      },
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": false,
          "bAutoWidth": false,
      'oLanguage':
      {
       
      },
      processing: true,
      serverSide: true,
      ajax: {"url": "<?php echo base_url()?>Sptpd_mblbm/Sptpd_mblbm/jsongalian_detail_temp", "type": "POST"},
      columns: [
      {"data":"ID_INC"},
      {"data":"DESKRIPSI"},
      {
        "data":"HARGA_DASAR",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      {"data":"PAJAK","className" : "text-right"},
      {
        "data":"ANALIS_TEMP",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 2, '' )
      },
      {"data":"VOL_PEKERJAAN_TEMP","className" : "text-right","render": $.fn.dataTable.render.number( '.', ',', 2, '' )},

      {"data":"VOLUME","className" : "text-right"},
      {"data":"I"},
      {
        "data":"DPP",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      {
        "data":"PAJAK_TERUTANG",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      {"data":"action"},
      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>