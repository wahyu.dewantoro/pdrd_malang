
      <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
    <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
      <a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>  
  <div class="clearfix"></div>   
  <div class="row">

    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >        
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="col-md-6">
                      <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-3">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($list_masa_pajak as $list_masa_pajak){ ?>
                            <option <?php if(!empty($MASA_PAJAK)){ if($list_masa_pajak==$MASA_PAJAK){echo "selected";}} else {if($list_masa_pajak==date("m")){echo "selected";}}?> value="<?php echo $list_masa_pajak?>"><?php echo $namabulan[$list_masa_pajak] ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <label class="control-label col-md-1 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                        </label>
                        <div class="col-md-3 col-sm-7 col-xs-12">
                          <input style="font-size: 11px" type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                
                        </div>
                      </div>                                
                      <div class="form-group">
                        <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                        </label>
                        <div class="col-md-9 col-sm-7 col-xs-12">
                          <input type="text" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" <?php echo $disable ?> onchange="NamaUsaha(this.value);">                  
                        </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" id="NAMA_USAHA" name="NAMA_USAHA" required class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <?php foreach($jenis as $jns){ ?>
                          <option <?php if($jns->NAMA_USAHA==$NAMA_USAHA){echo "selected";}?> value="<?php echo $jns->NAMA_USAHA.'|'.$jns->ID_INC?>"><?php echo $jns->NAMA_USAHA ?></option>
                          <?php } ?> 
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                          <div class="col-md-8">
                          <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                          </div>
                    </div>  
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP<sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <input type="text" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>" <?php echo $disable ?>>                
                        </div>
                      </div> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  WP<sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <textarea style="font-size: 11px" <?php echo $disable ?> id="ALAMAT_WP" rows="3" class="resizable_textarea form-control" placeholder="Alamat Wajib Pajak" name="ALAMAT_WP"><?php echo $ALAMAT_WP; ?></textarea>                
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>>               
                        </div>
                      </div> --> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <textarea style="font-size: 11px" <?php echo $disable ?> id="ALAMAT_USAHA" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                        </div>
                      </div>  
                    </div> 
                  </div>
                </div>
              </div> 

            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-5">           
                    
                    <div class="form-group">
                      <label class="control-label col-md-4 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-5">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="control-label col-md-4 col-xs-12"> Pekerjaan   <sup>*</sup></label>
                      <div class="col-md-8">
                        <div class="input-group">
                          <span class="input-group-addon"></span>
                          <input type="text" name="PEKERJAAN" class="form-control col-md-7 col-xs-12" value="" required <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                    
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" required <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select  style="font-size:11px" id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  required placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                  <?php if($button==('Update SPTPD Hotel'||'Form SPTPD Hiburan')){?>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php }} ?>                          
                        </select>
                      </div>
                    </div> 
                    <?php
                    $persen = "var persen = new Array();\n";
                    $masa = "var masa = new Array();\n";
                    $tarif = "var tarif = new Array();\n";
                    $analis = "var analis = new Array();\n";
                    $bidang = "var bidang = new Array();\n";
                    ?> 
                    
                </div> 
                   <script type="text/Javascript">
function checkDec(el){
 var ex = /[0-9]+\.?[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
}
</script>               
                <div class="col-md-12">
                  <table class="table table-striped table-bordered" border="1">
                    <thead>
                      <tr>
                        <th width="199">Golongan</th>            
                        <th width="130">Harga Pasar</th>
                        <th width="70">Persen</th>
                        <th width="70">Analis</th>
                        <th width="70">Vol Pekerjaan</th>
                        <th width="120">Volume (m<sup>3</sup>)</th>
                        <th width="50">Index</th>
                        <th width="110">DPP</th>
                        <th width="130">Pajak</th>
                        <!-- <th width="10">Aksi</th> -->
                      </tr>
                    </thead>
                    <?php if ($disable=='disabled') {?>
                    <?php $no=1;$tot=0;foreach ($detail as $rk_d)  { ?>
                    <tbody id="kontentdiag">
                      <tr>
                        <td><?= $rk_d->DESKRIPSI?></td> 
                        <td align="right"><?= number_format($rk_d->HARGA_DASAR,'0','','.')?></td>
                        <td align="right"><?= number_format($rk_d->PAJAK,'0','','.')?></td> 
                        <td></td>
                        <td align="right"><?= number_format($rk_d->VOLUME,'0','','.')?></td>
                        <td align="right"><?= number_format($rk_d->I,'0','','.')?></td> 
                        <td align="right"><?= number_format($dpp=$rk_d->HARGA_DASAR*$rk_d->VOLUME,'0','','.')?></td>
                        <td align="right"><?= number_format($dpp*$rk_d->PAJAK/100,'0','','.')?></td>
                        <!-- <td><span class="fa fa-trash"></span></td> -->
                      </tr>
                      </tbody>
                      <?php }?>
                    <?php  
                    } else {?>
                    <tbody id="kontentdiag">
                    <?php $count=2;
                for($i=1;$i<$count;$i++){?>
                      <tr>
                        <td><select onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN" <?php echo $disable?> name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                          <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo '('.$GOLONGAN->STATUS.') '.$GOLONGAN->DESKRIPSI ?></option>
                          <?php 
                          $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                          $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                          $tarif .= "tarif['" . $GOLONGAN->ID_OP . "'] = {tarif:'".addslashes(number_format($GOLONGAN->TARIF,'0','','.'))."'};\n";
                           $analis .= "analis['" . $GOLONGAN->ID_OP . "'] = {analis:'".addslashes(number_format($GOLONGAN->ANALIS,4))."'};\n";
                           $bidang .= "bidang['" . $GOLONGAN->ID_OP . "'] = {bidang:'".addslashes($GOLONGAN->BIDANG)."'};\n";
                        } ?>
                      </select>
                        </td>
                          <script type="text/javascript">  
                            <?php echo $persen;echo $masa;echo $tarif;echo $analis;echo $bidang;?>             
                            function changeValue(id){
                              //var aa= document.getElementById('BIDANG').value
                              document.getElementById('PAJAK').value = persen[id].persen;
                              document.getElementById('MASA').value = masa[id].masa;
                              document.getElementById('HARGA_JUAL_MINERAL').value = tarif[id].tarif;
                              document.getElementById('ANALIS').value = analis[id].analis;
                              var aa=document.getElementById('BIDANG').value = bidang[id].bidang;
                              //alert(aa);
                              DPP.value ="";
                              PAJAK_TERUTANG.value="";
                              PENGAMBILAN_MINERAL.value="";
                              V_P.value="";
                              if (id==440) {
                                   alert('Khusus Jenis Marmer 0.714m3 = 1 ton/ 60.000');
                              } else {

                              }
                              if (aa==2) {
                                  document.getElementById("V_P").readOnly = true;
                                  document.getElementById("PENGAMBILAN_MINERAL").readOnly = false;
                              } else {
                                  document.getElementById("V_P").readOnly = false;
                                  document.getElementById("PENGAMBILAN_MINERAL").readOnly = true;
                              }
                              
                            }
                          </script> 
                        <td><input  onchange='get(this);' <?php echo $disable ?> type="text" id="HARGA_JUAL_MINERAL" name="HARGA_JUAL_MINERAL" placeholder="Harga Jual Mineral" class="form-control col-md-7 col-xs-12" value="<?php echo $HARGA_JUAL_MINERAL; ?>" ></td>
                        <td><input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"  <?php echo $disable?> placeholder="Pajak" onchange='get(this);'></td>
                        <td><input readonly="" type="text" name="ANALIS" id="ANALIS" class="form-control col-md-7 col-xs-12" <?php echo $disable?> placeholder="Analis" onchange='get(this);'></td>
                         <td><input onchange='get(this);' onkeyup="checkDec(this);" <?php echo $disable ?> type="text" id="V_P" name="V_P"  placeholder="V" class="form-control col-md-7 col-xs-12"></td>
                        <td><input <?php echo $disable ?> type="text"  id="PENGAMBILAN_MINERAL" name="PENGAMBILAN_MINERAL"   placeholder="Volume" onchange='get(this);' class="form-control col-md-7 col-xs-12" value="<?php echo $PENGAMBILAN_MINERAL; ?>" ></td>
                        <td><input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"  <?php echo $disable?> placeholder="" ></td>
                        <td><input <?php echo $disable ?> type="text" id="DPP" name="DPP"  readonly placeholder="DPP" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>"></td>
                        <td><input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                </td>
                        <input type="hidden" id="BIDANG">
                        <!-- <td><span class="fa fa-trash"></span></td> -->
                      </tr>
                      <tr>
                        <td colspan="9"><font color="red" style='font-size: 9px;'>* Gunakan Tanda (.) 'titik' jika volume terdapat angka pecahan </font></td>
                      </tr>
                      </tbody>
                      <?php
                }?>
                    <?php }?>
                </table>
                <!-- <a class="btn btn-xs btn-success" id="tambahdiag"><i class="fa fa-plus"></i> Tambah</a> -->
                <a class="btn btn-xs btn-success" id="tambah" ><i class="fa fa-plus"></i> Tambah</a>
                <?php if ($disable=='') {?>
                  <div id="templist"></div>
                <?php
                } else {?>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">DPP <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="DPP1" id="DPP1" readonly required class="col-md-7 col-xs-12 form-control" value="<?php echo $DPP ?>" <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Pajak Terutang <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PAJAK_TERUTANG1" onchange='get(this);' value="<?php echo $PAJAK_TERUTANG ?>" id="PENERIMAAN" required class="col-md-7 col-xs-12 form-control" value="" <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                <?php }?>
                    <?php if($disable==''){?>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                        <a href="<?php echo site_url('Sptpd_mblbm/sptpd_mblbm') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                      </div>
                    </div>
                    <?php } ?>  
                </div>
              </div>
            </div>     
          </div>
            </form>
          </div>
        </div>
      </div>



    </div>

    <script>

      function listgalian(){
        $('#templist').load("<?php echo base_url().'Sptpd_mblbm/Sptpd_mblbm/templist'?>"); 
      }
       $(document).ready(function(){
   listgalian(); /* it will load products when document loads */
   
   $(document).on('click', '#tambah', function(e){
    var vol=document.getElementById("PENGAMBILAN_MINERAL").value;
    var gol=document.getElementById("GOLONGAN").value;
    if (gol==null || gol=="") {
      swal("Pilih Golongan Terlebih Dulu", "", "warning")
      return false;
    } else if (vol==null || vol==""){
      swal("Volume Harus Diisi", "", "warning")
      return false;
    } else{
      tambah();
    };
  });

 });
      function tambah(KECAMATAN,BERLAKU_MULAI){
        var data = $('#demo-form2').serialize();
        $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpd_mblbm/Sptpd_mblbm/tambah_dinas'?>",
         data: data
            }).done(function(response){
                swal('Data Ditambah', response.message, response.status);
              listgalian();
               })
               .fail(function(){
                swal('Oops...', 'Isikan data dengan Benar hapus penggunaan tanda petik satu', 'error');
               });;         
         GOLONGAN.value='';
         HARGA_JUAL_MINERAL.value='';
         PAJAK.value='';
         PENGAMBILAN_MINERAL.value='';
         DPP.value='';
         MASA.value='';
         PAJAK_TERUTANG.value='';
         V_P.value='';
      }



      function NamaUsaha(id){
                //alert(id);
                var npwpd=id;
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
        $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpd_mblbm/Sptpd_mblbm/get_nama_usaha'?>",
           data: { npwp: npwpd},
           cache: false,
           success: function(msga){
                  //alert(npwp);
                  $("#NAMA_USAHA").html(msga);
                }
              }); 
                
    }
    function alamat(sel)
    {
      var nama = sel;
      //alert(nama);
      if (nama=='') {
          $("#ALAMAT_USAHA").val(""); 
      } else{
           $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_alamat_usaha'?>",
           data: { nama: nama},
           cache: false,
           success: function(msga){
                //alert(msga);
                $("#ALAMAT_USAHA").val(msga); 
              }
            }); 
              
      };   
    }
      function get(id) {
        var harga= parseFloat(document.getElementById("PENGAMBILAN_MINERAL").value)|| 0;
        var bid= parseFloat(document.getElementById("BIDANG").value);
        var hasil= parseFloat(document.getElementById("HARGA_JUAL_MINERAL").value.replace(/\./g,''))|| 0;
        var pajak= parseFloat(document.getElementById("PAJAK").value.replace(/\./g,''))/100|| 0;
        var analis= parseFloat(document.getElementById("ANALIS").value)|| 0;
        var vol_p= parseFloat(document.getElementById("V_P").value)|| 0;
        var vol=Math.ceil(analis*vol_p);
        if (bid==1) {
            var dpp=vol*hasil;
            PENGAMBILAN_MINERAL.value= getNominal(vol);
        } else {
            var dpp=hasil*harga;
        }
        
        var pjk=dpp*pajak;

        DPP.value = getNominal(dpp);
        PAJAK_TERUTANG.value = getNominal(dpp*pajak);
        var P=pjk.toFixed(0);
        DPP1.value= getNominal(dpp);
        PENERIMAAN.value= getNominal(P);


        //
              
      }
//PENGAMBILAN_MINERAL 
/*var PENGAMBILAN_MINERAL = document.getElementById('PENGAMBILAN_MINERAL'),
numberPattern = /^[0-9]+$/;*/

/*var V_P = document.getElementById('V_P'),
numberPattern = /^[0-9]+$/;*/

/*PENGAMBILAN_MINERAL.addEventListener('keyup', function(e) {
  PENGAMBILAN_MINERAL.value = formatRupiah(PENGAMBILAN_MINERAL.value);
});*/
//HARGA_JUAL_MINERAL 
var HARGA_JUAL_MINERAL = document.getElementById('HARGA_JUAL_MINERAL'),
numberPattern = /^[0-9]+$/;

HARGA_JUAL_MINERAL.addEventListener('keyup', function(e) {
  HARGA_JUAL_MINERAL.value = formatRupiah(HARGA_JUAL_MINERAL.value);
});
 /* DPP */
/*  var DPP = document.getElementById('DPP1');
  DPP.addEventListener('keyup', function(e)
  {
    DPP.value = formatRupiah(this.value);
  });*/
  /* PAJAK_TERUTANG */
/*  var PAJAK_TERUTANG = document.getElementById('PAJAK_TERUTANG1');
  PAJAK_TERUTANG.addEventListener('keyup', function(e)
  {
    PAJAK_TERUTANG.value = formatRupiah(this.value);
  });*/
    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
  var x=1;
  $("#tambahdiag").click(function(){
    x++;
                $.get("karcis/"+x, function (data) {
                    $("#kontentdiag").append(data);
                });
 });

    $('#kontentdiag').on('click', '.remove_project_file', function(e) {
      x--;
    e.preventDefault();
    $(this).parents(".barisdiag").remove();
  });


    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
</script>


<script type="text/javascript">
function stockdatabase(id,value){
    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'<?php echo base_url()?>capotik/hargaobatlangsung_khusus',
      data:{id:value},      
      success:function(response){
        var ty=parseFloat(response);
        $('#harga'+id).attr('value',ty);
        var abc=$('#obatbiaya').val();
        
          $('#trxdetail_jumlah_'+id).keyup(function() {
            var jum=$('#trxdetail_jumlah_'+id).val();
            if(jum!=''){
              var df=jum;
            }else{
              var df=0;
            }
            var cv=(parseFloat(response) * parseFloat(df)) + parseFloat(abc);

            $('#obatbiaya').attr('value',cv);
            $('#qty'+id).attr('value',parseFloat(response) * parseFloat(df));
            /*$('#racikan').keyup(function() {

                var opx=$('#obatbiaya').val();
                var op=$('#racikan').val();
                if(op!=''){
                  var opp=op;
                }else{
                  var opp=0;
                }
                 var tb=parseFloat(opp)+parseFloat(opx);
                 $('#total').attr('value',tb);
               });*/
          });

         
      }
    }
    );
  }
       function aa(sel){
      var ext = $('#my_file_field').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
          alert('Format File Tidak Didukung!');
          $('#my_file_field').val('');
      }
    }
  

uploadField.onchange = function() {
    if(this.files[0].size > 2500000){
       alert("File Terlalu Besar!");
       this.value = "";
    };
};
</script>
