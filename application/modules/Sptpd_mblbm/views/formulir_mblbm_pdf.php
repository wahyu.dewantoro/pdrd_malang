<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SSPD-BPHTB</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 11px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
</style>
</head>

<body>
<div class="teacherPage">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" border="1">
  <tr>
    <td width="26%" rowspan="5" align="center" valign="top"><img width=93 height=100 src="assets/img/mlg.jpeg" v:shapes="Picture_x0020_1"></td>
    <td width="45%" align="center" valign="top">PEMERINTAH KABUPATEN MALANG</td>
    <td width="29%" rowspan="5" align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top"><strong>BADAN PENDAPATAN DAERAH</strong></td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top">JL. Raden Panji No. 158, Telp. (0341) 390683</td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top">Kepanjen - 65163</td>
  </tr>
  <tr>
    <td height="4" align="center" valign="top">Perda Kabupaten Malang Nomor 8 Tahun 2010</td>
  </tr>
</table>
<table width="98%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td height="70"><p align="center"><strong>SURAT PEMBERITAHUAN PAJAK DARAH (SPTPD)</strong></p>
      <p align="center"><strong>PAJAK MINERAL BUKAN LOGAN DAN BATUAN</strong></p>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="22%"><div align="center">Masa Pajak :</div></td>
          <td width="78%"><?= $namabulan[$wp->MASA_PAJAK]?></td>
        </tr>
        <tr>
          <td><div align="center">Tahun Pajak :</div></td>
          <td><?= $wp->TAHUN_PAJAK?></td>
        </tr>
      </table></td>
    <td width="49%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2"><div align="center">Kepada :</div></td>
        <td width="78%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2"><div align="center"></div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="5%">&nbsp; </td>
        <td colspan="2">Yth. Sdr. Kepala Badan Pendapatan Daerah</td>
        </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
        <td>Kabupaten Malang</td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
        <td>di</td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
        <td>Malang</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="14" colspan="2"><strong>Perhatian :</strong></td>
  </tr>
  <tr>
    <td height="70" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="3%"><div align="center">1.</div></td>
        <td>Harap diisi dalam rangkap 3 dan ditulis dengan huruf CETAK</td>
      </tr>
      <tr>
        <td><div align="center">2.</div></td>
        <td> Beri tanda X pada kotak yang tersedia sesuai dengan klasifikasinya</td>
      </tr>
    </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="3%"><div align="center">3.</div></td>
          <td>Setelah diisi dan ditanda tangani harap diserahkan kembali kepada UPTD Pendapatan setempat. </td>
        </tr>
        <tr>
          <td><div align="center"></div></td>
          <td>Atau langsung ke teempat pembayaran disetiap kota kecamatan. Sesuai dengan wilayah usahanya selambat lambatnya tanggal 10 pada bulan berikutnya.</td>
        </tr>
        <tr>
          <td><div align="center">4.</div></td>
          <td>Keterlambatan penyampaian SPTPD ini dikenakan sanksi sesuai dengan ketentuan yang berlaku.</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td height="14" colspan="2"><strong> I. Identitas Wajib Pajak</strong>
    </td>
  </tr>
  <tr>
    <td height="50" colspan="2" align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="3%"  class="border_bottom"><div align="center">a.</div></td>
        <td width="23%" class="border_bottom">Nama Wajib Pajak</td>
        <td class="border_bottom"><?= $wp->NAMA_WP?></td>
        </tr>
      <tr>
        <td  class="border_bottom"><div align="center">b.</div></td>
        <td  class="border_bottom">Alamat Wajib Pajak</td>
        <td  class="border_bottom"><?= $wp->ALAMAT_WP?></td>
        </tr>
      <tr>
        <td  class="border_bottom"><div align="center">c.</div></td>
        <td  class="border_bottom">Nama Tempat Usaha</td>
        <td  class="border_bottom"><?= $wp->NAMA_USAHA?></td>
        </tr>
      <tr>
        <td  class="border_bottom"><div align="center">d.</div></td>
        <td  class="border_bottom">Alamat Tempat Usaha</td>
        <td  class="border_bottom"><?= $wp->ALAMAT_OP?></td>
        </tr>
      <tr>
        <td  class="border_bottom"><div align="center">e</div></td>
        <td  class="border_bottom"><strong>NPWPD</strong></td>
        <td  class="border_bottom"><?= $wp->NPWPD?></td>
      </tr>
      </table></td>
  </tr>
  <tr>
    <td colspan="2"><strong>II. Diisi oleh Wajib Pajak</strong></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="border_bottom"><div align="center">a.</div></td>
        <td class="border_bottom" colspan="3">Jenis bahan mineral</td>
        <td class="border_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td class="border_bottom"><div align="center"></div></td>
        <td class="border_bottom" colspan="3">
          <table>
            <?php $tot=0;foreach($detail as $r){ ?>
              <tr>
                <td align="center"></td>
                <td align="center"><?php echo $r->NOREK; ?></td>
                <td align=""><?php echo $r->DESKRIPSI?></td>
                <td align="right"><?php echo number_format($r->VOLUME,'0','','.'); ?> x </td>
                <td align="right"><?php echo number_format($r->HARGA_DASAR,'0','','.'); ?> /</td>
                <td align="right"><?php echo number_format($r->PAJAK,'0','','.'); ?> %</td>
                <td align="right"> = <?php echo number_format($r->PAJAK_TERUTANG,'0','','.'); ?></td>
              </tr>
            <?php $tot+=$r->PAJAK_TERUTANG;} ?>
            <tr>
             <td class="border_bottom" colspan="6"><strong>Pajak yang terutang </strong> </td>
             <td class="border_bottom" align="right"><?php echo number_format($wp->PAJAK_TERUTANG,'0','','.'); ?></td>
            </tr>
          </table>
        </td>
      </tr>
       <!-- <tr>
        <td width="3%"><div align="center">a.</div></td>
        <td colspan="4">Jenis bahan mineral</td>
        </tr>
     <tr>
        <td><div align="center"></div></td>
        <td width="3%"><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td width="6%"><div align="center">06-04</div></td>
        <td width="16%">Batu Kapur</td>
        <td width="72%">&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center"></div></td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td><div align="center">06-06</div></td>
        <td>Batu gunung/belah/kali</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center"></div></td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td><div align="center">06-08</div></td>
        <td>Pasir</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center"></div></td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td><div align="center">06-09</div></td>
        <td>Tanah Urug</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td><div align="center">06-10</div></td>
        <td>Kerikil</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td><div align="center">06-40</div></td>
        <td>Phiropilite</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td>&nbsp;</td>
        <td>Bentonite</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td>&nbsp;</td>
        <td>Zeolite</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="70%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
        <td>&nbsp;</td>
        <td>Lainya</td>
        <td>&nbsp;</td>
      </tr> -->
  <!--     <tr>
        <td class="border_bottom"><div align="center">b</div></td>
        <td class="border_bottom" colspan="3">Jumlah pengambilan bahan mineral</td>
        <td class="border_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td class="border_bottom"><div align="center">c</div></td>
        <td class="border_bottom" colspan="3">Harga jual bahan mineral</td>
        <td class="border_bottom">&nbsp;</td>
      </tr>
      <tr>
        <td class="border_bottom"><div align="center">d</div></td>
        <td class="border_bottom" colspan="3">Dasar Pengenaan Pajak (<strong>DPP</strong>) =(bxc)</td>
        <td class="border_bottom">&nbsp;</td>
      </tr> -->
     <!--  <tr>
        <td class="border_bottom"><div align="center">b.</div></td>
        <td class="border_bottom" colspan="3"><strong>Pajak yang terutang </strong> </td>
        <td class="border_bottom"><?php echo number_format($wp->PAJAK_TERUTANG,'0','','.'); ?></td>
      </tr> -->
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><strong>III. Data Pendukung dilampirkan</strong></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="5%"><div align="center">a.</div></td>
        <td width="49%">Surat Setoran Pajak Daerah (SSPD)</td>
        <td width="3%" align="center">&nbsp;</td>
        <td width="43%">&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center">b.</div></td>
        <td>Laporan produksi/daftar pengiriman</td>
        <td align="center">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div align="center">c.</div></td>
        <td>Lainya</td>
        <td align="center">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><div align="center"><strong>PERNYATAAN WAJIB PAJAK</strong></div></td>
  </tr>
  <tr>
    <td height="68" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>Yang bertanda tangan dibawah ini :</td>
        </tr>
      <tr>
        <td>Nama : <?= $wp->NAMA_WP?></td>
        </tr>
      <tr>
        <td>Menyatakan dengan sebenarnya bahwa Surat Pemberitahuan Pajak Daerah (SPTPD) ini kami/saya isi dengan jelas,</td>
      </tr>
      <tr>
        <td>benar, lengkap dan tak bersyarat. Apabila kemudian hari ditemukan data selain tersebut di atas, kami bersedia </td>
      </tr>
      <tr>
        <td>untuk menerima sanksi sesuai ketentuan perundang-undangan pajak daerah.</td>
      </tr>
  <tr>
    <td height="161" colspan="2"><table width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td height="146" colspan="2">&nbsp;</td>
        <td width="34%" align="center">&nbsp;</td>
        <td width="33%" align="center"><p>Malang, <br />
          Yang menyatakan Wajib Pajak / Kuasa</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>(<span class="a"><?= $wp->NAMA_WP?>)</span></p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="21" colspan="2">Catatan :</td>
  </tr>
</table>
</div>
</body>
</html>
