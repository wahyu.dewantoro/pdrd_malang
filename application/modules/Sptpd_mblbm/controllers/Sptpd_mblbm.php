<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_mblbm extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Msptpd_mblbm');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->load->model('Master/Mpokok');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
    public function index()
    {
        if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $npwpd=$_GET['NPWPD'];
        } else {
                $bulan='';//date('n');//date('d-m-Y');
                $tahun='';//date('Y');//date('d-m-Y');
                $npwpd='';
                //$dasar_pengenaan='';
        }
        $sess=array(
                'mlbb_bulan'=>$bulan,
                'mlbb_tahun'=>$tahun,
                'mlbb_npwpd'=>$npwpd
                //'dasar_pengenaan_ppj'=>$dasar_pengenaan
         );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','Sptpd_mblbm/sptpd_mblbm_list',$data);
  }
  public function json() {
    header('Content-Type: application/json');
    echo $this->Msptpd_mblbm->json();
}
public function hapus()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $row = $this->Msptpd_mblbm->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_mblbm->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data SPTPD Mineral Bukan Logam dan Batuan Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}
 public function tambah()
    {
       $response = array();
       $GOLONGAN=$this->input->post('GOLONGAN');
       $HARGA_JUAL_MINERAL=str_replace('.', '',$this->input->post('HARGA_JUAL_MINERAL'));
       $PAJAK=$this->input->post('PAJAK');
       $PENGAMBILAN_MINERAL=$this->input->post('PENGAMBILAN_MINERAL');
       $MASA=$this->input->post('MASA');
       //$DPP=$this->input->post('DPP');
       $PAJAK_TERUTANG=str_replace('.', '',$this->input->post('PAJAK_TERUTANG'));
       $rek=$this->db->query("select  getNoRek('$GOLONGAN')REK from dual")->row();
       $sql=$this->db->query("INSERT INTO SPTPD_MINERAL_DETAIL_TEMP (ID_OP,HARGA_DASAR,PAJAK,VOLUME,I,PAJAK_TERUTANG,NIP,NOREK) VALUES ('$GOLONGAN','$HARGA_JUAL_MINERAL','$PAJAK','$PENGAMBILAN_MINERAL','$MASA','$PAJAK_TERUTANG','$this->nip','$rek->REK')");
       if ($sql) {  
         $response['status']  = 'success';
        $response['message'] = 'Data Ditambah';
    } else {
        $response['status']  = 'error';
        $response['message'] = 'Unable to delete product ...';
    }
    echo json_encode($response);
}
public function tambah_dinas()
    {
       $response = array();
       $GOLONGAN=$this->input->post('GOLONGAN');
       $HARGA_JUAL_MINERAL=str_replace('.', '',$this->input->post('HARGA_JUAL_MINERAL'));
       $PAJAK=$this->input->post('PAJAK');
       $PENGAMBILAN_MINERAL=$this->input->post('PENGAMBILAN_MINERAL');
       $MASA=$this->input->post('MASA');
       $ANALIS=$this->input->post('ANALIS');
       $V_P=$this->input->post('V_P');
       $PAJAK_TERUTANG=str_replace('.', '',$this->input->post('PAJAK_TERUTANG'));
       $rek=$this->db->query("select  getNoRek('$GOLONGAN')REK from dual")->row();
       $sql=$this->db->query("INSERT INTO SPTPD_MINERAL_DETAIL_TEMP (ID_OP,HARGA_DASAR,PAJAK,VOLUME,I,PAJAK_TERUTANG,NIP,NOREK,ANALIS_TEMP,VOL_PEKERJAAN_TEMP) VALUES ('$GOLONGAN','$HARGA_JUAL_MINERAL','$PAJAK','$PENGAMBILAN_MINERAL','$MASA','$PAJAK_TERUTANG','$this->nip','$rek->REK','$ANALIS','$V_P')");
       if ($sql) {  
         $response['status']  = 'success';
        $response['message'] = 'Data Ditambah';
    } else {
        $response['status']  = 'error';
        $response['message'] = 'Unable to delete product ...';
    }
    echo json_encode($response);
}

public function hapustemp()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $row=$this->db->query("DELETE FROM SPTPD_MINERAL_DETAIL_TEMP WHERE ID_INC='$id'");
        
        if ($row) {
            $response['status']  = 'success';
            $response['message'] = 'Data Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
} 
public function templist()
{
    $this->load->view('mblbm_detail_temp');
}
function jsongalian_detail_temp(){
 header('Content-Type: application/json');
 echo $this->Msptpd_mblbm->jsongalian_detail_temp();
}

public function table()
{
    $data['mp']=$this->Mpokok->listMasapajak();
   $this->load->view('Sptpd_mblbm/sptpd_mblbm_table',$data);
}	
public function create() 
{
    
    if ($this->role==8) {
            $data = array(
        'button'                       => 'Form SPTPD Mineral Bukan Logam dan Batuan',
        'action'                       => site_url('Sptpd_hotel/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'tu'                           => $this->Msptpd_mblbm->getTu(),

    );           
    $this->template->load('Welcome/halaman','Wp/beranda/beranda_form',$data);
    } else if($this->role==10){
    $data = array(
        'button'                       => 'Form SPTPD Mineral Bukan Logam dan Batuan',
        'action'                       => site_url('Sptpd_mblbm/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'HARGA_JUAL_MINERAL'           => set_value('HARGA_JUAL_MINERAL'),
        'PENGAMBILAN_MINERAL'          => set_value('PENGAMBILAN_MINERAL'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_mblbm->getGol(),
    );
            
    $this->template->load('Welcome/halaman','Sptpd_mblbm/sptpd_mblbm_form',$data);
}else{
    $data = array(
        'button'                       => 'Form SPTPD Mineral Bukan Logam dan Batuan',
        'action'                       => site_url('Sptpd_mblbm/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'HARGA_JUAL_MINERAL'           => set_value('HARGA_JUAL_MINERAL'),
        'PENGAMBILAN_MINERAL'          => set_value('PENGAMBILAN_MINERAL'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_mblbm->getGol_dinas(),
    );        
    $this->template->load('Welcome/halaman','Sptpd_mblbm/sptpd_mblbm_form_dinas',$data);
}
}
public function create_action() 
{
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'.'.$extensi;
                        $nf='upload/file_transaksi/mblb/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
    $this->db->trans_start();
    $kb=$this->Mpokok->getSqIdBiling();
    $kv=$this->Mpokok->getSqIdVirtual();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG1')));
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;
    $va="1800760019".$kv;
    
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    $this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    //$this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('PEKERJAAN', $this->input->post('PEKERJAAN',TRUE));
    //$this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    //$this->db->set('PENGAMBILAN_MINERAL',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGAMBILAN_MINERAL'))));
    //$this->db->set('HARGA_JUAL_MINERAL',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_JUAL_MINERAL'))));
    //$this->db->set('MASA', $this->input->post('MASA',TRUE));
    //$this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP1'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG1'))));
    $this->db->set('STATUS', 0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('KODE_BILING', "'3507600'||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('FILE_TRANSAKSI',$img_name_ktp);
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
if ($this->role=='8') {
    $this->db->set('NPWP_INSERT',$this->nip);
} else {
    $this->db->set('NIP_INSERT',$this->nip);
}

    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->insert('SPTPD_MINERAL_NON_LOGAM');
    $this->Mpokok->registration($va,$nm,$pt,$ed); 
    //$insert_id = $this->db->insert_id();
    $rk=$this->db->query("SELECT MAX(ID_INC) LAST_ID
                                    FROM SPTPD_MINERAL_NON_LOGAM
                                    WHERE NIP_INSERT='".$this->nip."' AND IP_INSERT='".$this->Mpokok->getIP()."'")->row();
    $this->db->query("INSERT INTO SPTPD_MINERAL_DETAIL (ID_OP,HARGA_DASAR,PAJAK,VOLUME,I,PAJAK_TERUTANG,NIP,NOREK,MINERAL_ID,ANALIS_DETAIL,VOL_PEKERJAAN_DETAIL) 
    select ID_OP,HARGA_DASAR,PAJAK,VOLUME,I,PAJAK_TERUTANG,NIP,NOREK,'$rk->LAST_ID',ANALIS_TEMP,VOL_PEKERJAAN_TEMP from  SPTPD_MINERAL_DETAIL_TEMP where nip='$this->nip'");
    $this->db->query("DELETE FROM SPTPD_MINERAL_DETAIL_TEMP WHERE NIP='$this->nip'"); 
    /*if(!empty($_POST['GOLONGAN'])){
                $count=count($_POST['GOLONGAN']);
                for($i=0;$i<$count;$i++){
                    $op=$_POST['GOLONGAN'][$i];
                    $rek=$this->db->query("select  getNoRek('$op')REK from dual")->row();
                    $this->db->set('ID_OP',$_POST['GOLONGAN'][$i]);
                    $this->db->set('HARGA_DASAR',str_replace('.', '', $_POST['HARGA_JUAL_MINERAL'][$i]));
                    $this->db->set('PAJAK',str_replace('.', '', $_POST['PAJAK'][$i]));
                    $this->db->set('VOLUME',$_POST['PENGAMBILAN_MINERAL'][$i]);
                    $this->db->set('I',str_replace('.', '', $_POST['MASA'][$i]));
                    $this->db->set('PAJAK_TERUTANG',str_replace('.', '', $_POST['PAJAK_TERUTANG'][$i]));
                    $this->db->set('MINERAL_ID',$rk->LAST_ID);
                    $this->db->set('NOREK',$rek->REK);
                    $this->db->insert('SPTPD_MINERAL_DETAIL');
                }
            }*/
    $this->db->trans_complete();
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Tambah SPTPD Mineral Bukan Logam dan Batuan", "", "success")
     });

     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpd_mblbm'));
    }
    
} 
public function update($id) 
{
    $row = $this->Msptpd_mblbm->get_by_id($id);

    if ($row) {
        $data = array(
            'button'              => 'Update SPTPD Mineral',
            'action'              => site_url('Sptpd_mblbm/update_action'),
            'disable'             => '',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'PENGAMBILAN_MINERAL' => set_value('PENGAMBILAN_MINERAL',number_format($row->PENGAMBILAN_MINERAL,'0','','.')),
            'HARGA_JUAL_MINERAL'  => set_value('HARGA_JUAL_MINERAL',number_format($row->HARGA_JUAL_MINERAL,'0','','.')),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_mblbm->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_mblbm->getGol(),      
        );
        $this->template->load('Welcome/halaman','Sptpd_mblbm/sptpd_mblbm_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
} 
public function update_action() 
{
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('PENGAMBILAN_MINERAL',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGAMBILAN_MINERAL'))));
    $this->db->set('HARGA_JUAL_MINERAL',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_JUAL_MINERAL'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
    $this->db->update('SPTPD_MINERAL_NON_LOGAM');
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Update", "", "success")
     });

     </script>');
    redirect(site_url('Sptpd_mblbm'));
}      
public function detail($id) 
{
    $id=rapikan($id);
    $row = $this->Msptpd_mblbm->get_by_id($id);

    if ($row) {
        $data = array(
            'button'              => 'Detail SPTPD Mineral',
            'action'              => site_url('Sptpd_mblbm/update_action'),
            'disable'             => 'disabled',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'PENGAMBILAN_MINERAL' => set_value('PENGAMBILAN_MINERAL',number_format($row->PENGAMBILAN_MINERAL,'0','','.')),
            'HARGA_JUAL_MINERAL'  => set_value('HARGA_JUAL_MINERAL',number_format($row->HARGA_JUAL_MINERAL,'0','','.')),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_mblbm->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_mblbm->getGol(),
            'detail'              => $this->db->query("select deskripsi,a.* from SPTPD_MINERAL_DETAIL a
                                                        join objek_pajak b on a.id_op=b.id_op where MINERAL_ID='$row->ID_INC'")->result(),        
        );
        $this->template->load('Welcome/halaman','Sptpd_mblbm/sptpd_mblbm_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
} 
function pdf_kode_biling_mblbm1($npwpd=""){
            $npwpd=rapikan($npwpd);
            $data['wp']=$this->db->query("select * from sptpd_hotel where id_inc='$npwpd'")->row();
            $html     =$this->load->view('Sptpd_mblbm/sptpd_mblbm_pdf',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = time().".pdf";
            $this->load->library('M_pdf');
            $this->m_pdf->pdf->WriteHTML($html);
            
            $this->m_pdf->pdf->Output();
    }
function pdf_kode_biling_mblbm($npwpd=""){
            /*$data['wp']=$this->db->query("select * from sptpd_hotel where id_inc='$npwpd'")->row();
            $html     =$this->load->view('Sptpd_mblbm/formulir_mblbm_pdf',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = time().".pdf";
            $this->load->library('M_pdf');
            $this->m_pdf->pdf->WriteHTML($html);
            
            $this->m_pdf->pdf->Output();*/

            //$id  =rapikan($ide);
            $kode_biling=rapikan($npwpd);
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $data['detail']= $this->db->query("select deskripsi,a.*,kode_biling from SPTPD_MINERAL_DETAIL a
                                                        join objek_pajak b on a.id_op=b.id_op
                                                        join sptpd_mineral_non_logam c on C.ID_INC=A.MINERAL_ID where kode_biling='$kode_biling'")->result();
            $file = "formulir_galian_C.pdf";
            $this->load->library('PdfGenerator');
            $html=$this->load->view('Sptpd_mblbm/formulir_mblbm_pdf',$data,true);
            $this->pdfgenerator->generate($html);
    }
    function get_nama_usaha(){
        $data1=$_POST['npwp'];
        
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT id_inc,nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$data1."' and jenis_pajak='06'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    public function karcis($x)
    {
         $data['x']=$x;
         $data['GOLONGAN']=$this->Msptpd_mblbm->getGol(); 
         $this->load->view('Sptpd_mblbm/sptpd_append',$data);
    }
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */