<!-- <style> .label {
         font: normal 12px ;
       }</style>     -->     
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2><?php echo $button; ?></h2>                   
              <div class="clearfix"></div>
            </div>
            <div class="x_content" >
              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >

                <input type="hidden" name="kd_sptpd" value="<?php echo $kd_sptpd; ?>" />   
                <div class="form-group">
                  <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="npwpd" name="npwpd" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $npwpd; ?>">                  
                  </div>
                </div>  
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > No Berkas <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="no_berkas" name="no_berkas" required="required" placeholder="No Berkas" class="form-control col-md-7 col-xs-12" value="<?php echo $no_berkas; ?>">               
                  </div>
                </div>                       
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >No SIPA / SIPPAT <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="no_sipa" name="no_sipa" required="required" placeholder="No SIPA / SIPPAT" class="form-control col-md-7 col-xs-12" value="<?php echo $no_sipa; ?>">                          
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >TMT<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="tmt" name="tmt" required="required" placeholder="TMT" class="form-control col-md-7 col-xs-12" value="<?php echo $tmt; ?>">                  
                  </div>
                </div>  

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama Wajib Pajak<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_wp" name="nm_wp" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_wp; ?>">                
                  </div>
                </div>  
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Nama Perusahaan<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="harga" name="nm_perusahaan" required="required" placeholder="Nama Perusahaan" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_perusahaan; ?>">                     
                  </div>
                </div>  
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat Usaha<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="alamat" rows="3" class="resizable_textarea form-control" placeholder="Alamat" name="alamat"><?php echo $alamat; ?></textarea>                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" >Peruntukan <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">   
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="radio">
                        <input type="radio" class="flat" value="" name="peruntukan"> Non Niaga
                      </div>
                      <div class="radio">
                        <input type="radio" class="flat" value="" name="peruntukan"> Niaga
                      </div>  
                      <div class="radio">
                        <input type="radio" class="flat" value="" name="peruntukan"> Industri Dengan Bahan Baku Air
                      </div>        
                    </div>
                  </div> 
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Bagian Bulan<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="bagian_bulan" name="bagian_bulan" required="required" placeholder="Bagian Bulan" class="form-control col-md-7 col-xs-12" value="<?php echo $bagian_bulan; ?>">                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="tahun" name="tahun" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php echo $tahun; ?>">                
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" >Cara Pengambilan <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">   
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="radio">
                        <input type="radio" class="flat" value="" name="cara_pembaggilan"> Meter
                      </div>
                      <div class="radio">
                        <input type="radio" class="flat" value="" name="cara_pembaggilan"> Non Meter
                      </div>         
                    </div>
                  </div> 
                </div>            

            <div align="center">
              <center>    <h2>Data Pengambilan dan Pemanfaatan Air Tanah</h2></center>
              <div class="clearfix"></div>
            </div>  
<div class="col-md-6">
   <center>    <h2>Non Meter</h2></center>
                <div class="form-group">
                  <label class="control-label col-md-4 col-sm-3 col-xs-12" for="last-name" >Debit Air<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="tmt" name="tmt" required="required" placeholder="TMT" class="form-control col-md-7 col-xs-12" value="<?php echo $tmt; ?>">                  
                  </div>
                </div>  

                <div class="form-group">
                  <label class="control-label col-md-4 col-sm-3 col-xs-12" for="last-name" >Penggunaan 1 Hari<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_wp" name="nm_wp" required="required" placeholder="Penggunaan 1 Hari" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_wp; ?>">                
                  </div>
                </div>  
                <div class="form-group">
                  <label class="control-label col-md-4 col-sm-3 col-xs-12" for="last-name"  >Penggunaan 1 Bulan<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="harga" name="nm_perusahaan" required="required" placeholder="Penggunaan 1 Bulan" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_perusahaan; ?>">                     
                  </div>
                </div>  
</div>
<div class="col-md-6">
   <center>    <h2>Meter</h2></center>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Hari Ini <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="tmt" name="tmt" required="required" placeholder="Hari Ini " class="form-control col-md-7 col-xs-12" value="<?php echo $tmt; ?>">                  
                  </div>
                </div>  

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Bulan Lalu<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_wp" name="nm_wp" required="required" placeholder="Bulan Lalu" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_wp; ?>">                
                  </div>
                </div>  
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Volume Air<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="harga" name="nm_perusahaan" required="required" placeholder="Volume Air" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_perusahaan; ?>">                     
                  </div>
                </div>  
</div>        
 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Catatan<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="catatan" rows="3" class="resizable_textarea form-control" placeholder="Catatan" name="catatan"><?php echo $catatan; ?></textarea>                
                  </div>
                </div>                                
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                    <a href="<?php echo site_url('Sptpd/sptpd_at') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>



      </div>

    <script>

      function validasi(){
        var npwpd=document.forms["demo-form2"]["npwpd"].value;
        var number=/^[0-9]+$/; 
        if (npwpd==null || npwpd==""){
          swal("NPWPD Harus di Isi", "", "warning")
          return false;
        };

      }
    </script>
