<!-- <style> .label {
         font: normal 12px ;
       }</style>     -->     
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2><?php echo $button; ?></h2>                   
              <div class="clearfix"></div>
            </div>
            <div class="x_content" >
              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >

                <input type="hidden" name="kd_sptpd" value="<?php echo $kd_sptpd; ?>" /> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="masa_pajak" name="masa_pajak" required="required" placeholder="Masa Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $masa_pajak; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="tahun" name="tahun" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php echo $tahun; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama Wajib Pajak<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_wp" name="nm_wp" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_wp; ?>">                
                  </div>
                </div> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Wajib Pajak<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="alamat_wp" rows="3" class="resizable_textarea form-control" placeholder="Alamat Wajib Pajak" name="alamat_wp"><?php echo $alamat_wp; ?></textarea>                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Tempat Usaha <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_tmp_usaha" name="nm_tmp_usaha" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_tmp_usaha; ?>">               
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="alamat_usaha" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="alamat_usaha"><?php echo $alamat_usaha; ?></textarea>                
                  </div>
                </div>                                  
                <div class="form-group">
                  <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="npwpd" name="npwpd" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $npwpd; ?>">                  
                  </div>
                </div>
                                                                                                        
          <div class="ln_solid"></div>                  
            <div align="center">
              <center>    <h2>Jenis Hiburan Dengan Menggunakan Karcis</h2></center>
              <div class="clearfix"></div>
            </div>                
                <table class="table table-striped">
                  <tr>
                    <td width="8%" >No </td>
                    <td>Jenis Hiburan</td>
                    <td>Kode Rek</td>
                    <td>Jumlah Lembar</td>
                    <td>Nominal</td>
                    <td>Penerimaan</td>
                  </tr>
                  <tr>
                    <td><input type="text" id="" name="" required="required" placeholder="No" class="form-control col-md-7 col-xs-12" value=""></td>
                    <td><input type="text" id="" name="" required="required" placeholder="Jenis Hiburan" class="form-control col-md-7 col-xs-12" value=""></td>
                    <td><input type="text" id="" name="" required="required" placeholder="Kode Rek" class="form-control col-md-7 col-xs-12" value=""></td>
                    <td><input type="text" id="" name="" required="required" placeholder="Jumlah Lembar" class="form-control col-md-7 col-xs-12" value=""></td>
                    <td><input type="text" id="" name="" required="required" placeholder="Nominal" class="form-control col-md-7 col-xs-12" value=""></td>
                    <td><input type="text" id="" name="" required="required" placeholder="Penerimaan" class="form-control col-md-7 col-xs-12" value=""></td>
                  </tr>

                  <tr>
                    <td colspan="5"><center>Jumlah Penerimaan Hiburan</center></td>
                    <td><input type="text" id="" name="" required="required" placeholder="Penerimaan" class="form-control col-md-7 col-xs-12" value=""></td>
                  </tr>
                </table>                        
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Penerimaan Wahana Non Karcis <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="jml_nonkarcis" name="jml_nonkarcis" required="required" placeholder="Jumlah Penerimaan Wahana Non Karcis" class="form-control col-md-7 col-xs-12" value="<?php echo $jml_nonkarcis; ?>">                
                  </div>
                </div>     
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="dpp" name="dpp" required="required" placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $dpp; ?>">                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Yang Terutang <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="pajak_terutang" name="pajak_terutang" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $pajak_terutang; ?>">                
                  </div>
                </div>  
                                                                                                                         
          <div class="ln_solid"></div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Surat Setoran Pajak Daerah <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="sspd" name="sspd" required="required" placeholder="Surat Setoran Pajak Daerah" class="form-control col-md-7 col-xs-12" value="<?php echo $sspd; ?>">                
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Rekapitulasi Penggunaan Bon / Bill <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="bill" name="bill" required="required" placeholder="Rekapitulasi Penggunaan Bon / Bill" class="form-control col-md-7 col-xs-12" value="<?php echo $bill; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Lainnya <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="kl_byr" name="lainnya" required="required" placeholder="Lainnya" class="form-control col-md-7 col-xs-12" value="<?php echo $lainnya; ?>">                
                  </div>
                </div>                                                                                                        
          <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                    <a href="<?php echo site_url('Sptpd/sptpd_hiburan') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>



      </div>
