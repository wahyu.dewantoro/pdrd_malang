<!-- <style> .label {
         font: normal 12px ;
       }</style>     -->     
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2><?php echo $button; ?></h2>                   
              <div class="clearfix"></div>
            </div>
            <div class="x_content" >
              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >

                <input type="hidden" name="kd_sptpd" value="<?php echo $kd_sptpd; ?>" /> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="masa_pajak" name="masa_pajak" required="required" placeholder="Masa Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $masa_pajak; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="tahun" name="tahun" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php echo $tahun; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama Wajib Pajak<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_wp" name="nm_wp" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_wp; ?>">                
                  </div>
                </div> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Wajib Pajak<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="alamat_wp" rows="3" class="resizable_textarea form-control" placeholder="Alamat Wajib Pajak" name="alamat_wp"><?php echo $alamat_wp; ?></textarea>                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Tempat Usaha <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_tmp_usaha" name="nm_tmp_usaha" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_tmp_usaha; ?>">               
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="alamat_usaha" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="alamat_usaha"><?php echo $alamat_usaha; ?></textarea>                
                  </div>
                </div>                                  
                <div class="form-group">
                  <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="npwpd" name="npwpd" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $npwpd; ?>">                  
                  </div>
                </div>              
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" >Jenis Burung <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">   
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="radio">
                        <input type="radio" class="flat" value="" name="jb"> Walet
                      </div>
                      <div class="radio">
                        <input type="radio" class="flat" value="" name="jb"> Sriti
                      </div>          
                    </div>
                  </div> 
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Hasil Pengambilan Sarang Burung <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="hasil_sb" name="hasil_sb" required="required" placeholder="Hasil Pengambilan Sarang Burung" class="form-control col-md-7 col-xs-12" value="<?php echo $hasil_sb; ?>">                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Harga Jual Sarang Burung <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="harga_sb" name="harga_sb" required="required" placeholder="Harga Jual Sarang Burung" class="form-control col-md-7 col-xs-12" value="<?php echo $harga_sb; ?>">                
                  </div>
                </div>    
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="dpp" name="dpp" required="required" placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $dpp; ?>">                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Yang Terutang <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="pajak_terutang" name="pajak_terutang" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $pajak_terutang; ?>">                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Surat Setoran Pajak Daerah <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="sspd" name="sspd" required="required" placeholder="Surat Setoran Pajak Daerah" class="form-control col-md-7 col-xs-12" value="<?php echo $sspd; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Laporan Produksi /Daftar Penjualan <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="lap_prod" name="lap_prod" required="required" placeholder="Laporan Produksi /Daftar Penjualan" class="form-control col-md-7 col-xs-12" value="<?php echo $lap_prod; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Lainnya <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="lain" name="lain" required="required" placeholder="Lainnya" class="form-control col-md-7 col-xs-12" value="<?php echo $lain; ?>">                
                  </div>
                </div>
               
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                    <a href="<?php echo site_url('Sptpd/sptpd_psb') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>



      </div>
