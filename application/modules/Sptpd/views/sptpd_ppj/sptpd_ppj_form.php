<!-- <style> .label {
         font: normal 12px ;
       }</style>     -->     
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2><?php echo $button; ?></h2>                   
              <div class="clearfix"></div>
            </div>
            <div class="x_content" >
              <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >

                <input type="hidden" name="kd_sptpd" value="<?php echo $kd_sptpd; ?>" /> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="masa_pajak" name="masa_pajak" required="required" placeholder="Masa Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $masa_pajak; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="tahun" name="tahun" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php echo $tahun; ?>">                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama Wajib Pajak<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_wp" name="nm_wp" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_wp; ?>">                
                  </div>
                </div> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Wajib Pajak<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="alamat_wp" rows="3" class="resizable_textarea form-control" placeholder="Alamat Wajib Pajak" name="alamat_wp"><?php echo $alamat_wp; ?></textarea>                
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Tempat Usaha <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_tmp_usaha" name="nm_tmp_usaha" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $nm_tmp_usaha; ?>">               
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="alamat_usaha" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="alamat_usaha"><?php echo $alamat_usaha; ?></textarea>                
                  </div>
                </div>                                  
                <div class="form-group">
                  <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="npwpd" name="npwpd" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $npwpd; ?>">                  
                  </div>
                </div>                                                                      
                <div class="ln_solid"></div>  

                <div align="center">
                  <center>    <h2>Data Enginering (Spesifikasi Generator)</h2></center>
                  <div class="clearfix"></div>
                </div>           

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Merk / Type Generator <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Merk / Type Generator" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kapasitas Daya <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Kapasitas Daya" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Mesin Penggerak <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Mesin Penggerak" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tegangan <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Tegangan" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Faktor Daya  <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Faktor Daya " class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Phase <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Phase" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div>                         
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" >Kegunaan <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">   
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="radio">
                        <input type="radio" class="flat" value="" name="klasifikasi"> Industri
                      </div>
                      <div class="radio">
                        <input type="radio" class="flat" value="" name="klasifikasi"> Non Industri
                      </div>
                    </div>
                  </div> 
                </div>                        
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" >Keperluan <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">   
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="radio">
                        <input type="radio" class="flat" value="" name="klasifikasi"> Utama (240 Jam Nyala)
                      </div>
                      <div class="radio">
                        <input type="radio" class="flat" value="" name="klasifikasi"> Cadangan (120 Jam Nyala)
                      </div>
                      <div class="radio">
                        <input type="radio" class="flat" value="" name="klasifikasi"> Darurat (30 Jam Nyala)
                      </div>       
                    </div>
                  </div> 
                </div> 
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Rumus Pemakaian Listrik <sup>*</sup>
                  </label>
                  <div class="col-md-1 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Daya" class="form-control col-md-7 col-xs-12" value="">                
                  </div> 
                  <div class="col-md-2 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Faktor Daya" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                  <div class="col-md-2 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Jam Nyala Sebulan" class="form-control col-md-7 col-xs-12" value="">                
                  </div> 
                  <div class="col-md-1 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Faktor Daya" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Perhitungan Pemakaian Berdasarkan Meteran <sup>*</sup>
                  </label>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Bulan Lalu" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <input type="text" id="" name="" required="required" placeholder="Bulan Ini" class="form-control col-md-7 col-xs-12" value="">                
                  </div>
                </div>    
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Pemakaian <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="dpp" name="dpp" required="required" placeholder="Jumlah Pemakaian" class="form-control col-md-7 col-xs-12" value="<?php echo $dpp; ?>">                
                  </div>
                </div>   
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="dpp" name="dpp" required="required" placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $dpp; ?>">                
                  </div>
                </div>  
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Yang Terutang <sup>*</sup>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="pajak_terutang" name="pajak_terutang" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $pajak_terutang; ?>">                
                  </div>
                </div>   
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                    <a href="<?php echo site_url('Sptpd/sptpd_ppj') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>



      </div>
