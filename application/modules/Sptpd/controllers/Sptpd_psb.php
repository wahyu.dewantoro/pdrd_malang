<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_psb extends CI_Controller {

    function __construct()
    {
        parent::__construct();
         
        $this->load->model('Msptpd_psb');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
    }
	public function index()
	{
		$this->template->load('Welcome/halaman','Sptpd/sptpd_psb/sptpd_psb_list');
	}
    public function json() {
        header('Content-Type: application/json');
        echo $this->Msptpd_psb->json_supplier();
    }
    public function hapus()
    {
            $response = array();
    
    if ($_POST['delete']) {
        
        
        $id = $_POST['delete'];
        $row = $this->Msptpd_psb->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_psb->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Supplier Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
    }
    public function table()
    {
         $this->load->view('Sptpd/sptpd_psb/sptpd_psb_table');
    }	
    public function create() 
    {
        $data = array(
            'button'     => 'Tambah Surat Pemberitahuan Pajak Daerah Pajak Sarang Burung',
            'action'     => site_url('Pembelian/man_harga_kayu/create_action'),
            'kd_sptpd' => set_value('kd_sptpd'),
            'masa_pajak'=> set_value('masa_pajak'),
            'tahun'=> set_value('tahun'),
            'nm_wp'=> set_value('nm_wp'),
            'alamat_wp'=> set_value('alamat_wp'),
            'nm_tmp_usaha' => set_value('nm_tmp_usaha'),
            'alamat_usaha'=> set_value('alamat_usaha'),
            'npwpd' => set_value('npwpd'),
            'jb'=> set_value('jb'),
            'hasil_sb'=> set_value('hasil_sb'),
            'harga_sb'=> set_value('harga_sb'),
            'dpp'=> set_value('dpp'),
            'pajak_terutang'=> set_value('pajak_terutang'),
            'sspd'=> set_value('sspd'),
            'lap_prod'=> set_value('lap_prod'),
            'lain'=> set_value('lain'),
        );          
        $this->template->load('Welcome/halaman','sptpd_psb/sptpd_psb_form',$data);
    }
    public function create_action() 
    {

        $data = array(
		'npwpd' => $this->input->post('npwpd',TRUE),
		'no_berkas' => $this->input->post('no_berkas',TRUE),
		'no_sipa' => $this->input->post('no_sipa',TRUE),
		'tmt' => $this->input->post('tmt',TRUE),
		'harga' => $this->input->post('harga',TRUE),
		'nm_wp' => $this->input->post('nm_wp',TRUE),
        'nm_perusahaan' => $this->input->post('nm_perusahaan',TRUE),
        'alamat' => $this->input->post('alamat',TRUE),
        'bagian_bulan' => $this->input->post('bagian_bulan',TRUE),
        'tahun' => $this->input->post('tahun',TRUE),
        'catatan' => $this->input->post('catatan',TRUE),
	    );

            $this->Msptpd_psb->insert($data);
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Tambah Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_psb'));
    }
    public function delete($id) 
    {
        $row = $this->Msptpd_psb->get_by_id($id);

        if ($row) {
            $this->Msptpd_psb->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Hapus Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_psb'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Sptpd/sptpd_psb'));
        }
    } 
    public function update($id) 
    {
        $row = $this->Msptpd_psb->get_by_id($id);

        if ($row) {
            $data = array(
                'button'     => 'Update Referensi Supplier',
                'action'     => site_url('Sptpd/sptpd_psb/update_action'),
            'kode_supplier' => set_value('kode_supplier',$row->kode_supplier),
            'nama_supplier' => set_value('nama_supplier',$row->nama_supplier),
            'email'=> set_value('email',$row->email),
            'hp'=> set_value('hp',$row->hp),
            'kecamatan'=> set_value('kecamatan',$row->kecamatan),
            'kabupaten'=> set_value('kabupaten',$row->kabupaten),
            'alamat'=> set_value('alamat',$row->alamat),
	    );
           $this->template->load('Welcome/halaman','sptpd_psb/sptpd_psb_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('setting/group'));
        }
    } 
        public function update_action() 
    {
            $data = array(
				'nama_supplier' => $this->input->post('nama_supplier',TRUE),
		'email' => $this->input->post('email',TRUE),
		'hp' => $this->input->post('hp',TRUE),
		'kecamatan' => $this->input->post('kecamatan',TRUE),
		'kabupaten' => $this->input->post('kabupaten',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Msptpd_psb->update($this->input->post('kode_supplier', TRUE), $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Update Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_psb'));
    }          
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */