<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_at extends CI_Controller {

    function __construct()
    {
        parent::__construct();
         
        $this->load->model('Msptpd_at');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
    }
	public function index()
	{
		$this->template->load('Welcome/halaman','Sptpd/sptpd_at/sptpd_at_list');
	}
    public function json() {
        header('Content-Type: application/json');
        echo $this->Msptpd_at->json_supplier();
    }
    public function hapus()
    {
            $response = array();
    
    if ($_POST['delete']) {
        
        
        $id = $_POST['delete'];
        $row = $this->Msptpd_at->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_at->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Supplier Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
    }
    public function table()
    {
         $this->load->view('Sptpd/sptpd_at/sptpd_at_table');
    }	
    public function create() 
    {
        $data = array(
            'button'     => 'Tambah Surat Pemberitahuan Pajak Daerah Air Tanah',
            'action'     => site_url('Pembelian/man_harga_kayu/create_action'),
            'kd_sptpd' => set_value('kd_sptpd'),
            'npwpd' => set_value('npwpd'),
            'no_berkas'=> set_value('no_berkas'),
            'no_sipa'=> set_value('no_sipa'),
            'tmt'=> set_value('tmt'),
            'harga'=> set_value('harga'),
            'nm_wp'=> set_value('nm_wp'),
            'nm_perusahaan'=> set_value('nm_perusahaan'),
            'alamat'=> set_value('alamat'),
            'bagian_bulan'=> set_value('bagian_bulan'),
            'tahun'=> set_value('tahun'),
            'catatan'=> set_value('catatan'),
        );      
        $this->template->load('Welcome/halaman','sptpd_at/sptpd_at_form',$data);
    }
    public function create_action() 
    {

        $data = array(
		'npwpd' => $this->input->post('npwpd',TRUE),
		'no_berkas' => $this->input->post('no_berkas',TRUE),
		'no_sipa' => $this->input->post('no_sipa',TRUE),
		'tmt' => $this->input->post('tmt',TRUE),
		'harga' => $this->input->post('harga',TRUE),
		'nm_wp' => $this->input->post('nm_wp',TRUE),
        'nm_perusahaan' => $this->input->post('nm_perusahaan',TRUE),
        'alamat' => $this->input->post('alamat',TRUE),
        'bagian_bulan' => $this->input->post('bagian_bulan',TRUE),
        'tahun' => $this->input->post('tahun',TRUE),
        'catatan' => $this->input->post('catatan',TRUE),
	    );

            $this->Msptpd_at->insert($data);
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Tambah Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_at'));
    }
    public function delete($id) 
    {
        $row = $this->Msptpd_at->get_by_id($id);

        if ($row) {
            $this->Msptpd_at->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Hapus Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_at'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Sptpd/sptpd_at'));
        }
    } 
    public function update($id) 
    {
        $row = $this->Msptpd_at->get_by_id($id);

        if ($row) {
            $data = array(
                'button'     => 'Update Referensi Supplier',
                'action'     => site_url('Sptpd/sptpd_at/update_action'),
            'kode_supplier' => set_value('kode_supplier',$row->kode_supplier),
            'nama_supplier' => set_value('nama_supplier',$row->nama_supplier),
            'email'=> set_value('email',$row->email),
            'hp'=> set_value('hp',$row->hp),
            'kecamatan'=> set_value('kecamatan',$row->kecamatan),
            'kabupaten'=> set_value('kabupaten',$row->kabupaten),
            'alamat'=> set_value('alamat',$row->alamat),
	    );
           $this->template->load('Welcome/halaman','sptpd_at/sptpd_at_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('setting/group'));
        }
    } 
        public function update_action() 
    {
            $data = array(
				'nama_supplier' => $this->input->post('nama_supplier',TRUE),
		'email' => $this->input->post('email',TRUE),
		'hp' => $this->input->post('hp',TRUE),
		'kecamatan' => $this->input->post('kecamatan',TRUE),
		'kabupaten' => $this->input->post('kabupaten',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Msptpd_at->update($this->input->post('kode_supplier', TRUE), $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Update Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_at'));
    }          
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */