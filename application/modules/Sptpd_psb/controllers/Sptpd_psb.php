<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_psb extends CI_Controller {

    function __construct()
    {
        parent::__construct();
         
        $this->load->model('Msptpd_psb');
        $this->load->library('form_validation');        
		$this->load->library('datatables');
        $this->load->model('Master/Mpokok');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
	public function index()
	{
        if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK'])/* OR isset($_GET['DASAR_PENGENAAN'])*/){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                //$dasar_pengenaan=$_POST['DASAR_PENGENAAN'];
        } else {
                $bulan=date('n');//date('d-m-Y');
                $tahun=date('Y');//date('d-m-Y');
                //$dasar_pengenaan='';
        }
        $sess=array(
                'psb_bulan'=>$bulan,
                'psb_tahun'=>$tahun,
                //'dasar_pengenaan_ppj'=>$dasar_pengenaan
         );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
	  $this->template->load('Welcome/halaman','Sptpd_psb/sptpd_psb_list',$data);
	}
    public function json() {
        header('Content-Type: application/json');
        echo $this->Msptpd_psb->json();
    }
function get_nama_usaha(){
        $data1=$_POST['npwp'];
        $jp=$_POST['jp'];
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT id_inc,nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$data1."' and jenis_pajak='$jp'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
function get_alamat_usaha(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("SELECT nama_usaha,ALAMAT_USAHA from tempat_usaha where id_inc='$exp[1]' ")->row();
            // print_r($data);
        echo $data->ALAMAT_USAHA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    function get_detail_usaha(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("select a.kodekel||'|'||D.NAMAKELURAHAN||'|'||c.kodekec||'|'||C.NAMAKEC||'|'||ID_OP data from tempat_usaha a
                                     join kecamatan c on c.kodekec=A.KODEKEC
                                     join kelurahan d on D.KODEKELURAHAN=A.KODEKEL
                                           and D.KODEKEC=A.KODEKEC
                                     where a.id_inc='$exp[1]'")->row();
            // print_r($data);
        echo $data->DATA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }               
    public function hapus()
    {
            $response = array();
    
    if ($_POST['delete']) {
        
        
        $id = $_POST['delete'];
        $row = $this->Msptpd_psb->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_psb->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Supplier Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
    }
    public function table()
    {
        $data['mp']=$this->Mpokok->listMasapajak();
         $this->load->view('Sptpd_psb/sptpd_psb_table',$data);
    }	
    public function ijin($id="")
    {

    $res=$this->Msptpd_psb->get_by_id($id);
    if($res){
        $data=array(
            'DARI'                    => set_value('DARI',$res->DARI),
            'NOMOR'                   => set_value('NOMOR',$res->NOMOR),
            'disable'                     => '',
        );    
    }
    else {
                $data=array(
            'DARI'                    => set_value('DARI'),
            'NOMOR'                   => set_value('NOMOR'),
            'disable'                 => '',
        ); 
    }
         $this->load->view('Sptpd_psb/ijin_form',$data);
    }   
    public function create() 
    {
    if ($this->role==8) {
            $data = array(
        'button'                       => 'Form SPTPD Sarang Burung',
        'action'                       => site_url('Sptpd_hotel/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'GOLONGAN'                     => set_value('GOLONGAN'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'tu'                           => $this->Msptpd_psb->getTu(),

    );           
    $this->template->load('Welcome/halaman','Wp/beranda/beranda_form',$data);
    } else {
        $data = array(
        'button'                       => 'Form SPTPD Sarang Burung',
        'action'                       => site_url('Sptpd_psb/preview'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_GALUR'                 => set_value('JUMLAH_GALUR'),
        'LUAS_AREA'                    => set_value('LUAS_AREA'),
        'DIPANEN_SETIAP'               => set_value('DIPANEN_SETIAP'),
        'HASIL_PANEN'                  => set_value('HASIL_PANEN'),
        'JENIS_PENGELOLAAN'            => set_value('JENIS_PENGELOLAAN'),
        'JENIS_BURUNG'                 => set_value('JENIS_BURUNG'),
        'HASIL_SETIAP_PANEN'           => set_value('HASIL_SETIAP_PANEN'),
        'HARGA_RATA_RATA'              => set_value('HARGA_RATA_RATA'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'KEPEMILIKAN_IJIN'             => set_value('KEPEMILIKAN_IJIN'),
        'GOLONGAN'                  => set_value('GOLONGAN'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'id'                           => 0,
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => '',
        );          
        $this->template->load('Welcome/halaman','Sptpd_psb/sptpd_psb_form',$data);
    }
    }
    public function preview()
    {

        $data = array(
        'button'                       => 'Preview SPTPD Sarang Burung',
        'action'                       => site_url('Sptpd_psb/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_GALUR'                 => set_value('JUMLAH_GALUR'),
        'LUAS_AREA'                    => set_value('LUAS_AREA'),
        'DIPANEN_SETIAP'               => set_value('DIPANEN_SETIAP'),
        'HASIL_PANEN'                  => set_value('HASIL_PANEN'),
        'JENIS_PENGELOLAAN'            => set_value('JENIS_PENGELOLAAN'),
        'JENIS_BURUNG'                 => set_value('JENIS_BURUNG'),
        'HASIL_SETIAP_PANEN'           => set_value('HASIL_SETIAP_PANEN'),
        'HARGA_RATA_RATA'              => set_value('HARGA_RATA_RATA'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'KELURAHAN'                    => set_value('KELURAHAN'),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'KEPEMILIKAN_IJIN'             => set_value('KEPEMILIKAN_IJIN',$this->input->post('KEPEMILIKAN_IJIN',TRUE)),

        'NOMOR'                    => set_value('NOMOR'),
        'DARI'                    => set_value('DARI'),
        'GOLONGAN'                  => set_value('GOLONGAN'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'id'                           => 0
        );          
        $this->template->load('Welcome/halaman','sptpd_psb/sptpd_psb_preview',$data);
    }
    public function create_action() 
    {

    $m=sprintf("%02d", $this->input->post('MASA_PAJAK',TRUE));
    $BERLAKU_MULAI=date('d/'.$m.'/Y');
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $OP=$this->input->post('GOLONGAN',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));

    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('JENIS_PENGELOLAAN', $this->input->post('JENIS_PENGELOLAAN',TRUE));
    $this->db->set('JENIS_BURUNG', $this->input->post('JENIS_BURUNG',TRUE));
    $this->db->set('KEPEMILIKAN_IJIN', $this->input->post('KEPEMILIKAN_IJIN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('DARI', $this->input->post('DARI',TRUE));
    $this->db->set('NOMOR', $this->input->post('NOMOR',TRUE));
    $this->db->set('LUAS_AREA',str_replace(',', '.', str_replace('.', '',$this->input->post('LUAS_AREA'))));
    $this->db->set('JUMLAH_GALUR',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_GALUR'))));
    $this->db->set('DIPANEN_SETIAP', $this->input->post('DIPANEN_SETIAP',TRUE));
    $this->db->set('HASIL_PANEN', $this->input->post('HASIL_PANEN',TRUE));
    $this->db->set('HASIL_SETIAP_PANEN',str_replace(',', '.', str_replace('.', '',$this->input->post('HASIL_SETIAP_PANEN'))));
    $this->db->set('HARGA_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_RATA_RATA'))));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->set('STATUS', 0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('ID_OP',$OP);
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
if ($this->role=='8') {
    $this->db->set('NPWP_INSERT',$this->nip);
} else {
    $this->db->set('NIP_INSERT',$this->nip);
}
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->insert('SPTPD_SARANG_BURUNG'); 
            $this->session->set_flashdata('message', '<script>
              $(window).load(function(){
               swal("Berhasil Tambah SPTPD Sarang Burung", "", "success")
              });

            </script>');
    if ($this->role=='10') {
            redirect(site_url('Upt/upt'));
        } else {
            redirect(site_url('Sptpd_psb'));
        }
    }
    public function delete($id) 
    {
        $row = $this->Msptpd_psb->get_by_id($id);

        if ($row) {
            $this->Msptpd_psb->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Hapus Supplier", "", "success")
  });

</script>');
            redirect(site_url('Sptpd/sptpd_psb'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Sptpd/sptpd_psb'));
        }
    } 
    public function update($id) 
    {
        $row = $this->Msptpd_psb->get_by_id($id);

        if ($row) {
            $data = array(
            'button'                => 'Form Update SPTPD Sarang Burung',
            'action'                => site_url('Sptpd_psb/update_action'),
            'disable'               => '',
            'ID_INC'                => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'            => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NAMA_WP'               => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'             => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'            => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NPWPD'                 => set_value('NPWPD',$row->NPWPD),

            'NO_FORMULIR'           => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'    => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'         => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'             => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'             => set_value('KELURAHAN',$row->KODEKEL),
            'JENIS_PENGELOLAAN'     => set_value('JENIS_PENGELOLAAN',$row->JENIS_PENGELOLAAN),
            'JENIS_BURUNG'          => set_value('JENIS_BURUNG',$row->JENIS_BURUNG),
            'KEPEMILIKAN_IJIN'      => set_value('KEPEMILIKAN_IJIN',$row->KEPEMILIKAN_IJIN),
            'DARI'                  => set_value('DARI',$row->DARI),
            'NOMOR'                 => set_value('NOMOR',$row->NOMOR),
            'JENIS_ENTRIAN'         => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'LUAS_AREA'             => set_value('LUAS_AREA',number_format($row->LUAS_AREA,'0','','.')),
            'JUMLAH_GALUR'          => set_value('JUMLAH_GALUR',number_format($row->JUMLAH_GALUR,'0','','.')),
            'HASIL_PANEN'           => set_value('HASIL_PANEN',$row->HASIL_PANEN),
            'DIPANEN_SETIAP'        => set_value('DIPANEN_SETIAP',$row->DIPANEN_SETIAP),
            'HASIL_SETIAP_PANEN'    => set_value('HASIL_SETIAP_PANEN',number_format($row->HASIL_SETIAP_PANEN,'0','','.')),
            'HARGA_RATA_RATA'       => set_value('HARGA_RATA_RATA',number_format($row->HARGA_RATA_RATA,'0','','.')),
            'DPP'                   => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'       => $this->Mpokok->listMasapajak(),
            'kel'                   => $this->Msptpd_psb->getkel($row->KODEKEC),
            'kec'                   => $this->Mpokok->getKec(),
            'id'                    => $id
        );
           $this->template->load('Welcome/halaman','Sptpd_psb/sptpd_psb_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('setting/group'));
        }
    } 
        public function update_action() 
    {
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));

    $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('JENIS_PENGELOLAAN', $this->input->post('JENIS_PENGELOLAAN',TRUE));
    $this->db->set('JENIS_BURUNG', $this->input->post('JENIS_BURUNG',TRUE));
    $this->db->set('KEPEMILIKAN_IJIN', $this->input->post('KEPEMILIKAN_IJIN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('DARI', $this->input->post('DARI',TRUE));
    $this->db->set('NOMOR', $this->input->post('NOMOR',TRUE));
    $this->db->set('LUAS_AREA',str_replace(',', '.', str_replace('.', '',$this->input->post('LUAS_AREA'))));
    $this->db->set('JUMLAH_GALUR',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_GALUR'))));
    $this->db->set('DIPANEN_SETIAP', $this->input->post('DIPANEN_SETIAP',TRUE));
    $this->db->set('HASIL_PANEN', $this->input->post('HASIL_PANEN',TRUE));
    $this->db->set('HASIL_SETIAP_PANEN',str_replace(',', '.', str_replace('.', '',$this->input->post('HASIL_SETIAP_PANEN'))));
    $this->db->set('HARGA_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_RATA_RATA'))));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
    $this->db->update('SPTPD_SARANG_BURUNG');
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
  $(window).load(function(){
   swal("Berhasil Update SPTPD Sarang Burung", "", "success")
  });

</script>');
            redirect(site_url('Sptpd_psb'));
    }     
    public function detail($id) 
    {
        $id=rapikan($id);
        $row = $this->Msptpd_psb->get_by_id($id);

        if ($row) {
        $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($row->KODEKEC);
        $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($row->KODEKEL,$row->KODEKEC);
            $data = array(
            'button'                => 'Detail SPTPD Sarang Burung',
            'action'                => site_url('Sptpd_psb/update_action'),
            'disable'               => 'disabled',
            'ID_INC'                => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'            => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NAMA_WP'               => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'             => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'            => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NPWPD'                 => set_value('NPWPD',$row->NPWPD),

            'NO_FORMULIR'           => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'    => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'         => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'             => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'             => set_value('KELURAHAN',$row->KODEKEL),
            'JENIS_PENGELOLAAN'     => set_value('JENIS_PENGELOLAAN',$row->JENIS_PENGELOLAAN),
            'JENIS_BURUNG'          => set_value('JENIS_BURUNG',$row->JENIS_BURUNG),
            'KEPEMILIKAN_IJIN'      => set_value('KEPEMILIKAN_IJIN',$row->KEPEMILIKAN_IJIN),
            'DARI'                  => set_value('DARI',$row->DARI),
            'NOMOR'                 => set_value('NOMOR',$row->NOMOR),
            'JENIS_ENTRIAN'         => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'LUAS_AREA'             => set_value('LUAS_AREA',number_format($row->LUAS_AREA,'0','','.')),
            'JUMLAH_GALUR'          => set_value('JUMLAH_GALUR',number_format($row->JUMLAH_GALUR,'0','','.')),
            'HASIL_PANEN'           => set_value('HASIL_PANEN',$row->HASIL_PANEN),
            'DIPANEN_SETIAP'        => set_value('DIPANEN_SETIAP',$row->DIPANEN_SETIAP),
            'HASIL_SETIAP_PANEN'    => set_value('HASIL_SETIAP_PANEN',number_format($row->HASIL_SETIAP_PANEN,'0','','.')),
            'HARGA_RATA_RATA'       => set_value('HARGA_RATA_RATA',number_format($row->HARGA_RATA_RATA,'0','','.')),
            'DPP'                   => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'       => $this->Mpokok->listMasapajak(),
            'kel'                   => $this->Msptpd_psb->getkel($row->KODEKEC),
            'kec'                   => $this->Mpokok->getKec(),
            'id'                    => $id,
            'list_masa_pajak'        =>$this->Mpokok->listMasapajak()   ,
            'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
            'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN,    
        );
           $this->template->load('Welcome/halaman','Sptpd_psb/sptpd_psb_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('setting/group'));
        }
    }
    function pdf_kode_biling_psb($KODE_BILING=""){
            $KODE_BILING=rapikan($KODE_BILING);
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$KODE_BILING'")->row();
            $html     =$this->load->view('Sptpd_psb/sptpd_psb_pdf',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }       
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */