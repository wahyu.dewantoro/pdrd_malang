<?php if ($this->session->userdata('MS_ROLE_ID')=='4' OR $this->session->userdata('MS_ROLE_ID')=='69') { ?>
                    <table id="example2" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th  class="text-center" width="2%">No</th>
                          <th  class="text-center" width="6%">Kode Biling</th>
                          <th  class="text-center" width="5%">Masa</th>
                          <th  class="text-center" width="5%">Tahun</th>
                          <th  class="text-center" width="6%">NPWPD</th>
                          <th  class="text-center" width="10%">Nama Wp</th>
                          <th  class="text-center" width="10%">DPP</th>
                          <th class="text-center" width="12%" >Pajak Terutang</th>
                          <th  class="text-center" width="11%">Status</th>
                          <th  class="text-center" width="10%">Aksi</th>
                        </tr>
                      </thead>
                    </table>
<script type="text/javascript">
           $(document).ready(function() {
               $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
               {
                   return {
                       "iStart": oSettings._iDisplayStart,
                       "iEnd": oSettings.fnDisplayEnd(),
                       "iLength": oSettings._iDisplayLength,
                       "iTotal": oSettings.fnRecordsTotal(),
                       "iFilteredTotal": oSettings.fnRecordsDisplay(),
                       "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                       "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                   };
               };
      
               var t = $("#example2").dataTable({
                   initComplete: function() {
                       var api = this.api();
                       $('#mytable_filter input')
                               .off('.DT')
                               .on('keyup.DT', function(e) {
                                   if (e.keyCode == 13) {
                                       api.search(this.value).draw();
                           }
                       });
                   },
                          "bPaginate": true,
                          "bLengthChange": false,
                          "bFilter": false,
                          "bSort": true,
                          "bInfo": true,
                          "bAutoWidth": false,
                       
                   
                   'oLanguage':
                   {
                     "sProcessing":   "Sedang memproses...",
                     "sLengthMenu":   "Tampilkan _MENU_ entri",
                     "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                     "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                     "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                     "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                     "sInfoPostFix":  "",
                     "sSearch":       "Cari:",
                     "sUrl":          "",
                     "oPaginate": {
                       "sFirst":    "Pertama",
                       "sPrevious": "Sebelumnya",
                       "sNext":     "Selanjutnya",
                       "sLast":     "Terakhir"
                     }
                   },
                   processing: true,
                   serverSide: true,
                   ajax: {"url": "<?php echo base_url()?>Sptpd_psb/sptpd_psb/json", "type": "POST"},
                 columns: [
                 {
                   "data": "ID_INC",
                   "orderable": false,
                   "className" : "text-center",
                 },
                  {"data": "KODE_BILING","className": "text-right"},
      {"data":"MASA_PAJAK",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '1' : return "Januari"; break;
          case '2' : return "Februari"; break;
          case '3' : return "Maret"; break;
          case '4' : return "April"; break;
          case '5' : return "Mei"; break;
          case '6' : return "Juni"; break;
          case '7' : return "Juli"; break;
          case '8' : return "Agustus"; break;
          case '9' : return "September"; break;
          case '10' : return "Oktober"; break;
          case '11' : return "November"; break;
          case '12' : return "Desember"; break;
         default  : return 'N/A';
       }}},     
                 {"data": "TAHUN_PAJAK"},
                 {"data": "NPWPD"},
                 {"data": "NAMA_WP"},
                 {"data": "DPP","className": "text-right","render": $.fn.dataTable.render.number( '.', '.', 0, '' )},
                 {"data": "PAJAK_TERUTANG","className": "text-right","render": $.fn.dataTable.render.number( '.', '.', 0, '' )},
{"data":"STATUS",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '0' : return "Belum di Bayar"; break;
          // case '1' : return "Lunas"; break;
          case '1' : return row.TGL_BAYAR; break;
         default  : return 'N/A';
       }}},                   
                 {
                   "data" : "STATUS",
                   "orderable": false,
                   "className" : "text-center",
                   render: function ( data, type, row, meta ) {
                            switch(data) {
                              case '0' : return row.actionb; break;
                              case '1' : return row.actionb; break;
                             default  : return 'N/A';
                           }}
                 }
                 ],
                   rowCallback: function(row, data, iDisplayIndex) {
                       var info = this.fnPagingInfo();
                       var page = info.iPage;
                       var length = info.iLength;
                       var index = page * length + (iDisplayIndex + 1);
                       $('td:eq(0)', row).html(index);
                   }
               });
           });
       </script>
<?php } elseif ($this->session->userdata('MS_ROLE_ID')=='8') { ?>
                    <table id="example2" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th  class="text-center" width="2%">No</th>
                          <th  class="text-center" width="6%">Kode Biling</th>
                          <th  class="text-center" width="5%">Masa</th>
                          <th  class="text-center" width="4%">Tahun</th>
                          <th  class="text-center" width="10%">NPWPD</th>
                          <th  class="text-center" width="10%">Nama Wp</th>
                          <th  class="text-center" width="10%">DPP</th>
                          <th class="text-center" width="10%" >Pajak Terutang</th>
                          <th  class="text-center" width="10%">Status</th>
                          <th  class="text-center" width="10%">Aksi</th>
                        </tr>
                      </thead>
                    </table>
<script type="text/javascript">
           $(document).ready(function() {
               $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
               {
                   return {
                       "iStart": oSettings._iDisplayStart,
                       "iEnd": oSettings.fnDisplayEnd(),
                       "iLength": oSettings._iDisplayLength,
                       "iTotal": oSettings.fnRecordsTotal(),
                       "iFilteredTotal": oSettings.fnRecordsDisplay(),
                       "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                       "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                   };
               };
      
               var t = $("#example2").dataTable({
                   initComplete: function() {
                       var api = this.api();
                       $('#mytable_filter input')
                               .off('.DT')
                               .on('keyup.DT', function(e) {
                                   if (e.keyCode == 13) {
                                       api.search(this.value).draw();
                           }
                       });
                   },
                   
                       
                   "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false,  
                   'oLanguage':
                   {
                     "sProcessing":   "Sedang memproses...",
                     "sLengthMenu":   "Tampilkan _MENU_ entri",
                     "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                     "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                     "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                     "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                     "sInfoPostFix":  "",
                     "sSearch":       "Cari:",
                     "sUrl":          "",
                     "oPaginate": {
                       "sFirst":    "Pertama",
                       "sPrevious": "Sebelumnya",
                       "sNext":     "Selanjutnya",
                       "sLast":     "Terakhir"
                     }
                   },
                   processing: true,
                   serverSide: true,
                   ajax: {"url": "<?php echo base_url()?>Sptpd_psb/sptpd_psb/json", "type": "POST"},
                 columns: [
                 {
                   "data": "ID_INC",
                   "orderable": false,
                   "className" : "text-center",
                 },
                          {"data": "KODE_BILING","className": "text-right"},
      {"data":"MASA_PAJAK",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '1' : return "Januari"; break;
          case '2' : return "Februari"; break;
          case '3' : return "Maret"; break;
          case '4' : return "April"; break;
          case '5' : return "Mei"; break;
          case '6' : return "Juni"; break;
          case '7' : return "Juli"; break;
          case '8' : return "Agustus"; break;
          case '9' : return "September"; break;
          case '10' : return "Oktober"; break;
          case '11' : return "November"; break;
          case '12' : return "Desember"; break;
         default  : return 'N/A';
       }}},     
                 {"data": "TAHUN_PAJAK"},
                 {"data": "NPWPD"},
                 {"data": "NAMA_WP"},
                 {"data": "DPP","className": "text-right","render": $.fn.dataTable.render.number( '.', '.', 0, '' )},
                 {"data": "PAJAK_TERUTANG","className": "text-right","render": $.fn.dataTable.render.number( '.', '.', 0, '' )},
{"data":"STATUS",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '0' : return "Belum di Bayar"; break;
          // case '1' : return "Lunas"; break;
          case '1' : return row.TGL_BAYAR; break;
         default  : return 'N/A';
       }}},                   
                 {
                   "data" : "STATUS",
                   "orderable": false,
                   "className" : "text-center",
                   render: function ( data, type, row, meta ) {
                            switch(data) {
                              case '0' : return row.actionb; break;
                              case '1' : return row.actionb; break;
                             default  : return 'N/A';
                           }}
                 }
                 ],
                   rowCallback: function(row, data, iDisplayIndex) {
                       var info = this.fnPagingInfo();
                       var page = info.iPage;
                       var length = info.iLength;
                       var index = page * length + (iDisplayIndex + 1);
                       $('td:eq(0)', row).html(index);
                   }
               });
           });
       </script>

<?php } ?>