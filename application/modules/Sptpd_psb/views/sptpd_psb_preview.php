    <?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>

    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 12px;}
    textarea { font-size: 12px;}
    .input-group span{ font-size: 12px;}
    input[type='text'] { font-size: 12px; height:30px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>

<?php if($this->session->userdata('MS_ROLE_ID')==4) {?>  
  <div class="pull-right">
    <a class="btn btn-sm btn-primary" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i> Kembali</a>
  </div>
  <?php } ?>
</div> 

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" />

                    <input type="hidden" name="MASA_PAJAK" value="<?php echo $MASA_PAJAK;?>">  
                    <input style="font-size: 12px" type="hidden" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                  
                    <input type="hidden" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" <?php echo $disable ?> onchange="NamaUsaha(this.value);" >                  
                    <input type="hidden" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>" <?php echo $disable ?>>
                    <input type="hidden" name="ALAMAT_WP" value="<?php echo $ALAMAT_WP; ?>">

                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Masa Pajak</b></td>
                          <td>: <?php echo $namabulan[$MASA_PAJAK] ?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>Tahun</b></td>
                          <td>: <?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>NPWPD</b></td>
                          <td>: <?php echo $NPWPD; ?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>Nama WP</b></td>
                          <td>: <?php echo $NAMA_WP; ?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>Alamat WP</b></td>
                          <td>: <?php echo $ALAMAT_WP; ?> </td>
                        </tr>                      
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="19%"><b>Nama Usaha</b></td>
                            <td>: <?php $exp=explode('|', $NAMA_USAHA);echo $exp[0]; ?></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Almat Usaha</b></td>
                            <td>: <?php echo $ALAMAT_USAHA; ?></td>
                          </tr>
                          <tr>
                            <?php $kec=$this->db->query(" select namakec from kecamatan where kodekec='$KECAMATAN'")->row();?>
                            <td><b>Kecamatan </b></td>
                            <td>: <?php echo $kec->NAMAKEC;?></td>
                          </tr>
                          <tr>
                            <?php $kel=$this->db->query("select namakelurahan from kelurahan where KODEKELURAHAN='$KELURAHAN'  and kodekec='$KECAMATAN'")->row();?>
                            <td><b>Kelurahan</b></td>
                            <td>: <?php echo $kel->NAMAKELURAHAN;?></td>
                          </tr>                       
                        </tbody>
                      </table>
                      <input name="NAMA_USAHA"   value="<?php echo $NAMA_USAHA?>" type="hidden">
                      <input name="ALAMAT_USAHA" value="<?php echo $ALAMAT_USAHA?>" type="hidden">
                      <input type="hidden" name="GOLONGAN" value="<?php echo $GOLONGAN;?>">                                        
                      <input type="hidden" value="<?php echo $KECAMATAN?>"  name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12">
                      <input type="hidden" value="<?php echo $KELURAHAN?>"  name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12">  
                  </div> 
                </div>
              </div>
            </div> 
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">       
                        <!-- <input type="hidden" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"  <?php echo $disable?> placeholder="No Formulir"> -->
                          <input type="hidden" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                          <input type="hidden" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required <?php echo $disable?> >
<!--                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select  style="font-size:11px" id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                  <?php if($button==('Update SPTPD Hotel'||'Form SPTPD Hiburan')){?>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php }} ?>                          
                        </select>
                      </div>
                    </div>  -->
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jenis Pengelolaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_PENGELOLAAN" <?php echo $disable?> name="JENIS_PENGELOLAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($JENIS_PENGELOLAAN=='Pengusahaan'){echo "selected";}?> value="Pengusahaan">Pengusahaan</option>
                        <option  <?php if($JENIS_PENGELOLAAN=='Habitat Alami'){echo "selected";}?> value="Habitat Alami">Habitat Alami</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Burung</label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_BURUNG" <?php echo $disable?> name="JENIS_BURUNG"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($JENIS_BURUNG=='Walet'){echo "selected";}?> value="Walet">Walet</option>
                        <option  <?php if($JENIS_BURUNG=='Sriti'){echo "selected";}?> value="Sriti">Sriti</option>
                      </select>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kepemilikan Ijin <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="KEPEMILIKAN_IJIN" <?php echo $disable?> name="KEPEMILIKAN_IJIN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih <?php echo "$KEPEMILIKAN_IJIN"; ?></option>
                        <option  <?php if($KEPEMILIKAN_IJIN=='Ijin'){echo "selected";}?> value="Ijin">Ijin</option>
                        <option  <?php if($KEPEMILIKAN_IJIN=='Belum Ijin'){echo "selected";}?> value="Belum Ijin">Belum Ijin</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                        </label>
                      </div>
                    </div>
                  </div>  
                  <div id="DATA_PAJAK"> 
<?php if ($KEPEMILIKAN_IJIN=='Ijin') { ?>
<div id="IJIN">

  <div class="form-group">
    <label class="control-label col-md-3 col-xs-12" > Dari </label>
    <div class="col-md-3">
      <input type="text" name="DARI" id="DARI" required <?php echo $disable?> class="form-control col-md-7 col-xs-12" value="<?php echo $DARI; ?>"  placeholder="Dari" >
    </div>
    <label class="control-label col-md-3 col-xs-12" > Nomor </label>
    <div class="col-md-3">
      <input type="text" name="NOMOR" id="NOMOR" required <?php echo $disable?> class="form-control col-md-7 col-xs-12" value="<?php echo $NOMOR; ?>"  placeholder="Nomor" >
    </div>
  </div>
  
</div>
<?php } ?>


                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Luas Area </label>
                    <div class="col-md-3">
                      <input type="text" name="LUAS_AREA" id="LUAS_AREA" class="form-control col-md-7 col-xs-12" value="<?php echo $LUAS_AREA; ?>"  <?php echo $disable?> placeholder="Luas Area" >
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Jumlah Galur </label>
                    <div class="col-md-3">
                      <input type="text" name="JUMLAH_GALUR" id="JUMLAH_GALUR" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_GALUR; ?>"  <?php echo $disable?> placeholder="Jumlah Galur" >
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dipanen Setiap <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="DIPANEN_SETIAP" <?php echo $disable?> name="DIPANEN_SETIAP"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($DIPANEN_SETIAP=='4 Bulan Sekali'){echo "selected";}?> value="4 Bulan Sekali">4 Bulan Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='6 Bulan Sekali'){echo "selected";}?> value="6 Bulan Sekali">6 Bulan Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='1 Tahun Sekali'){echo "selected";}?> value="1 Tahun Sekali">1 Tahun Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='Lainnya'){echo "selected";}?> value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Hasil Panen </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="HASIL_PANEN" <?php echo $disable?> name="HASIL_PANEN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($HASIL_PANEN=='Dipasarkan Sendiri'){echo "selected";}?> value="Dipasarkan Sendiri">Dipasarkan Sendiri</option>
                        <option  <?php if($HASIL_PANEN=='Disetrokan ke Asosiasi'){echo "selected";}?> value="Disetrokan ke Asosiasi">Disetrokan ke Asosiasi</option>
                        <option  <?php if($HASIL_PANEN=='Diambil Tengkulak'){echo "selected";}?> value="Diambil Tengkulak">Diambil Tengkulak</option>
                        <option  <?php if($HASIL_PANEN=='Lainnya'){echo "selected";}?> value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Hasil Setiap Panen </label>
                    <div class="col-md-3">
    <div class="input-group">
                      <input  onchange='get(this);'   type="text" name="HASIL_SETIAP_PANEN" id="HASIL_SETIAP_PANEN" class="form-control col-md-7 col-xs-12" value="<?php echo $HASIL_SETIAP_PANEN; ?>"  <?php echo $disable?> placeholder="Hasil Setiap Panen" >
      <span class="input-group-addon">Kg</span></div>
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Harga Rata Rata </label>
                    <div class="col-md-3">
    <div class="input-group">
       <span class="input-group-addon">Rp</span>
                      <input   onchange='get(this);'  type="text" name="HARGA_RATA_RATA" id="HARGA_RATA_RATA" class="form-control col-md-7 col-xs-12" value="<?php echo $HARGA_RATA_RATA; ?>"  <?php echo $disable?> placeholder="Harga Rata Rata" >
                    </div>
                    </div>
                  </div>  
                  <?php if($disable==''){?>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Generate ID Biling</button>
                      <a href="<?php echo site_url('Sptpd_psb/sptpd_psb') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  <?php } ?> 
                </div>                  
                <div class="col-md-6">    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($PAJAK)){echo $PAJAK;} else { echo "5";}?>"  <?php echo $disable?> placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                      </div>
                    </div> 
                  </div> 
                </div>
              </div>
            </div>     
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<style type="text/css">
  .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    padding: 6px 6px 6px 6px;
  }
  td{
    font-size:12px;
  }
</style>
<script>
  function get() {
    var TARIF_RATA_RATA= parseFloat(nominalFormat(document.getElementById("TARIF_RATA_RATA").value))||0;
    var KAMAR_TERISI= parseFloat(nominalFormat(document.getElementById("KAMAR_TERISI").value))||0;
    var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
    var dpp=KAMAR_TERISI*TARIF_RATA_RATA;
    DPP.value=getNominal(dpp)  ;
    PAJAK_TERUTANG.value=getNominal(dpp*PAJAK)  ;
    }
    function validasi(){
      var MASA_PAJAK=document.forms["demo-form2"]["MASA_PAJAK"].value;
      var TAHUN_PAJAK=document.forms["demo-form2"]["TAHUN_PAJAK"].value;
      var NPWPD=document.forms["demo-form2"]["NPWPD"].value;
      if (MASA_PAJAK==null || MASA_PAJAK==""){
        swal("Masa Pajak Harus di Isi", "", "warning")
        return false;
      };
      if (TAHUN_PAJAK==null || TAHUN_PAJAK==""){
        swal("Tahun Pajak Harus di Isi", "", "warning")
        return false;
      }; 
      if (NPWPD==null || NPWPD==""){
        swal("NPWPD Harus di Isi", "", "warning")
        return false;
      };
      if (!NPWPD.match(number)){
        swal("NPWPD Harus Angka", "", "warning")
        return false;
      };

    }  

    $("#NPWPD").keyup(function(){
      var npwpd = $("#NPWPD").val();
      
    });   
    /* Tanpa Rupiah */
    var JUMLAH_KAMAR = document.getElementById('JUMLAH_KAMAR');
    JUMLAH_KAMAR.addEventListener('keyup', function(e)
    {
      JUMLAH_KAMAR.value = formatRupiah(this.value);
    });
    /* Tanpa Rupiah */
    var TARIF_RATA_RATA = document.getElementById('TARIF_RATA_RATA');
    TARIF_RATA_RATA.addEventListener('keyup', function(e)
    {
      TARIF_RATA_RATA.value = formatRupiah(this.value);
    });     
    /* Tanpa Rupiah */
    var KAMAR_TERISI = document.getElementById('KAMAR_TERISI');
    KAMAR_TERISI.addEventListener('keyup', function(e)
    {
      KAMAR_TERISI.value = formatRupiah(this.value);
    });   
    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
  
    $('#KEPEMILIKAN_IJIN').on('change', function() {
    if(this.value=='Ijin'){
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_psb/sptpd_psb/ijin/'.$id;?>'); 
    } else {
      $('#IJIN').remove();
    }
  }) 
  </script>
