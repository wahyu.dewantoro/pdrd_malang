     <?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>

    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:30px}
  </style>
    <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
      <a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>  
  <div class="clearfix"></div>   
  <div class="row">

    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >        
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">


        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($list_masa_pajak as $list_masa_pajak){ ?>
                            <option <?php if(!empty($MASA_PAJAK)){ if($list_masa_pajak==$MASA_PAJAK){echo "selected";}} else {if($list_masa_pajak==date("m")){echo "selected";}}?> value="<?php echo $list_masa_pajak?>"><?php echo $namabulan[$list_masa_pajak] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input style="font-size: 11px" type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                
                      </div>
                    </div>                              
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input type="text" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" <?php echo $disable ?> onchange="NamaUsaha(this.value);" >                  
                      </div>
                    </div> 
                      <?php if ($disable=='disabled') { ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" id="NAMA_USAHA" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <option  value="<?php echo $NAMA_USAHA?>"><?php echo $NAMA_USAHA ?></option>
                        </select>
                        <!-- <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>> -->               
                      </div>
                    </div>  
<?php } else { ?>                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" id="NAMA_USAHA" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <?php foreach($jenis as $jns){ ?>
                          <option <?php if($jns->NAMA_USAHA==$NAMA_USAHA){echo "selected";}?> value="<?php echo $jns->NAMA_USAHA?>"><?php echo $jns->NAMA_USAHA ?></option>
                          <?php } ?> 
                        </select>
                        <!-- <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>> -->               
                      </div>
                    </div>
<?php } ?>                    
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Kecamatan </b></td>
                          <td><span id="NAMAKEC"><?php if ($disable=='disabled') {echo "$NAMA_KECAMATAN";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kelurahan</b></td>
                          <td><span id="NAMAKELURAHAN"><?php if ($disable=='disabled') {echo "$NAMA_KELURAHAN";} ?></span></td>
                        </tr>                       
                      </tbody>
                    </table>
                    <input type="hidden" id="V_KODEKEC" name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="V_KODEKEL" name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="V_ID_OP" name="GOLONGAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >                                        
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>" <?php echo $disable ?>>                
                      </div>
                    </div> 

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  WP<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <textarea style="font-size: 11px" <?php echo $disable ?> id="ALAMAT_WP" rows="3" class="resizable_textarea form-control" placeholder="Alamat Wajib Pajak" name="ALAMAT_WP"><?php echo $ALAMAT_WP; ?></textarea>                
                      </div>
                    </div>
<!--                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>>               
                      </div>
                    </div>  -->
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <textarea style="font-size: 11px" <?php echo $disable ?> id="ALAMAT_USAHA" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                      </div>
                    </div>  
                  </div> 
                </div>
              </div>
            </div> 
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">              
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"  <?php echo $disable?> placeholder="No Formulir">
                      </div>
                    </div> -->
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                        </div>
                      </div>
                    </div>  
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required <?php echo $disable?> >
                        </div>
                      </div>
                    </div> -->
<!--                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select  style="font-size:11px" id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                  <?php if($button==('Update SPTPD Hotel'||'Form SPTPD Hiburan')){?>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php }} ?>                          
                        </select>
                      </div>
                    </div>  -->
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jenis Pengelolaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_PENGELOLAAN" <?php echo $disable?> name="JENIS_PENGELOLAAN"  required class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($JENIS_PENGELOLAAN=='Pengusahaan'){echo "selected";}?> value="Pengusahaan">Pengusahaan</option>
                        <option  <?php if($JENIS_PENGELOLAAN=='Habitat Alami'){echo "selected";}?> value="Habitat Alami">Habitat Alami</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Burung</label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_BURUNG" <?php echo $disable?> name="JENIS_BURUNG"  required class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($JENIS_BURUNG=='Walet'){echo "selected";}?> value="Walet">Walet</option>
                        <option  <?php if($JENIS_BURUNG=='Sriti'){echo "selected";}?> value="Sriti">Sriti</option>
                      </select>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kepemilikan Ijin <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="KEPEMILIKAN_IJIN" <?php echo $disable?> name="KEPEMILIKAN_IJIN" required class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($KEPEMILIKAN_IJIN=='Ijin'){echo "selected";}?> value="Ijin">Ijin</option>
                        <option  <?php if($KEPEMILIKAN_IJIN=='Belum Ijin'){echo "selected";}?> value="Belum Ijin">Belum Ijin</option>
                      </select>
                    </div>
                    <!-- <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                        </label>
                      </div>
                    </div> -->
                  </div>  
                  <div id="DATA_PAJAK"> 
<?php if ($KEPEMILIKAN_IJIN=='Ijin') { ?>
<div id="IJIN">

  <div class="form-group">
    <label class="control-label col-md-3 col-xs-12" > Dari </label>
    <div class="col-md-3">
      <input type="text" name="DARI" id="DARI" required <?php echo $disable?> class="form-control col-md-7 col-xs-12" value="<?php echo $DARI; ?>"  placeholder="Dari" >
    </div>
    <label class="control-label col-md-3 col-xs-12" > Nomor </label>
    <div class="col-md-3">
      <input type="text" name="NOMOR" id="NOMOR" required <?php echo $disable?> class="form-control col-md-7 col-xs-12" value="<?php echo $NOMOR; ?>"  placeholder="Nomor" >
    </div>
  </div>
  
</div>
<?php } ?>


                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Luas Area </label>
                    <div class="col-md-3">
                      <input type="text" name="LUAS_AREA" id="LUAS_AREA" required class="form-control col-md-7 col-xs-12" value="<?php echo $LUAS_AREA; ?>"  <?php echo $disable?> placeholder="Luas Area" >
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Jumlah Galur </label>
                    <div class="col-md-3">
                      <input type="text" name="JUMLAH_GALUR" id="JUMLAH_GALUR" required class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_GALUR; ?>"  <?php echo $disable?> placeholder="Jumlah Galur" >
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dipanen Setiap <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="DIPANEN_SETIAP" <?php echo $disable?> name="DIPANEN_SETIAP"  required class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($DIPANEN_SETIAP=='4 Bulan Sekali'){echo "selected";}?> value="4 Bulan Sekali">4 Bulan Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='6 Bulan Sekali'){echo "selected";}?> value="6 Bulan Sekali">6 Bulan Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='1 Tahun Sekali'){echo "selected";}?> value="1 Tahun Sekali">1 Tahun Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='Lainnya'){echo "selected";}?> value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Hasil Panen </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="HASIL_PANEN" <?php echo $disable?> name="HASIL_PANEN" required class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($HASIL_PANEN=='Dipasarkan Sendiri'){echo "selected";}?> value="Dipasarkan Sendiri">Dipasarkan Sendiri</option>
                        <option  <?php if($HASIL_PANEN=='Disetrokan ke Asosiasi'){echo "selected";}?> value="Disetrokan ke Asosiasi">Disetrokan ke Asosiasi</option>
                        <option  <?php if($HASIL_PANEN=='Diambil Tengkulak'){echo "selected";}?> value="Diambil Tengkulak">Diambil Tengkulak</option>
                        <option  <?php if($HASIL_PANEN=='Lainnya'){echo "selected";}?> value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Hasil Setiap Panen </label>
                    <div class="col-md-3">
    <div class="input-group">
                      <input  onchange='get(this);'   type="text" required name="HASIL_SETIAP_PANEN" id="HASIL_SETIAP_PANEN" class="form-control col-md-7 col-xs-12" value="<?php echo $HASIL_SETIAP_PANEN; ?>"  <?php echo $disable?> placeholder="Hasil Setiap Panen" >
      <span class="input-group-addon">Kg</span></div>
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Harga Rata Rata </label>
                    <div class="col-md-3">
    <div class="input-group">
       <span class="input-group-addon">Rp</span>
                      <input   onchange='get(this);'  type="text" required name="HARGA_RATA_RATA" id="HARGA_RATA_RATA" class="form-control col-md-7 col-xs-12" value="<?php echo $HARGA_RATA_RATA; ?>"  <?php echo $disable?> placeholder="Harga Rata Rata" >
                    </div>
                    </div>
                  </div>  
                  <?php if($disable==''){?>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Submit</button>
                      <a href="<?php echo site_url('Sptpd_psb/sptpd_psb') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  <?php } ?> 
                </div>                  
                <div class="col-md-6">    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($PAJAK)){echo $PAJAK;} else { echo "5";}?>"  <?php echo $disable?> placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                      </div>
                    </div> 
                  </div> 
                </div>
              </div>
            </div>     
          </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
      <script>
    function alamat(sel)
    {
            if (sel=="") {
        $("#ALAMAT_USAHA").val(""); 
        $("#DESKRIPSI").html("");  
        $("#NAMAKELURAHAN").html("");
        $("#NAMAKEC").html("");    

      } else {
      var nama = sel;
      //alert(nama);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Sptpd_psb/sptpd_psb/get_alamat_usaha'?>",
       data: { nama: nama},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#ALAMAT_USAHA").val(msga); 
          }
        });
       $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Sptpd_psb/sptpd_psb/get_detail_usaha'?>",
       data: { nama: nama},
       cache: false,
       success: function(msga){
            if(msga!=0){
                var exp = msga.split("|"); 
                $("#NAMAKELURAHAN").html(exp[1]);
                $("#NAMAKEC").html(exp[3]);

                $("#V_KODEKEL").val(exp[0]);
                $("#V_KODEKEC").val(exp[2]);
                $("#V_ID_OP").val(exp[4]);                  
                
               // alert(msga);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null); 
                $("#VNAMA_WP").html("BELUM TERDAFTAR");
                $("#VALAMAT_WP").html("BELUM TERDAFTAR");
                
              }
          }
        });    
     }
    }
    function NamaUsaha(id){
                //alert(id);
                var npwpd=id;
                var jp='09';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
        
        $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpd_psb/sptpd_psb/get_nama_usaha'?>",
           data: { npwp: npwpd,
                   jp: jp   },
           cache: false,
           success: function(msga){
                  //alert(jp);
                  $("#NAMA_USAHA").html(msga);
                }
              }); 
                
    }  
    function get() {
      var HARGA_RATA_RATA= parseInt(document.getElementById("HARGA_RATA_RATA").value.replace(/\./g,''))|| 0;
      var HASIL_SETIAP_PANEN= parseInt(document.getElementById("HASIL_SETIAP_PANEN").value.replace(/\./g,''))|| 0;
      var dpp=HARGA_RATA_RATA*HASIL_SETIAP_PANEN;
      DPP.value = ribuan(dpp);
      PAJAK_TERUTANG.value = ribuan(dpp*0.05);
    }
//LUAS_AREA 
var LUAS_AREA = document.getElementById('LUAS_AREA'),
numberPattern = /^[0-9]+$/;

LUAS_AREA.addEventListener('keyup', function(e) {
  LUAS_AREA.value = getFormattedData(LUAS_AREA.value);
});
//JUMLAH_GALUR 
var JUMLAH_GALUR = document.getElementById('JUMLAH_GALUR'),
numberPattern = /^[0-9]+$/;

JUMLAH_GALUR.addEventListener('keyup', function(e) {
  JUMLAH_GALUR.value = getFormattedData(JUMLAH_GALUR.value);
});
//HASIL_SETIAP_PANEN 
var HASIL_SETIAP_PANEN = document.getElementById('HASIL_SETIAP_PANEN'),
numberPattern = /^[0-9]+$/;

HASIL_SETIAP_PANEN.addEventListener('keyup', function(e) {
  HASIL_SETIAP_PANEN.value = getFormattedData(HASIL_SETIAP_PANEN.value);
});
//HARGA_RATA_RATA 
var HARGA_RATA_RATA = document.getElementById('HARGA_RATA_RATA'),
numberPattern = /^[0-9]+$/;

HARGA_RATA_RATA.addEventListener('keyup', function(e) {
  HARGA_RATA_RATA.value = getFormattedData(HARGA_RATA_RATA.value);
});

function checkNumeric(str) {
  return str.replace(/\./g, '');
}
Number.prototype.format = function() {
  return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

function getFormattedData(num) {
  var i = checkNumeric(num),isNegative = checkNumeric(num) < 0,val;

  if (num.indexOf(',') > -1) {
    return num;
  }
  else if (num[num.length - 1] === '.') {
    return num;
  }
  else if(i.length < 3) {
   return i;
 }else {
  val = Number(i.replace(/[^\d]+/g, ''));
}

return (isNegative ? '-' : '') + val.format();
}    
function ribuan (angka)
{
  if(angka< 0)
  {
    var str = angka.toString();
    var res = str.substr(1);
    var reverse = res.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return "-"+ribuan;    
    return "-"+res;
  } else {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }
}        
    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  $('#KEPEMILIKAN_IJIN').on('change', function() {
    if(this.value=='Ijin'){
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_psb/sptpd_psb/ijin/'.$id;?>'); 
    } else {
      $('#IJIN').remove();
    }
  }) 
      </script>