<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_restoran extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Msptpd_restoran');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
    public function index()
    {
      if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $dasar_pengenaan=$_GET['NPWPD'];
        } else {
                $bulan='';//date('n');//date('d-m-Y');
                $tahun='';//date('Y');//date('d-m-Y');
                $dasar_pengenaan='';
        }
        $sess=array(
                'resto_bulan'=>$bulan,
                'resto_tahun'=>$tahun,
                'dasar_pengenaan_restoran'=>$dasar_pengenaan
         );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','Sptpd_restoran/sptpd_restoran_list',$data);
  }
  public function json() {
    header('Content-Type: application/json');
    echo $this->Msptpd_restoran->json_supplier();
}
public function hapus()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $id=rapikan($id);
        $row = $this->Msptpd_restoran->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_restoran->delete($id);
            $this->db->query("delete from tagihan where kode_biling='$id'");
            $response['status']  = 'success';
            $response['message'] = 'Data Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}
public function table()
{
   $data['mp']=$this->Mpokok->listMasapajak();
   $this->load->view('Sptpd_restoran/sptpd_restoran_table',$data);
}	
public function create() 
{
    if ($this->role==8) {
            $data = array(
        'button'                       => 'Form SPTPD Restoran',
        'action'                       => site_url('Sptpd_hotel/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JENIS_RESTO'                  => set_value('JENIS_RESTO'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'list_masa_pajak1'             => $this->Mpokok->listMasapajak(),
        'tu'                           => $this->Msptpd_restoran->getTu(),

    );           
    $this->template->load('Welcome/halaman','Wp/beranda/beranda_form',$data);
    } else {
    $data = array(
        'button'                       => 'Form SPTPD Restoran',
        'action'                       => site_url('Sptpd_restoran/review'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_MEJA'                  => set_value('JUMLAH_MEJA'),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'MEJA_TERISI'                  => set_value('MEJA_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'JENIS_RESTO'                  => set_value('JENIS_RESTO'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'list_masa_pajak1'             => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_restoran->getGol(),
        'detail_wp'                    => $this->Msptpd_restoran->detail_wp(),
        'detail_tu'                    => $this->Msptpd_restoran->detail_tempat_usaha(),
    );           
    $this->template->load('Welcome/halaman','sptpd_restoran_form',$data);
}
}

public function review() 
{
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_1'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/resto/'.str_replace(',', '', $img_name_ktp);
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                $tmp_file1=$_FILES['FILE1']['tmp_name'];
                $name_file1 =$_FILES['FILE1']['name'];
                if($name_file1!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp1=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_2'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/resto/'.str_replace(',', '', $img_name_ktp1);
                        move_uploaded_file($tmp_file1,$nf);
                        
                    }
                $tmp_file2=$_FILES['FILE2']['tmp_name'];
                $name_file2 =$_FILES['FILE2']['name'];
                if($name_file2!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file2);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp2=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_3'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/resto/'.str_replace(',', '', $img_name_ktp2);
                        move_uploaded_file($tmp_file2,$nf);
                        
                    }
    $data = array(
        'button'                       => 'Review Data SPTPD Restoran',
        'action'                       => site_url('Sptpd_restoran/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_MEJA'                  => set_value('JUMLAH_MEJA'),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'MEJA_TERISI'                  => set_value('MEJA_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'JENIS_RESTO'                  => set_value('JENIS_RESTO'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'list_masa_pajak1'             => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'KELURAHAN'                    => set_value('KELURAHAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$this->input->post('GOLONGAN',TRUE)),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_restoran->getGol(),
        'NAMA_GOLONGAN'                => '',
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => '',
        'FILE'                         => $img_name_ktp.$img_name_ktp1.$img_name_ktp2,
        'KEGIATAN'                     => set_value('KEGIATAN'),
    );           
    $this->template->load('Welcome/halaman','Sptpd_restoran_preview',$data);
}
public function create_action() 
{
    if ($this->input->post('JENIS_RESTO',TRUE)!='') {$RESTO='1';} else {$RESTO='0';};
    $OP=$this->input->post('GOLONGAN_ID',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $expired=$this->db->query("select TO_CHAR(sysdate, 'yyyymmdd')ed,sysdate now from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $va=$this->Mpokok->getVA($OP);
    $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;

    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $m=sprintf("%02d", $this->input->post('MASA_PAJAK',TRUE));
    $BERLAKU_MULAI=date('d/'.$m.'/Y');
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    $this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false);
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN_ID',TRUE));
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('JUMLAH_MEJA',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_MEJA'))));
    $this->db->set('TARIF_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF_RATA_RATA'))));
    $this->db->set('MEJA_TERISI',str_replace(',', '.', str_replace('.', '',$this->input->post('MEJA_TERISI'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->set('STATUS', 0);
    $this->db->set('JENIS_RESTO',$RESTO);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('DEVICE','web');
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('FILE_TRANSAKSI',$this->input->post('FILE',TRUE));
    $this->db->set('KEGIATAN_MAMIN',$this->input->post('KEGIATAN',TRUE));
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
if ($this->role=='8') {
    $this->db->set('NPWP_INSERT',$this->nip);
} else {
    $this->db->set('NIP_INSERT',$this->nip);
}
    $this->db->insert('SPTPD_RESTORAN');
    $this->Mpokok->registration($va,$nm,$pt,$ed);
    //$noform=$this->Mpokok->getFormulir();
    //$this->db->query("CALL INSERTFORMULIR('$noform','02')");*/
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Tambah SPTPD Restoran", "", "success")
     });

     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpd_restoran'));
    }
}
public function create_action1() 
{
    
    $OP=$this->input->post('GOLONGAN_ID',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $va=$this->Mpokok->getVA($OP);
    $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;

    $m=sprintf("%02d", $this->input->post('MASA_PAJAK',TRUE));
    $BERLAKU_MULAI=date('d/'.$m.'/Y');
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    //$this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    $this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false);
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN_ID',TRUE));
    $this->db->set('DASAR_PENGENAAN', 'Karcis Non');
    $this->db->set('JENIS_ENTRIAN', '1');
    $this->db->set('JUMLAH_MEJA',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_MEJA'))));
    $this->db->set('TARIF_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF_RATA_RATA'))));
    $this->db->set('MEJA_TERISI',0);
    $this->db->set('MASA', 1);
    $this->db->set('PAJAK', 0);
    $this->db->set('DPP',0);
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->set('STATUS', 0);
    $this->db->set('JENIS_RESTO',0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('DEVICE','web');
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
if ($this->role=='8') {
    $this->db->set('NPWP_INSERT',$this->nip);
} else {
    $this->db->set('NIP_INSERT',$this->nip);
}
    $this->db->insert('SPTPD_RESTORAN');
    $this->Mpokok->registration($va,$nm,$pt,$ed);
    /*$noform=$this->Mpokok->getFormulir();
    $this->db->query("CALL INSERTFORMULIR('$noform','02')");*/
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Tambah SPTPD Restoran", "", "success")
     });

     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpd_restoran'));
    }
}
public function delete($id) 
{
    $row = $this->Msptpd_restoran->get_by_id($id);

    if ($row) {
        $this->Msptpd_restoran->delete($id);
        $this->db->query("commit");
        $this->session->set_flashdata('message', '<script>
          $(window).load(function(){
             swal("Berhasil Hapus Supplier", "", "success")
         });

         </script>');
        redirect(site_url('Sptpd/sptpd_restoran'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('Sptpd/sptpd_restoran'));
    }
} 
public function update($id) 
{
    $id=rapikan($id);
    $row = $this->Msptpd_restoran->get_by_id($id);

    if ($row) {
        $data = array(
            'button'              => 'Update SPTPD Restoran',
            'action'              => site_url('Sptpd_restoran/update_action_skpd'),
            'disable'             => 'readonly',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'DASAR_PENGENAAN'     => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'JUMLAH_MEJA'        => set_value('JUMLAH_MEJA',number_format($row->JUMLAH_MEJA,'0','','.')),
            'TARIF_RATA_RATA'     => set_value('TARIF_RATA_RATA',number_format($row->TARIF_RATA_RATA,'0','','.')),
            'MEJA_TERISI'        => set_value('MEJA_TERISI',number_format($row->MEJA_TERISI,'0','','.')),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'JENIS_RESTO'         =>set_value('JENIS_RESTO',$row->JENIS_RESTO),
            'KODE_BILING'         =>set_value('KODE_BILING',$row->KODE_BILING),
            'KEGIATAN_MAMIN'      => set_value('KEGIATAN_MAMIN',$row->KEGIATAN_MAMIN),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_restoran->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_restoran->getGol(),
        );
        //$this->template->load('Welcome/halaman','Sptpd_restoran/sptpd_restoran_form', $data);
        $this->template->load('Welcome/halaman','Sptpd_restoran/edit_skpd_sptpd_restoran', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
} 
public function update_action_skpd() 
{
    $KODE_BILING=$this->input->post('KODE_BILING',TRUE);
    $KEGIATAN=$this->input->post('KEGIATAN',TRUE);
        $JR=$this->input->post('JENIS_RESTO',TRUE);
        if ($JR=='') {
           $JENIS_RESTO='0';
        } else {
           $JENIS_RESTO='1';
        }   
    $this->db->query("update tagihan set jenis_resto='$JENIS_RESTO',PROYEK='$KEGIATAN' where kode_biling='$KODE_BILING'");
    $this->db->query("update sptpd_restoran set jenis_resto='$JENIS_RESTO',KEGIATAN_MAMIN='$KEGIATAN' where kode_biling='$KODE_BILING'");
    /*$this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Update SPTPD Restoran", "", "success")
     });

     </script>');
    redirect(site_url('Sptpd_restoran'));*/
} 
public function update_action() 
{
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('JUMLAH_MEJA',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_MEJA'))));
    $this->db->set('TARIF_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF_RATA_RATA'))));
    $this->db->set('MEJA_TERISI',str_replace(',', '.', str_replace('.', '',$this->input->post('MEJA_TERISI'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
    $this->db->update('SPTPD_RESTORAN');
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Update SPTPD Restoran", "", "success")
     });

     </script>');
    redirect(site_url('Sptpd_restoran'));
}   
public function detail($id) 
{
    $id=rapikan($id);
    $row = $this->Msptpd_restoran->get_by_id($id);

    if ($row) {
        $NAMA_GOLONGAN = $this->Mpokok->getNamaGolongan($row->ID_OP);
        $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($row->KODEKEC);
        $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($row->KODEKEL,$row->KODEKEC);        
        $data = array(
            'button'              => 'Detail SPTPD Restoran',
            'action'              => site_url('Sptpd_restoran/update_action'),
            'disable'             => 'disabled',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'DASAR_PENGENAAN'     => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'JUMLAH_MEJA'         => set_value('JUMLAH_MEJA',number_format($row->JUMLAH_MEJA,'0','','.')),
            'TARIF_RATA_RATA'     => set_value('TARIF_RATA_RATA',number_format($row->TARIF_RATA_RATA,'0','','.')),
            'MEJA_TERISI'         => set_value('MEJA_TERISI',number_format($row->MEJA_TERISI,'0','','.')),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'list_masa_pajak1'    => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_restoran->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_restoran->getGol(),
            'JENIS_RESTO'         => set_value('JENIS_RESTO',$row->JENIS_RESTO),
            'NAMA_GOLONGAN'       => $NAMA_GOLONGAN->DESKRIPSI,
            'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
            'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN,
        );
        $this->template->load('Welcome/halaman','Sptpd_restoran/sptpd_restoran_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
}
function pdf_kode_biling_restoran($npwpd=""){
            $npwpd=rapikan($npwpd);
            $data['wp']=$this->db->query("select * from v_get_tagihan where kode_biling='$npwpd'")->row();
            $html     =$this->load->view('Sptpd_restoran/sptpd_restoran_pdf',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }         
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */