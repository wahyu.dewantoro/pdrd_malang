<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msptpd_restoran extends CI_Model
{

    public $table = 'SPTPD_RESTORAN';
    public $id = 'ID_INC';
    public $order = 'DESC';
    public $KODE_BILING = 'KODE_BILING';

    function __construct()
    {
        parent::__construct();
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
        $this->upt  =$this->session->userdata('UNIT_UPT_ID');
    }

    // datatables
    function json_supplier() {
        $t1 =$this->session->userdata('resto_bulan');
        $t2 =$this->session->userdata('resto_tahun');
        $cp =$this->session->userdata('dasar_pengenaan_restoran');
        if ($this->session->userdata('resto_tahun')!='' AND $this->session->userdata('resto_bulan')!='' AND $this->session->userdata('dasar_pengenaan_restoran')!='') {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND NPWPD LIKE '%$cp%'";
        } else if ($this->session->userdata('resto_tahun')!='' AND $this->session->userdata('resto_bulan')!='') {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2'";
        } else if ($this->session->userdata('resto_tahun')!='' AND $this->session->userdata('dasar_pengenaan_restoran')!='') {
            $wh ="TAHUN_PAJAK='$t2' AND NPWPD LIKE '%$cp%'";
        } else if ($this->session->userdata('resto_tahun')!='') {
            $wh ="TAHUN_PAJAK='$t2'";
        } else {
            $wh ="TO_CHAR(TGL_INSERT, 'mm')=TO_CHAR(SYSDATE, 'mm') ";
        }
        /*if ($this->session->userdata('dasar_pengenaan_restoran')=='') {
            $wh ="TO_CHAR(TGL_INSERT, 'mm')=TO_CHAR(SYSDATE, 'mm')";
            //$wh ="TGL_INSERT = SYSDATE";
        } else {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND NPWPD LIKE '%$cp%'";
        }*/
        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,DPP,PAJAK_TERUTANG,KODE_BILING, STATUS,to_char(TGL_BAYAR,'dd/mm/yyyy HH24: MI: SS')  TGL_BAYAR,GETPETUGAS(NIP_INSERT)||getpetugas(NPWP_INSERT)USER_INSERT");
        $this->datatables->from('SPTPD_RESTORAN');
        if ($this->role==8) {
            $this->datatables->where("NPWPD='$this->nip'");
        }
        $this->datatables->where($wh);
        $this->db->order_by("TGL_INSERT DESC, TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
        $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_restoran/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_restoran/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('Pdf/pdf/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'acak(KODE_BILING)');
        /* $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_restoran/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_restoran/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('pembelian/Sptpd_restoran/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="delete" data-id="$1" href="javascript:void(0)"').anchor(site_url('Pdf/pdf/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'acak(KODE_BILING)');*/
        $this->datatables->add_column('actionb1', '<div class="btn-group">'.anchor(site_url('Sptpd_restoran/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Pdf/pdf/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'acak(KODE_BILING)');
        return $this->datatables->generate();
    }
 
    // get data by id
    function get_by_id($id)
    {
        return $this->db->query("SELECT ID_INC, MASA_PAJAK, TAHUN_PAJAK, NAMA_WP, ALAMAT_WP, NAMA_USAHA, ALAMAT_USAHA, NPWPD, NO_FORMULIR,TO_CHAR(TANGGAL_PENERIMAAN, 'dd/mm/yyyy') AS TANGGAL_PENERIMAAN, TO_CHAR(BERLAKU_MULAI, 'dd/mm/yyyy') AS BERLAKU_MULAI, KODEKEC, KODEKEL, ID_OP, DASAR_PENGENAAN, JENIS_ENTRIAN, JUMLAH_MEJA, TARIF_RATA_RATA, MEJA_TERISI, MASA, PAJAK, DPP, PAJAK_TERUTANG, JENIS_RESTO,KEGIATAN_MAMIN,KODE_BILING FROM SPTPD_RESTORAN WHERE KODE_BILING='$id'")->row();
    }
    
  

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->KODE_BILING, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->KODE_BILING, $id);
        $this->db->delete($this->table);
    }

     function getGol(){
        return $this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI FROM OBJEK_PAJAK WHERE ID_GOL=184 OR ID_GOL=185 OR ID_GOL=186 OR ID_GOL=187 OR ID_GOL=495 ")->result();
     }

     function getKel($id){
        return $this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$id' order by NAMAKELURAHAN asc")->result();
     }

    function ListJenisRestoran(){
        return $this->db->query("SELECT * FROM JENIS_RESTORAN ORDER BY ID_INC ASC")->result();
    }
    function getTu()
    {
        return $this->db->query("SELECT ID_INC, NAMA_USAHA, ALAMAT_USAHA, JENIS_PAJAK FROM TEMPAT_USAHA WHERE NPWPD='$this->nip' AND JENIS_PAJAK='02' order by JENIS_PAJAK asc")->result();
    }
    function detail_tempat_usaha(){
        $npwpd='0000000000'.$this->upt;
        return $this->db->query("SELECT * FROM tempat_usaha where npwpd='$npwpd' AND JENIS_PAJAK='02'")->row();
    }
    function detail_wp(){
        $npwpd='0000000000'.$this->upt;
        return $this->db->query("SELECT * FROM wajib_pajak where npwp='$npwpd'")->row();
    }

}

/* End of file Mmenu.php */
/* Location: ./application/models/Mmenu.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-08 05:30:09 */
/* http://harviacode.com */