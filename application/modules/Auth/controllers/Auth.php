<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Sptpd_hotel/Msptpd_hotel');
        $this->load->model('Sptpd_restoran/Msptpd_restoran');
        $this->load->model('Sptpdhiburan/Mhiburan');
        $this->load->model('Sptpd_ppj/Msptpd_ppj');
        $this->load->model('Sptpdair/Mair');
        
        
 }
 
	public function index()
	{
		if(ENVIRONMENT=='development'){
			$data['res']=$this->db->query("SELECT  USERNAME, NAMA ||' ('|| NAMA_ROLE||')' NAMA
										FROM MS_PENGGUNA A
										JOIN MS_ROLE B ON A.MS_ROLE_ID=B.ID_INC")->result();

		}else{
			$data=[];
		}

		$this->load->view('Auth/formlogin2',$data);

	}

	function proses(){
 		$username =trim($_POST['username']);
		$password =trim($_POST['password']);

		if(ENVIRONMENT=='development'){
			if($password=='pass'){
				$sql  ="SELECT A.*,NAMA_ROLE,NAMA_UNIT
						FROM MS_PENGGUNA A
						JOIN MS_ROLE B ON A.MS_ROLE_ID=B.ID_INC
						JOIN UNIT_UPT C ON C.ID_INC=A.UNIT_UPT_ID
						WHERE LOWER(USERNAME)=?";
				$query    =$this->db->query($sql,array($username))->row();
			}else{
				$query=[];
			}

			

		}else{
			$sql  ="SELECT A.*,NAMA_ROLE,NAMA_UNIT
						FROM MS_PENGGUNA A
						JOIN MS_ROLE B ON A.MS_ROLE_ID=B.ID_INC
						JOIN UNIT_UPT C ON C.ID_INC=A.UNIT_UPT_ID
						WHERE LOWER(USERNAME)=? AND LOWER(PASSWORD)=? AND STATUS=1" ;
			$query    =$this->db->query($sql,array($username,$password))->row();

		}

			


		if(count($query)>0){
				$data['pdrd'] ='1';
				$data['NIP']=$query->NIP;
				$data['NAMA']=$query->NAMA;
				$data['UNIT_UPT_ID']=$query->UNIT_UPT_ID;
				$data['MS_ROLE_ID']=$query->MS_ROLE_ID;
				$data['NAMA_ROLE']=$query->NAMA_ROLE;
				$data['NAMA_UNIT']=$query->NAMA_UNIT;
				$data['INC_USER']=$query->ID_INC;
				$row    =$this->db->query("SELECT NAMA, ALAMAT FROM WAJIB_PAJAK WHERE NPWP='$query->NIP'")->row();
				if($row){
					$data['NAMA_WP']=$row->NAMA;
					$data['ALAMAT_WP']=$row->ALAMAT;
				}
				
				$this->session->set_userdata($data);
				
			redirect('Wp/Beranda');
		}else{
			$this->session->set_flashdata('notif','<div class="badge">
                        Silahkan login dengan username dan password anda.
                        </div>');
			redirect('Auth');
		}
	}

	function logout(){
		session_destroy();
		redirect('Auth');
	}
	public function daftar(){
		$data['kec']=$this->db->query("select * from kecamatan where kodekec !='99'")->result();
        $data['kec1']=$this->db->query("select * from kecamatan where kodekec !='99'")->result();
        $data['jenis']=$this->db->query("select * from JENIS_PAJAK order by ID_INC")->result();
		    $this->template->load('halaman_depan','form_pendaftaran',$data);
		
	}
	function getkel(){
        $KODEKEC=$_POST['KODEKEC'];
        $ex= explode('|', $KODEKEC);
        $data['kc']=$this->db->query("SELECT KODEKELURAHAN,NAMAKELURAHAN FROM KELURAHAN WHERE KODEKEC ='$ex[0]' order by NAMAKELURAHAN asc")->result();
        $this->load->view('Auth/getkel',$data);
    } 
    function getkel1(){
        $KODEKEC=$_POST['KODEKEC'];
        $data['kc']=$this->db->query("SELECT KODEKELURAHAN,NAMAKELURAHAN FROM KELURAHAN WHERE KODEKEC ='$KODEKEC' order by NAMAKELURAHAN asc")->result();
        $this->load->view('Auth/getkel1',$data);
    } 
    function Proses_pendaftaran(){
    	$this->db->trans_start();
    	    $wp_id=$this->db->query("select max(no_daftar)no  from wajib_pajak_temp")->row();
    	    $id_wp=sprintf("%07d", $wp_id->NO+1);
    	    $ex= explode('|', $this->input->post('KECAMATAN',TRUE));
    	    $this->db->set('WP_ID', $id_wp);
		    //$this->db->set('NPWP', $this->input->post('NPWP',TRUE));
		    $this->db->set('NAMA', $this->input->post('NAMA_WP',TRUE));
		    $this->db->set('JENISWP', $this->input->post('JENIS_WP',TRUE));
		    $this->db->set('ALAMAT', $this->input->post('ALAMAT_WP',TRUE));
		    $this->db->set('KECAMATAN', $ex[0]);
		    $this->db->set('KELURAHAN', $this->input->post('KELURAHAN',TRUE));
		    $this->db->set('NO_TELP', $this->input->post('NO_TELP',TRUE));
		    $this->db->set('NO_BUKTI_DIRI', $this->input->post('KTP',TRUE));
		    
		    $this->db->set('NAMA_INSTANSI', $this->input->post('NAMA_INSTANSI',TRUE));
		    $this->db->set('ALAMAT_INSTANSI', $this->input->post('ALAMAT_INSTANSI',TRUE));
		    $this->db->set('NO_DAFTAR', $wp_id->NO+1);
		    $this->db->set('TGL_DAFTAR', "sysdate",false);
		    $this->db->set('STATUS', '0');//ststus 0 menunggu, 1 setuju, 3 ditolak
		    $this->db->set('EMAIL', $this->input->post('EMAIL',TRUE));
    		$step1=$this->db->insert('WAJIB_PAJAK_TEMP');
    		if ($step1==true) {
    			$this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
			    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
			    //$this->db->set('NPWPD', $this->input->post('NPWP',TRUE));
			    $this->db->set('KODEKEC', $this->input->post('KECAMATAN_USAHA',TRUE));
			    $this->db->set('KODEKEL', $this->input->post('KELURAHAN_USAHA',TRUE));
			    $this->db->set('JENIS_PAJAK', $this->input->post('JENIS_USAHA',TRUE));
			    //$step2=$this->db->insert('TEMPAT_USAHA_temp');
			    if ($step2==true) {
			    	//$this->db->set('NIP', $this->input->post('NPWP',TRUE));
				    $this->db->set('NAMA', $this->input->post('NAMA_WP',TRUE));
				    $this->db->set('UNIT_UPT_ID', $ex[1]);
				    $this->db->set('MS_ROLE_ID', '8');
				    $this->db->set('USERNAME', $this->input->post('EMAIL',TRUE));
				    $this->db->set('PASSWORD', $this->input->post('PASSWORD1',TRUE));
				    $this->db->set('TANGGAL_INSERT', "sysdate",false);
				    $this->db->set('EMAIL', $this->input->post('EMAIL',TRUE));
				    $this->db->set('STATUS', '0');//ststus 0 menunggu, 1 setuju, 3 ditolak
				    $this->db->set('WP_ID',$id_wp);
				    $step3=$this->db->insert('MS_PENGGUNA');
			    }
    		}
    		$this->db->trans_complete();
		    if ($this->db->trans_status() === TRUE) {
		    	$tujuan=$this->input->post('EMAIL',TRUE);
		    	//$tujuan='adypabbsi@gmail.com';
	  			$ci                  = get_instance();
				$ci->load->library('email');
				$config['protocol']  = "smtp";
				$config['smtp_host'] = "ssl://smtp.gmail.com";
				$config['smtp_port'] = "465";
				$config['smtp_user'] = "muhammad.adypranoto@gmail.com"; 
				$config['smtp_pass'] = "12345hikam";
				$config['charset']   = "utf-8";
				$config['mailtype']  = "html";
				$config['newline']   = "\r\n";			
				$ci->email->initialize($config);			
				$ci->email->from('muhammad.adypranoto@gmail.com', 'PDRD.KAB.MALANG');
				$list                = array($tujuan);
				$ci->email->to($list);
				$this->email->reply_to('no-reply@account-gmail.com', 'no replay');
				$ci->email->subject('Informasi Pendaftaran');
				$ci->email->message('Terimakasih Bapak/Ibu '.$this->input->post('NAMA_WP',TRUE).' Sudah Mendaftar di aplikasi E-Pajak, tim penetapan akan segera meng verivikasi data anda');
				$ci->email->send();
		    	echo "<script>window.alert('Pendaftaran WP Baru Berhasil');window.location='".site_url('Auth')."';</script>";
		    } else {
		    	echo "<script>window.alert('Pendaftaran WP Baru Gagal');window.location='".site_url('Auth')."';</script>";
		    }
		    
		    
    }

    function proses_pengajuan_npwpd(){
            $this->db->trans_start();
            $ex= explode('|', $this->input->post('KECAMATAN',TRUE));
            $jwp=$this->input->post('JENIS_WP',TRUE);
            $email=$this->input->post('EMAIL',TRUE);
            
            $this->db->set('NAMA', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('JENISWP',$jwp);
            $this->db->set('ALAMAT', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('KECAMATAN', $ex[0]);
            $this->db->set('KELURAHAN', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('NO_TELP', $this->input->post('TELEPON',TRUE));
            $this->db->set('BUKTIDIRI', $this->input->post('JENIS_BUKTI',TRUE));            
            $this->db->set('NO_BUKTI_DIRI', $this->input->post('NO_BUKTi',TRUE));            
            $this->db->set('JABATAN', $this->input->post('JABATAN',TRUE)); 
            $this->db->set('NAMA_BADAN', $this->input->post('NAMA_USAHA',TRUE)); 

            
            $this->db->set('TGL_DAFTAR', "sysdate",false);
            $this->db->set('S_VALID', "sysdate",false);
            $this->db->set('STATUS', '0');//ststus 0 menunggu, 1 setuju, 3 ditolak
            $this->db->set('EMAIL', $email);
            $this->db->set('UPT', $this->session->userdata('UNIT_UPT_ID'));
            $this->db->insert('WAJIB_PAJAK_TEMP');
          
                $wp_id=$this->db->query("select max(ID_INC_WP_TEMP)no from WAJIB_PAJAK_temp")->row();
               
                /*$tmp_file=$_FILES['file_ktp']['tmp_name'];
                $name_file =$_FILES['file_ktp']['name'];

                $tmp_file1=$_FILES['file_daftar']['tmp_name'];
                $name_file1 =$_FILES['file_daftar']['name'];*/

                    /*if($name_file!=''){
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp='ID_'.$wp_id->NO.'.'.$extensi;
                        $nf='upload/ktp/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                    if($name_file1!=''){
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_form='F_'.$wp_id->NO.'.'.$extensi;
                        $nf='upload/form_daftar/'.$img_name_form;
                        move_uploaded_file($tmp_file1,$nf);
                    }*/
                /*$this->db->query("UPDATE WAJIB_PAJAK_TEMP SET FILE_BUKTI_DIRI='$img_name_ktp',FILE_FORMULIR='$img_name_form' WHERE ID_INC_WP_TEMP='$wp_id->NO'");*/
                $id_wp=$wp_id->NO;
                $NAMA_USAHA= $this->input->post('NAMA_USAHA',TRUE);
                $ALAMAT_USAHA= $this->input->post('ALAMAT_USAHA',TRUE);
                $KODEKEC= $this->input->post('KECAMATAN_USAHA',TRUE);
                $KODEKEL= $this->input->post('KELURAHAN_USAHA',TRUE);
                $JENIS_PAJAK= $this->input->post('JENIS_USAHA',TRUE);
                $ID_OP= $this->input->post('GOLONGAN',TRUE);

                $this->db->query("INSERT INTO TEMPAT_USAHA_TEMP (NAMA_USAHA,ALAMAT_USAHA,NPWPD,KODEKEC,KODEKEL,JENIS_PAJAK,ID_OP,STATUS,REF_ID_INC_WP_TEMP) 
                                 VALUES('$NAMA_USAHA','$ALAMAT_USAHA','','$KODEKEC','$KODEKEL','$JENIS_PAJAK','$ID_OP','0','$id_wp')");;
              
            $this->db->trans_complete();

            if ($this->db->trans_status() === TRUE) {
            	//$tujuan=$this->input->post('EMAIL',TRUE);
		    	$tujuan=$email;
		        //$tujuan='adypabbsi@gmail.com';
		        			$ci                  = get_instance();
                            $ci->load->library('email');
                            $config['protocol']  = "smtp";
                            $config['smtp_host'] = "ssl://smtp.gmail.com";
                            $config['smtp_port'] = "465";
                            $config['smtp_user'] = "malangbapendakab@gmail.com"; 
                            $config['smtp_pass'] = "B4p3nd4km";
                            $config['charset']   = "utf-8";
                            $config['mailtype']  = "html";
                            $config['newline']   = "\r\n";          
                            $ci->email->initialize($config);            
                            $ci->email->from('Bapenda Kab. Malang', 'PDRD.KAB.MALANG');
                            $list                = array($tujuan);
                            $ci->email->to($list);
                            $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                            $ci->email->subject('Pengajuan NPWPD');
                            $messageFromHtml = 'Terimakasih Bapak/Ibu '.$this->input->post('NAMA_WP',TRUE).' Sudah Mendaftar di aplikasi Si PANJI, tim penetapan NPWPD akan segera meng verivikasi data anda';
                            $ci->email->message($messageFromHtml);
                            //$ci->email->message('Pembayaran Tagihan Nomor virtual Account '.$q->VIRTUAL_ACCOUNT.' dengan nominal Rp. '.number_format($TAGIHAN,0,",",".").' Berhasil');
                            $ci->email->send();
                echo "<script>window.alert('Pengajuan WP Baru Berhasil');window.location='".site_url('Auth')."';</script>";
            } else {
                echo "<script>window.alert('Pengajuan WP Baru Gagal');window.location='".site_url('Auth')."';</script>";
            }
            
            
    }

    function getgol(){
        $ID_INC=$_POST['ID_INC'];
        //HOTEL
        if ($ID_INC=='01') {

        $data['gol']=$this->Msptpd_hotel->getGol();
        }
        //RESTORAN
        if ($ID_INC=='02') {

        $data['gol']=$this->Msptpd_restoran->getGol();;
        }
        //HIBURAN
        if ($ID_INC=='03') {

        $data['gol']=$this->Mhiburan->getGol();
        }
        //PPJ
        if ($ID_INC=='05') {

        $data['gol']=$this->Msptpd_ppj->getGol();
        }
        //Parkir
        if ($ID_INC=='07') {

        $data['gol']=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM OBJEK_PAJAK WHERE ID_OP=456 ")->result();
        }
        if ($ID_INC=='08') {

        $data['gol']=$this->Mair->Gol();
        }
        if ($ID_INC=='09') {

        $data['gol']=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM OBJEK_PAJAK WHERE ID_OP=459")->result();
        }
        $this->load->view('Master/getgol',$data);
    }
   

}
