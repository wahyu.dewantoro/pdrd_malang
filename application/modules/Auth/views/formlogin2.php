<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sistem Pajak Daerah Retribusi Daerah</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url('loginasset/')?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('loginasset/')?>css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url('loginasset/')?>images/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="<?php echo base_url('Auth/proses')?>" method="post">
					<span class="login100-form-logo"> <img src="<?php echo base_url('loginasset/')?>images/icons/logo.jpeg" alt="">
					</span>
<?php echo $this->session->flashdata('notif')?>
					<span class="login100-form-title p-b-34 p-t-27">
						SIPANJI <br>
Kab. Malang
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<?php if(ENVIRONMENT=='development'){?>
						<select class="form-control" name="username">
							<option value="">Pilih</option>
							<?php foreach($res as $rk){?>
							<option value="<?= $rk->USERNAME ?>"><?= $rk->NAMA ?></option>
							<?php } ?>
						</select>
						<?php }else{?>
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
						<?php } ?>
						
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button> &nbsp;&nbsp;
						<a href="<?php echo base_url('Auth/daftar');?>" class="login100-form-btn">
							Daftar WP Baru ?
						</a>
					</div>
					<div class="container-login-form-btn">
						
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url('loginasset/')?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('loginasset/')?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('loginasset/')?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('loginasset/')?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('loginasset/')?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('loginasset/')?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url('loginasset/')?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('loginasset/')?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('loginasset/')?>js/main.js"></script>

</body>
</html>