
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

 
    <title>Sistem Pajak Daerah Retribusi Daerah</title>
    <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="<?php echo base_url().'gentelella/'?>wizard/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'gentelella/'?>wizard/assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'gentelella/'?>wizard/assets/css/form-elements.css">
        <link rel="stylesheet" href="<?php echo base_url().'gentelella/'?>wizard/assets/css/style.css">

        
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url().'gentelella/'?>wizard/assets/ico/apple-touch-icon-57-precomposed.png"><script src="<?php echo base_url().'gentelella/'?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>    <!-- iCheck -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    
    <script src="<?php echo base_url().'AdminLTE/'?>plugins/chosen/chosen.jquery.js" type="text/javascript"></script>    

    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>    
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url().'gentelella/'?>build/css/custom.min.css" rel="stylesheet">
    <script src="<?php echo base_url().'gentelella/'?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css') ?>"/>
    <!-- iCheck -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <script src="<?php echo base_url().'AdminLTE/'?>plugins/chosen/chosen.jquery.js" type="text/javascript"></script>    
       <!-- Select2 -->
    <link href="<?php echo base_url().'gentelella/'?>vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>    
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url().'gentelella/'?>build/css/custom.min.css" rel="stylesheet">
    <script src="<?php echo base_url().'gentelella/'?>datetimepicker/jquery.datetimepicker.full.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'gentelella/'?>datetimepicker/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
    <link href="<?php echo base_url().'gentelella/'?>datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css" />    <script type="text/javascript" src="<?php echo base_url().'gentelella/datepicker/'?>bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url().'gentelella/datepicker/'?>bootstrap-datepicker3.css"/>
    <!-- Datatables -->
    
    <link href="<?php echo base_url().'gentelella/'?>/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'gentelella/'?>/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'gentelella/'?>/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'gentelella/'?>/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  </head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col footer_fixed">
          <div class="left_col scroll-view">
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img  src="<?php echo base_url('loginasset/')?>images/icons/logo.jpeg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info" >
                <span style="color:#011C53;"></span>
                <h2>REGISTRASI</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br /><br />
            <br /><br />
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">                 
                  <li><a href="<?php echo base_url('Auth');?>"><i class="fa fa-lock"></i>LOGIN</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars" style="color: #011C53;"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class=" fa fa-angle-down"></span>
                  </a>
                </li>
                
                
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <?php echo $contents; ?> 
          </div>
          </div> 
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Copyright © 2018 All rights reserved</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
  </body>
  <!-- Javascript -->
        <script src="<?php echo base_url().'gentelella/'?>wizard/assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url().'gentelella/'?>wizard/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url().'gentelella/'?>wizard/assets/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo base_url().'gentelella/'?>wizard/assets/js/retina-1.1.0.min.js"></script>
        <script src="<?php echo base_url().'gentelella/'?>wizard/assets/js/scripts.js"></script> 
</html>

   

        
        
     