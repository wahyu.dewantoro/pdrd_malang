
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Formulir Pendaftaran</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <form role="form" method="post" class="f1" action="<?php echo base_url('Auth/Proses_pendaftaran');?>">
                        <div class="f1-steps">
                          <div class="f1-progress">
                          <div class="f1-progress-line" data-now-value="22.66" data-number-of-steps="4" style="width: 14.66%;"></div>
                          </div>
                          <div class="f1-step active">
                            <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                            <p style="font-size:13px;">Data Diri</p>
                            <p><h4>STEP 1</h4></p>
                          </div>
                          <div class="f1-step">
                            <div class="f1-step-icon"><i class="fa fa-building"></i></div>
                            <p>Instansi</p>
                            <p><h4>STEP 2</h4></p>
                          </div>
                         <!--  <div class="f1-step">
                            <div class="f1-step-icon"><i class="fa fa-file"></i></div>
                            <p>Usaha</p>
                            <p><h4>STEP 3</h4></p>
                          </div> -->
                          <div class="f1-step">
                            <div class="f1-step-icon"><i class="fa fa-lock"></i></div>
                            <p>Akun Login</p>
                            <p><h4>STEP 4</h4></p>
                          </div>
                        </div>
                        <div class="x_title garis">
                          <div class="clearfix"></div>
                        </div>
                            <fieldset>
                            <!-- <h4>Pendaftaran Wajib Pajak</h4><br/> -->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>NAMA</h4> </label>
                                  <div class="col-md-9">
                                    <input type="text" name="NAMA_WP" class="form-control col-md-7 col-xs-12" placeholder="Nama Wajib Pajak">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-xs-12"> <h4>ALAMAT</h4></label>
                                    <div class="col-md-9">
                                    <textarea name="ALAMAT_WP" placeholder="Alamat..." class="f1-about-yourself form-control" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>EMAIL</h4> </label>
                                  <div class="col-md-9">
                                    <input type="text" name="EMAIL" class="form-control col-md-7 col-xs-12" placeholder="Email">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>NO KTP</h4> </label>
                                  <div class="col-md-9">
                                    <input max="15" type="text" name="KTP" class="form-control col-md-7 col-xs-12" placeholder="No KTP">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>KECAMATAN</h4> </label>
                                  <div class="col-md-9">
                                    <select onchange="getkel(this);"  style="font-size:12px" name="KECAMATAN" class="form-control chosen col-md-7 col-xs-12">
                                      <option value="">Pilih</option>
                                      <?php foreach($kec as $kec){ ?>
                                      <option value="<?php echo $kec->KODEKEC.'|'.$kec->KODE_UPT?>"><?php echo $kec->NAMAKEC ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>KELURAHAN</h4> </label>
                                  <div class="col-md-9">
                                    <select  style="font-size:12px" id="KELURAHAN" name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option>                       
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>TELEPON</h4> </label>
                                  <div class="col-md-9">
                                    <input type="text" name="TELEPON" class="form-control col-md-7 col-xs-12" placeholder="Telepon">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>FOTO KTP</h4> </label>
                                  <div class="col-md-9">
                                    <input type="file" name="file" class="form-control col-md-7 col-xs-12">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>FILE PENDAFTARAN</h4> </label>
                                  <div class="col-md-9">
                                    <input type="file" name="file" class="form-control col-md-7 col-xs-12">
                                  </div>
                                </div>                              
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-next">Berikutnya</button>
                                </div>
                              </div>
                            </fieldset>

                            <fieldset>
                                 <!-- <h4>Data Instansi</h4><br/> -->
                                <div class="col-md-6">  
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>JENIS WP</h4> </label>
                                  <div class="col-md-8">
                                    <select name="JENIS_WP" class="form-control chosen col-md-7 col-xs-12">>
                                      <option value="">Pilih Jenis WP</option>
                                      <option value="1">Perorangan</option>
                                      <option value="2">Badan Hukum</option>
                                    </select>
                                  </div>
                                </div>                              
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>NAMA INSTANSI</h4> </label>
                                  <div class="col-md-8">
                                    <input type="text" name="NAMA_INSTANSI" class="form-control col-md-7 col-xs-12" placeholder="Nama Instansi">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4 col-xs-12"> <h4>ALAMAT INSTANSI</h4></label>
                                    <div class="col-md-8">
                                    <textarea name="ALAMAT_INSTANSI" placeholder="Alamat Instansi..." class="f1-about-yourself form-control" ></textarea>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Sebelumnya</button>
                                    <button type="button" class="btn btn-next">Berikutnya</button>
                                    <!-- <button type="" class="btn btn-submit" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')" >Selesai</button> -->
                                </div>                                
                              </div>
                              <div class="col-md-6">
                              </div> 
                            </fieldset>

                           <!--  <fieldset>
                                <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>JENIS WP</h4> </label>
                                  <div class="col-md-9">
                                    <select name="JENIS_WP" class="form-control chosen col-md-7 col-xs-12">>
                                      <option value="">Pilih Jenis WP</option>
                                      <option value="1">Perorangan</option>
                                      <option value="2">Badan Hukum</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>NAMA USAHA</h4> </label>
                                  <div class="col-md-9">
                                    <input type="text" name="NAMA_USAHA" class="form-control col-md-7 col-xs-12" placeholder="Nama Usaha">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-xs-12"> <h4>ALAMAT USAHA</h4></label>
                                    <div class="col-md-9">
                                    <textarea name="ALAMAT_USAHA" placeholder="Alamat..." class="f1-about-yourself form-control" ></textarea>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>KECAMATAN</h4> </label>
                                  <div class="col-md-9">
                                    <select onchange="getkel1(this);"  style="font-size:12px" name="KECAMATAN_USAHA" class="form-control chosen col-md-7 col-xs-12">
                                      <option value="">Pilih</option>
                                      <?php foreach($kec1 as $keca){ ?>
                                      <option value="<?php echo $keca->KODEKEC?>"><?php echo $keca->NAMAKEC ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>KELURAHAN</h4> </label>
                                  <div class="col-md-9">
                                    <select  style="font-size:12px" id="KELURAHAN1" name="KELURAHAN_USAHA"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option>                       
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-xs-12"> <h4>JENIS USAHA</h4> </label>
                                  <div class="col-md-9">
                                    <select  style="font-size:12px"  name="JENIS_USAHA"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                      <option value="">Pilih</option>
                                      <?php foreach($jenis as $jns){ ?>
                                      <option value="<?php echo $jns->ID_INC?>"><?php echo $jns->NAMA_PAJAK ?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Sebelumnya</button>
                                    <button type="button" class="btn btn-next">Berikutnya</button>
                                </div>
                              </div>
                            </fieldset> -->
                            <fieldset>
                                <!-- <h4>Social media profiles:</h4> -->
                                <div class="col-md-7">
                                <div class="form-group">
                                  <label class="control-label col-md-4 col-xs-12"> <h4>PASSWORD LOGIN</h4> </label>
                                  <div class="col-md-8">
                                    <input type="password"  id="pass1" name="PASSWORD1" class="form-control col-md-7 col-xs-12" placeholder="Password">
                                  </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="control-label col-md-4 col-xs-12"> <h4>TULIS ULANG PASSWORD LOGIN</h4></label>
                                    <div class="col-md-8">
                                      <input type="password"  id="pass2" name="PASSWORD1" class="form-control col-md-7 col-xs-12" placeholder="Password">
                                    </div>
                                </div> -->
                                 <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Sebelumnya</button>
                                    <button type="" class="btn btn-submit" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')" >Selesai</button>
                                </div>
                              </div>
                              <div class="col-md-5">
                              </div>
                            </fieldset>
                      
                      </form>
                  </div>
                </div>
              </div>
            </div>
   
<script type="text/javascript">
  function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Auth/Auth/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
</script>

<script type="text/javascript">
  function getkel1(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Auth/Auth/getkel1'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN1").html(msga);
          }
        });    
    }
</script>
<style type="text/css">
  .garis{
    border-bottom: 3px solid #436ED5;
  }
</style>
