<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>

 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRealisasiReklame.xls");  ?>
  <h3>Laporan Realisasi Pajak Reklame Bulan <?php echo $namabulan[date('m')]?></h3>

        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tgl Bayar</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Lokasi Pasang</th>
              <th class="text-center">KEC</th>
              <th class="text-center">KEL</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">TEKS</th>
              <th class="text-center">P/L</th>
              <th class="text-center">SISI</th>
              <th class="text-center">JUMLAH</th>
              <th class="text-center">AWAL PAJAK</th>
              <th class="text-center">AKHIR PAJAK</th>
              <th class="text-center">KETETAPAN</th>
              <th class="text-center">KODE REK</th>          
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
            <tr>
              <td  align="center"><?php echo $no ?></td>
                <td><?= $rk->TGL_TRX?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->LOKASI_PASANG?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td><?= $rk->TEKS?></td>
                <td><?= number_format($rk->P,1,",",".").' x '.number_format($rk->L,1,",",".")?></td>
                <td align="right"><?= $rk->S?></td>
                <td align="right"><?= $rk->JUMLAH?></td>
                <td><?= $rk->BERLAKU?></td>
                <td><?= $rk->AKHIR_BERLAKU?></td>
                <td align="right"><?=  $rk->KETETAPAN?></td>
                <td><?= $rk->NOREK?></td>
            </tr>
            <?php $no++;}?>
          </tbody>
        </table>