<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanPengajuanNpwpd.xls");  ?>
  <h3>Laporan Pengajuan NPWPD</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">JENIS</th>
              <th class="text-center">NAMA WP</th>
              <th class="text-center">JABATAN</th>
              <th class="text-center">ALAMAT</th>
              <th class="text-center">KELURAHAN</th>
              <th class="text-center">KECAMATAN</th>
              <th class="text-center">HP</th>
              <th class="text-center">NAMA USAHA</th>
              <th class="text-center">KTP</th>
              <th class="text-center">FORMULIR</th>
              <th class="text-center">TGL DAFTAR</th>
              <th class="text-center">UPT</th> 
              <th class="text-center">STATUS</th>                            
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
              <tr>
                <td  align="center"><?php echo $no ?></td>
                <td><?php if ($rk->NPWP=='') {echo "Pengajuan";} else {echo "'".$rk->NPWP;}?></td>
                <td><?php if ($rk->JENISWP=='1') {
                  echo "Perorangan";
                } else if ($rk->JENISWP=='2') {
                  echo "Badan";
                } else {
                    
                }?></td>
                <td><?= strtoupper($rk->NAMA)?></td>
                <td><?= strtoupper($rk->JABATAN)?></td>
                <td><?= $rk->ALAMAT?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NO_TELP?></td>
                <td><?= $rk->NAMA_BADAN?></td>
                <td><a href="<?php echo base_url('upload/ktp').'/'.$rk->FILE_BUKTI_DIRI ?>" target="_blank"><?= $rk->FILE_BUKTI_DIRI?></a></td>
                <td><a href="<?php echo base_url('upload/form_daftar').'/'.$rk->FILE_FORMULIR ?>" target="_blank"><?= $rk->FILE_FORMULIR?></a></td>
                <td><?= $rk->TGL_DAFTAR?></td>
                <td><?= $rk->NAMA_UNIT?></td>
                <td><?php if ($rk->STATUS==0) {echo "Pengajuan";} else if ($rk->STATUS==2) {echo "Ditolak";} else {echo "Disetujui";}?></td>
              </tr>
              <?php $no++;}?>
          </tbody>
        </table>