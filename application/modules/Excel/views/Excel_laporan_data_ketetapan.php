   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=LaporanDataKetetapan.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<div class="page-title">
 <div class="title_left">
  <h3>Laporan Data Ketetapan</h3>
</div>
        <table id="example2" class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Kode Biling</th>
              <th>NPWPD</th>
              <th>Nama WP /Objek Pajak</th>
              <th>Alamat</th>
              <th>DPP</th>
              <th>Pajak Terutang</th>
              <th>Tgl Ditetapkan</th>
              <th>Jatuh Tempo</th> 
              <th>Status</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1; foreach ($data as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo $no?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td><?= $rk->DPP?></td>
                <td align="right"><?=  $rk->PAJAK_TERUTANG?></td>
                <td><?= $rk->TGL_KETETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?= $rk->STATUS?></td>
              </tr>
              <?php $no++ ;} ?>
            </tbody>
          </table>

