<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<?php if ($this->session->userdata('rekap_bul_upt')==null) {
       $nama='SemuaUPT';
    }else{
      $nama="UPT_".$upt->NAMA_UNIT;
    }?></h3>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRekap".$nama.".xls");  ?>
  <h3>Laporan Rekap Bulanan <?php if ($this->session->userdata('rekap_bul_upt')==null) {
        echo "Semua UPT";
    }else{
        echo "UPT ".$upt->NAMA_UNIT;
    }?></h3>

        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th>NPWPD</th>
              <th>NAMA</th>
              <th>OBJEK</th>
              <th>NO REK</th>
              <th>JAN</th>
              <th>FEB</th>
              <th>MAR</th>
              <th>APR</th>
              <th>MEI</th>
              <th>JUN</th>
              <th>JUl</th>
              <th>AGU</th>
              <th>SEP</th>
              <th>OKT</th>
              <th>NOV</th>
              <th>DES</th>
              <th>JUMLAH</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($rekap as $rk){?>
            <tr>
              <td>'<?= $rk->NPWPD;?></td>
              <td><?= $rk->NAMA_WP;?></td>
              <td><?= $rk->OBJEK_PAJAK;?></td>
              <td></td>
              <td><?= $rk->JAN;?></td>
              <td><?= $rk->FEB;?></td>
              <td><?= $rk->MAR;?></td>
              <td><?= $rk->APR;?></td>
              <td><?= $rk->MEI;?></td>
              <td><?= $rk->JUN;?></td>
              <td><?= $rk->JUL;?></td>
              <td><?= $rk->AGU;?></td>
              <td><?= $rk->SEP;?></td>
              <td><?= $rk->OKT;?></td>
              <td><?= $rk->NOV;?></td>
              <td><?= $rk->DES;?></td>
              <td><?= $rk->JUMLAH;?></td>
            </tr>
            <?php $no++;}?>
          </tbody>
        </table>