<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>

 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRealisasiAirTanah.xls");  ?>
  <h3>Laporan Realisasi Pajak Air Tanah </h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Kode Biling</th>
              <th>NPWPD</th>
              <th>Nama WP</th>
              <th>Objek Pajak</th>
              <th>Alamat</th>
              <th>Desa</th>   
              <th>Kecamatan</th>
              <th>Cara Hitung</th>
              <th>Tgl Terbit</th> 
              <th>Ketetapan</th>
              <th>Realisasi</th>          
              <th>Tunggakkan</th>
              <th>Tgl Lunas</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
            <tr>
              <td><?= $no?></td>
              <td>'<?= $rk->KODE_BILING?></td>
              <td>'<?= $rk->NPWPD?></td>
              <td><?= $rk->NAMA_WP?></td>
              <td><?= $rk->ALAMAT_WP?></td>
              <td><?= $rk->OBJEK_PAJAK?></td>
              <td><?= $rk->ALAMAT_OP?></td>
              <td><?= $rk->NAMAKELURAHAN?></td>
              <td><?= $rk->NAMAKEC?></td>
              <td><?= $rk->CARA_HITUNG?></td>
              <td><?= $rk->TGL_PENETAPAN?></td>
              <td align="right"><?= $rk->REALISASI?></td>
              <td align="right"><?= $rk->DENDA?></td>
              <td><?= $rk->DATE_BAYAR?></td>
            </tr>
              
            <?php $no++;}?>
          </tbody>
        </table>