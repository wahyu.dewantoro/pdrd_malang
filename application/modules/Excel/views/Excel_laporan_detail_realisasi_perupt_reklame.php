<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanDetailRealisasiUpt.xls");  ?>
  <h3>Laporan Detail Realisasi</h3>
        <table class="table table-striped table-bordered" border="1">
        <thead>
            <tr>
              <th class="text-center">No</th>            
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama Wp</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Kec Pasang</th>
              <th class="text-center">Kel Pasang</th>
              <th class="text-center">Teks</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">No Rek</th>
              <th class="text-center">Tgl bayar</th>
            </tr>
          </thead>
          <tbody>
            <?php $tot=0;$no=1; foreach($detail_reklame as $rk){?>
            <tr>
              <td ><?= $no;?></td>
              <td align="right">'<?= $rk->KODE_BILING?></td>
              <td >'<?= $rk->NPWPD?></td>
              <td ><?= $rk->NAMA_WP?></td>
              <td ><?= $rk->OBJEK_PAJAK?></td>
              <td ><?= $rk->DESKRIPSI?></td>
              <td ><?= $rk->NAMAKEC?></td>
              <td ><?= $rk->NAMAKELURAHAN?></td>
              <td ><?= $rk->TEKS?></td>
              <td align="right"></td>
              <td align="right"><?= $rk->KETETAPAN?></td>
              <td ><?= $rk->NOREK?></td>
              <td ><?= $rk->TGL_TRX?></td>
            </tr>
              <?php $no++;$tot+=$rk->KETETAPAN;}?>
            <tr>
              <td colspan="8" class="tede" align="right"><b>TOTAL </b></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"><b><?= $tot?></b></td>
            </tr>
          </tbody>
        </table>