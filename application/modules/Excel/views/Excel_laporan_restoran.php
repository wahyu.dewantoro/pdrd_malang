<style>
  
  .num {
  mso-number-format:""
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSPTPDPajakRestoran.xls");  ?>
  <h3>Laporan SPTPD Pajak Restoran</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">SKPD</th>
              <th class="text-center">Jumlah Meja</th>
              <th class="text-center">Harga rata-rata</th>
              <th class="text-center">Meja terisi</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($restoran as $rk){?>
              <tr>
                <td  align="center"><?php echo $no ?></td>
                <td class="">'<?= $rk->KODE_BILING?></td>
                <td class="">'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?php if ($rk->JENIS_RESTO=='1') {
                  echo "YA";} else { echo "TIDAK";}?></td>
                <td align="right"><?=  $rk->JUMLAH_MEJA?></td>
                <td align="right"><?=  $rk->TARIF_RATA_RATA?></td>
                <td align="right"><?=  $rk->MEJA_TERISI?></td>
                <td align="right"><?=  $rk->DPP?></td>
                <td align="right"><?=  $rk->TAGIHAN?></td>
                <td align="right"><?=  $rk->DENDA?></td>
                <td align="right"><?=  $rk->POTONGAN?></td>
                <td align="right"><?=  $rk->JUMLAH_KETETAPAN?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
              </tr>
              
            <?php $no++;}?>
            <tr>
              <td colspan="14" align="right">Total Tagihan</td>
              <td><?php echo $total_tagihan->JUMLAH?></td>
            </tr>
          </tbody>
        </table>