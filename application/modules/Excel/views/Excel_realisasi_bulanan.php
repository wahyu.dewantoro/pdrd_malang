   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRealisasi.xls");  ?>
  <h3>Realisasi Bulanan <?php echo $namabulan[(int)$this->session->userdata('bulan')];?></h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
           <tr>
              <th class="teha">No</th>
              <th class="teha">Pajak</th>
              <th class="teha">Bulan <?php echo $namabulan[(int)$this->session->userdata('bulan')];?></th>
            </tr>
          </thead>
          <tbody>
            <?php $target=0;$no=1; foreach($bulanan as $rk){?>
            <tr>
              <td class="tede" align="center"><?php echo $no?></td>
              <td class="tede"><?= $rk->NAMA_PAJAK;?></td>
              <td class="tede" align="right"><?= $rk->JUMLAH?></td>
            </tr>
              <?php $no++; $target+=$rk->JUMLAH;}?>
            <tr>
              <td colspan="2" class="tede" align="right"><b>TOTAL </b></td>
              <td class="tede" align="right"><b><?= $target;?>
            </tr>
          </tbody>
        </table>