<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRealisasiUpt.xls");  ?>
  <h3>Laporan Realisasi</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tgl Trx</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Masa</th>
              <th class="text-center">Tahun</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Desa</th>
              <th class="text-center">Nominal</th>
              <th class="text-center">Kode Rek</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1; foreach($data as $rk){?>
              <tr>
                <td  align="center"><?php echo $no ?></td>
                <td><?= $rk->TGL_LUNAS?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td><?= $rk->MASA_PAJAK?></td>
                <td><?= $rk->TAHUN_PAJAK?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td align="right"><?=  $rk->JUMLAH_BAYAR?></td>
                <td><?= $rk->KODE_REK?></td>            
              </tr>
               <?php $no++;}?>
            </tbody>
          </table>