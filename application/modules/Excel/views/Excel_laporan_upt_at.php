<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRekapPajakAirTanah.xls");  ?>
  <h3>Laporan Rekap Pajak Air Tanah </h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th width="20%" class="num">Kode Biling</th>
              <th class="text-center">Masa</th>
              <th class="text-center">Tahun</th>
              <th width="20%" class="num">NPWPD</th>
               <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Kelurahan</th>
              <th class="text-center">Cara Pengambilan</th>

              <th class="text-center">Peruntukan</th>
              <th class="text-center">Volume Air</th>
              <th class="text-center">Debit Air</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
            <tr>
                <td><?= $no ?></td>
                <td class="num">'<?= $rk->KODE_BILING?></td>
                <td ><?= $namabulan[$rk->MASA_PAJAK]?></td>
                <td ><?= $rk->TAHUN_PAJAK?></td>
                <td class="num">'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->CARA_PENGAMBILAN?></td>
                <td><?= $rk->JENIS?></td>
                <td align="right"><?=  $rk->VOLUME_AIR_METER?></td>
                <td align="right"><?=  $rk->DEBIT_NON_METER?></td>
                <td align="right"><?=  $rk->DPP?></td>
                <td align="right"><?=  $rk->TAGIHAN?></td>
                <td align="right"><?=  $rk->DENDA?></td>
                <td align="right"><?=  $rk->JUMLAH_KETETAPAN?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
            </tr>
              
            <?php $no++;}?>
          </tbody>
        </table>