<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<?php if ($this->session->userdata('dbhd_kecamatan')==null) {
       $nama='SemuaKecamtan';
    }else{
      $nama="KEC".$kec->NAMAKEC;
    }?></h3>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanDanaBagihasilDesa".$nama.".xls");  ?>
  <h3>LAPORAN DANA BAGI HASIL DESA  <?php if ($this->session->userdata('dbhd_kecamatan')==null) {
        echo "SEMUA KECAMATAN";
    }else{
        echo "KECAMATAN ".$kec->NAMAKEC;
    }?> TAHUN <?php echo $this->session->userdata('dbhd_tahun')?></h3>

        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th>NO</th>
              <th>KEC</th>
              <th>DESA</th>
              <th>JAN</th>
              <th>FEB</th>
              <th>MAR</th>
              <th>APR</th>
              <th>MEI</th>
              <th>JUN</th>
              <th>JUl</th>
              <th>AGU</th>
              <th>SEP</th>
              <th>OKT</th>
              <th>NOV</th>
              <th>DES</th>
              <th>JUMLAH</th>
              <th>DBH</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($bhd as $rk){?>
            <tr>
              <td><?= $no;?></td>
              <td><?= $rk->NAMAKEC;?></td>
              <td><?= $rk->NAMAKELURAHAN;?></td>
              <td><?= $rk->JAN;?></td>
              <td><?= $rk->FEB;?></td>
              <td><?= $rk->MAR;?></td>
              <td><?= $rk->APR;?></td>
              <td><?= $rk->MEI;?></td>
              <td><?= $rk->JUN;?></td>
              <td><?= $rk->JUL;?></td>
              <td><?= $rk->AGU;?></td>
              <td><?= $rk->SEP;?></td>
              <td><?= $rk->OKT;?></td>
              <td><?= $rk->NOV;?></td>
              <td><?= $rk->DES;?></td>
              <td><?= $rk->JUMLAH;?></td>
              <td><?= $rk->BHD;?></td>
            </tr>
            <?php $no++;}?>
          </tbody>
        </table>