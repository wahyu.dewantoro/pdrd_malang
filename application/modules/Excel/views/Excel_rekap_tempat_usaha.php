<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRekapTempatUsaha.xls");  ?>
  <h3>Laporan Rekap Tempat Usaha</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Desa</th>
              <th class="text-center">Jenis pajak</th>
              <th class="text-center">kategori</th>
              <th class="text-center">Total Pajak</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1; foreach($data as $rk){?>
              <tr>
                <td  align="center"><?php echo $no ?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td><?= $rk->DESKRIPSI?></td> 
                <td><?= $rk->REALISASI?></td>                     
              </tr>
               <?php $no++;}?>
            </tbody>
          </table>