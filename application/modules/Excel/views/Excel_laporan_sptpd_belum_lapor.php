<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSPTPDPajakReklame.xls");  ?>
  <h3>Laporan SPTPD Belum Lapor</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>             
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Alamat WP</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">ALamat OP</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Kelurahan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
              <tr>
                <td valign="top"   align="center"><?php echo $no ?></td>
                <td valign="top"  class="num"><?= $rk->NPWP?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->ALAMAT?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
              </tr>              
            <?php $no++;}?>
          </tbody>
        </table>