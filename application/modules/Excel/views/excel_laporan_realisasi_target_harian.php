
 <?php
 $tahun=date('Y');
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRealisasiTargetHarian".$tahun.".xls");  ?>
  <h3>Laporan Realisasi Harian Pajak Daerah Tahun <?php echo $tahun;?> </h3>

        <table class="table table-striped table-bordered" border="1">
          <thead>
                                <tr>
                                  <th class="teha">NO</th>
                                  <th class="teha">PAJAK</th>
                                  <th class="teha">TARGET</th>
                                  <th class="teha">S/D LALU</th>
                                 
                                  <th class="teha">HARI INI</th>
                                  <th class="teha">S/D HARI INI</th>
                                  <th class="teha"> %</th>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php $tot=0;$target=0;$sd_ll=0;$hr_ini=0;$no=1; foreach($harian as $rk){?>
                                      <tr>
                                        <td class="tede" align="center"><?php echo $no?></td>
                                        <td class="tede"><?= $rk->NAMA_PAJAK;?></td>
                                        <td class="tede" align="right"><?= number_format($rk->TARGET,'0','','.')?></td>
                                        <td class="tede" align="right"><?= number_format($rk->JUMLAH_SD_LALU,'0','','.');?></td>
                                        <td class="tede" align="right"><?= number_format($rk->JUMLAH_HARI_INI,'0','','.');?></td>
                                        <td class="tede" align="right"><?= number_format($rk->JUMLAH_SD_HARI_INI,'0','','.');?></td>
                                        <td class="tede" align="right"><?= number_format((float)$rk->PERSEN,'2','.',',');?> %</td>
                                      </tr>
                                  <?php $no++; 
                                        $tot+=$rk->JUMLAH_SD_HARI_INI;
                                        $target+=$rk->TARGET;
                                        $sd_ll+=$rk->JUMLAH_SD_LALU;
                                        $hr_ini+=$rk->JUMLAH_HARI_INI;
                                        }?>
                                  <tr>
                                    <?php $total_persen=$tot/$target*100;?>
                                    <td colspan="2" class="tede" align="right"><b>TOTAL </b></td>
                                    <td class="tede" align="right"><b><?= number_format($target,'0','','.');?>
                                    <td class="tede" align="right"><b><?= number_format($sd_ll,'0','','.');?>
                                    <td class="tede" align="right"><b><?= number_format($hr_ini,'0','','.');?>
                                    <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?></b></td>
                                    <td class="tede" align="right"><b><?= number_format((float)$total_persen,'2','.',',');?></b> %</td>
                                  </tr>
                              </tbody>
                           </table>