<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRekapPajakHotel.xls");  ?>
  <h3>Laporan Rekap Pajak Hotel </h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th width="20%" class="num">Kode Biling</th>
              <th width="20%" class="num">NPWPD</th>
              <th>Nama WP</th>
              <th>Nama Usaha</th>
              <th>Alamat Usaha</th>
              <th>Jumlah Kamar  </th>   
              <th>Tarif Rata-Rata</th>
              <th>Kamar Terisi</th>
              <th>DPP</th> 
              <th>Ketetapan</th>
              <th>Denda</th>          
              <th>Total Pajak Terutang</th>
              <th>Tgl Penetapan</th>
              <th>Jatuh Tempo</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
            <tr>
                <td><?= $no ?></td>
                <td class="num">'<?= $rk->KODE_BILING?></td>
                <td class="num">'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td align="right"><?=  $rk->JUMLAH_KAMAR?></td>
                <td align="right"><?=  $rk->TARIF_RATA_RATA?></td>
                <td align="right"><?=  $rk->KAMAR_TERISI?></td>
                <td align="right"><?=  $rk->DPP?></td>
                <td align="right"><?=  $rk->TAGIHAN?></td>
                <td align="right"><?=  $rk->DENDA?></td>
                <td align="right"><?=  $rk->JUMLAH_KETETAPAN?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
            </tr>
              
            <?php $no++;}?>
          </tbody>
        </table>