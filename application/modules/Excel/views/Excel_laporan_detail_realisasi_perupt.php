<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanDetailRealisasiUpt.xls");  ?>
  <h3>Laporan Detail Realisasi</h3>
        <table class="table table-striped table-bordered" border="1">
         <thead>
            <tr>
              <th class="text-center">No</th>            
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama Wp</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Tagihan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Jumlah Bayar</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Tgl bayar</th>
            </tr>
          </thead>
          <tbody>
            <?php $tot=0;$no=1; foreach($detail as $rk){?>
            <tr>
              <td ><?= $no;?></td>
              <td align="right">'<?= $rk->KODE_BILING?></td>
              <td >'<?= $rk->NPWPD?></td>
              <td ><?= $rk->NAMA_WP?></td>
              <td ><?= $rk->OBJEK_PAJAK?></td>
              <td align="right"><?= $rk->TAGIHAN;?></td>
              <td align="right"><?= $rk->DENDA;?></td>
              <td align="right"><?= $rk->JUMLAH_BAYAR;?></td>
              <td ><?= $rk->JATUH_TEMPO?></td>
              <td ><?= $rk->TGL_BAYAR?></td>
            </tr>
              <?php $no++;$tot+=$rk->JUMLAH_BAYAR;}?>
            <tr>
              <td colspan="5" class="tede" align="right"><b>TOTAL </b></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"><b><?= $tot;?></b></td>
            </tr>
          </tbody>
        </table>