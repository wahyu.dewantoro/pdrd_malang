<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSPTPDPajakPPJ.xls");  ?>
  <h3>Laporan SPTPD Pajak PPJ</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Keperluan</th>

              <th class="text-center">tarif</th>
              <th class="text-center">Jumlah Pemakaian</th>
              <th class="text-center">Daya terpasang</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($ppj as $rk){?>
              <tr>
                <td  align="center"><?php echo $no ?></td>
                <td class="num"><?= $rk->KODE_BILING?></td>
                <td class="num"><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->KEPERLUAN?></td>
                <td align="right"><?=  number_format($rk->TARIF,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH_PEMAKAIAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DAYA_TERPASANG,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DPP,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->TAGIHAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DENDA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->POTONGAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH_KETETAPAN,'0','','.')?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?php if ($rk->PLN=='1') {
                  echo "PLN";} else { echo "NON PLN";}?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
              </tr>
              
            <?php $no++;}?>
            <tr>
              <td colspan="14" align="right">Total Tagihan</td>
              <td><?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></td>
            </tr>
          </tbody>
        </table>