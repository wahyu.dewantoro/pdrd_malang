<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSPTPDPajakReklame.xls");  ?>
  <h3>Laporan SPTPD Pajak Reklame</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Tagihan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($reklame as $rk){?>
              <tr>
                <td valign="top"   align="center"><?php echo $no ?></td>
                <td valign="top"  class="num"><?= $rk->KODE_BILING?></td>
                <td valign="top"  class="num"><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->TAGIHAN),'0','','.')?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->DENDA),'0','','.')?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->POTONGAN),'0','','.')?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->JUMLAH_KETETAPAN),'0','','.')?></td>
                <td align="center"><?= $rk->TGL_PENETAPAN?></td>
              </tr>
              
            <?php $no++;}?>
            <tr>
              <td colspan="9" align="right">Total Tagihan</td>
              <td><?php echo number_format(str_replace(",",".",$total_tagihan->JUMLAH),'0','','.')?></td>
            </tr>
          </tbody>
        </table>