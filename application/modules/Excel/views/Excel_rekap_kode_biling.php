   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
)
 ?>
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=RekapKodeBilingg.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
  <h3>Laporan Rekap Kode Biling <?php if ($this->session->userdata('kd_bil_jenis_pajak')==null) {
        
    }else{
        echo "Pajak  ";echo $jp->NAMA_PAJAK;
    }?>
      Bulan <?php echo $namabulan[$bulan]; ?>
    </h3>

        <table id="example2" class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Kode Biling</th>
              <th>Tanggal</th>
              <th>NPWPD</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>DPP</th>
              <th>Pajak Terutang</th>
              <th>Jatuh Tempo</th>
              <th>Status</th>
             <!--  <th>Aksi</th> -->
            </tr>
          </thead>
          <tbody>
                        <?php $no=1; foreach($data as $rk){?>
            <tr>
              <td align="center"><?php echo $no?></td>
              <td>'<?= $rk->KODE_BILING;?></td>
              <td><?= $rk->TANGGAL_PENERIMAAN;?></td>
              <td>'<?= $rk->NPWPD;?></td>
              <td><?= $rk->NAMA_WP;?></td>
              <td><?= $rk->ALAMAT_WP;?></td>
              <td><?= $rk->DPP;?></td>
              <td><?= $rk->PAJAK_TERUTANG;?></td>
              <td><?= $rk->JATUH_TEMPO;?></td>
              <td><?php if ($rk->STATUS==0) {
                          echo "DITETAPKAN / BELUM LUNAS";
                      }elseif($rk->STATUS==1) {
                          echo "LUNAS";}?></td>
            </tr>
            <?php $no++;}?>
          </tbody>
        </table>
