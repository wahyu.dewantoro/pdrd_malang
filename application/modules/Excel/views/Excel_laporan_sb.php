<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSPTPDPajakSarangBurung.xls");  ?>
  <h3>Laporan SPTPD Pajak Sarang Burung</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Jenis Burung</th>

              <th class="text-center">Kepemilikan</th>
              <th class="text-center">Luas Area</th>
              <th class="text-center">Jumlah Galur</th>
              <th class="text-center">Hasil tiap Panen</th>
              <th class="text-center">Harga</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($sb as $rk){?>
              <tr>
                <td valign="top"   align="center"><?php echo $no ?></td>
                <td valign="top"  class="num"><?= $rk->KODE_BILING?></td>
                <td valign="top"  class="num"><?= $rk->NPWPD?></td>
                <td valign="top" ><?= $rk->NAMA_WP?></td>
                <td valign="top" ><?= $rk->OBJEK_PAJAK?></td>
                <td valign="top" ><?= $rk->ALAMAT_OP?></td>
                <td valign="top" ><?= $rk->JENIS_BURUNG?></td>
                <td valign="top" ><?= $rk->KEPEMILIKAN_IJIN?></td>
                <td valign="top"  align="right"><?=  number_format($rk->LUAS_AREA,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->JUMLAH_GALUR,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->HASIL_SETIAP_PANEN,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->HARGA_RATA_RATA,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->DPP,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->TAGIHAN,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->DENDA,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->POTONGAN,'0','','.')?></td>
                <td valign="top"  align="right"><?=  number_format($rk->JUMLAH_KETETAPAN,'0','','.')?></td>
                <td valign="top" ><?= $rk->TGL_PENETAPAN?></td>
                <td valign="top" ><?= $rk->JATUH_TEMPO?></td>
                <td valign="top" ><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
              </tr>
              
            <?php $no++;}?>
            <tr>
              <td colspan="16" align="right">Total Tagihan</td>
              <td><?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></td>
            </tr>
          </tbody>
        </table>