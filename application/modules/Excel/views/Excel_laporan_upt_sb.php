<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRekapPajakSarangBurung.xls");  ?>
  <h3>Laporan Rekap Pajak Sarang Burung </h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th width="20%" class="num">Kode Biling</th>
              <th width="20%" class="num">NPWPD</th>
               <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Jenis Burung</th>

              <th class="text-center">Kepemilikan</th>
              <th class="text-center">Luas Area</th>
              <th class="text-center">Jumlah Galur</th>
              <th class="text-center">Hasil tiap Panen</th>
              <th class="text-center">Harga</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
            <tr>
                <td><?= $no ?></td>
                <td class="num"><?= $rk->KODE_BILING?></td>
                <td class="num"><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->JENIS_BURUNG?></td>
                <td><?= $rk->KEPEMILIKAN_IJIN?></td>
                <td align="right"><?=  number_format($rk->LUAS_AREA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH_GALUR,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->HASIL_SETIAP_PANEN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->HARGA_RATA_RATA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DPP,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->TAGIHAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DENDA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH_KETETAPAN,'0','','.')?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
            </tr>
              
            <?php $no++;}?>
          </tbody>
        </table>