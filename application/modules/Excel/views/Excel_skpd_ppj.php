<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSKPDReklame.xls");  ?>
  <h3>Laporan SKPD Pajak PPJ PLN </h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP/Nama Usaha</th>
              <th class="text-center">Alamat</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($data as $rk){?>
            <tr>
                <td  align="center"><?php echo $no ?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td align="right"><?=  number_format($rk->DPP,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->PAJAK_TERUTANG,'0','','.')?></td>
                <td align="center"><?= $rk->TGL_KETETAPAN?></td>
                 <!-- <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td> -->
            </tr>
              
            <?php $no++;}?>
           <!--  <tr>
              <td colspan="7" align="right">Total Tagihan</td>
              <td><?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></td>
            </tr> -->
          </tbody>
        </table>