   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>

        <style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSPTPDPajakGalianDetail.xls");  ?>
  <h3>Laporan SPTPD Pajak Galian</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Masa</th>
              <th class="text-center">Kodd Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Kelurahan</th>
              <th class="text-center">Golongan</th>            
              <th class="text-center">Harga Pasar</th>
              <th class="text-center">Persen</th>
              <th class="text-center">Volume (m<sup>3</sup>)</th>
              <th class="text-center">Pajak</th>
              <th class="text-center">NOREK</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Tgl Bayar</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($galian as $rk){?>
              <tr>
                <td valign="top"   align="center"><?php echo $no ?></td>
                <td><?= $namabulan[$rk->MASA_PAJAK]?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td align="right"><?=  $rk->HARGA_DASAR?></td>
                <td align="right"><?=  $rk->PAJAK?></td>
                <td align="right"><?=  $rk->VOLUME?></td>
                <td align="right"><?=  $rk->JUMLAH_KETETAPAN?></td>
                <td align="center"><?= $rk->NOREK?></td>
                <td align="center"><?= $rk->TGL_PENETAPAN?></td>
                <td align="center"><?php if ($rk->STATUS==1) { 
                                    echo $rk->TGL_BAYAR;
                                  } else {
                                    echo "Belum bayar";
                                  }?></td>
              </tr>
              </tr>
              
            <?php $no++;}?>
            <tr>
              <td colspan="9" align="right">Total Tagihan</td>
              <td><?php echo $total_tagihan->JUMLAH?></td>
            </tr>
          </tbody>
        </table>