

 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanDetailPerforasi.xls");  ?>
  <h3>Laporan Detail Perforasi</h3>
        <table class="table table-striped table-bordered" border="1">
         <thead>
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">TGL</th>
              <th class="text-center">No BAP</th>
              <th class="text-center">Jenis Karcis</th>
              <th class="text-center">Nama Pemohon</th>
              <th class="text-center">Tempat Usaha</th>
              <th class="text-center">Kode</th>
              <th class="text-center">No urut</th>
              <th class="text-center">Jml Blok</th>
              <th class="text-center">Jml lmbr/Blok</th>
              <th class="text-center">Harga /lembar</th>
              <th class="text-center">Total</th>
              <th class="text-center">% Pajak</th>
              <th class="text-center">Nilai Pajak</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1; foreach ($det_perforasi as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo $no ?></td>
                <td><?= $rk->TGL_PERFORASI?></td>
                <td align="center"><?= $rk->NOMOR_INC_BLN.' / '.sprintf('%03d', $rk->NOMOR_INC_THN)?></td>
                <td><?php if ($rk->JENIS_KARCIS=='1') {$JNS='Karcis Masuk';} else {$JNS='Karcis Parkir';} ;
                   echo  '('.$JNS.') '.$rk->CATATAN ?></td>
                 <td><?= $rk->NAMA_WP_PERF?></td>
                 <td><?= $rk->NAMA_USAHA?></td>
                 <td><?= $rk->NOMOR_SERI?></td>
                 <td><?= $rk->NO_URUT_KARCIS?></td>
                 <td align="right"><?=  $rk->JML_BLOK?></td>
                 <td align="right"><?=  $rk->ISI_LEMBAR?></td>
                 <td align="right"><?=  $rk->NILAI_LEMBAR?></td>
                 <td align="right"><?=  $rk->TOT_BEFORE_TAX?></td>
                 <td align="right"><?=  $rk->PERSEN?> %</td>
                 <td align="right"><?=  $rk->PAJAK?></td>
              </tr>
             <?php $no++;}?>
            </tbody>
          </table>