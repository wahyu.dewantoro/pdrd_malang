   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=LaporanJatuhTempo.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<div class="page-title">
 <div class="title_left">
  <h3>Laporan Jatuh Tempo</h3>
</div>
        <table id="example2" class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>No Berkas</th>
              <th>Kode Biling</th>
              <th>NPWPD</th>
              <th>Nama WP</th>
              <th>Alamat WP</th>
              <th>Nama Usaha</th>
              <th>Alamat Usaha</th>
              <th>Tgl Ketetapan</th>
              <th>Jatuh Tempo</th>
              <th>Periode</th>
              <th>Tahun</th>
              <th>Denda</th>
              <th>Potongan</th>
              <th>Pajak Terutang</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1; foreach ($data as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo $no?></td>
                <td><?= $rk->NOMOR_BERKAS?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td align="center"><?= $rk->MASA_PAJAK?></td>
                <td align="center"><?= $rk->TAHUN_PAJAK?></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right"><?=  ceil($rk->TAGIHAN)?></td>
              </tr>
              <?php $no++ ;} ?>
            </tbody>
          </table>

