<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=rekap_tempat_usaha.xls");  ?>
  <h3>Tempat Usaha Berdasarkan Tgl Daftar</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">NAMA WP</th>
              <th class="text-center">NAMA USAHA</th>
              <th class="text-center">ALAMAT USAHA</th>
              <th class="text-center">KECAMATAN</th>
              <th class="text-center">KELURAHAN</th>
              <th class="text-center">GOLONGAN</th>
              <th class="text-center">JENIS PAJAK</th>
              <th class="text-center">HP</th>
              <th class="text-center">TGL DAFTAR</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1;foreach ($tu as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo $no++ ?></td>
                <td>'<?= $rk->NPWP?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td><?= $rk->NO_TELP?></td>                
                <td><?= $rk->TGL_DAFTAR?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>