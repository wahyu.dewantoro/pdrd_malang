<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>

 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRealisasiMBLB".$namabulan[date('m')].".xls");  ?>
  <h3>Laporan Realisasi Pajak MBLB Mineral Bulan <?php echo $namabulan[date('m')]?></h3>

        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Kode Biling</th>            
              <th>Objek Pajak</th>
              <th>Nama</th>
              <th>Desa</th>
              <th>Kecamatan</th>
              <!-- <th>No rek</th> -->
              <th>Pajak Terutang</th>
              <th>Tgl Lunas</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($restoran as $rk){?>
            <tr>
              <td align="center"><?php echo $no?></td>
              <td>'<?= $rk->KODE_BILING;?></td>
              <td><?= $rk->OBJEK_PAJAK;?></td>
              <td><?= $rk->NAMA_WP;?></td>
              <td><?= $rk->NAMAKEC;?></td>
              <td><?= $rk->NAMAKELURAHAN;?></td>
              <td align="center"><?= $rk->TAGIHAN;?></td>
              <td><?php if ($rk->DATE_BAYAR=='') {
                          echo "Belum Bayar";
                      }else {
                          echo date('d/m/Y',strtotime( $rk->DATE_BAYAR ));}?></td>
            </tr>
            <?php $no++;}?>
          </tbody>
        </table>