<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSKPDMamin.xls");  ?>
  <h3>Laporan SKPD Pajak Mamin </h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Masa</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Kelurahan</th>
              <th class="text-center">SKPD</th>
              <th class="text-center">Jumlah Meja</th>
              <th class="text-center">Harga rata-rata</th>
              <th class="text-center">Meja terisi</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($total_rows as $rk){?>
            <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $namabulan[$rk->MASA_PAJAK]?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?php if ($rk->JENIS_RESTO=='1') {
                  echo "YA";} else { echo "TIDAK";}?></td>
                <td align="right"><?=  $rk->JUMLAH_MEJA;?></td>
                <td align="right"><?=  $rk->TARIF_RATA_RATA;?></td>
                <td align="right"><?=  $rk->MEJA_TERISI;?></td>
                <td align="right"><?=  $rk->DPP;?></td>
                <td align="right"><?=  $rk->TAGIHAN;?></td>
                <td align="right"><?=  $rk->DENDA;?></td>
                <td align="right"><?=  $rk->POTONGAN;?></td>
                <td align="right"><?=  $rk->JUMLAH_KETETAPAN;?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
              </tr>
              
            <?php $no++;}?>
            <tr>
              
            </tr>
          </tbody>
        </table>