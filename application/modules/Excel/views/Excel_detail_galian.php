
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=detailgalian.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<div class="page-title">
 <div class="title_left">
  <h3>Detail Galian</h3>
  '<?= $data1->KODE_BILING?><br>
  '<?= $data1->NPWPD?><br>
  <?= $data1->NAMA_WP?><br>
  <?= $data1->NAMA_USAHA?><br>
  <?= $data1->PEKERJAAN?>
</div>
        <table id="example2" class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">GOLONGAN</th>
              <th class="text-center">Harga Dasar</th>
              <th class="text-center">Persen Pajak</th>
              <th class="text-center">Analis</th>
              <th class="text-center">Vol Pekerjaan</th>
              <th class="text-center">Volume</th>
              <th class="text-center">Index</th>             
              <th class="text-center">Pajak Terutang</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0;if(count($data)>0){  foreach ($data as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $rk_d->DESKRIPSI?></td>
                <td align="right"><?=  $rk_d->HARGA_DASAR?></td>
                 <td align="right"><?=  $rk_d->PAJAK?></td>
                 <td align="right"><?=  number_format($rk_d->ANALIS_DETAIL,2)?></td>
                 <td align="right"><?=  $rk_d->VOL_PEKERJAAN_DETAIL?></td>
                 <td align="right"><?=  $rk_d->VOLUME?></td>
                <td align="right"><?= $rk_d->I?></td>
                <td align="right"><?=  $rk_d->PAJAK_TERUTANG?></td>
              </tr>
              <?php  $no++;$tot+=$rk_d->PAJAK_TERUTANG;}  }else{ ?>
              <tr>
                <th colspan="6"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="6"><b>TOTAL</b></td>
                <td align="right"><?=  $tot?></td>                                
              </tr>
            </tbody>
          </table>

