   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
        <style>
  
  .num {
  mso-number-format:"0"
}
</style>
 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanSPTPDPajakReklameDetail.xls");  ?>
  <h3>Laporan SPTPD Detail Pajak Reklame</h3>
        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Masa</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">KEC</th>
              <th class="text-center">KEL</th>
              <th class="text-center">Lokasi Pasang</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">TEKS</th>
              <th class="text-center">P/L</th>
              <th class="text-center">SISI</th>
              <th class="text-center">JUMLAH</th>
              <th class="text-center">TERBIT</th>
              <th class="text-center">AWAL PAJAK</th>
              <th class="text-center">AKHIR PAJAK</th>
              <th class="text-center">KETETAPAN</th>
              <th class="text-center">STATUS</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($reklame as $rk){?>
              <tr>
                <td align="center"><?php echo $no ?></td>
                <td>'<?= $rk->KODE_BILING?></td>
                <td align="center"><?= $namabulan[$rk->MASA_PAJAK]?></td>
                <td>'<?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->LOKASI_PASANG?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td><?= $rk->TEKS?></td>
                <td align="right"><?=  number_format($rk->P,'0','','.').' X' .number_format($rk->L,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->S,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH,'0','','.')?></td>
                <td><?= $rk->BERLAKU_MULAI?></td>
                <td><?= $rk->BERLAKU_MULAI?></td>
                <td><?= $rk->AKHIR_PAJAK?></td>
                <td align="right"><?=  number_format($rk->PAJAK_TERUTANG,'0','','.')?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else if ($rk->STATUS=='2'){echo "Belum Ditetapkan";}else{ echo "Belum Bayar";}?></td>
              </tr>
              
            <?php $no++;}?>
            <tr>
              <td colspan="9" align="right">Total Tagihan</td>
              <td><?php echo number_format(str_replace(",",".",$total_tagihan->JUMLAH),'0','','.')?></td>
            </tr>
          </tbody>
        </table>