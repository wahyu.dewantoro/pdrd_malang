<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>

 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LaporanRealisasiPPJ".$namabulan[$bulan].".xls");  ?>
  <h3>Laporan Realisasi Pajak PPJ Bulan <?php echo $namabulan[$bulan]?></h3>

        <table class="table table-striped table-bordered" border="1">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Kode Biling</th>            
              <th>Objek Pajak</th>
              <th>Nama</th>
              <th>Desa</th>
              <th>Kecamatan</th>
              <!-- <th>No rek</th> -->
              <th>Pajak Terutang</th>
              <th>Tgl Lunas</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($restoran as $rk){?>
            <tr>
              <td align="center"><?php echo $no?></td>
              <td>'<?= $rk->KODE_BILING;?></td>
              <td><?= $rk->OBJEK_PAJAK;?></td>
              <td><?= $rk->NAMA_WP;?></td>
              <td><?= $rk->NAMAKEC;?></td>
              <td><?= $rk->NAMAKELURAHAN;?></td>
              <td align="center"><?= $rk->TAGIHAN;?></td>
              <td><?php if ($rk->DATE_BAYAR=='') {
                          echo "Belum Bayar";
                      }else {
                          echo date('d/m/Y',strtotime( $rk->DATE_BAYAR ));}?></td>
            </tr>
            <?php $no++;}?>
          </tbody>
        </table>