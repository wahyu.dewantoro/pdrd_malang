<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mexcel extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->nip  =$this->session->userdata('NIP');
        // $this->role =$this->session->userdata('MS_ROLE_ID');
    }    
    function laporan_detail_perforasi($tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
       $this->db->select("*");
       if ($npwpd!='') {
        $this->db->where("(LOWER(NAMA_WP_PERF) LIKE '%$npwpd%' OR LOWER(CATATAN) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
        } 
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->order_by('TGL_PERFORASI', 'DESC');
            return $this->db->get('V_LAP_DET_PERFORASI')->result();
    } 


}