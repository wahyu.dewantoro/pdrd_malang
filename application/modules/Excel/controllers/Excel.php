<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mexcel');
        $this->load->model('Laporan_sptpd/Mlaporan_sptpd');
        $this->load->model('Laporan_piutang/Mlaporan_piutang');
        $this->load->model('Laporan/Mlaporan');
         $this->upt=$this->session->userdata('UNIT_UPT_ID');
 }
  
public function Excel_laporan_detail_perforasi()
 {
       
            $tgl1=$this->session->userdata('tgl1');;
            $tgl2=$this->session->userdata('tgl2');;
            $npwpd=$this->session->userdata('npwpd');;
          
        $data['det_perforasi']                  = $this->Mexcel->laporan_detail_perforasi($tgl1,$tgl2,$npwpd);
        $this->load->view('Excel/Excel_laporan_detail_perforasi',$data);
 }
  public function Excel_laporan_piutang_hotel()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') { 
           $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['hotel']                  = $this->Mlaporan_piutang->get_all_rekap_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_hotel',$data);
 }
   public function Excel_laporan_piutang_restoran()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {  
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));        
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['restoran']                  = $this->Mlaporan_piutang->get_all_rekap_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_restoran',$data);
 }
    public function Excel_laporan_piutang_hiburan()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {    
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));      
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['hiburan']                  = $this->Mlaporan_piutang->get_all_rekap_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_hiburan',$data);
 }
     public function Excel_laporan_piutang_reklame()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {  
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));        
        } else {
            $bulan='';
            $tahun=$tahun;
            $kecamatan='';
            $kelurahan='';
        }     
        $data['reklame']                  = $this->Mlaporan_piutang->get_all_rekap_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_reklame',$data);
 }
      public function Excel_laporan_piutang_ppj()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {  
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));        
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['ppj']                  = $this->Mlaporan_piutang->get_all_rekap_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_ppj',$data);
 }
       public function Excel_laporan_piutang_galian()
 {
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));  
        if ($bulan <> '') {  
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));        
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['galian']                  = $this->Mlaporan_piutang->get_all_rekap_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_galian',$data);
 }
       public function Excel_laporan_piutang_parkir()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {  
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));        
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['parkir']                  = $this->Mlaporan_piutang->get_all_rekap_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_parkir',$data);
 }
        public function Excel_laporan_piutang_at()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {   
            $cek      = urldecode($this->input->get('cek', TRUE));
            $tahun    = urldecode($this->input->get('tahun', TRUE));
            $bulan    = urldecode($this->input->get('bulan', TRUE));
            $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
            $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
            $npwpd    = urldecode($this->input->get('npwpd', TRUE));       
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['at']                  = $this->Mlaporan_piutang->get_all_rekap_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_at',$data);
 }
        public function Excel_laporan_piutang_sb()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['sb']                  = $this->Mlaporan_piutang->get_all_rekap_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_piutang->sum_tagihan_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_piutang_sb',$data);
 }
 public function Excel_laporan_hotel()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['hotel']                  = $this->Mlaporan_sptpd->get_all_rekap_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_hotel',$data);
 }
 public function Excel_laporan_restoran()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['restoran']                  = $this->Mlaporan_sptpd->get_all_rekap_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_restoran',$data);
 }
  public function Excel_laporan_hiburan()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['hiburan']                  = $this->Mlaporan_sptpd->get_all_rekap_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_hiburan',$data);
 }
   public function Excel_laporan_ppj()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['ppj']                  = $this->Mlaporan_sptpd->get_all_rekap_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_ppj',$data);
 }
    public function Excel_laporan_parkir()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['parkir']                  = $this->Mlaporan_sptpd->get_all_rekap_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_parkir',$data);
 }
     public function Excel_laporan_at()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['at']                  = $this->Mlaporan_sptpd->get_all_rekap_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_at',$data);
 }
      public function Excel_laporan_sb()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['sb']                  = $this->Mlaporan_sptpd->get_all_rekap_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_sb',$data);
 }
       public function Excel_laporan_reklame()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['reklame']                  = $this->Mlaporan_sptpd->get_all_rekap_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_reklame',$data);
 }

 public function Excel_laporan_reklame_detail()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $text     = urldecode($this->input->get('text', TRUE));
        $habis_masa=urldecode($this->input->get('habis_masa', TRUE));
        /*if ($tahun <> '') {          
        } else {
            $tahun=date('n');
            $kecamatan='';
            $kelurahan='';
        }*/     
        $data['reklame']=$this->Mlaporan_sptpd->get_all_data_rekap_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$text,$habis_masa);
        $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$text,$habis_masa);
        $this->load->view('Excel/Excel_laporan_reklame_detail',$data);
 }

 public function Excel_laporan_piutang_reklame_detail()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        //$text     = urldecode($this->input->get('text', TRUE));
        $jns=urldecode($this->input->get('jns', TRUE));
        /*if ($tahun <> '') {          
        } else {
            $tahun=date('n');
            $kecamatan='';
            $kelurahan='';
        }*/     
        $data['reklame']=$this->Mlaporan_piutang->get_all_data_rekap_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$jns);
        $data['total_tagihan']=$this->Mlaporan_piutang->sum_tagihan_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$jns);
        $this->load->view('Excel/Excel_laporan_piutang_reklame_detail',$data);
 }
        public function Excel_laporan_galian()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['galian']                  = $this->Mlaporan_sptpd->get_all_rekap_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_galian',$data);
 }
        public function Excel_laporan_galian_detail()
 {
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
        $data['galian']                  = $this->Mlaporan_sptpd->get_all_rekap_galian_detail($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $data['total_tagihan']          = $this->Mlaporan_sptpd->sum_tagihan_galian_detail($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);   
        $this->load->view('Excel/Excel_laporan_galian_detail',$data);
 }
  function Excel_realisasi_hotel(){
    if ($this->session->userdata('real_hotel_upt')!='') {
        $upt=$this->session->userdata('real_hotel_upt');
        $bulan=$this->session->userdata('real_hotel_bulan');
        $tahun=$this->session->userdata('real_hotel_tahun');
    } else {
        $upt='';
        $bulan=date('n');//date('d-m-Y');
        $tahun=date('Y');//date('d-m-Y');
    }
    $data['hotel']=$this->db->query("select ID_INC,kode_biling,NAMA_WP,ALAMAT_WP,objek_pajak,ALAMAT_OP,JENIS_PAJAK,NAMAKEC,NAMAKELURAHAN,tagihan,date_bayar from 
                                    tagihan a
                                    join KELURAHAN B on A.KODEKEL = B.KODEKELURAHAN AND A.KODEKEC=B.KODEKEC
                                    join KECAMATAN D on A.KODEKEC=D.KODEKEC
                                    where jenis_pajak='01' and kode_upt='$upt' AND  MASA_PAJAK='$bulan' AND TAHUN_PAJAK='$tahun'")->result();
    $this->load->view('Excel/excel_realisasi_hotel',$data);
  }

  function Excel_realisasi_restoran(){
        $upt=$this->session->userdata('real_restoran_upt');
        $bulan=$this->session->userdata('real_restoran_bulan');
        $tahun=$this->session->userdata('real_restoran_tahun');
        $skpd=$this->session->userdata('skpd');
   
       if ($this->upt==2) {
            if ($upt!=='') {$wh3=" AND KODE_UPT='$upt'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
        if ($skpd!=='') {$wh2=" AND JENIS_RESTO='$skpd'";} else {$wh2=''; }    
    
            $data['restoran']=$this->db->query("select ID_INC,kode_biling,NAMA_WP,ALAMAT_WP,objek_pajak,ALAMAT_OP,JENIS_PAJAK,NAMAKEC,NAMAKELURAHAN,tagihan,date_bayar,JENIS_RESTO from 
                                    tagihan a
                                    join KELURAHAN B on A.KODEKEL = B.KODEKELURAHAN AND A.KODEKEC=B.KODEKEC
                                    join KECAMATAN D on A.KODEKEC=D.KODEKEC
                                    where jenis_pajak='02' AND  TO_CHAR(DATE_BAYAR,'fmMM')='$bulan' AND TO_CHAR(DATE_BAYAR,'YYYY')='$tahun' AND STATUS='1' $wh3 $wh2 ")->result();
    $this->load->view('Excel/excel_realisasi_restoran',$data);
  }

  function Excel_realisasi_hiburan(){
    if ($this->session->userdata('real_hiburan_upt')=='') {
        $upt=$this->session->userdata('real_hiburan_upt');
        $bulan=sprintf("%02d", $this->session->userdata('real_hiburan_bulan'));
        $tahun=$this->session->userdata('real_hiburan_tahun');
    } else {
        $upt='';
        $bulan=date('n');//date('d-m-Y');
        $tahun=date('Y');//date('d-m-Y');
    }
    //laporan_realisasi_hiburan';
    $data['restoran']=$this->db->query("select c.KODE_BILING,c.ID_INC,c.NAMA_WP,c.ALAMAT_WP,objek_pajak,ALAMAT_OP,c.JENIS_PAJAK,NAMAKEC,NAMAKELURAHAN,c.JUMLAH_bayar tagihan,date_bayar,C.TGL_BAYAR from 
                                    tagihan a
                                    join sptpd_pembayaran c on A.KODE_BILING=C.KODE_BILING
                                    join KELURAHAN B on A.KODEKEL = B.KODEKELURAHAN AND A.KODEKEC=B.KODEKEC
                                    join KECAMATAN D on A.KODEKEC=D.KODEKEC
                                    where c.jenis_pajak='03' AND to_char(to_date(tgl_bayar, 'DD-mm-YYYY'), 'mm')='$bulan' AND to_char(tgl_bayar, 'YYYY')='$tahun' AND STATUS='1'
                                    order by tgl_bayar asc")->result();
    $this->load->view('Excel/excel_realisasi_hiburan',$data);
  }

  function Excel_realisasi_ppj(){
    if ($this->session->userdata('real_ppj_upt')!='') {
        $pt=$this->session->userdata('real_ppj_upt');
        $bulan=$this->session->userdata('real_ppj_bulan');
        $tahun=$this->session->userdata('real_ppj_tahun');
        $upt="and kode_upt='$pt'";
    } else {
        $upt='';
        $bulan=$this->session->userdata('real_ppj_bulan');
        $tahun=$this->session->userdata('real_ppj_tahun');
    }
    $data['bulan']=$bulan;
    $data['restoran']=$this->db->query("select ID_INC,kode_biling,NAMA_WP,ALAMAT_WP,objek_pajak,ALAMAT_OP,JENIS_PAJAK,NAMAKEC,NAMAKELURAHAN,tagihan,date_bayar from 
                                    tagihan a
                                    join KELURAHAN B on A.KODEKEL = B.KODEKELURAHAN AND A.KODEKEC=B.KODEKEC
                                    join KECAMATAN D on A.KODEKEC=D.KODEKEC
                                    where jenis_pajak='05' AND  TO_CHAR(DATE_BAYAR,'fmMM')='$bulan' AND TO_CHAR(DATE_BAYAR,'YYYY')='$tahun' $upt")->result();
    $this->load->view('Excel/excel_realisasi_ppj',$data);
  }

  function Excel_realisasi_mblb(){
    if ($this->session->userdata('real_mblb_upt')!='') {
        $upt=$this->session->userdata('real_mblb_upt');
        $bulan=$this->session->userdata('real_mblb_bulan');
        $tahun=$this->session->userdata('real_mblb_tahun');
    } else {
        $upt='';
        $bulan=date('n');//date('d-m-Y');
        $tahun=date('Y');//date('d-m-Y');
    }
    $data['restoran']=$this->db->query("select ID_INC,kode_biling,NAMA_WP,ALAMAT_WP,objek_pajak,ALAMAT_OP,JENIS_PAJAK,NAMAKEC,NAMAKELURAHAN,tagihan,date_bayar from 
                                    tagihan a
                                    join KELURAHAN B on A.KODEKEL = B.KODEKELURAHAN AND A.KODEKEC=B.KODEKEC
                                    join KECAMATAN D on A.KODEKEC=D.KODEKEC
                                    where jenis_pajak='06' and kode_upt='$upt' AND  MASA_PAJAK='$bulan' AND TAHUN_PAJAK='$tahun'")->result();
    $this->load->view('Excel/Excel_realisasi_mblb',$data);
  }
  

  function Excel_rekap_bulanan(){
        $upt =$this->session->userdata('rekap_bul_upt');
        if ($upt!='') {
          $u="AND KODE_UPT='$upt'";
        } else {
          $u="";
        }

        $t2 =$this->session->userdata('rekap_bul_tahun');
        $jp =$this->session->userdata('rekap_bul_jenis');
        
        if ($jp==2) {
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }elseif ($jp==3) {
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";                
        }elseif ($jp==4) {
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u"; 
        }elseif ($jp==5) {
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";  
        }elseif ($jp==6) {
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u"; 
        }elseif ($jp==7) {
             $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";   
        }elseif ($jp==8) {
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }elseif ($jp==9) {                
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }else if ($jp==1) {
            $wh="WHERE JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        } else {
            $wh="WHERE JENIS_PAJAK='' AND TAHUN_PAJAK=''";
        }
        $data['rekap']=$this->db->query("SELECT NPWPD,NAMA_WP,OBJEK_PAJAK,JAN,FEB,MAR,APR,MEI,JUN,JUL,AGU,SEP,OKT,NOV,DES,JUMLAH
                                          FROM V_REKAP_BULANAN ".$wh." ")->result();
        $data['upt']=$this->db->query("SELECT NAMA_UNIT FROM  UNIT_UPT WHERE ID_INC='$upt'")->row();
        $this->load->view('Excel/excel_rekap_bulanan',$data);
  }
  
  function Excel_dbhd(){
        $tahun =$this->session->userdata('dbhd_tahun');
        $kec   =$this->session->userdata('dbhd_kecamatan');
        if ($kec!='') {
          $u="WHERE KODEKEC='$kec' AND TAHUN_PAJAK='$tahun'";
        } else {
          $u="WHERE TAHUN_PAJAK='$tahun'";
        }
        $data['bhd']=$this->db->query("SELECT TAHUN,KODEKELURAHAN,KODEKEC,NAMAKEC,NAMAKELURAHAN,JAN,FEB,MAR,APR,MEI,JUN,JUL,AGU,SEP,OKT,NOV,DES,JUMLAH,BHD
                                        FROM V_BHD  ORDER BY KODEKELURAHANB,NO_URUT_PERBUB")->result();
        $data['kec']=$this->db->query("SELECT NAMAKEC FROM  KECAMATAN WHERE KODEKEC='$kec'")->row();
        $this->load->view('Excel/excel_rekap_dbhd',$data);
  }
  function Excel_laporan_ralisasi_target_bulanan(){
        $data['bulan']=$this->db->query("select * from v_target_bulanan")->result(); 
        $this->load->view('Excel/excel_laporan_realisasi_target_bulanan',$data);
  }
  function Excel_laporan_ralisasi_target_harian(){
        $data['harian']=$this->db->query("select * from v_target_harian")->result(); 
        $this->load->view('Excel/excel_laporan_realisasi_target_harian',$data);
  }
    function Excel_rekap_kode_biling(){
    $t1 =$this->session->userdata('l_kodebil_bulan');
    $t2 =$this->session->userdata('l_kodebil_tahun');
    $jp =$this->session->userdata('kd_bil_jenis_pajak');
    $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND JENIS_PAJAK='$jp' ";
    $data['bulan']=$t1;
    $data['jp']=$this->db->query(" select NAMA_PAJAK from JENIS_PAJAK where ID_INC='$jp'")->row();$jp;
        $this->db->where($wh);
        $this->db->SELECT("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP,' 'DPP,TAGIHAN PAJAK_TERUTANG,STATUS,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') TANGGAL_PENERIMAAN,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') JATUH_TEMPO,KODE_BILING");
        $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
        $data['data']=$this->db->get('TAGIHAN')->result();
        $this->load->view('Excel/Excel_rekap_kode_biling',$data);

  }
  function Excel_laporan_jatuh_tempo(){
    $q =$this->session->userdata('q');
    $this->db->where('JENIS_PAJAK', $q);
    $data['data']=$this->db->get('V_CEK_JATUH_TEMPO')->result();
    $this->load->view('Excel/Excel_laporan_jatuh_tempo',$data);
  }
  function Excel_laporan_data_ketetapan(){
        $t1 =$this->session->userdata('data_ketetapan_bulan');
        $t2 =$this->session->userdata('data_ketetapan_tahun');
        $jp =$this->session->userdata('ketetapan_jenis_pajak');
           /*if ($jp==8) {*/
                    $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND JENIS_PAJAK='$jp'";        
                    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||OBJEK_PAJAK NAMA_WP,ALAMAT_WP, '' DPP,TAGIHAN PAJAK_TERUTANG,STATUS,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') TGL_KETETAPAN, TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') JATUH_TEMPO");
                    $this->db->where($wh);
                    $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                    $data['data']=$this->db->get('TAGIHAN')->result();
    $this->load->view('Excel/Excel_laporan_data_ketetapan',$data);
  }

  function Excel_realisasi_air(){
        $t1 =$this->session->userdata('real_air_bulan');
        $t2 =$this->session->userdata('real_air_tahun');
        $sts=$this->session->userdata('sts_byr_air');
         if ($this->session->userdata('sts_byr_air')=='') {
            $and="";
        } else {
            $and="AND STATUS='$sts'";
        }
        $kodekec =$this->session->userdata('real_air_upt');
        $wh="MASA_PAJAK='$t1' AND JENIS_PAJAK='08' AND TAHUN_PAJAK='$t2' AND KODE_UPT='$kodekec' $and";
        $this->db->select("ID_INC,KODE_BILING,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,ALAMAT_WP,NAMAKELURAHAN,NAMAKEC,CARA_HITUNG,TGL_PENETAPAN,TAGIHAN,DATE_BAYAR,STATUS,CASE WHEN STATUS = 1 THEN (TAGIHAN+DENDA) ELSE 0 END AS REALISASI,DENDA");
        $this->db->where($wh);
        $data['data']=$this->db->get('V_REALISASI')->result();
        $this->load->view('Excel/Excel_realisasi_air',$data);
  }
  function Excel_laporan_upt_hotel(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                            JUMLAH_KAMAR,TARIF_RATA_RATA,KAMAR_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        if ($upt!=NULL) {$this->db->where("KODE_UPT",$upt);}
        $data['data']=$this->db->get('UPT_REKAP_HOTEL')->result();
        $this->load->view('Excel/Excel_laporan_upt_hotel',$data);
  }  
  function Excel_laporan_upt_restoran(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                            JUMLAH_MEJA,TARIF_RATA_RATA,MEJA_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_RESTO");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        if ($upt!=NULL) {$this->db->where("KODE_UPT",$upt);}
        $data['data']=$this->db->get('UPT_REKAP_RESTORAN')->result();
        $this->load->view('Excel/Excel_laporan_upt_restoran',$data);
  }  
    function Excel_laporan_upt_hiburan(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,DASAR_PENGENAAN,HARGA_TANDA_MASUK,JUMLAH");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        if ($upt!=NULL) {$this->db->where("KODE_UPT",$upt);}
        $data['data']=$this->db->get('UPT_REKAP_HIBURAN')->result();
        $this->load->view('Excel/Excel_laporan_upt_hiburan',$data);
  }   
    function Excel_laporan_upt_ppj(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,KEPERLUAN,TARIF,JUMLAH_PEMAKAIAN,DAYA_TERPASANG,PLN");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        if ($upt!=NULL) {$this->db->where("KODE_UPT",$upt);}
        $data['data']=$this->db->get('UPT_REKAP_PPJ')->result();
        $this->load->view('Excel/Excel_laporan_upt_ppj',$data);
  }       
  function Excel_laporan_upt_at(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $kecamatan=$this->session->userdata('h_kecamatan');
        $kelurahan=$this->session->userdata('h_kelurahan');
        $sts=$this->session->userdata('sts');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,CEIL(DPP)DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,CEIL(TAGIHAN)TAGIHAN,DENDA,(CEIL(TAGIHAN)+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,CARA_PENGAMBILAN,PERUNTUKAN,NAMAKEC,NAMAKELURAHAN,
        PENGGUNAAN_HARI_NON_METER,PENGGUNAAN_BULAN_NON_METER,PENGGUNAAN_HARI_INI_METER,PENGGUNAAN_BULAN_LALU_METER,VOLUME_AIR_METER,JENIS,DEBIT_NON_METER");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        //$this->db->where("STATUS ='0'");
        if ($sts!=NULL) {$this->db->where("STATUS ='$sts'");}
        if ($upt!=NULL) {$this->db->where("KODE_UPT",$upt);}
        if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
        if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
         $this->db->order_by('NAMAKEC', 'ASC');
        $data['data']=$this->db->get('UPT_REKAP_AT')->result();
        $this->load->view('Excel/Excel_laporan_upt_at',$data);
  }    
  function Excel_laporan_upt_sb(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_PENGELOLAAN,JENIS_BURUNG,KEPEMILIKAN_IJIN,LUAS_AREA,JUMLAH_GALUR,DIPANEN_SETIAP,HASIL_PANEN,HASIL_SETIAP_PANEN,HARGA_RATA_RATA");   
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        if ($upt!=NULL) {$this->db->where("KODE_UPT",$upt);}
        $data['data']=$this->db->get('UPT_REKAP_SB')->result();
        $this->load->view('Excel/Excel_laporan_upt_sb',$data);
  }  
  function Excel_laporan_upt_reklame(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,TOTAL");  
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        $data['data']=$this->db->get('UPT_REKAP_REKLAME')->result();
        $this->load->view('Excel/Excel_laporan_upt_reklame',$data);
  }
  function Excel_laporan_upt_galian(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN");   
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        $data['data']=$this->db->get('UPT_REKAP_GALIAN')->result();
        $this->load->view('Excel/Excel_laporan_upt_galian',$data);
  }
  function Excel_laporan_upt_parkir(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $upt=$this->session->userdata('upt');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,SSPD,REKAP_BON,REKAP_BIL,LAINYA,ID_PARKIR");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("STATUS ='0'");
        $data['data']=$this->db->get('UPT_REKAP_PARKIR')->result();
        $this->load->view('Excel/Excel_laporan_upt_parkir',$data);
  }

   public function Excel_skpd_airtanah(){
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $cara_pengambilan    = urldecode($this->input->get('cara_pengambilan', TRUE));
        if ($bulan <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
        }     
         $data['total_rows']   = $this->Mlaporan->get_all_data_rekap_at($tahun,$bulan,$kecamatan,$kelurahan,$cara_pengambilan);
         $data['total_tagihan']=$this->Mlaporan->sum_tagihan_at($tahun,$bulan,$kecamatan,$kelurahan,$cara_pengambilan);
        $this->load->view('Excel/Excel_skpd_air_tanah',$data);
    }
    public function Excel_skpd_reklame(){
        $tahun    = urldecode($this->input->get('th_skpd', TRUE));
        $bulan    = urldecode($this->input->get('bl_skpd', TRUE));
        $sts      = urldecode($this->input->get('sts_skpd', TRUE));
        $param     = urldecode($this->input->get('param', TRUE));
        if ($tahun <> '') {          
        } else {
            $bulan='';
            $tahun=date('Y');
            $sts='';
            $param='';
        }     
         $data['total_rows']   = $this->Mlaporan->get_all_data_rekap_reklame($bulan,$tahun,$sts,$param);
         $data['total_tagihan']=$this->Mlaporan->sum_skpd_reklame($bulan,$tahun,$sts,$param);
        $this->load->view('Excel/Excel_skpd_reklame',$data);
    }
    public function Excel_skpd_reklame_detail(){
        //$cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $jns     = urldecode($this->input->get('jns', TRUE));
        if ($tahun <> '') {          
        } else {
            $tahun=date('Y');
            $bulan='';
            $kecamatan='';
            $kelurahan='';
            $npwpd='';
            $jns='';
        }     
        $data['total_rows']   = $this->Mlaporan->get_all_data_rekap_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$bulan,$jns);
        $data['total_tagihan']=$this->Mlaporan->sum_skpd_reklame($tahun,$kecamatan,$kelurahan,$npwpd,$bulan,$jns);
        $this->load->view('Excel/Excel_skpd_reklame_detail',$data);
    }
    function Excel_skpd_ppj(){
        $t1 =$this->session->userdata('skpd_ppj_bulan');
        $t2 =$this->session->userdata('skpd_ppj_tahun');
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||OBJEK_PAJAK NAMA_WP,ALAMAT_WP,DPP_TAGIHAN DPP,TAGIHAN PAJAK_TERUTANG,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_KETETAPAN,STATUS ,KODE_BILING");    
       
        $this->db->where("MASA_PAJAK='$t1'");
        $this->db->where("TAHUN_PAJAK='$t2'");
        $this->db->where("JENIS_PAJAK='05'");
        $this->db->where("PLN='1'");
            $data['data']=$this->db->get('TAGIHAN')->result();
        $this->load->view('Excel/Excel_skpd_ppj',$data);
  }

  public function Excel_laporan_skpd_mamin(){
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        if ($tahun <> '') {          
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $npwpd='';
        }     
        $data['total_rows']   = $this->Mlaporan_sptpd->get_all_rekap_restoran_mamin($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->view('Excel/Excel_skpd_mamin',$data);
    }
  function Excel_laporan_sptpd_belum_lapor(){

        $t1 =$this->session->userdata('bl_bulan');
        $t2 =$this->session->userdata('bl_tahun');
        $kodekec =$this->session->userdata('bl_kecamatan');
        $jp =$this->session->userdata('bl_jenis_pajak');
        if ($kodekec=='' AND $jp=='') {
            $and =" ";
        } else if ($kodekec=='' AND $jp!='') {
            $and =" AND JENIS_PAJAK ='$jp'";
        } else if ($kodekec!='' AND $jp!='') {
            $and =" AND KECAMATAN='$kodekec' AND JENIS_PAJAK ='$jp'";
        } else {
            $and =" AND KECAMATAN='$kodekec'";
        }                
                $wh1="NPWP NOT IN (SELECT NPWPD 
                      FROM SPTPD_SARANG_BURUNG WHERE MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2') $and";
                $this->db->select("ID_INC,NPWP,NAMA,ALAMAT,NAMA_USAHA,ALAMAT_USAHA,JENIS_PAJAK,KECAMATAN,NAMA_PAJAK,NAMAKELURAHAN,NAMAKEC"); 
                $this->db->where($wh1);        
                $data['data']=$this->db->get('V_BELUM_ENTRY')->result();
        $this->load->view('Excel/Excel_laporan_sptpd_belum_lapor',$data);
  }

  function Excel_laporan_pembayaran(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $jp=$this->session->userdata('jp');
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        //$this->db->where("STATUS ='0'");
        $this->db->where('JENIS_PAJAK',$jp);
        $this->db->order_by('TGL_BAYAR');
        $data['data']=$this->db->get('V_LAPORAN_PEMBAYARAN')->result();
        $this->load->view('Excel/Excel_laporan_pembayaran',$data);
  }
  function Excel_laporan_realisasi_perpajak(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $jp=$this->session->userdata('jp');
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        //$this->db->where("STATUS ='0'");
        $this->db->where('JENIS_PAJAK',$jp);
        $this->db->order_by('TGL_BAYAR');
        $data['data']=$this->db->get('V_REALISASI_PERPAJAK')->result();
        $this->load->view('Excel/Excel_laporan_realisasi_perpajak',$data);
  }
  function Excel_realisasi_bulanan(){
        $bulan=$this->session->userdata('bulan');
        $tahun=$this->session->userdata('tahun');
        $data['bulanan']=$this->db->query("select nama_pajak,jumlah from jenis_pajak a
                left join (
                 SELECT jenis_pajak,SUM(JUMLAH_BULAN_INI)JUMLAH from v_realisasi_bulanan where bulan='$bulan' and tahun='$tahun'
                group by jenis_pajak)b on id_inc=b.jenis_pajak
                order by id_inc")->result();
        $this->load->view('Excel/Excel_realisasi_bulanan',$data);
  }

  function Excel_laporan_realisasi_upt_pajak(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $jp=$this->session->userdata('jp');
        $kec=$this->session->userdata('kec');
        $kel=$this->session->userdata('kel');
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");       
        $this->db->where('JENIS_PAJAK',$jp);
        if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
        $this->db->order_by('TGL_BAYAR');
        $data['data']=$this->db->get('V_REALISASI_PERPAJAK_NON_MAMIN')->result();
        $this->load->view('Excel/Excel_laporan_realisasi_perpajak_upt',$data);
  }
   function Excel_detail_realisasi_perupt($jenis="",$upt=""){
        $bulan=$this->session->userdata('bulan');
        $tahun=$this->session->userdata('tahun');
        $data['pajak']=$this->db->query("SELECT NAMA_PAJAK FROM JENIS_PAJAK where id_inc='$jenis'")->row();
        $data['detail']=$this->db->query("select * from v_det_bayar_perupt 
                                          where jenis_pajak='$jenis' 
                                          and kode_upt='$upt' 
                                          and bulan='$bulan' and tahun='$tahun' 
                                          order by tgl_bayar")->result();
        $data['upt']=$this->db->query("SELECT * FROM UNIT_UPT where id_inc='$upt'")->row();  
        $this->load->view('Excel/Excel_laporan_detail_realisasi_perupt',$data);
  }
  function Excel_detail_realisasi_perupt_reklame($jenis="",$upt=""){
        $bulan=$this->session->userdata('bulan');
        $tahun=$this->session->userdata('tahun');
        $data['pajak']=$this->db->query("SELECT NAMA_PAJAK FROM JENIS_PAJAK where id_inc='$jenis'")->row();
        $data['detail_reklame']=$this->db->query("select tgl_trx,kode_biling,npwpd,nama_wp,objek_pajak,namakec,namakelurahan,deskripsi,teks,ketetapan,norek from v_realisasi_reklame
                                where to_char(to_date(date_bayar, 'DD-mm-YYYY'), 'mm')='$bulan'
                                and   to_char(date_bayar, 'YYYY')='$tahun' 
                                and kode_upt='$upt'")->result();
        $data['upt']=$this->db->query("SELECT * FROM UNIT_UPT where id_inc='$upt'")->row();  
        $this->load->view('Excel/Excel_laporan_detail_realisasi_perupt_reklame',$data);
  }
  function Excel_rekap_pengajuan(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $sts=$this->session->userdata('sts');
        $cek=$this->session->userdata('cek');
        $jwp=$this->session->userdata('jwp');
        $this->db->select("*");
        if ($tgl1!=NULL){
            $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
            $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
        }
        if ($sts!=NULL){
            $this->db->where("STATUS","$sts");
        }
        if ($cek!=NULL){
            $this->db->like("NAMA","%$cek%");
           // $this->db->or_like("NAMA_BADAN","%$cek%");
        }
         if ($this->session->userdata('MS_ROLE_ID')==10) {
            $this->db->where("UPT", $this->session->userdata('UNIT_UPT_ID'));
        }
        if ($jwp!=NULL){
                    $this->db->where("JENISWP","$jwp");
                }
        
        $this->db->order_by('ID_INC_WP_TEMP', 'DESC');
        $data['data']=$this->db->get('V_PENGAJUAN_NPWPD')->result();
        $this->load->view('Excel/Excel_laporan_pengajuan',$data);
  } 

  function Excel_laporan_realisasi_reklame(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $kec=$this->session->userdata('kec');
        $kel=$this->session->userdata('kel');
        $user=$this->session->userdata('user');
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
            if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KECAMATAN",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KECAMATAN",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KELURAHAN",$kel);}
            if ($user!=NULL){
                if ($user==2) {
                    $this->db->where("UPT_INSERT ='2'");
                } else if($user==1){
                    $this->db->where("UPT_INSERT !='2'");
                }
                      
            }
        $this->db->order_by('DATE_BAYAR');
        $data['data']=$this->db->get('V_REALISASI_REKLAME')->result();
       $this->load->view('Excel/Excel_realisasi_reklame',$data);
  }

  function Excel_rekap_tempat_usaha(){
        $jp=$this->session->userdata('jp');
        $kec=$this->session->userdata('kec');
        $kel=$this->session->userdata('kel');
        $npwp=$this->session->userdata('npwp');
        $thn=$this->session->userdata('thn');
        //$this->db->select("*"); 
        $this->db->select("NAMA,NPWPD,NAMA_USAHA,ALAMAT_USAHA,NAMAKELURAHAN,NAMAKEC,NAMA_PAJAK,DESKRIPSI,ID_INC,TOTAL_PELAPORAN_PAJAK(ID_INC,'$thn') REALISASI");   
        $this->db->where('JENIS_PAJAK',$jp);
        if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
             if ($npwp!=NULL) {
                $this->db->where("(LOWER(NAMA) LIKE '%$npwp%' OR LOWER(NAMA_USAHA) LIKE '%$npwp%' OR NPWPD LIKE '%$npwp%')",null,false);
            }
         $this->db->order_by('KODEKEC');
        $this->db->order_by('ID_INC');
        $data['data']=$this->db->get('V_REKAP_TEMPAT_USAHA')->result();
        $this->load->view('Excel/Excel_rekap_tempat_usaha',$data);
  }
  function Excel_rekap_wp_tempat_usaha(){
        $tgl1=$this->session->userdata('tgl1');
        $tgl2=$this->session->userdata('tgl2');
        $sts=$this->session->userdata('sts');
         $jwp=$this->session->userdata('jwp');
         $golongan=$this->session->userdata('golongan');
        $this->db->select("*");
        if ($tgl1!=NULL){
            $this->db->where("TO_DATE(TO_CHAR(S_VALID,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
            $this->db->where("TO_DATE(TO_CHAR(S_VALID,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
        }
        if ($sts!=NULL){
            $this->db->where("JENIS_PAJAK","$sts");
        }
        if ($jwp!=NULL){
            $this->db->where("JENISWP","$jwp");
        }
        if ($golongan!=NULL){
            $this->db->where("ID_OP","$golongan");
        }
        $this->db->order_by('S_VALID', 'DESC');
        $this->db->order_by('ID_INC', 'DESC');
        $data['tu']=$this->db->get('V_REKAP_WP_TEMPAT_USAHA')->result();    
        $this->load->view('Excel/Excel_rekap_wp_tempat_usaha',$data);
 }
 function Excel_detail_galian($id=""){
        $data['data']=$this->db->query("SELECT * 
                    FROM  sptpd_mineral_detail a
                    join objek_pajak b on a.id_op=b.id_op
                    where mineral_id='$id'")->result();
        $data['data1']=$this->db->query("SELECT nama_wp,NAMA_USAHA,pekerjaan,kode_biling,status,npwpd
                    FROM  sptpd_mineral_non_logam
                    where id_inc='$id'")->row();
        $this->load->view('Excel/Excel_detail_galian',$data);
 }
   
}