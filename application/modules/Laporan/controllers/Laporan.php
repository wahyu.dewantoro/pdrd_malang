<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mlaporan');
        $this->load->model('Master/Mpokok');
        $this->load->model('Sptpd_ppj/Msptpd_ppj');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->load->model('Laporan_sptpd/Mlaporan_sptpd');
        $this->nip=$this->session->userdata('NIP');
        $this->id_pengguna=$this->session->userdata('INC_USER');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
    private function cekAkses($var=null){
        return cek($this->id_pengguna,$var);
    }
    public function index(){
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $jenis_pajak=$_POST['JENIS_PAJAK'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
        }
        $sess=array(
                'l_kodebil_bulan'=>$bulan,
                'l_kodebil_tahun'=>$tahun,
                'kd_bil_jenis_pajak'=>$jenis_pajak
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_kode_biling',$data);
  }
  function json_lap_kodebiling(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_lap_kodebiling();     
 }
 public function data_ketetapan()
    {
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $jenis_pajak=$_POST['JENIS_PAJAK'];
        } else {
            $bulan=date('m');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
        }
        $sess=array(
                'data_ketetapan_bulan'=>$bulan,
                'data_ketetapan_tahun'=>$tahun,
                'ketetapan_jenis_pajak'=>$jenis_pajak
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_ketetapan',$data);
  }
    function json_lap_ketetapan(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_lap_ketetapan();     
 }
 public function skpd_airtanah(){
  $akses =$this->cekAkses(uri_string());  
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $cara_pengambilan    = urldecode($this->input->get('cara_pengambilan', TRUE));
        $npwpd= urldecode($this->input->get('npwpd', TRUE));

        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'laporan/skpd_airtanah?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&cara_pengambilan='.urldecode($cara_pengambilan).'&npwpd='.urldecode($npwpd);
            $config['first_url'] = base_url() . 'laporan/skpd_airtanah?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&cara_pengambilan='.urldecode($cara_pengambilan).'&npwpd='.urldecode($npwpd);
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'laporan/skpd_airtanah';
            $config['first_url'] = base_url() . 'laporan/skpd_airtanah';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_at($tahun,$bulan,$kecamatan,$kelurahan,$cara_pengambilan,$npwpd);
        $at                 = $this->Mlaporan->get_limit_data_rekap_at($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$cara_pengambilan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'cara_pengambilan'        => $cara_pengambilan,
            'npwpd'        => $npwpd,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SKPD Pajak Air Tanah',
            'at'            => $at,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       $data['total_tagihan']=$this->Mlaporan->sum_tagihan_at($tahun,$bulan,$kecamatan,$kelurahan,$cara_pengambilan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mpokok->getKec();
       $this->template->load('Welcome/halaman','Laporan/laporan_skpd_air_tanah1',$data);
  }
   

 public function skpd_reklame1()
    {
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
        }
        $sess=array(
                'skpd_rek_bulan'=>$bulan,
                'skpd_rek_tahun'=>$tahun
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','Laporan/laporan_skpd_reklame',$data);
  }
    function json_lap_skpd_reklame(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_lap_skpd_reklame();     
 }


 public function skpd_PPJ()
    {
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK']) OR isset($_POST['GOLONGAN_LISTRIK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
        }
        $sess=array(
                'skpd_ppj_bulan'=>$bulan,
                'skpd_ppj_tahun'=>$tahun,
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['GOLONGAN']= $this->Msptpd_ppj->getGol();
      $this->template->load('Welcome/halaman','Laporan/laporan_skpd_ppj',$data);
  }
    function json_lap_skpd_ppj(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_lap_skpd_ppj();     
 }
 public function sptpd_harian()
    {
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['TGL1']) && isset($_POST['TGL2'])){
            $tanggal1   =$_POST['TGL1'];
            $tanggal2   =$_POST['TGL2'];
            $jenis_pajak=$_POST['JENIS_PAJAK'];
        } else {
            $tanggal1   =date('d/m/Y');
            $tanggal2   =date('d/m/Y');
            $jenis_pajak='';
        }
        $sess=array(
                'uptharian_tgl1'    =>$tanggal1,
                'uptharian_tgl2'    =>$tanggal2,
                'utp_harian_jp'     =>$jenis_pajak
                
        );
      $this->session->set_userdata($sess);
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_sptpd_harian',$data);
  }
  function json_lap_sptpd_harian(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_lap_sptpd_harian();     
 }

   public function sptpd_belumbayar()
    {
      $akses =$this->cekAkses(uri_string());  
      if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $jenis_pajak=$_POST['JENIS_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'l_b_bulan'=>$bulan,
                'l_b_tahun'=>$tahun,
                'l_b_jenis_pajak'=>$jenis_pajak,
                'l_b_kecamatan'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
      $data['keca']=$this->db->query("SELECT * FROM kecamatan  order by namakec asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_sptpd_belumbayar',$data);
  }

  function json_lap_sptpd_belumbayar(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_lap_sptpd_belumbayar();     
 }
 function sptpd_belum_lapor(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $jenis_pajak=$_POST['JENIS_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'bl_bulan'=>$bulan,
                'bl_tahun'=>$tahun,
                'bl_jenis_pajak'=>$jenis_pajak,
                'bl_kecamatan'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
      $data['keca']=$this->db->query("SELECT * FROM KECAMATAN WHERE KODEKEC !='01' AND kodekec !='99' order by namakec asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_sptpd_belumlapor',$data);
 }
 function json_sptpd_belum_lapor(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_sptpd_belum_lapor();     
 }

 function laporan_realisasi_hotel(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $kecamatan='';
        }
        $sess=array(
                'real_hotel_bulan'=>$bulan,
                'real_hotel_tahun'=>$tahun,
                'real_hotel_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_hotel',$data);
 }
 function json_laporan_realisasi_hotel(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_hotel();     
 }

 function laporan_realisasi_restoran(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
            $skpd=$_POST['SKPD'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
            $skpd='';
        }
        $sess=array(
                'real_restoran_bulan'=>$bulan,
                'real_restoran_tahun'=>$tahun,
                'real_restoran_upt'=>$kecamatan,
                'skpd'=>$skpd
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_restoran',$data);
 }
 function json_laporan_realisasi_restoran(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_restoran();     
 }

 function laporan_realisasi_hiburan(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'real_hiburan_bulan'=>$bulan,
                'real_hiburan_tahun'=>$tahun,
                'real_hiburan_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_hiburan',$data);
 }
 function json_laporan_realisasi_hiburan(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_hiburan();     
 }

 function laporan_realisasi_ppj(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'real_ppj_bulan'=>$bulan,
                'real_ppj_tahun'=>$tahun,
                'real_ppj_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_ppj',$data);
 }
 function json_laporan_realisasi_ppj(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_ppj();     
 }

 /*function laporan_realisasi_mblb(){
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'real_mblb_bulan'=>$bulan,
                'real_mblb_tahun'=>$tahun,
                'real_mblb_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_mblb',$data);
 }
 function json_laporan_realisasi_mblb(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_mblb();     
 }*/

 function laporan_realisasi_mblb(){   
        $akses =$this->cekAkses(uri_string());   
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_realisasi_mblb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_realisasi_mblb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_galian_detail?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_realisasi_mblb';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_realisasi_mblb';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_galian_detail?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_galian_detail($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $galian                 = $this->Mlaporan->get_limit_data_rekap_galian_detail($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Detail Realisasi Pajak Galian C',
            'galian'            => $galian,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak,
            'option'           => $wh
        );
       $data['total_tagihan']=$this->Mlaporan->sum_tagihan_galian_detail($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->db->query("SELECT * FROM KECAMATAN WHERE KODE_UPT='$kecamatan'")->result();
       $data['upt']=$this->Mlaporan->get_upt();
       $this->template->load('Welcome/halaman','laporan_realisasi_mblb',$data);
    }

 function laporan_realisasi_parkir(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'real_parkir_bulan'=>$bulan,
                'real_parkir_tahun'=>$tahun,
                'real_parkir_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_parkir',$data);
 }
 function json_laporan_realisasi_parkir(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_parkir();     
 }
 function laporan_realisasi_sb(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $kecamatan='';
        }
        $sess=array(
                'real_sb_bulan'=>$bulan,
                'real_sb_tahun'=>$tahun,
                'real_sb_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_sb',$data);
 }
 function json_laporan_realisasi_sb(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_sb();     
 }

 function laporan_realisasi_air(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan=$_POST['MASA_PAJAK'];
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
            /*if ($_POST['STS']!='') {
              $sts=$_POST['STS'];
            } else {
              $sts='';
            }
            $sts1=$sts;*/
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $kecamatan='';
           // $sts1='';
        }
        $sess=array(
                'real_air_bulan'=>$bulan,
                'real_air_tahun'=>$tahun,
                'real_air_upt'=>$kecamatan,
                //'sts_byr_air'=>$sts1
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_air',$data);
 }
 function json_laporan_realisasi_air(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_realisasi_air();     
 }

 function laporan_realisasi_reklame(){
  $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        //$jp     = urldecode($this->input->get('jp', TRUE));
        $kec   = urldecode($this->input->get('kec', TRUE));
        $kel   = urldecode($this->input->get('kel', TRUE));
        $user   = urldecode($this->input->get('user', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            //'jp'       => $jp,
            'kec'       => $kec,
            'kel'       => $kel,
            'user'      => $user
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_reklame?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kec='.urlencode($kec).'&kel='.urlencode($kel).'&user='.urlencode($user);
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_reklame?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kec='.urlencode($kec).'&kel='.urlencode($kel).'&user='.urlencode($user);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_reklame';
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_reklame';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_realisasi_reklame($tgl1,$tgl2,$kec,$kel,$user);
        $realisasi                 = $this->Mlaporan->get_limit_data_rekap_realisasi_reklame($config['per_page'], $start, $tgl1,$tgl2,$kec,$kel,$user);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Laporan Realisasi reklame berdasarkan tempat pasang ',
            'realisasi'            => $realisasi,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            //'jp'               => $jp,
            'kec'               => $kec,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['total_tagihan']=$this->Mlaporan->sum_realisasi_reklame($tgl1,$tgl2,$kec,$kel,$user);
        $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kec'")->result();
        $data['kec']=$this->Mlaporan_sptpd->get_kec();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_reklame',$data);
 }

public function laporan_rekap()
    {
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['TAHUN_PAJAK'])&& isset($_POST['JENIS_PAJAK'])){           
            $tahun=$_POST['TAHUN_PAJAK'];
            $jenis_pajak=$_POST['JENIS_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'rekap_bul_tahun'=>$tahun,
                'rekap_bul_jenis'=>$jenis_pajak,
                'rekap_bul_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc !='04' and id_inc !='06' order by ID_INC asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_sptpd_rekap',$data);
  }
  /*public function skpdkb()
    {
       if(isset($_POST['TAHUN_PAJAK'])&& isset($_POST['JENIS_PAJAK'])){           
            $tahun=$_POST['TAHUN_PAJAK'];
            $jenis_pajak=$_POST['JENIS_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $bulan=date('n');//date('d-m-Y');
            $tahun=date('Y');//date('d-m-Y');
            $jenis_pajak='';
            $kecamatan='';
        }
        $sess=array(
                'rekap_bul_tahun'=>$tahun,
                'rekap_bul_jenis'=>$jenis_pajak,
                'rekap_bul_upt'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua UPT</option>";
        } else {
            $wh="";
        }
      $data['option']=$wh;
      $data['upt']=$this->Mlaporan->get_upt();
      $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc !='04' and id_inc !='06' order by ID_INC asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_skpdkb',$data);
  }*/
  function json_laporan_rekap(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_rekap();     
 }
 function json_laporan_skpdkb(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_rekap();     
 }

 public function laporan_dbhd()
    {
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['TAHUN_PAJAK'])&& isset($_POST['KECAMATAN'])){           
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $tahun=date('Y');//date('d-m-Y');
            $kecamatan='';
        }
        $sess=array(
                'dbhd_tahun'=>$tahun,
                'dbhd_kecamatan'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);     
      $data['kec']=$this->db->query("SELECT * FROM KECAMATAN  order by NAMAKEC asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_dbhd',$data);
  }
  function json_laporan_dbhd(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_dbhd();     
 }
 public function laporan_dbhd_reklame()
    {
      $akses =$this->cekAkses(uri_string());  
       if(isset($_POST['TAHUN_PAJAK'])&& isset($_POST['KECAMATAN'])){           
            $tahun=$_POST['TAHUN_PAJAK'];
            $kecamatan=$_POST['KECAMATAN'];
        } else {
            $tahun=date('Y');//date('d-m-Y');
            $kecamatan='';
        }
        $sess=array(
                'dbhd_tahun'=>$tahun,
                'dbhd_kecamatan'=>$kecamatan
                
        );
      $this->session->set_userdata($sess);     
      $data['kec']=$this->db->query("SELECT * FROM KECAMATAN  order by NAMAKEC asc")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_dbhd_reklame',$data);
  }
  function json_laporan_dbhd_reklame(){
    header('Content-Type: application/json');
    echo $this->Mlaporan->json_laporan_dbhd_reklame();     
 }

 public function target_realisasi_bulanan(){
  $akses =$this->cekAkses(uri_string());  
    if(isset($_GET['bulan'])){
            $bulan=sprintf('%02d', $_GET['bulan']);
            $tahun=$_GET['tahun'];
        } else {
            $bulan=date('m');
            $tahun=date('Y');
        }
      $data['bulan']   =$bulan;
      $data['tahun']   =$tahun;
      $data['bulanan']=$this->db->query("select jenis_pajak,nama_pajak,jumlah from jenis_pajak a
                left join (
                 SELECT jenis_pajak,SUM(JUMLAH_BULAN_INI)JUMLAH from v_realisasi_bulanan where bulan='$bulan' and tahun='$tahun'
                group by jenis_pajak)b on id_inc=b.jenis_pajak
                where a.id_inc!='09'
                union
                select jenis_pajak,nama_pajak,sum(jumlah_hari_ini)jml  from mv_realisasi_harian_pbb@Tolayanan
                WHERE TO_CHAR(tgl_pembayaran_sppt,'yyyy')='$tahun' and TO_CHAR(tgl_pembayaran_sppt,'mm')='$bulan'
                group by jenis_pajak,nama_pajak
                union
                select '10' jenis_pajak,'BPHTB' nama_pajak,sum(total) jml  from PEMBAYARAN_SSPD@tobphtb
                WHERE TO_CHAR(tanggal_pembayaran,'yyyy')='$tahun' and TO_CHAR(tanggal_pembayaran,'mm')='$bulan'")->result();
      $sess=array(
                'bulan'=>$bulan,
                'tahun'=>$tahun
                
        );
      $this->session->set_userdata($sess); 
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','Laporan/laporan_target_realisasi_bulanan',$data);
  }
  public function target_realisasi()
    {
      $akses =$this->cekAkses(uri_string());  
      $data['bulan']=$this->db->query("select * from v_target_bulanan
        union
        select * FROM mv_target_realisasi_thn_pbb@to17 where thn_pajak=to_char(sysdate, 'YYYY')
        UNION
        SELECT * FROM v_target_REALISASI_BPHTB")->result(); 
      $this->template->load('Welcome/halaman','Laporan/laporan_target_realisasi',$data);
  }
  public function laporan_per_rekening()
    {
      $akses =$this->cekAkses(uri_string());  
      $data['lp_realisasi_per_rekening']=$this->db->query("SELECT * FROM V_REALISASI_PER_REKENING 
                                                           UNION
                                                           SELECT * FROM V_REALISASI_PER_REK_REKLAME
                                                           UNION
                                                           SELECT * FROM V_REALISASI_PER_REK_GALIAN")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_per_rekening',$data);
  }
  public function laporan_jatuh_tempo(){
    $akses =$this->cekAkses(uri_string());  
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $this->session->set_userdata('q',$q);
        if ($q <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_jatuh_tempo?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'Laporan/laporan_jatuh_tempo?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_jatuh_tempo';
            $config['first_url'] = base_url() . 'Laporan/laporan_jatuh_tempo';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_j_tempo($q);
        $jatuh_tempo                 = $this->Mlaporan->get_limit_data_j_tempo($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'jatuh_tempo'         => $jatuh_tempo,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start
        );
        if ($this->role=='161') {
          $wh="where id_inc='04'";
        } else {
          $wh="";
        }
        
        $data['total_tagihan']=$this->Mlaporan->sum_j_tempo($q);
        $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK ".$wh." order by ID_INC asc")->result();
        $this->template->load('Welcome/halaman','Laporan/laporan_jatuh_tempo',$data);
  }
  public function laporan_realisasi_bapenda(){
    $akses =$this->cekAkses(uri_string());  
      if(isset($_GET['bulan'])){
            $bulan=$_GET['bulan'];
            $tahun=$_GET['tahun'];  
            //$data['lp_realisasi_per_upt']=$this->db->query("SELECT * FROM V_REALISASI_PER_UPT where PLN='0' $where")->result();
            $data['lp_realisasi_per_upt']=$this->db->query("select TAHUN_PAJAK,JENIS_PAJAK,NAMA_PAJAK,TARGET,JUMLAH_BULAN_INI,SD_BULAN_INI,sd_BULAN_LALU,CASE WHEN ab.target <> 0 THEN (sd_bulan_ini / ab.target) * 100 END persen
                from (
                select  a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,sum(target)target,nvl(sum(jumlah_bulan_ini),0) jumlah_bulan_ini,nvl(sum(sd_bulan_ini),0) sd_bulan_ini,nvl(sum(sd_bulan_lalu),0) sd_bulan_lalu
                from v_sum_target a
                left join ( 
                              select jenis_pajak,sum(jumlah_bayar)jumlah_bulan_ini
                                from sptpd_pembayaran where to_char(tgl_bayar,'mm')='$bulan' and to_char(tgl_bayar,'YYYY') ='$tahun' 
                                group by jenis_pajak) d on a.jenis_pajak=d.jenis_pajak 
                left join   (
                               select jenis_pajak,sum(jumlah_bayar)sd_bulan_ini
                                from sptpd_pembayaran where to_char(tgl_bayar,'YYYY') ='$tahun' 
                                group by jenis_pajak) e on a.jenis_pajak=e.jenis_pajak 
                left join (
                               select jenis_pajak,sum(jumlah_bayar)sd_bulan_lalu
                                from sptpd_pembayaran where to_char(tgl_bayar,'YYYY') ='$tahun'  and to_char(tgl_bayar,'mm')='$bulan'
                                group by jenis_pajak) f on a.jenis_pajak=f.jenis_pajak  
                                where tahun_pajak='$tahun' and a.jenis_pajak!='09'
                                group by a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan    
                ) ab order by jenis_pajak")->result();
        }else{
            $bulan=date('m');
            $tahun=date('Y');
            $data['lp_realisasi_per_upt']=$this->db->query("select TAHUN_PAJAK,JENIS_PAJAK,NAMA_PAJAK,TARGET,JUMLAH_BULAN_INI,SD_BULAN_INI,sd_BULAN_LALU,CASE WHEN ab.target <> 0 THEN (sd_bulan_ini / ab.target) * 100 END persen
                from (
                select  a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,sum(target)target,nvl(sum(jumlah_bulan_ini),0) jumlah_bulan_ini,nvl(sum(sd_bulan_ini),0) sd_bulan_ini,nvl(sum(sd_bulan_lalu),0) sd_bulan_lalu
                from v_sum_target a
                left join ( 
                              select jenis_pajak,sum(jumlah_bayar)jumlah_bulan_ini
                                from sptpd_pembayaran where to_char(tgl_bayar,'mm')='$bulan' and to_char(tgl_bayar,'YYYY') ='$tahun' 
                                group by jenis_pajak) d on a.jenis_pajak=d.jenis_pajak 
                left join   (
                               select jenis_pajak,sum(jumlah_bayar)sd_bulan_ini
                                from sptpd_pembayaran where to_char(tgl_bayar,'YYYY') ='$tahun' 
                                group by jenis_pajak) e on a.jenis_pajak=e.jenis_pajak 
                left join (
                               select jenis_pajak,sum(jumlah_bayar)sd_bulan_lalu
                                from sptpd_pembayaran where to_char(tgl_bayar,'YYYY') ='$tahun'  and to_char(tgl_bayar,'mm')='$bulan'
                                group by jenis_pajak) f on a.jenis_pajak=f.jenis_pajak  
                                where tahun_pajak='$tahun' and a.jenis_pajak!='09'
                                group by a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan    
                ) ab order by jenis_pajak")->result();
        }
        $sess=array(
                'tahun'=>$tahun,
                'bulan'=>$bulan
                
        );
      $this->session->set_userdata($sess);
        $data['option']=$wh;
        $data['mp']=$this->Mpokok->listMasapajak();
        $data['upt']=$this->Mlaporan->get_upt();
        
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_bapenda',$data);
  }
  public function laporan_realisasi_perupt(){
    $akses =$this->cekAkses(uri_string());  
      $upt_id=$this->session->userdata('UNIT_UPT_ID');
      if(isset($_GET['bulan'])){
            $upt=$_GET['upt'];
            $bulan=$_GET['bulan'];
            $tahun=$_GET['tahun'];  
            //$data['lp_realisasi_per_upt']=$this->db->query("SELECT * FROM V_REALISASI_PER_UPT where PLN='0' $where")->result();
            $data['lp_realisasi_per_upt']=$this->db->query("select upt,PLN,TAHUN_PAJAK,JENIS_PAJAK,NAMA_PAJAK,TARGET,JUMLAH_BULAN_INI,SD_BULAN_INI,sd_BULAN_LALU,CASE WHEN ab.target <> 0 THEN (sd_bulan_ini / ab.target) * 100 END persen
                from (
                select  upt,a.pln,a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,sum(target)target,nvl(sum(jumlah_bulan_ini),0) jumlah_bulan_ini,nvl(sum(sd_bulan_ini),0) sd_bulan_ini,nvl(sum(sd_bulan_lalu),0) sd_bulan_lalu
                from v_sum_target_nonpln a
                left join ( 
                              select jenis_pajak,pln,kode_upt,sum(jumlah_bulan_ini)jumlah_bulan_ini
                                from a_tes_realisasi_upt where bulan='$bulan' and tahun ='$tahun' 
                                group by jenis_pajak,pln,kode_upt) d on a.jenis_pajak=d.jenis_pajak and a.pln=d.pln and a.upt=d.KODE_UPT 
                left join   (
                                select jenis_pajak,pln,kode_upt,sum(jumlah_bulan_ini)sd_bulan_ini
                                from a_tes_realisasi_upt where tahun ='$tahun' 
                                group by jenis_pajak,pln,kode_upt) e on a.jenis_pajak=e.jenis_pajak and a.pln=e.pln and a.upt=e.KODE_UPT 
                left join (
                               select jenis_pajak,pln,kode_upt,sum(jumlah_bulan_ini)sd_bulan_lalu
                                from a_tes_realisasi_upt where tahun ='$tahun' and bulan < '$bulan'
                                group by jenis_pajak,pln,kode_upt) f on a.jenis_pajak=f.jenis_pajak and a.pln=f.pln and a.upt=f.KODE_UPT 
                                where upt='$upt' and tahun_pajak='$tahun' and a.jenis_pajak!='04' and a.jenis_pajak!='09'
                                group by a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,a.pln,upt                
                ) ab 
                union 
                              select upt,'0' pln,TAHUN_PAJAK,JENIS_PAJAK,NAMA_PAJAK,TARGET,JUMLAH_BULAN_INI,SD_BULAN_INI,sd_BULAN_LALU,CASE WHEN ab.target <> 0 THEN (sd_bulan_ini / ab.target) * 100 END persen
                from (
                select  a.upt,a.pln,a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,sum(target)target,nvl(sum(jumlah_bulan_ini),0) jumlah_bulan_ini,nvl(sum(sd_bulan_ini),0) sd_bulan_ini,nvl(sum(sd_bulan_lalu),0) sd_bulan_lalu
                from v_sum_target_nonpln a
              left  join (  select kode_upt,sum(ketetapan) jumlah_bulan_ini from V_REALISASI_REKLAME
                               where to_char(to_date(date_bayar, 'DD-mm-YYYY'), 'mm')='$bulan'
                               and to_char(date_bayar, 'YYYY')='$tahun'
                                group by kode_upt) d on a.upt=d.KODE_UPT 
              left  join (  select kode_upt,sum(ketetapan) sd_bulan_ini from V_REALISASI_REKLAME
                               where to_char(date_bayar, 'YYYY')='$tahun'
                                group by kode_upt) da on a.upt=da.KODE_UPT
              left    join (  select kode_upt,sum(ketetapan) sd_bulan_lalu from V_REALISASI_REKLAME
                               where to_char(to_date(date_bayar, 'DD-mm-YYYY'), 'mm')<'$bulan' and  to_char(date_bayar, 'YYYY')='$tahun'
                                group by kode_upt) de on a.upt=de.KODE_UPT
                 where a.upt='$upt' and tahun_pajak='$tahun' and a.jenis_pajak='04' 
               group by a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,a.pln,a.upt
                    ) ab order by jenis_pajak")->result();
        }else{
            $upt=$upt_id;
            $bulan=date('m');
            $tahun=date('Y');
            $data['lp_realisasi_per_upt']=$this->db->query("select upt,PLN,TAHUN_PAJAK,JENIS_PAJAK,NAMA_PAJAK,TARGET,JUMLAH_BULAN_INI,SD_BULAN_INI,sd_BULAN_LALU,CASE WHEN ab.target <> 0 THEN (sd_bulan_ini / ab.target) * 100 END persen
                from (
                select  upt,a.pln,a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,sum(target)target,nvl(sum(jumlah_bulan_ini),0) jumlah_bulan_ini,nvl(sum(sd_bulan_ini),0) sd_bulan_ini,nvl(sum(sd_bulan_lalu),0) sd_bulan_lalu
                from v_sum_target_nonpln a
                left join ( 
                              select jenis_pajak,pln,kode_upt,sum(jumlah_bulan_ini)jumlah_bulan_ini
                                from a_tes_realisasi_upt where bulan='$bulan' and tahun ='$tahun' 
                                group by jenis_pajak,pln,kode_upt) d on a.jenis_pajak=d.jenis_pajak and a.pln=d.pln and a.upt=d.KODE_UPT 
                left join   (
                                select jenis_pajak,pln,kode_upt,sum(jumlah_bulan_ini)sd_bulan_ini
                                from a_tes_realisasi_upt where tahun ='$tahun' 
                                group by jenis_pajak,pln,kode_upt) e on a.jenis_pajak=e.jenis_pajak and a.pln=e.pln and a.upt=e.KODE_UPT 
                left join (
                               select jenis_pajak,pln,kode_upt,sum(jumlah_bulan_ini)sd_bulan_lalu
                                from a_tes_realisasi_upt where tahun ='$tahun' and bulan < '$bulan'
                                group by jenis_pajak,pln,kode_upt) f on a.jenis_pajak=f.jenis_pajak and a.pln=f.pln and a.upt=f.KODE_UPT 
                                where upt='$upt' and tahun_pajak='$tahun' and a.jenis_pajak!='04' and a.jenis_pajak!='09'
                                group by a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,a.pln,upt                
                ) ab 
                union 
                              select upt,'0' pln,TAHUN_PAJAK,JENIS_PAJAK,NAMA_PAJAK,TARGET,JUMLAH_BULAN_INI,SD_BULAN_INI,sd_BULAN_LALU,CASE WHEN ab.target <> 0 THEN (sd_bulan_ini / ab.target) * 100 END persen
                from (
                select  a.upt,a.pln,a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,sum(target)target,nvl(sum(jumlah_bulan_ini),0) jumlah_bulan_ini,nvl(sum(sd_bulan_ini),0) sd_bulan_ini,nvl(sum(sd_bulan_lalu),0) sd_bulan_lalu
                from v_sum_target_nonpln a
              left  join (  select kode_upt,sum(ketetapan) jumlah_bulan_ini from V_REALISASI_REKLAME
                               where to_char(to_date(date_bayar, 'DD-mm-YYYY'), 'mm')='$bulan'
                               and to_char(date_bayar, 'YYYY')='$tahun'
                                group by kode_upt) d on a.upt=d.KODE_UPT 
              left  join (  select kode_upt,sum(ketetapan) sd_bulan_ini from V_REALISASI_REKLAME
                               where to_char(date_bayar, 'YYYY')='$tahun'
                                group by kode_upt) da on a.upt=da.KODE_UPT
              left    join (  select kode_upt,sum(ketetapan) sd_bulan_lalu from V_REALISASI_REKLAME
                               where to_char(to_date(date_bayar, 'DD-mm-YYYY'), 'mm')<'$bulan' and  to_char(date_bayar, 'YYYY')='$tahun'
                                group by kode_upt) de on a.upt=de.KODE_UPT
                 where a.upt='$upt' and tahun_pajak='$tahun' and a.jenis_pajak='04' 
               group by a.tahun_pajak,a.jenis_pajak,a.nama_pajak,rek_depan,a.pln,a.upt
                    ) ab order by jenis_pajak")->result();
        }
        if ($upt_id=='2') {
            $wh="<option value=''>Pilih UPT</option>";
        } else {
            $wh="";
        }
        $sess=array(
                'tahun'=>$tahun,
                'bulan'=>$bulan,
                'upt'=>$upt
                
        );
      $this->session->set_userdata($sess);
        $data['option']=$wh;
        $data['mp']=$this->Mpokok->listMasapajak();
        $data['upt']=$this->Mlaporan->get_upt();
        
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_perupt',$data);
  }
  public function detail_realisasi_perupt($jenis="",$upt="",$bulan="",$tahun="")
    {
      
        $data['pajak']=$this->db->query("SELECT NAMA_PAJAK FROM JENIS_PAJAK where id_inc='$jenis'")->row();
        $data['detail']=$this->db->query("select * from v_det_bayar_perupt 
                                          where jenis_pajak='$jenis' 
                                          and kode_upt='$upt' 
                                          and bulan='$bulan' and tahun='$tahun' 
                                          order by tgl_bayar")->result();
        $data['detail_reklame']=$this->db->query("select tgl_trx,kode_biling,npwpd,nama_wp,objek_pajak,namakec,namakelurahan,deskripsi,teks,ketetapan,norek from v_realisasi_reklame
                                where to_char(to_date(date_bayar, 'DD-mm-YYYY'), 'mm')='$bulan'
                                and   to_char(date_bayar, 'YYYY')='$tahun' 
                                and kode_upt='$upt'")->result();
        $data['upt']=$this->db->query("SELECT * FROM UNIT_UPT where id_inc='$upt'")->row(); 
        $data['jenis']=$jenis;
        $data['upt']=$upt;       
      $this->template->load('Welcome/halaman','Laporan/laporan_detail_realisasi_perupt',$data);
  }

  public function laporan_realisasi_harian_rekening()
    {
      $akses =$this->cekAkses(uri_string());  
      $data['lp_realisasi_per_rekening']=$this->db->query("SELECT * FROM V_REALISASI_PER_REKENINGHARIAN where jenis_pajak !='04' and jenis_pajak !='06' 
                                                           UNION 
                                                           SELECT * FROM V_REALISASI_PER_REK_HARI_REKL
                                                           UNION 
                                                           SELECT * FROM V_REALISASI_PER_REK_HARI_GALI")->result();
                                                                  
      $this->template->load('Welcome/halaman','Laporan/laporan_realisasi_per_rekening_harian',$data);
  }
  function tracking(){
    $akses =$this->cekAkses(uri_string());  
        if(isset($_POST['kode_biling'])){
            $data['kode_biling']=$_POST['kode_biling'];
            $data['rk']=$this->Mlaporan->getKodebiling($_POST['kode_biling']);
        }else{
            $data['kode_biling']=null;
        }

    // getKodebiling
    $this->template->load('Welcome/halaman','Laporan/laporan_tracking',$data);
  }

  public function skpd_reklame(){        
        /*$th_skpd    = urldecode($this->input->get('th_skpd', TRUE));
        $bl_skpd    = urldecode($this->input->get('bl_skpd', TRUE));
        $this->session->set_userdata('th_skpd',$th_skpd);
        $this->session->set_userdata('bl_skpd',$bl_skpd);
        $start = intval($this->input->get('start'));
        
        if ($th_skpd <> '') {
            $th_skpd1=$th_skpd;
            $bl_skpd1=$bl_skpd;
            $config['base_url']  = base_url() . 'Laporan/skpd_reklame?th_skpd=' . urlencode($th_skpd).'&bl_skpd='.urlencode($bl_skpd);
            $config['first_url'] = base_url() . 'Laporan/skpd_reklame?th_skpd=' . urlencode($th_skpd).'&bl_skpd='.urlencode($bl_skpd);
        } else {
            $th_skpd1=date('Y');
            $bl_skpd1=date('n');
            $config['base_url']  = base_url() . 'Laporan/skpd_reklame';
            $config['first_url'] = base_url() . 'Laporan/skpd_reklame';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_skpd_reklame($th_skpd1,$bl_skpd1);
        $reklame                 = $this->Mlaporan->get_limit_data_skpd_reklame($config['per_page'], $start, $th_skpd1,$bl_skpd1);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
            'reklame'         => $reklame,
            'th_skpd'             => $th_skpd1,
            'bl_skpd'             => $bl_skpd1,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start
        );

        $data['mp']=$this->Mpokok->listMasapajak();*/
        $akses =$this->cekAkses(uri_string());  
        $th_skpd    = urldecode($this->input->get('th_skpd', TRUE));
        $bl_skpd    = urldecode($this->input->get('bl_skpd', TRUE));
        $sts_skpd    = urldecode($this->input->get('sts_skpd', TRUE));
        $param     = urldecode($this->input->get('param', TRUE));
        $start   = intval($this->input->get('start'));
        $this->session->set_userdata('th_skpd',$th_skpd);
        $this->session->set_userdata('bl_skpd',$bl_skpd);
        $this->session->set_userdata('param',$param);
       
        if ($th_skpd <> '') {
            $cek='1';
            $config['base_url']  = base_url() . 'Laporan/skpd_reklame?bl_skpd=' . urlencode($bl_skpd).'&th_skpd='.urlencode($th_skpd).'&sts_skpd='.urlencode($sts_skpd).'&param='.urlencode($param);
            $config['first_url'] = base_url() . 'Laporan/skpd_reklame?bl_skpd=' . urlencode($bl_skpd).'&th_skpd='.urlencode($th_skpd).'&sts_skpd='.urlencode($sts_skpd).'&param='.urlencode($param);
            $cetak= base_url() . 'Excel/Excel/Excel_skpd_reklame?bl_skpd=' . urlencode($bl_skpd).'&th_skpd='.urlencode($th_skpd).'&sts_skpd='.urlencode($sts_skpd).'&param='.urlencode($param);
        } else {
            $cek='';
            $th_skpd=date('Y');
            $bl_skpd=date('n');
            $param='';
            $sts_skpd='';
            $config['base_url']  = base_url() . 'Laporan/skpd_reklame';
            $config['first_url'] = base_url() . 'Laporan/skpd_reklame';
            $cetak= base_url() . 'Excel/Excel/Excel_skpd_reklame?bl_skpd='. urlencode($bl_skpd).'&th_skpd='.urlencode($th_skpd).'&sts_skpd='.urlencode($sts_skpd).'&param='.urlencode($param);
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_reklame($bl_skpd,$th_skpd,$sts_skpd,$param);
        $reklame                 = $this->Mlaporan->get_limit_data_rekap_reklame($config['per_page'], $start, $bl_skpd,$th_skpd,$sts_skpd,$param);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        
        $data = array(
          'title'        => 'Laporan Rekap Pajak Reklame',
            'reklame'          => $reklame,
            'thn_skpd'          => $th_skpd,
            'bl_skpd'          => $bl_skpd,
            'param'           => $param,
            'sts_skpd'           => $sts_skpd,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cek'              => $cek,
             'cetak'              => $cetak
        );
        $data['total_tagihan']=$this->Mlaporan->sum_skpd_reklame($bl_skpd,$th_skpd,$sts_skpd,$param);
        $data['mp']=$this->Mpokok->listMasapajak();
        $data['list_upt']=$this->Mlaporan->get_upt();
        $this->template->load('Welcome/halaman','Laporan/laporan_skpd_reklame',$data);

  }
  function laporan_potongan(){
    $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $jenis_pajak     = urldecode($this->input->get('jenis_pajak', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'jenis_pajak'       => $jenis_pajak,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_potongan?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jenis_pajak='.urlencode($jenis_pajak);
            $config['first_url'] = base_url() . 'Laporan/laporan_potongan?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jenis_pajak='.urlencode($jenis_pajak);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_potongan';
            $config['first_url'] = base_url() . 'Laporan/laporan_potongan';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_potongan($tgl1,$tgl2,$jenis_pajak);
        $potongan                 = $this->Mlaporan->get_limit_data_potongan($config['per_page'], $start, $tgl1,$tgl2,$jenis_pajak);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Laporan Potongan Pajak',
            'potongan'         => $potongan,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'jenis_pajak'      => $jenis_pajak,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','laporan_potongan_denda',$data);
    }
    public function penerimaan_harian(){
     // $akses =$this->cekAkses(uri_starget_realisasi_bulanantring());  
      if(isset($_GET['tgl'])){
            $tanggal=$_GET['tgl'];
        } else {
            $tanggal=date('d/m/Y');
        }
      $data['tgl']   =$tanggal;
      $data['harian']=$this->db->query("select a.jenis_pajak,a.nama_pajak,nvl(jumlah_hari_ini,0) jumlah_hari_ini from v_sum_target_bapenda a
                                    left join ( 
                                            select jenis_pajak,sum(jumlah_bayar) jumlah_hari_ini 
                                            from sptpd_pembayaran 
                                            where TO_CHAR(tgl_bayar, 'dd/mm/yyyy')='$tanggal'
                                            group by jenis_pajak) b on a.jenis_pajak=b.jenis_pajak
                                     join jenis_pajak c on a.jenis_pajak=c.id_inc 
                                     where c.id_inc!='09'
                                     union  
                                   select jenis_pajak,nama_pajak,jumlah_hari_ini from mv_realisasi_harian_pbb@Tolayanan                                      
                                     WHERE TO_CHAR(tgl_pembayaran_sppt,'dd/mm/yyyy')='$tanggal'
                                     union
                                     select '10' jenis_pajak,'BPHTB' nama_pajak,sum(total) jumlah_hari_ini from PEMBAYARAN_SSPD@tobphtb  WHERE TO_CHAR(tanggal_pembayaran,'dd/mm/yyyy')='$tanggal'")->result();
      $this->template->load('Welcome/halaman','Laporan/laporan_penerimaan_harian',$data);
  }
  function laporan_perforasi(){
    $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $npwpd     = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'npwpd'       => $npwpd,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_perforasi?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&npwpd='.urlencode($npwpd);
            $config['first_url'] = base_url() . 'Laporan/laporan_perforasi?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&npwpd='.urlencode($npwpd);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_perforasi';
            $config['first_url'] = base_url() . 'Laporan/laporan_perforasi';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_perforasi($tgl1,$tgl2,$npwpd);
        $perforasi                 = $this->Mlaporan->get_limit_data_perforasi($config['per_page'], $start, $tgl1,$tgl2,$npwpd);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Laporan Perforasi',
            'perforasi'         => $perforasi,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'npwpd'      => $npwpd,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       $this->template->load('Welcome/halaman','laporan_perforasi',$data);
    }
   function laporan_detail_perforasi(){
    $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $npwpd   = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'npwpd'       => $npwpd,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_detail_perforasi?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&npwpd='.urlencode($npwpd);
            $config['first_url'] = base_url() . 'Laporan/laporan_detail_perforasi?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&npwpd='.urlencode($npwpd);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_detail_perforasi';
            $config['first_url'] = base_url() . 'Laporan/laporan_detail_perforasi';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_detail_perforasi($tgl1,$tgl2,$npwpd);
        $perforasi                 = $this->Mlaporan->get_limit_detail_data_perforasi($config['per_page'], $start, $tgl1,$tgl2,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Laporan Detail Perforasi',
            'perforasi'         => $perforasi,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'npwpd'      => $npwpd,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
        $data['total_tagihan']=$this->Mlaporan->get_sum_data_perforasi($tgl1,$tgl2,$npwpd);
       $this->template->load('Welcome/halaman','laporan_detail_perforasi',$data);
    }
    
    function management_kodebiling(){
      $akses =$this->cekAkses(uri_string());  
        // $tahun    = urldecode($this->input->get('tahun', TRUE));
        // $bulan    = urldecode($this->input->get('bulan', TRUE));
        $param    = urldecode($this->input->get('param', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            /*'tahun'       => $tahun,
            'bulan'       => $bulan,*/
            'param'       => $param,
        );
        $this->session->set_userdata($sess);
        if ($param <> '') {
            $config['base_url']  = base_url() . 'Laporan/management_kodebiling?param=' .urlencode($param);
            $config['first_url'] = base_url() . 'Laporan/management_kodebiling?param=' .urlencode($param);
        } else {
            $config['base_url']  = base_url() . 'Laporan/management_kodebiling';
            $config['first_url'] = base_url() . 'Laporan/management_kodebiling';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_manage_kobil(/*$tahun,$bulan,*/$param);
        $manage_kobil                = $this->Mlaporan->get_limit_data_manage_kobil($config['per_page'], $start /*$tahun,$bulan*/,$param);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Management Kode Biling',
            'manage_kobil'         => $manage_kobil,
         /*   'tahun'             => $tahun,
            'bulan'             => $bulan,*/
            'param'             => $param,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       //echo $this->db->last_query();
       $this->template->load('Welcome/halaman','laporan_management_kodebiling',$data);
    }
    public function hapus_kobil($kode_biling="",$jp="") {      
            if ($jp=='01') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_hotel where kode_biling='$kode_biling'");
            } else if ($jp=='02') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_restoran where kode_biling='$kode_biling'");
            } else if ($jp=='03') {
                $cek=$this->db->query("select id_inc from sptpd_hiburan where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_hiburan_detail where id_sptpd_hiburan='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_hiburan where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='04') {
              $cek=$this->db->query("select id_inc from sptpd_reklame where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_reklame_detail where sptpd_reklame_id='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_reklame where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='05') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_ppj where kode_biling='$kode_biling'");
            } else if ($jp=='06') {
              $cek=$this->db->query("select id_inc from sptpd_mineral_non_logam where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_mineral_detail where mineral_id='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_mineral_non_logam where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='07') {
              $cek=$this->db->query("select id_inc from sptpd_parkir where kode_biling='$kode_biling'")->row();
                      $this->db->query("delete from sptpd_parkir_karcis where sptpd_parkir_id='$cek->ID_INC'");
                      $this->db->query("delete from sptpd_parkir where kode_biling='$kode_biling'");
                      $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
            } else if ($jp=='08') {
                $this->db->query("delete from tagihan where kode_biling='$kode_biling'");
                $this->db->query("delete from sptpd_air_tanah where kode_biling='$kode_biling'");
            } else {
              # code...
            }
            redirect(site_url('Laporan/management_kodebiling'));
 
    }

    function laporan_pembayaran()
 {      
  $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $jp     = urldecode($this->input->get('jp', TRUE));
        $mtd     = urldecode($this->input->get('mtd', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'jp'       => $jp,
            'mtd'       => $mtd
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_pembayaran?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp).'&mtd='.urlencode($mtd);
            $config['first_url'] = base_url() . 'Laporan/laporan_pembayaran?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp).'&mtd='.urlencode($mtd);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_pembayaran';
            $config['first_url'] = base_url() . 'Laporan/laporan_pembayaran';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_pembayaran($tgl1,$tgl2,$jp,$mtd);
        $hotel                 = $this->Mlaporan->get_limit_data_rekap_pembayaran($config['per_page'], $start, $tgl1,$tgl2,$jp,$mtd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Laporan Pembayaran BANK JATIM',
            'hotel'            => $hotel,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'jp'               => $jp,
            'mtd'               => $mtd,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['total_tagihan']=$this->Mlaporan->sum_pembayaran($tgl1,$tgl2,$jp,$mtd);
        $data['total_pembayaran']=$this->Mlaporan->sum_pembayaran($tgl1,$tgl2,$jp,$mtd);
        $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','laporan_pembayaran',$data);
    }

    function laporan_realisasi_perpajak()
 {      
        $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $jp     = urldecode($this->input->get('jp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'jp'       => $jp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_perpajak?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp);
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_perpajak?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_perpajak';
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_perpajak';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_realisasi_perpajak($tgl1,$tgl2,$jp);
        $realisasi                 = $this->Mlaporan->get_limit_data_rekap_realisasi_perpajak($config['per_page'], $start, $tgl1,$tgl2,$jp);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Laporan Realisasi Perpajak',
            'realisasi'            => $realisasi,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'jp'               => $jp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['total_tagihan']=$this->Mlaporan->sum_realisasi_perpajak($tgl1,$tgl2,$jp);
        $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc !='09' order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','laporan_realisasi_perpajak',$data);
    }
    function laporan_realisasi_upt_pajak()
 {      

      $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $jp     = urldecode($this->input->get('jp', TRUE));
        $kec   = urldecode($this->input->get('kec', TRUE));
        $kel   = urldecode($this->input->get('kel', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'jp'       => $jp,
            'kec'       => $kec,
            'kel'       => $kel
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_upt_pajak?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp).'&kec='.urlencode($kec).'&kel='.urlencode($kel);
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_upt_pajak?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp).'&kec='.urlencode($kec).'&kel='.urlencode($kel);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_upt_pajak';
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_upt_pajak';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_realisasi_perpajak_upt($tgl1,$tgl2,$jp,$kec,$kel);
        $realisasi                 = $this->Mlaporan->get_limit_data_rekap_realisasi_perpajak_upt($config['per_page'], $start, $tgl1,$tgl2,$jp,$kec,$kel);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Laporan Realisasi UPT Perpajak ',
            'realisasi'            => $realisasi,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'jp'               => $jp,
            'kec'               => $kec,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['total_tagihan']=$this->Mlaporan->sum_realisasi_perpajak_upt($tgl1,$tgl2,$jp,$kec,$kel);
        $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc !='04' and id_inc!='09' order by ID_INC asc")->result();
        $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kec'")->result();
        $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_realisasi_upt_pajak',$data);
    }

    public function edit_tagihan_bayar($kode_biling="") {   
            //$akses =$this->cekAkses(uri_string());  
            $data['data_detail']=$this->db->query("select * from tagihan where kode_biling='$kode_biling'")->row();
            $cek=$this->db->query("select NPWPD,JENIS_PAJAK from tagihan where kode_biling='$kode_biling'")->row();
            $data['tempat_usaha']=$this->db->query("SELECT * FROM tempat_usaha where npwpd='$cek->NPWPD' and jenis_pajak='$cek->JENIS_PAJAK'")->result();
            $data['kel']=$this->db->query("SELECT * FROM KELURAHAN ")->result();
            $data['kec']=$this->Mlaporan_sptpd->get_kec();
            $data['list_masa_pajak']     = $this->Mpokok->listMasapajak();  
            $this->template->load('Welcome/halaman','edit_tagihan_bayar',$data);
    }
    public function aksi_edit_tagihan_bayar() { 
            $jp=$_POST['JENIS_PAJAK'];
            $kode_biling=$_POST['KODE_BILING'];
            $kecamatan=$_POST['kecamatan'];
            $kelurahan=$_POST['kelurahan'];
            $masa_pajak=$_POST['MASA_PAJAK'];
            $tahun_pajak=$_POST['TAHUN_PAJAK'];
            $user=$this->session->userdata('NIP');
            $this->db->query("CALL P_INSERT_TAGIHAN_HISTORI('$kode_biling','$user')");
            $this->db->query("update tagihan set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            $this->db->query("update sptpd_pembayaran set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak' where kode_biling='$kode_biling'");
            if ($jp=='01') {
               $this->db->query("update sptpd_hotel set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else if ($jp=='02') {
              $this->db->query("update sptpd_restoran set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else if ($jp=='03') {
               $this->db->query("update sptpd_hiburan set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else if ($jp=='04') {
               $this->db->query("update sptpd_reklame set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else if ($jp=='05') {
                $this->db->query("update sptpd_ppj set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else if ($jp=='06') {
              $this->db->query("update sptpd_mineral_non_logam set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else if ($jp=='07') {
              $this->db->query("update sptpd_parkir set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else if ($jp=='08') {
                $this->db->query("update sptpd_air_tanah set masa_pajak='$masa_pajak',tahun_pajak='$tahun_pajak',kodekec='$kecamatan',kodekel='$kelurahan' where kode_biling='$kode_biling'");
            } else {
              # code...
            }
            redirect(site_url('Laporan/laporan_pembayaran'));
 
    }

     function rekap_tempat_usaha(){     
     $akses =$this->cekAkses(uri_string());   
        $jp     = urldecode($this->input->get('jp', TRUE));
        $kec   = urldecode($this->input->get('kec', TRUE));
        $kel   = urldecode($this->input->get('kel', TRUE));
        $npwp  = urldecode($this->input->get('npwp', TRUE));
        $thn     = $this->input->get('thn', TRUE);
        $start   = intval($this->input->get('start'));
        $sess=array(
            'jp'       => $jp,
            'kec'       => $kec,
            'kel'       => $kel,
            'npwp'       => $npwp,
            'thn'       => $thn
        );
        $this->session->set_userdata($sess);
        if ($jp <> '') {
            $config['base_url']  = base_url() . 'Laporan/rekap_tempat_usaha?&jp='.urlencode($jp).'&kec='.urlencode($kec).'&kel='.urlencode($kel).'&npwp='.urlencode($npwp);
            $config['first_url'] = base_url() . 'Laporan/rekap_tempat_usaha?&jp='.urlencode($jp).'&kec='.urlencode($kec).'&kel='.urlencode($kel).'&npwp='.urlencode($npwp);
        } else {
            $config['base_url']  = base_url() . 'Laporan/rekap_tempat_usaha';
            $config['first_url'] = base_url() . 'Laporan/rekap_tempat_usaha';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_tempat_usaha($jp,$kec,$kel,$npwp);
        $realisasi                 = $this->Mlaporan->get_limit_data_rekap_tempat_usaha($config['per_page'], $start, $jp,$kec,$kel,$npwp,$thn);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Rekap Wajib Pajak dan Tempat Usaha ',
            'realisasi'            => $realisasi,
            'jp'               => $jp,
            'kec'               => $kec,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc!='09' order by ID_INC asc")->result();
        $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kec'")->result();
        $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_rekap_tempat_usaha',$data);
    }

    function cek_laporan_pajak(){  
      $akses =$this->cekAkses(uri_string());  
        $th     = urldecode($this->input->get('th', TRUE));    
        $jp     = urldecode($this->input->get('jp', TRUE));
        $kec   = urldecode($this->input->get('kec', TRUE));
        $kel   = urldecode($this->input->get('kel', TRUE));
        $npwpd   = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'th'       => $th,
            'jp'       => $jp,
            'kec'       => $kec,
            'kel'       => $kel,
            'npwpd'       => $npwpd
        );
        $this->session->set_userdata($sess);
        if ($jp <> '') {
            $config['base_url']  = base_url() . 'Laporan/cek_laporan_pajak?&th='.urlencode($th).'&jp='.urlencode($jp).'&kec='.urlencode($kec).'&kel='.urlencode($kel).'&npwpd='.urlencode($npwpd);
            $config['first_url'] = base_url() . 'Laporan/cek_laporan_pajak?&th='.urlencode($th).'&jp='.urlencode($jp).'&kec='.urlencode($kec).'&kel='.urlencode($kel).'&npwpd='.urlencode($npwpd);
        } else {
            $config['base_url']  = base_url() . 'Laporan/cek_laporan_pajak';
            $config['first_url'] = base_url() . 'Laporan/cek_laporan_pajak';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_cek_laporan_pajak($th,$jp,$kec,$kel,$npwpd);
        $realisasi                 = $this->Mlaporan->get_limit_data_cek_laporan_pajak($config['per_page'], $start,$th, $jp,$kec,$kel,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Cek Laporan Pajak ',
            'realisasi'            => $realisasi,
            'jp'               => $jp,
            'kec'               => $kec,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc!='09' order by ID_INC asc")->result();
        $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kec'")->result();
        $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','cek_laporan_pajak',$data);
    }
        
    function laporan_wp_tempat_usaha(){   
    $akses =$this->cekAkses(uri_string());   
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $sts     = urldecode($this->input->get('sts', TRUE));
        $jwp     = urldecode($this->input->get('jwp', TRUE));
        $golongan     = urldecode($this->input->get('golongan', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_wp_tempat_usaha?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&sts='.urlencode($sts).'&jwp='.urlencode($jwp).'&golongan='.urlencode($golongan);
            $config['first_url'] = base_url() . 'Laporan/laporan_wp_tempat_usaha?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&sts='.urlencode($sts).'&jwp='.urlencode($jwp).'&golongan='.urlencode($golongan);
            //$cetak= base_url() . 'Excel/Excel/Excel_pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kecamatan='.urlencode($kecamatan);
        } else {
            $tgl2='';
            $tgl1='';
            $sts='';
            $config['base_url']  = base_url() . 'Laporan/laporan_wp_tempat_usaha';
            $config['first_url'] = base_url() . 'Laporan/laporan_wp_tempat_usaha';
            //$cetak= base_url() . 'Excel/Excel/Excel_pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_wp_tempat_usaha($tgl1,$tgl2,$sts,$jwp,$golongan);
        $pengajuan                 = $this->Mlaporan->get_limit_data_rekap_wp_tempat_usaha($config['per_page'], $start, $tgl1,$tgl2,$sts,$jwp,$golongan);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'sts'        => $sts,
            'jwp'         => $jwp,
            'golongan'         => $golongan,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Tempat Usaha',
            'pengajuan'        => $pengajuan,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'sts'              => $sts,
            'jwp'               => $jwp,
            'golongan'               => $golongan,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'cetak'            => $cetak
        );
       $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc!='09' order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','Laporan/laporan_wp_tempat_usaha',$data);
    }

    function rekon(){    
      $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));       
        $start   = intval($this->input->get('start'));
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/rekon?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2);
            $config['first_url'] = base_url() . 'Laporan/rekon?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2);
            //$cetak= base_url() . 'Excel/Excel/Excel_pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kecamatan='.urlencode($kecamatan);
        } else {
            $tgl2='';
            $tgl1='';
            $sts='';
            $config['base_url']  = base_url() . 'Laporan/rekon';
            $config['first_url'] = base_url() . 'Laporan/rekon';
            //$cetak= base_url() . 'Excel/Excel/Excel_pengajuan_npwpd?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekon($tgl1,$tgl2);
        $pengajuan                 = $this->Mlaporan->get_limit_data_rekon($config['per_page'], $start, $tgl1,$tgl2);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'sts'        => $sts,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Data Rekon',
            'pengajuan'        => $pengajuan,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'sts'              => $sts,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'cetak'            => $cetak
        );
       $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK where id_inc!='09' order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','Laporan/laporan_rekon',$data);
    }
    public function import(){
        $data = array('action' => site_url('Laporan/importaction'), );
        $this->template->load('Welcome/halaman','Laporan/import_form',$data);
    }
    function importaction(){
        include_once ( APPPATH."/libraries/Excel_reader.php");
        if(!empty($_FILES['fileimport']['name']) && !empty($_FILES['fileimport']['tmp_name'])){ 
             $target = basename($_FILES['fileimport']['name']) ;
             move_uploaded_file($_FILES['fileimport']['tmp_name'], $target);

              chmod($_FILES['fileimport']['name'],0777);
              $data  = new Spreadsheet_Excel_Reader($_FILES['fileimport']['name'],false);
              $baris = $data->rowcount($sheet_index=0);
              $no    =1;
              
                 
              $this->db->query("DELETE FROM rekon_temp WHERE user_import='$this->nip'");
              for($i=2; $i<=$baris; $i++){
                    $this->db->set('CABANG',$data->val($i,1));
                    $this->db->set('KODE_BILING',$data->val($i, 2));
                    $this->db->set('NAMA',$data->val($i, 3));
                    $this->db->set('POKOK',$data->val($i, 4));
                    $this->db->set('DENDA',$data->val($i, 5));
                    $this->db->set('TANGGAL',"to_date('".$data->val($i, 6)."','yyyymmdd')",false );
                    $this->db->set('NO_REF',$data->val($i, 7));
                    $this->db->set('PENGESAHAN',$data->val($i, 8));
                    $this->db->set('USER_BANK',$data->val($i, 9));
                    $this->db->set('JENIS_PAJAK',$data->val($i, 10));
                    $this->db->set('DATE_INSERT',"SYSDATE",false);
                    $this->db->set('USER_IMPORT',$this->nip);
                    $this->db->insert('REKON_TEMP');


            }

            $arr['record']=$baris-1;
            unlink($_FILES['fileimport']['name']);
            //echo $this->db->last_query();
            $this->template->load('Welcome/halaman','Laporan/import_preview',$arr);

        //}
    }
    }
    function prosesinsertimport($ku=""){
        if(!empty($this->session->userdata('NIP'))){
            $berhasil=0;
            $gagal=0;
           
           $temp = $this->db->query("insert into rekon (CABANG, KODE_BILING, NAMA, POKOK, DENDA, TANGGAL, NO_REF, PENGESAHAN, USER_BANK, JENIS_PAJAK, DATE_INSERT, USER_IMPORT) 
select CABANG, KODE_BILING, NAMA, POKOK, DENDA, TANGGAL, NO_REF, PENGESAHAN, USER_BANK, JENIS_PAJAK, sysdate, USER_IMPORT from rekon_temp where user_import='$this->nip'");
    $notifs="<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> ".$berhasil." Data berhasil di import</div>";
    $this->session->set_flashdata('notifs',$notifs);
 
        $sess=array(
                'gagal'=>$gagal,
                'berhasil'=>$berhasil,
                );
        $this->session->set_userdata($sess);      
            redirect('Laporan/rekon');
        }
    }
         

    public function detail_rekon($tgl=""){
      //$akses =$this->cekAkses(uri_string());  
      $data['tgl']=$tgl;
      $data['rekon']=$this->db->query("select tanggal,a.kode_biling,nama,pokok+a.denda jumlah,b.kode_biling kode_biling_panji,nama_wp,jumlah_bayar from rekon a
        left join sptpd_pembayaran b on a.kode_biling=b.kode_biling 
        where tanggal='$tgl'")->result();
      $this->template->load('welcome/halaman','Laporan/laporan_rekon_detail',$data);
    }

    function rekap_harian_resto()
 {      
  $akses =$this->cekAkses(uri_string());  
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $jp     = urldecode($this->input->get('jp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'jp'       => $jp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Laporan/rekap_harian_resto?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp);
            $config['first_url'] = base_url() . 'Laporan/rekap_harian_resto?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&jp='.urlencode($jp);
        } else {
            $config['base_url']  = base_url() . 'Laporan/rekap_harian_resto';
            $config['first_url'] = base_url() . 'Laporan/rekap_harian_resto';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_harian_resto($tgl1,$tgl2,$jp);
        $realisasi                 = $this->Mlaporan->get_limit_data_rekap_harian_resto($config['per_page'], $start, $tgl1,$tgl2,$jp);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Laporan Rekap harian Resto',
            'realisasi'            => $realisasi,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'jp'               => $jp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['total_tagihan']=$this->Mlaporan->sum_realisasi_harian_resto($tgl1,$tgl2,$jp);
        $data['wp']=$this->db->query("select npwpd||' - '||nama_wp||' - '||nm_tempat_usaha nama,npwpd,ID_TEMPAT_USAHA from t_tempat_usaha")->result();
       $this->template->load('Welcome/halaman','laporan_rekap_resto',$data);
    }

    function laporan_realisasi_perwp()
 {      
  $akses =$this->cekAkses(uri_string());  
        $tahun_pajak    = urldecode($this->input->get('tahun_pajak', TRUE));
        $jp     = urldecode($this->input->get('jp', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tahun_pajak'       => $tahun_pajak,
            'npwpd'       => $npwpd,
            'jp'       => $jp,
        );
        $this->session->set_userdata($sess);
        if ($tahun_pajak <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_perwp?tahun_pajak=' . urlencode($tahun_pajak).'&jp='.urlencode($jp).'&npwpd='.urlencode($npwpd);
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_perwp?tahun_pajak=' . urlencode($tahun_pajak).'&jp='.urlencode($jp).'&npwpd='.urlencode($npwpd);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_realisasi_perwp';
            $config['first_url'] = base_url() . 'Laporan/laporan_realisasi_perwp';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_realisasi_perwp($tahun_pajak,$jp,$npwpd);
        $realisasi                 = $this->Mlaporan->get_limit_data_rekap_realisasi_perwp($config['per_page'], $start, $tahun_pajak,$jp,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Laporan Realisasi Setiap Wajib Pajak',
            'realisasi'            => $realisasi,
            'tahun_pajak'             => $tahun_pajak,
            'jp'             => $jp,
            'npwpd'               => $npwpd,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
        $data['total_tagihan']=$this->Mlaporan->sum_realisasi_perwp($tahun_pajak,$jp,$npwpd);
        $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','laporan_realisasi_perwp',$data);
    }


    function skpd_reklame_detail(){  
      $akses =$this->cekAkses(uri_string());  
        $akses =$this->cekAkses(uri_string());  
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $jns     = urldecode($this->input->get('jns', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan/skpd_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&bulan='.urlencode($bulan).'&jns='.urlencode($jns));
            $config['first_url'] = base_url() . 'Laporan/skpd_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&bulan='.urlencode($bulan).'&jns='.urlencode($jns));
            $cetak= base_url() . 'Excel/Excel/Excel_skpd_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&bulan='.urlencode($bulan).'&jns='.urlencode($jns));        } else {
            //$bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan/skpd_reklame_detail';
            $config['first_url'] = base_url() . 'Laporan/skpd_reklame_detail';
            $cetak= base_url() . 'Excel/Excel/Excel_skpd_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&bulan='.urlencode($bulan).'&jns='.urlencode($jns));        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_rekap_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$bulan,$jns);
        $reklame                 = $this->Mlaporan->get_limit_data_rekap_reklame_detail($config['per_page'], $start, $tahun,$kecamatan,$kelurahan,$npwpd,$bulan,$jns);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'npwpd'         => $npwpd,
            'jns_reklame'   => $jns
           // 'text'          => $text,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Detail SKPD Pajak Reklame',
            'reklame'            => $reklame,
            'cek'            => $cek,
            'bulan'    =>$bulan,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan->sum_tagihan_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$bulan,$jns);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->db->query("SELECT * FROM KECAMATAN WHERE KODEKEC!='01'")->result();
       $this->template->load('Welcome/halaman','laporan_skpd_reklame_detail',$data);
    }

    function laporan_wp_non_aktif()
 {      
  $akses =$this->cekAkses(uri_string());  
       /* $tahun_pajak    = urldecode($this->input->get('tahun_pajak', TRUE));
        $jp     = urldecode($this->input->get('jp', TRUE));*/
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'npwpd'       => $npwpd
        );
        $this->session->set_userdata($sess);
        if ($npwpd <> '') {
            $config['base_url']  = base_url() . 'Laporan/laporan_wp_non_aktif?npwpd=' . urlencode($npwpd);
            $config['first_url'] = base_url() . 'Laporan/laporan_wp_non_aktif?npwpd=' . urlencode($npwpd);
        } else {
            $config['base_url']  = base_url() . 'Laporan/laporan_wp_non_aktif';
            $config['first_url'] = base_url() . 'Laporan/laporan_wp_non_aktif';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan->total_rows_wp_non_aktif($npwpd);
        $data_wp                 = $this->Mlaporan->get_limit_data_wp_non_aktif($config['per_page'], $start, $npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Laporan WP Non Aktif',
            'data_wp'            => $data_wp,
            'npwpd'               => $npwpd,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
       // $data['total_tagihan']=$this->Mlaporan->sum_realisasi_perwp($tahun_pajak,$jp,$npwpd);
       // $data['jpjk']=$this->db->query("SELECT * FROM JENIS_PAJAK order by ID_INC asc")->result();
       $this->template->load('Welcome/halaman','laporan_wp_non_aktif',$data);
    }


   
        
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */