<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mlaporan extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
        $this->upt=$this->session->userdata('UNIT_UPT_ID');
    }

  function json_lap_kodebiling(){
        $t1 =$this->session->userdata('l_kodebil_bulan');
        $t2 =$this->session->userdata('l_kodebil_tahun');
        $jp =$this->session->userdata('kd_bil_jenis_pajak');
        $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND JENIS_PAJAK='$jp'";
                $this->datatables->select("OBJEK_PAJAK,ALAMAT_OP,ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP,DPP_TAGIHAN,TAGIHAN PAJAK_TERUTANG,STATUS,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') TANGGAL_PENERIMAAN,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') JATUH_TEMPO,KODE_BILING");
                $this->datatables->from('TAGIHAN');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_restoran/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_restoran/pdf_kode_biling_restoran/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();                                  
    }
    function json_lap_skpd_AirTanah(){
        $t1 =$this->session->userdata('skpd_ar_bulan');
        $t2 =$this->session->userdata('skpd_ar_tahun');
        $cp =$this->session->userdata('skpd_c_pengambilan');
        if ($this->session->userdata('skpd_c_pengambilan')=='') {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND STATUS!='2'";
        } else {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND CARA_PENGAMBILAN='$cp' AND STATUS!='2'";
        }
        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||NAMA_USAHA NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,KODE_BILING,TO_CHAR(TGL_KETETAPAN, 'dd-mm-yyyy') AS TGL_KETETAPAN");
        $this->datatables->from('SPTPD_AIR_TANAH');
        $this->datatables->where($wh);
        $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");


        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        return $this->datatables->generate();
    }
    function json_lap_skpd_reklame(){
        $t1 =$this->session->userdata('skpd_rek_bulan');
        $t2 =$this->session->userdata('skpd_rek_tahun');
        $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' ";
        //$this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||NAMA_USAHA NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,KODE_BILING");
        $this->datatables->from('SPTPD_REKLAME');
        $this->datatables->where($wh);
        $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");

        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||NAMA_USAHA NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,GETTOTALREKLAME(ID_INC) AS TOTAL,STATUS ,KODE_BILING,TO_CHAR(TGL_KETETAPAN, 'dd/mm/yyyy') AS TGL_KETETAPAN");
        //$this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        return $this->datatables->generate();
    }

    function json_lap_skpd_ppj(){
        $t1 =$this->session->userdata('skpd_ppj_bulan');
        $t2 =$this->session->userdata('skpd_ppj_tahun');
        //$cp =$this->session->userdata('skpd_gol_listrik');
        $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND JENIS_PAJAK='05' AND PLN='1'";
        //$this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||NAMA_USAHA NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,KODE_BILING");
        $this->datatables->from('TAGIHAN');
        $this->datatables->where($wh);
        $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC");

        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||OBJEK_PAJAK NAMA_WP,ALAMAT_WP,DPP_TAGIHAN DPP,TAGIHAN PAJAK_TERUTANG,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_KETETAPAN,STATUS ,KODE_BILING");
        //$this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        return $this->datatables->generate();
    }
    function json_lap_sptpd_harian(){
        $t1 =$this->session->userdata('uptharian_tgl1');
        $t2 =$this->session->userdata('uptharian_tgl2');
        $jp =$this->session->userdata('utp_harian_jp');
        $wh =" TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN  TO_DATE('$t1','DD/MM/YYYY')  AND TO_DATE('$t2','DD/MM/YYYY') AND JENIS_PAJAK='$jp'";
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,' 'DPP,TAGIHAN PAJAK_TERUTANG,STATUS,TO_CHAR (TGL_PENETAPAN, 'dd/mm/yyyy') TANGGAL_PENERIMAAN,TO_CHAR (JATUH_TEMPO, 'dd/mm/yyyy') JATUH_TEMPO,KODE_BILING");
                $this->datatables->from('TAGIHAN');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_restoran/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_restoran/pdf_kode_biling_restoran/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();                                  
    }
    function json_lap_sptpd_belumbayar(){
        $t1 =$this->session->userdata('l_b_bulan');
        $t2 =$this->session->userdata('l_b_tahun');
        $jp =$this->session->userdata('l_b_jenis_pajak');
        $kodekec =$this->session->userdata('l_b_kecamatan');
        $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND STATUS !='2' AND KODEKEC='$kodekec'";
        $ks ="STATUS ='2000'";
       if ($jp==1) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_HOTEL');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_hotel/Sptpd_hotel/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_hotel/Sptpd_hotel/pdf_kode_biling_hotel/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();
        }elseif ($jp==2) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_RESTORAN');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_restoran/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_restoran/pdf_kode_biling_restoran/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();
        }elseif ($jp==3) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_HIBURAN');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpdhiburan/hiburan/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpdhiburan/hiburan/pdf_kode_biling_hiburan/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();             
        }elseif ($jp==4) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,GETTOTALREKLAME(ID_INC) PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_REKLAME');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Sptpdreklame/Reklame2/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').'</div>', 'ID_INC');
                return $this->datatables->generate();
        }elseif ($jp==5) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_PPJ');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_ppj/Sptpd_ppj/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_ppj/Sptpd_ppj/pdf_kode_biling_ppj/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();
        }elseif ($jp==6) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_MINERAL_NON_LOGAM');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_mblbm/Sptpd_mblbm/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_mblbm/Sptpd_mblbm/pdf_kode_biling_mblbm/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();
        }elseif ($jp==7) {
                /*$this->datatables->select('ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP,DPP,PAJAK_TERUTANG,""STATUS,""TANGGAL_PENERIMAAN,""KODE_BILING,PENERIMAAN_NON_KARCIS,PENERIMAAN_KARCIS');
                $this->datatables->from("VIEW_SPTPD_PARKIR");
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Sptpd_parkir/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').'</div>', 'ID_INC');
                return $this->datatables->generate();*/
        }elseif ($jp==8) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_AIR_TANAH');
                $this->datatables->where($wh);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
                return $this->datatables->generate();

        }elseif ($jp==9) {
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_SARANG_BURUNG');
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_psb/Sptpd_psb/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_psb/Sptpd_psb/pdf_kode_biling_psb/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();

        }else{
                $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING");
                $this->datatables->from('SPTPD_HOTEL');
                $this->datatables->where($ks);
                $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_hotel/Sptpd_hotel/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_hotel/Sptpd_hotel/pdf_kode_biling_hotel/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                return $this->datatables->generate();

        }
                                  
    }


    function json_sptpd_belum_lapor(){
        $t1 =$this->session->userdata('bl_bulan');
        $t2 =$this->session->userdata('bl_tahun');
        $kodekec =$this->session->userdata('bl_kecamatan');
        $jp =$this->session->userdata('bl_jenis_pajak');
        if ($kodekec=='' AND $jp=='') {
            $and =" ";
        } else if ($kodekec=='' AND $jp!='') {
            $and =" AND JENIS_PAJAK ='$jp'";
        } else if ($kodekec!='' AND $jp!='') {
            $and =" AND KECAMATAN='$kodekec' AND JENIS_PAJAK ='$jp'";
        } else {
            $and =" AND KECAMATAN='$kodekec'";
        }                
                $wh1="NPWP NOT IN (SELECT NPWPD 
                      FROM SPTPD_SARANG_BURUNG WHERE MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2') $and";
                $this->datatables->select("ID_INC,NPWP,NAMA,ALAMAT,NAMA_USAHA,ALAMAT_USAHA,JENIS_PAJAK,KECAMATAN,NAMA_PAJAK,NAMAKELURAHAN,NAMAKEC");
                $this->datatables->from('V_BELUM_ENTRY'); 
               // $this->datatables->where($wh); 
                $this->datatables->where($wh1); 
                //$this->datatables->where("KECAMATAN","30");  
                $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Pdf/Pdf/Pdf_peringatan_belum_lapor/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-success"').'</div>', 'NPWP');
                return $this->datatables->generate();
                                  
    }

    function json_laporan_realisasi_hotel(){
        $t1 =$this->session->userdata('real_hotel_bulan');
        $t2 =$this->session->userdata('real_hotel_tahun');
        $kodekec =$this->session->userdata('real_hotel_upt');
        if ($this->upt==2) {
            if ($kodekec!=='') {$wh2=" AND KODE_UPT='$kodekec'";} else {$wh2=''; } 
        } else {
            $wh2="AND KODE_UPT='$this->upt'"; 
        }
       // if ($kodekec!=='') {$wh2=" AND KODE_UPT='$kodekec'";} else {$wh2=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='01' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' AND STATUS='1' $wh2";
        $this->datatables->select("A.ID_INC,KODE_BILING,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,JENIS_PAJAK,(TAGIHAN+DENDA-POTONGAN) TAGIHAN,NAMAKEC,NAMAKELURAHAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') DATE_BAYAR");
        $this->datatables->from('TAGIHAN A'); 
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        $this->datatables->where($wh);
        return $this->datatables->generate();
                                  
    }
    function json_laporan_realisasi_restoran(){
        $t1 =$this->session->userdata('real_restoran_bulan');
        $t2 =$this->session->userdata('real_restoran_tahun');
        $kodekec =$this->session->userdata('real_restoran_upt');
        $skpd =$this->session->userdata('skpd');
        if ($this->upt==2) {
            if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
        //if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        if ($skpd!=='') {$wh2=" AND JENIS_RESTO='$skpd'";} else {$wh2=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='02' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' AND STATUS='1' $wh2 $wh3";
        $this->datatables->select("A.ID_INC,KODE_BILING,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,JENIS_PAJAK,(TAGIHAN+DENDA-POTONGAN) TAGIHAN,NAMAKEC,NAMAKELURAHAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') DATE_BAYAR,JENIS_RESTO");
        $this->datatables->from('TAGIHAN A'); 
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }

    function json_laporan_realisasi_hiburan(){
        $t1 =$this->session->userdata('real_hiburan_bulan');
        $t2 =$this->session->userdata('real_hiburan_tahun');
        $kodekec =$this->session->userdata('real_hiburan_upt');
        if ($this->upt==2) {
            if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
        //if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='03' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' AND STATUS='1' $wh3";
        $this->datatables->select("A.ID_INC,KODE_BILING,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,JENIS_PAJAK,(TAGIHAN+DENDA-POTONGAN) TAGIHAN,NAMAKEC,NAMAKELURAHAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') DATE_BAYAR");
        $this->datatables->from('TAGIHAN A'); 
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }

    function json_laporan_realisasi_ppj(){
        $t1 =$this->session->userdata('real_ppj_bulan');
        $t2 =$this->session->userdata('real_ppj_tahun');
        $kodekec =$this->session->userdata('real_ppj_upt');
        if ($this->upt==2) {
            if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
        //if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='05' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' $wh3 AND STATUS='1'";
        $this->datatables->select("A.ID_INC,KODE_BILING,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,JENIS_PAJAK,(TAGIHAN+DENDA-POTONGAN) TAGIHAN,NAMAKEC,NAMAKELURAHAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') DATE_BAYAR,PLN");
        $this->datatables->from('TAGIHAN A'); 
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }
    function json_laporan_realisasi_mblb(){
        $t1 =$this->session->userdata('real_mblb_bulan');
        $t2 =$this->session->userdata('real_mblb_tahun');
        $kodekec =$this->session->userdata('real_mblb_upt');
         if ($this->upt==2) {
            if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
         //if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='06' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' $wh3 AND STATUS='1'";
        $this->datatables->select("A.ID_INC,KODE_BILING,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,JENIS_PAJAK,(TAGIHAN+DENDA-POTONGAN) TAGIHAN,NAMAKEC,NAMAKELURAHAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') DATE_BAYAR");
        $this->datatables->from('TAGIHAN A'); 
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }

    function json_laporan_realisasi_parkir(){
        $t1 =$this->session->userdata('real_parkir_bulan');
        $t2 =$this->session->userdata('real_parkir_tahun');
        $kodekec =$this->session->userdata('real_parkir_upt');
        if ($this->upt==2) {
            if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
        //if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='07' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' AND STATUS='1' $wh3";
        $this->datatables->select("A.ID_INC,KODE_BILING,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,JENIS_PAJAK,(TAGIHAN+DENDA-POTONGAN) TAGIHAN,NAMAKEC,NAMAKELURAHAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') DATE_BAYAR");
        $this->datatables->from('TAGIHAN A'); 
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }
    function json_laporan_realisasi_sb(){
        $t1 =$this->session->userdata('real_parkir_bulan');
        $t2 =$this->session->userdata('real_parkir_tahun');
        $kodekec =$this->session->userdata('real_parkir_upt');
        if ($this->upt==2) {
            if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
        //if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='09' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' AND STATUS='1' $wh3";
        $this->datatables->select("A.ID_INC,KODE_BILING,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,JENIS_PAJAK,(TAGIHAN+DENDA-POTONGAN) TAGIHAN,NAMAKEC,NAMAKELURAHAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') DATE_BAYAR");
        $this->datatables->from('TAGIHAN A'); 
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }
    function json_laporan_realisasi_air(){
        $t1 =$this->session->userdata('real_air_bulan');
        $t2 =$this->session->userdata('real_air_tahun');
        $sts=$this->session->userdata('sts_byr_air');
         /*if ($this->session->userdata('sts_byr_air')=='') {
            $and="";
        } else {
            $and="AND STATUS='$sts'";
        }*/
        $kodekec =$this->session->userdata('real_air_upt');
         if ($this->upt==2) {
            if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        } else {
            $wh3="AND KODE_UPT='$this->upt'"; 
        }
         //if ($kodekec!=='') {$wh3=" AND KODE_UPT='$kodekec'";} else {$wh3=''; } 
        $wh="TO_CHAR(DATE_BAYAR,'fmMM')='$t1' AND JENIS_PAJAK='08' AND TO_CHAR(DATE_BAYAR,'YYYY')='$t2' AND KODE_UPT='$kodekec' $wh3";
        $this->datatables->select("ID_INC,KODE_BILING,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,NAMAKELURAHAN,NAMAKEC,CARA_HITUNG,TGL_PENETAPAN,TAGIHAN,DATE_BAYAR,STATUS,CASE WHEN STATUS = 1 THEN (TAGIHAN+DENDA) ELSE 0 END AS REALISASI,DENDA");
        $this->datatables->from('V_REALISASI'); 
        $this->datatables->where($wh);
        return $this->datatables->generate();
    }
   

    function json_laporan_rekap(){
        $t2 =$this->session->userdata('rekap_bul_tahun');
        $jp =$this->session->userdata('rekap_bul_jenis');
        $upt =$this->session->userdata('rekap_bul_upt');
        if ($upt!='') {
          $u="AND KODE_UPT='$upt'";
        } else {
          $u="";
        }
        if ($jp==2) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }elseif ($jp==3) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";                
        }elseif ($jp==4) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u"; 
        }elseif ($jp==5) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";  
        }elseif ($jp==6) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u"; 
        }elseif ($jp==7) {
             $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";   
        }elseif ($jp==8) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }elseif ($jp==9) {                
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }else {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";    
        }
                $this->datatables->select("NPWPD,NAMA_WP,OBJEK_PAJAK,'4 1 1'||' '||JENIS_PAJAK||' '||NEWKOREK2 AS REK,JAN,FEB,MAR,APR,MEI,JUN,JUL,AGU,SEP,OKT,NOV,DES,JUMLAH");
                $this->datatables->from('V_REKAP_BULANAN');
                $this->datatables->where($wh);
                $this->datatables->add_column('ACTION','');
                return $this->datatables->generate();
                                  
    }
    function json_laporan_skpdkb(){
        $t2 =$this->session->userdata('rekap_bul_tahun');
        $jp =$this->session->userdata('rekap_bul_jenis');
        $upt =$this->session->userdata('rekap_bul_upt');
        if ($upt!='') {
          $u="AND KODE_UPT='$upt'";
        } else {
          $u="";
        }
        if ($jp==2) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }elseif ($jp==3) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";                
        }elseif ($jp==4) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u"; 
        }elseif ($jp==5) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";  
        }elseif ($jp==6) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u"; 
        }elseif ($jp==7) {
             $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";   
        }elseif ($jp==8) {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }elseif ($jp==9) {                
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";
        }else {
            $wh="JENIS_PAJAK='$jp' AND TAHUN_PAJAK='$t2' $u";    
        }
                $this->datatables->select("*");
                $this->datatables->from('V_SKPDKB');
                $this->datatables->where($wh);
                $this->datatables->add_column('ACTION','');
                return $this->datatables->generate();
                                  
    }
    function json_laporan_dbhd(){
        $tahun =$this->session->userdata('dbhd_tahun');
        $kec   =$this->session->userdata('dbhd_kecamatan');
        if ($kec!='') {
          $u="KODEKEC='$kec' AND TAHUN='$tahun'";
          $this->datatables->where($u);
        } else {
          $u="TAHUN='$tahun'";
          $this->datatables->where($u);
        }
                $this->datatables->select("NVL(TAHUN,'$tahun') TAHUN_PAJAK,KODEKELURAHAN KODEKEL,KODEKEC,NAMAKEC,NAMAKELURAHAN,JAN,FEB,MAR,APR,MEI,JUN,JUL,AGU,SEP,OKT,NOV,DES,JUMLAH,BHD");
                $this->datatables->from('V_BHD');
                $this->db->order_by("KODEKELURAHANB,NO_URUT_PERBUB");
                return $this->datatables->generate();
                                  
    }
    function json_laporan_dbhd_reklame(){
        $tahun =$this->session->userdata('dbhd_tahun');
        $kec   =$this->session->userdata('dbhd_kecamatan');
        if ($kec!='') {
          $u="KODEKEC='$kec' AND TAHUN='$tahun'";
          $this->datatables->where($u);
        } else {
          $u="TAHUN='$tahun'";
          $this->datatables->where($u);
        }
                $this->datatables->select("NVL(TAHUN,'$tahun') TAHUN_PAJAK,KODEKELURAHAN KODEKEL,KODEKEC,NAMAKEC,NAMAKELURAHAN,JAN,FEB,MAR,APR,MEI,JUN,JUL,AGU,SEP,OKT,NOV,DES,JUMLAH,BHD");
                $this->datatables->from('V_BHD_REKLAME');
                
                return $this->datatables->generate();
                                  
    }

    function total_rows_j_tempo($q = NULL) {
    $this->db->from('V_CEK_JATUH_TEMPO');
    $this->db->where('JENIS_PAJAK', $q);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_j_tempo($limit, $start = 0, $q = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->where('JENIS_PAJAK', $q);
    $this->db->limit($limit, $start);
        return $this->db->get('V_CEK_JATUH_TEMPO')->result();
    }
    function sum_j_tempo($q = NULL) {
        $this->db->select('SUM(TAGIHAN+DENDA-POTONGAN) JUMLAH');
        $this->db->where('JENIS_PAJAK', $q);
        return $this->db->get('V_CEK_JATUH_TEMPO')->row();
    }

    function json_lap_ketetapan(){
        $t1 =$this->session->userdata('data_ketetapan_bulan');
        $t2 =$this->session->userdata('data_ketetapan_tahun');
        $jp =$this->session->userdata('ketetapan_jenis_pajak');
           /*if ($jp==8) {*/
                    $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND JENIS_PAJAK='$jp'";       
                    $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||OBJEK_PAJAK NAMA_WP,ALAMAT_WP, '' DPP,TAGIHAN PAJAK_TERUTANG,STATUS,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') TGL_KETETAPAN, TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') JATUH_TEMPO,DENDA");
                    $this->datatables->from('TAGIHAN');
                    $this->datatables->where($wh);
                    $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                    $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('Sptpd_restoran/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('Sptpd_restoran/pdf_kode_biling_restoran/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
                    return $this->datatables->generate();
            /*}else {*/
                    /*$wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND STATUS='1'";
                    $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||NAMA_USAHA NAMA_WP,ALAMAT_WP, DPP,GETTOTALREKLAME(ID_INC) PAJAK_TERUTANG,STATUS,TANGGAL_PENERIMAAN,KODE_BILING,TO_CHAR(TGL_KETETAPAN, 'dd-mm-yyyy') TGL_KETETAPAN");
                    $this->datatables->from('SPTPD_REKLAME');
                    $this->datatables->where($wh);
                    $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                    $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Sptpdreklame/Reklame2/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').'</div>', 'ID_INC');
                    return $this->datatables->generate();
            }*/
    }
    function getKodebiling($kode){
        $this->db->where('KODE_BILING',$kode);
        return $this->db->get('V_GET_TAGIHAN');
    }

    function total_rows_skpd_reklame($th_skpd = NULL,$bl_skpd = NULL) {
    $this->db->from('SPTPD_REKLAME');
    $this->db->where('MASA_PAJAK', $bl_skpd);
    $this->db->where('TAHUN_PAJAK', $th_skpd);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_skpd_reklame($limit, $start = 0, $th_skpd = NULL,$bl_skpd = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||NAMA_USAHA NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,GETTOTALREKLAME(ID_INC) AS TOTAL,STATUS ,KODE_BILING,TO_CHAR(TGL_KETETAPAN, 'dd/mm/yyyy') AS TGL_KETETAPAN,JUMLAH_KETETAPAN");    
    $this->db->where('MASA_PAJAK', $bl_skpd);
    $this->db->where('TAHUN_PAJAK', $th_skpd);
    $this->db->order_by('ID_INC DESC');
    $this->db->limit($limit, $start);
        return $this->db->get('SPTPD_REKLAME')->result();
    }

    function total_rows_potongan($tgl1 = NULL,$tgl2 = NULL,$jenis_pajak = NULL) {
    $this->db->from('V_GET_TAGIHAN');
    if ($jenis_pajak!=NULL) {$this->db->where('JENIS_PAJAK', $jenis_pajak);} else { }
    $this->db->where("TO_DATE(TO_CHAR(SYSDATE_POTONGAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(SYSDATE_POTONGAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_potongan($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$jenis_pajak = NULL) {
    $this->db->select("*");
    if ($jenis_pajak!=NULL) {$this->db->where('JENIS_PAJAK', $jenis_pajak);} else { }
    $this->db->where("TO_DATE(TO_CHAR(SYSDATE_POTONGAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(SYSDATE_POTONGAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->order_by('SYSDATE_POTONGAN DESC');
    $this->db->order_by('KODE_BILING DESC');
        return $this->db->get('V_GET_TAGIHAN')->result();
    }
    function total_rows_rekap_at($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$cara_pengambilan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_AT');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!='') {
        $this->db->where('MASA_PAJAK', $bulan);
    }
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(OBJEK_PAJAK) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
            } 
    if ($cara_pengambilan!=NULL) {$this->db->where("CARA_PENGAMBILAN",$cara_pengambilan);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_at($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$cara_pengambilan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(CEIL(TAGIHAN+DENDA-POTONGAN)) AS JUMLAH_KETETAPAN,CARA_PENGAMBILAN,PERUNTUKAN,
        PENGGUNAAN_HARI_NON_METER,PENGGUNAAN_BULAN_NON_METER,PENGGUNAAN_HARI_INI_METER,PENGGUNAAN_BULAN_LALU_METER,VOLUME_AIR_METER,JENIS,DEBIT_NON_METER,CETAK_SKPD");    
        $this->db->where('TAHUN_PAJAK', $tahun);
        if ($bulan!='') {
        $this->db->where('MASA_PAJAK', $bulan);
        }
        if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
        if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
        if ($cara_pengambilan!=NULL) {$this->db->where("CARA_PENGAMBILAN",$cara_pengambilan);}
         if ($npwpd !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(OBJEK_PAJAK) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
            } 
        $this->db->order_by('ID_INC DESC');
        $this->db->limit($limit, $start);
        return $this->db->get('UPT_REKAP_AT')->result();
    }
     function get_all_data_rekap_at($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$cara_pengambilan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(CEIL(TAGIHAN+DENDA-POTONGAN)) AS JUMLAH_KETETAPAN,CARA_PENGAMBILAN,PERUNTUKAN,
        PENGGUNAAN_HARI_NON_METER,PENGGUNAAN_BULAN_NON_METER,PENGGUNAAN_HARI_INI_METER,PENGGUNAAN_BULAN_LALU_METER,VOLUME_AIR_METER,JENIS,DEBIT_NON_METER,NAMAKEC");    
        $this->db->where('TAHUN_PAJAK', $tahun);
        if ($bulan!='') {
        $this->db->where('MASA_PAJAK', $bulan);
        }
        if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
        if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
        if ($npwpd !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(OBJEK_PAJAK) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
            } 
        if ($cara_pengambilan!=NULL) {$this->db->where("CARA_PENGAMBILAN",$cara_pengambilan);}
        $this->db->order_by('NAMAKEC ASC');
        return $this->db->get('UPT_REKAP_AT')->result();
    }
    function sum_tagihan_at($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$cara_pengambilan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!='') {
        $this->db->where('MASA_PAJAK', $bulan);
    }
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(OBJEK_PAJAK) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
            } 
    if ($cara_pengambilan!=NULL) {$this->db->where("CARA_PENGAMBILAN",$cara_pengambilan);}
    return $this->db->get('UPT_REKAP_AT')->row();
    }

    function total_rows_perforasi($tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
        if ($npwpd!='') {
            $this->db->where("(LOWER(NAMA_WP_PERF) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR A.NPWPD LIKE '%$npwpd%')",null,false);
        }
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->from('NOMOR_PERFORASI A');
        $this->db->join('WAJIB_PAJAK B','A.NPWPD=B.NPWP');
        return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_perforasi($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
        if ($npwpd!='') {
            $this->db->where("(LOWER(NAMA_WP_PERF) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR A.NPWPD LIKE '%$npwpd%')",null,false);
        }
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->join('WAJIB_PAJAK B','A.NPWPD=B.NPWP');
        $this->db->order_by('A.TGL_PERFORASI', 'DESC');
        $this->db->order_by('A.ID_INC DESC');
        $this->db->limit($limit, $start);
        //$this->db->select("NAMA_USAHA,A.ID_INC,JENIS_KARCIS,NOMOR_SERI,NILAI_LEMBAR,JML_BLOK,ISI_LEMBAR,PERSEN,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,A.NPWPD,CATATAN,B.NAMA,NO_PERMOHONAN");
        $this->db->select("NAMA_USAHA,A.ID_INC,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,A.NPWPD,NAMA_WP_PERF NAMA,NOMOR_INC_BLN||' / '||LPAD(NOMOR_INC_THN, 3, '0')||' / '||NOMOR_OBJEK||' / '||NOMOR_BLN_THN NO_PERMOHONAN");
            return $this->db->get('NOMOR_PERFORASI A')->result();
    }
    function total_rows_detail_perforasi($tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
        if ($npwpd!='') {
             $this->db->where("(LOWER(NAMA_WP_PERF) LIKE '%$npwpd%' OR LOWER(CATATAN) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
            // $this->db->like('NPWPD', $npwpd);
            // $this->db->or_like('NAMA_USAHA', $npwpd);
            // $this->db->or_like('NAMA_WP_PERF', $npwpd);
        } 
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->from('V_LAP_DET_PERFORASI');
        return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_detail_data_perforasi($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
       $this->db->select("*");
       if ($npwpd!='') {
        $this->db->where("(LOWER(NAMA_WP_PERF) LIKE '%$npwpd%' OR LOWER(CATATAN) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
            /*$this->db->like('NPWPD', $npwpd);
            $this->db->or_like('NAMA_USAHA', $npwpd);
            $this->db->or_like('NAMA_WP_PERF', $npwpd);*/
        } 
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->order_by('TGL_PERFORASI', 'DESC');
        $this->db->limit($limit, $start);
        //$this->db->select("NAMA_USAHA,A.ID_INC,JENIS_KARCIS,NOMOR_SERI,NILAI_LEMBAR,JML_BLOK,ISI_LEMBAR,PERSEN,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,A.NPWPD,CATATAN,B.NAMA,NO_PERMOHONAN");
        /*$this->db->select("NAMA_USAHA,A.ID_INC,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,A.NPWPD,B.NAMA,NOMOR_INC_BLN||' / '||LPAD(NOMOR_INC_THN, 3, '0')||' / '||NOMOR_OBJEK||' / '||NOMOR_BLN_THN NO_PERMOHONAN");*/
            return $this->db->get('V_LAP_DET_PERFORASI')->result();
    }
    function get_sum_data_perforasi($tgl1 = NULL,$tgl2 = NULL,$npwpd = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
       $this->db->select("SUM(PAJAK)AS JUMLAH");
       if ($npwpd!='') {
        $this->db->where("(LOWER(NAMA_WP_PERF) LIKE '%$npwpd%' OR LOWER(CATATAN) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
            
        } 
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->limit($limit, $start);
        //$this->db->select("NAMA_USAHA,A.ID_INC,JENIS_KARCIS,NOMOR_SERI,NILAI_LEMBAR,JML_BLOK,ISI_LEMBAR,PERSEN,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,A.NPWPD,CATATAN,B.NAMA,NO_PERMOHONAN");
        /*$this->db->select("NAMA_USAHA,A.ID_INC,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,A.NPWPD,B.NAMA,NOMOR_INC_BLN||' / '||LPAD(NOMOR_INC_THN, 3, '0')||' / '||NOMOR_OBJEK||' / '||NOMOR_BLN_THN NO_PERMOHONAN");*/
            return $this->db->get('V_LAP_DET_PERFORASI')->row();
    }
    function get_upt(){
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="";
        } else {
            $wh="and id_inc='$upt_id'";
        }
        return $this->db->query("SELECT * FROM UNIT_UPT where id_inc !='2' $wh")->result();
     }
     function total_rows_rekap_reklame($bulan = NULL,$tahun = NULL,$sts = NULL,$param=NULL) {
        $this->db->from('UPT_REKAP_REKLAME');
        $this->db->where("TO_CHAR(TGL_PENETAPAN,'YYYY')='$tahun'");
            if ($sts !='') {
                $this->db->where('STATUS', $sts);
            }
            if ($bulan !='') {
                $this->db->where("TO_CHAR(TGL_PENETAPAN,'fmMM')='$bulan'");
            }
            if ($param !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(OBJEK_PAJAK) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
           } 
        //$this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        //$this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        //$this->db->where("STATUS ='0'");
        return $this->db->count_all_results();
        }
        // get data with limit and search
    function get_limit_data_rekap_reklame($limit, $start = 0, $bulan = NULL,$tahun = NULL,$sts = NULL,$param=NULL) {
       // $this->db->order_by('ID_INC', $this->order);
        $this->db->select("CETAK_SKPD,ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') AS TGL_BAYAR,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,POTONGAN,TOTAL");    
       // $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
       // $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("TO_CHAR(TGL_PENETAPAN,'YYYY')='$tahun'");
        if ($sts !='') {
                $this->db->where('STATUS', $sts);
            }
        if ($bulan !='') {
            $this->db->where("TO_CHAR(TGL_PENETAPAN,'fmMM')='$bulan'");
            }
        if ($param !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(OBJEK_PAJAK) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
        } 
        $this->db->order_by('ID_INC DESC');
        //$this->db->order_by('TGL_PENETAPAN DESC');
        //$this->db->where("STATUS ='0'");
        $this->db->limit($limit, $start);
        return $this->db->get('UPT_REKAP_REKLAME')->result();
        }
    function get_all_data_rekap_reklame($bulan = NULL,$tahun =NULL,$sts = NULL,$param=NULL) {
       // $this->db->order_by('ID_INC', $this->order);
        $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') AS TGL_BAYAR,
                          TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,TOTAL");    
       // $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
       // $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where("TO_CHAR(TGL_PENETAPAN,'YYYY')='$tahun'");
        if ($sts !='') {
                $this->db->where('STATUS', $sts);
            }
        if ($bulan !='') {
            $this->db->where("TO_CHAR(TGL_PENETAPAN,'fmMM')='$bulan'");
            }
        if ($param !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(OBJEK_PAJAK) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
        } 
        $this->db->order_by('TGL_PENETAPAN DESC');
        //$this->db->where("STATUS ='0'");
        return $this->db->get('UPT_REKAP_REKLAME')->result();
        }
    function sum_skpd_reklame($bulan = NULL,$tahun = NULL,$sts = NULL,$param=NULL) {
            $this->db->select("SUM(TAGIHAN+DENDA)JUMLAH");  
            $this->db->where("TO_CHAR(TGL_PENETAPAN,'YYYY')='$tahun'");
            if ($sts !='') {
                $this->db->where('STATUS', $sts);
            }
            if ($bulan !='') {
            $this->db->where("TO_CHAR(TGL_PENETAPAN,'fmMM')='$bulan'");
            }
            if ($param !='') {
            $this->db->where("(LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(OBJEK_PAJAK) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
            } 
            return $this->db->get('UPT_REKAP_REKLAME')->row();
    }
    function total_rows_manage_kobil(/*$tahun = NULL,$bulan = NULL,*/$param = NULL) {
        $this->db->select("*"); 
        if ($param!=null) {
            $this->db->where("STATUS ='0' AND (KODE_BILING LIKE '%$param%' OR LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(NAMA_USAHA) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
            /*$this->db->like("KODE_BILING",$param);
            $this->db->or_like("NAMA_WP",$param);
            $this->db->or_like("NAMA_USAHA",$param);
            $this->db->or_like("NPWPD",$param);*/
        } else {
           $this->db->where("STATUS =''");
        }
        $this->db->from("V_GET_TAGIHAN");
        return $this->db->count_all_results();
        }

    function get_limit_data_manage_kobil($limit, $start = 0, /*$tahun = NULL,$bulan = NULL,*/$param = NULL) {        
        $this->db->select("*");
        
        if ($param!=null) {
            $this->db->where("STATUS ='0' AND (KODE_BILING LIKE '%$param%' OR LOWER(NAMA_WP) LIKE '%$param%' OR LOWER(NAMA_USAHA) LIKE '%$param%' OR NPWPD LIKE '%$param%')",null,false);
            /*$this->db->like("KODE_BILING",$param);
            $this->db->or_like("NAMA_WP",$param);
            $this->db->or_like("NAMA_USAHA",$param);
            $this->db->or_like("NPWPD",$param);*/
        } else {
           $this->db->where("STATUS =''");
        }
        
        
        //$this->db->where("TO_CHAR(TO_CHAR(TO_DATE(TGL_PENETAPAN,'dd/mm/yyyy'),'YYYY')='$tahun'");
        //$this->db->where("TO_CHAR(TO_CHAR(TO_DATE(TGL_PENETAPAN,'dd/mm/yyyy'),'fmMM')='$bulan'");
       // $this->db->order_by('TGL_PENETAPAN');
        $this->db->order_by('ID_INC DESC');
        $this->db->limit($limit, $start);
        return $this->db->get('V_GET_TAGIHAN')->result();
    }
    function total_rows_rekap_pembayaran($tgl1 = NULL,$tgl2 = NULL,$upt=NULL,$mtd=NULL) {
        $this->db->from('V_LAPORAN_PEMBAYARAN');
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('JENIS_PAJAK',$upt);
        if ($mtd!=null) {
            $this->db->where('METODE_PEMBAYARAN',$mtd);
        }
        //$this->db->where("STATUS ='0'");
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_pembayaran($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$upt=NULL,$mtd=NULL) {
        
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        //$this->db->where("STATUS ='0'");
        $this->db->where('JENIS_PAJAK',$upt);
        if ($mtd!=null) {
            $this->db->where('METODE_PEMBAYARAN',$mtd);
        }
        $this->db->order_by('TGL_BAYAR');
        $this->db->order_by('KODE_BILING');
        $this->db->limit($limit, $start);
    return $this->db->get('V_LAPORAN_PEMBAYARAN')->result();
    }
     function sum_pembayaran($tgl1 = NULL,$tgl2 = NULL,$upt=NULL,$mtd=NULL) {
        $this->db->select("SUM(JUMLAH_BAYAR)JUMLAH");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('JENIS_PAJAK',$upt);
        if ($mtd!=null) {
            $this->db->where('METODE_PEMBAYARAN',$mtd);
        }
        return $this->db->get('V_LAPORAN_PEMBAYARAN')->row();
    }
    function sum_tagihan($tgl1 = NULL,$tgl2 = NULL,$upt=NULL,$mtd=NULL) {
        $this->db->select("SUM(TAGIHAN)JUMLAH");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('JENIS_PAJAK',$upt);
        if ($mtd!=null) {
            $this->db->where('METODE_PEMBAYARAN',$mtd);
        }
        return $this->db->get('V_LAPORAN_PEMBAYARAN')->row();
    }
    function total_rows_rekap_realisasi_perpajak($tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
        $this->db->from('V_REALISASI_PERPAJAK');
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('JENIS_PAJAK',$upt);
        //$this->db->where("STATUS ='0'");
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_realisasi_perpajak($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
        
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        //$this->db->where("STATUS ='0'");
        $this->db->where('JENIS_PAJAK',$upt);
        $this->db->order_by('TGL_BAYAR');
        $this->db->order_by('KODE_BILING');
        $this->db->limit($limit, $start);
    return $this->db->get('V_REALISASI_PERPAJAK')->result();
    }
     function sum_realisasi_perpajak($tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
        $this->db->select("SUM(JUMLAH_BAYAR)JUMLAH");    
        //$this->db->from('V_REALISASI_PERPAJAK');
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('JENIS_PAJAK',$upt);
        return $this->db->get('V_REALISASI_PERPAJAK')->row();
    }
    function total_rows_rekap_realisasi_perpajak_upt($tgl1 = NULL,$tgl2 = NULL,$jp=NULL,$kec=NULL,$kel=NULL) {
        $this->db->from('V_REALISASI_PERPAJAK_NON_MAMIN');
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('JENIS_PAJAK',$jp);
           if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_realisasi_perpajak_upt($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$jp=NULL,$kec=NULL,$kel=NULL) {
        
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('JENIS_PAJAK',$jp);
            if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
        $this->db->order_by('TGL_BAYAR');
        $this->db->order_by('KODE_BILING');
        $this->db->limit($limit, $start);
    return $this->db->get('V_REALISASI_PERPAJAK_NON_MAMIN')->result();
    }
     function sum_realisasi_perpajak_upt($tgl1 = NULL,$tgl2 = NULL,$jp=NULL,$kec=NULL,$kel=NULL) {
        $this->db->select("SUM(JUMLAH_BAYAR)JUMLAH");    
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
                if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
        $this->db->where('JENIS_PAJAK',$jp);
        return $this->db->get('V_REALISASI_PERPAJAK_NON_MAMIN')->row();
    }

    function total_rows_rekap_realisasi_reklame($tgl1 = NULL,$tgl2 = NULL,$kec=NULL,$kel=NULL,$user=NULL) {
        $this->db->from('V_REALISASI_REKLAME');
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
           if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KECAMATAN",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KECAMATAN",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KELURAHAN",$kel);}
            if ($user!=NULL){
                if ($user==2) {
                    $this->db->where("UPT_INSERT ='2'");
                } else if($user==1){
                    $this->db->where("UPT_INSERT !='2'");
                }
                      
            }
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_realisasi_reklame($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$kec=NULL,$kel=NULL,$user=NULL) {
        
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
            if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KECAMATAN",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KECAMATAN",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KELURAHAN",$kel);}
            if ($user!=NULL){
                if ($user==2) {
                    $this->db->where("UPT_INSERT ='2'");
                } else if($user==1){
                    $this->db->where("UPT_INSERT !='2'");
                }
                      
            }
        $this->db->order_by('DATE_BAYAR');
        $this->db->order_by('ID_DET_REKLAME');
        $this->db->limit($limit, $start);
    return $this->db->get('V_REALISASI_REKLAME')->result();
    }
     function sum_realisasi_reklame($tgl1 = NULL,$tgl2 = NULL,$kec=NULL,$kel=NULL,$user=NULL) {
        $this->db->select("SUM(KETETAPAN)JUMLAH");    
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
                if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KECAMATAN",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KECAMATAN",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KELURAHAN",$kel);}
            if ($user!=NULL){
                if ($user==2) {
                    $this->db->where("UPT_INSERT ='2'");
                } else if($user==1){
                    $this->db->where("UPT_INSERT !='2'");
                }
                      
            }
        return $this->db->get('V_REALISASI_REKLAME')->row();
    }
    function total_rows_rekap_tempat_usaha($jp=NULL,$kec=NULL,$kel=NULL,$npwp=NULL) {
        $this->db->from('V_REKAP_TEMPAT_USAHA');
        $this->db->where('JENIS_PAJAK',$jp);
           if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
            if ($npwp!=NULL) {
                $this->db->where("(LOWER(NAMA) LIKE '%$npwp%' OR LOWER(NAMA_USAHA) LIKE '%$npwp%' OR NPWPD LIKE '%$npwp%')",null,false);
            }
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_tempat_usaha($limit, $start = 0, $jp=NULL,$kec=NULL,$kel=NULL,$npwp=NULL,$thn=NULL) {        
        $this->db->select("NAMA,NPWPD,NAMA_USAHA,ALAMAT_USAHA,NAMAKELURAHAN,NAMAKEC,NAMA_PAJAK,DESKRIPSI,ID_INC,TOTAL_PELAPORAN_PAJAK(ID_INC,'$thn') REALISASI");    
        $this->db->where('JENIS_PAJAK',$jp);
            if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
            if ($npwp!=NULL) {
                $this->db->where("(LOWER(NAMA) LIKE '%$npwp%' OR LOWER(NAMA_USAHA) LIKE '%$npwp%' OR NPWPD LIKE '%$npwp%')",null,false);
            }
        $this->db->order_by('KODEKEC');
        $this->db->order_by('ID_INC');
        $this->db->limit($limit, $start);
    return $this->db->get('V_REKAP_TEMPAT_USAHA')->result();
    }

    function total_rows_cek_laporan_pajak($th=NULL,$jp=NULL,$kec=NULL,$kel=NULL,$npwpd=NULL) {
        $this->db->from('V_SKPDKB');
        $this->db->where('JENIS_PAJAK',$jp);
        $this->db->where('TAHUN_PAJAK',$th);
           if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
            if ($npwpd!=NULL) {
                 $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%')",null,false);
            }
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_cek_laporan_pajak($limit, $start = 0, $th=NULL,$jp=NULL,$kec=NULL,$kel=NULL,$npwpd=NULL) {        
        $this->db->select("*");    
        $this->db->where('JENIS_PAJAK',$jp);
        $this->db->where('TAHUN_PAJAK',$th);
            if ($this->upt==2) {
                    if ($kec!=NULL){$this->db->where("KODEKEC",$kec);}
                } else {
                     if ($kec==NULL) {
                        $this->db->where("KODE_UPT",$this->upt);            
                    }else {
                        $this->db->where("KODEKEC",$kec);
                    }
                }
            if ($kel!=NULL) {$this->db->where("KODEKEL",$kel);}
            if ($npwpd!=NULL) {
                 $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%')",null,false);
            }
        $this->db->order_by('KODEKEC');
        $this->db->order_by('ID_INC');
        $this->db->limit($limit, $start);
    return $this->db->get('V_SKPDKB')->result();
    }

    function total_rows_rekap_wp_tempat_usaha($tgl1 = NULL,$tgl2 = NULL,$sts=NULL,$jwp=NULL,$id_op=NULL) {
                $this->db->from('V_REKAP_WP_TEMPAT_USAHA');
                if ($tgl1!=NULL){
                   $this->db->where("TO_DATE(TO_CHAR(S_VALID,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
                   $this->db->where("TO_DATE(TO_CHAR(S_VALID,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
                }
                if ($sts!=NULL){
                    $this->db->where("JENIS_PAJAK","$sts");
                }
                if ($jwp!=NULL){
                    $this->db->where("JENISWP","$jwp");
                }
                if ($id_op!=NULL){
                    $this->db->where("ID_OP","$id_op");
                }  
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_wp_tempat_usaha($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$sts=NULL,$jwp=NULL,$id_op=NULL ) {

    $this->db->select("*");
    if ($tgl1!=NULL){
        $this->db->where("TO_DATE(TO_CHAR(S_VALID,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(S_VALID,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
    }
    if ($sts!=NULL){
        $this->db->where("JENIS_PAJAK","$sts");
    }
    if ($jwp!=NULL){
                    $this->db->where("JENISWP","$jwp");
                }
                if ($id_op!=NULL){
                    $this->db->where("ID_OP","$id_op");
                }
    $this->db->order_by('S_VALID', 'DESC');
    $this->db->order_by('ID_INC', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('V_REKAP_WP_TEMPAT_USAHA')->result();
    }

    function total_rows_rekon($tgl1 = NULL,$tgl2 = NULL,$sts=NULL) {
                $this->db->from('V_REKON');
                if ($tgl1!=NULL){
                   $this->db->where("TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
                   $this->db->where("TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
                }
                
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekon($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$sts=NULL ) {

    $this->db->select("*");
    if ($tgl1!=NULL){
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')"); 
    }
    
    $this->db->order_by('TANGGAL', 'DESC');
    /*$this->db->order_by('ID_INC', 'DESC');*/
    $this->db->limit($limit, $start);
    return $this->db->get('V_REKON')->result();
    }

     function total_rows_rekap_galian_detail($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_GALIAN_DETAIL');
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($this->upt==2) {
        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
    } else {
         if ($kecamatan==NULL) {
            $this->db->where("KODE_UPT",$this->upt);            
        }else {
            $this->db->where("KODEKEC",$kecamatan);
        }
    }
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
    }
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_galian_detail($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
  
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') AS TGL_BAYAR,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,NAMAKEC,NAMAKELURAHAN,PAJAK_TERUTANG TAGIHAN,PAJAK_TERUTANG AS JUMLAH_KETETAPAN,ID_LOGAM,FILE_TRANSAKSI,DESKRIPSI,NOREK,VOLUME,PAJAK,HARGA_DASAR");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($this->upt==2) {
        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
    } else {
         if ($kecamatan==NULL) {
            $this->db->where("KODE_UPT",$this->upt);            
        }else {
            $this->db->where("KODEKEC",$kecamatan);
        }
    }
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
    }
    $this->db->order_by('ID_INC', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_GALIAN_DETAIL')->result();
    }
    function get_all_rekap_galian_detail( $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') AS TGL_BAYAR,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,NAMAKEC,NAMAKELURAHAN,PAJAK_TERUTANG TAGIHAN,PAJAK_TERUTANG AS JUMLAH_KETETAPAN,ID_LOGAM,FILE_TRANSAKSI,DESKRIPSI,NOREK,VOLUME,PAJAK,HARGA_DASAR");       
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($this->upt==2) {
        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
    } else {
         if ($kecamatan==NULL) {
            $this->db->where("KODE_UPT",$this->upt);            
        }else {
            $this->db->where("KODEKEC",$kecamatan);
        }
    }
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
    }
    return $this->db->get('UPT_REKAP_GALIAN_DETAIL')->result();
    }    
    function sum_tagihan_galian_detail($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(PAJAK_TERUTANG)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($this->upt==2) {
        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
    } else {
         if ($kecamatan==NULL) {
            $this->db->where("KODE_UPT",$this->upt);            
        }else {
            $this->db->where("KODEKEC",$kecamatan);
        }
    }
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR NPWPD LIKE '%$npwpd%')",null,false);
    }
    return $this->db->get('UPT_REKAP_GALIAN_DETAIL')->row();
    }
    
    function total_rows_rekap_harian_resto($tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
        $this->db->from('V_REKAP_TRANSAKSI_RESTORAN');
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL_TRX,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL_TRX,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('ID_TEMPAT_USAHA',$upt);
        //$this->db->where("STATUS ='0'");
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_harian_resto($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
        
        $this->db->select("*");    
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL_TRX,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL_TRX,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        //$this->db->where("STATUS ='0'");
        $this->db->where('ID_TEMPAT_USAHA',$upt);
        $this->db->order_by('TANGGAL_TRX');
        $this->db->order_by('ID_TRX');
        $this->db->limit($limit, $start);
    return $this->db->get('V_REKAP_TRANSAKSI_RESTORAN')->result();
    }
     function sum_realisasi_harian_resto($tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
        $this->db->select("SUM(HARGA*QTY)JUMLAH");    
        //$this->db->from('V_REKAP_TRANSAKSI_RESTORAN');
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL_TRX,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
        $this->db->where("TO_DATE(TO_CHAR(TANGGAL_TRX,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        $this->db->where('ID_TEMPAT_USAHA',$upt);
        return $this->db->get('V_REKAP_TRANSAKSI_RESTORAN')->row();
    }

    function total_rows_rekap_realisasi_perwp($tahun = NULL,$jp = NULL,$npwpd=NULL) {
        $this->db->from('V_REALISASI_PERWP');
        $this->db->where('TAHUN_LUNAS',$tahun);
        if ($jp!=NULL){
        $this->db->where('JENIS_PAJAK',$jp); 
        }
        if ($npwpd!=NULL) {
            $this->db->where("NPWPD LIKE '%$npwpd%'",null,false);
        }
        
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_realisasi_perwp($limit, $start = 0, $tahun = NULL,$jp = NULL,$npwpd=NULL) {
        
        $this->db->select("*");    
        $this->db->where('TAHUN_LUNAS',$tahun);
        if ($jp!=NULL){
        $this->db->where('JENIS_PAJAK',$jp);
        }
        if ($npwpd!=NULL) {
            $this->db->where("NPWPD LIKE '%$npwpd%'",null,false);
        }        
        $this->db->order_by('TOTAL DESC');
        $this->db->order_by('JENIS_PAJAK');
        $this->db->limit($limit, $start);
    return $this->db->get('V_REALISASI_PERWP')->result();
    }
     function sum_realisasi_perwp($tahun = NULL,$jp = NULL,$npwpd=NULL) {
        $this->db->select("SUM(TOTAL)JUMLAH");    
        $this->db->where('TAHUN_LUNAS',$tahun);
        if ($jp!=NULL){
        $this->db->where('JENIS_PAJAK',$jp);
        }
        if ($npwpd!=NULL) {
            $this->db->where("NPWPD LIKE '%$npwpd%'",null,false);
        }
        return $this->db->get('V_REALISASI_PERWP')->row();
    }



    function total_rows_rekap_reklame_detail($tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$bulan= NULL,$jns=NULL) {
    $date=date('d-m-Y');
    $this->db->from('V_DET_REKLAME');
    $this->db->where('TAHUN_KETETAPAN', $tahun);
    
    if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
    if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
    }
    if ($bulan!=NULL) {$this->db->where("BULAN_KETETAPAN",$bulan);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_reklame_detail($limit, $start = 0, $tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$bulan= NULL,$jns=NULL) {
    $date=date('d-m-Y');
    $this->db->select("A.*,CASE WHEN JENIS_WAKTU = 1 THEN MASA||' th' WHEN JENIS_WAKTU = 2 THEN MASA||' bln' WHEN JENIS_WAKTU = 3 THEN MASA||' mgu' ELSE MASA||' hr' END AS WAKTU");
    $this->db->where('TAHUN_KETETAPAN', $tahun);
    if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
    if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
    }
    if ($bulan!=NULL) {$this->db->where("BULAN_KETETAPAN",$bulan);}
    $this->db->order_by('ID_INC', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('V_DET_REKLAME A')->result();
    }
    function get_all_data_rekap_reklame_detail($tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$bulan= NULL,$jns=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("A.*,CASE WHEN JENIS_WAKTU = 1 THEN MASA||' th' WHEN JENIS_WAKTU = 2 THEN MASA||' bln' WHEN JENIS_WAKTU = 3 THEN MASA||' mgu' ELSE MASA||' hr' END AS WAKTU");    
    $this->db->where('TAHUN_KETETAPAN', $tahun);
    if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
    if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
    }
    if ($bulan!=NULL) {$this->db->where("BULAN_KETETAPAN",$bulan);}
    return $this->db->get('V_DET_REKLAME A')->result();
    }
    function sum_tagihan_reklame_detail($tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$bulan= NULL,$jns=NULL) {
    $date=date('m-d-Y');
    $this->db->select("SUM(PAJAK_TERUTANG)JUMLAH");    
    $this->db->where('TAHUN_KETETAPAN', $tahun);
    if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
    if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
    }
    if ($bulan!=NULL) {$this->db->where("BULAN_KETETAPAN",$bulan);}
    return $this->db->get('V_DET_REKLAME')->row();
    }
    function total_rows_wp_non_aktif($nama=NULL) {
                $this->db->from('V_WP_NON_AKTIF');
                
                if ($cek!=NULL){
                    $this->db->like("NAMA","%$cek%");
                }         

    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_wp_non_aktif($limit, $start = 0,$nama=NULL) {

    $this->db->select("*");
    if ($nama!=NULL){
        $this->db->like("NAMA","%$nama%");
    }
    $this->db->order_by('WP_ID', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('V_WP_NON_AKTIF')->result();
    }
}
