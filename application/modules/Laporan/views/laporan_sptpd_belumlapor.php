   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>SPTPD belum Lapor</h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="post" action="<?php echo base_url().'Laporan/sptpd_belum_lapor'?>">
                <div class="form-group">
                  <select id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('bl_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php if ($this->session->userdata('bl_bulan')==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="KECAMATAN" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($keca as $kec){ ?>
                            <option  value="<?php echo $kec->KODEKEC?>"
                              <?php if ($this->session->userdata('bl_kecamatan')==$kec->KODEKEC) {echo "selected";} ?>><?php echo $kec->NAMAKEC ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="JENIS_PAJAK"  placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Jenis Pajak</option>
                            <?php foreach($jp as $jns){ ?>
                            <option  value="<?php echo $jns->ID_INC?>"
                              <?php if ($this->session->userdata('bl_jenis_pajak')==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
                <a href="<?php echo site_url('Excel/Excel/Excel_laporan_sptpd_belum_lapor'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>

        </form>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>             
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Alamat WP</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">ALamat OP</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Kelurahan</th>
              <th class="text-center">Aksi</th>
              <!-- <th>Status</th> -->
             <!--  <th>Aksi</th> -->
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };
    $.fn.dataTable.ext.errMode = 'throw';
    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        var myvar='<?php echo $session_value;?>';
        $('#mytable_filter input')
        .off('.DT')
        .on('keyup.DT', function(e) {
          if (e.keyCode == 13) {
            api.search(this.value).draw();
          }
        });
      },
      "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,


      'oLanguage':
      {
        "sProcessing":   "Sedang memproses...",
        "sLengthMenu":   "Tampilkan _MENU_ entri",
        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
        "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix":  "",
        "sSearch":       "Cari:",
        "sUrl":          "",
        "oPaginate": {
          "sFirst":    "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext":     "Selanjutnya",
          "sLast":     "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      pageLength: 20,
      ajax: {"url": "<?php echo base_url()?>Laporan/Laporan/json_sptpd_belum_lapor", "type": "POST"},
      columns: [
      {
        "data":"ID_INC",
        "orderable": false,
        "className" : "text-center"
      },
      /*{"data":"MASA_PAJAK",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '1' : return "Januari"; break;
          case '2' : return "Februari"; break;
          case '3' : return "Maret"; break;
          case '4' : return "April"; break;
          case '5' : return "Mei"; break;
          case '6' : return "Juni"; break;
          case '7' : return "Juli"; break;
          case '8' : return "Agustus"; break;
          case '9' : return "September"; break;
          case '10' : return "Oktober"; break;
          case '11' : return "November"; break;
          case '12' : return "Desember"; break;
         default  : return 'N/A';
       }}},
       {"data":"TAHUN_PAJAK"},*/
       {"data":"NAMA"},
       {"data":"NPWP"},
       {"data":"ALAMAT"},
       {"data":"NAMA_PAJAK"},
       {"data":"NAMA_USAHA"},
       {
        "data":"ALAMAT_USAHA"
        //"className" : "text-right",
      },
       {"data":"NAMAKEC"},
        {"data":"NAMAKELURAHAN"},
        {
        "data":"action",
        "className" : "text-center"
        
      },
      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>

