   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/cek_laporan_pajak'?>">
                <div class="form-group">
                  <select name="th" required="required" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('th')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select name="jp"  class="form-control select2 col-md-7 col-xs-12" required>
                            <option value="">Pilih Jenis Pajak</option>
                            <?php foreach($jpjk as $jns){ ?>
                            <option  value="<?php echo $jns->ID_INC?>"
                              <?php if ($jp==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <div class="form-group">
                      <select onchange="getkel(this);" name="kec"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('kec')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kel"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('kel')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('npwpd'); ?>" placeholder="Nama Wp/Usaha/npwpd">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($jp <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/cek_laporan_pajak'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <!-- <a href="<?php echo site_url('Excel/Excel/Excel_cek_laporan_pajak'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a> -->
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">NAMA WP</th>
              <th class="text-center">Alamat WP</th>
              <th class="text-center">Nama Usaha</th>   
              <th class="text-center">Alamat Usaha</th>           
              <th class="text-center">JAN</th>
              <th class="text-center">FEB</th>
              <th class="text-center">MAR</th>
              <th class="text-center">APR</th>
              <th class="text-center">MEI</th>
              <th class="text-center">JUN</th>
              <td class="text-center">JUl</td>
              <th class="text-center">AGU</th>
              <th class="text-center">SEP</th>
              <th class="text-center">OKT</th>
              <th class="text-center">NOV</th>
              <th class="text-center">DES</th>
              <th class="text-center">Detail</th>  
              <th class="text-center">Kd Usaha</th> 
              <th class="text-center">Aksi</th>            
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($realisasi as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td></td>
                <td align="right"><?= number_format($rk->JAN,'0','','.')?></td>
                <td align="right"><?= number_format($rk->FEB,'0','','.')?></td>
                <td align="right"><?= number_format($rk->MAR,'0','','.')?></td>
                <td align="right"><?= number_format($rk->APR,'0','','.')?></td>
                <td align="right"><?= number_format($rk->MEI,'0','','.')?></td>   
                <td align="right"><?= number_format($rk->JUN,'0','','.')?></td>
                <td align="right"><?= number_format($rk->JUL,'0','','.')?></td>
                <td align="right"><?= number_format($rk->AGU,'0','','.')?></td>
                <td align="right"><?= number_format($rk->SEP,'0','','.')?></td>       
                <td align="right"><?= number_format($rk->OKT,'0','','.')?></td>
                <td align="right"><?= number_format($rk->NOV,'0','','.')?></td>
                <td align="right"><?= number_format($rk->DES,'0','','.')?></td>
                <td><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk->TEMPAT_USAHA_ID?>">detail</a> </td>
                <td><?= $rk->TEMPAT_USAHA_ID;?></td>
                <th class="text-center"><?php if ($this->session->userdata('MS_ROLE_ID')==3) {?>
                 <a href="<?php echo base_url('Laporan/Laporan/penerbitan_skpdkb/'.$rk->NPWPD.'/'.$rk->TEMPAT_USAHA_ID.'/'.$jp);?>">Terbitkan SKPDKB</a>
                <?php }else {
                  }?></th> 
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <?php foreach ($realisasi as $rk)  { ?>
  <div class="modal fade" id="myModal<?= $rk->TEMPAT_USAHA_ID?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $ID=$rk->JENIS_PAJAK;
        $npwpd=$rk->NPWPD;$tahun=$rk->TAHUN_PAJAK;$id_usaha=$rk->TEMPAT_USAHA_ID;?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">WAJIB PAJAK <?= strtoupper($rk->NAMA_WP);?>, NAMA USAHA <?= strtoupper($rk->NAMA_USAHA);?></h4>
        </div>
        <div class="modal-body">
         <div class="table-responsive">
        <table tyle="width: 100%;" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Masa</th> 
              <th class="text-center">Tahun</th>   
              <th class="text-center">NPWPD</th>  
              <th class="text-center">Nama</th>            
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">Nominal</th>
              <th class="text-center">Tgl Bayar</th>  
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0;$det_rek=$this->db->query("select kode_biling,nama_wp,objek_pajak,MASA_PAJAK,tahun_pajak,TGL_LUNAS,TAGIHAN from V_PENDATAAN_TAGIHAN
                    WHERE JENIS_PAJAK='$ID' and NPWPD='$npwpd' and tahun_pajak='$tahun' and tempat_usaha_id='$id_usaha' order by MASA_PAJAK")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $namabulan[$rk_d->MASA_PAJAK]?></td>
                <td><?= $rk_d->TAHUN_PAJAK?></td>
                <td><?= $rk_d->KODE_BILING?></td>
                <td><?= $rk_d->NAMA_WP?></td>
                <td><?= $rk_d->OBJEK_PAJAK?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk_d->TAGIHAN),'0','','.')?></td>
                <td align="center"><?php if ($rk_d->TGL_LUNAS==''){echo 'Belum Bayar';}else{echo $rk_d->TGL_LUNAS;}?></td>
              </tr>
              <?php  $no++;$tot+=str_replace(",",".",$rk_d->TAGIHAN);}  }else{ ?>
              <tr>
                <th colspan="12"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="6" class="tede" align="right"><b>TOTAL </b></td>
                <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?>
                </tr>
            </tbody>
          </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>

  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>