   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/management_kodebiling'?>">
                <div class="form-group">
                    <input type="text" name="param"  class="form-control1" value="<?php echo $this->session->userdata('param'); ?>" placeholder="KODE BILNG / NPWPD / NAMA WP / USAHA">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($param <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/management_kodebiling'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/management_kodebiling'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table width="100%" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Kode biling</th>
              <th>tgl</th>
              <th>Masa</th>
              <th>Tahun</th>
              <th>NPWPD</th>
              <th>Nama WP</th>
              <th>Alamat</th>
              <th>Nama Usaha</th>
              <th>Pajak</th>
              <th>Tagihan</th>
              <th>Denda</th>
              <th>Norek</th>
              <th>Aksi</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($manage_kobil as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td align="center"><?= $rk->KODE_BILING?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->MASA_PAJAK?></td>
                <td><?= $rk->TAHUN_PAJAK?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td align="right"><?=  number_format($rk->PAJAK_TERUTANG,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DENDA,'0','','.')?></td>
                <td align="right"><?=  $rk->KODE_REK ?></td>
                <td><?php if ($rk->STATUS=='1') {
                  # code...
                } else {
                    if ($this->session->userdata('MS_ROLE_ID')=='9') {?>
                      <a href="<?php echo base_url('Laporan/hapus_kobil/'.$rk->KODE_BILING.'/'.$rk->JENIS_PAJAK);?>" onclick="return confirm('Apakah anda yakin menghapus kode biling dari <?= $rk->NAMA_WP?>');"><span class="fa fa-trash"></span></a>
                      <?php
                    } else {
                      # code...
                    }
                }?>
                    <!-- <a href="<?php echo base_url('Pdf/Pdf/permohonan_perforasi/'.$rk->ID_INC);?>">[PERMOHONAN]</a> --></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
           <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }

.form-control1 {
    display: block;
    width:400px;
    height: 28px;
    padding: 3px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;}
  </style>

