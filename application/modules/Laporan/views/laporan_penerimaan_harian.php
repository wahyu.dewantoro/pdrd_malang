
<link href="<?php echo base_url().'gentelella/'?>vendors/main.css" rel="stylesheet">
    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:28px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3>Laporan Penerimaan Harian Pajak Daerah</h3>
  </div>
<div class="clearfix"></div>                  
<div class="row">
<?php  echo $this->session->flashdata('notif')?>
    <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/penerimaan_harian'?>">
                <div class="form-group">
                    <input style="font-size:16px;" type="text" name="tgl"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php echo $tgl;?>" >
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
        </form>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_content" >
                   <div class="col-md-12 col-sm-12 col-xs-12">                
                <div class="x_content">
                    <table  class="" cellspacing="0" width="100%" border>
                              <thead>
                                <tr>
                                  <th class="teha">NO</th>
                                  <th class="teha">PAJAK</th>
                                  <th class="teha">PENERIMAAN HARI INI</th>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php $tot=0;$target=0;$sd_ll=0;$hr_ini=0;$no=1; foreach($harian as $rk){?>
                                      <tr>
                                        <td class="tede" align="center"><?php echo $no?></td>
                                        <td class="tede"><?= $rk->NAMA_PAJAK;?></td>
                                        <td class="tede" align="right"><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk->JENIS_PAJAK?>"><?= number_format($rk->JUMLAH_HARI_INI,'0','','.');?></a></td>
                                      </tr>
                                  <?php $no++; 
                                        $hr_ini+=$rk->JUMLAH_HARI_INI;
                                        }?>
                                  <tr>
                                    <td colspan="2" class="tede" align="right"><b>TOTAL </b></td>
                                    <td class="tede" align="right"><b><?= number_format($hr_ini,'0','','.');?>
                                  </tr>
                              </tbody>
                           </table>
                  </div>
                </div>
                </div>
              </div>
            </div> 
          </div>
      </div>
    </div>
  </div>
</div>
<?php foreach ($harian as $rk)  { ?>
  <div class="modal fade" id="myModal<?= $rk->JENIS_PAJAK?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $ID=$rk->JENIS_PAJAK;?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">DETAIL DATA PAJAK <?= strtoupper($rk->NAMA_PAJAK);?></h4>
        </div>
        <div class="modal-body">
         <div class="table-responsive">
          <?php if ($rk->JENIS_PAJAK=='10') {?>
            <table tyle="width: 100%;" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="text-center" width="3%">No</th>
                  <th class="text-center">Kode Biling</th>  
                  <th class="text-center">Nama</th>            
                  <th class="text-center">NOP</th>
                  <th class="text-center">Nominal</th>
                </tr>
              </thead>
                <tbody>
                  <?php $no=1;$tot=0;$det_rek=$this->db->query("select * FROM PEMBAYARAN_SSPD@tobphtb
                        WHERE TO_CHAR(TANGGAL_PEMBAYARAN,'dd/mm/yyyy')= '$tgl'")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
                  <tr>
                    <td><?= $no?></td>
                    <td><?= $rk_d->ID_BILLING?></td>
                    <td><?= $rk_d->NAMA_WP?></td>
                    <td><?= $rk_d->NOP?></td>
                    <td align="right"><?=  number_format(str_replace(",",".",$rk_d->NOMINAL),'0','','.')?></td>
                  </tr>
                  <?php  $no++;$tot+=str_replace(",",".",$rk_d->NOMINAL);}  }else{ ?>
                  <tr>
                    <th colspan="9"> Tidak ada data.</th>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td colspan="4" class="tede" align="right"><b>TOTAL </b></td>
                    <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?>
                    </tr>
                </tbody>
              </table>
          <?php
            
          } else if ($rk->JENIS_PAJAK=='09') {?>
            <table tyle="width: 100%;" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="text-center" width="3%">No</th>
                  <!-- <th class="text-center">Kode Biling</th>  
                  <th class="text-center">Nama</th>  -->           
                  <th class="text-center">NOP</th>
                  <th class="text-center">Nominal</th>
                </tr>
              </thead>
                <tbody>
                  <?php $no=1;$tot=0;$det_rek=$this->db->query("select KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK nop,JML_SPPT_YG_DIBAYAR NOMINAL from pembayaran_sppt@Tolayanan
WHERE TO_CHAR(tgl_pembayaran_sppt,'dd/mm/yyyy')='$tgl'")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
                  <tr>
                    <td><?= $no?></td>
                   <!--  <td><?= $rk_d->ID_BILLING?></td>
                    <td><?= $rk_d->NAMA_WP?></td> -->
                    <td><?= $rk_d->NOP?></td>
                    <td align="right"><?=  number_format(str_replace(",",".",$rk_d->NOMINAL),'0','','.')?></td>
                  </tr>
                  <?php  $no++;$tot+=str_replace(",",".",$rk_d->NOMINAL);}  }else{ ?>
                  <tr>
                    <th colspan="9"> Tidak ada data.</th>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td colspan="4" class="tede" align="right"><b>TOTAL </b></td>
                    <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?>
                    </tr>
                </tbody>
              </table>
          <?php      
          } else {?>
            <table tyle="width: 100%;" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="text-center" width="3%">No</th>
                  <th class="text-center">NPWPD</th>  
                  <th class="text-center">Nama</th>            
                  <th class="text-center">Objek Pajak</th>
                  <th class="text-center">Nominal</th>
                </tr>
              </thead>
                <tbody>
                  <?php $no=1;$tot=0;$det_rek=$this->db->query("select kode_biling,nama_wp,objek_pajak,jumlah_bayar from V_REALISASI_PERPAJAK 
                        WHERE JENIS_PAJAK='$ID' and TO_DATE(TO_CHAR(TGL_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('$tgl','DD-MM-YYYY')")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
                  <tr>
                    <td><?= $no?></td>
                    <td><?= $rk_d->KODE_BILING?></td>
                    <td><?= $rk_d->NAMA_WP?></td>
                    <td><?= $rk_d->OBJEK_PAJAK?></td>
                    <td align="right"><?=  number_format(str_replace(",",".",$rk_d->JUMLAH_BAYAR),'0','','.')?></td>
                  </tr>
                  <?php  $no++;$tot+=str_replace(",",".",$rk_d->JUMLAH_BAYAR);}  }else{ ?>
                  <tr>
                    <th colspan="9"> Tidak ada data.</th>
                  </tr>
                  <?php } ?>
                  <tr>
                    <td colspan="4" class="tede" align="right"><b>TOTAL </b></td>
                    <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?>
                    </tr>
                </tbody>
              </table>
          <?php }?>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>
<style type="text/css">
  .teha{
    font-size:18px;
    text-align: center;
    background-color:#eee;
     border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  .tede {
    font-size:19px;
    background-color:#eee;
    border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  body{
    color: #000;
  }
  a {
  color: black;
}
</style>
<script type="text/javascript">
  
  setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 64000);
</script>

