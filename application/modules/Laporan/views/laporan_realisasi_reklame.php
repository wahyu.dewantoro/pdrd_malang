   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_realisasi_reklame'?>">
                <div class="form-group">
                    <input type="text" name="tgl1"  required="required" class="form-control tanggal" style="width: 119px" value="<?php if ($tgl1==null) {echo $def1;} else {echo $tgl1;}?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2"  required="required" class="form-control tanggal" style="width: 119px" value="<?php if ($tgl2==null) {echo $def2;} else {echo $tgl2;}?>" >
                </div>
                <div class="form-group">
                      <select onchange="getkel(this);" name="kec"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('kec')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kel"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('kel')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                <div class="form-group">
                      <select  name="user"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua User input</option>
                          <option <?php if($this->session->userdata('user')=='2'){echo "selected";}?> value="2">Dinas</option>
                          <option <?php if($this->session->userdata('user')=='1'){echo "selected";}?> value="1">Upt</option>
                         <!--  <option <?php if($this->session->userdata('user')=='0'){echo "selected";}?> value="0">Wp</option>      -->
                        </select>
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/laporan_realisasi_reklame'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_laporan_realisasi_reklame'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
             <th class="text-center" width="3%">No</th>
              <th class="text-center">Tgl Bayar</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Lokasi Pasang</th>
              <th class="text-center">KEC</th>
              <th class="text-center">KEL</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">TEKS</th>
              <th class="text-center">P/L</th>
              <th class="text-center">SISI</th>
              <th class="text-center">JUMLAH</th>
              <th class="text-center">AWAL PAJAK</th>
              <th class="text-center">AKHIR PAJAK</th>
              <th class="text-center">KETETAPAN</th>
              <th class="text-center">KODE REK</th>
              <th class="text-center">User Input</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($realisasi as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->TGL_TRX?></td>
                <td><?= $rk->KODE_BILING?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->LOKASI_PASANG?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td><?= $rk->TEKS?></td>
                <td><?= number_format($rk->P,1,",",".").' x '.number_format($rk->L,1,",",".")?></td>
                <td align="right"><?= $rk->S?></td>
                <td align="right"><?= $rk->JUMLAH?></td>
                <td><?= $rk->BERLAKU?></td>
                <td><?= $rk->AKHIR_BERLAKU?></td>
                <td align="right"><?=  number_format($rk->KETETAPAN,'0','','.')?></td>
                <td><?= $rk->NOREK?></td>
                <td><?= $rk->NAMA_UNIT?></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Total Pembayaran : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
     <!--  <marquee direction="right" style="font-size:14px;color:#fff;" bgcolor="#000080" scrolldelay="199"><i>Khusus Pajak Reklame Realisasi berdasarkan Kode Biling, <b>Bukan Berdasarkan tempat pasang</b></i></marquee> -->
    </div>
  </div>
</div>

  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
 <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>
