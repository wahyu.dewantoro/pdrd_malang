   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_wp_non_aktif'?>">
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('npwpd'); ?>" placeholder="npwpd">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tahun <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/laporan_wp_non_aktif'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_laporan_wp_non_aktif'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">JENIS</th>
              <th class="text-center">NAMA WP</th>
              <th class="text-center">ALAMAT</th>
              <th class="text-center">HP</th>
              <th class="text-center">NAMA USAHA</th>
              <th class="text-center">TGL DAFTAR</th>
              <th class="text-center">TGL NON AKTIF</th> 
              <th class="text-center">DETAIL</th> 
              <!-- <th class="text-center">STATUS</th> -->                            
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($data_wp as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?php echo $rk->NPWP;?></td>
                <td><?php if ($rk->JENISWP=='1') {
                  echo "Perorangan";
                } else if ($rk->JENISWP=='2') {
                  echo "Badan";
                } else {
                    
                }?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->ALAMAT?></td>
                <td><?= $rk->NO_TELP?></td>
                <td><?= $rk->NAMA_BADAN?></td>
                <td><?= $rk->TGL_DAFTAR?></td>
                <td><?= $rk->DATE_HAPUS?></td>
                <td align="center"></td>               
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
      
    </div>
  </div>
</div>
  
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>

