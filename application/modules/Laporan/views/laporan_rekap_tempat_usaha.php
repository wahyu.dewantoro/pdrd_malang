   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/rekap_tempat_usaha'?>">
                <div class="form-group">
                      <select name="thn" required="required" class="form-control select2 col-md-7 col-xs-12">
                                <?php $thnskg = date('Y');
                                for($i=$thnskg; $i>=$thnskg-3; $i--){ ?>
                                <option value="<?php echo $i; ?>"
                                <?php if ($this->session->userdata('thn')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                          </select>
                </div>
                <div class="form-group">
                      <select name="jp"  class="form-control select2 col-md-7 col-xs-12" required>
                            <option value="">Pilih Jenis Pajak</option>
                            <?php foreach($jpjk as $jns){ ?>
                            <option  value="<?php echo $jns->ID_INC?>"
                              <?php if ($jp==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <div class="form-group">
                      <select onchange="getkel(this);" name="kec"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('kec')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kel"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('kel')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                <div class="form-group">
                    <input type="text" name="npwp"  class="form-control" value="<?php echo $this->session->userdata('npwp'); ?>" placeholder="npwpd / nama wp / usaha">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($jp <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/rekap_tempat_usaha'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_rekap_tempat_usaha'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Desa</th>
              <th class="text-center">Jenis pajak</th>
              <th class="text-center">kategori</th>
              <th class="text-center">Realisasi</th>
              <th class="text-center">Detail</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($realisasi as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td align="right"><?=  number_format($rk->REALISASI,'0','','.')?></td>
                <td><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk->ID_INC?>">detail</a> </td>        
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <?php foreach ($realisasi as $rk)  { ?>
  <div class="modal fade" id="myModal<?= $rk->ID_INC?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $id_usaha=$rk->ID_INC; $tahun=$this->session->userdata('thn');?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h5 class="modal-title">WAJIB PAJAK <?= strtoupper($rk->NAMA);?>, NAMA USAHA <?= strtoupper($rk->NAMA_USAHA);?> REALISASI TAHUN <?= $this->session->userdata('thn')?></h5>
        </div>
        <div class="modal-body">
         <div class="table-responsive">
        <table tyle="width: 100%;" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Masa</th> 
              <th class="text-center">Tahun</th>   
              <th class="text-center">NPWPD</th>  
              <th class="text-center">Nama</th>            
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">Nominal</th>
              <th class="text-center">Tgl Bayar</th>  
            </tr>
          </thead>
           <tbody>
              <?php $no=1;$tot=0;$det_rek=$this->db->query("SELECT * from V_LAPORAN_PEMBAYARAN_ALL WHERE TAHUN_BAYAR='$tahun' and tempat_usaha_id='$id_usaha' order by MASA_PAJAK")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $namabulan[$rk_d->MASA_PAJAK]?></td>
                <td><?= $rk_d->TAHUN_PAJAK?></td>
                <td><?= $rk_d->KODE_BILING?></td>
                <td><?= $rk_d->NAMA_WP?></td>
                <td><?= $rk_d->OBJEK_PAJAK?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk_d->JUMLAH_BAYAR),'0','','.')?></td>
                <td align="center"><?php if ($rk_d->TGL_LUNAS==''){echo 'Belum Bayar';}else{echo $rk_d->TGL_LUNAS;}?></td>
              </tr>
              <?php  $no++;$tot+=str_replace(",",".",$rk_d->JUMLAH_BAYAR);}  }else{ ?>
              <tr>
                <th colspan="12"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="6" class="tede" align="right"><b>TOTAL </b></td>
                <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?>
                </tr>
            </tbody>
          </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>

  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>