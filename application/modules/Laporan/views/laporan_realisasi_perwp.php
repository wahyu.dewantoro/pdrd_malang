   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_realisasi_perwp'?>">
                <div class="form-group">
                  <select id="TAHUN_PAJAK" name="tahun_pajak" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-3; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('tahun_pj')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select name="jp"  class="form-control select2 col-md-7 col-xs-12" >
                            <option value="">semua Jenis Pajak</option>
                            <?php foreach($jpjk as $jns){ ?>
                            <option  value="<?php echo $jns->ID_INC?>"
                              <?php if ($jp==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('npwpd'); ?>" placeholder="npwpd">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tahun <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/laporan_realisasi_perwp'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_laporan_realisasi_perwp'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tahun Realisasi</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Alamat WP</th>
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">Jenis Pajak</th>
              <th class="text-center">Total Realisasi</th>
              <th class="text-center">Detail</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($realisasi as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->TAHUN_LUNAS?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td align="right"><?=  number_format($rk->TOTAL,'0','','.')?></td>
                <td align="center"></td>               
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Total Pembayaran : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
      
    </div>
  </div>
</div>
<?php foreach ($realisasi as $rk)  { ?>
  <div class="modal fade" id="myModal<?= $rk->KODE_BILING?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $ID=$rk->KODE_BILING;?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">DETAIL DATA REKLAME</h4>
        </div>
        <div class="modal-body">
         <div class="table-responsive">
        <table tyle="width: 100%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Deskripsi</th>            
              <th class="text-center">Pajak Terutang</th>
              <th class="text-center">No Rek</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0;$det_rek=$this->db->query("SELECT a.id_inc,DESKRIPSI,
         SPTPD_REKLAME_ID,b.KODE_BILING,a.PAJAK_TERUTANG ,norek
                  from sptpd_reklame_detail a
                   join  sptpd_reklame b on A.SPTPD_REKLAME_ID=B.ID_INC     
                   join objek_pajak f on a.golongan=f.id_op WHERE KODE_BILING='$ID'")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $rk_d->DESKRIPSI?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk_d->PAJAK_TERUTANG),'0','','.')?></td>
                <td align="right"><?= $rk_d->NOREK?></td>
              </tr>
              <?php  $no++;$tot+=str_replace(",",".",$rk_d->PAJAK_TERUTANG);}  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              
            </tbody>
          </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>
  <?php foreach ($realisasi as $rk)  { ?>
  <div class="modal fade" id="myModal1<?= $rk->KODE_BILING?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $ID=$rk->KODE_BILING;?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">DETAIL DATA REKLAME</h4>
        </div>
        <div class="modal-body">
         <div class="table-responsive">
        <table tyle="width: 100%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Deskripsi</th>            
              <th class="text-center">Pajak Terutang</th>
              <th class="text-center">No Rek</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0;$det_rek=$this->db->query("select deskripsi,a.* from SPTPD_MINERAL_DETAIL a
                                                        join objek_pajak b on a.id_op=b.id_op 
                                                        join sptpd_mineral_non_logam c on c.id_inc=A.MINERAL_ID
                                                        WHERE KODE_BILING='$ID'")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $rk_d->DESKRIPSI?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk_d->PAJAK_TERUTANG),'0','','.')?></td>
                <td align="right"><?= $rk_d->NOREK?></td>
              </tr>
              <?php  $no++;$tot+=str_replace(",",".",$rk_d->PAJAK_TERUTANG);}  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              <!-- <tr>
                <td colspan="8"><b>TOTAL</b></td>
               
                <td align="right"><?=  number_format($tot,'0','','.')?></td>                                
              </tr>
              <tr>
                <td colspan="8"><b>DENDA</b></td>
              
                <td align="right"><?=  number_format($rk->DENDA,'0','','.')?></</td>
                                     
              </tr>
              <tr>
                <td colspan="8"><b>POTONGAN</b></td>
                <td align="right"><?=  number_format($rk->POTONGAN,'0','','.')?></</td>
                                            
              </tr>
              <tr>
                <td colspan="8"><b>TOTAL TAGIHAN</b></td>
                <td align="right"><?=  number_format($tot+$rk->DENDA-$rk->POTONGAN,'0','','.')?></td>                                
              </tr> -->
            </tbody>
          </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>
  
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>

