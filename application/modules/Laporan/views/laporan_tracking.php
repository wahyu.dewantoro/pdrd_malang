    <div class="page-title">
     <div class="title_left">
     <!--  <h3>Tracking Kode Biling</h3> -->
    </div>
    <div class="pull-right">
      
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    
      <div class="col-md-10 col-sm-12 col-xs-12">
      <form id="demo-form2" data-parsley-validate class="form-inline"  method="post" enctype="multipart/form-data" >
        <?php echo $this->session->flashdata('notif')?>
        <div class="x_panel">
          <div class="x_title">
                  <h4><i class="fa fa-search"></i> Tracking Kode Biling</h4>
                  <div class="clearfix"></div>
                </div>
          <div class="x_content" >
            
              
                <div class="form-group">
                    Kode Biling
                </div>
                <div class="form-group">
                  <input id="KODE_BILING" type="text" name="kode_biling" required class="form-control" value="<?= $kode_biling?>" placeholder="xxxxxxx">
                </div>
                <a class="tampil btn btn-primary"><i class="fa fa-search"></i> Cari</a>
                <a href="<?php echo base_url('Laporan/tracking');?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-10 col-sm-12 col-xs-12" id="KOSONG"> 
        <div class="x_panel">
        <div class="alert alert-danger" style="font-size: 15px;">
          <i class="fa fa-close kosong"></i>
        </div>
        </div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12" id="TERBAYAR"> 
        <div class="x_panel">
        <div class="alert alert-info" style="font-size: 15px;">
          <i class="fa fa-close terbayar"></i>
        </div>
        <div class="col-md-8">
            <table class="table table-user-information">
              <tbody>
                <tr>
                  <td width="19%"><b>Kode Biling</b></td>
                  <td>: <span id="KODE_BILINGS1"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Tgl Bayar</b></td>
                  <td>: <span id="DATE_BYR"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Tgl Dibuat</b></td>
                  <td>: <span id="TGL_PENETAPAN"></span> </td>
                </tr>  
                <tr>
                  <td width="19%"><b>Nama Wp</b></td>
                  <td>:<span id="NAMA_WP"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Alamat Wp</b></td>
                  <td>:<span id="ALAMAT_WP"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Nama Usaha</b></td>
                  <td>:<span id="NAMA_USAHA"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Alamat Usaha</b></td>
                  <td>:<span id="ALAMAT_OP"></span> </td>
                </tr>                  
              </tbody>
            </table>
          </div>
          <div class="col-md-4">
            <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="40%"><b>Jenis Pajak</b></td>
                            <td>:<span id="NAMA_PAJAK"></span> </td>
                          </tr>
                          <tr>
                            <td width="40%"><b>Masa Pajak</b></td>
                            <td>:<span id="MASA"></span> </td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Tahun Pajak</b></td>
                            <td>:<span id="TAHUN_PAJAK"></span> </td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Pajak Terutang</b></td>
                            <td>:<span id="PAJAK_TERUTANG"></span> </td>
                          </tr>
                                              
                        </tbody>
                      </table>
          </div>
        </div>
      </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="DATA">
          <div class="col-md-6">
            <table class="table table-user-information">
              <tbody>
                <tr>
                  <td width="19%"><b>Kode Biling</b></td>
                  <td>:<span id="KODE_BILINGS"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Nama Wp</b></td>
                  <td>:<span id="NAMA_WP1"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Alamat Wp</b></td>
                  <td>:<span id="ALAMAT_WP1"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Nama Usaha</b></td>
                  <td>:<span id="NAMA_USAHA1"></span> </td>
                </tr>                    
              </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="19%"><b>Masa Pajak</b></td>
                            <td>:<span id="MASA1"></span> </td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Tahun Pajak</b></td>
                            <td>:<span id="TAHUN_PAJAK1"></span> </td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Pajak Terutang</b></td>
                            <td>:<span id="PAJAK_TERUTANG1"></span> </td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Jatuh Tempo</b></td>
                            <td>:<span id="JATUH_TEMPO"></span> </td>
                          </tr>                    
                        </tbody>
                      </table>
          </div>
        </div>
    </div>
  
</div>



</div>

<script type="text/javascript">
  $('#DATA').hide();
  $('#TERBAYAR').hide();
  $('#KOSONG').hide();

function tandaPemisahTitik(b){
var _minus = false;
if (b<0) _minus = true;
b = b.toString();
b=b.replace(".","");
b=b.replace("-","");
c = "";
panjang = b.length;
j = 0;
for (i = panjang; i > 0; i--){
j = j + 1;
if (((j % 3) == 1) && (j != 1)){
c = b.substr(i-1,1) + "." + c;
} else {
c = b.substr(i-1,1) + c;
}
}
if (_minus) c = "-" + c ;
return c;
}

function numbersonly(ini, e){
if (e.keyCode>=49){
if(e.keyCode<=57){
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
ini.value = tandaPemisahTitik(b);
return false;
}
else if(e.keyCode<=105){
if(e.keyCode>=96){
//e.keycode = e.keycode - 47;
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
ini.value = tandaPemisahTitik(b);
//alert(e.keycode);
return false;
}
else {return false;}
}
else {
return false; }
}else if (e.keyCode==48){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==95){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==8 || e.keycode==46){
a = ini.value.replace(".","");
b = a.replace(/[^\d]/g,"");
b = b.substr(0,b.length -1);
if (tandaPemisahTitik(b)!=""){
ini.value = tandaPemisahTitik(b);
} else {
ini.value = "";
}

return false;
} else if (e.keyCode==9){
return true;
} else if (e.keyCode==17){
return true;
} else {
//alert (e.keyCode);
return false;
}

}
var bulan = [        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'];
      $(function(){
            $(document).on('click','.tampil',function(e){
                e.preventDefault();
                // $("#myModal").modal('show');
                $.get("<?= base_url().'Wp/Potongan/inquiry' ?>",
                    {KODE_BILING:$("#KODE_BILING").val() },
                    function(html){
                      var obj = $.parseJSON(JSON.stringify(html));
                      if (obj['STATUS']['ResponseCode']=='00')    {
                        if (obj['STATUS_BAYAR']==1) {
                          $('#TERBAYAR').show();
                          $('#DATA').hide();
                          $("#KODE_BILINGS1" ).html(obj['KODE_BILING']);
                          $("#DATE_BYR" ).html(obj['DATE_BAYAR']);
                          $("#TGL_PENETAPAN" ).html(obj['TGL_PENETAPAN']);
                          $("#NAMA_WP" ).html(obj['NAMA_WP']);
                          $("#ALAMAT_WP" ).html(obj['ALAMAT_WP']);
                          $("#NAMA_USAHA" ).html(obj['OBJEK_PAJAK']);
                          $("#MASA" ).html(bulan[obj['MASA_PAJAK']]);
                          $("#MASA_PAJAK" ).html(obj['MASA_PAJAK']);
                          $("#TAHUN_PAJAK" ).html(obj['TAHUN_PAJAK']);
                          $("#KETERANGAN" ).html(obj['KETERANGAN']);
                          $("#NAMA_PAJAK" ).html(obj['NAMA_PAJAK']);
                          $("#ALAMAT_OP" ).html(obj['ALAMAT_OP']);
                          $("#PAJAK_TERUTANG" ).html(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#JUMLAH_BAYAR" ).html(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $( ".terbayar" ).text(obj['KODE_BILING']+" Sudah Dibayar ");
                        } else {
                          var masa_pajak= obj['MASA_PAJAK'];
                          $('#DATA').show();
                          $('#TERBAYAR').hide();
                          $("#KODE_BILINGS" ).html(obj['KODE_BILING']);
                          $("#NAMA_WP1" ).html(obj['NAMA_WP']);
                          $("#ALAMAT_WP1" ).html(obj['ALAMAT_WP']);
                          $("#NAMA_USAHA1" ).html(obj['OBJEK_PAJAK']);
                          $("#MASA1" ).html(bulan[obj['MASA_PAJAK']]);
                          $("#MASA_PAJAK" ).html(obj['MASA_PAJAK']);
                          $("#TAHUN_PAJAK1" ).html(obj['TAHUN_PAJAK']);
                          $("#KETERANGAN" ).html(obj['KETERANGAN']);
                          $("#JENIS_PAJAK" ).html(obj['JENIS_PAJAK']);
                          $("#JATUH_TEMPO" ).html(obj['JATUH_TEMPO']);
                          $("#PAJAK_TERUTANG1" ).html(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#JUMLAH_BAYAR" ).html(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));

                        }
                        $('#KOSONG').hide();
                      } else {

                        $('#KOSONG').show();
                        $('#TERBAYAR').hide();
                        $('#DATA').hide();
                        $( ".kosong" ).text(obj['KODE_BILING']+" Tidak di temukan" );
                      }
                    }
                );
            });
            $(document).on('click','.kirim',function(e){
                e.preventDefault();
                // $("#myModal").modal('show');
                $.ajax({
                 type: "POST",
                 url: "<?= base_url().'pembayaran/webservice/payment' ?>",
                 data: $('#forms').serialize(),
                 cache: false,
                 success: function(html){
                  // alert(JSON.stringify(html));
                      var obj = $.parseJSON(JSON.stringify(html));
                      if (obj['STATUS']['ResponseCode']=='14')    {                  
                         swal("Gagal",obj['STATUS']['ErrorDesc'],"error");
                      } else{
                        swal("Berhasil",obj['STATUS']['ErrorDesc'],"success")
                        .then((value) => {
                          window.location = "<?php echo base_url().'pembayaran/data'?>";
                        });
                        
                      }
                }
              });    
            });            
        });
</script>