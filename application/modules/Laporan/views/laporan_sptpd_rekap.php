   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Laporan Rekap SPTPD</h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="post" action="<?php echo base_url().'Laporan/laporan_rekap'?>">
               <div class="form-group">
                  <select id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('rekap_bul_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="JENIS_PAJAK"  placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" required>
                            <option value="">Pilih Jenis Pajak</option>
                            <?php foreach($jp as $jns){ ?>
                            <option  value="<?php echo $jns->ID_INC?>"
                              <?php if ($this->session->userdata('rekap_bul_jenis')==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="KECAMATAN" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih upt Kecamatan</option>
                            <?php foreach($upt as $kec){ ?>
                            <option  value="<?php echo $kec->ID_INC?>"
                              <?php if ($this->session->userdata('rekap_bul_upt')==$kec->ID_INC) {echo "selected";} ?>><?php echo $kec->NAMA_UNIT ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
                <?php echo anchor('Excel/Excel/Excel_rekap_bulanan','<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');?>

        </form>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">NAMA</th>
              <th class="text-center">OBJEK</th>
              <th class="text-center">NO REK</th>
              <th class="text-center">JAN</th>
              <th class="text-center">FEB</th>
              <th class="text-center">MAR</th>
              <th class="text-center">APR</th>
              <th class="text-center">MEI</th>
              <th class="text-center">JUN</th>
              <td class="text-center">JUl</td>
              <th class="text-center">AGU</th>
              <th class="text-center">SEP</th>
              <th class="text-center">OKT</th>
              <th class="text-center">NOV</th>
              <th class="text-center">DES</th>
              <th class="text-center">JUMLAH</th>
            
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };
    $.fn.dataTable.ext.errMode = 'throw';
    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        var myvar='<?php echo $session_value;?>';
        $('#mytable_filter input')
        .off('.DT')
        .on('keyup.DT', function(e) {
          if (e.keyCode == 13) {
            api.search(this.value).draw();
          }
        });
      },
      "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,

      'oLanguage':
      {
        "sProcessing":   "Sedang memproses...",
        "sLengthMenu":   "Tampilkan _MENU_ entri",
        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
        "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix":  "",
        "sSearch":       "Cari:",
        "sUrl":          "",
        "oPaginate": {
          "sFirst":    "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext":     "Selanjutnya",
          "sLast":     "Terakhir"
        }
      },
      processing: true,
      serverSide: false,
      pageLength: 20,
      ajax: {"url": "<?php echo base_url()?>Laporan/Laporan/json_laporan_rekap", "type": "POST"},
      columns: [
     /* {
        "data":"ID_INC",
        "orderable": false,
        "className" : "align =center"
      },*/
       {"data":"NPWPD"},
       {"data":"NPWPD",
        "className" : "kiri"},
       {"data":"NAMA_WP"},
       {"data":"OBJEK_PAJAK"},
       {"data":"REK"},
       {
        "data":"JAN",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"FEB",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"MAR",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"APR",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"MEI",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"JUN",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"JUL",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"AGU",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"SEP",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"OKT",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"NOV",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"DES",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {
        "data":"JUMLAH",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      }

      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>

<style type="text/css">
  th {
    text-align: center;
  }
/*  tbody{
    text-align: right;
  }
  thead{
    text-align: center;
  }
  .kiri{
    text-align: left;
  }
  /*td{
    text-align: center;
  }*/
  td, th {
    /*display: table-cell;*/
    vertical-align: center;
}*/
</style>