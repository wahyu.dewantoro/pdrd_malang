    <?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>

    <style>
    th { font-size: 12px; }
    td { font-size: 12px; }
    label { font-size: 12px;}
    textarea { font-size: 12px;}
    .input-group span{ font-size: 12px;}
    input[type='text'] { font-size: 12px; height:30px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>
 
  <div class="pull-right">
    <!-- <?php echo anchor('Sptpd_hotel/sptpd_hotel','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?> -->
<a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>    
  </div>
</div> 

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo site_url('Laporan/aksi_edit_tagihan_bayar'); ?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Detail Tagihan</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="KODE_BILING" value="<?php echo $data_detail->KODE_BILING; ?>" />                  
                    <input type="hidden" name="JENIS_PAJAK" value="<?php echo $data_detail->JENIS_PAJAK; ?>" />                  
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Kode Biling</b></td>
                          <td><b><?= $data_detail->KODE_BILING?></b></td>
                        </tr>
                        <tr>
                          <td><b>NPWPD </b></td>
                          <td><b><?= $data_detail->NPWPD?></b></td>
                        </tr>
                        <tr>
                          <td><b>Nama WP</b></td>
                          <td><b><?= $data_detail->NAMA_WP?></b></td>
                        </tr>
                        <tr>
                          <td><b>Nama Usaha</b></td>
                          <td><b><?= $data_detail->OBJEK_PAJAK?></b></td>
                        </tr>                       
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6">
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Tagihan</b></td>
                          <td><b><?= number_format($data_detail->TAGIHAN,'0','','.')?></b></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>Denda</b></td>
                          <td><b><?= number_format($data_detail->DENDA,'0','','.')?></b></td>
                        </tr>
                        <tr>
                          <td><b>Tgl Bayar </b></td>
                          <td><b><?= $data_detail->DATE_BAYAR?></b></td>
                        </tr>
                        <tr>
                          <td><b>Kategori Pajak</b></td>
                          <td><b><?= $data_detail->JENIS_PAJAK?></b></td>
                        </tr>                       
                      </tbody>
                    </table>                     
                  </div> 
                </div>
              </div>
            </div> 
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Data Yang dirubah</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >                     
                <div class="col-md-6"> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($list_masa_pajak as $list_masa_pajak){ ?>
                            <option <?php if(!empty($data_detail->MASA_PAJAK)){ if($list_masa_pajak==$data_detail->MASA_PAJAK){echo "selected";}} else {if($list_masa_pajak==date("m")){echo "selected";}}?> value="<?php echo $list_masa_pajak?>"><?php echo $namabulan[$list_masa_pajak] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>     
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input style="font-size: 12px" type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($data_detail->TAHUN_PAJAK)){echo $data_detail->TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                
                      </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Usaha <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($tempat_usaha as $tu){ ?>
                            <option <?php if ($tu->ID_INC==$data_detail->TEMPAT_USAHA_ID) { echo "selected";}?>  value="<?php echo $tu->ID_INC?>"><?php echo $tu->NAMA_USAHA ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>                                                              
                 </div>
                 <div class="col-md-6">
                  <div class="form-group">
                      <select onchange="getkel(this);" name="kecamatan"  required class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($row->KODEKEC==$data_detail->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kelurahan"  id="KELURAHAN" required class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($data_detail->KODEKEL==$kel->KODEKELURAHAN AND $data_detail->KODEKEC==$kel->KODEKEC){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                 </div>
                 <?php if($disable==''){?>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Submit</button>
                      <a href="javascript:history.back()" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  <?php } ?> 
                </div>
              </div>
            </div>     
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>

  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>
