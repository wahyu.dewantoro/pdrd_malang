   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Laporan Jatuh Tempo</h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_jatuh_tempo'?>">
                <div class="form-group">
                      <select name="q"  placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" required>
                            <option value="">Pilih Jenis Pajak</option>
                            <?php foreach($jp as $jns){ ?>
                            <option  value="<?php echo $jns->ID_INC?>"
                              <?php if ($q==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/laporan_jatuh_tempo'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_laporan_jatuh_tempo'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>                                 

        </form>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">No Berkas</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Alamat WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Tgl Ketetapan</th>
              <th class="text-center">Jatuh Tempo</th>
              <th class="text-center">Periode</th>
              <th class="text-center">Tahun</th>
              <th class="text-center">Pokok</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Pajak Terutang</th>
              <th class="text-center">Detail</th>
              <th class="text-center">Surat Teguran</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($jatuh_tempo as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->NOMOR_BERKAS?></td>
                <td><?= $rk->KODE_BILING?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td align="center"><?= $rk->MASA_PAJAK?></td>
                <td align="center"><?= $rk->TAHUN_PAJAK?></td>
                <td align="right"><?=  number_format($T=$rk->TAGIHAN,'0','','.')?></td>
                <td align="right"><?=  number_format($D=$rk->DENDA,'0','','.')?></td>
                <td align="right"><?=  number_format($P=$rk->POTONGAN,'0','','.')?></td>
                <td align="right"><?=  number_format($T+$D-$P,'0','','.')?></td>
                <td><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk->TEMPAT_USAHA_ID?>">Cek</a></td>
                <td align="center"><a href="<?php echo base_url('Pdf/Pdf/Pdf_peringatan_belum_bayar/'.$rk->KODE_BILING);?>"><span class='fa fa-print'></span><a/></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="10"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Total Tagihan : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>

<?php foreach ($jatuh_tempo as $rk)  { ?>
  <div class="modal fade" id="myModal<?= $rk->TEMPAT_USAHA_ID?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $ID=$rk->JENIS_PAJAK;
        $npwpd=$rk->NPWPD;$tahun=$rk->TAHUN_PAJAK;$id_usaha=$rk->TEMPAT_USAHA_ID;?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">WAJIB PAJAK <?= strtoupper($rk->NAMA_WP);?>, NAMA USAHA <?= strtoupper($rk->OBJEK_PAJAK);?></h4>
        </div>
        <div class="modal-body">
         <div class="table-responsive">
        <table tyle="width: 100%;" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Masa</th> 
              <th class="text-center">Tahun</th>   
              <th class="text-center">NPWPD</th>  
              <th class="text-center">Nama</th>            
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">Nominal</th>
              <th class="text-center">Tgl Bayar</th>  
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0;$det_rek=$this->db->query("select kode_biling,nama_wp,objek_pajak,MASA_PAJAK,tahun_pajak,TGL_LUNAS,TAGIHAN from V_PENDATAAN_TAGIHAN
                    WHERE JENIS_PAJAK='$ID' and NPWPD='$npwpd' and tahun_pajak='$tahun' and tempat_usaha_id='$id_usaha' order by MASA_PAJAK")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $namabulan[$rk_d->MASA_PAJAK]?></td>
                <td><?= $rk_d->TAHUN_PAJAK?></td>
                <td><?= $rk_d->KODE_BILING?></td>
                <td><?= $rk_d->NAMA_WP?></td>
                <td><?= $rk_d->OBJEK_PAJAK?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk_d->TAGIHAN),'0','','.')?></td>
                <td align="center"><?php if ($rk_d->TGL_LUNAS==''){echo 'Belum Bayar';}else{echo $rk_d->TGL_LUNAS;}?></td>
              </tr>
              <?php  $no++;$tot+=str_replace(",",".",$rk_d->TAGIHAN);}  }else{ ?>
              <tr>
                <th colspan="12"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="6" class="tede" align="right"><b>TOTAL </b></td>
                <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?>
                </tr>
            </tbody>
          </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>

<style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
</style>
