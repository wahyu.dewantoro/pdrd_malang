<?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Detail Realisasi Pajak <?= $pajak->NAMA_PAJAK?> Bulan <?php echo $namabulan[$this->session->userdata('bulan')].' Tahun '.$this->session->userdata('tahun');?>  UPT <?= $upt->NAMA_UNIT?></h3>
</div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <?php if ($jenis=='04') {?>
        <?php echo anchor('Excel/Excel/Excel_detail_realisasi_perupt_reklame/'.$jenis.'/'.$upt,'<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');?>
      <a class="btn btn-sm btn-primary" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i> Kembali</a>
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center">No</th>            
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama Wp</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Kec Pasang</th>
              <th class="text-center">Kel Pasang</th>
              <th class="text-center">Teks</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">No Rek</th>
              <th class="text-center">Tgl bayar</th>
            </tr>
          </thead>
          <tbody>
            <?php $tot=0;$no=1; foreach($detail_reklame as $rk){?>
            <tr>
              <td ><?= $no;?></td>
              <td align="right"><?= $rk->KODE_BILING?></td>
              <td ><?= $rk->NPWPD?></td>
              <td ><?= $rk->NAMA_WP?></td>
              <td ><?= $rk->OBJEK_PAJAK?></td>
              <td ><?= $rk->DESKRIPSI?></td>
              <td ><?= $rk->NAMAKEC?></td>
              <td ><?= $rk->NAMAKELURAHAN?></td>
              <td ><?= $rk->TEKS?></td>
              <!-- <td align="right"><?= number_format($rk->KETETAPAN,'0','','.');?></td> -->
              <td align="right"><?= number_format(0,'0','','.');?></td>
              <td align="right"><?= number_format($rk->KETETAPAN,'0','','.');?></td>
              <td ><?= $rk->NOREK?></td>
              <td ><?= $rk->TGL_TRX?></td>
            </tr>
              <?php $no++;$tot+=$rk->KETETAPAN;}?>
            <tr>
              <td colspan="5" class="tede" align="right"><b>TOTAL </b></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?></b></td>
            </tr>
          </tbody>
        </table>
      </div>
      <?php
      } else {?>
      <?php echo anchor('Excel/Excel/Excel_detail_realisasi_perupt/'.$jenis.'/'.$upt,'<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');?>
      <a class="btn btn-sm btn-primary" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i> Kembali</a>
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center">No</th>            
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama Wp</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Tagihan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Jumlah Bayar</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Tgl bayar</th>
            </tr>
          </thead>
          <tbody>
            <?php $tot=0;$no=1; foreach($detail as $rk){?>
            <tr>

              <td ><?= $no;?></td>
              <td align="right"><?= $rk->KODE_BILING?></td>
              <td ><?= $rk->NPWPD?></td>
              <td ><?= $rk->NAMA_WP?></td>
              <td ><?= $rk->OBJEK_PAJAK?></td>
              <td align="right"><?= number_format($rk->TAGIHAN,'0','','.');?></td>
              <td align="right"><?= number_format($rk->DENDA,'0','','.');?></td>
              <td align="right"><?= number_format($rk->JUMLAH_BAYAR,'0','','.');?></td>
              <td ><?= $rk->JATUH_TEMPO?></td>
              <td ><?= $rk->TGL_BAYAR?></td>
            </tr>
              <?php $no++;$tot+=$rk->JUMLAH_BAYAR;}?>
            <tr>
              <td colspan="5" class="tede" align="right"><b>TOTAL </b></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"></td>
              <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?></b></td>
            </tr>
          </tbody>
        </table>
      </div>
      <?php }?>
    </div>
  </div>
</div>
<style type="text/css">
  .teha{
    font-size:15px;
    text-align: center;
    background-color:#eee;
     border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  .tede {
    font-size:13px;
    background-color:#eee;
    border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  body{
    color: #000;
  }
</style>

