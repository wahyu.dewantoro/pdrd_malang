   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Laporan Realisasi Pajak Hotel</h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="post" action="<?php echo base_url().'Laporan/laporan_realisasi_hotel'?>">
                <div class="form-group">
                  <select id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('real_hotel_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php if ($this->session->userdata('real_hotel_bulan')==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="KECAMATAN" class="form-control select2 col-md-7 col-xs-12" >
                            <?= $option?>
                            <?php foreach($upt as $kec){ ?>
                            <option  value="<?php echo $kec->ID_INC?>"
                              <?php if ($this->session->userdata('real_hotel_upt')==$kec->ID_INC) {echo "selected";} ?>><?php echo $kec->NAMA_UNIT ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php echo anchor('Excel/Excel/Excel_realisasi_hotel','<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');?>

        </form>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>            
              <th class="text-center">Objek Pajak</th>
              <th class="text-center">Nama</th>
              <th class="text-center">Desa</th>
              <th class="text-center">Kecamatan</th>
              <!-- <th class="text-center">No rek</th> -->
              <th class="text-center">Realisasi</th>
              <th class="text-center">Tgl Lunas</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };
    $.fn.dataTable.ext.errMode = 'throw';
    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        var myvar='<?php echo $session_value;?>';
        $('#mytable_filter input')
        .off('.DT')
        .on('keyup.DT', function(e) {
          if (e.keyCode == 13) {
            api.search(this.value).draw();
          }
        });
      },
      "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,


      'oLanguage':
      {
        "sProcessing":   "Sedang memproses...",
        "sLengthMenu":   "Tampilkan _MENU_ entri",
        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
        "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix":  "",
        "sSearch":       "Cari:",
        "sUrl":          "",
        "oPaginate": {
          "sFirst":    "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext":     "Selanjutnya",
          "sLast":     "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      pageLength: 20,
      ajax: {"url": "<?php echo base_url()?>Laporan/Laporan/json_laporan_realisasi_hotel", "type": "POST"},
      columns: [
      {
        "data":"ID_INC",
        "orderable": false,
        "className" : "text-center"
      },
       {"data":"KODE_BILING"},
       {"data":"OBJEK_PAJAK"},
       {"data":"NAMA_WP"},
       {"data":"NAMAKELURAHAN"},
       {"data":"NAMAKEC"},
       /*{"data":"NAMAKEC"},*/
       {
        "data":"TAGIHAN",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {"data":"DATE_BAYAR",
        "className" : "text-center"},

      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>

