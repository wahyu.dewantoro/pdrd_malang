
   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<link href="<?php echo base_url().'gentelella/'?>vendors/main.css" rel="stylesheet">
    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:28px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3>Laporan Realisasi Bulanan Pajak Daerah</h3>
  </div>
<div class="clearfix"></div>                  
<div class="row">
  <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/target_realisasi_bulanan'?>">
                        <div class="form-group">
                          <select  name="tahun" required="required" class="form-control select2 col-md-4 col-xs-4">
                                  <?php $thnskg = date('Y');
                                  for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                                  <option value="<?php echo $i; ?>"
                                  <?php if ($tahun==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                              <select  name="bulan" required="required" class="form-control select2 col-md-7 col-xs-12">
                                    <option value="">Pilih</option>
                                    <?php foreach($mp as $mp){ ?>
                                    <option  value="<?php echo $mp?>"
                                      <?php if ($bulan==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                                    <?php } ?>      
                              </select>
                        </div>
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                        <?php if ($bulan <> '')  { ?>
                                            <a href="<?php echo site_url('Laporan/target_realisasi_bulanan'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                            <a href="<?php echo site_url('Excel/Excel/Excel_realisasi_bulanan'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                                      <?php }   ?>  
                </form> 
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_content" >
                   <div class="col-md-12 col-sm-12 col-xs-12">               
                <div class="x_content">
                    <table  class="" cellspacing="0" width="100%" border>
                              <thead>
                                <tr>
                                  <th class="teha">No</th>
                                  <th class="teha">Pajak</th>
                                  <th class="teha">Bulan <?php echo $namabulan[(int)$bulan];?></th>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php $target=0;$no=1; foreach($bulanan as $rk){?>
                                      <tr>
                                        <td class="tede" align="center"><?php echo $no?></td>
                                        <td class="tede"><?= $rk->NAMA_PAJAK;?></td>
                                        <td class="tede" align="right"><?= number_format($rk->JUMLAH,'0','','.')?></td>
                                      </tr>
                                  <?php $no++; $target+=$rk->JUMLAH;}?>
                                  <tr>
                                    <?php error_reporting(E_ALL^(E_NOTICE|E_WARNING));
                                          $total_persen=$tot/$target*100;?>
                                    <td colspan="2" class="tede" align="right"><b>TOTAL </b></td>
                                    <td class="tede" align="right"><b><?= number_format($target,'0','','.');?>
                                  </tr>
                              </tbody>
                           </table>
                  </div>
                </div>
              
              <!-- /form input mask -->

              <!-- <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <h3><span class="label label-primary">DATA WP</span></h3>
                  <div class="x_content">
                  <div class="col-md-6">
                  </div>
                  </div>
                </div>
              </div> -->
                </div>
              </div>
            </div> 
          </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .teha{
    font-size:18px;
    text-align: center;
    background-color:#eee;
     border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  .tede {
    font-size:19px;
    background-color:#eee;
    border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  body{
    color: #000;
  }
</style>
