   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
<div class="  pull-right">
      </div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_wp_tempat_usaha'?>">
               <div class="form-group">
                    <input type="text" name="tgl1" placeholder="Tanggal 1" required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php echo $tgl1?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2" placeholder="S/d Tanggal 2" required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php echo $tgl2;?>" >
                </div>
                <div class="form-group">
                      <select onchange="getkel(this);" name="sts"  class="form-control select2 col-md-7 col-xs-12" >
                            <option value="">Semua Jenis Pajak</option>
                            <?php foreach($jpjk as $jns){ ?>
                            <option  value="<?php echo $jns->ID_INC?>"
                              <?php if ($sts==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <div class="form-group" id="MY_GOL">
                        <select  style="font-size:" id="GOLONGAN" name="golongan"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Golongan</option>  
                          <?php foreach($gol as $gol){ ?>
                          <option <?php if($gol->ID_OP==$this->session->userdata('h_golongan')){echo "selected";}?> value="<?php echo $gol->ID_OP?>"><?php echo $gol->DESKRIPSI ?></option>
                          <?php } ?>                                                 
                        </select>
                </div> 
                <div class="form-group">
                      <select name="jwp"  class="form-control select2 col-md-7 col-xs-12" >
                            <option value="">Jenis Wajib Pajak</option>
                            <option value="1" <?php if ($jwp=='1') {echo "selected";} ?>>Perseorangan</option>
                            <option value="2" <?php if ($jwp=='2') {echo "selected";} ?>>Badan</option>
                              
                      </select>
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/laporan_wp_tempat_usaha'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_rekap_wp_tempat_usaha'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">NAMA WP</th>
              <th class="text-center">NAMA USAHA</th>
              <th class="text-center">ALAMAT USAHA</th>
              <th class="text-center">KECAMATAN</th>
              <th class="text-center">KELURAHAN</th>
              <th class="text-center">GOLONGAN</th>
              <th class="text-center">JENIS PAJAK</th>
              <th class="text-center">HP</th>
              <th class="text-center">TGL DAFTAR</th>
              
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($pengajuan as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->NPWP?></td>
                <td><?= $rk->NAMA?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td><?= $rk->NAMAKELURAHAN?></td>
                <td><?= $rk->DESKRIPSI?></td>
                <td><?= $rk->NAMA_PAJAK?></td>
                <td><?= $rk->NO_TELP?></td>                
                <td><?= $rk->TGL_DAFTAR?></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          </button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      /*var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });  */
      var ID_INC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getgol'?>",
       data: { ID_INC: ID_INC},
       cache: false,
       success: function(msga){
           // alert(msga);
            $("#GOLONGAN").html(msga);
          }
        });
    }  
    
  </script>

