
<?php function tglIndonesia($str){
       $tr   = trim($str);
       $str    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);
       return $str;
       //call funtion
        tglIndonesia(date('D, d F, Y'));
   }?>
<div class="page-title">
 <div class="title_left">
  <h3>LAPORAN REALISASI APBD PENDAPATAN HARIAN : <?php echo tglIndonesia(date('D, d F, Y'));?> </h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <?php echo anchor('Excel/Excel/Excel_laporan_ralisasi_target_per_rekening','<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');?>
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="teha">Kode Rekening</th>            
              <th class="teha">Pajak</th>
              <th class="teha">Uraian</th>
              <th class="teha">Anggaran</th>
              <th class="teha">S.d Kemaren</th>
              <th class="teha">Hari ini</th>
              <th class="teha">S.d Hari Ini</th>
              <th class="teha">(%)</th>
            </tr>
          </thead>
          <tbody>
            <?php $tot=0;$target=0;$sd_ll=0;$hr_ini=0;$no=1; foreach($lp_realisasi_per_rekening as $rk){?>
            <tr>
              <td class="tede"><?= '4 1 1 '.$rk->JENIS_PAJAK.' '.$rk->REKENING;?></td>
              <td class="tede"><?= $rk->NAMA_PAJAK;?></td>
              <td class="tede"><?= $rk->KETERANGAN;?></td>
              <td class="tede" align="right"><?= number_format($rk->TARGET,'0','','.')?></td>
              <td class="tede" align="right"><?= number_format($rk->JUMLAH_SD_KEMAREN,'0','','.');?></td>
              <td class="tede" align="right"><?= number_format($rk->JUMLAH_HARI_INI,'0','','.');?></td>
              <td class="tede" align="right"><?= number_format($rk->JUMLAH_SD_HARI_INI,'0','','.');?></td>
              <td class="tede" align="right"><?= number_format((float)$rk->PERSEN,'2','.',',');?> %</td>
            </tr>
              <?php $no++; 
              /*$tot+=$rk->JUMLAH_SD_HARI_INI;
              $target+=$rk->TARGET;
              $sd_ll+=$rk->JUMLAH_SD_LALU;
              $hr_ini+=$rk->JUMLAH_HARI_INI;*/
              }?>
        <!--<tr>
                <?php $total_persen=$tot/$target*100;?>
                <td colspan="2" class="tede" align="right"><b>TOTAL </b></td>
                <td class="tede" align="right"><b><?= number_format($target,'0','','.');?>
                <td class="tede" align="right"><b><?= number_format($sd_ll,'0','','.');?>
                <td class="tede" align="right"><b><?= number_format($hr_ini,'0','','.');?>
                <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?></b></td>
                <td class="tede" align="right"><b><?= number_format((float)$total_persen,'2','.',',');?></b> %</td>
            </tr> -->
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .teha{
    font-size:15px;
    text-align: center;
    background-color:#eee;
     border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  .tede {
    font-size:13px;
    background-color:#eee;
    border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  body{
    color: #000;
  }
</style>

