   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
<div class="  pull-right">
      </div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
 
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <h4> Detail data rekon Tanggal <?= $tgl?> <?php echo anchor(site_url('Laporan/rekon'), '<i class="fa fa-back"></i> Kembali', 'class="btn btn-sm btn-warning"'); ?></h4>
        <!-- <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/Laporan/rekon'?>">
               <div class="form-group">
                    <input type="text" name="tgl1" placeholder="Tanggal 1" required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php echo $tgl1?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2" placeholder="S/d Tanggal 2" required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php echo $tgl2;?>" >
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php echo anchor(site_url('Laporan/Laporan/import'), '<i class="fa fa-cloud-upload"></i> Import Data', 'class="btn btn-primary btn-sm"');?>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/Laporan/rekon'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                                    <?php echo anchor(site_url('Laporan/Laporan/import'), '<i class="fa fa-cloud-upload"></i> Import Data', 'class="btn btn-primary btn-sm"');?>
                              <?php }   ?>  
        </form> -->
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th rowspan="2" class="text-center" width="3%">No</th>
              <th colspan='3' class="text-center">BANK JATIM</th>
              <th colspan='3' class="text-center">PANJI</th>
              
            </tr>
            <tr>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Rp</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Rp</th>
            </tr>
          </thead>
            <tbody>
              <?php foreach ($rekon as $rk)  { ?>
              <tr>
                <td align="center"><?php echo ++$start ?></td>
                <td align="center"><?= $rk->KODE_BILING?></td>
                <td align=""><?= $rk->NAMA?></td>
                <td <?php if ($rk->JUMLAH!=$rk->JUMLAH_BAYAR) {echo "bgcolor='#ec4949'"; }?> align="right"><?= number_format($rk->JUMLAH,'0','','.')?></td>
                <td align="center"><?= $rk->KODE_BILING_PANJI?></td>
                <td align=""><?= $rk->NAMA_WP?></td>
                <td align="right"><?= number_format($rk->JUMLAH_BAYAR,'0','','.')?></td>
              </tr>
              <?php  } ?>
             <!--  <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
               -->
            </tbody>
          </table>
          </div>
          <!-- <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          </button> -->
          <div class="float-right">
            
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

