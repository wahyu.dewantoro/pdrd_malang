   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_potongan'?>">
                <div class="form-group">
                    <input type="text" name="tgl1"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($tgl1==null) {echo $def1;} else {echo $tgl1;}?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($tgl2==null) {echo $def2;} else {echo $tgl2;}?>" >
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="jenis_pajak"  placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" >
                            <option value="">Semua Jenis Pajak</option>
                            <?php foreach($jp as $rk){ ?>
                            <option  value="<?php echo $rk->ID_INC?>"
                              <?php if ($jenis_pajak==$rk->ID_INC) {echo "selected";} ?>><?php echo $rk->NAMA_PAJAK   ?></option>
                            <?php } ?>  
                      </select>
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/laporan_potongan'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_laporan_potongan'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tanggal</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Nomor SK</th>
              <th class="text-center">Tgl SK</th>
              <th class="text-center">Pajak Terutang</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Tagihan</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($potongan as $rk_d)  { ?>
              <tr>
                <td><?= ++$start?></td>
                <td><?= $rk_d->DATE_POTONGAN?></td>
                <td><?= $rk_d->KODE_BILING?></td>
                <td><?= $rk_d->NPWPD?></td>
                <td><?= $rk_d->NAMA_WP?></td>
                <td><?= $rk_d->NAMA_USAHA?></td>
                <td><?= $rk_d->ALAMAT_OP?></td>
                <td><?= $rk_d->NAMA_PAJAK?></td>
                <td><?= $rk_d->NO_SK_POTONGAN?></td>
                <td><?= $rk_d->TGL_SK_POTONGAN?></td>
                <td align="right"><?=  number_format($rk_d->KETETAPAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk_d->DENDA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk_d->POTONGAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk_d->PAJAK_TERUTANG,'0','','.')?></td>
                <td><?php if ($rk_d->STATUS=='1') {
                  echo "Terbayar"; } else {echo "Belum Bayar";}
                ?></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="11"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>

