
    <?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<div class="page-title">
 <div class="title_left">
  <h3>LAPORAN REALISASI BAPENDA <?php $namabulan[date('m')].' '.date('Y');?> </h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_realisasi_bapenda'?>">
                <div class="form-group">
                  <select  name="tahun" required class="form-control select2 col-md-4 col-xs-4">
                    <?php $thnskg = date('Y');
                    for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                    <option value="<?php echo $i; ?>"
                    <?php if ($this->session->userdata('tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                  </select>
                </div>
                <div class="form-group">
                    <select  name="bulan" required class="form-control select2 col-md-7 col-xs-12">
                      <option value="">Pilih</option>
                      <?php foreach($mp as $mp){ ?>
                        <option  value="<?php echo $bl=sprintf('%02d', $mp)?>"
                      <?php if ($this->session->userdata('bulan')==$bl) {echo "selected";} ?>><?php echo $namabulan[$bl] ?></option>
                      <?php } ?>      
                    </select>
                </div>
                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cari</button>
                <?php echo anchor('Excel/Excel/Excel_rekap_bulanan','<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');?>
        </form>
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="teha">No</th>            
              <th class="teha">Jenis Pendapatan</th>
              <th class="teha">Target</th>
              <th class="teha">S.d Bulan Lalu</th>
              <th class="teha">Bulan <?= $namabulan[$this->session->userdata('bulan')]?></th>
              <th class="teha">S.d Bulan Ini</th>
              <th class="teha">(%)</th>
              <th class="teha">Keterangan</th>
              <th class="teha">Detail</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($lp_realisasi_per_upt!=null) {
               $tot=0;$target=0;$sd_ll=0;$hr_ini=0;$min=0;$no=1; foreach($lp_realisasi_per_upt as $rk){?>
                <tr>
                    <td class="tede"><?= $no;?></td>
                    <td class="tede"><?php if ($rk->NAMA_PAJAK=='PPJ') {echo "PPJ NON PLN";} else {echo $rk->NAMA_PAJAK;}?></td>
                    <td class="tede" align="right"><?= number_format($rk->TARGET,'0','','.')?></td>
                    <td class="tede" align="right"><?= number_format($rk->SD_BULAN_LALU,'0','','.');?></td>
                    <td class="tede" align="right"><?= number_format($rk->JUMLAH_BULAN_INI,'0','','.');?></td>
                    <td class="tede" align="right"><?= number_format($rk->SD_BULAN_INI,'0','','.');?></td>
                    <td class="tede" align="right"><?= number_format((float)$rk->PERSEN,'2','.',',');?> %</td>
                    <td class="tede" align="right"><?= number_format($k=$rk->SD_BULAN_INI-$rk->TARGET,'0','','.');?></td>
                    <td class="tede" align="center"><a href="<?php echo base_url('Laporan/detail_realisasi_perupt/'.$rk->JENIS_PAJAK.'/'.$rk->UPT.'/'.$this->session->userdata('bulan').'/'.$this->session->userdata('tahun'));?>"><span class="fa fa-search"></span></a></td>
                  </tr>
                    <?php $no++; 
                    $tot+=$rk->SD_BULAN_INI;
                    $target+=$rk->TARGET;
                    $sd_ll+=$rk->SD_BULAN_LALU;
                    $hr_ini+=$rk->JUMLAH_BULAN_INI;
                    $min+=$k;
                    }?>
              <tr>
                      <?php $total_persen=$tot/$target*100;?>
                      <td colspan="2" class="tede" align="right"><b>TOTAL </b></td>
                      <td class="tede" align="right"><b><?= number_format($target,'0','','.');?>
                      <td class="tede" align="right"><b><?= number_format($sd_ll,'0','','.');?>
                      <td class="tede" align="right"><b><?= number_format($hr_ini,'0','','.');?>
                      <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?></b></td>
                      <td class="tede" align="right"><b><?= number_format((float)$total_persen,'2','.',',');?></b> %</td>
                      <td class="tede" align="right"><b><?= number_format($min,'0','','.');?></b></td>
                      <td class="tede" align="right"><b></b></td>
                  </tr> 

               <?php
            } else {
                
            }?>
           
           
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .teha{
    font-size:15px;
    text-align: center;
    background-color:#eee;
     border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  .tede {
    font-size:13px;
    background-color:#eee;
    border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  body{
    color: #000;
  }
</style>

