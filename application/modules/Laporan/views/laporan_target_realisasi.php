
<link href="<?php echo base_url().'gentelella/'?>vendors/main.css" rel="stylesheet">
    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:28px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3>Laporan Realisasi Per <?= date('d-m-Y h:i')?></h3>
  </div>
<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_content" >
                   <div class="col-md-12 col-sm-12 col-xs-12">                
                <div class="x_content">
                <?php echo anchor('Excel/Excel/Excel_laporan_ralisasi_target_bulanan','<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');?>
                    <table  class="" cellspacing="0" width="100%" border>
                              <thead>
                                <tr>
                                  <th class="teha">NO</th>
                                  <th class="teha">PAJAK</th>
                                  <th class="teha">TARGET</th>
                                  <th class="teha">S/D BULAN LALU</th>
                                  <!-- <th class="teha">BULAN LALU</th> -->
                                  <th class="teha">BULAN INI</th>
                                  <th class="teha">S/D BULAN INI</th>
                                  <th class="teha"> %</th>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php $tot=0;$target=0;$sd_bl=0;$jml_bi=0;$no=1; foreach($bulan as $rk){?>
                                      <tr>
                                        <td class="tede" align="center"><?php echo $no?></td>
                                        <td class="tede"><?= $rk->NAMA_PAJAK;?></td>
                                        <td class="tede" align="right"><?= number_format($rk->TARGET,'0','','.')?></td>
                                        <td class="tede" align="right"><?= number_format($rk->JUMLAH_SD_BULAN_LALU,'0','','.');?></td>
                                        <!-- <td class="tede" align="right"><?= number_format($rk->JUMLAH_BULAN_LALU,'0','','.');?></td> -->
                                        <td class="tede" align="right"><?= number_format($rk->JUMLAH_BULAN_INI,'0','','.');?></td>
                                        <td class="tede" align="right"><?= number_format($rk->JUMLAH_SD_BULAN_INI,'0','','.');?></td>
                                        <td class="tede" align="right"><?= number_format((float)$rk->PERSEN,'2','.',',');?> %</td>
                                      </tr>
                                  <?php $no++; $tot+=$rk->JUMLAH_SD_BULAN_INI;$target+=$rk->TARGET;$sd_bl+=$rk->JUMLAH_SD_BULAN_LALU;$jml_bi+=$rk->JUMLAH_BULAN_INI;}?>
                                  <tr>
                                    <?php error_reporting(E_ALL^(E_NOTICE|E_WARNING));
                                          $total_persen=$tot/$target*100;?>
                                    <td colspan="2" class="tede" align="right"><b>TOTAL </b></td>
                                    <td class="tede" align="right"><b><?= number_format($target,'0','','.');?>
                                    <td class="tede" align="right"><b><?= number_format($sd_bl,'0','','.');?>
                                     <td class="tede" align="right"><b><?= number_format($jml_bi,'0','','.');?>
                                    <td class="tede" align="right"><b><?= number_format($tot,'0','','.');?></b></td>
                                     <td class="tede" align="right"><b><?= number_format((float)$total_persen,'2','.',',');?></b> %</td>
                                  </tr>
                              </tbody>
                           </table>
                  </div>
                </div>
              
              <!-- /form input mask -->

              <!-- <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <h3><span class="label label-primary">DATA WP</span></h3>
                  <div class="x_content">
                  <div class="col-md-6">
                  </div>
                  </div>
                </div>
              </div> -->
                </div>
              </div>
            </div> 
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .teha{
    font-size:18px;
    text-align: center;
    background-color:#eee;
     border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  .tede {
    font-size:19px;
    background-color:#eee;
    border-bottom: 4px solid #fff;
    border-top: 4px solid #fff;
    border-left: 4px solid #fff;
    border-right: 4px solid #fff;
    padding: 10px;
  }
  body{
    color: #000;
  }
</style>

<script type="text/javascript">
  
  setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 79000);
</script>
