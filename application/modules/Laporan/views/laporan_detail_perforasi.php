   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan/laporan_detail_perforasi'?>">
                <div class="form-group">
                    <input type="text" name="tgl1"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($tgl1==null) {echo $def1;} else {echo $tgl1;}?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($tgl2==null) {echo $def2;} else {echo $tgl2;}?>" >
                </div>
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('npwpd'); ?>" placeholder="NAMA WP / USAHA/ NPWPD">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan/laporan_detail_perforasi'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_laporan_detail_perforasi'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table width="100%" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>TGL</th>
              <th>No BAP</th>
              <th>Jenis Karcis</th>
              <th>Nama Pemohon</th>
              <th>Tempat Usaha</th>
              <th>Kode</th>
              <th>No urut</th>
              <th>Jml Blok</th>
              <th>Jml lmbr/Blok</th>
              <th>Harga /lembar</th>
              <th>Total</th>
              <th>% Pajak</th>
              <!-- <th>Jenis Karcis dan No urut</th>
              <th>No Seri</th>
              <th>Nominal</th>
              <th>Jml Blok</th>
              <th>Isi/Blok</th>
              <th>jml Lembar</th>
              <th>jml Pendapatan</th> -->
              <th>Nilai Pajak</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($perforasi as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->TGL_PERFORASI?></td>
                <td align="center"><?= $rk->NOMOR_INC_BLN.' / '.sprintf('%03d', $rk->NOMOR_INC_THN)?></td>
                <td><?php if ($rk->JENIS_KARCIS=='1') {$JNS='Karcis Masuk';} else {$JNS='Karcis Parkir';} ;
                   echo  '('.$JNS.') '.$rk->CATATAN ?></td>
                 <td><?= $rk->NAMA_WP_PERF?></td>
                 <td><?= $rk->NAMA_USAHA?></td>
                 <td><?= $rk->NOMOR_SERI?></td>
                 <td><?= $rk->NO_URUT_KARCIS?></td>
                 <td align="right"><?=  number_format($rk->JML_BLOK,'0','','.')?></td>
                 <td align="right"><?=  number_format($rk->ISI_LEMBAR,'0','','.')?></td>
                 <td align="right"><?=  number_format($rk->NILAI_LEMBAR,'0','','.')?></td>
                 <td align="right"><?=  number_format($rk->TOT_BEFORE_TAX,'0','','.')?></td>
                 <td align="right"><?=  number_format($rk->PERSEN,'0','','.')?> %</td>
                 <td align="right"><?=  number_format($rk->PAJAK,'0','','.')?></td>
                <!-- <td><?= $rk->NOMOR_SERI?></td>
                <td align="right"><?=  number_format($rk->NILAI_LEMBAR,'0','','.')?></td>
                
                
                <td align="right"><?=  number_format($qtt=$rk->JML_BLOK*$rk->ISI_LEMBAR,'0','','.')?></td>
                <td align="right"><?=  number_format($qtt*$rk->NILAI_LEMBAR,'0','','.')?></td> -->
                <!-- <td><a href="<?php echo base_url('Pdf/Pdf/bap_perforasi/'.$rk->ID_INC);?>">[BAP]</a>
                    <a href="<?php echo base_url('Pdf/Pdf/permohonan_perforasi/'.$rk->ID_INC);?>">[PERMOHONAN]</a></td> -->
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Nominal Perforasi : <?= number_format($total_tagihan->JUMLAH,'0','','.');?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>

