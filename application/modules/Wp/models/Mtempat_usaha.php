<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mtempat_usaha extends CI_Model
{

    public $table = 'TEMPAT_USAHA';
    public $id = 'ID_INC';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        $this->npwpd=$this->session->userdata('NIP');
        $this->role =$this->session->userdata('MS_ROLE_ID');
        $this->upt=$this->session->userdata('UNIT_UPT_ID');
    }

    // datatables
    function json() {
        if ($this->role==8 OR $this->role==81) {
          // $wh="NPWPD='$this->npwpd' AND (JENIS_PAJAK!='08' AND JENIS_PAJAK!='04')";
           $wh="NPWPD='$this->npwpd'";
           $this->datatables->where($wh);
        } else {
            $wh='';
        }
        /*if ($this->role==10) {
           $wh="(JENIS_PAJAK!='08' AND JENIS_PAJAK!='04') AND KODE_UPT='$this->upt'";
           $this->datatables->where($wh);
        } else {
            $wh='';
        }*/
        
        $this->datatables->select("A.ID_INC, NAMA_USAHA, ALAMAT_USAHA, NAMA_PAJAK, DESKRIPSI, NAMAKELURAHAN||' / '||NAMAKEC AS KEC,NPWPD,NAMA");
        $this->datatables->from('TEMPAT_USAHA A');
        $this->datatables->join('JENIS_PAJAK B', 'A.JENIS_PAJAK = B.ID_INC');
        $this->datatables->join('OBJEK_PAJAK C', 'A.ID_OP = C.ID_OP','LEFT');
        $this->datatables->join('KECAMATAN D', 'A.KODEKEC = D.KODEKEC');
        $this->datatables->join('WAJIB_PAJAK F', 'A.NPWPD = F.NPWP');
        $this->datatables->join('KELURAHAN E', 'A.KODEKEL = E.KODEKELURAHAN AND A.KODEKEC=E.KODEKEC');
        if ($this->session->userdata('INC_USER')=='311') {
            $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Wp/tempat_usaha/detail/$1'),'<i class="fa fa-search"></i>','class="btn btn-xs btn-info"').anchor(site_url('Wp/tempat_usaha/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').'</div>', 'acak(ID_INC)');
        } else {
            $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Wp/tempat_usaha/detail/$1'),'<i class="fa fa-search"></i>','class="btn btn-xs btn-info"')./*anchor(site_url('Wp/tempat_usaha/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').*//*.anchor(site_url('pembelian/sptpd_hotel/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="delete" data-id="$1" href="javascript:void(0)"').*/'</div>', 'acak(ID_INC)');
        }
        return $this->datatables->generate();
    }
 
    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
  

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function ListTuByNip(){
        return $this->db->query("SELECT A.ID_INC, NAMA_USAHA, NAMA_PAJAK, JENIS_PAJAK,NAMAKEC,NAMAKELURAHAN
FROM TEMPAT_USAHA A
JOIN JENIS_PAJAK B ON A.JENIS_PAJAK = B.ID_INC
LEFT JOIN OBJEK_PAJAK C ON A.ID_OP = C.ID_OP
join KECAMATAN d on A.KODEKEC = d.KODEKEC
join KELURAHAN e on A.KODEKEL = e.KODEKELURAHAN AND A.KODEKEC=e.KODEKEC
WHERE NPWPD = '$this->npwpd'")->result();
    }
    function ListJenisHotel(){
        return $this->db->query("SELECT ID_INC, HOTEL FROM JENIS_HOTEL")->result();
    }
    function ListJenisPajak(){
         if ($this->role==8) {
           $wh="";//" WHERE ID_INC!='08' AND ID_INC!='04'";
           $this->datatables->where($wh);
        } else {
            $wh='';
        }
        return $this->db->query("SELECT * FROM JENIS_PAJAK ".$wh." ")->result();
    }
    function ListRekeningPajak(){
         if ($this->role==8) {
           $wh=" WHERE JENIS_PAJAK!='08' AND JENIS_PAJAK!='04'";
           //$this->datatables->where($wh);
        } else {
            $wh='';
        }
        return $this->db->query("SELECT * FROM MS_REKENING ".$wh." ")->result();
    }
    
     function getKel($id){
        return $this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$id'")->result();
     }
     function getGol(){
        return $this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI FROM OBJEK_PAJAK WHERE ID_GOL=172 OR ID_GOL=174 OR ID_GOL=175 OR ID_GOL=176 OR ID_GOL=493 OR ID_GOL=494")->result();
     }
     function getGolId($id){
        return $this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI FROM OBJEK_PAJAK WHERE ID_OP='$id'")->result();
     }
     function get_rekID($id,$jp){
        return $this->db->query("SELECT * FROM MS_REKENING WHERE KODE_REKENING_PAJAK='$id' AND JENIS_PAJAK='$jp'")->result();
     }

}

/* End of file Mmenu.php */
/* Location: ./application/models/Mmenu.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-08 05:30:09 */
/* http://harviacode.com */