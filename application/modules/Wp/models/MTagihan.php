<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mtagihan extends CI_Model
{


    function __construct()
    {
        parent::__construct();
        $this->npwpd=$this->session->userdata('NIP');
    }
     function getData($tahun,$kodeusaha){
    /*    if ($jenisPajak==1) {
             $jns='01';
        } 
        elseif ($jenisPajak==2) {
             $jns='02';
        }
        elseif ($jenisPajak==3) {
            $jns='03';
        }    
        elseif ($jenisPajak==5) {
             $jns='05';
        }   
        elseif ($jenisPajak==9) {
             $jns='09';
        }  
        elseif ($jenisPajak==6) {
            $jns='06';
        }  
        elseif ($jenisPajak==7) {
            $jns='07';
        }else{
            $jns='';
        }*/
        return $this->db->query("select b.status,a.masa_pajak, KODE_BILING,tagihan+denda-potongan PAJAK_TERUTANG,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy')  TANGGAL_PENERIMAAN,tagihan+denda-potongan JUMLAH_BAYAR,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') TGL_BAYAR
                                from masa_pajak a
                                left join (select * from  tagihan WHERE NPWPD='$this->npwpd' AND tempat_usaha_id='$kodeusaha' AND TAHUN_PAJAK='$tahun' ) b on a.masa_pajak=b.masa_pajak
                                order by a.masa_pajak asc")->result();                           
     }
 function getDataPenagihan($npwpd,$jenisPajak,$namaUsaha,$tahun,$kodeusaha){
        if ($jenisPajak==1) {
             $jns='01';
        } 
        elseif ($jenisPajak==2) {
             $jns='02';
        }
        elseif ($jenisPajak==3) {
            $jns='03';
        }    
        elseif ($jenisPajak==5) {
             $jns='05';
        }   
        elseif ($jenisPajak==9) {
             $jns='09';
        }  
        elseif ($jenisPajak==6) {
            $jns='06';
        }  
        elseif ($jenisPajak==7) {
            $jns='07';
        }elseif ($jenisPajak==8) {
            $jns='08';
        }   
        elseif ($jenisPajak==4) {
            $jns='04';
        }else{
            $jns='';
        }
        return $this->db->query("select b.status,a.masa_pajak, KODE_BILING,tagihan+denda-potongan PAJAK_TERUTANG,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy')  TANGGAL_PENERIMAAN,tagihan+denda-potongan JUMLAH_BAYAR,TO_CHAR(DATE_BAYAR, 'dd-mm-yyyy') TGL_BAYAR
                                from masa_pajak a
                                left join (select * from  tagihan WHERE NPWPD='$npwpd' AND tempat_usaha_id='$kodeusaha' AND TAHUN_PAJAK='$tahun' and jenis_pajak='$jns') b on a.masa_pajak=b.masa_pajak
                                order by a.masa_pajak asc")->result();                          
     }     
    function ListJenisPajak(){
        return $this->db->query("SELECT * FROM JENIS_PAJAK ")->result();
    }
    function dataWp($npwpd){
        return $this->db->query("SELECT NPWP, NAMA, ALAMAT FROM WAJIB_PAJAK where NPWP='$npwpd' ")->result();
    }    
}

/* End of file Mmenu.php */
/* Location: ./application/models/Mmenu.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-08 05:30:09 */
/* http://harviacode.com */