
                        <h3>Transaksi Tanggal : <?php echo $tanggal; ?>
                        <br>Nama WP           : <?php echo $_SESSION['NAMA']; ?>
                        </h3>
                        <table class="table table-striped table-bordered table-hover" border="1" id="" >
                            <thead>
                                <tr>
                                      <th  class="text-center" width="5%">No</th>
                                      <th  class="text-center" width="10%">NPWPD</th>
                                      <th  class="text-center" width="15%">Nama Objek Pajak</th>
                                      <th  class="text-center">Department</th>
                                      <th  class="text-center" width="15%">Date Trx</th>
                                      <th  class="text-center" width="17%">Jumlah</th>
                                </tr>
                            </thead>
                           <tbody>
                                <?php
                                $start = 0;
                                $tot = 0;
                                foreach ($transaksi_data as $transaksi)
                                {
                                    ?>
                                    <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td align='center'><?php echo "'".$transaksi->npwpd ?></td>
                                <td><?php echo $transaksi->nama_wp ?></td>
                                <td><?php echo $transaksi->keterangan ?></td>
                                <td align='center'><?php echo $transaksi->tgl_transaksi ?></td>
                                <td align="right"><?php echo 'Rp. '.number_format($transaksi->jumlah,'0','','.') ?></td>
                                </tr>
                                    <?php
                                    $tot+=$transaksi->jumlah;
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr style="height: 28px;">
                                  <td align='center' colspan="5"><h4><b>TOTAL</b></h4></td>
                                  <td align="right"><h5><b><?php echo 'Rp. '.number_format($tot,'0','','.') ?></b></h5></td>
                                </tr>
                            </tfoot>
                        </table>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=cetaktransaksiharian.xls");
 
     ?>