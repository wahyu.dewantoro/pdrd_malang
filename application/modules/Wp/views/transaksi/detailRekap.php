
      <div class="x_content">
      <table width="100%" id="examplex" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th  class="text-center" width="5%">No</th>
                          <th  class="text-center" width="10%">NPWPD</th>
                          <th  class="text-center" width="15%">Nama Objek Pajak</th>
                          <th  class="text-center" width="10%"">Department</th>
                          <th  class="text-center" width="15%">Date Trx</th>
                          <th  class="text-center" width="40%">Jumlah</th>
                        </tr>
                      </thead>
                      <tbody>
                                <?php
                                $start = 0;
                                $tot = 0;
                                foreach ($res as $transaksi)
                                {
                                    ?>
                                    <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td align='center'><?php echo $transaksi->npwpd ?></td>
                                <td><?php echo $transaksi->nama_wp ?></td>
                                <td width="10%"><?php echo $transaksi->keterangan ?></td>
                                <td align='center'><?php echo $transaksi->tgl_transaksi ?></td>
                                <td width="20%" align="right"><?php echo 'Rp. '.number_format($transaksi->jumlah,'0','','.') ?></td>
                                </tr>
                                    <?php
                                    $tot+=$transaksi->jumlah;
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr style="height: 28px;">
                                  <td align='center' colspan="5"><h4><b>TOTAL</b></h4></td>
                                  <td align="right"><h5><b><?php echo 'Rp. '.number_format($tot,'0','','.') ?></b></h5></td>
                                </tr>
                            </tfoot>
                    </table>
              </div>
      <script type="text/javascript">
    $(document).ready(function() {
    $('#examplex').DataTable();
} );
</script>