<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title"> 
        <h2>Data Transaksi Harian</h2>
        <div class="pull-right">
          
        </div>                  
        <div class="clearfix"></div>
      </div>
        <form class="form-inline" method="post" action="<?php echo base_url().'Wp/Transaksi'?>">
                <div class="form-group">
                  <!-- <select id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('hotel_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select> -->
                </div>
                <div class="form-group">
                    <input type="text" name="tanggal" placeholder="Tanggal" required="required" class="form-control col-md-6 col-xs-6 tanggal" value="<?php echo $tanggal ?>">

                    <!-- <input style="width: 120px;" type="text" class="form-control col-md-7 col-xs-12 tanggal" name="tanggal" placeholder="Tanggal" value="<?php echo $tanggal ?>"> -->
                </div>
                <button style="height: 28px;" type="submit" class="form-control btn btn-primary btn-xs"><i class="fa fa-search"></i> Cari</button>

                <?php if($jum_row!=0){ $arr = explode('/',$tanggal); $filltanggal = $arr[2].'-'.$arr[1].'-'.$arr[0];?> <a href="<?php echo base_url('Wp/Transaksi/cetakharini/').$filltanggal?>"> <button style="height: 28px;" type="button" class="form-control btn btn-success btn-xs"><i class="fa fa-file-excel-o"></i> Cetak</button> </a> <?php } ?>
        </form>
      <div class="x_content">
      <table id="example" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th  class="text-center" width="5%">No</th>
                          <th  class="text-center" width="10%">NPWPD</th>
                          <th  class="text-center" width="15%">Nama Objek Pajak</th>
                          <th  class="text-center">Department</th>
                          <th  class="text-center" width="15%">Date Trx</th>
                          <th  class="text-center" width="17%">Jumlah</th>
                        </tr>
                      </thead>
                      <tbody>
                                <?php
                                $start = 0;
                                $tot = 0;
                                foreach ($transaksi_data as $transaksi)
                                {
                                    ?>
                                    <tr>
                                <td align='center'><?php echo ++$start ?></td>
                                <td align='center'><?php echo $transaksi->npwpd ?></td>
                                <td><?php echo $transaksi->nama_wp ?></td>
                                <td><?php echo $transaksi->keterangan ?></td>
                                <td align='center'><?php echo $transaksi->tgl_transaksi ?></td>
                                <td align="right"><?php echo 'Rp. '.number_format($transaksi->jumlah,'0','','.') ?></td>
                                </tr>
                                    <?php
                                    $tot+=$transaksi->jumlah;
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr style="height: 28px;">
                                  <td align='center' colspan="5"><h4><b>TOTAL</b></h4></td>
                                  <td align="right"><h5><b><?php echo 'Rp. '.number_format($tot,'0','','.') ?></b></h5></td>
                                </tr>
                            </tfoot>
                    </table>
                    
      </div>
    </div>
  </div>



</div> 
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
