
                        <h3>Rekap Bulanan <br>
                            Bulan   : <?php echo $tanggal?> <br>
                            Nama WP : <?php echo $nama_wp;  ?> 
                        </h3>
                        <table class="table table-striped table-bordered table-hover" border="1" id="" >
                            <thead>
                        <tr style="height: 28px;">
                          <th width="5%" align="center">No</th>
                            <th align="center">Tanggal</th>
                            <th align="center">Omset</th>
                          <tr>
                      </thead>
                      <tbody>
                      <?php
                        $tot = 0;
                        $start = 0;
                        foreach ($rekap as $r)
                        {
                      ?>
                        <tr>
                          <td align='center'><?php echo ++$start ?></td>
                          <td align="center"><?php echo $r->tgl.'-'.$bulan.'-'.$tahun ?></td>
                          <td align="right"><?php echo 'Rp. '.number_format($r->omset,'0','','.') ?></td>
                        </tr>
                      <?php
                        $tot+=$r->omset;
                        }
                      ?>
                      </tbody>
                      <tfoot>
                        <tr style="height: 28px;">
                          <td align='center' colspan="2"><h4><b>TOTAL</b></h4></td>
                          <td align="right"><b><?php echo 'Rp. '.number_format($tot,'0','','.') ?></b></td>
                        </tr>
                      </tfoot>
                        </table>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=RekapBulanan.xls");
 
     ?>