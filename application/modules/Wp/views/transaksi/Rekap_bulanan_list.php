
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title"> 
        <h2>Rekap Bulanan</h2>
        <div class="pull-right">
          
        </div>                  
        <div class="clearfix"></div>
      </div>
        <form class="form-inline" method="post" action="<?php echo base_url().'Wp/Transaksi/Rekap'?>">
                <div class="form-group">
                  <select id="tahun" name="tahun" required="required" placeholder="Tahun" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($tahun==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                  <select id="bulan" name="bulan" required="required" placeholder="Bulan" class="form-control select2 col-md-7 col-xs-12">
                        <?php 
                            foreach ($ref_bulan as $key => $value) {
                        ?>
                            <option <?php if($key==$bulan){ echo "selected"; }?> value="<?php echo $key ?>"><?php echo $value; ?></option>
                        <?php
                        } ?>
                    </select>
                </div>
                <button style="height: 28px;" type="submit" class="form-control btn btn-primary btn-xs"><i class="fa fa-search"></i> Cari</button>

                <?php if($jum_row!=0){ ?> <a href="<?php echo base_url('Wp/Transaksi/cetak_laporan/').$bulan.'/'.$tahun?>"> <button style="height: 28px;" type="button" class="form-control btn btn-success btn-xs"><i class="fa fa-file-excel-o"></i> Cetak</button> </a> <?php } ?>
        </form>
      <div align="center" class="x_content">
      <table style="width: 450px;" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr style="height: 28px;">
                          <th width="5%" align="center">No</th>
                            <th align="center">Tanggal</th>
                            <th align="center">Omset</th>
                            <th width="10%" align="center">Aksi</th>
                          <tr>
                      </thead>
                      <tbody>
                      <?php
                        $tot = 0;
                        $start = 0;
                        foreach ($rekap as $r)
                        {
                      ?>
                        <tr>
                          <td align='center'><?php echo ++$start ?></td>
                          <td align="center"><?php echo $r->tgl.'-'.$bulan.'-'.$tahun ?></td>
                          <td align="right"><?php echo 'Rp. '.number_format($r->omset,'0','','.') ?></td>
                          <td align="center"><button <?php if($r->omset==0){ echo "disabled";}?> class="btn btn-xs btn-success popup" data-tanggal="<?php echo $r->tgl.'-'.$bulan.'-'.$tahun;?>"><i class="fa fa-search"></i> Detail</button></td>
                        </tr>
                      <?php
                        $tot+=$r->omset;
                        }
                      ?>
                      </tbody>
                      <tfoot>
                        <tr style="height: 28px;">
                          <td align='center' colspan="2"><h4><b>TOTAL</b></h4></td>
                          <td align="right"><h5><b><?php echo 'Rp. '.number_format($tot,'0','','.') ?></b></h5></td>
                        </tr>
                      </tfoot>
                    </table>
                    
      </div>
    </div>
  </div>



</div> 
<div class="modal fade" id="modalDetail" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 750px">
            <div class="modal-body">
                <div  id="detail"></div>
               <i class="fa fa-refresh fa-spin  fa-fw"></i>
                
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
     $(document).ready(function() {
    $(".popup").click(function(){
        $.ajax({
            url: "<?= base_url('Wp/Transaksi/ajaxDetail') ?>",
            method: 'POST',
            data: {
                tanggal: $(this).data("tanggal")
            },
            success: function (data) {
                // alert(data);
                $('#detail').html(data);
                $('#modalDetail').modal('show');
            }
        });
    }); 
});
 </script>
