
        <style>
            
            
            .rating {
                border: none;
                float: left;
            }
            .rating > input { display: none; }
            .rating > label::before {
                margin: 5px;
                font-size: 3.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }
            .rating > label {
                color: #ddd;
                float: right;
            }
            .rating > input:checked ~ label,
            .rating:not(:checked) > label:hover,
            .rating:not(:checked) > label:hover ~ label {
                color: #f7d106;
            }
            .rating > input:checked + label:hover,
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label,
            .rating > input:checked ~ label:hover ~ label {
                color: #fce873;
            }
            h4 {
                font-weight: normal;
                margin-top: 40px;
                margin-bottom: 0px;
            }
            #hasil {
                font-size: 20px;
            }
            #star {
                float: left;
                padding-right: 20px;
            }
            #star span{
                padding: 3px;
                font-size: 10px;
            }
            .on { color:#f7d106 }
            .off { color:#ddd; }
            textarea.form-control {
  width: 250px;
}
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#rating .rate").click(function () {
                  var rat=$(this).val();
                  $('#bintang').val(rat);
                });
            });

            function myFunction(sel){

            var bintang = $('#bintang').val();
            var msg= document.getElementById("message").value;

            if (bintang=='' && msg=='') {
              alert('Rating dan pesan tidak boleh kosong !!!');
            } else if (bintang !='' && msg=='') {
              alert('pesan tidak boleh kosong !!!');
            } else if (bintang=='' && msg!='') {
              alert('Rating tidak boleh kosong !!!');
            } else {
              $.ajax({
             type: "POST",
             url : "<?php echo base_url().'Wp/Profil/add_rating';?>",
             data: { bintang: bintang,
                      msg:msg},
             cache: false,
             success: function(msga){
              alert(msga);
                  //var exp = msga.split("|");
                  //alert(exp[0]);
                  
                }
              });
            }
           
            
        }
            /* $.ajax({
                        url : "<?php echo base_url().'Wp/Profil/add_rating';?>",
                        method : "POST",
                        data : {rate : $(this).val()},
                        success: function(obj) {
                            var obj = obj.split('|');
                            alert(obj);
                            //$('#star'+obj[0]).attr('checked');
                           // $('#hasil').html('Rating '+obj[1]+'.0');
                           // $('#star').html(obj[2]);
                            //alert("terima kasih atas penilaian anda");
                        }
                    });*/
        </script>




  <div class="page-title">
   <div class="title_left">
    
  </div>
<div class="clearfix"></div>                  
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_content" >
                   <div class="col-md-12 col-sm-12 col-xs-12">                
                <div class="x_content">
                   <?php if ($star->STAR=='') {
                     $dsb='';
                   } else {
                     $dsb='disabled';
                   }?>

                   <?php if ($this->session->userdata('MS_ROLE_ID')=='8') {?>
                    <h3>Silahkan berikan penilaian anda</h3>  
                   <input type="hidden" name="bintang" id='bintang'>
                  <form id='rating' class="rating">                    
                    <input  <?= $dsb?>  <?php if ($star->STAR == '5') { echo 'checked'; } ?> type="radio" class="rate" id="star5" name="rating" value="5" />
                    <label for="star5" title="Sempurna - 5 Bintang"></label>
               
                    <input  <?= $dsb?> <?php if ($star->STAR == '4') { echo 'checked'; } ?> type="radio" class="rate" id="star4" name="rating" value="4"  />
                    <label for="star4" title="Sangat Bagus - 4 Bintang"></label>
               
                    <input  <?= $dsb?> <?php if ($star->STAR == '3') { echo 'checked'; } ?>  type="radio" class="rate" id="star3" name="rating" value="3" />
                    <label for="star3" title="Bagus - 3 Bintang"></label>
               
                    <input  <?= $dsb?> <?php if ($star->STAR == '2') { echo 'checked'; } ?> type="radio" class="rate" id="star2" name="rating" value="2"  />
                    <label for="star2" title="Tidak Buruk - 2 Bintang"></label>
               
                    <input  <?= $dsb?> <?php if ($star->STAR == '1') { echo 'checked'; } ?> type="radio" class="rate" id="star1" name="rating" value="1" />
                    <label for="star1" title="Buruk - 1 Bintang"></label>
                    <br>
                    <textarea <?php if($star->COMENT!=''){echo "disabled";}?> id="message" required="required" class="form-control" name="message" rows="6"><?= $star->COMENT?></textarea>
                    <br>
                    <?php if ($star->STAR=='') {?>
                        <a href="#" onclick="myFunction('sel')" class="btn btn-success btn-sm"><span class="fa fa-send"></span> Kirim</a >
                      <?php
                    } else {
                      echo "Terimakasih telah menilai aplikasi Sipanji";
                    }?>
                </form>
                   <?php 
                   } else {?>
                    <h3>Hasil Penilaian Wajib Pajak</h3>  
<?php 
$lm=$lima->CN;$em=$empat->CN;$tg=$tiga->CN;$da=$dua->CN;$st=$satu->CN;
$lima=100/$tot->CN*$lima->CN;
$empat=100/$tot->CN*$empat->CN;
$tiga=100/$tot->CN*$tiga->CN;
$dua=100/$tot->CN*$dua->CN;
$satu=100/$tot->CN*$satu->CN;
?>
<style>
{
  box-sizing: border-box;
}

.fa {
  font-size: 14px;
}

.checked {
  color: orange;
}

/* Three column layout */
.side {
  float: left;
  width: 10%;
  margin-top:10px;
}

.middle {
  margin-top:10px;
  float: left;
  width: 70%;
  margin-left: 70px;
}
.middle1 {
 
  float: left;
 
  margin-left: -85px;
}

/* Place text to the right */
.right {
  text-align: right;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* The bar container */
.bar-container {
  width: 100%;
  background-color: #f1f1f1;
  text-align: center;
  color: white;
}

/* Individual bars */
.bar-5 {width: <?= $lima?>%; height: 18px; background-color: #4CAF50;}
.bar-4 {width: <?= $empat?>%; height: 18px; background-color: #2196F3;}
.bar-3 {width: <?= $tiga?>%; height: 18px; background-color: #00bcd4;}
.bar-2 {width: <?= $dua?>%; height: 18px; background-color: #ff9800;}
.bar-1 {width: <?= $satu?>%; height: 18px; background-color: #f44336;}

/* Responsive layout - make the columns stack on top of each other instead of next to each other */
@media (max-width: 400px) {
  .side, .middle {
    width: 100%;
  }
  .right {
    display: none;
  }
}
</style>

<!-- <span class="heading">User Rating</span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span> -->
<p>Rata rata bintang 4.1 dari <?= $tot->CN?> reviews.</p>
<hr style="border:3px solid #f1f1f1">

 
  <div class="middle">
    <div class="middle1">
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
    </div>
    <div class="bar-container">
      <div class="bar-5"><?= $lima ?> %</div>
    </div>
  </div>
  <div class="side right">
    <div><a href="<?php echo base_url('Wp/Profil/detail_rating/5')?>"> <?= $lm ?> Wajib Pajak</a></div>
  </div>


    
  
  <div class="middle">
    <div class="middle1">
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
</div>
    <div class="bar-container">
      <div class="bar-4"><?= $empat ?> %</div>
    </div>
  </div>
  <div class="side right">
    <div><a href="<?php echo base_url('Wp/Profil/detail_rating/4')?>"> <?= $em ?> Wajib Pajak </a></div>
  </div>
 
  <div class="middle">
    <div class="middle1">
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
    </div>
    <div class="bar-container">
      <div class="bar-3"><?= $tiga ?> %</div>
    </div>
  </div>
  <div class="side right">
    <div><a href="<?php echo base_url('Wp/Profil/detail_rating/3')?>"><?= $tg ?> Wajib Pajak</a></div>
  </div>
 
  <div class="middle">
    <div class="middle1">
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
    </div>
    <div class="bar-container">
      <div class="bar-2"><?= $dua ?> %</div>
    </div>
  </div>
  <div class="side right">
    <div><a href="<?php echo base_url('Wp/Profil/detail_rating/2')?>"><?= $da ?> Wajib Pajak</a></div>
  </div>
  
  <div class="middle">
    <div class="middle1">
      <span class="fa fa-star checked"></span>
    </div>
    <div class="bar-container">
      <div class="bar-1"><?= $satu ?> %</div>
    </div>
  </div>
  <div class="side right">
    <div><a href="<?php echo base_url('Wp/Profil/detail_rating/1')?>"><?= $st ?> Wajib Pajak</a></div>
  </div>

                   <?php }?>
                   
                  </div>
                </div>
                </div>
              </div>
            </div> 
          </div>
      </div>
    </div>
  </div>
</div>


