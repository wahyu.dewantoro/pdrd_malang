<style>
.user-row {
    margin-bottom: 14px;
}

.user-row:last-child {
    margin-bottom: 0;
}

.dropdown-user {
    margin: 13px 0;
    padding: 5px;
    height: 100%;
}

.dropdown-user:hover {
    cursor: pointer;
}

.table-user-information > tbody > tr {
    border-top: 1px solid rgb(221, 221, 221);
}

.table-user-information > tbody > tr:first-child {
    border-top: 0;
}


.table-user-information > tbody > tr > td {
    border-top: 0;
}
.toppad
{margin-top:20px;
}
 <?php echo $this->session->flashdata('message')?> 
</style>
<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Profil</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                <div class=" col-md-12 col-lg-12 "> 
                  <table class="table table-user-information table-hover">
                    <tbody>
                      <?php foreach ($profil as $profil) {?>

                      <tr>
                        <td>NPWPD</td>
                        <td><?php echo $profil->NIP; ?></td>
                      </tr>
                      <tr>
                        <td>Nama User</td>
                        <td><?php echo $profil->NAMA; ?></td>
                      </tr>                      
                      <?php } ?>

                      <tr>
                        <td>Alamat</td>
                        <td><?php echo $wp->ALAMAT; ?></td>
                      </tr>
                      <tr>
                        <td>No Telp</td>
                        <td><?php echo $wp->NO_TELP; ?></td>
                      </tr>         
                     
                    </tbody>
                  </table>
                  </div>
            </div> 
        </div>
      </form>
    </div>
  </div>
</div>


</div>
</div>
</div>