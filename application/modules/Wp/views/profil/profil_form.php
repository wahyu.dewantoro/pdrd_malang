
    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:30px}
  </style>

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Profil</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama User <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA" name="NAMA" required="required" placeholder="Nama User" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA; ?>" <?php echo $disable ?>>               
                      </div>
                    </div>  
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                      <a href="<?php echo site_url('Wp/profil') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>                                        
                  </div>
                  <div class="col-md-6"> 
                  </div> 
                </div>
              </div>
            </div> 
        </div>
      </form>
    </div>
  </div>
</div>



</div>