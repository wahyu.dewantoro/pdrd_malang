                    <table id="example2" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th  class="text-center" width="3%">No</th>
                          <th  class="text-center" width="3%">Kd Usaha</th>
                          <th  class="text-center" width="8%">NPWP</th>
                          <th  class="text-center" width="10%">Nama Wp</th>
                          <th  class="text-center" width="14%">Nama Objek Pajak</th>
                          <th  class="text-center" width="16%">Alamat Objek Pajak</th>
                          <th  class="text-center" width="9%">Jenis Pajak</th>
                          <th  class="text-center" width="12%">Golongan</th>
                          <th  class="text-center" width="17%">Kel/Kec</th>
                          <th  class="text-center" width="7%">Aksi</th>
                        </tr>
                      </thead>
                    </table>
<script type="text/javascript">
         $(document).ready(function() {
             $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
             {
                 return {
                     "iStart": oSettings._iDisplayStart,
                     "iEnd": oSettings.fnDisplayEnd(),
                     "iLength": oSettings._iDisplayLength,
                     "iTotal": oSettings.fnRecordsTotal(),
                     "iFilteredTotal": oSettings.fnRecordsDisplay(),
                     "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                     "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                 };
             };
    
             var t = $("#example2").dataTable({
                 initComplete: function() {
                     var api = this.api();
                     $('#mytable_filter input')
                             .off('.DT')
                             .on('keyup.DT', function(e) {
                                 if (e.keyCode == 13) {
                                     api.search(this.value).draw();
                         }
                     });
                 },
                 
                     
                 
                 'oLanguage':
                 {
                   "sProcessing":   "Sedang memproses...",
                   "sLengthMenu":   "Tampilkan _MENU_ entri",
                   "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                   "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                   "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                   "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                   "sInfoPostFix":  "",
                   "sSearch":       "Cari:",
                   "sUrl":          "",
                   "oPaginate": {
                     "sFirst":    "Pertama",
                     "sPrevious": "Sebelumnya",
                     "sNext":     "Selanjutnya",
                     "sLast":     "Terakhir"
                    }
                 },
                 processing: true,
                 serverSide: true,
                 pageLength: 20,
                 ajax: {"url": "<?php echo base_url()?>Wp/tempat_usaha/json", "type": "POST"},
                 columns: [
                 {
                   "data": "ID_INC",
                   "orderable": false,
                   "className" : "text-center",
                 },
                 {"data":"ID_INC"}, 
                 {"data":"NPWPD"}, 
                 {"data":"NAMA"}, 
                 {"data":"NAMA_USAHA"},                 
                 {"data": "ALAMAT_USAHA"},
                 {"data": "NAMA_PAJAK"},
                 {"data": "DESKRIPSI"},
                 {"data": "KEC"},
                 {
                   "data" : "action",
                   "orderable": false,
                   "className" : "text-center"
                 }
                 ],
                 rowCallback: function(row, data, iDisplayIndex) {
                     var info = this.fnPagingInfo();
                     var page = info.iPage;
                     var length = info.iLength;
                     var index = page * length + (iDisplayIndex + 1);
                     $('td:eq(0)', row).html(index);
                 }
             });
         });
     </script>