
    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:30px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>
 
  <div class="pull-right">
    <?php echo anchor('Wp/tempat_usaha','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?>
  </div>
</div> 

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <!-- <div class="x_title">
                  <h4><i class="fa fa-building"></i> Tempat Usaha</h4>
                  <div class="clearfix"></div>
                </div> -->
                <div class="x_content" >
                  <div class="col-md-8">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 
                    <?php if ($this->session->userdata('MS_ROLE_ID')==8 OR $this->role==81) {?>
                      <input type="hidden" name="NPWPD" required="required" readonly value="<?php echo $this->session->userdata('NIP'); ?>">               
                    <?php    
                      } else {?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > NPWPD<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" name="NPWPD" required="required" onkeyup="formatangka(this)"; value="<?php echo $NPWPD; ?>" <?php echo $disable ?> <?php echo $disable1 ?> class="form-control col-md-7 col-xs-12" onchange="NamaUsaha(this.value);" >               
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama WP<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_WP" required="required" class="form-control col-md-7 col-xs-12" readonly value="<?php error_reporting(E_ALL^(E_NOTICE|E_WARNING));echo $NAMA_WP->NAMA;?>">               
                      </div>
                    </div>
                      <?php }?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Tempat Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>>               
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat Tempat Usaha<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <textarea style="font-size: 11px" <?php echo $disable ?> id="ALAMAT_USAHA" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                      </div>
                    </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan Tempat Usaha<sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan Tempat Usaha<sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select  style="font-size:11px" id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                  <?php if($button==('Update SPTPD Hotel'||'Form SPTPD Hiburan')){?>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php }} ?>                          
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jenis Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getgol(this);"  style="font-size:11px" id="JENIS_PAJAK" <?php echo $disable?> name="JENIS_PAJAK" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($jp as $jp){ ?>
                          <option <?php if($jp->ID_INC==$JENIS_PAJAK){echo "selected";}?> value="<?php echo $jp->ID_INC?>"><?php echo $jp->NAMA_PAJAK ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nomor Rek. Jenis Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select style="font-size:11px" id="REKENING" name="REKENING" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($rek as $rek){ ?>
                          <option <?php if($rek->KODE_REKENING_PAJAK==$REKENING){echo "selected";}?> value="<?php echo $rek->KODE_REKENING_PAJAK?>"><?php echo $rek->REKENING.'.'.$rek->JENIS_PAJAK.'.'.$rek->KODE_REKENING_PAJAK.' - '.$rek->KETERANGAN ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> -->
                    <div class="form-group" id="MY_GOL">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select  style="font-size:11px" id="GOLONGAN" <?php echo $disable?> name="GOLONGAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>  
                          <?php foreach($gol as $gol){ ?>
                          <option <?php if($gol->ID_OP==$GOLONGAN){echo "selected";}?> value="<?php echo $gol->ID_OP?>"><?php echo $gol->DESKRIPSI ?></option>
                          <?php } ?>                                                 
                        </select>
                      </div>
                    </div> 

                    <!-- <div class="form-group" style="<?php echo $style?>" id="MY_QTT">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Kamar / Meja <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <input type="text"  name="qtt_kamar_meja" value="<?php echo $jumlah_kamar_meja;?>" <?php echo $disable ?> class="form-control col-md-7 col-xs-12" >               
                      </div>
                    </div> -->
                     <input type="hidden"  name="qtt_kamar_meja" value="<?php echo $jumlah_kamar_meja;?>" <?php echo $disable ?> >
                  <?php if($disable==''){?>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                      <a href="<?php echo site_url('Wp/Tempat_usaha') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  <?php } ?>                                          
                  </div>
                  <div class="col-md-6"> 
                  </div> 
                </div>
              </div>
            </div> 
        </div>
      </form>
    </div>
  </div>
</div>



</div>
<script>
    function validasi(){
      var NAMA_WP      =document.forms["demo-form2"]["NAMA_WP"].value;
      var NAMA_USAHA   =document.forms["demo-form2"]["NAMA_USAHA"].value;
      var ALAMAT_USAHA =document.forms["demo-form2"]["ALAMAT_USAHA"].value;
      var JENIS_PAJAK  =document.forms["demo-form2"]["JENIS_PAJAK"].value;
      var KECAMATAN    =document.forms["demo-form2"]["KECAMATAN"].value;
      var KELURAHAN    =document.forms["demo-form2"]["KELURAHAN"].value;
      if (NAMA_USAHA==null || NAMA_USAHA==""){
        swal("Nama Usaha Harus di Isi", "", "warning")
        return false;
      };
      if (ALAMAT_USAHA==null || ALAMAT_USAHA==""){
        swal("Alamat Usaha Harus di Isi", "", "warning")
        return false;
      }; 
      if (JENIS_PAJAK==null || JENIS_PAJAK==""){
        swal("Jenis Pajak Harus di Isi", "", "warning")
        return false;
      };
      if (KECAMATAN==null || KECAMATAN==""){
        swal("Kecamatan Harus di Isi", "", "warning")
        return false;
      }; 
      if (KELURAHAN==null || KELURAHAN==""){
        swal("Kelurahan Harus di Isi", "", "warning")
        return false;
      };
      if (NAMA_WP==null || NAMA_WP==""){
        swal("NPWP/ Nama WP Harus Terdaftar Terlebih Dahulu", "", "warning")
        return false;
      };

    }  
    function getgol(sel)
    {
      var ID_INC = sel.value;
      if (ID_INC==06||ID_INC==04) {document.getElementById("MY_GOL").style.visibility = "hidden";} else{document.getElementById("MY_GOL").style.visibility = "visible";};
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getgol'?>",
       data: { ID_INC: ID_INC},
       cache: false,
       success: function(msga){
           // alert(msga);
            $("#GOLONGAN").html(msga);
          }
        });

      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getrek'?>",
       data: { ID_INC: ID_INC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#REKENING").html(msga);
          }
        });
      if (ID_INC==01||ID_INC==02) {
          document.getElementById("MY_QTT").style.visibility = "visible";} 
      else{
        document.getElementById("MY_QTT").style.visibility = "hidden";};    
    }
    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

  <script>
function validate() {
    if ((document.getElementById('ROKOK').checked)&&(document.getElementById('KETINGGIAN').checked)) {//rokok dan ketinggain cek
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var jr=parseFloat(nominalFormat(document.getElementById("JNS").value))
      if (jr=1) {
            var a=125/100;
            var b=25/100;
            var c=10/100;
        } else{
            var a=0/100;
            var b=25/100;
            var c=10/100;
        };
      var pjt=dpp+(dpp*persen)+(dpp*persen)/*+(dpp*a/b*c)*/;
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else if((document.getElementById('ROKOK').checked)||(document.getElementById('KETINGGIAN').checked)){//jika rokok cek /ketinggian 
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var pjt=dpp+(dpp*persen);
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else if (document.getElementById('ROKOK').checked) {//jika rokok cek
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var pjt=dpp+(dpp*persen);
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else if(document.getElementById('KETINGGIAN').checked){//jika ketinggian cek
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var pjt=dpp+(dpp*persen);
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else if(document.getElementById('JASBONG').checked){//jika ketinggian cek
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var jr=parseFloat(nominalFormat(document.getElementById("JNS").value))
      var pjt=dpp+(dpp*a/b*c);
      PAJAK_TERUTANG.value=getNominal(pjt);
    }else if ((document.getElementById('ROKOK').checked)||(document.getElementById('JASBONG').checked)) {//rokok dan ketinggain cek
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var jr=parseFloat(nominalFormat(document.getElementById("JNS").value))
      var pjt=dpp+(dpp*persen)+(dpp*a/b*c)/*+(dpp*a/b*c)*/;
      PAJAK_TERUTANG.value=getNominal(pjt);
    }else {

      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      PAJAK_TERUTANG.value=getNominal(dpp);
    }
    
  }
  function NamaUsaha(id){
                //alert(id);
                var npwpd=id;
                var jp='01';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                // alert(msg);
              }else{
                alert('NPWPD Tidak ditemukan');
                $("#NAMA_WP").val(null);   
              }
              
            }
          });                
    } 
function formatangka(objek) {
           a = objek.value;
           b = a.replace(/[^\d]/g,"");
           c = "";
           panjang = b.length;
           j = 0;
           for (i = panjang; i > 0; i--) {
             j = j + 1;
               c = b.substr(i-1,1) + c;
           }
           objek.value = c;
        }
  </script> 