<?php echo $this->session->flashdata('message')?><br>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title"> 
        <h2>Tempat Usaha</h2>
        <div class="pull-right">
          <div class="btn-group">
            <?php echo anchor(site_url('Wp/tempat_usaha/create'), '<i class="fa fa-plus"></i> Tambah', 'class="btn btn-primary btn-sm"'); 
            ?>
          </div>
        </div>                  
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

      </div>
    </div>
  </div>



</div> <script>
  $(document).ready(function(){

   readProducts(); /* it will load products when document loads */

   $(document).on('click', '#delete', function(e){

    var id = $(this).data('id');
    SwalDelete(id);
    e.preventDefault();
  });

 });
  
  function SwalDelete(id){

    swal({
      title: 'Apakah Anda Yakin?',
      text: "Data Akan dihapus Permanen!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!',
      showLoaderOnConfirm: true,

      preConfirm: function() {
        return new Promise(function(resolve) {

         $.ajax({
          url: '<?php echo base_url()?>Wp/tempat_usaha/hapus',
          type: 'POST',
          data: 'delete='+id,
          dataType: 'json'
        })
         .done(function(response){
          swal('Deleted!', response.message, response.status);
          readProducts();
        })
         .fail(function(){
          swal('Oops...', 'Something went wrong with ajax !', 'error');
        });
       });
      },
      allowOutsideClick: false        
    }); 
    
  }
  function readProducts(){
    $('.x_content').load('tempat_usaha/table'); 
  }
</script>