   <?php $namabulan=array(
    '',
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
    ) ?>
      <style>

      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
      
      select {
        font-weight: bold;
      }
      .table{
        font-size: 13.5px;
        font-weight: bold;
      }
      h2{ font-size: 16.5px;}
    </style>  
     
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title"> 
        <h2><b>Laporan Pelaporan Pajak</b></h2>
        <div class="pull-right">
        </div>                  
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php if ($this->session->userdata('MS_ROLE_ID')==8 OR $this->session->userdata('MS_ROLE_ID')==81) {?>
           <div class="col-md-6 col-lg-6 "> 
             <table class="table" style="border: 0 !important">
               <tbody>
                 <?php foreach ($profil as $profil) {?>
                 <tr>
                   <td>NPWPD</td>
                   <td>:</td>
                   <td><?php echo $profil->NIP; ?></td>
                 </tr>
                 <tr>
                   <td>Nama</td>
                   <td>:</td>
                   <td><?php echo $profil->NAMA; ?></td>
                 </tr>                      
                 <?php } ?>
                 <tr>
                   <td>Alamat</td>
                   <td>:</td>
                   <td><?php echo $wp->ALAMAT; ?></td>
                 </tr>                 
               </tbody>
             </table>
           </div>
           <div class="col-md-6 col-lg-6 "> 
            <form id="myform" data-parsley-validate   action=""<?php echo base_url().'Data_tagihan'?>" method="get">
            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
              <select id="tahun"  class="tahun form-control input-sm" name="tahun" required>
                <option>Tahun</option>
                <?php $thskr =date('Y');
                    for ($i=$thskr; $i>=$thskr-7 ; $i--) {  ?>
                <option <?php if($i==$tahun){echo "selected";}?> value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php } ?>
              </select>                   
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
              <select id="tu"  class="tahun form-control input-sm" name="tu" required >
                <option></option>
                <?php foreach ($TU as $key ) { ?>
                <option  <?php if($key->ID_INC==$tu){echo "selected";}?> value="<?php echo $key->ID_INC?>"><?php echo '('.$key->ID_INC.')'.$key->NAMA_PAJAK." - ".$key->NAMA_USAHA." - ".$key->NAMAKELURAHAN?></option>
                <?php } ?>
              </select>                   
            </div>  
           <div class=" col-md-2 col-sm-12 col-xs-12"> 
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tampilkan</button>
          </div> 
          </div>  

       <?php } else { ?>
        <div class="row">
           <div class="col-md-12 col-lg-12 "> 
            <form id="myform" data-parsley-validate   action=""<?php echo base_url().'Data_tagihan'?>" method="get">
            <div class="col-md-2 col-sm-3 col-xs-12 form-group">
              <select id="tahun"  class="tahun form-control input-sm" name="tahun" required="" >
                <option>Tahun</option>
                <?php $thskr =date('Y');
                    for ($i=$thskr; $i>=$thskr-7 ; $i--) {  ?>                
                <option <?php if($i==$tahun){echo "selected";}?> value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php } ?>
              </select>             
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                <input type="text" required id="npwpd" onchange="npwpd1(this.value);"  name="npwpd"  required placeholder="NPWPD" class="form-control col-md-4 col-xs-4 wp" value="<?php echo $npwpd ?>">                
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 form-group">
              <select  style="font-size:11px" id="JENIS_PAJAK1" required name="jenisPajak"  class="form-control select2 col-md-7 col-xs-12" onchange="jns_pajak(this.value);">
                          <option value="">Pilih</option>
                          <?php foreach ($kc as $jns) {?>               
                          <option <?php if ($jns->KD==$jenisPajak.'|'.$npwpd) {echo "selected";}?> value="<?php echo $jns->KD?>"><?php echo $jns->NAMA_PAJAK ?></option>
                          <?php } ?>
              </select>
            </div>              
            <div class="col-md-2 col-sm-12 col-xs-12 form-group">
                <select  style="font-size:11px" id="NAMA_USAHA" name="nama_usaha"  class="form-control select2 col-md-7 col-xs-12" required>
                          <option value="">Pilih</option>
                          <?php foreach ($tu as $jns) {?>               
                          <option <?php if ($jns->ID_INC==$id_inc) {echo "selected";}?> value="<?php echo $jns->ID_INC?>"><?php echo '('.$jns->ID_INC.')'.$jns->NAMA_USAHA.' / '.$jns->NAMAKELURAHAN.' / '.$jns->NAMAKEC ?></option>
                          <?php } ?>
                </select>
              <!-- <input type="text" name="nama_usaha" required placeholder="Nama Usaha" class="form-control col-md-4 col-xs-4" value="<?php echo $nama_usaha ?>">  -->
            </div>             
           <div class=" col-md-2 col-sm-12 col-xs-12"> 
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tampilkan</button>
          </div> 
          </div> 
        </div>          
           <div class=" col-md-12 col-lg-12 "> 
             <table class="table borderless" style="border: none !important">
               <tbody>
                 <?php foreach ($wp as $wp) {?>
                 <tr>
                   <td width="30">NPWPD</td>
                   <td width="5">:</td>
                   <td><?php echo $wp->NPWP; ?></td>
                 </tr>
                 <tr>
                   <td>Nama</td>
                   <td>:</td>
                   <td><?php echo $wp->NAMA; ?></td>
                 </tr>         

                 <tr>
                   <td>Alamat</td>
                   <td>:</td>
                   <td><?php echo $wp->ALAMAT; ?></td>
                 </tr>                  
                 <?php } ?>
                
               </tbody>
             </table>
           </div>        
           <br>  
       <?php } ?>
<br>   
  <?php if($rk=="tampil"){?>
         <table id="example2" class="table table-striped table-bordered table-hover" style="font-size: 12px">
          <thead>
            <tr>
              <th colspan="9" class="text-center"> Data Transaksi Tahun <?php echo $tahun ?></th>
            </tr>
            <tr>
              <th class="text-center" rowspan="2" width="5%">No</th>
              <th class="text-center" rowspan="2" width="10%">Masa Pajak</th>
              <th class="text-center" colspan="4">Laporan</th>
              <th class="text-center" colspan="4">Pembayaran</th>
            </tr>
            <tr>
              <th class="text-center" width="10%">Kode Biling</th>
              <!-- <th class="text-center" width="8%">Tanggal</th> -->
              <th class="text-center" width="20%">Tanggal Lapor</th>
              <th class="text-center">Pajak (Rp)</th>
              <th class="text-center" width="8%">Tanggal Bayar</th>
              <th class="text-center">Jumlah</th>
              <th class="text-center">Cetak Tagihan</th>
              <th class="text-center">Bukti Pembayaran</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $month=''; $no=1;$tot=0; foreach ($res as $key) { 
              if (strlen($key->MASA_PAJAK==1)) {
                  $month="0".$key->MASA_PAJAK;
              }


              ?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $namabulan[$key->MASA_PAJAK]; ?></td>
              <?php if($key->MASA_PAJAK!='') { ?>
                  <td><?php echo $key->KODE_BILING ?></td>
                  <!-- <td align="center"><?php echo $key->TANGGAL_PENERIMAAN; ?></td> -->
                  <?php  if (empty($key->TANGGAL_PENERIMAAN)) {
                    $a=$tahun;
                    $b=$td->TAHUN_DAFTAR;
                    $c=$key->MASA_PAJAK;
                    $d=$td->BULAN_DAFTAR;
                    if ($b<$a ) { ?>
                        <td class="text-center" style="color:red;">BELUM LAPOR </td>
                        <td></td>
                      <?php                      
                    } else if ($b==$a AND $d>$c) {?>
                        <td class="text-center" style="color:red;">BELUM TERDAFTAR</td>
                        <td></td>
                      <?php    
                    } else {?>
                        <td class="text-center" style="color:red;">BELUM LAPOR</td>
                        <td></td>
                      <?php    
                    }                    

                  } else { ?>
                    <td class="text-center"> <?php echo $key->TANGGAL_PENERIMAAN ?></td>
                  <td class="text-right"><?php echo number_format($key->PAJAK_TERUTANG,0,",",".")?></td>                
                  <?php } ?>
                  <td align="center"><?php if ($key->STATUS=='0') { echo "Belum Bayar";
                  } else { echo $key->TGL_BAYAR;} ?></td>
                  <td class="text-right"><?php if ($key->STATUS=='0') { $byr=0;echo "Belum Bayar";
                  } else { echo number_format($byr=$key->JUMLAH_BAYAR,0,",",".");} ?></td>  
                  <td align='center'> <?php if (empty($key->TANGGAL_PENERIMAAN)) {

                  } else {?> <a href="<?php echo base_url('Pdf/pdf_kode_biling/').acak($key->KODE_BILING);?>"><i class="fa fa-print"></i></a><?php }?></td> 
                  <td align="center"><?php if ($key->STATUS=='1') {?><a href="<?php echo base_url('Pdf/pdf_bukti_pembayaran/').acak($key->KODE_BILING);?>" target="_blank"><i class="fa fa-print"></i></a><?php
                  } else { } ?></td>
            <?php } else {?>
                  <td class="text-center">-</td>
                  <td class="text-center">-</td>
                  <td class="text-center">-</td>
                  <td class="text-center">-</td>
                  <td class="text-center">-</td>
                  <td class="text-center">-</td>
                  <td class="text-center">-</td>
            <?php } ?>                  
            </tr>
            <?php $tot+=$byr;} ?>
            <tr>
                  <td class="text-center" colspan="6"><b>TOTAL</b></td>  
                  <td class="text-right"><b><?php echo number_format($tot,0,",",".");?></b></td>
            </tr>
          </tbody>
        </table><?php } ?>                              
         </div>        
      </div>
    </div>
  </div>
</div> 
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
  function npwpd1(value){  
          var data1=value;    
         data2=data1.split('|'); 
         //alert(data2[0]);
         npwp=data2[0]
         $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Wp/Data_tagihan/get_jenis_pajak'?>",
           data: { npwp: npwp},
           cache: false,
           success: function(msga){
                  //alert(msga);
                  $("#JENIS_PAJAK1").html(msga);
                }
              });   
        
    }
    function jns_pajak(value){  
          var data1=value; 
          //alert(data1);
          //data2=data1.split('|'); 
         //alert(data2[0]);
         //npwp=data2[0]
         $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Wp/Data_tagihan/get_nama_usaha'?>",
           data: { data1: data1},
           cache: false,
           success: function(msga){
                  //alert(msga);
                  $("#NAMA_USAHA").html(msga);
                }
          }); 
    }
</script>
   <script type="text/javascript">
      $(this).ready( function() {
        $("#npwpd").autocomplete({
            minLength: 1,
            source: 
            function(req, add){
              //alert(req);
                $.ajax({
                url: "<?php echo base_url(); ?>Wp/Data_tagihan/lookup_wp",
                  dataType: 'json',
                  type: 'POST',
                  data: req,
                  success:    
                  function(data){
                    //alert(data);
                      if(data.response =="true"){
                        add(data.message);
                      }

                  },
                  });
            },    
        });
      }); 
      
</script>
