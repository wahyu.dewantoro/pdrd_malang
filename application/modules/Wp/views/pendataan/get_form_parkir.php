      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content" >  
            <div class="row">
              
              <div class="col-md-7 col-sm-">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-desktop"></i> Data Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <!-- konten -->
                    <div class="form-group">
                      <label class="control-label col-md-xs-12">Menggunakan Karcis 
                       <a class="btn btn-xs btn-success" id="tambahdiag"><i class="fa fa-plus"></i> Tambah</a>
                      </label>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">

                        <table  width="100%" class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Jenis</th>
                              <th>Jum Lembar</th>
                              <th>Nominal</th>
                              <th>Penerimaan</th>
                              <th><i class="fa fa-trash"></i></th>
                            </tr>
                          </thead>
                          <tbody id="kontentdiag">
                            <tr>
                              <td align="center"><input  type="text" name="JENIS_PARKIR[]" class="form-control" required > </td>
                              <td align="center"><input onchange='get(this);' id="JUMLAH_LEMBAR" type="text" name="JUMLAH_LEMBAR[]" class="form-control" required > </td>
                              <td align="center"><input readonly=""  onchange='get(this);' id="NOMINAL" type="text" name="NOMINAL[]" class="form-control" required > </td>
                              <td align="center"><input id="PENERIMAAN1" type="text" name="PENERIMAAN[]" class="form-control" required > </td>
                              <td align="center">-</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Karcis <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PENERIMAAN_KARCIS" onchange='get(this);' id="PENERIMAAN_KARCIS" required class="col-md-7 col-xs-12 form-control" value="" >
                        </div>
                      </div>
                    </div
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Non Karcis <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PENERIMAAN_NON_KARCIS" onchange='get(this);' id="PENERIMAAN_NON_KARCIS" required class="col-md-7 col-xs-12 form-control">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">DPP <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="DPP" id="DPP" readonly required class="col-md-7 col-xs-12 form-control" >
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Pajak Terutang <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" name="PAJAK_TERUTANG" id="PAJAK_TERUTANG" required class="col-md-7 col-xs-12 form-control"  >
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">SSPD <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="SSPD" id="SSPD" required class="col-md-7 col-xs-12 form-control" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Rekap Penggunaan BON <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="REKAP_BON" id="REKAP_BON" required class="col-md-7 col-xs-12 form-control" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-md-xs-12">Lainya <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="LAINYA" id="LAINYA" required class="col-md-7 col-xs-12 form-control">
                      </div>
                    </div>                    
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" onClick="return validasi()"><i class="fa fa-save"></i> Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
 <script>
    function get() {
      //alert("oek");
      var L=parseFloat(nominalFormat(document.getElementById("JUMLAH_LEMBAR").value))|| 0;
      var N=parseFloat(nominalFormat(document.getElementById("NOMINAL").value))|| 0;
      var NON=parseFloat(nominalFormat(document.getElementById("PENERIMAAN_NON_KARCIS").value))|| 0;
      //alert(getNominal(D*H*B*3600/1000));
      var P=(L*N).toFixed(0);
      PENERIMAAN1.value= getNominal(P);
      PENERIMAAN_KARCIS.value= getNominal(P);
      var dpp=parseFloat(P)+parseFloat(NON);
      DPP.value= getNominal(dpp);
      PAJAK_TERUTANG.value= getNominal(dpp*0.3);
    }
    
    function gantiTitikKoma(angka){
        return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
        return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
      }
    }

  /* JUMLAH_LEMBAR */
  var JUMLAH_LEMBAR = document.getElementById('JUMLAH_LEMBAR');
  JUMLAH_LEMBAR.addEventListener('keyup', function(e)
  {
    JUMLAH_LEMBAR.value = formatRupiah(this.value);
  });
  /* NOMINAL */
  var NOMINAL = document.getElementById('NOMINAL');
  NOMINAL.addEventListener('keyup', function(e)
  {
    NOMINAL.value = formatRupiah(this.value);
  });    
  /* PENERIMAAN_NON_KARCIS */
  var PENERIMAAN_NON_KARCIS = document.getElementById('PENERIMAAN_NON_KARCIS');
  PENERIMAAN_NON_KARCIS.addEventListener('keyup', function(e)
  {
    PENERIMAAN_NON_KARCIS.value = formatRupiah(this.value);
  });
  /* DPP */
  var DPP = document.getElementById('DPP');
  DPP.addEventListener('keyup', function(e)
  {
    DPP.value = formatRupiah(this.value);
  });
  /* PAJAK_TERUTANG */
  var PAJAK_TERUTANG = document.getElementById('PAJAK_TERUTANG');
  PAJAK_TERUTANG.addEventListener('keyup', function(e)
  {
    PAJAK_TERUTANG.value = formatRupiah(this.value);
  });
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
function ribuan (angka)
{
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan  = reverse.match(/\d{1,3}/g);
  ribuan  = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}
  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
  }

 $("#NOMINAL").prop('readonly', false);
var x=1;
  $("#tambahdiag").click(function(){
    //alert("ok");
    x++;
                $.get("Pendataan/karcis/"+x, function (data) {
                    $("#kontentdiag").append(data);
                });
 });

    $('#kontentdiag').on('click', '.remove_project_file', function(e) {
      x--;
    e.preventDefault();
    $(this).parents(".barisdiag").remove();
  });
</script>