<div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Data Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">              
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12"   placeholder="No Formulir">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo date('d/m/Y')?>" required >
                        </div>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo date('01/m/Y')?>" required >
                        </div>
                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>  -->
                    <?php
                    $persen = "var persen = new Array();\n";
                    $masa = "var masa = new Array();\n";
                    $tarif = "var tarif = new Array();\n";
                    ?>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN" name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                          <option  value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                          <?php 
                          $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                          $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                          $tarif .= "tarif['" . $GOLONGAN->ID_OP . "'] = {tarif:'".addslashes(number_format($GOLONGAN->TARIF,'0','','.'))."'};\n";
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <script type="text/javascript">  
                    <?php echo $persen;echo $masa;echo $tarif;?>             
                    function changeValue(id){
                      document.getElementById('PAJAK').value = persen[id].persen;
                      document.getElementById('MASA').value = masa[id].masa;
                      document.getElementById('HARGA_JUAL_MINERAL').value = tarif[id].tarif;
                      DPP.value ="";
                      PAJAK_TERUTANG.value="";
                      PENGAMBILAN_MINERAL.value="";
                    }
                  </script>    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-9">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN"  > Entrian Sederhana
                        </label>
                      </div>
                    </div>
                  </div>    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Pengambilan Mineral <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                      <input onchange='get(this);' type="text" id="PENGAMBILAN_MINERAL" name="PENGAMBILAN_MINERAL" required="required" placeholder="Jumlah Pengambilan Mineral" class="form-control col-md-7 col-xs-12" >    
                        <span class="input-group-addon">M <sup>2</sup></span>   
                    </div>
                  </div>   
                </div>

              
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                </div>
              </div>
             
                </div>                  
                <div class="col-md-6">       
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Harga Jual Mineral <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input readonly="" onchange='get(this);' type="text" id="HARGA_JUAL_MINERAL" name="HARGA_JUAL_MINERAL" required="required" placeholder="Harga Jual Mineral" class="form-control col-md-7 col-xs-12"  >                
                      </div>
                    </div>  
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12"   placeholder="Masa Pajak" >
                    </div>
                    <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12"   placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" >                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" >                
                      </div>
                    </div> 
                  </div> 
                </div>
              </div>
            </div> 
             <script>
      $("#NPWPD").keyup(function(){
        var npwpd = $("#NPWPD").val();
        $.ajax({
          url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
          type: 'POST',
          data: "npwpd="+npwpd,
          cache: false,
          success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
      });
      function get() {
        var harga= parseInt(document.getElementById("PENGAMBILAN_MINERAL").value.replace(/\./g,''))|| 0;
        var hasil= parseInt(document.getElementById("HARGA_JUAL_MINERAL").value.replace(/\./g,''))|| 0;
        var pajak= parseInt(document.getElementById("PAJAK").value.replace(/\./g,''))/100|| 0;
        var dpp=hasil*harga;
        DPP.value = getNominal(dpp);
        PAJAK_TERUTANG.value = getNominal(dpp*pajak);
      }
//PENGAMBILAN_MINERAL 
var PENGAMBILAN_MINERAL = document.getElementById('PENGAMBILAN_MINERAL'),
numberPattern = /^[0-9]+$/;

PENGAMBILAN_MINERAL.addEventListener('keyup', function(e) {
  PENGAMBILAN_MINERAL.value = formatRupiah(PENGAMBILAN_MINERAL.value);
});
//HARGA_JUAL_MINERAL 
var HARGA_JUAL_MINERAL = document.getElementById('HARGA_JUAL_MINERAL'),
numberPattern = /^[0-9]+$/;

HARGA_JUAL_MINERAL.addEventListener('keyup', function(e) {
  HARGA_JUAL_MINERAL.value = formatRupiah(HARGA_JUAL_MINERAL.value);
});

    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
</script>