               
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Data Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">              
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="NO_FORMULIR" class="form-control col-md-7 col-xs-12" placeholder="No Formulir">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" required value="<?php echo date('d/m/Y')?>">
                        </div>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo date('01/m/Y')?>" required  >
                        </div>
                      </div>
                    </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jenis Pengelolaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_PENGELOLAAN"  name="JENIS_PENGELOLAAN"  class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option value="Pengusahaan">Pengusahaan</option>
                        <option value="Habitat Alami">Habitat Alami</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Burung</label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_BURUNG"  name="JENIS_BURUNG"  class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option value="Walet">Walet</option>
                        <option value="Sriti">Sriti</option>
                      </select>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kepemilikan Ijin <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="KEPEMILIKAN_IJIN"  name="KEPEMILIKAN_IJIN"  class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option value="Ijin">Ijin</option>
                        <option value="Belum Ijin">Belum Ijin</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN"> Entrian Sederhana
                        </label>
                      </div>
                    </div>
                  </div>  
                  <div id="DATA_PAJAK"> 
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Luas Area </label>
                    <div class="col-md-3">
                      <input type="text" name="LUAS_AREA" id="LUAS_AREA" class="form-control col-md-7 col-xs-12" placeholder="Luas Area" >
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Jumlah Galur </label>
                    <div class="col-md-3">
                      <input type="text" name="JUMLAH_GALUR" id="JUMLAH_GALUR" class="form-control col-md-7 col-xs-12"   placeholder="Jumlah Galur" >
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dipanen Setiap <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="DIPANEN_SETIAP"  name="DIPANEN_SETIAP" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option value="4 Bulan Sekali">4 Bulan Sekali</option>
                        <option value="6 Bulan Sekali">6 Bulan Sekali</option>
                        <option value="1 Tahun Sekali">1 Tahun Sekali</option>
                        <option value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Hasil Panen </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="HASIL_PANEN"  name="HASIL_PANEN" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option value="Dipasarkan Sendiri">Dipasarkan Sendiri</option>
                        <option value="Disetrokan ke Asosiasi">Disetrokan ke Asosiasi</option>
                        <option value="Diambil Tengkulak">Diambil Tengkulak</option>
                        <option value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Hasil Setiap Panen </label>
                    <div class="col-md-3">
                      <div class="input-group">
                      <input  onchange='get(this);'   type="text" name="HASIL_SETIAP_PANEN" id="HASIL_SETIAP_PANEN" class="form-control col-md-7 col-xs-12"    placeholder="Hasil Setiap Panen" >
                        <span class="input-group-addon">Kg</span></div>
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Harga Rata Rata </label>
                    <div class="col-md-3">
                    <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                      <input   onchange='get(this);'  type="text" name="HARGA_RATA_RATA" id="HARGA_RATA_RATA" class="form-control col-md-7 col-xs-12"  placeholder="Harga Rata Rata" >
                    </div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                    </div>
                  </div>
                </div>                  
                <div class="col-md-6">    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" value="5" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12"  placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" >                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" >                
                      </div>
                    </div> 
                  </div> 
                </div>
              </div>
            </div>     
          </div>
            </div>
      


      </div>
      <script>
      

    function get() {
      var HARGA_RATA_RATA= parseInt(document.getElementById("HARGA_RATA_RATA").value.replace(/\./g,''))|| 0;
      var HASIL_SETIAP_PANEN= parseInt(document.getElementById("HASIL_SETIAP_PANEN").value.replace(/\./g,''))|| 0;
      var dpp=HARGA_RATA_RATA*HASIL_SETIAP_PANEN;
      DPP.value = ribuan(dpp);
      PAJAK_TERUTANG.value = ribuan(dpp*0.05);
    }
//LUAS_AREA 
var LUAS_AREA = document.getElementById('LUAS_AREA'),
numberPattern = /^[0-9]+$/;

LUAS_AREA.addEventListener('keyup', function(e) {
  LUAS_AREA.value = getFormattedData(LUAS_AREA.value);
});
//JUMLAH_GALUR 
var JUMLAH_GALUR = document.getElementById('JUMLAH_GALUR'),
numberPattern = /^[0-9]+$/;

JUMLAH_GALUR.addEventListener('keyup', function(e) {
  JUMLAH_GALUR.value = getFormattedData(JUMLAH_GALUR.value);
});
//HASIL_SETIAP_PANEN 
var HASIL_SETIAP_PANEN = document.getElementById('HASIL_SETIAP_PANEN'),
numberPattern = /^[0-9]+$/;

HASIL_SETIAP_PANEN.addEventListener('keyup', function(e) {
  HASIL_SETIAP_PANEN.value = getFormattedData(HASIL_SETIAP_PANEN.value);
});
//HARGA_RATA_RATA 
var HARGA_RATA_RATA = document.getElementById('HARGA_RATA_RATA'),
numberPattern = /^[0-9]+$/;

HARGA_RATA_RATA.addEventListener('keyup', function(e) {
  HARGA_RATA_RATA.value = getFormattedData(HARGA_RATA_RATA.value);
});

function checkNumeric(str) {
  return str.replace(/\./g, '');
}
Number.prototype.format = function() {
  return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

function getFormattedData(num) {
  var i = checkNumeric(num),isNegative = checkNumeric(num) < 0,val;

  if (num.indexOf(',') > -1) {
    return num;
  }
  else if (num[num.length - 1] === '.') {
    return num;
  }
  else if(i.length < 3) {
   return i;
 }else {
  val = Number(i.replace(/[^\d]+/g, ''));
}

return (isNegative ? '-' : '') + val.format();
}    
function ribuan (angka)
{
  if(angka< 0)
  {
    var str = angka.toString();
    var res = str.substr(1);
    var reverse = res.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return "-"+ribuan;    
    return "-"+res;
  } else {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }
}        
    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  $('#KEPEMILIKAN_IJIN').on('change', function() {
    if(this.value=='Ijin'){
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_psb/sptpd_psb/ijin/';?>'); 
    } else {
      $('#IJIN').remove();
    }
  }) 
      </script>