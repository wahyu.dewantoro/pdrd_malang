    <?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>

    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:30px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3></h3>
  </div>
</div> 
<div class="clearfix"></div>                  
<div class="row">
  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo site_url('Wp/Pendataan/create_action');?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($list_masa_pajak as $list_masa_pajak){ ?>
                            <option <?php if (date('m')==$list_masa_pajak) {echo "selected";}?> value="<?php echo $list_masa_pajak?>"><?php echo $namabulan[$list_masa_pajak] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input style="font-size: 11px" type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php echo date('Y');?>" >                
                      </div>
                    </div>                              
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input type="text" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value=""  onchange="NamaUsaha(this.value);" >                  
                      </div>
                    </div>             
                    <div class="form-group"> 
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Jenis Pajak<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select id="JENIS_PAJAK1" name="JENIS_PAJAK"  placeholder="Masa Pajak" class="form-control select2 form-control select2 col-md-7 col-xs-12" required onchange="jns_pajak(this.value);">
                            <option value="">Pilih</option>
                       </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:11px" id="NAMA_USAHA" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="form_entry(this.value);">
                          <option value="">Pilih</option>
                        </select>               
                      </div>
                    </div>
                    <input type="hidden" id="NAMA_WP" name="NAMA_WP" required>
                    <input type="hidden" id="ALAMAT_WP" required name="ALAMAT_WP">
                    <input type="hidden" id="ALAMAT_USAHA2" required name="ALAMAT_USAHA">
                    <input type="hidden" id="FKECAMATAN" required name="KECAMATAN">
                    <input type="hidden" id="FKELURAHAN" required name="KELURAHAN">
                    <!-- <input type="hidden" id="FGOLONGAN" required name="GOLONGAN"> -->
                  </div>
                  <div class="col-md-6">
                    <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td width="19%"><b>NAMA WP</b></td>
                        <td><span id="VNAMA_WP"></span></td>
                      </tr>
                      <tr>
                        <td><b>ALAMAT WP</b></td>
                        <td><span id="VALAMAT_WP"></span></td>
                      </tr>
                      <tr>
                        <td><b>NAMA USAHA</b></td>
                        <td><span id="VNAMA_USAHA"></span></td>
                      </tr> 
                      <tr>
                        <td><b>ALAMAT USAHA</b></td>
                        <td><span id="VALAMAT_USAHA1"></span></td>
                      </tr>
                      <tr>
                        <td><b>KELURAHAN</b></td>
                        <td><span id="VKELURAHAN"></span></td>
                      </tr>
                      <tr>
                        <td><b>KECAMATAN</b></td>
                        <td><span id="VKECAMATAN"></span></td>
                      </tr>                        
                    </tbody>
                  </table>
                    <!-- <div class="col-md-6" >
                      
                    </div>
                    <div class="col-md-6">
                      
                    </div> -->
                  </div> 
                  <div id="opsi_form_entry" class="col-md-12 "></div>
                </div>
                <!-- x content -->
              </div>
            </div> 
                 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



</div>
<script>

    function NamaUsaha(id){
                //alert(id);
      var npwpd = $("#NPWPD").val();
      $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);
                $("#ALAMAT_USAHA2").val("");
                $("#FKECAMATAN").val("");
                $("#FKELURAHAN").val("");

                $("#VNAMA_WP").html(exp[0]);
                $("#VALAMAT_WP").html(exp[1]);
                $("#VNAMA_USAHA").html("PILIH NAMA USAHA");  
                $("#VALAMAT_USAHA1").html("");
                $("#VKELURAHAN").html("");
                $("#VKECAMATAN").html("");
                
                
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null); 
                $("#VNAMA_WP").html("BELUM TERDAFTAR");
                $("#VALAMAT_WP").html("BELUM TERDAFTAR");
                
              }
              
            }
          }); 
         //alert(data2[0]);
         npwp=id;
         $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Wp/Data_tagihan/get_jenis_pajak'?>",
           data: { npwp: npwp},
           cache: false,
           success: function(msga){
                  //alert(npwp);
                  $("#JENIS_PAJAK1").html(msga);
                }
              });  
    }
    function jns_pajak(value){  
          var data1=value; 
         // alert(data1);
          //data2=data1.split('|'); 
         //alert(data2[0]);
         //npwp=data2[0]
         $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Wp/Pendataan/get_nama_usaha'?>",
           data: { data1: data1},
           cache: false,
           success: function(msga){
                 //alert(msga);
                  $("#ALAMAT_USAHA2").val("");
                  $("#FKECAMATAN").val("");
                  $("#FKELURAHAN").val("");

                  $("#VALAMAT_USAHA1").html("");
                  $("#VNAMA_USAHA").html("PILIH NAMA USAHA");
                  $("#NAMA_USAHA").html(msga);
                  $("#VKELURAHAN").html("");
                  $("#VKECAMATAN").html("");
                }
          }); 
    }
    function form_entry(value){  
          var data1=value; 
         // alert (data1)
         $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Wp/Pendataan/get_alamat_usaha'?>",
           data: { data1: data1},
           cache: false,
           success: function(msga){
                 //alert(msga);
                 var exp = msga.split("|");
                  $("#ALAMAT_USAHA2").val(exp[1]);
                  $("#FKECAMATAN").val(exp[2]);
                  $("#FKELURAHAN").val(exp[4]);


                  $("#VALAMAT_USAHA1").html(exp[1]);
                  $("#VNAMA_USAHA").html(exp[0]);
                  $("#VKELURAHAN").html(exp[3]);
                  $("#VKECAMATAN").html(exp[5]);
                }
          });
          $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Wp/Pendataan/get_form_entry'?>",
           data: { data1: data1},
           cache: false,
           success: function(msga){
                 //alert(msga);
                  $("#opsi_form_entry").html(msga);
                }
          }); 
    }
  </script>
  <style type="text/css">
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
      padding: 8px 8px 8px 8px;
    }
  </style>
  <script src="<?php echo base_url().'gentelella/'?>build/swal/dist/sweetalert2.all.js"></script>
  <script src="<?php echo base_url().'gentelella/'?>build/swal/dist/sweetalert2.js"></script>
  <link rel="stylesheet" href="<?php echo base_url().'gentelella/'?>build/swal/dist/sweetalert2.css">

    <script src="<?php echo base_url().'gentelella/'?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="<?php echo base_url().'gentelella/'?>/vendors/bootstrap/dist/css/bootstrap.css" rel="stylesheet"> 
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url().'gentelella/'?>build/css/custom.min.css" rel="stylesheet">
  
    <!-- /Skycons -->


   