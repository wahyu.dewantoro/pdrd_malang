


              <div class="x_title">
                <h4><i class="fa fa-ship"></i> Data Pajak</h4>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-6">            
                <div class="col-md-12 col-sm-12">
                  <div style="font-size:12px">
                    <div class="x_content" >
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > No Berkas  <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <input type="text" id="NO_BERKAS" name="NO_BERKAS" required="required" placeholder="No Berkas" class="form-control col-md-7 col-xs-12" >
                            </div>
                          </div>                       
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >SIPA / SIPPAT  <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <input type="text" id="NO_SIPA_SIPPAT" name="NO_SIPA_SIPPAT" required="required" placeholder="No SIPA / SIPPAT" class="form-control col-md-7 col-xs-12" >                          
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >TMT <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <input type="text" id="tmt" name="TMT" required="required" placeholder="TMT" class="form-control col-md-7 col-xs-12" >                  
                            </div>
                          </div> 

                          <?php
                          $persen = "var a = new Array();\n";
                          ?>                
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan Objek Pajak <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <select style="font-size: 11px" id="GOLONGAN_OP" name="GOLONGAN_OP" required="required" class="form-control select2 col-md-7 col-xs-12" onchange="changegol(this.value)">
                                <option value="">Pilih</option>
                                <?php foreach ($golongan as $gol) { ?>

                                <option value="<?php echo $gol->ID_OP ?>"><?php echo $gol->DESKRIPSI ?></option>    
                                 <?php $persen .= "a['" . $gol->ID_OP . "'] = {a:'".addslashes($gol->PERSEN)."'};\n";} ?>
                              </select>

                            </select>  
                            <script type="text/javascript">  
                              <?php echo $persen;?>
                              function changegol(id){
                                document.getElementById('PAJAK').value = a[id].a;
                              }
                            </script>                 
                          </div>
                        </div> 
                        <?php
                        $s1d50 = "var s1 = new Array();\n";
                        $s51d500 = "var s51 = new Array();\n";
                        $s501d1000 = "var s501 = new Array();\n";
                        $s1001d2500 = "var s1001 = new Array();\n";
                        $s2501d5000 = "var s2501 = new Array();\n";
                        $s5001d7500 = "var s5001 = new Array();\n";
                        $s7500 = "var s7500 = new Array();\n";
                        ?>               
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-4 col-xs-12" for="" >Peruntukan  <sup>*</sup>
                          </label>

                          <div class="col-md-9">
                            <select style="font-size: 11px" id="PERUNTUKAN" name="PERUNTUKAN" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" onchange="changeValue(this.value)">
                              <option value="">Pilih</option>
                              <?php foreach ($peruntukan as $row) { ?>


                              <option value="<?php echo $row->NO ?>"><?php echo $row->JENIS." - ".$row->JENISPENGAMBILAN; ?></option>
                              <?php 

                              $s1d50 .= "s1['" . $row->NO . "'] = {s1:'".addslashes(number_format($row->S1D50,'0','','.'))."'};\n";
                              $s51d500 .= "s51['" . $row->NO . "'] = {s51:'".addslashes(number_format($row->S51D500,'0','','.'))."'};\n";
                              $s501d1000 .= "s501['" . $row->NO . "'] = {s501:'".addslashes(number_format($row->S501D1000,'0','','.'))."'};\n";
                              $s1001d2500 .= "s1001['" . $row->NO . "'] = {s1001:'".addslashes(number_format($row->S1001D2500,'0','','.'))."'};\n";
                              $s2501d5000 .= "s2501['" . $row->NO . "'] = {s2501:'".addslashes(number_format($row->S2501D5000,'0','','.'))."'};\n";
                              $s5001d7500 .= "s5001['" . $row->NO . "'] = {s5001:'".addslashes(number_format($row->S5001D7500,'0','','.'))."'};\n";
                              $s7500 .= "s7500['" . $row->NO . "'] = {s7500:'".addslashes(number_format($row->S7500D,'0','','.'))."'};\n";
                            } ?>
                          </select>
                        </div>
                      </div> 
                      <script type="text/javascript">  
                        <?php echo $s1d50; echo $s51d500;echo $s501d1000;echo $s1001d2500;echo $s2501d5000;echo $s5001d7500;echo $s7500;?>             
                        function changeValue(id){
                          document.getElementById('A').value = s1[id].s1;
                          document.getElementById('B').value = s51[id].s51;
                          document.getElementById('C').value = s501[id].s501;
                          document.getElementById('D').value = s1001[id].s1001;
                          document.getElementById('E').value = s2501[id].s2501;
                          document.getElementById('F').value = s5001[id].s5001;
                          document.getElementById('G').value = s7500[id].s7500;
                          CARA_PENGAMBILAN.value='';
                          $('#FORM_NON_METER').remove();
                          $('#FORM_METER').remove();
                          DPP.value='';
                          PAJAK_TERUTANG.value='';
                        }
                      </script>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Cara Pengambilan  <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select onchange="CaraPengambilan(this.value)" style="font-size: 11px" id="CARA_PENGAMBILAN" name="CARA_PENGAMBILAN" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <option value="Meter">Meter</option>
                            <option value="Non Meter">Non Meter</option>
                          </select>
                        </div>
                      </div> 
                      <div id="kontentdiag">
                      </div>                
                    </div>
                  </div>
                </div>                    
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                    <button type="submit" class="btn btn-primary" onClick="return validasi()"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>               
              </div> 
            </div>
          </div>
              <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak (%)
                          </label>
                          <div class="col-md-9">
                            <input type="text" id="PAJAK" name="PAJAK" required="required" placeholder="Pajak (%)" class="form-control col-md-7 col-xs-12"  readonly>                  
                          </div>
                        </div> 
                      <div class="form-group" style="font-size: 12px !important">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Harga Dasar
                        </label>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >1-50</span>
                            <input disabled="" id="A" type="text"  class="form-control col-md-2 col-xs-12" >
                          </div>                
                        </div>
                        <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon" >51-500</span>
                            <input disabled="" id="B" type="text"  class="form-control col-md-2 col-xs-12" >
                          </div>                
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >501-1000</span>
                            <input disabled="" id="C" type="text"  class="form-control col-md-2 col-xs-12">                
                          </div>                
                        </div>
                        <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon" >1001-2500</span>
                            <input disabled="" id="D" type="text"  class="form-control col-md-2 col-xs-12" >                
                          </div>                
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >2501-5000</span>
                            <input disabled="" id="E" type="text"  class="form-control col-md-2 col-xs-12">                
                          </div>                
                        </div>
                        <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon" >5001-7500</span>
                            <input disabled="" id="F" type="text"  class="form-control col-md-2 col-xs-12" >                
                          </div>                
                        </div>
                        <div class="col-md-3">         
                        </div>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >7500 <</span>
                            <input disabled="" id="G" type="text"  class="form-control col-md-2 col-xs-12">                
                          </div>                
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" >                
                          </div>
                        </div>  
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Yang Terutang <sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" >                
                          </div>
                        </div> 
                      </div>                
              </div>
                    

        </div>
</div>
<style>
label { font-size: 11px;}
textarea { font-size: 11px;}
.input-group span{ font-size: 11px;}
input[type='text'] { font-size: 11px; height:30px}
</style>
<script>
var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value));
   PAJAK.value=persen;
    function CaraPengambilan(id){
      //alert(id);
      if (id=='Meter') {
         $('#FORM_NON_METER').remove();
         $('#kontentdiag').load('/pdrd/Sptpdair/Air/meter/'); 
      }else if (id=='Non Meter') {
       
      }else{
         $('#FORM_METER').remove();
          $('#FORM_NON_METER').remove();
      };
    }
 /*$('#CARA_PENGAMBILAN').on('change', function() {
  if(this.value=='Meter'){
    $('#FORM_NON_METER').remove();
    $('#kontentdiag').load('/pdrd/Sptpdair/Air/meter/'); 
  } else if (this.value=='Non Meter') {
    $('#FORM_METER').remove();
    $('#kontentdiag').load('/pdrd/Sptpdair/Air/non_meter/'); 
  } else {
    $('#FORM_METER').remove();
    $('#FORM_NON_METER').remove();
  }
})*/
 function gantiTitikKoma(angka){
  return angka.toString().replace(/\./g,',');
}
function nominalFormat(angka){
  return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
}
function getNominal(angka){
  var nominal=gantiTitikKoma(angka);
  var indexKoma=nominal.indexOf(',');
  if (indexKoma==-1) {
    return ribuan(angka);
  } else {
    var ribu=nominal.substr(0,indexKoma);
    var koma=nominal.substr(indexKoma,3);
    return ribuan(ribu)+koma;        
  }
}
/* Fungsi */
function formatRupiah(angka, prefix)
{
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split = number_string.split(','),
  sisa  = split[0].length % 3,
  rupiah  = split[0].substr(0, sisa),
  ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}
function ribuan (angka)
{
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan  = reverse.match(/\d{1,3}/g);
  ribuan  = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}      

function validasi(){
  var npwpd=document.forms["demo-form2"]["NPWPD"].value;
  var number=/^[0-9]+$/; 
  if (npwpd==null || npwpd==""){
    swal("NPWPD Harus di Isi", "", "warning")
    return false;
  };
}


        </script>
