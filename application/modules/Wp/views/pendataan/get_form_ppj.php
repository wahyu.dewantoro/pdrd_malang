
  <div class="clearfix"></div>   
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-ship"></i>  Data Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >     
                    <div class="col-md-6">              
                      <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                        <div class="col-md-9">
                          <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" placeholder="No Formulir">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                        <div class="col-md-9">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar">
                            </i></span>
                            <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal1" value="<?php echo date('d/m/Y')?>" required >
                          </div>
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                        <div class="col-md-9">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar">
                            </i></span>
                            <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal1" value="<?php echo date('01/m/Y')?>" required >
                          </div>
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            
                          </select>
                        </div>
                      </div> --> 
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select  style="font-size:11px" id="KELURAHAN" name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>                         
                          </select>
                        </div>
                      </div>  -->
                      <?php
                      $persen = "var persen = new Array();\n";
                      $masa = "var masa = new Array();\n";
                      ?>   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN" name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                            <option  value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                            <?php 
                            $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                            $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                          } ?>
                        </select>
                      </div>
                    </div> 
                          <script type="text/javascript">  
                          <?php echo $persen;echo $masa;?>             
                          function changeValue(id){
                            document.getElementById('PAJAK').value = persen[id].persen;
                            document.getElementById('MASA').value = masa[id].masa;
                          }
                        </script>    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Cara Perhitungan <sup>*</sup>
                      </label>
                      <div class="col-md-3">
                        <select onchange="perhitungan(this.value)" style="font-size:11px" id="CARA_PERHITUNGAN" name="CARA_PERHITUNGAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <option  value="Formula">Formula</option>
                          <option  value="Selisih Kwh">Selisih Kwh</option>
                        </select>
                      </div>
                      <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                      <div class="col-md-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN"  > Entrian Sederhana
                          </label>
                        </div>
                      </div>
                    </div>   
                    <div id="DATA_PAJAK">
                    </div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                      </div>
                    </div>
                  </div>                  
                  <div class="col-md-6">        
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                      <div class="col-md-4">
                        <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12"  placeholder="Masa Pajak" >
                      </div>
                      <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                      <div class="col-md-4">
                        <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12"  placeholder="Pajak" onchange='get(this);'>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Pemakaian <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input  type="text" id="JUMLAH_PEMAKAIAN" name="JUMLAH_PEMAKAIAN" required="required" placeholder="Jumlah Pemakaian" class="form-control col-md-7 col-xs-12"  readonly="" 
                          <span class="input-group-addon">Kwh</span>
                        </div>
                      </div>
                    </div>     
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" >                
                        </div>
                      </div>  
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" >                
                        </div>
                      </div> 
                    </div> 
                  </div>
                </div>
              </div>     
            </div>
          </div>
      </div>
    </div>
  </div>



</div>


<script>

function validasi(){
  var npwpd=document.forms["demo-form2"]["NPWPD"].value;
  var number=/^[0-9]+$/; 
  if (npwpd==null || npwpd==""){
    swal("NPWPD Harus di Isi", "", "warning")
    return false;
  };
}
     
  function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  function perhitungan(id)
    {
      if (id=='Formula') {
        $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/formula/';?>'); 
      }else if (id=='Selisih Kwh') {
        $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/selisih/';?>'); 
      }else{
        $('#FORMULA').remove();
        $('#SELISIH').remove();
      };
      /*if(id=='Formula'){
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/formula/'.$id;?>'); 
    } else if (id=='Selisih Kwh') {
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/selisih/'.$id;?>'); 

    } else {
      
    }*/
    }  
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa  = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
  function ribuan (angka)
  {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }

  function gantiTitikKoma(angka){
    return angka.toString().replace(/\./g,',');
  }
  function nominalFormat(angka){
    return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
  }
  function getNominal(angka){
    var nominal=gantiTitikKoma(angka);
    var indexKoma=nominal.indexOf(',');
    if (indexKoma==-1) {
      return ribuan(angka);
    } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
    }
  }
</script>
<script>
  $(document).ready(function(){
    var date_input=$('input[name="date1"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    $('.tanggal1').datepicker({
      format: 'dd/mm/yyyy',
      container: container,
      todayHighlight: true,
      autoclose: true,
    })
  })
</script>

