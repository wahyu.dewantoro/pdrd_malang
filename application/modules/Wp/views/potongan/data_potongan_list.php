    <div class="page-title">
     <div class="title_left">
     <!--  <h3>Tracking Kode Biling</h3> -->
    </div>
    <div class="pull-right">
      
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    
      <div class="col-md-10 col-sm-12 col-xs-12">
      <form id="demo-form2" data-parsley-validate class="form-inline"  method="post" enctype="multipart/form-data" >
        <?php echo $this->session->flashdata('notif')?>
        <div class="x_panel">
          <div class="x_title">
                  <h4><i class="fa fa-plus"></i> Tambah Potongan</h4>
                  <div class="clearfix"></div>
                </div>
          <div class="x_content" >
            
              
                <div class="form-group">
                    Kode Biling
                </div>
                <div class="form-group">
                  <input id="KODE_BILING" type="text" name="kode_biling" required class="form-control" value="<?= $kode_biling?>" placeholder="xxxxxxx">
                </div>
                <a class="tampil btn btn-primary"><i class="fa fa-search"></i> Cari</a>
                <a href="<?php echo base_url('Wp/Potongan');?>" class="btn btn-success"><i class="fa fa-refresh"></i> Reset</a>

            </form>
          </div>
        </div>
      </div>

      <div class="col-md-10 col-sm-12 col-xs-12" id="KOSONG"> 
        <div class="x_panel">
        <div class="alert alert-danger" style="font-size: 15px;">
          <i class="fa fa-close kosong"></i>
        </div>
        </div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12" id="TERBAYAR"> 
        <div class="x_panel">
        <div class="alert alert-info" style="font-size: 15px;">
          <i class="fa fa-close terbayar"></i>
        </div>
        <div class="col-md-6">
            <table class="table table-user-information">
              <tbody>
                <tr>
                  <td width="19%"><b>Kode Biling</b></td>
                  <td>: <span id="KODE_BILINGS1"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Tgl Bayar</b></td>
                  <td>: <span id="DATE_BYR"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Tgl Dibuat</b></td>
                  <td>: <span id="TGL_PENETAPAN"></span> </td>
                </tr>                    
              </tbody>
            </table>
          </div>
        </div>
      </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="DATA">
          <div class="col-md-6">
            <table class="table table-user-information">
              <tbody>
                <tr>
                  <td width="19%"><b>Kode Biling</b></td>
                  <td>:<span id="KODE_BILINGS"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Nama Wp</b></td>
                  <td>:<span id="NAMA_WP"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Alamat Wp</b></td>
                  <td>:<span id="ALAMAT_WP"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Nama Usaha</b></td>
                  <td>:<span id="NAMA_USAHA"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Masa Pajak</b></td>
                  <td>:<span id="MASA"></span> </td>
                </tr>
                <tr>
                  <td width="19%"><b>Tahun Pajak</b></td>
                  <td>:<span id="TAHUN_PAJAK"></span> </td>
                </tr>                    
              </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="19%"><b>Ketetapan</b></td>
                            <td>:<span id="PAJAK_TERUTANG"></span> </td>
                          </tr>
                           <tr>
                            <td width="19%"><b>Denda</b></td>
                            <td>:<span id="DENDA"></span> </td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Potongan Denda</b></td>
                            <td>:<span id="POTONGAN"></span> </td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Jatuh Tempo</b></td>
                            <td>:<span id="JATUH_TEMPO"></span> </td>
                          </tr>                    
                        </tbody>
                      </table>
                      <span><a href="#"  data-toggle="modal" id="POT" class="btn btn-success" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Potongan</a></span>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
                  <h4><i class="fa fa-list"></i> Data Potongan</h4>
                  <div class="clearfix"></div>
                </div>
          <div class="x_content" >
              <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tgl Potongan</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Jenis Pajak</th>
              <th class="text-center">Pajak Terutang</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Tagihan</th>
              <th class="text-center">Status</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0; foreach ($list as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $rk_d->DATE_POTONGAN?></td>
                <td><?= $rk_d->KODE_BILING?></td>
                <td><?= $rk_d->NPWPD?></td>
                <td><?= $rk_d->NAMA_WP?></td>
                <td><?= $rk_d->NAMA_USAHA?></td>
                <td><?= $rk_d->ALAMAT_OP?></td>
                <td><?= $rk_d->NAMA_PAJAK?></td>
                <td align="right"><?=  number_format($rk_d->KETETAPAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk_d->DENDA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk_d->POTONGAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk_d->PAJAK_TERUTANG,'0','','.')?></td>
                <td><?php if ($rk_d->STATUS=='1') {
                  echo "Terbayar"; } else {echo "Belum Bayar";}
                ?></td>
              </tr>
              <?php  $no++;$tot+=$rk_d->PAJAK_TERUTANG; } ?>
              <tr>
                <td colspan="11"><b>TOTAL</b></td>
                <td align="right"><?=  number_format($tot,'0','','.')?></td>                                
              </tr>
            </tbody>
            </tbody>
          </table>
          </div>
    </div>
  
</div>



</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog"> 
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Potongan</h4>
        </div>
        <form class="form-inline"  method="post" action="<?php echo base_url('Wp/Potongan/tambah_potongan');?>" enctype="multipart/form-data" >
        <div class="modal-body">
        <input id="KODE_BILING1" type="hidden" name="kode_biling" required class="form-control" >
        <input id="JUMLAH_POTONGAN_SEBELUM" type="hidden" name="JUMLAH_POTONGAN_SEBELUM" required class="form-control">
        
        <table width="100%" class="table-bordered table-hover">
          <tr>
            <td align="center" width="3%">Persen Pot(%)</td>
            <td align="center" width="10%">Nominal Pot(Rp)</td>
            <td align="center" width="13%">Tagihan</td>
            <td align="center" width="14%">Nilai Akhir</td>
            <td align="center" width="20%">No SK</td>
            <td align="center" width="10%">Tgl SK</td>
          </tr>
          <tbody>
            <tr>
              <!-- <td><input required type="text" value="" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="JUMLAH_POTONGAN" id="JUMLAH_POTONGAN" autofocus class="form-control" required></td> -->
              <td><input required type="text" onchange='get(this);' id="PERSEN" autofocus class="form-control" required></td>
              <td><input required type="text" readonly id="NOMINAL_POTONGAN" name="JUMLAH_POTONGAN"  autofocus class="form-control" required></td>
              <td><input required type="text" readonly name="" id="POKOK"  class="form-control" required></td>
              <td><input required type="text" readonly id="NILAI_AKHIR" name="" autofocus class="form-control" required></td>
              <td><input required type="text" name="NOMOR_SK" autofocus class="form-control" required></td>
              <td><input required type="text" name="TGL_SK" autofocus class="form-control tanggal" required></td>
            </tr>
          </tbody>
        </table> 
        
                  <!-- <div class="col-md-6">
                      <div class="form-group">
                          <label> Jumlah Potongan </label>
                          <input required type="text" value="" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="JUMLAH_POTONGAN" id="JUMLAH_POTONGAN" autofocus class="form-control" required>
                      </div>
                  </div> -->
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" ><i class="fa fa-save"></i> Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>

<script type="text/javascript">
  $('#DATA').hide();
  $('#TERBAYAR').hide();
  $('#KOSONG').hide();

function get() {
      var PN=parseFloat(nominalFormat(document.getElementById("PERSEN").value))|| 0;
      var PK=parseFloat(nominalFormat(document.getElementById("POKOK").value))|| 0;

      var NP=parseFloat(PN)*parseFloat(PK)/100;
      var NA=parseFloat(Math.round(PK))-parseFloat(Math.round(NP));
      $("#NOMINAL_POTONGAN" ).val(String(Math.round(NP)).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
      $("#NILAI_AKHIR" ).val(String(Math.round(NA)).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
      //PAJAK_TERUTANG.value=getNominal(Math.round(dpp*PAJAK));
      //alert(NP);
      /*var N=parseFloat(nominalFormat(document.getElementById("NOMINAL").value))|| 0;
      var NON=parseFloat(nominalFormat(document.getElementById("PENERIMAAN_NON_KARCIS").value))|| 0;*/
      //alert(getNominal(D*H*B*3600/1000));
     /* var P=(L*N).toFixed(0);
      PENERIMAAN1.value= getNominal(P);
      PENERIMAAN_KARCIS.value= getNominal(P);
      var dpp=parseFloat(P)+parseFloat(NON);
      DPP.value= getNominal(dpp);
      PAJAK_TERUTANG.value= getNominal(dpp*0.3);*/
}
function gantiTitikKoma(angka){
        return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
        return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
      }
    }
/* PERSEN */
  var PERSEN = document.getElementById('PERSEN');
  PERSEN.addEventListener('keyup', function(e)
  {
    PERSEN.value = formatRupiah(this.value);
  });
    function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
function ribuan (angka)
{
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan  = reverse.match(/\d{1,3}/g);
  ribuan  = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}
  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
  }



function tandaPemisahTitik(b){
var _minus = false;
if (b<0) _minus = true;
b = b.toString();
b=b.replace(".","");
b=b.replace("-","");
c = "";
panjang = b.length;
j = 0;
for (i = panjang; i > 0; i--){
j = j + 1;
if (((j % 3) == 1) && (j != 1)){
c = b.substr(i-1,1) + "." + c;
} else {
c = b.substr(i-1,1) + c;
}
}
if (_minus) c = "-" + c ;
return c;
}

function numbersonly(ini, e){
if (e.keyCode>=49){
if(e.keyCode<=57){
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
ini.value = tandaPemisahTitik(b);
return false;
}
else if(e.keyCode<=105){
if(e.keyCode>=96){
//e.keycode = e.keycode - 47;
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
ini.value = tandaPemisahTitik(b);
//alert(e.keycode);
return false;
}
else {return false;}
}
else {
return false; }
}else if (e.keyCode==48){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==95){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==8 || e.keycode==46){
a = ini.value.replace(".","");
b = a.replace(/[^\d]/g,"");
b = b.substr(0,b.length -1);
if (tandaPemisahTitik(b)!=""){
ini.value = tandaPemisahTitik(b);
} else {
ini.value = "";
}

return false;
} else if (e.keyCode==9){
return true;
} else if (e.keyCode==17){
return true;
} else {
//alert (e.keyCode);
return false;
}

}
var bulan = [        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'];
      $(function(){
            $(document).on('click','.tampil',function(e){
                e.preventDefault();
               //alert($("#KODE_BILING").val());
                // $("#myModal").modal('show');
                $.get("<?= base_url().'Wp/Potongan/inquiry' ?>",
                    {KODE_BILING:$("#KODE_BILING").val() },
                    function(html){
                      var obj = $.parseJSON(JSON.stringify(html));
                      if (obj['STATUS']['ResponseCode']=='00')    {
                        if (obj['STATUS_BAYAR']==1) {
                          $('#TERBAYAR').show();
                          $('#DATA').hide();
                          $("#KODE_BILINGS1" ).html(obj['KODE_BILING']);
                          $("#DATE_BYR" ).html(obj['DATE_BAYAR']);
                          $("#TGL_PENETAPAN" ).html(obj['TGL_PENETAPAN']);
                          $( ".terbayar" ).text(obj['KODE_BILING']+" Sudah Dibayar ");
                        } else {
                          var masa_pajak= obj['MASA_PAJAK'];
                          $('#DATA').show();
                          $('#TERBAYAR').hide();
                          $("#KODE_BILINGS" ).html(obj['KODE_BILING']);
                          $("#NAMA_WP" ).html(obj['NAMA_WP']);
                          $("#ALAMAT_WP" ).html(obj['ALAMAT_WP']);
                          $("#NAMA_USAHA" ).html(obj['OBJEK_PAJAK']);
                          $("#MASA" ).html(bulan[obj['MASA_PAJAK']]);
                          $("#MASA_PAJAK" ).html(obj['MASA_PAJAK']);
                          $("#TAHUN_PAJAK" ).html(obj['TAHUN_PAJAK']);
                          $("#KETERANGAN" ).html(obj['KETERANGAN']);
                          $("#JENIS_PAJAK" ).html(obj['JENIS_PAJAK']);
                          $("#JATUH_TEMPO" ).html(obj['JATUH_TEMPO']);
                          $("#DENDA" ).html(String(obj['DENDA']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#POTONGAN" ).html(String(obj['POTONGAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#PAJAK_TERUTANG" ).html(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#JUMLAH_BAYAR" ).html(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#POKOK" ).val(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          //alert(val(obj['POTONGAN']));
                          $("#KODE_BILING1" ).val(obj['KODE_BILING']);
                          $("#JUMLAH_POTONGAN_SEBELUM" ).val(obj['POTONGAN']);
                          if (obj['POTONGAN']==0) {$('#POT').show();} else{$('#POT').hide();};
                          if (obj['POTONGAN']==0) {document.getElementById("JUMLAH_POTONGAN").disabled = false;} else{document.getElementById("JUMLAH_POTONGAN").disabled = true;};
                        }
                        $('#KOSONG').hide();
                      } else {

                        $('#KOSONG').show();
                        $('#TERBAYAR').hide();
                        $('#DATA').hide();
                        $( ".kosong" ).text(obj['KODE_BILING']+" Tidak di temukan" );
                      }
                    }
                );
            });
            $(document).on('click','.kirim',function(e){
                e.preventDefault();
                // $("#myModal").modal('show');
                $.ajax({
                 type: "POST",
                 url: "<?= base_url().'pembayaran/webservice/payment' ?>",
                 data: $('#forms').serialize(),
                 cache: false,
                 success: function(html){
                  // alert(JSON.stringify(html));
                      var obj = $.parseJSON(JSON.stringify(html));
                      if (obj['STATUS']['ResponseCode']=='14')    {                  
                         swal("Gagal",obj['STATUS']['ErrorDesc'],"error");
                      } else{
                        swal("Berhasil",obj['STATUS']['ErrorDesc'],"success")
                        .then((value) => {
                          window.location = "<?php echo base_url().'pembayaran/data'?>";
                        });
                        
                      }
                }
              });    
            });            
        });
</script>
  <style type="text/css">
  .modal-dialog {
    width: 800px;
    margin: 40px auto;
    margin-right: 80;
    z-index: 99999999999999999;
  }
  table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }

.form-inline .form-control {
     display: inline-block; 
     width: 100%; 
    vertical-align: middle}
  </style>