   
    <?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>

    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:28px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>

  <?php if($this->session->userdata('MS_ROLE_ID')==4) {?>  
  <div class="pull-right">
    <?php echo anchor('Sptpd_hotel/sptpd_hotel','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?>
  </div>
  <?php } ?>
</div> 

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <!-- <h4><i class="fa fa-user"></i> Identitas Wajib Pajak </h4> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                   <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <h3><span class="label label-primary">DATA PAJAK</span></h3>
                  <div class="x_content">
                       <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($list_masa_pajak as $list_masa_pajak){ ?>
                            <option <?php if(!empty($MASA_PAJAK)){ if($list_masa_pajak==$MASA_PAJAK){echo "selected";}} else {if($list_masa_pajak==date("m")){echo "selected";}}?> value="<?php echo $list_masa_pajak?>"><?php echo $namabulan[$list_masa_pajak] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input style="font-size: 11px" type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                
                      </div>
                    </div>                              
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input type="text" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $this->session->userdata('NIP'); ?>" readonly>                  
                      </div>
                    </div> 
                    <div class="form-group" style="background-color: #49EF1F">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Objek Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select style="font-size:11px" id="TEMPAT_USAHA"  name="TEMPAT_USAHA" placeholder="TEMPAT_USAHA" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($tu as $tu){ ?>
                          <option  value="<?php echo $tu->ID_INC."|".$tu->JENIS_PAJAK?>"><?php echo $tu->NAMA_USAHA.' ('.$tu->NAMA_PAJAK.')';?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /form input mask -->

              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <h3><span class="label label-primary">DATA WP</span></h3>
                  <div class="x_content">
                  <div class="col-md-6">
                    <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td width="50%"><b>NAMA WP</b></td>
                        <td><span id="VNAMA_WP"><?php echo $this->session->userdata('NAMA_WP'); ?></span></td>
                      </tr>
                      <tr>
                        <td><b>ALAMAT WP</b></td>
                        <td><span id="VALAMAT_WP"><?php echo $this->session->userdata('ALAMAT_WP'); ?></span></td>
                      </tr>
                      <tr>
                        <td><b>NAMA USAHA</b></td>
                        <td><span id="VNAMA_USAHA"></span></td>
                      </tr> 
                      <tr>
                        <td><b>ALAMAT USAHA</b></td>
                        <td><span id="VALAMAT_USAHA1"></span></td>
                      </tr>
                      <!-- <tr>
                        <td><b>KELURAHAN</b></td>
                        <td><span id="VKELURAHAN"></span></td>
                      </tr>
                      <tr>
                        <td><b>KECAMATAN</b></td>
                        <td><span id="VKECAMATAN"></span></td>
                      </tr>  -->                       
                    </tbody>
                  </table>
                  </div>
                  <input type="hidden" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $this->session->userdata('NAMA_WP'); ?>" readonly> 
                  <input type="hidden" id="ALAMAT_WP" name="ALAMAT_WP" required="required"  class="form-control col-md-7 col-xs-12" value="<?php echo $this->session->userdata('ALAMAT_WP'); ?>" readonly> 
                  <input readonly type="hidden" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="">
                  <input type="hidden" id="ALAMAT_USAHA" name="ALAMAT_USAHA" required="required"  class="form-control col-md-7 col-xs-12" value="<?php echo $this->session->userdata('ALAMAT_USAHA'); ?>" readonly>
                  <input type="hidden" id="KODEKEC1" name="KECAMATAN" required="required"  class="form-control col-md-7 col-xs-12"  readonly> 
                  <input type="hidden" id="KODEKEL1" name="KELURAHAN" required="required"  class="form-control col-md-7 col-xs-12"  readonly>                                  
                  
                  </div>
                </div>
              </div>
                </div>
              </div>
            </div> 
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <h3><span class="label label-primary">DATA SPTPD</span></h3>
                  <div class="x_content" id="data_pajak">
                    
                  </div>
                </div>
              </div>
          </div>
        </form>
      </div>
    </div>
  </div>



</div>
<script>
  function validasi(){
    var MASA_PAJAK=document.forms["demo-form2"]["MASA_PAJAK"].value;
    var TAHUN_PAJAK=document.forms["demo-form2"]["TAHUN_PAJAK"].value;
    var NPWPD=document.forms["demo-form2"]["NPWPD"].value;
    if (MASA_PAJAK==null || MASA_PAJAK==""){
      swal("Masa Pajak Harus di Isi", "", "warning")
      return false;
    };
    if (TAHUN_PAJAK==null || TAHUN_PAJAK==""){
      swal("Tahun Pajak Harus di Isi", "", "warning")
      return false;
    }; 
    if (NPWPD==null || NPWPD==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
    if (!NPWPD.match(number)){
      swal("NPWPD Harus Angka", "", "warning")
      return false;
    };

  }  

  $("#NPWPD").keyup(function(){
    var npwpd = $("#NPWPD").val();
    $.ajax({
      url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
      type: 'POST',
      data: "npwpd="+npwpd,
      cache: false,
      success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
  });     
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa  = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
  function ribuan (angka)
  {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }

  function gantiTitikKoma(angka){
    return angka.toString().replace(/\./g,',');
  }
  function nominalFormat(angka){
    return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
  }
  function getNominal(angka){
    var nominal=gantiTitikKoma(angka);
    var indexKoma=nominal.indexOf(',');
    if (indexKoma==-1) {
      return ribuan(angka);
    } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
    }
  }
  
  function getkel(sel)
  {
    var KODEKEC = sel.value;
    $.ajax({
     type: "POST",
     url: "<?php echo base_url().'Master/pokok/getkel'?>",
     data: { KODEKEC: KODEKEC},
     cache: false,
     success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
  }
  $('#TEMPAT_USAHA').on('change', function() {
    var x=this.value.split("|");
    var id=x[0];
    var jp=x[1];
    $.ajax({
      url: "<?php echo base_url().'Master/pokok/cektu1';?>",
      type: 'POST',
      data: "id="+id,
      cache: false,
      success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split('"');
                $("#NAMA_USAHA").val(exp[0]);  
                $("#ALAMAT_USAHA").val(exp[1]); 
                $("#VNAMA_USAHA").html(exp[0]);                 
                $("#VALAMAT_USAHA1").html(exp[1]); 
                $("#KODEKEC1").val(exp[2]);  
                $("#KODEKEL1").val(exp[3]);  
                // alert(msg);
              }else{
                $("#NAMA_USAHA").val(null);  
                $("#ALAMAT_USAHA").val(null);
                $("#VNAMA_USAHA").html(""); 
                $("#VALAMAT_USAHA1").html("");
                $("#KODEKEC1").val(null);  
                $("#KODEKEL1").val(null);  
              }
              
            }
          });
    if(jp=='01'){
      $('#demo-form2').attr('action', '<?php echo base_url().'Sptpd_hotel/preview';?>');
      $.ajax({
        url: "<?php echo base_url().'Wp/Beranda/hotel';?>",
        type: 'POST',
        data: "id="+id,
        cache: false,
        success: function(msg){
               //alert(msg);    
               $("#data_pajak").html(msg);          
            }
          });
    } else if (jp=='02') {
    $('#demo-form2').attr('action', '<?php echo base_url().'Sptpd_restoran/review';?>');
      $.ajax({
        url: "<?php echo base_url().'Wp/Beranda/restoran';?>",
        type: 'POST',
        data: "id="+id,
        cache: false,
        success: function(msg){
               //alert(msg);    
               $("#data_pajak").html(msg);          
            }
          });
  } else if (jp=='03') {
    $('#demo-form2').attr('action', '<?php echo base_url().'Sptpdhiburan/Hiburan/preview';?>');
      $.ajax({
        url: "<?php echo base_url().'Wp/Beranda/hiburan';?>",
        type: 'POST',
        data: "id="+id,
        cache: false,
        success: function(msg){
               //alert(msg);    
               $("#data_pajak").html(msg);          
            }
          });
  } else if (jp=='05') {
    $('#demo-form2').attr('action', '<?php echo base_url().'Sptpd_ppj/sptpd_ppj/preview';?>');
      $.ajax({
        url: "<?php echo base_url().'Wp/Beranda/ppj';?>",
        type: 'POST',
        data: "id="+id,
        cache: false,
        success: function(msg){
               //alert(msg);    
               $("#data_pajak").html(msg);          
            }
          });
  }else if (jp=='09') {
    $('#demo-form2').attr('action', '<?php echo base_url().'Sptpd_psb/sptpd_psb/preview';?>');
    //alert(jp); 
      $.ajax({
        url: "<?php echo base_url().'Wp/Beranda/psb';?>",
        type: 'POST',
        data: "id="+id,
        cache: false,
        success: function(msg){
               //alert(msg);    
               $("#data_pajak").html(msg);          
            }
          });
  }else if (jp=='06') {
    $('#demo-form2').attr('action', '<?php echo base_url().'Sptpd_mblbm/sptpd_mblbm/create_action';?>');
      $.ajax({
        url: "<?php echo base_url().'Wp/Beranda/mineral';?>",
        type: 'POST',
        data: "id="+id,
        cache: false,
        success: function(msg){
               //alert(msg);    
               $("#data_pajak").html(msg);          
            }
          });
  }else if (jp=='07') {
    $('#demo-form2').attr('action', '<?php echo base_url().'sptpd_parkir/sptpd_parkir/create_action';?>');
      $.ajax({
        url: "<?php echo base_url().'Wp/Beranda/parkir';?>",
        type: 'POST',
        data: "id="+id,
        cache: false,
        success: function(msg){
               //alert(msg);    
               $("#data_pajak").html(msg);          
            }
          });
  }else {
    $('#HOTEL').remove();
    $('#RESTORAN').remove();
    $('#HIBURAN').remove();
    $('#PPJ').remove();
    $('#PSB').remove();
    $('#MINERAL').remove();
    $('#PARKIR').remove();
  }
})    
</script>