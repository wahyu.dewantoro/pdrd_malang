
<div class="col-md-12 col-sm-12" id="HIBURAN">
  <div style="font-size:12px">
    <div class="x_title">
      <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
      <div class="clearfix"></div>
    </div>
    <div class="x_content" >     
      <div class="col-md-6">              
        <div class="form-group">
          <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
          <div class="col-md-9">
            <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"   placeholder="No Formulir">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
          <div class="col-md-9">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar">
              </i></span>
              <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required  >
            </div>
          </div>
        </div>  
        <div class="form-group">
          <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
          <div class="col-md-9">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar">
              </i></span>
              <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required  >
            </div>
          </div>
        </div>   
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Harga Tanda masuk <sup>*</sup>
          </label>
          <div class="col-md-9 col-sm-6 col-xs-12">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input onchange='get(this);'  type="text" id="HARGA_TANDA_MASUK" name="HARGA_TANDA_MASUK" required="required" placeholder="Harga Tanda masuk" class="form-control col-md-7 col-xs-12" value="<?php echo $HARGA_TANDA_MASUK; ?>">       </div>
            </div>
          </div>   
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <input onchange='get(this);'   type="text" id="JUMLAH" name="JUMLAH" required="required" placeholder="Jumlah" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH; ?>" >                
            </div>
          </div>  
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
            </label>
            <div class="col-md-3">
              <select  style="font-size:11px" id="DASAR_PENGENAAN"  name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                <option value="">Pilih</option>
                <option  <?php if($DASAR_PENGENAAN=='Omzet'){echo "selected";}?> value="Omzet">Omzet</option>
                <option  <?php if($DASAR_PENGENAAN=='Ketetapan'){echo "selected";}?> value="Ketetapan">Ketetapan</option>
                <option  <?php if($DASAR_PENGENAAN=='Karcis'){echo "selected";}?> value="Karcis">Karcis</option>
              </select>
            </div>
            <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
            <div class="col-md-3">
              <div class="checkbox">
                <label>
                  <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                </label>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-primary" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')">Simpan</button>
                <a href="<?php echo site_url('Sptpdhiburan/hiburan') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
              </div>
            </div>
          </div>   
        </div>                  
        <div class="col-md-6">       
          <!-- <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
            </label>
            <div class="col-md-9">
              <select readonly onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN"  name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                <option value="">Pilih</option>
                <?php foreach($kec as $kec){ ?>
                <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                <?php } ?>
              </select>
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
            </label>
            <div class="col-md-9">
              <select readonly  style="font-size:11px" id="KELURAHAN"  name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                <option value="">Pilih</option>
                <?php foreach($kel as $kel){ ?>
                <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                <?php } ?>                          
              </select>
            </div>
          </div>  -->
          <?php
          $persen = "var persen = new Array();\n";
          $masa = "var masa = new Array();\n";
          ?>   
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
            </label>
            <div class="col-md-9">
              <select readonly onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN"  name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                <option value="">Pilih</option>
                <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                <?php 
                $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
              } ?>
            </select>
          </div>
        </div> 
        <script type="text/javascript">  
          <?php echo $persen;echo $masa;?>             
          function changeValue(id){
            document.getElementById('PAJAK').value = persen[id].persen;
            document.getElementById('MASA').value = masa[id].masa;
          }
        </script>  
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Total <sup>*</sup>
          </label>
          <div class="col-md-9 col-sm-6 col-xs-12">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input readonly=""  type="text" id="JUMLAH_TOTAL" name="JUMLAH_TOTAL" required="required" placeholder="Jumlah Total" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_TOTAL; ?>" >                
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
          <div class="col-md-4">
            <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"   placeholder="Masa Pajak" >
          </div>
          <label class="control-label col-md-1 col-xs-12" > Pajak </label>
          <div class="col-md-4">
            <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"   placeholder="Pajak" onchange='get(this);'>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
          </label>
          <div class="col-md-9 col-sm-6 col-xs-12">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input  type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
            </div>
          </div>  
        </div> 
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
          </label>
          <div class="col-md-9 col-sm-6 col-xs-12">
            <div class="input-group">
              <span class="input-group-addon">Rp</span>
              <input  type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
            </div>
          </div> 
        </div> 
      </div>
    </div>
  </div>     
  <script>
    
    function get() {
      var HARGA_TANDA_MASUK= parseFloat(nominalFormat(document.getElementById("HARGA_TANDA_MASUK").value))||0;
      var JUMLAH= parseFloat(nominalFormat(document.getElementById("JUMLAH").value))||0;
      var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
      var dpp=HARGA_TANDA_MASUK*JUMLAH;
      JUMLAH_TOTAL.value=getNominal(HARGA_TANDA_MASUK*JUMLAH)  ;
      DPP.value=getNominal(dpp)  ;
      PAJAK_TERUTANG.value=getNominal(dpp*PAJAK)  ;
    }
    
    /* HARGA_TANDA_MASUK */
    var HARGA_TANDA_MASUK = document.getElementById('HARGA_TANDA_MASUK');
    HARGA_TANDA_MASUK.addEventListener('keyup', function(e)
    {
      HARGA_TANDA_MASUK.value = formatRupiah(this.value);
    });
    /* JUMLAH */
    var JUMLAH = document.getElementById('JUMLAH');
    JUMLAH.addEventListener('keyup', function(e)
    {
      JUMLAH.value = formatRupiah(this.value);
    });
        $('.tanggal').datepicker({
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      autoclose: true,
    })      
  </script>            
</div>