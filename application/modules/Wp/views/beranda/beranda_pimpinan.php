   
    <?php $namabulan=array(
      '01'=>'Januari',
      '02'=>'Februari',
      '03'=>'Maret',
      '04'=>'April',
      '05'=>'Mei',
      '06'=>'Juni',
      '07'=>'Juli',
      '08'=>'Agustus',
      '09'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>

    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:28px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3><b>
    DASHBOARD</b></h3>
  </div>
</div> 

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >
      <!-- /.row -->
      <div class="col-md-3">          
          <!-- /.box -->
        </div>
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <!-- <h4><i class="fa fa-user"></i> Identitas Wajib Pajak </h4> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="small-box bg-blue">
                          <h3 >Jumlah Pendataan Pajak bulan <?php echo $namabulan[date('m')];?> </h3>          
                        </div> 
                        <div class="x_content">
                              <?php if (count($jumlah_tagihan_perpajak)<='0') {
                                echo "<marquee direction='right' style='font-size:13px;'>Belum ada data masuk bulan ini</marquee>";
                              } else {?>
                                <?php foreach($jumlah_tagihan_perpajak as $row){?>
                                    <a href="<?php echo base_url('Wp/Beranda/Data_tagihan/'.acak($row->ID_INC));?>" class="animated flipInY col-sm-2 col-md-2 col-sm-6 col-xs-12">
                                      <div class="tile-stats <?php echo $row->WARNA;?>" style="">
                                        <div class="icon" style="color:#fff;"><i class="<?php echo $row->ICON;?>"></i></div>
                                        <div class="count">Rp. <?php echo number_format($row->TAGIHAN,'0','','.');?> <p></p></div>
                                        <h6><?php echo $row->NAMA_PAJAK;?>  <?php echo number_format($row->JUMLAH_DAFTAR,'0','','.');?> </h6>
                                      </div>
                                    </a>                              
                                <?php }?>
                              <?php }?> 
                                <!-- ./col -->                
                        </div>
                      </div>
                   </div>
                   <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="small-box bg-blue">
                          <h3 >Jumlah WP per Pajak</h3>          
                        </div>
                        <div class="x_content">
                        <?php foreach($jumlah_wp_perpajak as $row){?>                                    
                            <a href="<?php echo base_url('Wp/Beranda/wajibPajak/'.acak($row->ID_INC));?>" class="animated flipInY col-sm-2 col-md-2 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="<?php echo $row->ICON;?>"></i></div>
                                <div class="count"><?php echo number_format($row->JUMLAH,'0','','.');?><p></p></div>
                                <h6><?php echo $row->NAMA_PAJAK;?></h6>
                              </div>
                            </a>
                            <?php }?>                         
                        </div>
                      </div>
                   </div>
                   <div class="col-md-6 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="small-box bg-blue">
                          <h3 >Top 5 Pembayaran bulan ini</h3>          
                        </div>
                        <div class="x_content">
                            <div class="form-group">
                                  <select onchange="jns_pajak(this.value);" name="JENIS_PAJAK"  placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" required>
                                        <!-- <option value="">Pilih Jenis Pajak</option> -->
                                        <?php foreach($jp as $jns){ ?>
                                        <option  value="<?php echo $jns->ID_INC?>"
                                          <?php if (1==$jns->ID_INC) {echo "selected";} ?>><?php echo $jns->NAMA_PAJAK ?></option>
                                        <?php } ?>  
                                  </select>
                            </div>
                            <div id="DETAIL_TOP">
                              <table class="table table-striped jambo_table bulk_action besar">
                              <thead>
                                <tr>
                                  <th class="text-center" width="5%">No</th>
                                  <th class="text-center" >Nama WP</th>
                                  <th class="text-center" >JUMLAH PEMBAYARAN</th>
                                </tr>
                              </thead>
                              <tbody>
                               <?php if (count($list_data)>0) {
                                error_reporting(E_ALL^(E_NOTICE|E_WARNING));$no=1; foreach($list_data as $row){?>
                                    <tr>
                                        <td align="center"><b> <?php echo $no;?></b></td>
                                        <td><b> <?php echo ucwords($row->NAMA_WP);?></b></td>
                                        <td align="right"><b> RP. <?php echo number_format($row->PAJAK_TERUTANG,'0','','.')?></b></td>
                                    </tr>
                                    <?php $no++;}
                               } else {
                                 echo "<tr><td colspan='3'>TIDAK ADA DATA</td></tr>";
                               }?>
                               
                               
                                </tbody>
                            </table>
                            </div>
                        </div>
                      </div>
                   </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="small-box bg-blue">
                          <h3>Validasi WP Baru</h3>          
                        </div>
                        <div class="x_content">
                            <?php if ($wp_baru->WP_BARU=='0') {
                              echo "<marquee direction='right' style='font-size:13px;'>Belum ada data Wajib Pajak baru</marquee>";
                            } else {?>
                              <div class="animated flipInY col-sm-4 col-md-4 col-sm-6 col-xs-12"></div>                                 
                              <a href="<?php echo base_url('Master/pokok/pengajuan_npwpd');?>" class="animated flipInY col-sm-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="tile-stats">
                                  <div class="icon"><i class="fa fa-group"></i></div>
                                  <div class="count"><?php echo $wp_baru->WP_BARU;?><p></p></div>
                                  <h6>Wajib Pajak Baru</h6>
                                </div>
                              </a>
                            </div>
                            <?php }?>                      
                        </div>
                      </div>
                   </div>
                </div>
                <!-- <div class="x_content" >
                   <div class="col-md-6 col-sm-12 col-xs-12">
                      <div class="x_panel">
                          <h3><span class="label label-primary">DATA PAJAK</span></h3>
                        <div class="x_content">
                    
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                      <div class="x_panel">
                          <h3><span class="label label-primary">DATA WP</span></h3>
                        <div class="x_content">
                          <div class="col-md-6">
                            
                          </div>
                        </div>
                      </div>
                    </div>
                </div> -->
              </div>
            </div>             
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .small-box{
    border-radius:3px;
    margin-bottom: 3px;
  }
  h4{
    margin-top: 2px;
    margin-left: 4px;
  }
  h3{
    margin-left: 5px;
    margin-right: 5px;
  }
  body{
    color: #000;
  }
  .tile-stats{
    margin-bottom: 1px;
    margin-top: 4px;
    border:solid #45b721;
  }
  .tile-stats h3{
    color: #185DB6;
  }
  .tile-stats .count
  {
      font-size: 15px;
  }
  .tile-stats .icon{
    width: 20px;
    height: 100px;
    color: #d26221;
    /*right: 17px;*/
    left: 4px;
    top: 0px;
  }
  .tile-stats .icon i{
    font-size: 21px;
  }
  .tile-stats .count, .tile-stats h3, .tile-stats p{
    margin-left:35px;
  }
  h6, .h6{
    font-size: 16px;
    margin-left:3px;
  }
  h3{
    font-size: 19px;
  }
  th{
    font-size: 17px;
  }
  td{
    font-size: 16px;
  }
  .bg-hotel{
    background: #66c0ca !important;
    border: 6px solid #66c0ca !important;
    color: #fff;
  }
  .bg-hiburan{
    background: #8c74d4 !important;
    border: 6px solid #8c74d4 !important;
    color: #fff;
  }
  .bg-restoran{
    background: #6abb56 !important;
    border: 6px solid #6abb56 !important;
    color: #fff;
  }
  .bg-reklame{
    background: #a9a53f !important;
    border: 6px solid #a9a53f !important;
    color: #fff;
  }
  .bg-ppj{
    background: #da7f49fa !important;
    border: 6px solid #da7f49fa !important;
    color: #fff;
  }
  .bg-minerba{
    background: #8e4c5c !important;
    border: 6px solid #8e4c5c !important;
    color: #fff;
  }
  .bg-parkir{
    background: #e45429 !important;
    border: 6px solid #e45429 !important;
    color: #fff;
  }
  .bg-air{
    background: #58a5e6 !important;
    border: 6px solid #58a5e6 !important;
    color: #fff;
  }
  .bg-sarang{
    background: #5a799a !important;
    border: 6px solid #5a799a !important;
    color: #fff;
  }
  h4, .h4, h5, .h5, h6, .h6 {
    margin-top:11px;
    margin-bottom: 0px;
  }
  
</style>
<script type="text/javascript">
  function jns_pajak(value){  
          var data1=value; 
         //alert(data1);
          //data2=data1.split('|'); 
         //alert(data2[0]);
         //npwp=data2[0]
         $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Wp/Beranda/ajax_top'?>",
           data: { data1: data1},
           cache: false,
           success: function(msga){
                 //]alert(msga);
                  $("#DETAIL_TOP").html(msga);
                }
          }); 
    }

</script>