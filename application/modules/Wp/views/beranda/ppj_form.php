<div id="PPJ">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >     
                    <div class="col-md-6">              
                      <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                        <div class="col-md-9">
                          <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"   placeholder="No Formulir">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                        <div class="col-md-9">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar">
                            </i></span>
                            <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required  >
                          </div>
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                        <div class="col-md-9">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar">
                            </i></span>
                            <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required  >
                          </div>
                        </div>
                      </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Cara Perhitungan <sup>*</sup>
                      </label>
                      <div class="col-md-3">
                        <select  style="font-size:11px" id="CARA_PERHITUNGAN"  name="CARA_PERHITUNGAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <option  <?php if($CARA_PERHITUNGAN=='Formula'){echo "selected";}?> value="Formula">Formula</option>
                          <option  <?php if($CARA_PERHITUNGAN=='Selisih Kwh'){echo "selected";}?> value="Selisih Kwh">Selisih Kwh</option>
                        </select>
                      </div>
                      <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                      <div class="col-md-3">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                          </label>
                        </div>
                      </div>
                    </div>   
                    <div id="DATA_PAJAK">
                      <?php if ($CARA_PERHITUNGAN=="Selisih Kwh") { ?>
 <div id="SELISIH">     
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Tarif/ TDL<sup>*</sup>
  </label>
  <div class="col-md-9 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);'  type="text" id="TARIF" name="TARIF" placeholder="Tarif/ TDL" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $TARIF?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Pemakaian Bulan Ini<sup>*</sup>
  </label>
  <div class="col-md-9 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);'  type="text" id="PEMAKAIAN_BULAN_INI" name="PEMAKAIAN_BULAN_INI" placeholder="Pemakaian Bulan Ini" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $PEMAKAIAN_BULAN_INI?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div> 
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Pemakaian Bulan Lalu<sup>*</sup>
  </label>
  <div class="col-md-9 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);'  type="text" id="PEMAKAIAN_BULAN_LALU" name="PEMAKAIAN_BULAN_LALU" placeholder="Pemakaian Bulan Lalu" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $PEMAKAIAN_BULAN_LALU?>" >
      <span class="input-group-addon">Kwh</span>
    </div>
  </div>
</div>         
<script>

    function get() {
      var PEMAKAIAN_BULAN_INI= parseFloat(nominalFormat(document.getElementById("PEMAKAIAN_BULAN_INI").value))||0;
      var PEMAKAIAN_BULAN_LALU= parseFloat(nominalFormat(document.getElementById("PEMAKAIAN_BULAN_LALU").value))||0;
      var TARIF= parseFloat(nominalFormat(document.getElementById("TARIF").value))||0; 
      var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100 ||0; 
      DPP.value=getNominal((PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)*TARIF)  ;
      PAJAK_TERUTANG.value=getNominal((PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)*TARIF*PAJAK)  ;
      JUMLAH_PEMAKAIAN.value=getNominal(PEMAKAIAN_BULAN_INI-PEMAKAIAN_BULAN_LALU)  ;
    }
    
    /* TARIF */
    var TARIF = document.getElementById('TARIF');
    TARIF.addEventListener('keyup', function(e)
    {
      TARIF.value = formatRupiah(this.value);
    });
    /* PEMAKAIAN_BULAN_LALU */
    var PEMAKAIAN_BULAN_LALU = document.getElementById('PEMAKAIAN_BULAN_LALU');
    PEMAKAIAN_BULAN_LALU.addEventListener('keyup', function(e)
    {
      PEMAKAIAN_BULAN_LALU.value = formatRupiah(this.value);
    });
    /* PEMAKAIAN_BULAN_INI */
    var PEMAKAIAN_BULAN_INI = document.getElementById('PEMAKAIAN_BULAN_INI');
    PEMAKAIAN_BULAN_INI.addEventListener('keyup', function(e)
    {
      PEMAKAIAN_BULAN_INI.value = formatRupiah(this.value);
    });
  $('#KEGUNAAN').on('change', function() {

    var exp = this.value.split("|");
    $("#JAM_OPERASI").val(exp[1]);
  })
  $('#KEPERLUAN').on('change', function() {

    var exp = this.value.split("|");
    $("#PAJAK").val(exp[1]);
  })
</script>

         </div>     
                     <?php } // selisih
elseif ($CARA_PERHITUNGAN=="Formula") { ?>
 <div id="FORMULA">
              
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kegunaan <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select  style="font-size:11px" id="KEGUNAAN" name="KEGUNAAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEGUNAAN=="Utama"){echo "selected";} ?> value="Utama|240">Utama</option>
      <option <?php if($KEGUNAAN=="Cadangan"){echo "selected";} ?> value="Cadangan|120">Cadangan</option>
      <option <?php if($KEGUNAAN=="Darurat"){echo "selected";} ?> value="Darurat|30">Darurat</option>
    </select>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Keperluan <sup>*</sup>
  </label>
  <div class="col-md-3">
    <select  style="font-size:11px" id="KEPERLUAN" name="KEPERLUAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
      <option value="">Pilih</option>
      <option <?php if($KEPERLUAN=="Industri"){echo "selected";} ?> value="Industri|3">Industri</option>
      <option <?php if($KEPERLUAN=="Non-Industri"){echo "selected";} ?> value="Non-Industri|5">Non Industri</option>
    </select>
  </div>                      
</div>   
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jam Operasi <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);' readonly="" type="text" id="JAM_OPERASI" name="JAM_OPERASI" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $JAM_OPERASI; ?>" >
      <span class="input-group-addon">Jam</span>
    </div>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tarif / TDL <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <span class="input-group-addon">Rp</span>
      <input onchange='get(this);' type="text" id="TARIF" name="TARIF" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $TARIF; ?>" >
    </div>
  </div>
</div>                     
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Daya Terpasang <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);' type="text" id="DAYA_TERPASANG" name="DAYA_TERPASANG" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $DAYA_TERPASANG; ?>" >
      <span class="input-group-addon">Kva</span>
    </div>
  </div>
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Faktor Daya <sup>*</sup>
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="input-group">
      <input onchange='get(this);' type="text" id="FAKTOR_DAYA" name="FAKTOR_DAYA" required="required"  class="form-control col-md-7 col-xs-12" value="<?php echo $FAKTOR_DAYA; ?>" >
      <span class="input-group-addon">Kva</span>
    </div>
  </div>
</div>  
<script>

    function get() {
      var JAM_OPERASI= parseFloat(nominalFormat(document.getElementById("JAM_OPERASI").value))||0;
      var FAKTOR_DAYA= parseFloat(nominalFormat(document.getElementById("FAKTOR_DAYA").value))||0;
      var DAYA_TERPASANG= parseFloat(nominalFormat(document.getElementById("DAYA_TERPASANG").value))||0; 
      var TARIF= parseFloat(nominalFormat(document.getElementById("TARIF").value))||0; 
      var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100 ||0; 
      DPP.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG*TARIF)  ;
      PAJAK_TERUTANG.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG*TARIF*PAJAK)  ;
      JUMLAH_PEMAKAIAN.value=getNominal(JAM_OPERASI*FAKTOR_DAYA*DAYA_TERPASANG)  ;
    }
    
    /* TARIF */
    var TARIF = document.getElementById('TARIF');
    TARIF.addEventListener('keyup', function(e)
    {
      TARIF.value = formatRupiah(this.value);
    });
    /* DAYA_TERPASANG */
    var DAYA_TERPASANG = document.getElementById('DAYA_TERPASANG');
    DAYA_TERPASANG.addEventListener('keyup', function(e)
    {
      DAYA_TERPASANG.value = formatRupiah(this.value);
    });
    /* FAKTOR_DAYA */
    var FAKTOR_DAYA = document.getElementById('FAKTOR_DAYA');
    FAKTOR_DAYA.addEventListener('keyup', function(e)
    {
      FAKTOR_DAYA.value = formatRupiah(this.value);
    });
  $('#KEGUNAAN').on('change', function() {

    var exp = this.value.split("|");
    $("#JAM_OPERASI").val(exp[1]);
  })
  $('#KEPERLUAN').on('change', function() {

    var exp = this.value.split("|");
    $("#PAJAK").val(exp[1]);
  })
</script>

         </div>     

<?php }?>

                    </div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
<!--                         <button type="submit" class="btn btn-primary" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')">Simpan</button> -->
                        <button type="submit" class="btn btn-primary">Preview</button>
                        <a href="<?php echo site_url('Sptpd_ppj/sptpd_ppj') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                      </div>
                    </div>
                  </div>                  
                  <div class="col-md-6">  
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select readonly  onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN"  name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($kec as $kec){ ?>
                            <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select readonly   style="font-size:11px" id="KELURAHAN"  name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($kel as $kel){ ?>
                            <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                            <?php } ?>                          
                          </select>
                        </div>
                      </div>  -->
                      <?php
                      $persen = "var persen = new Array();\n";
                      $masa = "var masa = new Array();\n";
                      ?>   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select  readonly onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN"  name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                            <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                            <?php 
                            $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                            $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                          } ?>
                        </select>
                      </div>
                    </div> 
                    <script type="text/javascript">  
                      <?php echo $persen;echo $masa;?>             
                      function changeValue(id){
                        document.getElementById('PAJAK').value = persen[id].persen;
                        document.getElementById('MASA').value = masa[id].masa;
                      }
                    </script>      
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                      <div class="col-md-4">
                        <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"   placeholder="Masa Pajak" >
                      </div>
                      <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                      <div class="col-md-4">
                        <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"   placeholder="Pajak" onchange='get(this);'>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Pemakaian <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input  type="text" id="JUMLAH_PEMAKAIAN" name="JUMLAH_PEMAKAIAN" required="required" placeholder="Jumlah Pemakaian" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_PEMAKAIAN; ?>" readonly="" >
                          <span class="input-group-addon">Kwh</span>
                        </div>
                      </div>
                    </div>     
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                        </div>
                      </div>  
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                        </div>
                      </div> 
                    </div> 
                  </div>
                </div>
              </div>     
            </div>

<script>
  
  $('#CARA_PERHITUNGAN').on('change', function() {
    if(this.value=='Formula'){
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/formula/'.$id;?>'); 
    } else if (this.value=='Selisih Kwh') {
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_ppj/sptpd_ppj/selisih/'.$id;?>'); 

    } else {
      $('#FORMULA').remove();
      $('#SELISIH').remove();
    }
  }) 
        $('.tanggal').datepicker({
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      autoclose: true,
    })    
</script>
</div>            