<div id="PSB">

            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak psb</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">              
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"   placeholder="No Formulir">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required  >
                        </div>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required  >
                        </div>
                      </div>
                    </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jenis Pengelolaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_PENGELOLAAN"  name="JENIS_PENGELOLAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($JENIS_PENGELOLAAN=='Pengusahaan'){echo "selected";}?> value="Pengusahaan">Pengusahaan</option>
                        <option  <?php if($JENIS_PENGELOLAAN=='Habitat Alami'){echo "selected";}?> value="Habitat Alami">Habitat Alami</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Burung</label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="JENIS_BURUNG"  name="JENIS_BURUNG"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($JENIS_BURUNG=='Walet'){echo "selected";}?> value="Walet">Walet</option>
                        <option  <?php if($JENIS_BURUNG=='Sriti'){echo "selected";}?> value="Sriti">Sriti</option>
                      </select>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kepemilikan Ijin <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="KEPEMILIKAN_IJIN"  name="KEPEMILIKAN_IJIN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($KEPEMILIKAN_IJIN=='Ijin'){echo "selected";}?> value="Ijin">Ijin</option>
                        <option  <?php if($KEPEMILIKAN_IJIN=='Belum Ijin'){echo "selected";}?> value="Belum Ijin">Belum Ijin</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                        </label>
                      </div>
                    </div>
                  </div>  
                  <div id="DATA_PAJAK"> 
<?php if ($KEPEMILIKAN_IJIN=='Ijin') { ?>
<div id="IJIN">

  <div class="form-group">
    <label class="control-label col-md-3 col-xs-12" > Dari </label>
    <div class="col-md-3">
      <input type="text" name="DARI" id="DARI"  class="form-control col-md-7 col-xs-12" value="<?php echo $DARI; ?>"  placeholder="Dari" >
    </div>
    <label class="control-label col-md-3 col-xs-12" > Nomor </label>
    <div class="col-md-3">
      <input type="text" name="NOMOR" id="NOMOR"   class="form-control col-md-7 col-xs-12" value="<?php echo $NOMOR; ?>"  placeholder="Nomor" >
    </div>
  </div>
  
</div>
<?php } ?>


                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Luas Area </label>
                    <div class="col-md-3">
                      <input type="text" name="LUAS_AREA" id="LUAS_AREA" class="form-control col-md-7 col-xs-12" value="<?php echo $LUAS_AREA; ?>"   placeholder="Luas Area" >
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Jumlah Galur </label>
                    <div class="col-md-3">
                      <input type="text" name="JUMLAH_GALUR" id="JUMLAH_GALUR" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_GALUR; ?>"   placeholder="Jumlah Galur" >
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dipanen Setiap <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="DIPANEN_SETIAP"  name="DIPANEN_SETIAP"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($DIPANEN_SETIAP=='4 Bulan Sekali'){echo "selected";}?> value="4 Bulan Sekali">4 Bulan Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='6 Bulan Sekali'){echo "selected";}?> value="6 Bulan Sekali">6 Bulan Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='1 Tahun Sekali'){echo "selected";}?> value="1 Tahun Sekali">1 Tahun Sekali</option>
                        <option  <?php if($DIPANEN_SETIAP=='Lainnya'){echo "selected";}?> value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Hasil Panen </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="HASIL_PANEN"  name="HASIL_PANEN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($HASIL_PANEN=='Dipasarkan Sendiri'){echo "selected";}?> value="Dipasarkan Sendiri">Dipasarkan Sendiri</option>
                        <option  <?php if($HASIL_PANEN=='Disetrokan ke Asosiasi'){echo "selected";}?> value="Disetrokan ke Asosiasi">Disetrokan ke Asosiasi</option>
                        <option  <?php if($HASIL_PANEN=='Diambil Tengkulak'){echo "selected";}?> value="Diambil Tengkulak">Diambil Tengkulak</option>
                        <option  <?php if($HASIL_PANEN=='Lainnya'){echo "selected";}?> value="Lainnya">Lainnya</option>
                      </select>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Hasil Setiap Panen </label>
                    <div class="col-md-3">
    <div class="input-group">
                      <input  onchange='get(this);'   type="text" name="HASIL_SETIAP_PANEN" id="HASIL_SETIAP_PANEN" class="form-control col-md-7 col-xs-12" value="<?php echo $HASIL_SETIAP_PANEN; ?>"   placeholder="Hasil Setiap Panen" >
      <span class="input-group-addon">Kg</span></div>
                    </div>
                    <label class="control-label col-md-3 col-xs-12" > Harga Rata Rata </label>
                    <div class="col-md-3">
    <div class="input-group">
       <span class="input-group-addon">Rp</span>
                      <input   onchange='get(this);'  type="text" name="HARGA_RATA_RATA" id="HARGA_RATA_RATA" class="form-control col-md-7 col-xs-12" value="<?php echo $HARGA_RATA_RATA; ?>"   placeholder="Harga Rata Rata" >
                    </div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
<!--                       <button type="submit" class="btn btn-primary" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')">Simpan</button> -->
                      <button type="submit" class="btn btn-primary">Submit</button>
                      <a href="<?php echo site_url('Sptpd_psb/sptpd_psb') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                </div>                  
                <div class="col-md-6">    
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select readonly onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN"  name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select readonly  style="font-size:11px" id="KELURAHAN"  name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                      </div>
                    </div>  -->
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($PAJAK)){echo $PAJAK;} else { echo "5";}?>"   placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input  type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input  type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                      </div>
                    </div> 
                  </div> 
                </div>
              </div>
            </div>     
          </div>
<script>
  
  $('#KEPEMILIKAN_IJIN').on('change', function() {
    if(this.value=='Ijin'){
      $('#DATA_PAJAK').load('<?php echo base_url().'Sptpd_psb/sptpd_psb/ijin/'.$id;?>'); 
    } else {
      $('#IJIN').remove();
    }
  }) 
//LUAS_AREA 
var LUAS_AREA = document.getElementById('LUAS_AREA'),
numberPattern = /^[0-9]+$/;

LUAS_AREA.addEventListener('keyup', function(e) {
  LUAS_AREA.value = formatRupiah(LUAS_AREA.value);
});
//JUMLAH_GALUR 
var JUMLAH_GALUR = document.getElementById('JUMLAH_GALUR'),
numberPattern = /^[0-9]+$/;

JUMLAH_GALUR.addEventListener('keyup', function(e) {
  JUMLAH_GALUR.value = formatRupiah(JUMLAH_GALUR.value);
});
//HASIL_SETIAP_PANEN 
var HASIL_SETIAP_PANEN = document.getElementById('HASIL_SETIAP_PANEN'),
numberPattern = /^[0-9]+$/;

HASIL_SETIAP_PANEN.addEventListener('keyup', function(e) {
  HASIL_SETIAP_PANEN.value = formatRupiah(HASIL_SETIAP_PANEN.value);
});
//HARGA_RATA_RATA 
var HARGA_RATA_RATA = document.getElementById('HARGA_RATA_RATA'),
numberPattern = /^[0-9]+$/;

HARGA_RATA_RATA.addEventListener('keyup', function(e) {
  HARGA_RATA_RATA.value = formatRupiah(HARGA_RATA_RATA.value);
});
    function get() {
      var HARGA_RATA_RATA= parseInt(document.getElementById("HARGA_RATA_RATA").value.replace(/\./g,''))|| 0;
      var HASIL_SETIAP_PANEN= parseInt(document.getElementById("HASIL_SETIAP_PANEN").value.replace(/\./g,''))|| 0;
      var dpp=HARGA_RATA_RATA*HASIL_SETIAP_PANEN;
      DPP.value = ribuan(dpp);
      PAJAK_TERUTANG.value = ribuan(dpp*0.05);
    }
        $('.tanggal').datepicker({
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      autoclose: true,
    })      
</script>
  
</div>