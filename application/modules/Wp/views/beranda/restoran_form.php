
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">              
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"   placeholder="No Formulir">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required  >
                        </div>
                      </div>
                    </div>  
                   <!--  <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required  >
                        </div>
                      </div>
                    </div> -->
                 <input  type="hidden" id="JUMLAH_MEJA" name="JUMLAH_MEJA" value="<?php echo $JUMLAH_MEJA; ?>"> 
                 <input onchange='getq(this);'  type="hidden" id="MEJA_TERISI" name="MEJA_TERISI" value="<?php echo $MEJA_TERISI; ?>" >    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Omzet <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input onchange='get(this);'  type="text" id="TARIF_RATA_RATA" name="TARIF_RATA_RATA" required="required" placeholder="Tarif Rata-rata" class="form-control col-md-7 col-xs-12" value="<?php echo $TARIF_RATA_RATA; ?>" >                
                      </div>
                    </div>  
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="DASAR_PENGENAAN"  name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($DASAR_PENGENAAN=='Omzet'){echo "selected";}?> value="Omzet">Omzet</option>
                        <!-- <option  <?php if($DASAR_PENGENAAN=='Ketetapan'){echo "selected";}?> value="Ketetapan">Ketetapan</option> -->
                        <option  <?php if($DASAR_PENGENAAN=='Karcis'){echo "selected";}?> value="Karcis">Karcis</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                        </label>
                      </div>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Kategori SKPD </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" id="SKPD"  onclick='skpd();' value="1" name="JENIS_RESTO" <?php if($JENIS_RESTO=='1'){echo "checked=''";}?> >
                        </label>
                      </div>
                    </div>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
<!--                       <button type="submit" class="btn btn-primary" onClick="return confirm('APAKAH ANDA YAKIN DENGAN DATA YANG TELAH DIMASUKAN?')">Simpan</button> -->
                      <button type="submit" class="btn btn-primary">Submit</button>
                      <a href="<?php echo site_url('Sptpd_hotel/sptpd_hotel') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  </div>   
                </div>                  
                <div class="col-md-6">     
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select readonly onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN"  name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select readonly  style="font-size:11px" id="KELURAHAN"  name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                      </div>
                    </div> --> 
                    <?php
                    $persen = "var persen = new Array();\n";
                    $masa = "var masa = new Array();\n";
                    ?>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN"  name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                          <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                          <?php 
                          $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                          $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <script type="text/javascript">  
                    <?php echo $persen;echo $masa;?>             
                    function changeValue(id){
                      //alert(id);
                      if (id=='688') {
                          document.getElementById('SKPD').checked= true;
                          document.getElementById("test").innerHTML=''
                                       +'<input type="text" name="KEGIATAN" id="KEGIATAN" class="form-control col-md-12 col-xs-12" >'; 
                      } else {
                          document.getElementById('SKPD').checked= false;
                          document.getElementById("test").innerHTML='';
                      }
                      document.getElementById('PAJAK').value = persen[id].persen;
                      document.getElementById('MASA').value = masa[id].masa;
                    }
                  </script>       
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"   placeholder="Masa Pajak" >
                    </div>
                    <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"   placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input  type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input  type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                      </div>
                    </div> 
                  </div>
                  <div class="form-group" >
                      <label class="control-label col-md-3 col-xs-12"> Nama Kegiatan  <sup>*</sup></label>
                        <div class="col-md-9" id="test">
                          <!-- <input type="text" name="KEGIATAN" id="KEGIATAN" class="form-control col-md-12 col-xs-12"  required > -->
                        </div>
                  </div> 
                </div>
              </div>
            </div>
<script>
  
  function get() {
    var TARIF_RATA_RATA= parseFloat(nominalFormat(document.getElementById("TARIF_RATA_RATA").value))||0;
    var MEJA_TERISI= parseFloat(nominalFormat(document.getElementById("MEJA_TERISI").value))||0;
    var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100 || 0;
    var dpp=TARIF_RATA_RATA;
    DPP.value=getNominal(dpp)  ;
    //PAJAK_TERUTANG.value=getNominal(dpp*PAJAK);
    PAJAK_TERUTANG.value=getNominal(Math.round(dpp*PAJAK));
    } 
    /* Tanpa Rupiah */
    var JUMLAH_MEJA = document.getElementById('JUMLAH_MEJA');
    JUMLAH_MEJA.addEventListener('keyup', function(e)
    {
      JUMLAH_MEJA.value = formatRupiah(this.value);
    });
    /* Tanpa Rupiah */
    var TARIF_RATA_RATA = document.getElementById('TARIF_RATA_RATA');
    TARIF_RATA_RATA.addEventListener('keyup', function(e)
    {
      TARIF_RATA_RATA.value = formatRupiah(this.value);
    });     
    /* Tanpa Rupiah */
    var MEJA_TERISI = document.getElementById('MEJA_TERISI');
    MEJA_TERISI.addEventListener('keyup', function(e)
    {
      MEJA_TERISI.value = formatRupiah(this.value);
    }); 
        $('.tanggal').datepicker({
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      autoclose: true,
    })  
function skpd()
{
  if (document.getElementById('SKPD').checked) 
  {
       document.getElementById("test").innerHTML=''
      +'<input type="text" name="KEGIATAN" id="KEGIATAN" class="form-control col-md-12 col-xs-12" >';
  } else {
      document.getElementById("test").innerHTML=''
      +'<input type="hidden" name="KEGIATAN" id="KEGIATAN" class="form-control col-md-12 col-xs-12" >';
  }
}
</script>                 
          </div>