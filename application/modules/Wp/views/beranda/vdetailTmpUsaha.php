<div class="page-title">
     <div class="title_left">
        <h3>Detail Objek Pajak</h3>
      </div>
      <div class="pull-right">
      <?php echo anchor('Master/pokok/wajibPajak','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?>
    </div>
</div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <table class="table borderless table-hover" style="border: none !important">
                   <tbody>                 
                     <tr>
                       <td width="30"><b>NPWPD</b></td>
                       <td width="5"><b>:</b></td>
                       <td><b> <?php echo $wp->NPWP;?></b></td>
                     </tr>
                     <tr>
                       <td><b> Nama</b></td>
                       <td><b>:</b></td>
                       <td><b> <?php echo $wp->NAMA;?></b></td>
                     </tr>  
                     <tr>
                       <td><b>Alamat</b></td>
                       <td><b>:</b></td>
                       <td><b> <?php echo $wp->ALAMAT; ?></b></td>
                     </tr> 
                   </tbody>
                  </table>
                  <div class="x_content">
                    <table id="example2" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th width="5%">No</th>
                          <th>Jenis Pajak</th>
                          <th>Nama Usaha</th>
                          <th>Alamat</th>
                        </tr>
                      </thead>
                      <tbody>
                       <?php 
                          if (count($detail_objek_pajak)<=0) {
                             echo "<tr ><td colspan='4'>belum ada objek pajak</td></tr>";
                           } else {
                            $no=1; foreach($detail_objek_pajak as $rk){?>
                       <tr>
                           <td align="center"><?php echo $no?></td>
                           <td><?php echo $rk->NAMA_PAJAK?></td>
                           <td><?php echo $rk->NAMA_USAHA?></td>
                           <td><?php echo $rk->ALAMAT_USAHA?></td>                           
                       </tr>
                       <?php $no++; } }?>
                   </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
