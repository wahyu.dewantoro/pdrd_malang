<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tempat_usaha extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mtempat_usaha');
        $this->load->model('Sptpd_hotel/Msptpd_hotel');
        $this->load->model('Sptpd_restoran/Msptpd_restoran');
        $this->load->model('Sptpdhiburan/Mhiburan');
        $this->load->model('Sptpd_ppj/Msptpd_ppj');
        $this->load->model('Sptpdair/Mair');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
        $this->role =$this->session->userdata('MS_ROLE_ID');
    }
    public function index()
    {
      $this->template->load('Welcome/halaman','Wp/tempat_usaha/tempat_usaha_list');
  }
  public function json() {
    header('Content-Type: application/json');
    echo $this->Mtempat_usaha->json();
}
public function hapus()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $id=rapikan($id);
        $row = $this->Mtempat_usaha->get_by_id($id);
        
        if ($row) {
            $this->Mtempat_usaha->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Sptpd Hotel Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}
public function table()
{
    $this->load->view('Wp/tempat_usaha/tempat_usaha_table');
}	
public function create() 
{
    $data = array(
        'button'                       => 'Form Tambah Tempat Usaha',
        'action'                       => site_url('Wp/Tempat_usaha/create_action'),
        'disable'                      => '',
        'disable1'                     => '',
        'style'                       =>  'visibility:hidden',
        'ID_INC'                       => set_value('ID_INC'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'KELURAHAN'                    => set_value('KELURAHAN'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'JENIS_PAJAK'                  => set_value('JENIS_PAJAK'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'NPWPD'                        => set_value('NPWPD'), 
        "jumlah_kamar_meja"            => set_value('JUMLAH_KAMAR_MEJA'),
        'NAMA_WP'                      => $this->Mpokok->get_det_wp(0),
        'jp'                           => $this->Mtempat_usaha->ListJenisPajak(),
        'rek'                          => $this->Mtempat_usaha->ListRekeningPajak(),
        'kec'                          => $this->Mpokok->getKec(),

    );           
    $this->template->load('Welcome/halaman','Wp/tempat_usaha/tempat_usaha_form',$data);
}
public function create_action() 
{
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('JENIS_PAJAK', $this->input->post('JENIS_PAJAK',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('REKENING', $this->input->post('REKENING',TRUE));
    $this->db->set('DATE_INSERT', "SYSDATE",false);
    $this->db->set('JUMLAH_KAMAR_MEJA', $this->input->post('qtt_kamar_meja',TRUE));
    
    if ($this->role==8) {
        $this->db->set('NPWPD',$this->nip);
    } else {
        $this->db->set('NPWPD',$this->input->post('NPWPD',TRUE));
    }    
    $this->db->insert('TEMPAT_USAHA'); 
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Tambah ", "", "success")
     });

     </script>');
    redirect(site_url('Wp/tempat_usaha'));
}
public function delete($id) 
{
    $row = $this->Msptpd_hotel->get_by_id($id);

    if ($row) {
        $this->Msptpd_hotel->delete($id);
        $this->db->query("commit");
        $this->session->set_flashdata('message', '<script>
          $(window).load(function(){
             swal("Berhasil Hapus Supplier", "", "success")
         });

         </script>');
        redirect(site_url('Sptpd/sptpd_hotel'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('Sptpd/sptpd_hotel'));
    }
} 
public function update($id) 
{
    $id=rapikan($id);
    $row = $this->Mtempat_usaha->get_by_id($id);

    if ($row) {
        $data = array(
            'button'              => 'Update '.set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'action'              => site_url('Wp/tempat_usaha/update_action'),
            'disable'             => '',
            'disable1'            => 'disabled',
            'style'               =>  'visibility:visible',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'JENIS_PAJAK'         => set_value('JENIS_PAJAK',$row->JENIS_PAJAK),
            'GOLONGAN'            => set_value('GOLONGAN',$row->ID_OP),
            'REKENING'            => set_value('REKENING',$row->REKENING),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'rek'                 => $this->Mtempat_usaha->get_rekID($row->REKENING,$row->JENIS_PAJAK),
            'kel'                 => $this->Mtempat_usaha->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'jp'                  => $this->Mtempat_usaha->ListJenisPajak(),
            'gol'                 => $this->Mtempat_usaha->getGolId($row->ID_OP),
            'NAMA_WP'             => $this->Mpokok->get_det_wp($row->NPWPD),
            //'gol1'                => $this->db->query($row->NPWPD),
            "jumlah_kamar_meja"   => set_value('JUMLAH_KAMAR_MEJA',$row->JUMLAH_KAMAR_MEJA),
        );
        $ID_INC=$row->JENIS_PAJAK;
        //HOTEL
        if ($ID_INC=='01') {

        $data['gol']=$this->Msptpd_hotel->getGol();
        }
        //RESTORAN
        if ($ID_INC=='02') {

        $data['gol']=$this->Msptpd_restoran->getGol();;
        }
        //HIBURAN
        if ($ID_INC=='03') {

        $data['gol']=$this->Mhiburan->getGol();
        }
        //PPJ
        if ($ID_INC=='05') {

        $data['gol']=$this->Msptpd_ppj->getGol();
        }
        //Parkir
        if ($ID_INC=='07') {

        $data['gol']=$this->db->query("SELECT ID_OP, ID_GOL,MASA,PERSEN,DESKRIPSI,TARIF FROM OBJEK_PAJAK WHERE ID_OP=456 ")->result();
        }
        if ($ID_INC=='08') {

        $data['gol']=$this->Mair->Gol();
        }
        $this->template->load('Welcome/halaman','Wp/tempat_usaha/tempat_usaha_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
} 
public function update_action() 
{
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('JENIS_PAJAK', $this->input->post('JENIS_PAJAK',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('REKENING', $this->input->post('REKENING',TRUE));
    $this->db->set('JUMLAH_KAMAR_MEJA', $this->input->post('qtt_kamar_meja',TRUE));
    //$this->db->set('NPWPD',$this->nip);

    $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
    $this->db->update('TEMPAT_USAHA');
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Update ", "", "success")
     });

     </script>');
    redirect(site_url('Wp/tempat_usaha'));
}   
function detail($id){
    $id=rapikan($id);
    $row = $this->Mtempat_usaha->get_by_id($id);
    if ($row) {
        $data = array(
            'button'              => 'Detail Tempat Usaha',
            'action'              => site_url('Wp/tempat_usaha/update_action'),
            'disable'             => 'disabled',
            'disable1'            => '',
            'style'               =>  'visibility:visible',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'JENIS_PAJAK'         => set_value('JENIS_PAJAK',$row->JENIS_PAJAK),
            'GOLONGAN'            => set_value('GOLONGAN',$row->ID_OP),
            'REKENING'            => set_value('REKENING',$row->REKENING),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            "jumlah_kamar_meja"   => set_value('JUMLAH_KAMAR_MEJA',$row->JUMLAH_KAMAR_MEJA),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'rek'                 => $this->Mtempat_usaha->get_rekID($row->REKENING,$row->JENIS_PAJAK),
            'kel'                 => $this->Mtempat_usaha->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'jp'                  => $this->Mtempat_usaha->ListJenisPajak(),
            'gol'                 => $this->Mtempat_usaha->getGolId($row->ID_OP),
            'NAMA_WP'             => $this->Mpokok->get_det_wp($row->NPWPD),
        );

        $this->template->load('Welcome/halaman','Wp/tempat_usaha/tempat_usaha_form', $data);
    } else {

        redirect(site_url('Sptpd_hotel/sptpd_hotel'));
    }
}       
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */