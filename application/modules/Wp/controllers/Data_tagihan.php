<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_Tagihan extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mprofil');
        $this->load->model('Mtempat_usaha');
        $this->load->model('MTagihan');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');   
    }
    public function index()
    {
        if ($this->session->userdata('MS_ROLE_ID')==8 OR $this->session->userdata('MS_ROLE_ID')==81) {
            if(isset($_GET['tu'])&&isset($_GET['tahun'])){
                $tahun    =$_GET['tahun'];
                $kodeusaha      =$_GET['tu'];
               // $ex=explode("-",$tu);
                //$kodeusaha= $tu;
                //$jenisPajak= $ex[1];
                $rk="tampil";
            }else{
                $tahun=date("Y");;
                $tu='';
                $rk ="";
                //$jenisPajak= "";
                //$namaUsaha= "";
                $kodeusaha= "";
            }
            $data = array(
                'TU'                           => $this->Mtempat_usaha->ListTuByNip(),
                'profil'                       => $this->Mprofil->getProfil(),
                'wp'                           => $this->Mprofil->getWp(),
                'res'                         => $this->MTagihan->getdata($tahun,$kodeusaha),
                'jenisPajak'                   => $this->MTagihan->ListJenisPajak(),
            );

            $data['tahun']=$tahun;
            $data['tu']=$kodeusaha;
            $data['rk']=$rk;
            $data['td']=$this->db->query("select TO_CHAR (s_valid, 'mm')bulan_daftar, TO_CHAR (s_valid, 'YYYY')tahun_daftar from wajib_pajak where npwp='$this->nip'")->row();
            $this->template->load('Welcome/halaman','Wp/data_tagihan/data_tagihan_list',$data);
        } else {
            if(isset($_GET['tahun'])&&isset($_GET['npwpd'])&&isset($_GET['jenisPajak'])&&isset($_GET['nama_usaha'])){
                $tahun     =$_GET['tahun'];
                $np        =explode("|", $_GET['npwpd']);
                $npwpd     =$np[0];
                $jp        =explode("|", $_GET['jenisPajak']);
                $jenisPajak=$jp[0];
                $nama_usaha='';
                $kodeusaha=$_GET['nama_usaha'];
                $rk="tampil";
                $data['kc']=$this->db->query("SELECT ID_INC||'|'||'$npwpd' kd, NAMA_PAJAK FROM JENIS_PAJAK  order by ID_INC asc")->result();
            }else{
                $tahun=date("Y");
                $npwpd      ="";
                $jenisPajak ="";
                $nama_usaha ="";
                $kodeusaha ="";
                $rk="";                
            }
            $data = array(
                'res'                   =>$this->MTagihan->getdataPenagihan($npwpd,$jenisPajak,$nama_usaha,$tahun,$kodeusaha),
                'JP'                    =>$this->MTagihan->ListJenisPajak(),
                'wp'                    =>$this->MTagihan->dataWp($npwpd),
            ); 
            $data['kc']=$this->db->query("SELECT ID_INC||'|'||'$npwpd' kd, NAMA_PAJAK FROM JENIS_PAJAK  order by ID_INC asc")->result();         
            $data['tu']=$this->db->query("SELECT nama_usaha,namakec,namakelurahan,a.ID_INC from tempat_usaha a
                                            join KECAMATAN d on A.KODEKEC = d.KODEKEC
                                            join KELURAHAN e on A.KODEKEL = e.KODEKELURAHAN AND A.KODEKEC=e.KODEKEC
                                            where npwpd='".$npwpd."' and jenis_pajak='".$jenisPajak."'")->result();
            $data['tahun']=$tahun;
            $data['jenisPajak']=$jenisPajak;
            $data['npwpd']=$npwpd;
            $data['id_inc']=$kodeusaha;
            $data['rk']=$rk;
            $data['td']=$this->db->query("select TO_CHAR (s_valid, 'mm')bulan_daftar,TO_CHAR (s_valid, 'YYYY')tahun_daftar from wajib_pajak where npwp='$npwpd'")->row();
            $this->template->load('Welcome/halaman','Wp/data_tagihan/data_tagihan_list',$data);            
        }
    }
    function lookup_wp(){
        $keyword = $this->input->post('term');
        $data['response'] = 'false'; //Set default response        
        $query = $this->db->query("select nama,npwp  from wajib_pajak where nama like UPPER('%$keyword%') or npwp like '%$keyword%'")->result(); //Search DB
        if( ! empty($query) )
        {
            $data['response'] = 'true'; //Set response
            $data['message'] = array(); //Create array
            foreach( $query as $row )
            {
                $data['message'][] = array(
                                         'value' => $row->NPWP.'|'.$row->NAMA,
                                        ''
                                     );  //Add a row to array
            }
        }
        if($data!=''){
            echo json_encode($data); //echo json string if ajax request
        }else{
            
        }
    }
    function get_jenis_pajak(){
        $npwp=$_POST['npwp'];

            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT a.id_inc,nama_pajak FROM JENIS_PAJAK  a 
                                        join tempat_usaha b on a.id_inc=b.jenis_pajak 
                                        where npwpd='$npwp'
                                        group by  a.id_inc,nama_pajak
                                        order by ID_INC asc")->result();
        $data['npwp']=$npwp;
            // print_r($data);
        $this->load->view('Wp/get_jenis_pajak',$data);
    }
    function get_nama_usaha(){
        $data1=$_POST['data1'];
        $exp=explode("|", $data1);
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT nama_usaha,ID_INC from tempat_usaha where npwpd='".$exp[1]."' and jenis_pajak='".$exp[0]."'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Wp/get_nama_usaha',$data);
    }
}