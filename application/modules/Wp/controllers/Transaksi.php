<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transaksi extends CI_Controller
{

    
    public function __construct()
    {
          parent::__construct();
          $this->load->model('Mtransaksi');
          $this->load->library('form_validation');
          $this->db2 = $this->load->database('simoni', TRUE);
          $connected = $this->db2->initialize();
          $this->id_pengguna=$this->session->userdata('INC_USER');
          if (!$connected) {
             echo 'Tidak Dapat Mengambil Data Dari Server. <a href="javascript:history.back()">Kembali Halaman Sebelumnya</a> ';
          }
 
    }

    private function cekAkses($var=null){
        return cek($this->id_pengguna,$var);
    }

    public function index()
    {
        // $transaksi = $this->Mtransaksi->getdatabynow();
        $akses =$this->cekAkses(uri_string());
        
        if(isset($_POST['tanggal'])){
            $tanggal     = $_POST['tanggal'];
            $arr         = explode('/',$tanggal);
            $filltanggal = $arr[2].'-'.$arr[1].'-'.$arr[0];
        }else{
            $tanggal=date('Y-m-d');
            $filltanggal=date('Y-m-d');
        }

        $transaksi = $this->Mtransaksi->getdatabynow($filltanggal, $_SESSION['NIP']);
        
        $total=0;
        foreach ($transaksi as $res) {
            $total += $res->jumlah;
        }

        $data = array(
            'tanggal'        => date('d/m/Y', strtotime($filltanggal)),    
            'transaksi_data' => $transaksi,
            'total'          => $total,
            'jum_row'        => count($transaksi),
        );

        $this->template->load('Welcome/halaman','Wp/transaksi/Transaksi_list', $data);
    }

    public function cetakharini(){
        $tanggal   = $this->uri->segment(4);

        $transaksi = $this->Mtransaksi->getdatabynow($tanggal, $_SESSION['NIP']);

        if($nama_wp=='all'){
            $nama_wp = 'Semua WP';
        }
      
        $data = array(
            'tanggal'        => date('d-m-Y', strtotime($tanggal)),
            'waktu'          => date('dmYHis'),
            'nama_wp'        => $nama_wp,
            'transaksi_data' => $transaksi,
            
        );
        
            // $this->template->load('blank','transaksi/Transaksi_list', $data);/
            $this->load->view('Wp/transaksi/cetakTransaksi_list', $data)    ;
    }

    function ajaxdatatransaksi_baru(){
        // echo "halaman auto refresh setiap 5 detik: ".rand(99,2); //Hanya contoh saja..
        
        $tanggal = date('Y-m-d', strtotime($this->uri->segment('4')));
        $nama_wp = $this->uri->segment('5');
        $transaksi = $this->Mtransaksi->getdatabynow($tanggal,$nama_wp);
        $total=0;
        foreach ($transaksi as $res) {
            $total += $res->jumlah;
        }

        $data = array(
             'tgl' => $tanggal,
             'total' => $total,
            'transaksi_data' => $transaksi,
        );

        $this->load->view('Wp/transaksi/kontenajaxtransaksi',$data);
        // $this->template->load('blank','transaksi/Transaksi_list', $data);
    }


    public function Rekap()
    {
        // $transaksi = $this->Mtransaksi->getdatabynow();
        if(isset($_POST['bulan'])){
            $bulan=$_POST['bulan'];
        }else{
            $bulan=date('m');
        }

        if(isset($_POST['tahun'])){
            $tahun=$_POST['tahun'];
        }else{
            $tahun=date('Y');
        }

        if(isset($_POST['nama_wp']) && $_POST['nama_wp'] != ''){
            $nama_wp = $_POST['nama_wp'];
        }else{
            $nama_wp = 'all';
        }


        $ref_bulan = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );


        $rekap = $this->Mtransaksi->getdata($bulan, $tahun, $_SESSION['NIP']);
        
        $total=0;
        foreach ($rekap as $res) {
            $total += $res->omset;
        }

        $data = array(
            'nama_wp'   => $nama_wp,
            'bulan'     =>$bulan,
            'tahun'     =>$tahun,    
            'rekap'     => $rekap,
            'jum_row'   => count($rekap),
            'ref_bulan' => $ref_bulan,
        );

        $this->template->load('Welcome/halaman','Wp/transaksi/Rekap_bulanan_list', $data);
    }


    public function cetak_laporan(){
         
        $bulan = $this->uri->segment('4');
        $tahun = $this->uri->segment('5');

        $rekap = $this->Mtransaksi->getdata($bulan, $tahun, $_SESSION['NIP']);
      
        $data = array(
            'tanggal' => date('F', strtotime($bulan)).' '.$tahun,
            'waktu'   => date('dmYHis'),
            'bulan'   => $bulan,
            'tahun'   => $tahun,
            'nama_wp' => $_SESSION['NAMA'],
            'rekap'   => $rekap
            
        );
        
            $this->load->view('Wp/transaksi/cetakRekapBulanan', $data);
    }

    public function ajaxDetail()
    {
         $tgl = date('Y-m-d', strtotime($_POST['tanggal']));
        $npwpd = $_SESSION['NIP'];
        $res = $this->db2->query("SELECT * FROM tb_transaksi_dept WHERE DATE(tgl_transaksi)='$tgl' AND npwpd='$npwpd' ORDER BY DATE(tgl_transaksi) DESC")->result();

        $total=0;
        foreach ($res as $a) {
            $total+=$a->jumlah;
        }

        $data = array(
            'res' => $res,
            'total' => $total
        );


        $this->load->view('Wp/transaksi/detailRekap',$data);
    }
 
 
}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-12-12 08:37:45 */
/* http://harviacode.com */