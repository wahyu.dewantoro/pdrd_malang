<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendataan extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /*$this->load->model('Mprofil');
        
        $this->load->library('form_validation');        
        $this->load->library('datatables');*/
        $this->nip=$this->session->userdata('NIP');
        $this->load->model('Sptpd_hotel/Msptpd_hotel');
        $this->load->model('Sptpd_restoran/Msptpd_restoran');
        $this->load->model('Sptpdhiburan/Mhiburan');
        $this->load->model('Sptpd_ppj/Msptpd_ppj');
        $this->load->model('Sptpd_mblbm/Msptpd_mblbm');
        $this->load->model('sptpdair/Mair');
        $this->load->model('Master/Mpokok');
        $this->role=$this->session->userdata('MS_ROLE_ID');
        
    }
    public function index(){
    $data['list_masa_pajak']=$this->Mpokok->listMasapajak();
    $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
    $this->template->load('Welcome/halaman','Wp/pendataan/form_entry',$data);
  }
  function get_nama_usaha(){
        $data1=$_POST['data1'];
        $exp=explode("|", $data1);
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$exp[1]."' and jenis_pajak='".$exp[0]."'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Wp/pendataan/get_nama_usaha',$data);
    }
    function get_alamat_usaha(){
        $data1=$_POST['data1'];
        $exp=explode("|", $data1);
            //$data['parent']=$parent;
        $data1=$this->db->query("SELECT NAMA_USAHA||'|'|| ALAMAT_USAHA||'|'||a.kodekec||'|'||B.NAMAKEC||'|'||A.kodekel||'|'||C.NAMAkelurahan USAHA from tempat_usaha a
                                join kecamatan b on a.kodekec =B.KODEKEC 
                                join kelurahan c on a.kodekel =C.kodekelurahan AND A.KODEKEC=C.KODEKEC
                                 where nama_usaha ='".$exp[0]."'")->row();
            // print_r($data);
        error_reporting(E_ALL^(E_NOTICE|E_WARNING));
        echo $data1->USAHA;
        //echo $data1;
        //$this->load->view('Wp/get_nama_usaha',$data);
    }
    function get_form_entry(){
       $data1=$_POST['data1'];
       $exp=explode("|", $data1);
       if ($exp[1]=='01'){
          $data['jenis_hotel']    = $this->Msptpd_hotel->ListJenisHotel();
          $data['kec']            = $this->Mpokok->getKec();
          $data['GOLONGAN']       = $this->Msptpd_hotel->getGol();
        $this->load->view('Wp/pendataan/get_form_hotel',$data);      
       }else if ($exp[1]=='02') {
            $data['tu']             = $this->Msptpd_restoran->getTu();
            $data['GOLONGAN']       = $this->Msptpd_restoran->getGol();
            $this->load->view('Wp/pendataan/get_form_restoran',$data);
       }else if ($exp[1]=='03') {                      
            $data['tu']             = $this->Mhiburan->getTu();
            $data['GOLONGAN']       = $this->Mhiburan->getGol();
            $this->load->view('Wp/pendataan/get_form_hiburan',$data);
       }else if ($exp[1]=='04') {
            $data['tu']             = $this->Msptpd_restoran->getTu();
            $data['GOLONGAN']       = $this->Msptpd_restoran->getGol();
            $this->load->view('Wp/pendataan/get_form_reklame',$data);
       }else if ($exp[1]=='05') {
            $data['tu']             = $this->Msptpd_ppj->getTu();
            $data['GOLONGAN']       = $this->Msptpd_ppj->getGol();
            $this->load->view('Wp/pendataan/get_form_ppj',$data);
       }else if ($exp[1]=='06') {
            $data['tu']             = $this->Msptpd_mblbm->getTu();
            $data['GOLONGAN']       = $this->Msptpd_mblbm->getGol();
            $this->load->view('Wp/pendataan/get_form_minerba',$data);
       }else if ($exp[1]=='07') {
            $this->load->view('Wp/pendataan/get_form_parkir');
       }else if ($exp[1]=='08') {
            $data['peruntukan']     =$this->Mair->Peruntukan();
            $data['golongan']       =$this->Mair->Gol();
            $this->load->view('Wp/pendataan/get_form_air_tanah',$data);
       }else if ($exp[1]=='09') {            
            $this->load->view('Wp/pendataan/get_form_sarang_burung');
       }else{
            //$this->load->view('Wp/pendataan/get_form_kosong');
       }
        
       
    }
    function create_action() {
        $MASA_PAJAK  =$this->input->post('MASA_PAJAK',TRUE);
        $TAHUN_PAJAK =$this->input->post('TAHUN_PAJAK',TRUE);
        $NAMA_WP     =$this->input->post('NAMA_WP',TRUE);
        $ALAMAT_WP   =$this->input->post('ALAMAT_WP',TRUE);
       // $NAMA_USAHA  =$this->input->post('NAMA_USAHA',TRUE);
        $ALAMAT_USAHA=$this->input->post('ALAMAT_USAHA',TRUE);
        $NPWPD       =$this->input->post('NPWPD',TRUE);
        $JP          =explode("|", $this->input->post('NAMA_USAHA',TRUE));
        $NAMA_USAHA  =$JP[0];
        $JENIS_PAJAK =$JP[1];

        if ($JENIS_PAJAK=='01') {
            $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
            $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
            $this->db->set('MASA_PAJAK', $MASA_PAJAK);
            $this->db->set('TAHUN_PAJAK', $TAHUN_PAJAK);
            $this->db->set('NAMA_WP', $NAMA_WP);
            $this->db->set('ALAMAT_WP', $ALAMAT_WP);
            $this->db->set('NAMA_USAHA', $NAMA_USAHA);
            $this->db->set('ALAMAT_USAHA', $ALAMAT_USAHA);
            $this->db->set('NPWPD', $NPWPD);
            $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
            $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
            $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
            $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
            $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
            $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
            $this->db->set('JUMLAH_KAMAR',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_KAMAR'))));
            $this->db->set('TARIF_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF_RATA_RATA'))));
            $this->db->set('KAMAR_TERISI',str_replace(',', '.', str_replace('.', '',$this->input->post('KAMAR_TERISI'))));
            $this->db->set('MASA', $this->input->post('MASA',TRUE));
            $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
            $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
            $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
            $this->db->set('STATUS', 0);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
                if ($this->role=='8') {
                    $this->db->set('NPWP_INSERT',$this->nip);
                    $this->db->set('KODE_BILING', "'01' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                } if ($this->role=='4') {
                    $this->db->set('NIP_INSERT',$this->nip);
                    $this->db->set('KODE_BILING', "'01' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                }

            $this->db->set('TGL_INSERT',"SYSDATE",false);
            $this->db->insert('SPTPD_HOTEL'); 
            $this->session->set_flashdata('message', '<script>
              $(window).load(function(){
                 swal("Berhasil Tambah ", "", "success")
             });
             </script>');
            redirect(site_url('wp/Pendataan'));
        } else if ($JENIS_PAJAK=='02') {
            $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
            $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
            $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
            $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
            $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
            $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
            $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
            $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
            $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
            $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
            $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
            $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
            $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
            $this->db->set('JUMLAH_MEJA',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_MEJA'))));
            $this->db->set('TARIF_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF_RATA_RATA'))));
            $this->db->set('MEJA_TERISI',str_replace(',', '.', str_replace('.', '',$this->input->post('MEJA_TERISI'))));
            $this->db->set('MASA', $this->input->post('MASA',TRUE));
            $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
            $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
            $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
            $this->db->set('STATUS', 0);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->set('TGL_INSERT',"SYSDATE",false);
                if ($this->role=='8') {
                    $this->db->set('NPWP_INSERT',$this->nip);
                    $this->db->set('KODE_BILING', "'02' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                } if ($this->role=='4') {
                    $this->db->set('NIP_INSERT',$this->nip);
                    $this->db->set('KODE_BILING', "'02' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                }
            $this->db->insert('SPTPD_RESTORAN'); 
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<script>
              $(window).load(function(){
                 swal("Berhasil Tambah SPTPD Restoran", "", "success")
             });
             </script>');
            redirect(site_url('wp/Pendataan'));
        } else if ($JENIS_PAJAK=='03') {
            $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
            $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
            $this->db->trans_start();
                    //  insert hibran
            $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
            $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
            $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
            $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
            $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
            $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
            $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
            $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
            $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
            $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
            $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
            $this->db->set('HARGA_TANDA_MASUK',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_TANDA_MASUK'))));
            $this->db->set('JUMLAH',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH'))));
            $this->db->set('JUMLAH_TOTAL',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_TOTAL'))));
            $this->db->set('MASA', $this->input->post('MASA',TRUE));
            $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
            $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
            $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
            $this->db->set('STATUS', 0);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->set('TGL_INSERT',"SYSDATE",false);
            $this->db->set('KODE_BILING', "'03' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
            if ($this->role=='8') {
                $this->db->set('NPWP_INSERT',$this->nip);
            } if ($this->role=='4') {
                $this->db->set('NIP_INSERT',$this->nip);
                //$this->db->set('KODE_BILING', "'03' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
            }
            $this->db->insert('SPTPD_HIBURAN');
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE){
                    // sukses
                $nt="Berhasil di simpan";
            }else{
                    // gagal
                $nt="Gagal di simpan";
            }
            
            $this->session->set_flashdata('notif', '<script>
              $(window).load(function(){
                 swal("'.$nt.'", "", "success")
             });
             </script>');
           redirect(site_url('wp/Pendataan'));
        } else if ($JENIS_PAJAK=='04') {
            # code...
        } else if ($JENIS_PAJAK=='05') {
            $exp=explode("|",$this->input->post('KEPERLUAN',TRUE));
            $kg=explode("|",$this->input->post('KEGUNAAN',TRUE));
            $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
            $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
            $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
            $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
            $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
            $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
            $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
            $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
            $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
            $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
            $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
            $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
            $this->db->set('CARA_PERHITUNGAN', $this->input->post('CARA_PERHITUNGAN',TRUE));

            $this->db->set('JAM_OPERASI', $this->input->post('JAM_OPERASI',TRUE));
            $this->db->set('KEGUNAAN', $kg[0]);
            $this->db->set('KEPERLUAN', $exp[0]);
            $this->db->set('TARIF',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF'))));
            $this->db->set('DAYA_TERPASANG',str_replace(',', '.', str_replace('.', '',$this->input->post('DAYA_TERPASANG'))));
            $this->db->set('FAKTOR_DAYA',str_replace(',', '.', str_replace('.', '',$this->input->post('FAKTOR_DAYA'))));

            $this->db->set('PEMAKAIAN_BULAN_LALU',str_replace(',', '.', str_replace('.', '',$this->input->post('PEMAKAIAN_BULAN_LALU'))));
            $this->db->set('PEMAKAIAN_BULAN_INI',str_replace(',', '.', str_replace('.', '',$this->input->post('PEMAKAIAN_BULAN_INI'))));

            $this->db->set('JUMLAH_PEMAKAIAN',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_PEMAKAIAN'))));
            $this->db->set('MASA', $this->input->post('MASA',TRUE));
            $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
            $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
            $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));

            $this->db->set('STATUS', 0);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
                if ($this->role=='8') {
                    $this->db->set('NPWP_INSERT',$this->nip);
                    $this->db->set('KODE_BILING', "'05' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                } if ($this->role=='4') {
                    $this->db->set('NIP_INSERT',$this->nip);
                    $this->db->set('KODE_BILING', "'05' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                }
            $this->db->set('TGL_INSERT',"SYSDATE",false);
            $this->db->insert('SPTPD_PPJ'); 
            $this->session->set_flashdata('message', '<script>
              $(window).load(function(){
                 swal("Berhasil Tambah SPTPD PPJ", "", "success")
             });

             </script>');
            redirect(site_url('Sptpd_ppj'));
        } else if ($JENIS_PAJAK=='06') {
            $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
            $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
            $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
            $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
            $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
            $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
            $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
            $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
            $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
            $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
            $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
            $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
            $this->db->set('PENGAMBILAN_MINERAL',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGAMBILAN_MINERAL'))));
            $this->db->set('HARGA_JUAL_MINERAL',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_JUAL_MINERAL'))));
            $this->db->set('MASA', $this->input->post('MASA',TRUE));
            $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
            $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
            $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
            $this->db->set('STATUS', 0);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
                if ($this->role=='8') {
                    $this->db->set('NPWP_INSERT',$this->nip);
                    $this->db->set('KODE_BILING', "'06' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                } if ($this->role=='4') {
                    $this->db->set('NIP_INSERT',$this->nip);
                }

            $this->db->set('TGL_INSERT',"SYSDATE",false);
            $this->db->insert('SPTPD_MINERAL_NON_LOGAM'); 
            $this->session->set_flashdata('message', '<script>
              $(window).load(function(){
                 swal("Berhasil Tambah SPTPD Mineral Bukan Logam dan Batuan", "", "success")
             });

             </script>');
            redirect(site_url('Sptpd_mblbm'));
        } else if ($JENIS_PAJAK=='07') {
            $this->db->trans_start();
            //  insert hibran
            $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
            $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
            $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
            $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
            $this->db->set('NAMA_USAHA',$this->input->post('NAMA_USAHA'));
            $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
            $this->db->set('NPWPD',$this->input->post('NPWPD'));
            $this->db->set('PENERIMAAN_NON_KARCIS',str_replace('.', '', $this->input->post('PENERIMAAN_NON_KARCIS')));
            $this->db->set('DPP',str_replace('.', '', $this->input->post('DPP')));
            $this->db->set('PAJAK_TERUTANG',str_replace('.', '', $this->input->post('PAJAK_TERUTANG')));
            $this->db->set('SSPD',$this->input->post('SSPD'));
            $this->db->set('REKAP_BON',$this->input->post('REKAP_BON'));
            $this->db->set('LAINYA',$this->input->post('LAINYA'));
            $this->db->set('NIP_INSERT',$this->nip);
            $this->db->set('TGL_INSERT',"sysdate",false);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->insert('SPTPD_PARKIR');
            // get 
            $rk=$this->db->query("SELECT MAX(ID_INC) SPTPD_PARKIR_ID
                                    FROM SPTPD_PARKIR
                                    WHERE NIP_INSERT='".$this->nip."' AND IP_INSERT='".$this->Mpokok->getIP()."'")->row();
            // $cek=count();
            if(!empty($_POST['JENIS_PARKIR'])){
                $count=count($_POST['JENIS_PARKIR']);
                for($i=0;$i<$count;$i++){
                    $this->db->set('SPTPD_PARKIR_ID',$rk->SPTPD_PARKIR_ID);
                    $this->db->set('JENIS_PARKIR',$_POST['JENIS_PARKIR'][$i]);
                    $this->db->set('JUMLAH_LEMBAR',str_replace('.', '', $_POST['JUMLAH_LEMBAR'][$i]));
                    $this->db->set('NOMINAL',str_replace('.', '', $_POST['NOMINAL'][$i]));
                    $this->db->set('PENERIMAAN',str_replace('.', '', $_POST['PENERIMAAN'][$i]));
                    $this->db->insert('SPTPD_PARKIR_KARCIS');
                }
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE){
                // sukses
                $nt="Berhasil di simpan";
            }else{
                // gagal
                $nt="Gagal di simpan";
            }
            
        $this->session->set_flashdata('message', '<script>
                                              $(window).load(function(){
                                               swal("'.$nt.'", "", "success")
                                              });
                                            </script>');
                redirect(site_url('Sptpd_parkir'));
        } else if ($JENIS_PAJAK=='08') {
            $this->db->trans_start();
            $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
            $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
            $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
            $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
            $this->db->set('NAMA_USAHA',$this->input->post('NAMA_USAHA'));
            $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
            $this->db->set('NPWPD',$this->input->post('NPWPD'));
            $this->db->set('NO_BERKAS',$this->input->post('NO_BERKAS'));
            $this->db->set('NO_SIPA_SIPPAT',$this->input->post('NO_SIPA_SIPPAT'));
            $this->db->set('TMT',$this->input->post('TMT'));
            $this->db->set('ID_GOL_OP',$this->input->post('GOLONGAN_OP'));
            $this->db->set('PERUNTUKAN',$this->input->post('PERUNTUKAN'));
            $this->db->set('CARA_PENGAMBILAN',$this->input->post('CARA_PENGAMBILAN'));
            $this->db->set('PAJAK',$this->input->post('PAJAK'));
            $this->db->set('DEBIT_NON_METER',str_replace(',', '.', str_replace('.', '', $this->input->post('DEBIT_NON_METER') ) ));
            $this->db->set('PENGGUNAAN_HARI_NON_METER', str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_NON_METER'))));
            $this->db->set('PENGGUNAAN_BULAN_NON_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_NON_METER'))));
            $this->db->set('PENGGUNAAN_HARI_INI_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_INI_METER'))));
            $this->db->set('PENGGUNAAN_BULAN_LALU_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_LALU_METER'))));
            $this->db->set('VOLUME_AIR_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('VOLUME_AIR_METER'))));
            $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
            $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
            $this->db->set('NIP_INSERT',$this->nip);
            $this->db->set('TGL_INSERT',"sysdate",false);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->insert('SPTPD_AIR_TANAH');
            $this->db->trans_complete();
            if ($this->db->trans_status() === TRUE){
                    // sukses
                $nt="Berhasil di simpan";
            }else{
                    // gagal
                $nt="Gagal di simpan";
            }

            $this->session->set_flashdata('notif', '<script>
              $(window).load(function(){
                 swal("'.$nt.'", "", "success")
             });
             </script>');
            
            redirect('sptpdair/air');
        } else if ($JENIS_PAJAK=='09') {
            $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
            $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
            $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
            $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
            $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
            $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
            $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
            $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
            $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));

            $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
            $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
            $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
            $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
            $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
            $this->db->set('JENIS_PENGELOLAAN', $this->input->post('JENIS_PENGELOLAAN',TRUE));
            $this->db->set('JENIS_BURUNG', $this->input->post('JENIS_BURUNG',TRUE));
            $this->db->set('KEPEMILIKAN_IJIN', $this->input->post('KEPEMILIKAN_IJIN',TRUE));
            $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
            $this->db->set('DARI', $this->input->post('DARI',TRUE));
            $this->db->set('NOMOR', $this->input->post('NOMOR',TRUE));
            $this->db->set('LUAS_AREA',str_replace(',', '.', str_replace('.', '',$this->input->post('LUAS_AREA'))));
            $this->db->set('JUMLAH_GALUR',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_GALUR'))));
            $this->db->set('DIPANEN_SETIAP', $this->input->post('DIPANEN_SETIAP',TRUE));
            $this->db->set('HASIL_PANEN', $this->input->post('HASIL_PANEN',TRUE));
            $this->db->set('HASIL_SETIAP_PANEN',str_replace(',', '.', str_replace('.', '',$this->input->post('HASIL_SETIAP_PANEN'))));
            $this->db->set('HARGA_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_RATA_RATA'))));
            $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
            $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
            $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
            $this->db->set('STATUS', 0);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
                    if ($this->role=='8') {
                        $this->db->set('NPWP_INSERT',$this->nip);
                        $this->db->set('KODE_BILING', "'09' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                    } if ($this->role=='4') {
                        $this->db->set('NIP_INSERT',$this->nip);
                        $this->db->set('KODE_BILING', "'09' ||TO_CHAR(SYSDATE, 'yymmddhhiiss')",false);
                    }
            $this->db->set('TGL_INSERT',"SYSDATE",false);
            $this->db->insert('SPTPD_SARANG_BURUNG'); 
                    $this->session->set_flashdata('message', '<script>
          $(window).load(function(){
           swal("Berhasil Tambah SPTPD Sarang Burung", "", "success")
          });

        </script>');
                    redirect(site_url('Sptpd_psb'));
        } else {
            echo "Data Tidak di temukan";
        }
    }
    public function karcis($x="")
    {
         $data['x']=$x;
         $this->load->view('Sptpd_parkir/sptpd_parkir_karcis',$data);
    }
 
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */