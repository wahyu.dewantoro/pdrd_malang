<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class beranda extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mberanda');
        $this->load->model('Sptpd_hotel/Msptpd_hotel');
        $this->load->model('Sptpd_restoran/Msptpd_restoran');
        $this->load->model('Sptpdhiburan/Mhiburan');
        $this->load->model('Sptpd_ppj/Msptpd_ppj');
        $this->load->model('Sptpd_mblbm/Msptpd_mblbm');
        $this->load->model('Sptpd_parkir/Msptpd_parkir');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
    }
    public function index(){
    $bulan=date('n');$tahun=date('Y');
    $data = array(
                    'button'                       => 'Formulir Pembuatan SPTPD',
                    'action'                       => site_url('Sptpd_hotel/create_action'),
                    'disable'                      => '',
                    'ID_INC'                       => set_value('ID_INC'),
                    'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
                    'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
                    'NAMA_WP'                      => set_value('NAMA_WP'),
                    'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
                    'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
                    'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
                    'NPWPD'                        => set_value('NPWPD'),
                    'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
                    'tu'                           => $this->Mberanda->getTu(),
        );
        $data['jp']=$this->db->query("SELECT * FROM JENIS_PAJAK  order by ID_INC asc")->result();
        $data['list_data']=$this->db->query("select * from v_lima_top_bayar where masa_pajak='$bulan' and tahun_pajak='$tahun' and jenis_pajak='01' and ROWNUM <= 5 ")->result();
        $data['jumlah_wp_perpajak']=$this->db->query("select * from v_jumlah_wp_perpajak")->result();
        $data['jumlah_tagihan_perpajak']=$this->db->query("select * from v_jml_tagihan_perpajak where masa_pajak='$bulan' and tahun_pajak='$tahun'")->result();
        $data['wp_baru']=$this->db->query("select count(*)wp_baru from wajib_pajak_temp where status='0'")->row();
    if ($this->session->userdata('MS_ROLE_ID')=='8' OR $this->session->userdata('MS_ROLE_ID')==81) {
         $this->template->load('Welcome/halaman','Wp/beranda/beranda_form',$data);
    } else if ($this->session->userdata('MS_ROLE_ID')=='3') {
        
         $this->template->load('Welcome/halaman','Wp/beranda/beranda_pimpinan',$data);
    } else if ($this->session->userdata('MS_ROLE_ID')=='4') {

        $this->template->load('Welcome/halaman','Wp/beranda/beranda_pimpinan',$data);
    } else if ($this->session->userdata('MS_ROLE_ID')=='221') {
        $data['air_tanah']=$this->db->query("select count(*) total from sptpd_air_tanah where status='2'")->row();
        $data['reklame']=$this->db->query("select count(*) total from sptpd_reklame where status='2'")->row();
        $this->template->load('Welcome/halaman','Wp/beranda/beranda_penetapan',$data);
    } else {
        $this->template->load('Welcome/halaman','Wp/beranda/beranda');
    }
  }
public function hotel()
{
    
    $id=$_POST['id'];
    $row = $this->Mberanda->get_by_id($id);
    $ob  = $this->Mberanda->getOb($row->ID_OP);
    $NO_FORMULIR=$this->db->query("select max(NO_FORMULIR)+1 as NO_FORMULIR FROM SPTPD_HOTEL")->row();
    $data = array(
        'JUMLAH_KAMAR'                 => set_value('JUMLAH_KAMAR',$row->JUMLAH_KAMAR_MEJA),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA',$ob->MASA),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN',1),
        'PAJAK'                        => set_value('PAJAK',$ob->PERSEN),
        'jenis_hotel'                  => $this->Msptpd_hotel->ListJenisHotel(),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN',$row->KODEKEC),
        'KELURAHAN'                    => set_value('KELURAHAN',$row->KODEKEL),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$row->ID_OP),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN','Omzet'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR',$NO_FORMULIR->NO_FORMULIR),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_hotel->getGol(),
        'kel'                          => $this->Msptpd_hotel->getkel($row->KODEKEC),

    );           
    $this->load->view('Wp/beranda/hotel_form',$data);
}   
public function restoran()
{
    
    $NO_FORMULIR=$this->db->query("select max(NO_FORMULIR)+1 as NO_FORMULIR FROM SPTPD_RESTORAN")->row();
    $id=$_POST['id'];
    $row = $this->Mberanda->get_by_id($id);
    $ob  = $this->Mberanda->getOb($row->ID_OP);
    $data = array(
        'JENIS_RESTO'                  => set_value('JENIS_RESTO'),
        'JUMLAH_MEJA'                  => set_value('JUMLAH_MEJA',$row->JUMLAH_KAMAR_MEJA),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'MEJA_TERISI'                  => set_value('MEJA_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA',$ob->MASA),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN',1),
        'PAJAK'                        => set_value('PAJAK',$ob->PERSEN),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN',$row->KODEKEC),
        'KELURAHAN'                    => set_value('KELURAHAN',$row->KODEKEL),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$row->ID_OP),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN','Omzet'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR',$NO_FORMULIR->NO_FORMULIR),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_restoran->getGol(),
        'kel'                          => $this->Msptpd_hotel->getkel($row->KODEKEC),

    );           
    $this->load->view('Wp/beranda/restoran_form',$data);
}
public function hiburan()
{
    
    $NO_FORMULIR=$this->db->query("select max(NO_FORMULIR)+1 as NO_FORMULIR FROM SPTPD_HIBURAN")->row();
    $id=$_POST['id'];
    $row = $this->Mberanda->get_by_id($id);
    $ob  = $this->Mberanda->getOb($row->ID_OP);
    $data = array(
        'HARGA_TANDA_MASUK'            => set_value('HARGA_TANDA_MASUK'),
        'JUMLAH'                       => set_value('JUMLAH'),
        'JUMLAH_TOTAL'                 => set_value('JUMLAH_TOTAL'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA',$ob->MASA),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN',1),
        'PAJAK'                        => set_value('PAJAK',$ob->PERSEN),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN',$row->KODEKEC),
        'KELURAHAN'                    => set_value('KELURAHAN',$row->KODEKEL),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$row->ID_OP),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN','Omzet'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR',$NO_FORMULIR->NO_FORMULIR),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Mhiburan->getGol(),
        'kel'                          => $this->Msptpd_hotel->getkel($row->KODEKEC),

    );           
    $this->load->view('Wp/beranda/hiburan_form',$data);
}
public function ppj()
{
    
    $NO_FORMULIR=$this->db->query("select max(NO_FORMULIR)+1 as NO_FORMULIR FROM SPTPD_PPJ")->row();
    $id=$_POST['id'];
    $row = $this->Mberanda->get_by_id($id);
    $ob  = $this->Mberanda->getOb($row->ID_OP);
    $data = array(
        'CARA_PERHITUNGAN'             => set_value('CARA_PERHITUNGAN'),
        'JUMLAH_PEMAKAIAN'             => set_value('JUMLAH_PEMAKAIAN'),
        'JUMLAH_TOTAL'                 => set_value('JUMLAH_TOTAL'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA',$ob->MASA),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN',1),
        'PAJAK'                        => set_value('PAJAK',$ob->PERSEN),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN',$row->KODEKEC),
        'KELURAHAN'                    => set_value('KELURAHAN',$row->KODEKEL),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$row->ID_OP),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN','Omzet'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR',$NO_FORMULIR->NO_FORMULIR),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_ppj->getGol(),
        'kel'                          => $this->Msptpd_hotel->getkel($row->KODEKEC),
        'id'                           => '0'

    );           
    $this->load->view('Wp/beranda/ppj_form',$data);
}
public function psb()
{
    
    //$NO_FORMULIR=$this->db->query("select max(NO_FORMULIR)+1 as NO_FORMULIR FROM SPTPD_SARANG_BURUNG")->row();
    $NO_FORMULIR=$this->db->query("select NO_FORMULIR as NO_FORMULIR FROM SPTPD_SARANG_BURUNG")->row();
    $id=$_POST['id'];
    $row = $this->Mberanda->get_by_id($id);
    $ob  = $this->Mberanda->getOb($row->ID_OP);
    $data = array(
        'KEPEMILIKAN_IJIN'             => set_value('KEPEMILIKAN_IJIN'),
        'JENIS_PENGELOLAAN'            => set_value('JENIS_PENGELOLAAN'),
        'JENIS_BURUNG'                 => set_value('JENIS_BURUNG'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN',1),
        'DIPANEN_SETIAP'               => set_value('DIPANEN_SETIAP'),
        'HASIL_PANEN'                  => set_value('HASIL_PANEN'),
        'HARGA_RATA_RATA'              => set_value('HARGA_RATA_RATA'),
        'PAJAK'                        => set_value('PAJAK',5),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN',$row->KODEKEC),
        'KELURAHAN'                    => set_value('KELURAHAN',$row->KODEKEL),
        'LUAS_AREA'                    => set_value('LUAS_AREA'),
        'JUMLAH_GALUR'                 => set_value('JUMLAH_GALUR'),
        'HASIL_SETIAP_PANEN'           => set_value('HASIL_SETIAP_PANEN'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR',$NO_FORMULIR->NO_FORMULIR),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'kel'                          => $this->Msptpd_hotel->getkel($row->KODEKEC),
        'id'                           => '0'

    );           
    $this->load->view('Wp/beranda/psb_form',$data);
}
public function mineral()
{
    
    $NO_FORMULIR=$this->db->query("select max(NO_FORMULIR)+1 as NO_FORMULIR FROM SPTPD_MINERAL_NON_LOGAM")->row();
    $id=$_POST['id'];
    $row = $this->Mberanda->get_by_id($id);
    $ob  = $this->Mberanda->getOb($row->ID_OP);
    $data = array(
        'PENGAMBILAN_MINERAL'          => set_value('PENGAMBILAN_MINERAL'),
        'HARGA_JUAL_MINERAL'           => set_value('HARGA_JUAL_MINERAL',number_format($ob->TARIF,'0','','.')),
        'MASA'                         => set_value('MASA',$ob->MASA),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN',1),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$row->ID_OP),
        'PAJAK'                        => set_value('PAJAK',$ob->PERSEN),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN',$row->KODEKEC),
        'KELURAHAN'                    => set_value('KELURAHAN',$row->KODEKEL),
        'LUAS_AREA'                    => set_value('LUAS_AREA'),
        'JUMLAH_GALUR'                 => set_value('JUMLAH_GALUR'),
        'HASIL_SETIAP_PANEN'           => set_value('HASIL_SETIAP_PANEN'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR',$NO_FORMULIR->NO_FORMULIR),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'kel'                          => $this->Msptpd_hotel->getkel($row->KODEKEC),
        'GOLONGAN'                     => $this->Msptpd_mblbm->getGol(),

    );           
    $this->load->view('Wp/beranda/mineral_form',$data);
}
public function parkir()
{    
    $this->load->view('Wp/beranda/parkir_form');
}

    public function ajax_top(){
        $ID=$_POST['data1'];
        $tahun=date('Y');
        $bulan=date('n');
        if ($ID=='01') {  
            $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='02') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='03') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='04') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='05') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='06') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='07') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='08') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else if ($ID=='09') {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        } else {
           $jns=" jenis_pajak='$ID' and masa_pajak='$bulan' and tahun_pajak='$tahun' and ROWNUM <= 5";
        }
        $data['list_data']=$this->db->query("select * from v_lima_top_bayar where ".$jns." ")->result();
        
        $this->load->view('Wp/beranda/ajx_top',$data);
    }
    public function wajibPajak($id=""){
        $id=rapikan($id);
        $data['jp']=$id;
        $this->template->load('Welcome/halaman','Wp/beranda/vwajibPajak',$data);
    }
    
     function jsonwp($id="") {
        header('Content-Type: application/json');
        echo $this->Mberanda->jsonwp($id);
    }
    public function data_tagihan($id=""){
        $id=rapikan($id);
        $data['jp']=$id;
        $data['jenis']=$this->db->query("select * from jenis_pajak where ID_INC='$id'")->row();
        $this->template->load('Welcome/halaman','Wp/beranda/vdata_tagihan',$data);
    }
    
     function json_dataentry($id="") {
        header('Content-Type: application/json');
        echo $this->Mberanda->json_dataentry($id);
    }
    public function wp_baru($id=""){
        $data['jp']=$id;
        $this->template->load('Welcome/halaman','Wp/beranda/wp_baru',$data);
    }
    
     function jsonwp_wpbaru() {
        header('Content-Type: application/json');
        echo $this->Mberanda->jsonwp_wpbaru();
    }
    function proses_approve($id="",$npwp="") {
        $this->db->trans_start();
        $this->db->query("update wajib_pajak set status='1',tgl_approve=sysdate  where wp_id='$id'");
        $this->db->query("update MS_PENGGUNA set status='1'  where wp_id='$id'");
        $data=$this->db->query("select EMAIL,NAMA from wajib_pajak where wp_id='$id'")->row();
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $tujuan=$data->EMAIL;
                //$tujuan='adypabbsi@gmail.com';
                $ci                  = get_instance();
                $ci->load->library('email');
                $config['protocol']  = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "muhammad.adypranoto@gmail.com"; 
                $config['smtp_pass'] = "12345hikam";
                $config['charset']   = "utf-8";
                $config['mailtype']  = "html";
                $config['newline']   = "\r\n";          
                $ci->email->initialize($config);            
                $ci->email->from('muhammad.adypranoto@gmail.com', 'PDRD.KAB.MALANG');
                $list                = array($tujuan);
                $ci->email->to($list);
                $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                $ci->email->subject('Informasi Pendaftaran');
                $ci->email->message('Terimakasih Bapak/Ibu '.$data->NAMA.' menunggu verivikasi dari tim penetapan, mohon maaf permohonan anda belum bisa disetujui.');
                $ci->email->send();
                echo "<script>window.alert('Persetujuan Berhasil');window.location='".site_url('Wp/Beranda/wp_baru')."';</script>";
        } else {
                echo "GAGAL";
        }
    }
    function proses_tolak_wp_baru($id="",$npwp="") {
        $this->db->trans_start();
        $this->db->query("update wajib_pajak set status='3'  where wp_id='$id'");
        $this->db->query("update MS_PENGGUNA set status='3'  where wp_id='$id'");
        $data=$this->db->query("select EMAIL,NAMA from wajib_pajak where wp_id='$id'")->row();
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $tujuan=$data->EMAIL;
                //$tujuan='adypabbsi@gmail.com';
                $ci                  = get_instance();
                $ci->load->library('email');
                $config['protocol']  = "smtp";
                $config['smtp_host'] = "ssl://smtp.gmail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "muhammad.adypranoto@gmail.com"; 
                $config['smtp_pass'] = "12345hikam";
                $config['charset']   = "utf-8";
                $config['mailtype']  = "html";
                $config['newline']   = "\r\n";          
                $ci->email->initialize($config);            
                $ci->email->from('muhammad.adypranoto@gmail.com', 'PDRD.KAB.MALANG');
                $list                = array($tujuan);
                $ci->email->to($list);
                $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                $ci->email->subject('Informasi Pendaftaran');
                $ci->email->message('Terimakasih Bapak/Ibu '.$data->NAMA.' data anda telah di verivikasi, Status anda sudah di tetapkan silahkan login ke aplikasi E-Pajak.');
                $ci->email->send();
                echo "<script>window.alert('Penolakan berhasil');window.location='".site_url('Wp/Beranda/wp_baru')."';</script>";
        } else {
                echo "GAGAL";
        }
            
    }
    public function detail_wpbaru($id=""){
        $data['jenis']=$this->db->query("select * from wajib_pajak where npwp='$id'")->row();
        $this->template->load('Welcome/halaman','Wp/beranda/detail_wpbaru',$data);
    }
    
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */

