<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class profil extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mprofil');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
    }
    public function index()
    {
    $data = array(
        'button'                       => 'Form SPTPD Hotel',
        'action'                       => site_url('Sptpd_hotel/create_action'),
        'profil'                       => $this->Mprofil->getProfil(),
        'wp'                           => $this->Mprofil->getWp(),

    );  
    $this->template->load('Welcome/halaman','Wp/profil/profil',$data);
  }
public function update() 
{

    $row = $this->Mprofil->get_by_id();
        $data = array(
            'button'              => 'Update SPTPD Hotel',
            'action'              => site_url('Wp/Profil/update_action'),
            'disable'             => '',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'NAMA'                => set_value('NAMA',$row->NAMA),
        );
        $this->template->load('Welcome/halaman','Wp/profil/profil_form', $data);
} 
public function update_action() 
{
    $this->db->set('NAMA', $this->input->post('NAMA',TRUE));

    $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
    $this->db->update('MS_PENGGUNA');
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Update", "", "success")
     });

     </script>');
    redirect(site_url('Wp/profil'));
}
public function rating() 
{
        $data['star']=$this->db->query("SELECT NIP,NAMA,STAR,COMENT FROM MS_PENGGUNA WHERE NIP='$this->nip'")->row();
        $data['tot']=$this->db->query("select count(star)cn from ms_pengguna where star is not null")->row();
        $data['satu']=$this->db->query("select count(star)cn from ms_pengguna where star is not null and star='1'")->row();
        $data['dua']=$this->db->query("select count(star)cn from ms_pengguna where star is not null and star='2'")->row();
        $data['tiga']=$this->db->query("select count(star)cn from ms_pengguna where star is not null and star='3'")->row();
        $data['empat']=$this->db->query("select count(star)cn from ms_pengguna where star is not null and star='4'")->row();
        $data['lima']=$this->db->query("select count(star)cn from ms_pengguna where star is not null and star='5'")->row();
        $this->template->load('Welcome/halaman','Wp/Profil/rating', $data);
}  
public function detail_rating($rat="") 
{
        $data['rat']=$rat;
        $data['detail']=$this->db->query("select a.nama,npwp,coment
                                        from wajib_pajak a
                                        join ms_pengguna  b on a.npwp=b.nip
                                        where star='$rat'")->result();
        $this->template->load('Welcome/halaman','Wp/Profil/rating_detail', $data);
}  
public function add_rating(){

        $a=$this->input->post('bintang',TRUE);
        $b=$this->input->post('msg',TRUE);
        $nip=$this->nip;
        $this->db->query("UPDATE MS_PENGGUNA SET STAR='$a',COMENT='$b' WHERE NIP='$nip'");
        $data=$this->db->query("SELECT NAMA FROM MS_PENGGUNA WHERE NIP='$nip'")->row();
        echo "Terimakasih ".$data->NAMA." Sudah menilai aplikasi SiPanji";
} 

  
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */