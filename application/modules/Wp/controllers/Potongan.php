<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Potongan extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mprofil');
        $this->load->model('Mtempat_usaha');
        $this->load->model('MTagihan');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');   
    }
    public function index(){
        if(isset($_POST['kode_biling'])){
            $data['kode_biling']=$_POST['kode_biling'];
            // /$data['rk']=$this->Mlaporan->getKodebiling($_POST['kode_biling']);
        }else{
            $data['kode_biling']=null;
        }
        $date=date('d-m-Y');
        $data['list']=$this->db->query("select * from  v_get_tagihan
                                          where date_potongan='$date'
                                          order by sysdate_potongan")->result();
            $this->template->load('Welcome/halaman','Wp/potongan/data_potongan_list',$data);            
    }
    public function inquiry()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $KODE_BILING= $this->input->get('KODE_BILING',TRUE);
            $query=$this->db->query(
            "SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, OBJEK_PAJAK,ALAMAT_OP, CEIL(TAGIHAN)TAGIHAN,CEIL(DENDA)DENDA,CEIL(POTONGAN)POTONGAN, JENIS_PAJAK, STATUS, TABEL AS KETERANGAN, TO_CHAR (TGL_PENETAPAN, 'dd/mm/yyyy HH24: MI: SS') TGL_PENETAPAN,TO_CHAR (DATE_BAYAR, 'dd/mm/yyyy HH24: MI: SS') DATE_BAYAR,TO_CHAR(JATUH_TEMPO, 'dd/mm/yyyy HH24: MI: SS') JATUH_TEMPO,NAMA_PAJAK
            FROM TAGIHAN A LEFT JOIN JENIS_PAJAK B ON A.JENIS_PAJAK=B.ID_INC
            WHERE KODE_BILING='$KODE_BILING'"
        )->row(); 
            $data = array();
            if (!empty($query->KODE_BILING)) {
                $data = array(
                    "KODE_BILING"   =>  $query->KODE_BILING,
                    "NAMA_WP"       =>  $query->NAMA_WP,
                    "ALAMAT_WP"     =>  $query->ALAMAT_WP,
                    "OBJEK_PAJAK"     =>  $query->OBJEK_PAJAK,
                    "JENIS_PAJAK"    =>  $query->JENIS_PAJAK,
                    "MASA_PAJAK"    =>  $query->MASA_PAJAK,
                    "TAHUN_PAJAK"   => $query->TAHUN_PAJAK,
                    "TAGIHAN"       =>  (int)$query->TAGIHAN,
                    "DENDA"      => (int)$query->DENDA,
                    "POTONGAN"      => (int)$query->POTONGAN,
                    "KETERANGAN"       =>  $query->KETERANGAN,
                    "STATUS_BAYAR"      => $query->STATUS,
                    "DATE_BAYAR"      => $query->DATE_BAYAR,
                    "TGL_PENETAPAN"      => $query->TGL_PENETAPAN,
                    "JATUH_TEMPO"      => $query->JATUH_TEMPO,
                    "ALAMAT_OP"      => $query->ALAMAT_OP,
                    "NAMA_PAJAK"      => $query->NAMA_PAJAK,
                    "STATUS"        => array(
                        "IsError"=>"False",
                        "ResponseCode"=>'00',
                        "ErrorDesc" => "Success"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data,200);
            } else {
                $data = array(
                    "KODE_BILING"   =>  $KODE_BILING,
                    "STATUS"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            }

            
       }

    }
    public function tambah_potongan(){
    if (str_replace('.','',$this->input->post('JUMLAH_POTONGAN_SEBELUM'))>0) {
        $this->session->set_flashdata('notif', '<script>
        $(window).load(function(){
         swal("Potongan Sudah Diajukan", "", "success")
        });</script>');
        redirect(site_url('Wp/Potongan'));
    } else {
        $potongan=str_replace('.','',$this->input->post('JUMLAH_POTONGAN'));
        $tgl=$this->input->post('TGL_SK', TRUE);
        $this->db->set('POTONGAN', $potongan);
        $this->db->set('DATE_POTONGAN', "sysdate",false);
        $this->db->set('NO_SK_POTONGAN',$this->input->post('NOMOR_SK', TRUE));
        $this->db->set('TGL_SK_POTONGAN', "to_date('$tgl','dd/mm/yyyy')",false );
        $this->db->where('KODE_BILING',$this->input->post('kode_biling', TRUE));
        $this->db->update('TAGIHAN');
        $this->db->query("commit");
        $this->session->set_flashdata('notif', '<script>
          $(window).load(function(){
             swal("Berhasil Menambah Potongan", "", "success")
         });

         </script>');
        redirect(site_url('Wp/Potongan'));       
    }
}
    
    
}