      <div class="page-title">
           <div class="title_left">
                <h3>Menu</h3>
              </div>

              <div class="  pull-right">
                 <?php echo anchor(site_url('Setting/Msmenu/create'), '<i class="fa fa-plus"></i> Tambah', 'class="btn btn-success btn-sm"'); ?>
                
              </div>
      </div>

            <div class="clearfix"></div>
<?php echo $this->session->flashdata('message')?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                   
                  <div class="x_content">
                    <table id="example2" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th width="5%">No</th>
                          <th>Nama Menu</th>
                          <th>Parent Menu</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>


                      
                    </table>
                  </div>
                </div>
              </div>



            </div>


       <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#example2").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    
                        
                    
                    'oLanguage':
                    {
                      "sProcessing":   "Sedang memproses...",
                      "sLengthMenu":   "Tampilkan _MENU_ entri",
                      "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                      "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                      "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                      "sInfoPostFix":  "",
                      "sSearch":       "Cari:",
                      "sUrl":          "",
                      "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                      }
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "<?php echo base_url()?>Setting/Msmenu/json", "type": "POST"},
                    columns: [
                        {
                            "data": "ID_INC",
                            "orderable": false,
                            "className" : "text-center",
                        },
                        {"data": "NAMA_MENU"},
                        {"data": "PARENT_MENU"},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }
                    ],
                    order: [[2, 'asc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>

<!-- <link rel="stylesheet" href="<?php echo base_url().'assets/kendo_tree/'?>kendo.common-material.min.css" />
<link rel="stylesheet" href="<?php echo base_url().'assets/kendo_tree/'?>kendo.material.min.css" />
<script src="<?php echo base_url().'assets/kendo_tree/'?>kendo.all.min.js"></script>

    
          
           <form class='form-horizontal' action="<?php echo base_url('spop/proses_penilaian');?>" method="post" enctype="multipart/form-data">
                
                        
                                     <div class="panel-body" style="padding:;border:0px;height:400px;overflow-y:auto">
                                        <div class="form-group">
                                                <div id="treeview"></div>
                                            <script>
                                                $("#treeview").kendoTreeView({
                                                    checkboxes: {
                                                        checkChildren: true
                                                    },

                                                    check: onCheck,
                                                    dataSource: [{
                                                        text: "KABUPATEN MALANG", expanded: true, /*spriteCssClass: "",*/ items: [
                                                            <?php foreach($kecamatan as $rp){?> 
                                                            {
                                                                
               text: "<?php echo $rp['NAMAKEC']?>", expanded: false, items: [
                                                                <?php $kelurahan=$this->db->query("select *
                                                                    from kelurahan where kodekec='".$rp['KODEKEC']."'")->result_array();foreach($kelurahan as $rt){?> 
                                                                        { id: "<?php echo $rt['KODEKELURAHAN']?>", text: "<?php echo $rt['NAMAKELURAHAN']?>", spriteCssClass: "" },
                                                                      <?php }?>
                                                                ]
                                                            },
                                                                <?php }?>
                                                        ]
                                                    }]
                                                });

                                                // function that gathers IDs of checked nodes
                                                function checkedNodeIds(nodes, checkedNodes) {
                                                    for (var i = 0; i < nodes.length; i++) {
                                                        if (nodes[i].checked) {
                                                            checkedNodes.push(nodes[i].id);
                                                        }

                                                        if (nodes[i].hasChildren) {
                                                            checkedNodeIds(nodes[i].children.view(), checkedNodes);
                                                        }
                                                    }
                                                }

                                                // show checked node IDs on datasource change
                                                function onCheck() {
                                                    var checkedNodes = [],
                                                        treeView = $("#treeview").data("kendoTreeView"),
                                                        message;

                                                    checkedNodeIds(treeView.dataSource.view(), checkedNodes);

                                                    if (checkedNodes.length > 0) {
                                                        message = /*"checked nodes: " +*/ checkedNodes.join("|");
                                                    } else {
                                                        message = "";
                                                    }

                                                    $("#result").html(message);
                                                   /* var e =parseFloat(message);
                                                    alert(e);*/
                                                    $('#result').attr('value',message);
                                                        $(document).ready(function() {
                                                            $('#submit').click(function(e) {
                                                                e.preventDefault();
                                                                $.ajax({
                                                                        type: 'POST',
                                                                        url: '<?php echo base_url()?>spop/tes_jason',
                                                                        data: {tahun: $('#tahun').val(),
                                                                                param: $('#result').val()},
                                                                        success: function(data) {
                                                                          $("#content").html(data);

                                                                        }
                                                                });
                                                            });
                                                        });
                                                       
                                                }
                                            </script>
                                    </div>
                                    </div>
                                  </div>
                                        <input type='text' id="result" value='' name='param'> 
                                        <input type="checkbox" id="njoptkp" value="Yes" /> Penetapan NJOPTKP
                                        <button id="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button>
                                  </div>
                                </form>    -->                                                                   
                                                 
        
       
 

