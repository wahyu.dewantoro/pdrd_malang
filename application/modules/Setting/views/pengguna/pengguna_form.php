<div class="page-title">

            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $button;?></h2>                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">



                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Username
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="last-name" name="username" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $USERNAME ?>">                         
                          <input type="hidden" id="last-name" name="ID_INC" required="required" value="<?php echo $ID_INC ?>">
                        </div>
                      </div>
        <?php if($button!='Update Pengguna'){?>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="last-name" name="password" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $PASSWORD ?>">
                        </div>
                      </div>  
        <?php }?>                      
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">NIP / NPWPD
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="last-name" name="npwpd" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $NIP ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="last-name" name="nama_pengguna" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA ?>">
                        </div>
                      </div>     
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Grup</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="kode_group">
                            <option value="">Pilih Group</option>
                    <?php foreach($group as $group){?>
                    <option <?php if($MS_ROLE_ID==$group->ID_INC){echo "selected";}?> value="<?php echo $group->ID_INC?>"> <?php echo $group->NAMA_ROLE?></option>
                    <?php }?>
                          </select>
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">UPT</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="unit_upt">
                            <option value="">Pilih Upt</option>
                    <?php foreach($upt as $upt){?>
                    <option <?php if($UNIT_UPT_ID==$upt->ID_INC){echo "selected";}?> value="<?php echo $upt->ID_INC?>"> <?php echo $upt->NAMA_UNIT?></option>
                    <?php }?>
                          </select>
                        </div>
                      </div>   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">STATUS</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="select2_single form-control" name="STATUS">
                            <option value="">Pilih Status</option>
                            <option <?php if($STATUS=='1'){echo "selected";}?> value="1">AKtif</option>
                            <option <?php if($STATUS=='0'){echo "selected";}?> value="0">Tidak Aktif</option>
                          </select>
                        </div>
                      </div>                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a href="<?php echo site_url('Setting/pengguna') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>



            </div>

