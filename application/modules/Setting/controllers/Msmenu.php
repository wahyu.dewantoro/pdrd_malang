<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Msmenu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        
        $this->load->model('Mmenu');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
    }

    public function index()
    {
        
        $data['kecamatan']               =$this->db->query("select * from kecamatan")->result_array();
        $this->template->load('Welcome/halaman','menu/menu_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Mmenu->json();
    }

    public function read($id) 
    {
        $row = $this->Mmenu->get_by_id($id);
        if ($row) {
            $data = array(
		'kode_menu' => $row->kode_menu,
		'nama_menu' => $row->nama_menu,
		'link_menu' => $row->link_menu,
		'parent_menu' => $row->parent_menu,
		'sort_menu' => $row->sort_menu,
		'icon_menu' => $row->icon_menu,
		'active' => $row->active,
	    );
            $this->load->view('menu/', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('menu'));
        }
    }

    public function create() 
    {
        $data = array(
            'button'    => 'Tambah Menu',
            'action'    => site_url('Setting/Msmenu/create_action'),
            'lparent'    =>$this->Mmenu->ListParent(),
            'ID_INC'    => set_value('ID_INC'),
            'NAMA_MENU' => set_value('NAMA_MENU'),
            'LINK_MENU' => set_value('LINK_MENU'),
            'PARENT'    => set_value('PARENT'),
            'ICON'      => set_value('ICON'),
            'SORT'      => set_value('SORT'),
	);
        $this->template->load('Welcome/halaman','menu/menu_form',$data);
    }
    
    public function create_action() 
    {       
            // $data['ID_INC']=$this->input->post('ID_INC');
            $data['NAMA_MENU']=$this->input->post('NAMA_MENU');
            $data['LINK_MENU']=$this->input->post('LINK_MENU');
            $data['PARENT']=$this->input->post('PARENT');
            $data['ICON']=$this->input->post('ICON');
            $data['SORT']=$this->input->post('SORT');

            $this->Mmenu->insert($data); 
            redirect(site_url('Setting/Msmenu'));
 
    }
    
    public function update($id) 
    {
        $id=rapikan($id);
        $row = $this->Mmenu->get_by_id($id);

        if ($row) {
            $data = array(
                'button'      => 'Edit Menu',
                'action'      => site_url('Setting/Msmenu/update_action'),
                'lparent'    =>$this->Mmenu->ListParent(),
                'ID_INC'    => set_value('ID_INC',$row->ID_INC),
                'NAMA_MENU' => set_value('NAMA_MENU',$row->NAMA_MENU),
                'LINK_MENU' => set_value('LINK_MENU',$row->LINK_MENU),
                'PARENT'    => set_value('PARENT',$row->PARENT),
                'ICON'      => set_value('ICON',$row->ICON),
                'SORT'      => set_value('SORT',$row->SORT),
	    );
            $this->template->load('Welcome/halaman','menu/menu_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('menu'));
        }
    }
    
    public function update_action() 
    {
        $data['ID_INC']=$this->input->post('ID_INC');
        $data['NAMA_MENU']=$this->input->post('NAMA_MENU');
        $data['LINK_MENU']=$this->input->post('LINK_MENU');
        $data['PARENT']=$this->input->post('PARENT');
        $data['ICON']=$this->input->post('ICON');
        $data['SORT']=$this->input->post('SORT');
        $this->Mmenu->update($this->input->post('ID_INC', TRUE), $data);
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('Setting/msmenu'));
    }
    
    public function delete($id) 
    {
        $id=rapikan($id);
        $row = $this->Mmenu->get_by_id($id);

        if ($row) {
            $this->Mmenu->delete($id);
            $this->session->set_flashdata('message', ' <button type="button" class="btn btn-danger"> '.$row->nama_menu.' Berhasil Dihapus</button>');
            redirect(site_url('Setting/Msmenu'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Setting/Msmenu'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_menu', 'nama menu', 'trim|required');
	$this->form_validation->set_rules('link_menu', 'link menu', 'trim|required');
	$this->form_validation->set_rules('parent_menu', 'parent menu', 'trim|required');
	$this->form_validation->set_rules('sort_menu', 'sort menu', 'trim|required');
	$this->form_validation->set_rules('icon_menu', 'icon menu', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');

	$this->form_validation->set_rules('kode_menu', 'kode_menu', 'trim');
	$this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }

    function getParent(){
        $parent=$_POST['kd_portal'];
        
        $data['parent']=$parent;
        $data['rk']=$this->db->query("SELECT KODE_MENU,NAMA_MENU FROM P_MENU WHERE PARENT_MENU=$parent ORDER BY SORT_MENU ASC")->result();
        // print_r($data);
        $this->load->view('menu/getParent',$data);
    }

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-08 05:30:08 */
