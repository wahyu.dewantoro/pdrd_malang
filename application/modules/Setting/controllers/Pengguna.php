<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
         
        $this->load->model('Mpengguna');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('Welcome/halaman','pengguna/pengguna_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Mpengguna->json();
    }

  

    public function create() 
    {
        $data = array(
            'button'        => 'Tambah Pengguna',
            'action'        => site_url('Setting/pengguna/create_action'),
            'ID_INC'        => set_value('ID_INC'),
            'NIP'           => set_value('NIP'),
            'USERNAME'      => set_value('USERNAME'),
            'PASSWORD'      => set_value('PASSWORD'),
            'NAMA'          => set_value('NAMA'),
            'MS_ROLE_ID'    => set_value('MS_ROLE_ID'),
            'UNIT_UPT_ID'   => set_value('UNIT_UPT_ID'),
            'STATUS'        => set_value('STATUS'),
            'group'         => $this->db->query("select * from ms_role")->result(),
            'upt'         => $this->db->query("select * from unit_upt")->result(),
	);
        $this->template->load('Welcome/halaman','pengguna/pengguna_form', $data);
    }
    
    public function create_action() 
    {

            $data = array(
        'USERNAME'      => $this->input->post('username',true),
        'PASSWORD'      => $this->input->post('password',true),
        'NAMA' => $this->input->post('nama_pengguna',true),
        'NIP' => $this->input->post('npwpd',true),
        'MS_ROLE_ID'    => $this->input->post('kode_group',true),
        'UNIT_UPT_ID'    => $this->input->post('unit_upt',true),
        'STATUS'    => $this->input->post('STATUS',true),
        );

            $this->db->insert('MS_PENGGUNA',$data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<button type="button" class="btn btn-danger"> Berhasil Menambah Pengguna</button>');
            redirect(site_url('Setting/pengguna'));
        
    }
    
    public function update($id) 
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $data = array(
                'button'        => 'update pengguna',
                'action'        => site_url('setting/pengguna/update_action'),
                'ID_INC'        => set_value('ID_INC',$row->ID_INC),
                'NIP'           => set_value('NIP',$row->NIP),
                'USERNAME'      => set_value('USERNAME',$row->USERNAME),
                'PASSWORD'      => set_value('PASSWORD',$row->PASSWORD),
                'NAMA'          => set_value('NAMA',$row->NAMA),
                'MS_ROLE_ID'    => set_value('MS_ROLE_ID',$row->MS_ROLE_ID),
                'UNIT_UPT_ID'   => set_value('UNIT_UPT_ID',$row->UNIT_UPT_ID),
                'STATUS'   => set_value('STATUS',$row->STATUS),
                'group'         => $this->db->query("select * from ms_role")->result(),
                'upt'         => $this->db->query("select * from unit_upt")->result(),
        );
            $this->template->load('Welcome/halaman','pengguna/pengguna_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Setting/pengguna'));
        }
    }
    
    public function update_action() 
    {

        $id=$this->input->post('ID_INC',true);
            $data = array(
        'USERNAME'      => $this->input->post('username',true),
        'PASSWORD'      => $this->input->post('password',true),
        'NAMA' => $this->input->post('nama_pengguna',true),
        'NIP' => $this->input->post('npwpd',true),
        'MS_ROLE_ID'    => $this->input->post('kode_group',true),
        'UNIT_UPT_ID'    => $this->input->post('unit_upt',true),
        'STATUS'    => $this->input->post('STATUS',true),
        );

            
            $this->db->where('ID_INC', $id);
            $this->db->update('MS_PENGGUNA',$data);
            //$this->mpengguna->update($this->input->post('kode_pengguna', true), $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', '<button type="button" class="btn btn-danger"> Berhasil Update Pengguna</button>');
            redirect(site_url('Setting/pengguna'));
        
    }
    
    public function delete($id) 
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $this->Mpengguna->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('Setting/pengguna'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Setting/pengguna'));
        }
    }

    public function _rules() 
    {
    $this->form_validation->set_rules('username', 'username', 'trim|required');
    $this->form_validation->set_rules('password', 'password', 'trim|required');
    $this->form_validation->set_rules('nama_pengguna', 'nama pengguna', 'trim|required');
    $this->form_validation->set_rules('kode_group', 'group', 'trim|required');
    $this->form_validation->set_rules('nip', 'nip', 'trim|required');
    $this->form_validation->set_rules('kd_seksi', 'seksi', 'trim|required');
    $this->form_validation->set_rules('kode_pengguna', 'kode_pengguna', 'trim');
    $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }

    function _rulesas(){
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('nama_pengguna', 'nama pengguna', 'trim|required');
        $this->form_validation->set_rules('kode_group', 'group', 'trim|required');
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('kd_seksi', 'seksi', 'trim|required');
        $this->form_validation->set_rules('kode_pengguna', 'kode_pengguna', 'trim');
        $this->form_validation->set_error_delimiters('<span class ="label label-danger ">', '</span>');
    }

}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-29 07:13:04 */
