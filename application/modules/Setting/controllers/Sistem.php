<?php 
	if (!defined('BASEPATH'))exit('No direct script access allowed');

	class Sistem extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('msistem');
		 
		
	}

	
	function privilege(){
			$data['data']=$this->db->query("select nama_group,kode_group from grup order by nama_group asc")->result_array();
        	 $this->template->load('Welcome/halaman','privilege',$data);
			
	}

		 
	function settingPrivilege($kode){
		$kode=rapikan($kode);
        $data['role']        =$this->msistem->getMenu($kode);
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;*/
        $data['kode_groupq'] =$kode;
        $data['ng']          =$this->db->query("SELECT nama_role   FROM ms_role WHERE id_inc='$kode'")->row();
         $this->template->load('Welcome/halaman','settingPrivilege',$data);
    }

	function do_role(){
	        $MS_ROLE_ID =$_POST['MS_ROLE_ID'];
	        $MS_MENU_ID       =$_POST['MS_MENU_ID'];
	        $role=$this->msistem->do_role($MS_ROLE_ID,$MS_MENU_ID);
	        if($role){
	        	$this->session->set_flashdata('message', '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Role berhasil disimpan</div>');
	        	// redirect('Setting/sistem/privilege');
	        }else{
	        	$this->session->set_flashdata('message', '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Role tidak tersimpan</div>');
	        	
	        }
	        redirect('Setting/group');
	 }

	 function ubah_password(){        
        $this->template->load('Welcome/halaman','ubah_password');
    }
    function simpanpassword(){
            $password_lama = $_POST['password_lama'];
            $password_baru = $_POST['password_baru'];
            $ulangi_password = $_POST['ulangi_password'];
            $kode_user=$this->session->userdata('INC_USER');

            $ambilpass=$this->db->query("SELECT password FROM ms_pengguna where id_inc='$kode_user'")->row();
            $password_db=$ambilpass->PASSWORD;
            if($password_db==$password_lama){
            if($password_baru==$ulangi_password){
                $query="update ms_pengguna set password='$password_baru' where ID_INC='$kode_user' ";
                $update=$this->db->query($query);
                if($update)
                    { $_SESSION['gagal']=1;
                	redirect('Setting/Sistem/ubah_password');
                       // echo "<script>window.alert('Password Berhasil Diganti !'); window.location='".base_url().'profil/profil_user'.'.html'."';</script>";
                }else{$_SESSION['gagal']=0;echo"gagal";}
            }
            else{ $_SESSION['gagal']=0;
            redirect('Setting/Sistem/ubah_password');
            //echo "<script>window.alert('Ganti Password Gagal !'); window.location='".base_url().'profil/password'.'.html'."';</script>";}
        	}
        }
        else{$_SESSION['gagal']=0; 
        redirect('Setting/Sistem/ubah_password');
        //echo "<script>window.alert('Ganti Password Gagal !'); window.location='".base_url().'profil/password'.'.html'."';</script>";
    }
    } 

 
}