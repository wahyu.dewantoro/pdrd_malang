<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msistem extends CI_Model {


	function getGroup($where=""){
		return $this->db->query("SELECT * FROM ms_role ".$where)->result_array();
	}

	function getMenu($id){
		$cm=$this->db->query('SELECT ID_INC KODE_MENU FROM MS_MENU')->result_array();
		foreach($cm as $rm){
			$kode_menu=$rm['KODE_MENU'];
			$sql="SELECT MS_MENU_ID KODE_MENU FROM MS_PRIVILEGE WHERE MS_ROLE_ID='$id' AND MS_MENU_ID='$kode_menu'";
			$qcm=$this->db->query($sql)->num_rows();
			if($qcm == null){
				$data=array('MS_MENU_ID'=>$kode_menu,'MS_ROLE_ID'=>$id);

				$this->db->insert('MS_PRIVILEGE',$data);
			}
		}

		$qr=$this->db->query("SELECT * FROM (
                                SELECT  MS_MENU_ID,NAMA_MENU,CASE PARENT WHEN 0 THEN '#'  ELSE NULL END AS PARENT,STATUS STATUS_ROLE,A.SORT SORT
                                FROM MS_MENU A , MS_PRIVILEGE B WHERE A.ID_INC=B.MS_MENU_ID AND PARENT=0 AND MS_ROLE_ID='$id' 
                                UNION 
                                    SELECT MS_MENU_ID,A.NAMA_MENU,B.NAMA_MENU PARENT,STATUS,A.SORT
                                    FROM MS_MENU A,(SELECT  ID_INC,NAMA_MENU FROM MS_MENU) B, MS_PRIVILEGE C WHERE A.PARENT=B.ID_INC AND A.ID_INC=C.MS_MENU_ID AND MS_ROLE_ID='$id'
                                    )
                                     CB ORDER BY PARENT ASC,CB.SORT ASC")->result_array();
		return $qr;		
	}

	function do_role($MS_ROLE_ID,$MS_MENU_ID){
		$ua=$this->db->query("UPDATE MS_PRIVILEGE SET STATUS=0 WHERE MS_ROLE_ID='$MS_ROLE_ID'");
		if($ua){
			$jumlah=count($MS_MENU_ID);
			for($i=0; $i < $jumlah; $i++){
				$menu=$MS_MENU_ID[$i];
				$ur=$this->db->query("UPDATE MS_PRIVILEGE SET STATUS='1' WHERE MS_ROLE_ID='$MS_ROLE_ID' and MS_MENU_ID='$menu'");
				// echo "UPDATE MS_PRIVILEGE SET STATUS='1' WHERE MS_ROLE_ID='$MS_ROLE_ID' and MS_MENU_ID='$menu'<br>";
			}
			
		}	
		return $ur;	
	}

	function dataPengguna(){
		return $this->db->query("select a.kode_user,nama,email,nama_group
									from p_user a
									join grup b on a.`kode_group`=b.`kode_group` order by nama_group asc")->result_array();
	}

	function kirimEmail($tujuan,$subjek,$isi) {
  			$this->load->library('email');

				$htmlContent        = '<h1>Mengirim email HTML dengan Codeigniter</h1>';
				$htmlContent        .= '<div>Contoh pengiriman email yang memiliki tag HTML dengan menggunakan Codeigniter</div>';
				
				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->to('dewwwantoro@gmail.com');
				$this->email->from('mojorotodev@gmail.com');
				$this->email->subject('Test Email (HTML)');
				$this->email->message($htmlContent);
				if($this->email->send()){
                        	echo "jos";
                        }else{
						
							show_error($this->email->print_debugger());
 

				}
    }
 
}
