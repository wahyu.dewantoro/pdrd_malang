   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>LAPORAN SMS TERKIRIM</h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
       
        <table class="table table-bordered table-hover table-fixed-header" id="myTable">
                <thead class='header'>
                    <tr>
                       
                        <th width="2%">NO</th>
                        <th width="2%">No HP</th>
                        <th width="20%">Isi</th>
                        <th width="5%">Jam Terkirim</th>
                    </tr>
                </thead>
                <tbody>
                   <?php $no=1; $m=0;$k=0;foreach($test as $row){?>
              <tr>
                <td><?php echo $no;?></td>
                <td><?php echo $row->DestinationNumber;?></td>
                <td><?php echo str_replace('|', '', $row->TextDecoded);?></td>
                <td><?php echo date('d-m-Y H:m:s',strtotime($row->SendingDateTime))?></td>
              </tr>
            <?php $no++;}?>
            <tr>
                </tbody>
            </table>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
  th {
    text-align: center;
  }
  .kiri{
    text-align: right;
  }
</style>