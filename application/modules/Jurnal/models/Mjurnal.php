<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mjurnal extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->nip  =$this->session->userdata('NIP');
         $this->upt  =$this->session->userdata('UNIT_UPT_ID');
        // $this->role =$this->session->userdata('MS_ROLE_ID');
    }     
    
    function total_rows_rekap_jurnal($tahun = NULL,$bulan = NULL) {
    $this->db->from('LK_JURNAL');
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_jurnal($limit, $start = 0, $tahun = NULL,$bulan = NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("*");    
    $this->db->order_by('JURNAL_ID', 'ASC');
    $this->db->limit($limit, $start);
    return $this->db->get('LK_JURNAL')->result();
    }
    function get_all_rekap_restoran($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                        JUMLAH_MEJA,TARIF_RATA_RATA,MEJA_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_RESTO");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($this->upt==2) {
        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
    } else {
         if ($kecamatan==NULL) {
            $this->db->where("KODE_UPT",$this->upt);            
        }else {
            $this->db->where("KODEKEC",$kecamatan);
        }
    }
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_RESTORAN')->result();
    }    
    function sum_tagihan_restoran($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($this->upt==2) {
        if ($kecamatan!=NULL){$this->db->where("KODEKEC",$kecamatan);}
    } else {
         if ($kecamatan==NULL) {
            $this->db->where("KODE_UPT",$this->upt);            
        }else {
            $this->db->where("KODEKEC",$kecamatan);
        }
    }
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_RESTORAN')->row();
    }

}