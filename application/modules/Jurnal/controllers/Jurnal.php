<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mjurnal');
        $this->load->model('Master/Mpokok');
 }
 function index()
 {
       
       $this->template->load('Welcome/halaman','sptpd_list');
    }
 
    function jurnal_manual_anggaran(){    
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Jurnal/jurnal_manual?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan);
            $config['first_url'] = base_url() . 'Jurnal/jurnal_manual?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan);
            $cetak='';// base_url() . 'Excel/Excel/Excel_jurnal_manual?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $config['base_url']  = base_url() . 'Jurnal/jurnal_manual';
            $config['first_url'] = base_url() . 'Jurnal/jurnal_manual';
            $cetak= '';//base_url() . 'Excel/Excel/Excel_laporan_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan));            
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mjurnal->total_rows_rekap_jurnal($tahun,$bulan);
        $jurnal                 = $this->Mjurnal->get_limit_data_rekap_jurnal($config['per_page'], $start, $tahun,$bulan);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Jurnal Anggaran',
            'jurnal'            => $jurnal,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'cetak'            => $cetak
        );
      // $data['total_tagihan']=$this->Mjurnal->sum_tagihan_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $this->template->load('Welcome/halaman','jurnal_manual_anggaran',$data);
    }

    function jurnal_manual_penyesuaian(){    
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Jurnal/jurnal_manual?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan);
            $config['first_url'] = base_url() . 'Jurnal/jurnal_manual?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan);
            $cetak='';// base_url() . 'Excel/Excel/Excel_jurnal_manual?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $config['base_url']  = base_url() . 'Jurnal/jurnal_manual';
            $config['first_url'] = base_url() . 'Jurnal/jurnal_manual';
            $cetak= '';//base_url() . 'Excel/Excel/Excel_laporan_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan));            
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mjurnal->total_rows_rekap_jurnal($tahun,$bulan);
        $jurnal                 = $this->Mjurnal->get_limit_data_rekap_jurnal($config['per_page'], $start, $tahun,$bulan);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Jurnal',
            'jurnal'            => $jurnal,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'cetak'            => $cetak
        );
      // $data['total_tagihan']=$this->Mjurnal->sum_tagihan_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $this->template->load('Welcome/halaman','jurnal_manual',$data);
    }

    function jurnal_otomatis(){
       $data['kj']=$this->db->query("select * from lk_kategori_jurnal")->result();
       $data['list_jurnal']=$this->db->query("select jurnal_akun,jurnal_uraian,jurnal_lawan,jurnal_rek,jurnal_kategori,
                                    sum(jurnal_debet)debet,sum(jurnal_kredit)kredit   from Lk_jurnal
                                     where jurnal_kategori='01'
                                     group by jurnal_akun,jurnal_uraian,jurnal_lawan,jurnal_rek,jurnal_kategori
                                     order by jurnal_rek,kredit")->result();
       $this->template->load('Welcome/halaman','jurnal_otomatis',$data);
    }
    function detail_jurnal_otomatis($akun='',$lawan=''){
       $data['kj']=$this->db->query("select * from lk_kategori_jurnal")->result();
       $data['detail_jurnal']=$this->db->query("select * from lk_jurnal 
                                              where (jurnal_akun='$akun' and jurnal_lawan='$lawan')
                                              or (jurnal_akun='$lawan' and jurnal_lawan='$akun') 
                                              order by jurnal_id,jurnal_rek,jurnal_tgl")->result();
       $this->template->load('Welcome/halaman','detail_jurnal_otomatis',$data);
    }

    function buku_besar(){
       $data['kj']=$this->db->query("select * from lk_kategori_jurnal")->result();
       $data['list_jurnal']=$this->db->query("select * from lk_coa")->result();
       $this->template->load('Welcome/halaman','buku_besar',$data);
    }

    function detail_buku_besar($akun='',$lawan=''){
       $data['kj']=$this->db->query("select * from lk_kategori_jurnal")->result();
       $data['detail_jurnal']=$this->db->query("select * from lk_jurnal 
                                              where jurnal_akun='$akun'
                                              order by jurnal_id,jurnal_rek,jurnal_tgl")->result();
       $this->template->load('Welcome/halaman','detail_buku_besar',$data);
    }

    function neraca(){
       $data['kj']=$this->db->query("select * from lk_kategori_jurnal")->result();
       $data['list_neraca']=$this->db->query("select * from lk_neraca")->result();
       $this->template->load('Welcome/halaman','neraca',$data);
    }

     function anggaran(){
       $data['anggaran']=$this->db->query("select distinct a.*,B.NAMA_PAJAK,C.NAMA_GOLONGAN
                                            from target_bapenda a
                                            join jenis_pajak b on A.JENIS_PAJAK=b.id_inc
                                            join golongan c on C.NEWKOREK2=a.rekening and a.jenis_pajak=C.ID_RINCIAN_ANGGARAN
                                            order by a.jenis_pajak")->result();
       $this->template->load('Welcome/halaman','anggaran',$data);
    }

   // account_coa
    
  
}