   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().''?>">
                <input type='hidden' value='1' name='cek'>
                <div class="form-group">
                  <select  name="tahun" required="required" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('h_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select  name="bulan" required="required" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php if ($this->session->userdata('h_bulan')==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($cek <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan_sptpd/laporan_restoran'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                              <?php }   ?>  
                                    <a href="<?php echo $cetak; ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tgl</th>
              <th class="text-center">Akun</th>
              <th class="text-center">Keterangan</th>
              <th class="text-center">Debet</th>
              <th class="text-center">Kredit</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($jurnal as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td align="center"><?= $rk->JURNAL_TGL?></td>
                <td><?php if ($rk->JURNAL_DEBET!=0) {
                    echo $rk->JURNAL_AKUN;
                } else {echo "&emsp;".$rk->JURNAL_AKUN;}?></td>
                <td><?php if ($rk->JURNAL_DEBET!=0) {
                    echo $rk->JURNAL_URAIAN;
                } else {echo "&emsp;".$rk->JURNAL_URAIAN;}?></td>
                <td align="right"><?= number_format($rk->JURNAL_DEBET,'0','','.')?></td></td>
                <td align="right">&emsp;<?= number_format($rk->JURNAL_KREDIT,'0','','.')?></td></td>
                 <td align="center"><?= $rk->JURNAL_KODE_BILING?></td>
                <td></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
         <!--  <button  class="btn  btn-space btn-success" disabled>Total Tagihan : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button> -->
          <div class="float-right">
           <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

