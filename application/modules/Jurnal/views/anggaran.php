   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Anggaran</h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Jurnal/jurnal_otomatis'?>">
               
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tahun</th>
              <th class="text-center">Pajak</th>
              <th class="text-center">Kategori</th>
              <th class="text-center">Jumlah Anggaran</th>
              <th class="text-center">Status</th> 
              <th class="text-center">Aksi</th> 
            </tr>
          </thead>
            <tbody>
              <?php $no=1; $sa=0;$d=0;$k=0;foreach($anggaran as $rk){?>
              <tr>
                <td align='center'><?php echo $no;?></td>
                <td><?= $rk->TAHUN_PAJAK ?></td>
                <td><?= $rk->NAMA_PAJAK ?></td>
                <td><?= $rk->NAMA_GOLONGAN ?></td>
                <td align="right"><?= number_format($rk->TARGET,'0','','.')?></td>
                <td></td>
                <td></td>               
              </tr>
            <?php $no++;}?>
           
            </tbody>
          </table>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

