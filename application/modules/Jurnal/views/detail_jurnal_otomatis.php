   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Detail Jurnal Otomatis</h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <table class="table table-hover table-striped" border="0" width="450" style="margin-left:0px;">
            <tr>
                <td width='100'><b><font style='font-size: ;'>KODE AKUN</font></b></td>
                <td ><b>: <?php echo $detail->jurnalb_reff;?></b></td>
            </tr>
            <tr>
                <td width='100'><b><font style='font-size: ;'>NAMA AKUN</font></b></td>
                <td ><b>: <?php echo $detail->uraian;?></b></td>
            </tr>
            <tr>
                <td ><b><font style='font-size: ;'></font></b></td>
                <td ><b>: </b></td>
            </tr>
            <tr>
                <td ><b><font style='font-size: ;'></font></b></td>
                <td ><b>: <?php echo 'Bulan '.$this->session->userdata("periode_bulan_gd").'/'.$this->session->userdata("periode_tahun_gd");;?></b></td>
            </tr>
          </table>    
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Tgl</th>
              <th class="text-center">Akun</th>
              <th class="text-center">Keterangan</th>
              <th class="text-center">Debet</th>
              <th class="text-center">Kredit</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1; $sa=0;$d=0;$k=0;foreach($detail_jurnal as $rk){?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td align="center"><?= $rk->JURNAL_TGL?></td>
                <td><?php if ($rk->JURNAL_DEBET!=0) {
                    echo $rk->JURNAL_AKUN;
                } else {echo "&emsp;".$rk->JURNAL_AKUN;}?></td>
                <td><?php if ($rk->JURNAL_DEBET!=0) {
                    echo $rk->JURNAL_URAIAN;
                } else {echo "&emsp;".$rk->JURNAL_URAIAN;}?></td>
                <td align="right"><?= number_format($rk->JURNAL_DEBET,'0','','.')?></td></td>
                <td align="right">&emsp;<?= number_format($rk->JURNAL_KREDIT,'0','','.')?></td></td>
                 <td align="center"><?= $rk->JURNAL_KODE_BILING?></td>
                <td></td>
              </tr>
            <?php $d +=$rk->JURNAL_DEBET;$k +=$rk->JURNAL_KREDIT;$no++;}?>
            <tr>
              <td colspan='4'><b>TOTAL</b></td>
              <td align='right'><?= number_format($d,'0',',','.');?></td>
              <td align='right'><?= number_format($k,'0',',','.');?></td>
            </tr>
            </tbody>
          </table>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

