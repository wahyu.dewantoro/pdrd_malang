<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Master/Mpokok');
        /*$this->load->library('datatables');*/
        $this->load->model('Mpdf');
 }

  function Pdf_laporan_data_ketetapan(){
        $t1 =$this->session->userdata('data_ketetapan_bulan');
        $t2 =$this->session->userdata('data_ketetapan_tahun');
        $jp =$this->session->userdata('ketetapan_jenis_pajak');
           /*if ($jp==8) {*/
                    $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND JENIS_PAJAK='$jp'";        
                    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP||' / '||OBJEK_PAJAK NAMA_WP,ALAMAT_WP, '' DPP,TAGIHAN PAJAK_TERUTANG,STATUS,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') TGL_KETETAPAN, TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') JATUH_TEMPO");
                    $this->db->where($wh);
                    $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
                    $data['data']=$this->db->get('TAGIHAN')->result();
            $html     =$this->load->view('Pdf/Pdf_laporan_data_ketetapan',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
  }
  function bap_perforasi($id=""){
            $data['data']=$this->db->query("SELECT * from PERFORASI WHERE no_permohonan='$id' order by sort,id_inc")->result();
            $data['detail']=$this->db->query("select a.npwp,NAMA_WP_PERF nama,alamat,a.jabatan,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,NAMA_USAHA,
                                               NOMOR_INC_BLN||' / '||LPAD(NOMOR_INC_THN, 3, '0')||' / '||NOMOR_OBJEK||' / '||NOMOR_BLN_THN NO_PERMOHONAN,c.NAMA pengatur_muda,c.JABATAN JABATAN_PEGAWAI,c.nip
                                                from wajib_pajak a
                                                join nomor_perforasi b on A.NPWP=b.npwpd
                                                join pegawai c on c.id_inc=b.pengatur_muda
                                                where b.id_inc='$id'")->row();
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $html     =$this->load->view('Pdf/Pdf_bap_perforasi',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = "bap".$id.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
  }
  function permohonan_perforasi($id=""){
            $data['data']=$this->db->query("SELECT * from PERFORASI WHERE no_permohonan='$id' order by sort,id_inc")->result();
            $data['detail']=$this->db->query("select npwp,NAMA_WP_PERF nama,alamat,jabatan,to_char(TGL_PERFORASI, 'dd-mm-yyyy ') TGL,NAMA_USAHA,
                                                NOMOR_INC_BLN||' / '||LPAD(NOMOR_INC_THN, 3, '0')||' / '||NOMOR_OBJEK||' / '||NOMOR_BLN_THN NO_PERMOHONAN
                                                from wajib_pajak a
                                                join nomor_perforasi b on A.NPWP=b.npwpd
                                                where id_inc='$id'")->row();
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $html     =$this->load->view('Pdf/Pdf_permohonan_perforasi',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = $id.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
  }

  function pdf_kode_biling($kode_biling=""){
            $data['catatan']=$this->Mpokok->getCatatan();
            $data['cara_bayar']=$this->db->query("select * from ms_catatan where keperluan='va' ")->result();
            $kode_biling=rapikan($kode_biling);
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Pdf/Pdf_kode_biling',$data,true);
            
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    function pdf_bukti_pembayaran($kode_biling=""){
            $data['catatan']=$this->Mpokok->getCatatan();
            $data['cara_bayar']=$this->db->query("select * from ms_catatan where keperluan='va' ")->result();
            $kode_biling=rapikan($kode_biling);
            $data['wp']=$this->db->query("select * from V_GET_PEMBAYARAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Pdf/pdf_bukti_pembayaran',$data,true);
            $qr=$this->db->query("select PENGESAHAN from sptpd_pembayaran where kode_biling='$kode_biling'")->row();
            $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/ntpd/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $qr->PENGESAHAN; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
            
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
function Pdf_peringatan_belum_lapor($NPWP=""){
            
            $data['wp']=$this->db->query("select npwpd,nama_wp,alamat_wp,objek_pajak,alamat_op,nomor_berkas,masa_pajak,TAHUN_PAJAK from tagihan where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Pdf/Pdf_peringatan_belum_lapor',$data,true);
            
            $filename = $NPWP.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
function Pdf_peringatan_belum_bayar($kode_biling=""){
             $cek=$this->db->query("select jenis_pajak from tagihan where kode_biling='$kode_biling'")->row();
            $data['jabatan']=$this->Mpokok->getJabatan('PUM');
            $data['wp']=$this->db->query("select npwpd,nama_wp,alamat_wp,objek_pajak,alamat_op,nomor_berkas,masa_pajak,TAHUN_PAJAK,jenis_pajak from tagihan where kode_biling='$kode_biling'")->row();
            if ($cek->JENIS_PAJAK=='08' OR $cek->JENIS_PAJAK=='04') {
              $html     =$this->load->view('Pdf/Pdf_peringatan_belum_bayar_skpd',$data,true);
            } else {
              $html     =$this->load->view('Pdf/Pdf_peringatan_belum_bayar_sptpd',$data,true);
            }
            
            $filename = "teguran";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }

    function pdf_skpd_air_meter($kode_biling=""){
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select NOMOR_BERKAS,HARGA,A.KODE_BILING,a.npwpd,a.nama_wp,a.alamat_wp,objek_pajak,alamat_op,a.MASA_PAJAK,a.TAHUN_PAJAK,a.DENDA,a.POTONGAN,PAJAK,
                                            CEIL(tagihan-a.POTONGAN) PAJAK_TERUTANG,namakec,namakelurahan,a.kode_rek,jenis,cara_pengambilan,volume_air_meter,DPP,
                                            penggunaan_bulan_lalu_meter,penggunaan_hari_ini_meter,jenispengambilan
                                                from tagihan A
                                                JOIN SPTPD_air_tanah b on a.kode_biling=b.kode_biling
                                                 JOIN KECAMATAN c  ON A.KODEKEC = c.KODEKEC 
                                                 JOIN KELURAHAN d ON A.KODEKEL = d.KODEKELURAHAN AND A.KODEKEC = d.KODEKEC
                                                 join peruntukan_air_tanah e on e.no=b.peruntukan
                                                where a.kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Pdf/Pdf_skpd_air_meter',$data,true);
              $qr=$this->db->query("select npwpd,nama_wp,MASA_PAJAK,TAHUN_PAJAK,kode_rek from tagihan where kode_biling='$kode_biling'")->row();
              $this->db->query("UPDATE TAGIHAN SET CETAK_SKPD='1' WHERE KODE_BILING='$kode_biling'");
              $jabatan=$this->Mpokok->getJabatan('kabid pdrd');
              $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $qr->KODE_REK.$qr->NPWPD.$qr->MASA_PAJAK.$qr->TAHUN_PAJAK.$jabatan->NIP; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

               $confi['cacheable']  = true; //boolean, the default is true
              $confi['cachedir']   = '/assets/'; //string, the default is application/cache/
              $confi['errorlog']   = '/assets/'; //string, the default is application/logs/
              $confi['imagedir']   = '/assets/images/ttd/'; //direktori penyimpanan qr code
              $confi['quality']    = true; //boolean, the default is true
              $confi['size']     = '1024'; //interger, the default is 1024
              $confi['black']    = array(224,255,255); // array, default is array(255,255,255)
              $confi['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($confi);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $param['data'] = 'http://36.89.91.154:8080/pdrd/Stiker/stiker/Pdf_skpd_air_meter/'.$kode_biling; //data yang akan di jadikan QR CODE
              $param['level'] = 'H'; //H=High
              $param['size'] = 10;
              $param['savename'] = FCPATH.$confi['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($param); // fungsi untuk generate QR CODE
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    function pdf_skpd_air_non_meter($kode_biling=""){
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select NOMOR_BERKAS,HARGA,A.KODE_BILING,a.npwpd,a.nama_wp,a.alamat_wp,objek_pajak,alamat_op,a.MASA_PAJAK,a.TAHUN_PAJAK,a.DENDA,ceil(a.POTONGAN)POTONGAN,PAJAK,
                                            CEIL(tagihan-a.POTONGAN)PAJAK_TERUTANG,namakec,namakelurahan,a.kode_rek,jenis,cara_pengambilan,volume_air_meter,DPP,
                                            jenispengambilan,debit_non_meter,penggunaan_hari_non_meter,penggunaan_bulan_non_meter
                                                from tagihan A
                                                JOIN SPTPD_air_tanah b on a.kode_biling=b.kode_biling
                                                 JOIN KECAMATAN c  ON A.KODEKEC = c.KODEKEC 
                                                 JOIN KELURAHAN d ON A.KODEKEL = d.KODEKELURAHAN AND A.KODEKEC = d.KODEKEC
                                                 join peruntukan_air_tanah e on e.no=b.peruntukan
                                                where a.kode_biling='$kode_biling'")->row();
              $qr=$this->db->query("select npwpd,nama_wp,MASA_PAJAK,TAHUN_PAJAK,kode_rek from tagihan where kode_biling='$kode_biling'")->row();
              $this->db->query("UPDATE TAGIHAN SET CETAK_SKPD='1' WHERE KODE_BILING='$kode_biling'");
              $jabatan=$this->Mpokok->getJabatan('kabid pdrd');
              $html     =$this->load->view('Pdf/Pdf_skpd_air_non_meter',$data,true);
              $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $qr->KODE_REK.$qr->NPWPD.$qr->MASA_PAJAK.$qr->TAHUN_PAJAK.$jabatan->NIP; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

              $confi['cacheable']  = true; //boolean, the default is true
              $confi['cachedir']   = '/assets/'; //string, the default is application/cache/
              $confi['errorlog']   = '/assets/'; //string, the default is application/logs/
              $confi['imagedir']   = '/assets/images/ttd/'; //direktori penyimpanan qr code
              $confi['quality']    = true; //boolean, the default is true
              $confi['size']     = '1024'; //interger, the default is 1024
              $confi['black']    = array(224,255,255); // array, default is array(255,255,255)
              $confi['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($confi);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $param['data'] = 'http://36.89.91.154:8080/pdrd/Stiker/stiker/pdf_skpd_air_non_meter/'.$kode_biling; //data yang akan di jadikan QR CODE
              $param['level'] = 'H'; //H=High
              $param['size'] = 10;
              $param['savename'] = FCPATH.$confi['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($param); // fungsi untuk generate QR CODE
            
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    function pdf_skpd_reklame($kode_biling=""){
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select nomor_berkas,A.KODE_BILING,a.npwpd,a.nama_wp,a.alamat_wp,objek_pajak,alamat_op,a.MASA_PAJAK,
                a.TAHUN_PAJAK,namakec,namakelurahan,NOREK,tagihan pajak_terutang, TO_CHAR (e.BERLAKU_MULAI, 'dd-mm-yyyy') BERLAKU_MULAI,
                                                TO_CHAR (akhir_masa_berlaku, 'dd-mm-yyyy') AKHIR_MASA_BERLAKU
                                                from tagihan A
                                                  JOIN SPTPD_reklame b on a.kode_biling=b.kode_biling
                                                   join sptpd_reklame_detail e on E.SPTPD_REKLAME_ID=b.id_inc
                                                  left JOIN KECAMATAN c  ON a.kodekec = c.KODEKEC 
                                                 left JOIN KELURAHAN d ON a.kodekel = d.KODEKELURAHAN AND a.kodekec = d.KODEKEC
                                                where a.kode_biling='$kode_biling'")->row();
            $data['data']=$this->db->query("SELECT * FROM V_DET_REKLAME WHERE KODE_BILING='$kode_biling'")->result();
            $this->db->query("UPDATE TAGIHAN SET CETAK_SKPD='1' WHERE KODE_BILING='$kode_biling'");
            $html     =$this->load->view('Pdf/Pdf_skpd_reklame',$data,true);
              $qr=$this->db->query("select npwpd,nama_wp,MASA_PAJAK,TAHUN_PAJAK,kode_rek from tagihan where kode_biling='$kode_biling'")->row();

              $jabatan=$this->Mpokok->getJabatan('kabid pdrd');
              $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $qr->KODE_REK.$qr->NPWPD.$qr->MASA_PAJAK.$qr->TAHUN_PAJAK.$jabatan->NIP; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
            
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    function Pdf_form_pendaftaran($npwp=""){            
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling=''")->row();
            $html     =$this->load->view('Pdf/Pdf_formulir_pendaftaran',$data,true);
            
            $filename = "formulir.pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    function Pdf_pengukuhan($usaha=""){
            $data['jabatan']=$this->Mpokok->getJabatan('PUM');
            $data['op']=$this->db->query("select nama_usaha,alamat_usaha,npwp,nama,alamat,TO_CHAR(S_VALID,'dd-Month-yyyy')TGL,S_VALID TGL_APPROVE,TO_CHAR(S_VALID,'yyyy') tahun_aprove
                                          from tempat_usaha a
                                          join wajib_pajak b on A.NPWPD=B.NPWP
                                          where id_inc='$usaha'")->row();
            $html     =$this->load->view('Pdf/Pdf_pengukuhan',$data,true);
            
            $filename = "Pengukuhan-".$npwpd.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    function Pdf_pengukuhan_baru($usaha=""){
            $data['jabatan']=$this->Mpokok->getJabatan('PUM');
            $data['op']=$this->db->query("select nama_usaha,alamat_usaha,npwp,nama,alamat,TO_CHAR(TGL_APPROVE,'dd-Month-yyyy')TGL,TGL_APPROVE,TO_CHAR(S_VALID,'yyyy') tahun_aprove
                                          from tempat_usaha_temp a
                                          join wajib_pajak_temp b on A.NPWPD=B.NPWP
                                          where id_inc='$usaha'")->row();
            $html     =$this->load->view('Pdf/Pdf_pengukuhan',$data,true);
            
            $filename = "Pengukuhan-".$npwpd.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }

    function pdf_skpdkb($kode_biling=""){
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select * from tagihan where kode_biling='$kode_biling'")->row();
            $data['rincian']=$this->db->query("select * from skpdkb_detail where kode_biling='$kode_biling'")->row();
            $data['nama']=$this->db->query("select nama_pajak
                                                from jenis_pajak a
                                                join tagihan b on A.ID_INC=B.JENIS_PAJAK
                                                where kode_biling='$kode_biling'")->row();
            
            //$data['data']=$this->db->query("SELECT * FROM TAGIHAN WHERE KODE_BILING='$kode_biling'")->row();
            $html     =$this->load->view('Pdf/pdf_skpdkb',$data,true);
              /*$qr=$this->db->query("select npwpd,nama_wp,MASA_PAJAK,TAHUN_PAJAK,kode_rek from tagihan where kode_biling='$kode_biling'")->row();*/
              $jabatan=$this->Mpokok->getJabatan('kabid pdrd');
              /*$this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $kode_biling; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE*/
            
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }

    function pdf_stiker_reklame($kode_biling=""){
              $data['kode_biling']=$kode_biling;
              $html     =$this->load->view('Pdf/pdf_qr_stiker',$data,true);
              $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/stiker/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = "http://36.89.91.154:8080/pdrd/Stiker/Stiker/data_pajak/".$kode_biling; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
            
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    
  
}