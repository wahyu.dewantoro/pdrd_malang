
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 14.6px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
@page { margin-bottom:5px; 
margin-top: 15px;}
/*body { margin-bottom: 25px;*/
p{
  line-height: 12px;
}
br{
  line-height: 15px;
}
</style>
</style>
</head>

<body>
<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ;
      function tglIndonesia($str){
       $tr   = trim($str);
       $str    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);
       return $str;
       //call funtion
        tglIndonesia(date('D, d F, Y'));}?>
<table width="100%" border="0" cellspacing="1" cellpadding="0" >
  <tr>
            <td width="23%" rowspan="4" align="center" valign="top"><img src="assets/img/mlg.png" width="80" height="90" /></td>
            <td align="center" valign="top"><strong>PEMERINTAH KABUPATEN MALANG</strong></td>
            <td width="21%" rowspan="4" align="center" valign="top"></td>
  </tr>
          <tr>
            <td align="center" > <strong>BADAN PENDAPATAN DAERAH</strong></td>
          </tr>
          <tr>
            <td align="center">Jalan Raden Panji No. 158 Kepanjen - Telp (0341) 3904898</td>
          </tr>
          <tr>
            <td width="56%" align="center"><b>  KEPANJEN -</b> 65163</td>
          </tr>
        </table>
        <hr>
  <table width="100%" border="0" cellspacing="4" cellpadding="0">
          <tr>
            <td height="11" align="center" valign="top"><strong>KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH</strong></td>
          </tr>
          <tr>
            <td height="11" align="center" valign="top"><strong>KABUPATEN MALANG</strong></td>
          </tr>
  <tr>
            <td height="11" align="center" valign="top"><strong>Nomor :       970 / <?= (int)substr($op->NPWP,1,7);?> /35.07.205/SK/<?= $op->TAHUN_APROVE?></strong></td>
  </tr>
  <tr>
    <td height="11" align="center" valign="top"><strong>TENTANG</strong></td>
  </tr>
  <tr>
    <td height="11" align="center" valign="top"><strong>PENGUKUHAN SEBAGAI WAJIB PAJAK DAERAH</strong></td>
  </tr>
  <tr>
    <td height="2" align="center" valign="top"></td>
  </tr>
  <tr>
            <td height="11" align="center" valign="top"><strong>KEPALA BADAN PENDAPATAN DAERAH,</strong></td>
  </tr>
        </table>
<!-- <table width="100%" border="0" cellspacing="4" cellpadding="0">
  <tr>
    <td height="35" colspan="7" align="center" valign="middle"><table width="100%" border="0" cellspacing="4" cellpadding="0">
        <tr>
          <td width="2%" height="1">&nbsp;</td>
          <td width="9%">Membaca</td>
          <td>:</td>
          <td colspan="2">  </td>
        </tr>
         <tr>
          <td width="2%" height="1">&nbsp;</td>
          <td width="9%"></td>
          <td></td>
          <td colspan="2"></td>
        </tr>
    </table> -->
    <table width="100%" border="0" cellspacing="4" cellpadding="">
        <tr>
          <td width="2%" height="1"></td>
          <td width="9%">Membaca</td>
          <td></td>
          <td></td>
          <td>:</td>
          <td>Surat Pendaftaran Wajib Pajak Daerah yang disampaikan kepada Badan Pendapatan Daerah</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td width="1%">&nbsp;</td>
          <td width="1%"></td>
          <td width="1%"></td>
          <td width="87%"> Kabupaten Malang Nomor : <?= (int)substr($op->NPWP,1,7)?> Tanggal <?= str_replace('-',' ',str_replace(' ','',tglIndonesia($op->TGL)))?></td>
        </tr>
    </table>
     <table width="100%" border="0" cellspacing="4" cellpadding="">
        <tr>
          <td width="2%" height="1"></td>
          <td width="9%">Menimbang</td>
          <td>:</td>
          <td>a.</td>
          <td>bahwa yang bersangkutan telah memenuhi persyaratan baik subyek maupun obyek Pajak Daerah.</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td width="1%">&nbsp;</td>
          <td width="1%">b.</td>
          <td width="87%">bahwa sehubungan dengan itu perlu menetapkan pengukuhan yang bersangkutan menjadi </td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td width="1%">&nbsp;</td>
          <td width="1%"></td>
          <td width="87%">Wajib Pajak Daerah.</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Mengingat</td>
          <td>:</td>
          <td>1</td>
          <td> Undang-Undang Republik Indonesia Nomor 28 Tahun 2009 Tentang Pajak Daerah dan Retribusi</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td></td>
          <td> Daerah ;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>2</td>
          <td>Peraturan Pemerintah Republik Indonesia Nomor 55 Tahun 2016 Tentang Ketentuan Umum dan </td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td></td>
          <td> Tata Cara Pemungutan Pajak Daerah;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>3</td>
          <td>Peraturan Daerah Kabupaten Malang Nomor 9 Tahun 2016 Tentang Pembentukan Susunan</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td></td>
          <td> Perangkat Daerah ;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>4</td>
          <td>Peraturan Daerah Kabupaten Malang Nomor 1 Tahun 2019 Tentang Perubahan atas Peraturan </td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td></td>
          <td> Daerah Nomor 8 Tahun 2010 Tentang Pajak Daerah ;</td>
        </tr>
    </table>
    </td>
  </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
    </table></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="4" cellpadding="0">
        <tr>
          <td width="2%" height="1"></td>
          <td colspan="4"><div align="center"><strong>M E M U T U S K A N</strong></div></td>
        </tr>
        <tr>
          <td height="1"></td>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td width="9%">MENETAPKAN</td>
          <td>:</td>
          <td colspan="2"><!-- SURAT KEPUTUSAN KEPALA BADAN PENDAPATAN DAERAH  --> </td>
        </tr>
        <!-- <tr>
          <td height="1"></td>
          <td width="9%"></td>
          <td></td>
          <td colspan="2">KABUPATEN MALANG TENTANG PENGUKUHAN SEBAGAI WAJIB PAJAK DAERAH.</td>
        </tr> -->
        <tr>
          <td height="1">&nbsp;</td>
          <td>PERTAMA</td>
          <td width="1%">:</td>
          <td width="30%">Mengukuhkan :</td>
          <td width="69%">&nbsp;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Nama / Merk Usaha</td>
          <td>: <?= $op->NAMA_USAHA?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Alamat</td>
          <td>: <?= $op->ALAMAT_USAHA?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Nomor Pokok Wajib Pajak Daerah (NPWPD)</td>
          <td>: <?= substr($op->NPWP,0,1).' '.substr($op->NPWP,1,2).' '.substr($op->NPWP,3,5).' '.substr($op->NPWP,8,4)?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Nama Penanggung Pajak </td>
          <td>: <?= $op->NAMA?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Alamat</td>
          <td>: <?= $op->ALAMAT?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>Sebagai Wajib Pajak Daerah.</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>KEDUA</td>
          <td>:</td>
          <td colspan="2">Keputusan  Kepala Badan Pendapatan Daerah ini Berlaku Sejak Tanggal Ditetapkan</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>KETIGA</td>
          <td>:</td>
          <td colspan="2">Apabila dikemudian hari terdapat kekeliruan dalam Surat Keputusan ini akan diadakan </td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td></td>
          <td></td>
          <td colspan="2">pembetulan seperlunya.</td>
        </tr>
        <tr>
          <td height="1"></td>
          <td></td>
          <td></td>
          <td colspan="2"></td>
        </tr>
    </table>
    <br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td colspan="2" rowspan="13"></td>
        <td width="20%">ditetapkan</td>
        <td width="27%">: K E P A N J E N</td>
      </tr>
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td>pada tanggal</td>
        <td>: <?= str_replace('-',' ',str_replace(' ','',tglIndonesia($op->TGL)))?></td>
      </tr>
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td colspan="2"><div align="center"><strong>Kepala Badan Pendapatan Daerah</strong></div></td>
      </tr>
      <tr>
        <td width="2%" height="1">&nbsp;</td>
        <td width="33%">&nbsp;</td>
        <td colspan="2"><div align="center"><strong>Kabupaten Malang</strong></div></td>
  </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2"><div align="center"></div></td>
  </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
  </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2" rowspan="2"><div align="center"></div></td>
      </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
  </tr>
      <!-- <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2"><div align="center"></div></td>
      </tr> -->
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2"><div align="center"><strong><u>
           <?= $jabatan->NAMA?>
        </u></strong></div></td>
      </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2"><div align="center">Pembina Utama Muda</div></td>
      </tr>
      <tr>
        <td height="18">&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2"><div align="center">NIP.   <?=$jabatan->NIP?></div></td>
      </tr>
</table>
</body>
</html>
