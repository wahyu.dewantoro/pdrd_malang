
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 11px;
}

.tbody{
  font-size: 9.5px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
@page { margin-bottom:75px; }
body { margin-bottom: 75px;
p{
  line-height: 0.2cm;
}
</style>
</style>
</head>

<body>
<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ;
      function tglIndonesia($str){
       $tr   = trim($str);
       $str    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);
       return $str;
       //call funtion
        tglIndonesia(date('D, d F, Y'));}
         error_reporting(E_ALL ^ E_NOTICE);?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
           <td width="23%" rowspan="7" align="center" valign="top"><p><img src="assets/img/mlg.png" width="80" height="90" /></p></td>
            <td align="center" valign="top"><strong>PEMERINTAH KABUPATEN MALANG</strong></td>
            <td width="21%" rowspan="7" align="center" valign="top"><img src="assets/images/<?= $detail->KODE_BILING?>.png" width="83" height="83" /></td>
  </tr>
          <tr>
            <td align="center" valign="top"> <strong>BADAN PENDAPATAN DAERAH KAB. MALANG</strong></td>
          </tr>
          <tr>
            <td align="center" valign="top">Jalan Raden Panji No. 158 Kepanjen - Telp (0341) 3904898 </td>
          </tr>
          <tr>
            <td width="56%" align="center" valign="top"><b> KEPANJEN -</b> 65163</td>
          </tr>
          <tr>
            <td height="11" align="center" valign="top"><strong>SURAT KETETAPAN PAJAK DAERAH (SKPD)</strong></td>
          </tr>
          <tr>
            <td height="11" align="center" valign="top">PAJAK REKLAME (<?= $detail->KODE_BILING?>)</td>
          </tr>
  <tr>
            <td height="11" align="center" valign="top">(Peraturan Daerah Kabupaten Malang No. 8 Tahun 2010) </td>
  </tr>
        </table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td height="50" align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="1%" height="1"></td>
          <td width="18%">Kepada Yth. Sdr</td>
          <td width="45%">: <?= $detail->NAMA_WP?></td>
          <td width="18%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Nama Badan</td>
          <td>: <?= $detail->OBJEK_PAJAK?></td>
          <td>No. Rekening</td>
          <td>: <?= $detail->NOREK?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Alamat</td>
          <td>: <?= $detail->ALAMAT_OP?></td>
          <td>No. Berkas</td>
          <td>: <?= sprintf('%04d', $detail->NOMOR_BERKAS)?></td>
        </tr>
        <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
        <tr>
          <td height="1">&nbsp;</td>
          <td>Kelurahan</td>
          <td>: <?= $detail->NAMAKELURAHAN?></td>
          <td>Kode Biling</td>
          <td>: <?= $detail->KODE_BILING?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Kecamatan</td>
          <td>: <?= $detail->NAMAKEC?></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>NPWD</td>
          <td>: <?= $detail->NPWPD?></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12%" height="13">&nbsp;</td>
        <td width="10%"><strong>Masa Berlaku</strong></td>
        <td>: <?= $detail->BERLAKU_MULAI?> S/d <?= $detail->AKHIR_MASA_BERLAKU?>
          <div align="center"></div></td>
        </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
    </table></td>
  </tr>
  <!-- <tr>
    <td height="44"></td>
  </tr>
  <tr>
    <td height="240"></td>
  </tr>
  <tr>
    <td></td>
  </tr> -->
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="13%" rowspan="2"><div align="center">Jenis Reklame/Klasifikasi</div></td>
        <td width="13%" rowspan="2"><div align="center">Teks Sebutan</div></td>
        <td width="18%" rowspan="2"><div align="center">Lokasi Pasang</div></td>
        <td height="33" colspan="4"><div align="center">Ukuran</div></td>
        <td width="8%" rowspan="2"><div align="center">Luas ( M2)</div></td>
        <td width="8%" rowspan="2"><div align="center">N.S ( Rp )</div></td>
        <td width="10%" rowspan="2"><div align="center">NJOP ( Rp )</div></td>
        <td width="5%" rowspan="2"><div align="center">Rokok</div></td>
        <td width="5%" rowspan="2"><div align="center">Ketinggian</div></td>
        <td width="15%" rowspan="2"><div align="center">Pajak Terutang</div></td>
      </tr>
      <tr>
        <td width="3%" height="19"><div align="center">Pjg</div></td>
        <td width="3%"><div align="center">Lbr</div></td>
        <td width="3%"><div align="center">Sisi</div></td>
        <td width="3%"><div align="center">Jml</div></td>
      </tr>
      <tbody class="tbody">
      <?php $tot=0;$jm_ps=0;$no=1;foreach ($data as $rk_d)  { ?>
      <tr>
        <td>&nbsp;<?= $rk_d->DESKRIPSI?></td>
        <td>&nbsp;<?= $rk_d->TEKS?></td>
        <td>&nbsp;<?= $rk_d->LOKASI_PASANG.' Ds.'.$rk_d->NAMAKELURAHAN.' KEC.'.$rk_d->NAMAKEC?></td>
        <td>&nbsp;<?= number_format($rk_d->P,1,",",".");?></td>
        <td>&nbsp;<?= number_format($rk_d->L,1,",",".");?></td>
        <td>&nbsp;<?= $rk_d->S?></td>
        <td>&nbsp;<?= $rk_d->JUMLAH?></td>
        <td align="right"><?=  number_format($rk_d->P*$rk_d->L*$rk_d->S,1,",",".")?>&nbsp;</td>
        <td align="right"><?=  number_format($rk_d->NILAI_STRATEGIS,1,",",".")?> &nbsp;</td>
        <td align="right"><?=  number_format($rk_d->NJOP,1,",",".")?> &nbsp;</td>
        <td align="center"><?php if ($rk_d->ROKOK==1) {
          echo "Ya";
        } else {
          echo "Tdk";
        }?></td>
       <td align="center"><?php if ($rk_d->KETINGGIAN==1) {
          echo "Ya";
        } else {
          echo "Tdk";
        }?></td>
        <td align="right"><?=  number_format($rk_d->PAJAK_TERUTANG,1,",",".")?> &nbsp;</td>
      </tr>
      <?php $tot+=str_replace(",",".",$rk_d->PAJAK_TERUTANG);$jm_ps+=$rk_d->JUMLAH; }?>
      </tbody>
      <tr>
        <td height="1" colspan="5"><div align="right"><strong>Jumlah</strong></div></td>
        <td>&nbsp;</td>
        <td align="right"><?=  number_format($jm_ps)?> &nbsp;</td>
        <td colspan="5">&nbsp;</td>
        <td align="right"><?=  number_format(ceil($tot),1,",",".")?> &nbsp;</td>
      </tr>
    </table>
    <table width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" height="13">&nbsp;</td>
        <td width="8%">Terbilang</td>
        <td width="88%">: <strong><?= terbilang(ceil($tot));?> Rupiah</strong></td>
        <td width="2%"><div align="center"></div></td>
      </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
    </table>
<br>
<div class="footer">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  
  <tr>
    <td height="10" colspan="3" valign="top" align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="1" colspan="2"></td>
        <td colspan="2" rowspan="11"></td>
        <td width="43%"><div align="center">Malang, <?= tglIndonesia(date('d-F-Y'))?></div></td>
      </tr>
      <tr>
        <td height="1" colspan="2">Catatan</td>
        <td><div align="center"><strong>Kepala Bidang Pajak Daerah dan Retribusi Daerah</strong></div></td>
        </tr>
      <tr>
        <td width="2%" height="1"><div align="center">1</div></td>
        <td width="50%">Jatuh tempo pembayaran Pajak 30 (tiga puluh) Hari </td>
         <td><div align="center"><strong>Kabupaten Malang</strong></div></td>
        </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>sejak tanggal  ditetapkan SKPD ini</td>
        <td><div align="center"></div></td>
        </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
      <tr>
        <td height="1"><div align="center">2</div></td>
        <td>Apabila melewati jatuh tempo pembayaran pajak</td>
        <!-- <td><div align="center"><strong>Kabupaten Malang</strong></div></td> -->
        </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>maka dikenakan sanksi Administrasi berupa bunga </td>
        <td rowspan="1"><div align="center"></div></td>
      </tr>
      <tr>
        <td height="1"></td>
        <td>2 % (dua persen) dari pokok pajak</td>
        </tr>
      <tr>
        <td height="1"<div align="center">3</div></td>
        <td>Surat Ketetapan Pajak Daerah (SKPD) ini</td>
        <td><div align="center"></div></td>
      </tr>
      <tr>
        <td height="1"></td>
        <td>bukan bukti pembayaran</td>
        <td><div align="center"><strong><u><?= $jabatan->NAMA?></u></strong></div></td>
      </tr>
      <tr>
        <td height="2"><div align="center">4</div></td>
        <td>Stiker wajib ditempelkan pada Obyek Reklame sebagai Bukti Lunas Pajak</td>
        <td><div align="center">Pembina</div></td>
      </tr>
      <tr>
        <td height="2"><div align="center"></div></td>
        <td></td>
        <td><div align="center">NIP.  <?= $jabatan->NIP?></div></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
  </div>
   
</body>
</html>
<style type="text/css">
  .footer {
  position: fixed;
  right: 0;
  bottom: 100;
  left: 0;
  padding: 1rem;
}
</style>
<?php 
  function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
      $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }
 
  function terbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim(penyebut($nilai));
    } else {
      $hasil = trim(penyebut($nilai));
    }         
    return $hasil;
  }
  
  ?>
