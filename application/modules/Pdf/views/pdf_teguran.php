
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 13px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
p{
  line-height: 0.2cm;
}
br{
   line-height:3px;
}
</style>
</style>
</head>
<?php 
$namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ;
?>
<body>
<table width="100%" height="772" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="21%" rowspan="2" align="center" valign="middle"><img width=93 height=100 src="assets/img/mlg.png" v:shapes="Picture_x0020_1"></td>
    <td width="62%" height="86" ><p align="center"><strong>PEMERINTAH KABUPATEN MALANG</strong></p>
    <p align="center"><strong>BADAN PENDAPATAN DAERAH </strong></p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" rowspan="2" align="center" valign="top"><p>&nbsp;</p></td>
        <td width="96%" align="center" valign="top">JL. Raden Panji No. 158, Telp. (0341) 3904898</td>
        <td width="2%" rowspan="2" align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td height="11" align="center" valign="top">Kepanjen - 65163</td>
      </tr>
    </table></td>
    <td width="17%" rowspan="2" align="center" valign="middle">NO URUT <br><?= sprintf('%06d', $rincian->ID_INC);?></td>
  </tr>
  <tr>
    <td height="47" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" rowspan="4" align="center" valign="top"><p>&nbsp;</p></td>
        <td colspan="2" align="center" valign="top">SURAT KETETAPAN PAJAK DAERAH KURANG BAYAR ( SKPDKB ) </td>
        <td width="1%" rowspan="4" align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" align="center" valign="top">PAJAK <?= strtoupper($nama->NAMA_PAJAK)?></td>
        </tr>
      <tr>
        <td width="9%" height="11" align="center" valign="top"><div align="left">MASA</div></td>
        <td width="88%" align="center" valign="top"><div align="left">: <?= $namabulan[$detail->MASA_PAJAK]?></div></td>
      </tr>
      <tr>
        <td height="15" align="center" valign="top"><div align="left">TAHUN</div></td>
        <td height="15" align="center" valign="top"><div align="left">: <?= $detail->TAHUN_PAJAK;?></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="17" colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td height="" colspan="3" valign="" align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="24%">NAMA USAHA </td>
    <td width="74%">: <?= $detail->OBJEK_PAJAK;?></td>
  </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>ALAMAT</td>
          <td>: <?= $detail->ALAMAT_OP;?></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>NPWPD</td>
          <td>: <?= $detail->NPWPD;?></td>
        </tr>
       <tr>
          <td height="15">&nbsp;</td>
          <td>NAMA PEMILIK/PENGELOLA </td>
          <td>: <?= $detail->NAMA_WP;?></td>
        </tr>
        <tr>
            <td height="15">&nbsp;</td>
            <td>TANGGAL JATUH TEMPO </td>
            <td>: <?= $detail->JATUH_TEMPO;?></td>
        </tr>
      </table><br>   
    
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="2%" height="15">&nbsp;</td>
          <td width="2%"><div align="center">I.</div></td>
          <td colspan="5">Berdasarkan Peraturan Daerah Kabupaten Malang Nomor 1 Tahun 2019 tentang Perubahan atas Peraturan Daerah Nomor 8 </td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td><div align="center"></div></td>
          <td colspan="5">Tahun 2010 tentang Pajak Daerah telah  dilakukan Pemeriksaan dan Keterangan Lain atas pelaksanaan kewajiban Wajib Pajak.</td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td><div align="center">II.</div></td>
          <td colspan="5">Dari Pemeriksaan atau Keterangan Lain tersebut diatas, Penghitungan jumlah yang masih harus dibayar adalah sebagai berikut : </td>
        </tr>
        <tr>
          <td height="15" rowspan="3">&nbsp;</td>
          <td rowspan="3"><div align="center"></div></td>
          <td width="2%"><div align="center">1.</div></td>
          <td width="46%">Dasar Pengenaan Pajak</td>
          <td width="1%">Rp </td>
          <td width="11%" align="right"><?= number_format($detail->DPP_TAGIHAN,'0','','.')?></td>
          <td width="36%"></td>
          
        </tr>
        <tr>
          <td><div align="center">2.</div></td>
          <td>Pajak terutang </td>
          <td>Rp</td>
          <td  align="right"><?= number_format($dua=$detail->TAGIHAN,'0','','.')?></td>
          <td></td>
        </tr>
        <tr>
          <td><div align="center">3.</div></td>
          <td>Kredit Pajak : </td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td></td>
        </tr>
      </table>      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>
          <td width="2%" height="15" rowspan="6">&nbsp;</td>
          <td width="2%" rowspan="6"><div align="center"></div></td>
          <td width="2%"><div align="center"></div></td>
          <td width="2%"><div align="center">a.</div></td>
          <td width="44%">Kompensasi kelebihan Pembayaran</td>
          <td width="1%">Rp</td>
          <td align="right"><?= number_format($a3=$rincian->KOMPENSASI,'0','','.')?></td>
          <td colspan="2" > </td>

        </tr>
        <tr>
          <td><div align="center"></div></td>
          <td><div align="center">b.</div></td>
          <td>Setoran yang terutang</td>
          <td>Rp</td>
          <td align="right"><?= number_format($b3=$rincian->SETORAN_TERUTANG,'0','','.')?></td>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div align="center">c.</div></td>
          <td>Lain - lain</td>
          <td>Rp</td>
          <td align="right"><?= number_format($c3=$rincian->LAIN_LAIN,'0','','.')?></td>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div align="center">d.</div></td>
          <td>Jumlah yang dapat dikreditkan ( a + b + c )</td>
          <td>Rp</td>
          <td align="right"><?= number_format($d3=$a3+$b3+$c3,'0','','.')?></td>
          <td colspan="2"></td>

        </tr>
        <tr>
          <td><div align="center">4.</div></td>
          <td colspan="2">Jumlah kekurangan pembayaran Pokok Pajak ( 2 - 3d ).</td>
          <td>&nbsp;</td>
          <td width="15%"><div align="right">Rp. </div></td>
          <td width="14%" align="right"><?= number_format($empat=$dua-$d3,'0','','.')?></td>
          <td width="16%"></td>
        </tr>
        <tr>
          <td><div align="center">5.</div></td>
          <td colspan="2"><div align="left">Sanksi Administrasi</div></td>
          <td colspan="3">&nbsp;</td>
          <td></td>
        </tr>
      </table>      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="2%" height="1">&nbsp;</td>
          <td width="2%"><div align="left"></div></td>
          <td width="2%"><div align="center"></div></td>
          <td width="2%"><div align="center">a.</div></td>
          <td width="44%">Bunga</td>
          <td width="1%">Rp</td>
          <td align="right"><?= number_format($a5=$rincian->BUNGA,'0','','.')?></td>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td width="2%" height="0">&nbsp;</td>
          <td width="2%">&nbsp;</td>
          <td><div align="center"></div></td>
          <td><div align="center">b.</div></td>
          <td>Kenaikan</td>
          <td>Rp</td>
          <td align="right"><?= number_format($b5=$rincian->KENAIKAN,'0','','.')?></td>
          <td colspan="2"></td>
        </tr>
        <tr>
          <td width="2%" height="1">&nbsp;</td>
          <td width="2%">&nbsp;</td>
          <td>&nbsp;</td>
          <td><div align="center">c.</div></td>
          <td>Jumlah sanksi administrasi ( a + b )</td>
          <td>&nbsp;</td>
          <td width="14%">Rp.</td>
          <td width="13%" align="right"><?= number_format($c5=$a5+$b5,'0','','.')?></td>
          <td width="20%"></td>
        </tr>

        <tr>
          <td width="2%" height="4">&nbsp;</td>
          <td width="2%">&nbsp;</td>
          <td><div align="center">6.</div></td>
          <td colspan="3">Jumlah yang masih harus dibayar ( 4 + 5c ).</td>
          <td>&nbsp;</td>
          <td><div align="right">Rp. </div></td>
          <td><?= number_format($enam=$empat+$c5,'0','','.')?></td>
          <td></td>
        </tr>
        <tr>
          <td width="2%" height="7">&nbsp;</td>
          <td width="2%">&nbsp;</td>
          <td colspan="7">&nbsp;</td>
        </tr>
      </table>      </td>
  </tr>
  <tr>
    <td height="17" colspan="3" valign="top" align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="9%" height="1"><div align="center"><strong>Terbilang</strong></div></td>
        <td>: <b><i><?php echo terbilang($enam);?> Rupiah</i></b></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="17" colspan="3" valign="top" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td height="40" colspan="3" valign="top" align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="1%" height="15">&nbsp;</td>
        <td colspan="2" style="font-size:11.5px"; >PERHATIAN<em> : </em></td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td>
        <td width="2%" style="font-size:11.5px"; ><div align="center">1.</div></td>
        <td style="font-size:11.5px"; >Pembayaran atas pajak terutang pada Bendahara Penerima Bada Pendapatan Daerah Kabupaten Malang dengan menggunakan</td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td>
        <td style="font-size:11.5px"; ><div align="center"></div></td>
        <td style="font-size:11.5px"; >Surat  Setoran Pajak Daerah ( SSPD ) yang diterbitkan oleh Bank Persepsi / Bank Jatim.</td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td>
        <td style="font-size:11.5px"; ><div align="center">2.</div></td>
        <td style="font-size:11.5px"; >Apabila   SKPDKB  ini tidak atau kurang  dibayar setelah  lewat waktu paling lama  30 ( tiga puluh ) hari sejak SKPDKB ini </td>
      </tr>
      <tr>
        <td height="15">&nbsp;</td>
        <td style="font-size:11.5px"; ><div align="center"></div></td>
        <td style="font-size:11.5px"; >diterbitkan dikenakan sanksi  administratif berupa denda 2% ( dua persen ) per bulan. </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="147" colspan="3" valign="top" align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td colspan="2" rowspan="10"></td>
          <td width="43%"><div align="center"><strong>Kepala Bidang Pajak Daerah dan Retribusi Daerah</strong></div></td>
        </tr>
        <tr>
          <td height="1"><div align="center"><strong>Kabupaten Malang</strong></div></td>
        </tr>
        <tr>
          <td height="1"><div align="center"></div></td>
        </tr>
        <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->

        <tr>
          <td height="2" rowspan="2"><div align="center">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
          </div></td>
        </tr>
        <tr>          </tr>
        <tr>
          <td height="1"><div align="center"></div></td>
        </tr>
        <tr>
          <td height="1"><div align="center"></div></td>
        </tr>
        <tr>
          <td height="1"><div align="center"><strong><u>
            <?= $jabatan->NAMA?>
          </u></strong></div></td>
        </tr>
        <tr>
          <td height="18"><div align="center">NIP.
            <?= $jabatan->NIP?>
          </div></td>
        </tr>
      </table>      
    <p></p></td>
  </tr>
</table>
</body>
</html>
 <?php 
  function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
      $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }
 
  function terbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim(penyebut($nilai));
    } else {
      $hasil = trim(penyebut($nilai));
    }         
    return $hasil;
  }
  
  ?>