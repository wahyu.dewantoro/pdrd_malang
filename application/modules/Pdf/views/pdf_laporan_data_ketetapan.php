


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 13px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
p{
  line-height: 0.2cm;
}
</style>
</style>
</head>

<body>
  <h3>Laporan Data Ketetapan</h3>
       <table width="100%" border="1" cellspacing="0" cellpadding="0">
          <thead>
            <tr>
              <th width="3%">No</th>
              <th>Kode Biling</th>
              <th>NPWPD</th>
              <th>Nama WP /Objek Pajak</th>
              <th>Alamat</th>
              <th>DPP</th>
              <th>Pajak Terutang</th>
              <th>Tgl Ditetapkan</th>
              <th>Jatuh Tempo</th> 
              <th>Status</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1; foreach ($data as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo $no?></td>
                <td><?= $rk->KODE_BILING?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->ALAMAT_WP?></td>
                <td><?= $rk->DPP?></td>
                <td align="right"><?=  number_format($rk->PAJAK_TERUTANG,'0','','.')?></td>
                <td><?= $rk->TGL_KETETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?= $rk->STATUS?></td>
              </tr>
              <?php $no++ ;} ?>
            </tbody>
          </table>
</body>
</html>