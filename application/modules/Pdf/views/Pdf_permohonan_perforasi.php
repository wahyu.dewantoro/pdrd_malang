<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Permohonan Perforasi</title>
<?php $cek=count($data); if ($cek<=10) {
  $margin='1';
} else {
  $margin='130';
}?>
<style type="text/css">
.a {
  font-size: 10px;
}
body{
  font-size: 12.5px; 
  /*font-family: "Calibri" ;*/
  font-family: Arial, Helvetica, sans-serif;
}
@page { margin-bottom: <?= $margin?>px; }
body { margin-bottom: <?= $margin?>px;
margin-top: 35px; }
</style>
</head>
<?php function tglIndonesia($str){
       $tr   = trim($str);
       $str    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);
       return $str;
       //call funtion
        tglIndonesia(date('D, d F, Y'));
   }?>
<body>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" height="8" rowspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="" align="center" valign="middle"><img width=66 height=79 src="assets/img/mlg.jpeg"></td>
        </tr>
      <tr>
        <td width="30%" height="1" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr>
        <td height="10" align="center" valign="top">PEMERINTAH KABUPATEN MALANG</td>
        </tr>
      <tr>
        <td height="10" align="center" valign="top"><strong>BADAN PENDAPATAN DAERAH</strong></td>
        </tr>
      <tr>
        <td height="10" align="center" valign="top">JL. Raden Panji No. 158</td>
        </tr>
      <tr>
        <td height="10" align="center" valign="top">Telp. (0341) 390683 , Kepanjen - 65163</td>
        </tr>
    </table></td>
    <td height="70" colspan="2" rowspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="9" align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td height="10" align="center" valign="top"><strong>SURAT PERMINTAAN</strong></td>
      </tr>
      <tr>
        <td height="0" align="center" valign="top"><strong>PERFORASI</strong></td>
      </tr>
    </table></td>
    <td height="10" colspan="2"><div align="center">Tanggal</div></td>
  </tr>
  <tr>
    <td height="10" colspan="2"><div align="center"><?= $detail->TGL?></div></td>
  </tr>
  <tr>
    <td height="10" colspan="2"><div align="center">Nomor</div></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%">Kepada Yth</td>
        <td width="70%">  Kepala Badan Pendapatan Kab. Malang</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>Upt Kepala Bidang Pajak Daerah </td>
        </tr>
      <tr>
        <td width="22%">&nbsp;</td>
        <td>dan Retribusi Daerah</td>
        </tr>
    </table></td>
    <td height="69" colspan="2"><div align="center"><?= $detail->NO_PERMOHONAN?></div></td>
  </tr>
  <tr>
    <td height="26" colspan="3">Mohon Agar dapat diperforasi sebagai berikut : <?= $detail->NAMA_USAHA?></td>
    <td width="10%" height="26">NPWPD</td>
    <td width="22%">&nbsp;<?= $detail->NPWP?></td>
  </tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
     <tr>
        <td rowspan="2"><div align="center">NO</div></td>
        <td width="43%" rowspan="2"><div align="center">Nama Benda Berharga</div></td>
        <td width="10%" rowspan="2"><div align="center">Kode Benda Berharga</div></td>
        <td width="13%" rowspan="2"><div align="center">Nominal Per Lembar</div></td>
        <td width="13%" rowspan="2"><div align="center">No Urut</div></td>
        <td colspan="4"><div align="center">Jumlah Yang Diterima /diserahkan</div></td>
  </tr>
      <tr>
        <td width="6%" ><div align="center">Blok</div></td>
        <td width="7%" ><div align="center">Isi Lembar</div></td>
        <td width="10%" ><div align="center">Jumlah Lembar</div></td>
        <td width="25%" ><div align="center">Jumlah Penerimaan</div></td>
      </tr>
      <?php $no=0; $td='0';$pnr='0';foreach ($data as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$no ?></td>
                <td><?php if ($rk->JENIS_KARCIS=='1') {$JNS='Karcis Masuk/';} else if($rk->JENIS_KARCIS=='2'){$JNS='Karcis Parkir/';}else{ } ;
                   echo  $JNS.$rk->CATATAN ?></td>
                <td align="center"><?= $rk->NOMOR_SERI?></td>
                <td align="right">Rp <?=  number_format($rk->NILAI_LEMBAR,'0','','.')?>&nbsp;</td>
                <td align="">&nbsp;<?= $rk->NO_URUT_KARCIS?></td>
                <td align="right"><?=  number_format($rk->JML_BLOK,'0','','.')?>&nbsp;</td>
                <td align="right"><?=  number_format($rk->ISI_LEMBAR,'0','','.')?>&nbsp;</td>
                <td align="right"><?=  number_format($qtt=$rk->JML_BLOK*$rk->ISI_LEMBAR,'0','','.')?>&nbsp;</td>
                <td align="right">Rp <?=  number_format($sum=$qtt*$rk->NILAI_LEMBAR,'0','','.')?>&nbsp;</td>
              </tr>
              <?php $td +=$qtt;$pnr +=$sum;} ?>
      <tr>
        <td>&nbsp;</td>
        <td colspan="6">JUMLAH YANG DIPERFORASI</td>
        <td align="right"><?php echo number_format($td,'0','','.')?>&nbsp;</td>
        <td align="right">Rp <?php echo number_format($pnr,'0','','.')?>&nbsp;</td>
      </tr>
</table>
<div class="footer">
<table width="100%" border="" cellspacing="0" cellpadding="0">
      <tr>
        <td height="140" colspan="2"><p align="center">Disetujui Oleh<br />
          Kepala Bidang Pajak Daerah dan Retribusi Daerah</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p align="center"><u><?= strtoupper($jabatan->NAMA)?></u><br />Pembina<br />NIP. <?= $jabatan->NIP?></p></td>
        <td width="34%" align="center"><p align="center">Diperiksa<br />
        Kepala Sub Bidang Pendaftarab dan Pencatatan</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p align="center"><u>TRIAS ARDHI YUDANA, S.AB</u><br />
          Penata <br />NIP. 19830427 2009031 003</p></td>
        <td width="33%" align="center"><p align="center">Pemohon<br />
        </p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p align="center"><u><?php echo $detail->NAMA;?></u> <br /> 
            <?php echo $detail->JABATAN;?></p></td>
      </tr>
</table>
</div>
</body>
</html>
<style type="text/css">
  .footer {
  position: ;
  right: 0;
  bottom: 100;
  left: 0;
  padding: 1rem;
}
</style>