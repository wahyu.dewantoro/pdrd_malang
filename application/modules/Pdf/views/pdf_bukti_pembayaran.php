
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 13px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
p{
  line-height: 0.2cm;
}
br{
   line-height:3px;
}
</style>
</style>
</head>

<body>
<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ?>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="23%" rowspan="2" align="center" valign="middle"><img width=93 height=100 src="assets/img/mlg.png" v:shapes="Picture_x0020_1"></td>
    <td width="55%" rowspan="2" ><p align="center"><strong>PEMERINTAH KABUPATEN MALANG</strong></p>
    <p align="center"><strong>BADAN PENDAPATAN DAERAH </strong></p>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" rowspan="3" align="center" valign="top"><p>&nbsp;</p></td>
        <td width="96%" align="center" valign="top">JL. Raden Panji No. 158, Telp. (0341) 3904898</td>
        <td width="2%" rowspan="3" align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td height="11" align="center" valign="top">Kepanjen - 65163</td>
      </tr>
      <tr>
        <td height="11" align="center" valign="top">Perda Kabupaten Malang Nomor 8 Tahun 2010</td>
      </tr>
    </table></td>
    <td width="22%" align="center" valign="middle"><b> KODE BILING</b><br><b><?php $chunks = str_split($wp->KODE_BILING, 3);echo implode(' ', $chunks)?></b></td>
  </tr>
  <tr>
    <td align="center" valign="middle"> <br><b> VIRTUAL ACCOUNT</b><br><b><?php $chunks = str_split($wp->VIRTUAL_ACCOUNT, 3);echo implode(' ', $chunks)?></b></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td height="144" colspan="3" valign="top" align="left"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <!-- <td width="0%" height="15">&nbsp;</td>
    <td width="0%"></td> -->
    <td colspan="3" width="1%" height="15" align="center"><font style='font-size: 20px;'><b>BUKTI PEMBAYARAN</b></font></td>
  </tr>
   <tr>
    <td width="2%" height="15">&nbsp;</td>
    <td width="19%">NPWPD</td>
    <td width="79%">: <?php echo $wp->NPWPD;?></td>
  </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Nama WP /Objek Pajak</td>
          <td>: <?php echo $wp->NAMA_WP.' / '.$wp->NAMA_USAHA;?></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Alamat</td>
          <td>: <?php echo $wp->ALAMAT_WP;?></td>
        </tr>
        <!-- <tr>
          <td height="15">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
        <!-- <tr>
          <td height="15">&nbsp;</td>
          <td>Nama Objek</td>
          <td>: <?php echo $wp->NAMA_USAHA;?></td>
        </tr> -->
        <?php if ($wp->JENIS_PAJAK=='02' and $wp->PROYEK!='') {?>
            <tr>
              <td height="15">&nbsp;</td>
              <td>Kegiatan</td>
              <td>: <?php echo $wp->PROYEK;?></td>
            </tr>
          <?php
        } else {
          # code...
        }?>
        <?php if ($wp->JENIS_PAJAK=='06') {
          $judul_alamat='Kegiatan/Proyek';
          $isi_alamat= $wp->PROYEK;
          $judul_kec_kel='Proyek';
        } else {
          $judul_alamat='Alamat Objek';
          $isi_alamat= $wp->ALAMAT_OP;
          $judul_kec_kel='Proyek';
        }?>
        <tr>
            <td height="15">&nbsp;</td>
            <td><?= $judul_alamat?></td>
            <td>: <?php echo $isi_alamat;?></td>
        </tr>
        <?php if ($wp->JENIS_PAJAK=='04') {
          # code...
        } else {?>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Kelurahan <?= $judul_kec_kel?></td>
          <td>: <?php echo $wp->NAMAKELURAHAN;?></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Kecamatan <?= $judul_kec_kel?></td>
          <td>: <?php echo $wp->NAMAKEC;?></td>
        </tr>
        <?php }?>        
        <tr>
          <td height="15">&nbsp;</td>
          <td>Jenis Pajak</td>
          <td>: <?php echo $wp->NAMA_PAJAK?></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td><?php if ($wp->JENIS_PAJAK=='04') {
            echo "Bulan / Tahun";
          } else {
            echo "Masa Pajak";
          }?></td>
          <td>: <?php echo $namabulan[$wp->MASA_PAJAK].' '.$wp->TAHUN_PAJAK ;?></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Jumlah Pajak</td>
          <td>: Rp. <?php echo number_format($wp->PAJAK_TERUTANG,'0',',','.');?>,-</td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Terbilang</td>
          <td>: <i><?php echo terbilang($wp->PAJAK_TERUTANG);?> Rupiah</i></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Tgl. Lunas</td>
          <td>: <?php echo $wp->JATUH_TEMPO;?></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Status</td>
          <td>: <i><?php if ($wp->STATUS==1) {
            echo "Terbayar";
          } else {
            echo "Belum Bayar";
          };?></i></td>
          <tr>
          <td height="15">&nbsp;</td>
          <td>NTPD</td>
          <td>: <?php echo $wp->PENGESAHAN;?></td>
        </tr>
        <tr>
          <td height="15">&nbsp;</td>
          <td>Metode Pembayaran</td>
          <td>: <?php if ($wp->METODE_PEMBAYARAN=='kb') {
            echo " Bank JATIM (KODE BILING)";
          } else {
             echo "Bank JATIM/Bank Lain (VIRTUAL ACCOUNT)";
          }?></td>
        </tr>
      </table>      
    
    </td>
  </tr>
</table>
<img src="assets/images/ntpd/<?= $wp->KODE_BILING?>.png" width="83" height="83" />
<br>
</body>
</html>

<?php 
  function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
      $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }
 
  function terbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim(penyebut($nilai));
    } else {
      $hasil = trim(penyebut($nilai));
    }         
    return $hasil;
  }
  
  ?>

