
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 11px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
p{
  line-height: 0.2cm;
}
br{
  line-height: 100px;
}
.style2 {
  font-size: 16px;
  font-weight: bold;
}
.style3 {
  font-size: 22px;
  font-weight: bold;
}
.style4 {
  font-size: 21px;
  font-weight: bold;
}
.style5 {font-size: 13px}
.style6 {
  font-size: 15px;
  font-weight: bold;
}
.style7 {font-size: 15px}
</style>
</style>
</head>

<body>
<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ;
      function tglIndonesia($str){
       $tr   = trim($str);
       $str    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);
       return $str;
       //call funtion
        tglIndonesia(date('D, d F, Y'));}?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
            <td width="25%" rowspan="4" align="center" valign="top"><p><img src="assets/img/mlg.png" width="80" height="0" /></p></td>
            <td height="1%" align="center" valign=""><span class="style2">PEMERINTAH KABUPATEN MALANG</span></td>
            <td width="21%" rowspan="4" align="center" valign="top">&nbsp;</td>
  </tr>
          <tr>
            <td height="1%" align="center" valign=""> <span class="style3">BADAN PENDAPATAN DAERAH</span></td>
          </tr>
          <tr>
            <td height="1%" align="center" valign="">Jalan Raden Panji No. 158 Kepanjen - Telp (0341) 3904898</td>
          </tr>
          <tr>
            <td width="54%" height="1%" align="center" valign="top"><b>  KEPANJEN - 65163</b></td>
          </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" height="20" align="center" valign="middle">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20%" height="1"></td>
        <td width="14%"><span class="style7"></span></td>
        <td width="66%"><span class="style6">Kepada</span></td>
      </tr>
      <tr>
        <td height="2%">&nbsp;</td>
        <td><span class="style7">Yth. Sdr. </span></td>
        <td><span class="style7">:
            <?= $wp->NAMA_WP;?>
        </span></td>
      </tr>
      <tr>
        <td height="2%%">&nbsp;</td>
        <td><span class="style7">NPWPD</span></td>
        <td><span class="style7">:
            <?= $wp->NPWPD?>
        </span></td>
      </tr>
      <!-- <tr>
          <td height="2%">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
      <tr>
        <td height="2%">&nbsp;</td>
        <td><span class="style7">Alamat</span></td>
        <td><span class="style7">:
            
            <?= $wp->ALAMAT_WP?>
        </span></td>
      </tr>
      <tr>
        <td height="2%">&nbsp;</td>
        <td><span class="style7">Nama Usaha </span></td>
        <td><span class="style7">:
            <?= $wp->OBJEK_PAJAK?>
        </span></td>
      </tr>
      <tr>
        <td height="2%">&nbsp;</td>
        <td><span class="style7">Alamat Usaha </span></td>
        <td><span class="style7">:
            <?= $wp->ALAMAT_OP?>
        </span></td>
      </tr>
    </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1"><div align="center"><span class="style4">SURAT TEGURAN </span></div></td>
        </tr>
        <tr>
          <td height="1"><div align="center" class="style5">Nomor/<?= sprintf("%04d", $wp->NOMOR_BERKAS)?>/35.07205/2019 </div></td>
        </tr>

        <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20%" height="22"></td>
          <td width="5%"><span class="style7"></span></td>
          <td width="75%" valign=""><span class="style7">Menurut tata usaha kami hingga saat ini Saudara belum melaporkan Surat</span></td>
        </tr>
        <tr>
          <td height="23"></td>
          <td colspan="2"><span class="style7"> Pemberitahuan Pajak Daerah (SPTPD) untuk masa pajak bulan <u><?= $namabulan[$wp->MASA_PAJAK]?></u> Tahun <?= $wp->TAHUN_PAJAK?></span></td>
        </tr>
        <tr>
          <td height="22"></td>
          <td colspan="2"><span class="style7"> sebagaimana kewajiban sesuai perundang-undangan pajak.</span></td>
        </tr>
        <tr>
          <td height="22"></td>
          <td><span class="style7"></span></td>
          <td><span class="style7"> Surat Pemberitahuan harus disampaikan selambat-lambatnya dalam waktu </span></td>
        </tr>
        <tr>
          <td height="23"></td>
          <td colspan="2"><span class="style7">7 (Tujuh) hari sejak diterimanya Surat Teguran ini.</span></td>
        </tr>
        <tr>
          <td height="24"></td>
          <td><span class="style7"></span></td>
          <td><span class="style7">Untuk menghindarkan pengenaan sanksi administrasi berupa denda 2% per bulan</span></td>
        </tr>
        <tr>
          <td height="26"></td>
          <td colspan="2"><span class="style7"> yang akan memberatkan saudara, maka diminta saudara menyampaikan SPTPD dalam </span></td>
        </tr>
        <tr>
          <td height="1"></td>
          <td colspan="2"><span class="style7">jangka waktu dimaksud.</span></td>
        </tr>
        <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
      </table>
      </td>
  </tr>
  <tr>
    <td height="2"></td>
  </tr>
  
  <tr>
    <td height="71"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td colspan="2" rowspan="11"></td>
        <td><div align="center" class="style7">Kepanjen,
          <?= tglIndonesia(date('d-F-Y'))?>
        </div></td>
      </tr>
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td width="43%"><div align="center" class="style7"><strong>KEPALA BADAN PENDAPATAN DAERAH</strong></div></td>
      </tr>
      <tr>
        <td width="2%" height="1">&nbsp;</td>
        <td width="33%">&nbsp;</td>
        <td><div align="center" class="style7"><strong>KABUPATEN MALANG</strong></div></td>
        </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center"><span class="style5"><span class="style7"><span class="style7"></span></span></span></div></td>
        </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <!-- <td><div align="center"><strong>Kabupaten Malang</strong></div></td> -->
        </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td rowspan="2"><div align="center"><span class="style5"><span class="style7"><span class="style7"></span></span></span></div></td>
      </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center"><span class="style5"><span class="style7"><span class="style7"></span></span></span></div></td>
      </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center" class="style7"><strong><u>
          <?= $jabatan->NAMA?>
        </u></strong></div></td>
      </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center" class="style7">Pembina Utama Muda</div></td>
      </tr>
      <tr>
        <td height="18">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center" class="style7">NIP.  <?= $jabatan->NIP?></div></td>
      </tr>
    </table></td>
  </tr>
</table>

</body>
</html>
<?php  function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
      $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }
 
  function terbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim(penyebut($nilai));
    } else {
      $hasil = trim(penyebut($nilai));
    }         
    return $hasil;
  }