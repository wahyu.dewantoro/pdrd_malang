<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BAP Perforasi</title>
<?php $cek=count($data); if ($cek<=9) {
  $margin='1';
} else {
  $margin='155';
}?>
<style type="text/css">
.a {
  font-size: 10px;
}
body{
  font-size: 12.5px; 
  /*font-family: "Calibri" ;*/
  font-family: Arial, Helvetica, sans-serif;
}
@page { margin-bottom: <?= $margin?>px; }
body { margin-bottom: <?= $margin?>px;
margin-top: 35px; }
</style>
</head>
<?php function tglIndonesia($str){
       $tr   = trim($str);
       $str    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);
       return $str;
       //call funtion
        tglIndonesia(date('D, d F, Y'));
   }?>
<body>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33%" height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="3" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr>
        <td width="23%" align="center" valign="middle"><img width=66 height=79 src="assets/img/mlg.jpeg"></td>
        </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top">PEMERINTAH KABUPATEN MALANG</td>
        </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top"><strong>BADAN PENDAPATAN DAERAH</strong></td>
        </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top">JL. Raden Panji No. 158</td>
        </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top"> Telp. (0341) 390683 Kepanjen - 65163</td>
        </tr>
    </table></td>
    <td width="37%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="10" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr>
        <td height="10" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top"><strong>BERITA ACARA </strong></td>
        </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top"><strong>PENERIMAAN / PENYERAHAN </strong></td>
        </tr>
      <tr>
        <td height="10" class="kepala"align="center" valign="top"><strong>BENDA BERHARGA</strong></td>
        </tr>
      <tr>
        <td height="26" align="center" valign="top">&nbsp;</td>
        </tr>
    </table></td>
    <td width="30%" height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" height="20" align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top">NOMOR</td>
      </tr>
      <tr>
        <td height="10" class="kepala" align="center" valign="top"><strong>B A P</strong></td>
      </tr>
      <tr>
        <td height="19" class="kepala" align="center" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td height="19" align="center" valign="top"><?= $detail->NO_PERMOHONAN?></td>
      </tr>
      <tr>
        <td height="26" align="center" valign="top">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12%">Pada Hari ini</td>
        <td width="17%">: <?php tglIndonesia(date('D'));?></td>
        <td width="6%">Tanggal</td>
        <td width="15%">: <?php tglIndonesia(date('d'));?></td>
        <td width="5%">Bulan</td>
        <td width="12%">: <?php tglIndonesia(date('F'));?></td>
        <td width="6%">Tahun</td>
        <td width="30%">: <?php tglIndonesia(date('Y'));?></td>
      </tr>
    </table>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">       
        <tr>
          <td colspan="3"><div align="left">Kami yang bertanda tangan dibawah ini,</div></td>
        </tr>
        <tr>
          <td width="3%"><div align="center">1</div></td>
          <td width="23%">Nama </td>
          <td>: <strong>TRIAS ARDHI YUDANA, S.AB</strong></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>NIP</td>
          <td>: 19830427 2009031 003</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Jabatan</td>
          <td>: Kepala Sub Bidang Pendaftaran dan Pendataan</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Alamat</td>
          <td>: jl. Raden panji No. 158 Kepanjen</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="2">Disebut dengan PIHAK KESATU</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td></td>
        </tr>
        <tr>
          <td><div align="center">2</div></td>
          <td width="23%">Nama </td>
          <td>: <?php echo $detail->NAMA;?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>NIP/NPWPD</td>
          <td>: <?php echo $detail->NPWP;?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Jabatan</td>
          <td>: <?php echo $detail->JABATAN;?></td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Alamat</td>
          <td>: <?php echo $detail->ALAMAT;?></td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="2">Disebut dengan PIHAK KEDUA</td>
        </tr>
        <tr>
          <td colspan="3">PIHAK KESATU telah Menyerahkan benda berharga berdasarkan bukti Surat Permintaan Perforasi</td>
        </tr>
        <tr>
          <td colspan="3"> &nbsp;&nbsp; No <b><?= $detail->NO_PERMOHONAN?></b>  ,tanggal <b><?= $detail->TGL?></b> kepada pihak KEDUA</td>
        </tr>
        <tr>
          <td colspan="3">Adapun benda berharga yang diterima dan diperiksa sebagai berikut :</td>
        </tr>
    </table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td rowspan="2"><div align="center">NO</div></td>
        <td width="41%" rowspan="2"><div align="center">Nama Benda Berharga</div></td>
        <td width="14%" rowspan="2"><div align="center">Kode Benda Berharga</div></td>
        <td width="16%" rowspan="2"><div align="center">Nominal Per Lembar</div></td>
         <td width="15%" rowspan="2"><div align="center">No Urut</div></td>
        <td colspan="3"><div align="center">Jumlah Yang Diterima /diserahkan</div></td>
      </tr>
      <tr>
        <td><div align="center">Blok</div></td>
        <td><div align="center">Isi Lembar</div></td>
        <td><div align="center">Jumlah Lembar</div></td>
        </tr>
      <tr>
        <td width="3%"><div align="center">1</div></td>
        <td><div align="center">2</div></td>
        <td><div align="center">3</div></td>
        <td><div align="center">4</div></td>
        <td width="13%"><div align="center">5</div></td>
        <td width="11%"><div align="center">6</div></td>
        <td width="12%"><div align="center">7</div></td>
        <td width="16%"><div align="center">8</div></td>
      </tr>
      <?php $no=0; $td=0;foreach ($data as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$no ?></td>
                 <td><?php if ($rk->JENIS_KARCIS=='1') {$JNS='Karcis Masuk/';} else if($rk->JENIS_KARCIS=='2'){$JNS='Karcis Parkir/';}else{ } ;
                   echo  $JNS.$rk->CATATAN ?></td>
                <td align="center"><?= $rk->NOMOR_SERI?></td>
                <td align="right">Rp <?=  number_format($rk->NILAI_LEMBAR,'0','','.')?>&nbsp;</td>
                <td align="">&nbsp;<?= $rk->NO_URUT_KARCIS?></td>
                <td align="right"><?=  number_format($rk->JML_BLOK,'0','','.')?>&nbsp;</td>
                <td align="right"><?=  number_format($rk->ISI_LEMBAR,'0','','.')?>&nbsp;</td>
                <td align="right"><?=  number_format($qtt=$rk->JML_BLOK*$rk->ISI_LEMBAR,'0','','.')?>&nbsp;</td>
               <!--  <td align="right"><?=  number_format($qtt*$rk->NILAI_LEMBAR,'0','','.')?></td> -->
              </tr>
              <?php  $td +=$qtt;} ?>
      <tr>
                <td>&nbsp;</td>
                <td colspan="6">JUMLAH YANG DIPERFORASI</td>
                <td align="right"><?php echo number_format($td,'0','','.')?></td>
  </tr>
      <tr>
        <td colspan="8">Demikian Berita Acara Penerimaan / Penyerahan Benda Berharga ini dibuat sesuai keadaan sebenarnya untuk digunakan sebagai mana mestinya.</td>
      </tr>
    </table>
<div class="footer">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="91" colspan="2"><p align="center">Yang Menerima<br />
          PIHAK KEDUA</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p align="center"><u><?php echo $detail->NAMA;?></u> <br /> 
            <?php echo $detail->JABATAN;?>
</p></td>
        <td width="34%" align="center"><p align="center">Yang Menyerahkan<br />
          PIHAK KESATU</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p align="center"><u>TRIAS ARDHI YUDANA, S.AB</u><br />
          Penata <br />NIP. 19830427 2009031 003</p></td>
        <td width="33%" align="center"><p align="center">Petugas Perforasi<br />
        </p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p align="center"><u><?= $detail->PENGATUR_MUDA;?></u><br />
          <?= $detail->JABATAN_PEGAWAI;?> <br />NIP. <?= $detail->NIP;?></p></td>
      </tr>
    </table>
</div>
</body>
</html>
<style type="text/css">
  .footer {
  position: ;
  right: 0;
  bottom: 90;
  left: 0;
  padding: 1rem;
}
</style>
