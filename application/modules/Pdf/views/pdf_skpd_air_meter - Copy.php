
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
@page teacher {
  size: A4 portrait;
  margin: 2cm;
}

.teacherPage {
   page: teacher;
  /* page-break-after: always;*/
}

body {
  font-size: 11px;
}


.border_bottom{
  border-bottom:solid;
  border-color:  #c1c1c1;
  /*border-style: dotted;*/
}
p{
  line-height: 0.2cm;
}
br{
  line-height: 100px;
}
</style>
</style>
</head>

<body>
<?php $namabulan=array(
      '1'=>'Januari',
      '2'=>'Februari',
      '3'=>'Maret',
      '4'=>'April',
      '5'=>'Mei',
      '6'=>'Juni',
      '7'=>'Juli',
      '8'=>'Agustus',
      '9'=>'September',
      '10'=>'Oktober',
      '11'=>'November',
      '12'=>'Desember'
      ) ;
      function tglIndonesia($str){
       $tr   = trim($str);
       $str    = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);
       return $str;
       //call funtion
        tglIndonesia(date('D, d F, Y'));}?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
            <td width="23%" rowspan="7" align="center" valign="top"><p><img src="assets/img/mlg.png" width="80" height="90" /></p></td>
            <td align="center" valign="top"><strong>PEMERINTAH KABUPATEN MALANG</strong></td>
            <td width="21%" rowspan="7" align="center" valign="top"><img src="assets/images/<?= $detail->KODE_BILING?>.png" width="83" height="83" /></td>
  </tr>
          <tr>
            <td align="center" valign="top"> <strong>BADAN PENDAPATAN DAERAH KAB. MALANG</strong></td>
          </tr>
          <tr>
            <td align="center" valign="top">Jalan Raden Panji No. 158 Kepanjen - Telp (0341) 3904898</td>
          </tr>
          <tr>
            <td width="56%" align="center" valign="top"><b>  KEPANJEN -</b> 65163</td>
          </tr>
          <tr>
            <td height="11" align="center" valign="top"><strong>SURAT KETETAPAN PAJAK DAERAH (SKPD)</strong></td>
          </tr>
          <tr>
            <td height="11" align="center" valign="top">PAJAK AIR TANAH (<?= $detail->KODE_BILING?>)</td>
          </tr>
  <tr>
            <td height="11" align="center" valign="top">(Peraturan Daerah Kabupaten Malang No. 8 Tahun 2010) </td>
  </tr>
        </table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" colspan="7" align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="0%" height="1"></td>
          <td width="15%">Nama Subyek</td>
          <td width="40%">: <?= $detail->OBJEK_PAJAK?></td>
          <td width="16%">No. Rekening</td>
          <td width="29%">: <?= $detail->KODE_REK?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Nama WP</td>
          <td>: <?= $detail->NAMA_WP?></td>
          <td>No. Berkas</td>
          <td>: <?= sprintf("%04d", $detail->NOMOR_BERKAS);?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Alamat</td>
          <td>: <?= $detail->ALAMAT_WP?></td>
          <td>Bulan/Tahun</td>
          <td>: <?= $namabulan[$detail->MASA_PAJAK].' / '.$detail->TAHUN_PAJAK?></td>
        </tr>
        <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
        <tr>
          <td height="1">&nbsp;</td>
          <td>Kelurahan</td>
          <td>: <?= $detail->NAMAKELURAHAN?></td>
          <td>Peruntukan</td>
          <td>: <?= $detail->JENIS?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Kecamatan</td>
          <td>: <?= $detail->NAMAKEC?></td>
          <td>Tarif Pajak</td>
          <td>: <?= $detail->PAJAK?> %</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>NPWD</td>
          <td>: <?= $detail->NPWPD?></td>
          <td>Jenis Pengambilan Air</td>
          <td>: <?= $detail->JENISPENGAMBILAN?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Jumlah Pajak</td>
          <td>: <?=  number_format($detail->PAJAK_TERUTANG,'0','','.')?></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td width="12%"><div align="center"><strong>Meter Bln Lalu</strong></div></td>
    <td width="12%"><div align="center"><strong>Meter Bln ini</strong></div></td>
    <td width="15%"><div align="center"><strong>Pemakaian(m3)</strong></div></td>
    <td width="15%"><div align="center"><strong>Jenis Pengambilan</strong></div></td>
    <td width="15%"><div align="center"><strong>HDA(Rp)</strong></div></td>
    <td width="15%"><div align="center"><strong>NPA (Rp)</strong></div></td>
    <td width="16%"><div align="center"><strong>Pajak Terutang</strong></div></td>
  </tr>
  <tr>
    <td height="31"><div align="center"><?= number_format($detail->PENGGUNAAN_BULAN_LALU_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->PENGGUNAAN_HARI_INI_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->VOLUME_AIR_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= $detail->JENISPENGAMBILAN?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->HARGA,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->HARGA*$detail->VOLUME_AIR_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->PAJAK_TERUTANG,'0','','.')?></div></div></td>
  </tr>
  <tr>
    <td colspan="7" height="5"></td>
  </tr>
  <tr>
    <td colspan="7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" height="13">&nbsp;</td>
        <td width="8%">Terbilang</td>
        <td width="88%">: <strong><?= terbilang($detail->PAJAK_TERUTANG);?> Rupiah</strong></td></td>
        <td width="2%"></td>
      </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
    </table></td>
  </tr>
  <tr>
    <td height="71" colspan="7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td colspan="2" rowspan="11"></td>
        <td><div align="center">Malang,
          <?= tglIndonesia(date('d-F-Y'))?>
        </div></td>
      </tr>
      <tr>
        <td height="1" colspan="2">Catatan</td>
        <td width="43%"><div align="center"><strong>Kepala Bidang Pajak Daerah dan Retribusi Daerah</strong></div></td>
      </tr>
      <tr>
        <td width="2%" height="1"><div align="center">1</div></td>
        <td width="33%">Jatuh tempo pembayaran Pajak 30 (tiga puuh) Hari </td>
        <td><div align="center"><strong>Kabupaten Malang</strong></div></td>
        </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>sejak tanggal  ditetapkan SKPD ini</td>
        <td><div align="center"></div></td>
        </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
      <tr>
        <td height="1"><div align="center">2</div></td>
        <td>Apabila melewati jatuh tempo pembayaran pajak</td>
        <!-- <td><div align="center"><strong>Kabupaten Malang</strong></div></td> -->
        </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>maka dikenakan sanksi Administrasi berupa bunga </td>
        <td rowspan="2"><div align="center"></div></td>
      </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>2 % (dua persen) dari pokok pajak</td>
        </tr>
      <tr>
        <td height="1"><div align="center">3</div></td>
        <td>Surat Ketetapan Pajak Daerah (SKPD) ini </td>
        <td><div align="center"></div></td>
      </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>bukan bukti pembayaran</td>
        <td><div align="center"><strong><u>
          <?= $jabatan->NAMA?>
        </u></strong></div></td>
      </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center">Pembina</div></td>
      </tr>
      <tr>
        <td height="18">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center">NIP.  <?= $jabatan->NIP?></div></td>
      </tr>
    </table></td>
  </tr>
</table>

<br />


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
            <td width="23%" rowspan="7" align="center" valign="top"><p><img src="assets/img/mlg.png" width="80" height="90" /></p></td>
            <td align="center" valign="top"><strong>PEMERINTAH KABUPATEN MALANG</strong></td>
            <td width="21%" rowspan="7" align="center" valign="top"><img src="assets/images/<?= $detail->KODE_BILING?>.png" width="83" height="83" /></td>
  </tr>
          <tr>
            <td align="center" valign="top"> <strong>BADAN PENDAPATAN DAERAH KAB. MALANG</strong></td>
          </tr>
          <tr>
            <td align="center" valign="top">Jalan Raden Panji No. 158 Kepanjen - Telp (0341) 3904898</td>
          </tr>
          <tr>
            <td width="56%" align="center" valign="top"><b>  KEPANJEN -</b> 65163</td>
          </tr>
          <tr>
            <td height="11" align="center" valign="top"><strong>SURAT KETETAPAN PAJAK DAERAH (SKPD)</strong></td>
          </tr>
          <tr>
            <td height="11" align="center" valign="top">PAJAK AIR TANAH (<?= $detail->KODE_BILING?>)</td>
          </tr>
  <tr>
            <td height="11" align="center" valign="top">(Peraturan Daerah Kabupaten Malang No. 8 Tahun 2010) </td>
  </tr>
        </table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" colspan="7" align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="0%" height="1"></td>
          <td width="15%">Nama Subyek</td>
          <td width="40%">: <?= $detail->OBJEK_PAJAK?></td>
          <td width="16%">No. Rekening</td>
          <td width="29%">: <?= $detail->KODE_REK?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Nama WP</td>
          <td>: <?= $detail->NAMA_WP?></td>
          <td>No. Berkas</td>
          <td>: <?= sprintf("%04d", $detail->NOMOR_BERKAS);?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Alamat</td>
          <td>: <?= $detail->ALAMAT_WP?></td>
          <td>Bulan/Tahun</td>
          <td>: <?= $namabulan[$detail->MASA_PAJAK].' / '.$detail->TAHUN_PAJAK?></td>
        </tr>
        <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
        <tr>
          <td height="1">&nbsp;</td>
          <td>Kelurahan</td>
          <td>: <?= $detail->NAMAKELURAHAN?></td>
          <td>Peruntukan</td>
          <td>: <?= $detail->JENIS?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Kecamatan</td>
          <td>: <?= $detail->NAMAKEC?></td>
          <td>Tarif Pajak</td>
          <td>: <?= $detail->PAJAK?> %</td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>NPWD</td>
          <td>: <?= $detail->NPWPD?></td>
          <td>Jenis Pengambilan Air</td>
          <td>: <?= $detail->JENISPENGAMBILAN?></td>
        </tr>
        <tr>
          <td height="1">&nbsp;</td>
          <td>Jumlah Pajak</td>
          <td>: <?=  number_format($detail->PAJAK_TERUTANG,'0','','.')?></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td width="12%"><div align="center"><strong>Meter Bln Lalu</strong></div></td>
    <td width="12%"><div align="center"><strong>Meter Bln ini</strong></div></td>
    <td width="15%"><div align="center"><strong>Pemakaian(m3)</strong></div></td>
    <td width="15%"><div align="center"><strong>Jenis Pengambilan</strong></div></td>
    <td width="15%"><div align="center"><strong>HDA(Rp)</strong></div></td>
    <td width="15%"><div align="center"><strong>NPA (Rp)</strong></div></td>
    <td width="16%"><div align="center"><strong>Pajak Terutang</strong></div></td>
  </tr>
  <tr>
    <td height="31"><div align="center"><?= number_format($detail->PENGGUNAAN_BULAN_LALU_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->PENGGUNAAN_HARI_INI_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->VOLUME_AIR_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= $detail->JENISPENGAMBILAN?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->HARGA,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->HARGA*$detail->VOLUME_AIR_METER,'0','','.')?></div></td>
    <td height="31"><div align="center"><?= number_format($detail->PAJAK_TERUTANG,'0','','.')?></div></div></td>
  </tr>
  <tr>
    <td colspan="7" height="5"></td>
  </tr>
  <tr>
    <td colspan="7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="2%" height="13">&nbsp;</td>
        <td width="8%">Terbilang</td>
        <td width="88%">: <strong><?= terbilang($detail->PAJAK_TERUTANG);?> Rupiah</strong></td></td>
        <td width="2%"></td>
      </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
    </table></td>
  </tr>
  <tr>
    <td height="71" colspan="7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="1" colspan="2">&nbsp;</td>
        <td colspan="2" rowspan="11"></td>
        <td><div align="center">Malang,
          <?= tglIndonesia(date('d-F-Y'))?>
        </div></td>
      </tr>
      <tr>
        <td height="1" colspan="2">Catatan</td>
        <td width="43%"><div align="center"><strong>Kepala Bidang Pajak Daerah dan Retribusi Daerah</strong></div></td>
      </tr>
      <tr>
        <td width="2%" height="1"><div align="center">1</div></td>
        <td width="33%">Jatuh tempo pembayaran Pajak 30 (tiga puuh) Hari </td>
        <td><div align="center"><strong>Kabupaten Malang</strong></div></td>
        </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>sejak tanggal  ditetapkan SKPD ini</td>
        <td><div align="center"></div></td>
        </tr>
      <!-- <tr>
          <td height="1">&nbsp;</td>
          <td>NOP</td>
          <td>:</td>
        </tr> -->
      <tr>
        <td height="1"><div align="center">2</div></td>
        <td>Apabila melewati jatuh tempo pembayaran pajak</td>
        <!-- <td><div align="center"><strong>Kabupaten Malang</strong></div></td> -->
        </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>maka dikenakan sanksi Administrasi berupa bunga </td>
        <td rowspan="2"><div align="center"></div></td>
      </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>2 % (dua persen) dari pokok pajak</td>
        </tr>
      <tr>
        <td height="1"><div align="center">3</div></td>
        <td>Surat Ketetapan Pajak Daerah (SKPD) ini </td>
        <td><div align="center"></div></td>
      </tr>
      <tr>
        <td height="1"><div align="center"></div></td>
        <td>bukan bukti pembayaran</td>
        <td><div align="center"><strong><u>
          <?= $jabatan->NAMA?>
        </u></strong></div></td>
      </tr>
      <tr>
        <td height="1">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center">Pembina</div></td>
      </tr>
      <tr>
        <td height="18">&nbsp;</td>
        <td>&nbsp;</td>
        <td><div align="center">NIP.  <?= $jabatan->NIP?></div></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
<?php  function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
      $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }
 
  function terbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim(penyebut($nilai));
    } else {
      $hasil = trim(penyebut($nilai));
    }         
    return $hasil;
  }