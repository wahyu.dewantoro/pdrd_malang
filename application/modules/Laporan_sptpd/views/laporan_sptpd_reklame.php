   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan_sptpd/laporan_reklame'?>">
                <input type='hidden' value='1' name='cek'>
                <div class="form-group">
                  <select  name="tahun" required="required" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('h_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select  name="bulan" class="form-control select2 col-md-7 col-xs-12">
                            <option value="" >Semua Bulan Lapor</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php if ($this->session->userdata('h_bulan')==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <!-- <div class="form-group">
                      <select onchange="getkel(this);" name="kecamatan"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('h_kecamatan')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kelurahan"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('h_kelurahan')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div> -->
                <div class="form-group"><input type="text" name="npwpd"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('h_npwpd'); ?>" placeholder="nama wp,nama usaha,npwpd"></div>
                
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($cek <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan_sptpd/laporan_reklame'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <!-- <a href="<?php echo site_url('Excel/Excel/Excel_laporan_reklame'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a> -->
                              <?php }   ?>  
                              <a href="<?php echo $cetak; ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
        </form>
        <div class="table-responsive">
        <table style="width: 100%;"class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Masa</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Tagihan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">No Perizinan</th>
              <th class="text-center">Status</th>
              <th class="text-center">Detail</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($reklame as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $namabulan[$rk->MASA_PAJAK]?></td>
                <td><?php if ($rk->KODE_BILING=='') {echo "Belum Ditetapkan";} else { echo $rk->KODE_BILING;}?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->NAMA_USAHA?></td>
                <td><?= $rk->ALAMAT_USAHA?></td>
                <td><?= $rk->NAMAKEC?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->PAJAK_TERUTANG),'0','','.')?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->DENDA),'0','','.')?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->POTONGAN),'0','','.')?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk->JUMLAH_KETETAPAN),'0','','.')?></td>
                <td align="center"><?php if ($rk->KODE_BILING=='') {echo "Belum Ditetapkan";} else { echo $rk->TGL_PENETAPAN ;}?></td>
                <td><?= $rk->NO_IZIN?></td>
                <td align="center"><?php if ($rk->STATUS=='2') {echo "Belum Ditetapkan";} else if($rk->STATUS=='1'){ echo "Terbayar";}else{echo "Belum Bayar";}?></td>
                <td align="center"><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk->ID_INC?>">Detail</a></td>
                <td align="center">
                <?php if ($session_value=='121') {
                        if ($rk->KODE_BILING !='') {?>
                             <a href="<?php echo base_url('Pdf/Pdf/pdf_kode_biling/').acak($rk->KODE_BILING);?>"><i class='fa fa-print'></i></a> || <a href="#"  data-toggle="modal" data-target="#myModal1<?= $rk->ID_INC?>"><i class='fa fa-edit'></i> No izin</a></td>
                        <?php }
                } else {
                        if ($rk->KODE_BILING=='') {?>
                          <a href="<?php echo base_url('Laporan_sptpd/Laporan_sptpd/hapus_reklame/'.$rk->ID_INC);?>" onclick="return confirm('Apakah anda yakin Data Akan Dihapus Permanen ?');"><i class="fa fa-trash"></i>
                        <?php } else {?>
                          <a href="<?php echo base_url('Pdf/Pdf/pdf_kode_biling/').acak($rk->KODE_BILING);?>"><i class='fa fa-print'></i></a> 
                          <a href="<?php echo base_url('Pdf/Pdf/pdf_stiker_reklame/').$rk->KODE_BILING;?>">Stiker</a>
                        </td>
                        <?php }
                }?>
                                 
              </tr>
                <div class="modal fade" id="myModal1<?= $rk->ID_INC?>" role="dialog">
    <div class="modal-dialog"> 
       
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Entry Nomor  Perizinan</h4>
        </div>

           <form class="form-inline"  method="post" action="<?php echo base_url('Laporan_sptpd/Laporan_sptpd/simpan_no_izin');?>" enctype="multipart/form-data" >
                                            <input  type="hidden" name="kode_biling" required value="<?= $rk->KODE_BILING?>" >
                                              <div class="modal-body">
                                                <div class="col-md-6">
                                                  <ul class="to_do">
                                                    <li><?= $rk->KODE_BILING?></li>
                                                    <li><?= $rk->NAMA_WP?></li>
                                                    <li><?= $rk->NAMA_USAHA?></li>
                                                    <li><?= $rk->ALAMAT_USAHA?></li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-6">
                                                  <ul class="to_do">
                                                    <li><?=  number_format(str_replace(",",".",$rk->JUMLAH_KETETAPAN),'0','','.')?></li>
                                                    <li><?php if ($rk->STATUS=='2') {echo "Belum Ditetapkan";} else if($rk->STATUS=='1'){ echo "Terbayar";}else{echo "Belum Bayar";}?></li>
                                                  </ul>
                                                </div>
                                              <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >No Perizinan <sup>*</sup>
                                                  </label>
                                                  <div class="col-md-9 col-sm-7">
                                                    <input type="text" name="nomor_izin" required="required" class="form-control col-md-7 col-xs-12" value="<?= $rk->NO_IZIN?>" >                
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <?php if ($rk->NO_IZIN =='') {?>
                                                     <button type="submit" class="btn btn-success" >Simpan</button>
                                                  <?php
                                                } else { }?>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </div>
                                            </form>
      </div>
      
    </div>
  </div>
 
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Total Tagihan : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
<?php foreach ($reklame as $rk)  { ?>
  <div class="modal fade" id="myModal<?= $rk->ID_INC?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $ID=$rk->ID_INC;?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">DETAIL DATA REKLAME</h4>
        </div>
        <div class="modal-body">
         <div class="table-responsive">
        <table tyle="width: 100%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Kelurahan</th>
              <th class="text-center">LOKASI</th>
              <th class="text-center">Text</th>
              <th class="text-center">PxLxS</th>
              <th class="text-center">Jumlah</th>
              <th class="text-center">Deskripsi</th>             
              <th class="text-center">Pajak Terutang</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0;$det_rek=$this->db->query("SELECT * FROM V_DET_REKLAME_BEFORE WHERE SPTPD_REKLAME_ID='$ID'")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $rk_d->NAMAKEC?></td>
                <td><?= $rk_d->NAMAKELURAHAN?></td>
                <td><?= $rk_d->LOKASI_PASANG?></td>
                <td><?= $rk_d->TEKS?></td>
                <td><?= $rk_d->P.'x'.$rk_d->L.'x'.$rk_d->S?></td>
                <td><?= $rk_d->JUMLAH?></td>
                <td><?= $rk_d->DESKRIPSI?></td>
                <td align="right"><?=  number_format(str_replace(",",".",$rk_d->PAJAK_TERUTANG),'0','','.')?></td>
              </tr>
              <?php  $no++;$tot+=str_replace(",",".",$rk_d->PAJAK_TERUTANG);}  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="8"><b>TOTAL</b></td>
               
                <td align="right"><?=  number_format($tot,'0','','.')?></td>                                
              </tr>
              <tr>
                <td colspan="8"><b>DENDA</b></td>
              
                <td align="right"><?=  number_format($rk->DENDA,'0','','.')?></</td>
                                     
              </tr>
              <tr>
                <td colspan="8"><b>POTONGAN</b></td>
                <td align="right"><?=  number_format($rk->POTONGAN,'0','','.')?></</td>
                                            
              </tr>
              <tr>
                <td colspan="8"><b>TOTAL TAGIHAN</b></td>
                <td align="right"><?=  number_format($tot+$rk->DENDA-$rk->POTONGAN,'0','','.')?></td>                                
              </tr>
            </tbody>
          </table>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>

 

  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 40px auto;
    margin-right: 80;
    z-index: 99999999999999999;
  }
  table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }

  </style>
    <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>
