   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan_sptpd/laporan_hotel'?>">
                <input type='hidden' value='1' name='cek'>
                <div class="form-group">
                  <select  name="tahun" required="required" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('h_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select  name="bulan" required="required" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php if ($this->session->userdata('h_bulan')==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select onchange="getkel(this);" name="kecamatan"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('h_kecamatan')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kelurahan"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('h_kelurahan')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('h_npwpd'); ?>" placeholder="nama wp / usaha / npwpd / kobil">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($cek <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan_sptpd/laporan_hotel'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                              <?php }   ?>  
                                     <a href="<?php echo $cetak; ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Masa</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Kecamatan</th>
              <th class="text-center">Kelurahan</th>
              <th class="text-center">Jumlah kamar</th>
              <th class="text-center">Tarif rata-rata</th>
              <th class="text-center">Kamar terisi</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Status</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($hotel as $rk)  { ?>             
              <tr>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="center"><?php echo ++$start ?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $namabulan[$rk->MASA_PAJAK]?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->KODE_BILING?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->NPWPD?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->NAMA_WP?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->OBJEK_PAJAK?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->ALAMAT_OP?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->NAMAKEC?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->NAMAKELURAHAN?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>align="right"><?=  number_format($rk->JUMLAH_KAMAR,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="right"><?=  number_format($rk->TARIF_RATA_RATA,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="right"><?=  number_format($rk->KAMAR_TERISI,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="right"><?=  number_format($rk->DPP,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="right"><?=  number_format($rk->TAGIHAN,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="right"><?=  number_format($rk->DENDA,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="right"><?=  number_format($rk->POTONGAN,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?> align="right"><?=  number_format($rk->JUMLAH_KETETAPAN,'0','','.')?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->TGL_PENETAPAN?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?= $rk->JATUH_TEMPO?></td>
                <td <?php if ($rk->FILE_TRANSAKSI!='') {echo "bgcolor='#95e89c'"; }?>><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
                <td align="center"><a href="<?php echo base_url('Pdf/Pdf/pdf_kode_biling/').acak($rk->KODE_BILING);?>"><i class='fa fa-print'></i></a>
                <a href="<?php echo base_url('Master/pokok/upload_file/').$rk->KODE_BILING.'/01';?>"><i class='fa fa-paperclip'>
                </td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Total Tagihan : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
  <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>

   
