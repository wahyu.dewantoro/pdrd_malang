<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_sptpd extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mlaporan_sptpd');
        $this->load->model('Master/Mpokok');
 }
 function index()
 {
       
       $this->template->load('Welcome/halaman','sptpd_list');
    }
 function laporan_hotel()
 {      
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_hotel';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_hotel';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_hotel?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $hotel                 = $this->Mlaporan_sptpd->get_limit_data_rekap_hotel($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,
        );
        $this->session->set_userdata($sess);
        $data = array(
        	'title'			   => 'Laporan SPTPD Pajak Hotel',
            'hotel'            => $hotel,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_hotel($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_hotel',$data);
    }
    function laporan_restoran(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_restoran';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_restoran';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_restoran?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $restoran                 = $this->Mlaporan_sptpd->get_limit_data_rekap_restoran($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak Restoran',
            'restoran'            => $restoran,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_restoran($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_restoran',$data);
    }
     function laporan_hiburan(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_hiburan';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_hiburan';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_hiburan?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $hiburan                 = $this->Mlaporan_sptpd->get_limit_data_rekap_hiburan($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak Hiburan',
            'hiburan'            => $hiburan,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_hiburan($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_hiburan',$data);
    }
    function laporan_reklame(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= '';//urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= '';//urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = strtolower(urldecode($this->input->get('npwpd', TRUE)));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
      $cetak= base_url() . 'Excel/Excel/Excel_laporan_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            //$bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_reklame';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_reklame';
      $cetak= base_url() . 'Excel/Excel/Excel_laporan_reklame?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $reklame                 = $this->Mlaporan_sptpd->get_limit_data_rekap_reklame($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $this->input->get('npwpd', TRUE),    
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak Reklame',
            'reklame'            => $reklame,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
             'cetak'            => $cetak

        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_reklame($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_reklame',$data);
    }
    function laporan_ppj(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_ppj';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_ppj';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_ppj?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $ppj                 = $this->Mlaporan_sptpd->get_limit_data_rekap_ppj($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak PPJ',
            'ppj'            => $ppj,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_ppj($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_ppj',$data);
    }
    function laporan_galian(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_galian';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_galian';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_galian?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $galian                 = $this->Mlaporan_sptpd->get_limit_data_rekap_galian($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak Galian C',
            'galian'            => $galian,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_galian($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_galian',$data);
    }
    function laporan_galian_detail(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_galian_detail?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_galian_detail?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_galian_detail?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_galian_detail';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_galian_detail';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_galian_detail?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_galian_detail($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $galian                 = $this->Mlaporan_sptpd->get_limit_data_rekap_galian_detail($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
             'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Detail SPTPD Pajak Galian C',
            'galian'            => $galian,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_galian_detail($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_galian_detail',$data);
    }
    function laporan_parkir(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_parkir';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_parkir';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_parkir?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $parkir                 = $this->Mlaporan_sptpd->get_limit_data_rekap_parkir($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,

            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak Parkir',
            'parkir'            => $parkir,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_parkir($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_parkir',$data);
    }
    function laporan_at(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_at';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_at';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_at?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $at                 = $this->Mlaporan_sptpd->get_limit_data_rekap_at($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak Air Tanah',
            'at'            => $at,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
             'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_at($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_at',$data);
    }
    function delete_air($id=""){
    $db=$this->db->query("DELETE SPTPD_AIR_TANAH WHERE ID_INC='$id'");
        if($db){
            $nt="Data berhasil di hapus";    
        }else{
            $nt="Data tidak di hapus";    
        }
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    redirect('Laporan_sptpd/laporan_at');   
}
    function laporan_sb(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_sb';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_sb';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_sb?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $sb                 = $this->Mlaporan_sptpd->get_limit_data_rekap_sb($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SPTPD Pajak Sarang Burung',
            'sb'            => $sb,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_sb($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_sptpd_sb',$data);
    }
    function getkel(){
        $KODEKEC=$_POST['KODEKEC'];
        $data['kc']=$this->db->query("SELECT KODEKELURAHAN,NAMAKELURAHAN FROM KELURAHAN WHERE KODEKEC ='$KODEKEC' order by NAMAKELURAHAN asc")->result();
        $this->load->view('Laporan_sptpd/getkel',$data);
    }

        function laporan_reklame_detail(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        //$bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $habis_masa=urldecode($this->input->get('habis_masa', TRUE));
        $jns     = urldecode($this->input->get('jns', TRUE));
        $start   = intval($this->input->get('start'));
        if ($tahun <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&habis_masa='.urlencode($habis_masa).'&jns='.urlencode($jns));
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&habis_masa='.urlencode($habis_masa).'&jns='.urlencode($jns));
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&habis_masa='.urlencode($habis_masa).'&jns='.urlencode($jns));        } else {
            //$bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_reklame_detail';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_reklame_detail';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_reklame_detail?tahun=' . urlencode($tahun).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE).'&habis_masa='.urlencode($habis_masa).'&jns='.urlencode($jns));        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$habis_masa,$jns);
        $reklame                 = $this->Mlaporan_sptpd->get_limit_data_rekap_reklame_detail($config['per_page'], $start, $tahun,$kecamatan,$kelurahan,$npwpd,$habis_masa,$jns);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            //'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'npwpd'         => $npwpd,
            'jns_reklame'   => $jns
           // 'text'          => $text,
            //'upt'        => $upt,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan Detail SPTPD Pajak Reklame',
            'reklame'            => $reklame,
            'cek'            => $cek,
            'habis_masa'    =>$habis_masa,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_reklame_detail($tahun,$kecamatan,$kelurahan,$npwpd,$habis_masa,$jns);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->db->query("SELECT * FROM KECAMATAN WHERE KODEKEC!='01'")->result();
       $this->template->load('Welcome/halaman','laporan_sptpd_reklame_detail',$data);
    }

    function laporan_skpd_mamin(){    
        $cek      = urldecode($this->input->get('cek', TRUE));
        $tahun    = urldecode($this->input->get('tahun', TRUE));
        $bulan    = urldecode($this->input->get('bulan', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $npwpd    = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        if ($bulan <> '') {
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_skpd_mamin?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_skpd_mamin?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));;
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_skpd_mamin?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));
        } else {
            $bulan=date('n');
            $tahun=date('Y');
            $kecamatan='';
            $kelurahan='';
            $config['base_url']  = base_url() . 'Laporan_sptpd/laporan_skpd_mamin';
            $config['first_url'] = base_url() . 'Laporan_sptpd/laporan_skpd_mamin';
            $cetak= base_url() . 'Excel/Excel/Excel_laporan_skpd_mamin?tahun=' . urlencode($tahun).'&bulan='.urlencode($bulan).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&npwpd='.urldecode($this->input->get('npwpd', TRUE));            
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mlaporan_sptpd->total_rows_rekap_restoran_mamin($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $restoran                 = $this->Mlaporan_sptpd->get_limit_data_rekap_restoran_mamin($config['per_page'], $start, $tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $sess=array(
            'h_bulan'       => $bulan,
            'h_tahun'       => $tahun,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'h_npwpd'        => $npwpd,
        );
        $this->session->set_userdata($sess);
        $data = array(
            'title'            => 'Laporan SKPD Pajak Mamin',
            'restoran'            => $restoran,
            'cek'            => $cek,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'cetak'            => $cetak
        );
       $data['total_tagihan']=$this->Mlaporan_sptpd->sum_tagihan_restoran_mamin($tahun,$bulan,$kecamatan,$kelurahan,$npwpd);
       $data['mp']=$this->Mpokok->listMasapajak();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_skpd_mamin',$data);
    }

    function hapus_reklame($id=''){
         $res=$this->db->query("select * from SPTPD_REKLAME where ID_INC='$id' and status='2'")->row();
         if($res){
            $db=$this->db->query("DELETE SPTPD_REKLAME WHERE ID_INC='$id'");
            if($db){
                $nt="Data berhasil di hapus";    
            }else{
                $nt="Data tidak di hapus";    
            }
        }else{
            $nt="Data tidak ada";
        }
        $this->session->set_flashdata('notif', '<script>
          $(window).load(function(){
           swal("'.$nt.'", "", "success")
        });
        </script>');
        redirect('Laporan_sptpd/laporan_reklame');   
}
        function simpan_no_izin(){
        $kode_biling=$_POST['kode_biling'];
        $izin=$_POST['nomor_izin'];
        $this->db->query("update SPTPD_REKLAME set no_izin='$izin' where kode_biling='$kode_biling'");
        $this->db->query("update TAGIHAN set no_izin_reklmae='$izin' where kode_biling='$kode_biling'");
         redirect('Laporan_sptpd/laporan_reklame');   
    }
  
}