<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mreklame extends CI_Model
{

    
    function __construct()
    {
        parent::__construct();
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }

    // datatables
  function jsonreklame(){
        $t1  =$this->session->userdata('rek_bulan');
        $t2  =$this->session->userdata('rek_tahun');
        $gol =$this->session->userdata('golongan_reklame');
        if ($this->role==221) {
            if ($this->session->userdata('rek_tahun')!='' AND $this->session->userdata('rek_bulan')!='' AND $this->session->userdata('golongan_reklame')!='') {
            $ts ="TO_CHAR(TGL_INSERT, 'mm')='$t1' AND TO_CHAR(TGL_INSERT, 'YYYY')='$t2' AND NPWPD LIKE '%$gol%' OR LOWER(NAMA_WP) LIKE '%$gol%' OR LOWER(NAMA_USAHA) LIKE '%$gol%'";
            } else if ($this->session->userdata('rek_tahun')!='' AND $this->session->userdata('rek_bulan')!='') {
                $ts ="TO_CHAR(TGL_INSERT, 'mm')='$t1' AND TO_CHAR(TGL_INSERT, 'YYYY')='$t2'";
            } else if ($this->session->userdata('rek_tahun')!='' AND $this->session->userdata('golongan_reklame')!='') {
                $ts ="TO_CHAR(TGL_INSERT, 'YYYY')='$t2' AND NPWPD LIKE '%$gol%' OR LOWER(NAMA_WP) LIKE '%$gol%' OR LOWER(NAMA_USAHA) LIKE '%$gol%'";
            } else if ($this->session->userdata('rek_tahun')!='') {
                $ts ="TO_CHAR(TGL_INSERT, 'YYYY')='$t2'";
            } else {
                $ts ="TO_CHAR(TGL_INSERT, 'mm')=TO_CHAR(SYSDATE, 'mm') AND STATUS='2'";
            }
        } else if ($this->role==8) {
                $ts = "NPWP_INSERT = '$this->nip'";
        } else {
            if ($this->session->userdata('rek_tahun')!='' AND $this->session->userdata('rek_bulan')!='' AND $this->session->userdata('golongan_reklame')!='') {
            $ts ="TO_CHAR(TGL_INSERT, 'mm')='$t1' AND TO_CHAR(TGL_INSERT, 'YYYY')='$t2' AND NPWPD LIKE '%$gol%' OR LOWER(NAMA_WP) LIKE '%$gol%' OR LOWER(NAMA_USAHA) LIKE '%$gol%'";
            } else if ($this->session->userdata('rek_tahun')!='' AND $this->session->userdata('rek_bulan')!='') {
                $ts ="TO_CHAR(TGL_INSERT, 'mm')='$t1' AND TO_CHAR(TGL_INSERT, 'YYYY')='$t2'";
            } else if ($this->session->userdata('rek_tahun')!='' AND $this->session->userdata('golongan_reklame')!='') {
                $ts ="TO_CHAR(TGL_INSERT, 'YYYY')='$t2' AND NPWPD LIKE '%$gol%' OR LOWER(NAMA_WP) LIKE '%$gol%' OR LOWER(NAMA_USAHA) LIKE '%$gol%'";
            } else if ($this->session->userdata('rek_tahun')!='') {
                $ts ="TO_CHAR(TGL_INSERT, 'YYYY')='$t2'";
            } else {
                $ts ="TO_CHAR(TGL_INSERT, 'mm')=TO_CHAR(SYSDATE, 'mm')";
            }
        }
        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NAMA_USAHA,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,GETTOTALREKLAME(ID_INC) AS TOTAL,STATUS,TO_CHAR(TGL_INSERT, 'dd-mm-yyyy') AS TGL_,GETPETUGAS(NIP_INSERT)||getpetugas(NPWP_INSERT)USER_INSERT");
        $this->datatables->from('SPTPD_REKLAME');
        $this->datatables->where($ts);
        $this->db->order_by("TGL_INSERT DESC, TAHUN_PAJAK DESC, ID_INC DESC");
        $this->datatables->add_column('actionc', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdreklame/Reklame2/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        if ($this->role==221) {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/form_penetapan/$1'),'Proses','class="btn btn-xs btn-success"').'</div>', 'acak(ID_INC)');
        } else {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', ''.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdreklame/Reklame2/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'', 'acak(ID_INC)');     

        }        
        return $this->datatables->generate();
     }
     function jsonreklame_penetapan(){
        $ts ="STATUS='2'";
        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NAMA_USAHA,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,GETTOTALREKLAME(ID_INC) AS TOTAL,STATUS,GETPETUGAS(NIP_INSERT)||getpetugas(NPWP_INSERT)USER_INSERT");
        $this->datatables->from('SPTPD_REKLAME');
        $this->datatables->where($ts);
        $this->db->order_by("TGL_INSERT DESC, TAHUN_PAJAK DESC,ID_INC DESC");
        $this->datatables->add_column('actionc', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdreklame/Reklame2/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        if ($this->role==221) {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/form_penetapan/$1'),'Proses','class="btn btn-xs btn-success"').anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'acak(ID_INC)');
        } else {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', ''.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdreklame/Reklame2/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'', 'acak(ID_INC)');     

        }        
        return $this->datatables->generate();
     }
 
     function jsonreklamedetail($var=array()){
       /* $tahun=$var['tahun'];
        $bulan=$var['bulan'];*/
        $npwpd=$var['npwpd'];

        // if($npwpd!=''){
            /*$wh="WHERE KODE_BILING='$npwpd'  AND TO_CHAR(TGL_KETETAPAN,'YYYY')='$tahun' AND TO_CHAR(TGL_KETETAPAN,'fmMM')='$bulan' AND STATUS !='2'";*/
            $wh="WHERE KODE_BILING='$npwpd' AND STATUS !='2'";
        /*}else{
            $wh="WHERE TAHUN_PAJAK='$tahun' AND MASA_PAJAK='$bulan'";
        }*/

        
        $sql="SELECT KODE_BILING,A.ID_INC,NPWPD,NAMA_WP,ALAMAT_WP,B.PAJAK_TERUTANG,C.DESKRIPSI GOLONGAN,TO_CHAR(TGL_KETETAPAN,'DD-MM-YYYY') TGL_KETETAPAN,
TEKS,P||'x'||L PL,S,JUMLAH,B.MASA,TO_CHAR(B.BERLAKU_MULAI,'DD-MM-YYYY')MULAI,TO_CHAR(B.AKHIR_MASA_BERLAKU,'DD-MM-YYYY')AKHIR,LOKASI_PASANG
            FROM SPTPD_REKLAME A
            JOIN SPTPD_REKLAME_DETAIL B ON  B.SPTPD_REKLAME_ID = A.ID_INC
            JOIN OBJEK_PAJAK C ON C.ID_OP=B.GOLONGAN    
            $wh";
        // NPWPD,NAMA_WP,ALAMAT_WP,PAJAK_TERUTAN,GOLONGAN
        $this->datatables->select("ID_INC,NPWPD,NAMA_WP,ALAMAT_WP,PAJAK_TERUTANG,GOLONGAN,TGL_KETETAPAN,KODE_BILING,PL,TEKS,LOKASI_PASANG,S,JUMLAH,MULAI,AKHIR");
        $this->datatables->from("($sql)");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdreklame/Reklame2/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        return $this->datatables->generate();
     }

     function jsonreklame_detail_temp(){
        $this->datatables->select("ID_INC,BERLAKU_MULAI,B.NAMAKEC,C.NAMAKELURAHAN,D.DESKRIPSI,LOKASI_PASANG, TEKS,P,L,S,JUMLAH,A.MASA,ROKOK,KETINGGIAN,PAJAK_TERUTANG,A.JENIS_WAKTU,CASE WHEN A.JENIS_WAKTU = 1 THEN A.MASA||' th' WHEN A.JENIS_WAKTU = 2 THEN A.MASA||' bln' WHEN A.JENIS_WAKTU = 3 THEN A.MASA||' mgu' ELSE A.MASA||' hr' END AS WAKTU");
        $this->datatables->from('SPTPD_REKLAME_DETAIL_TEMP A');
        $this->datatables->join('KECAMATAN B', 'A.KECAMATAN = B.KODEKEC');
        $this->datatables->join('KELURAHAN C', 'A.KELURAHAN = C.KODEKELURAHAN AND A.KECAMATAN=C.KODEKEC');
        $this->datatables->join('OBJEK_PAJAK D', 'A.GOLONGAN = D.ID_OP');
        $this->datatables->where("NIP='$this->nip'");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="deletetemp" data-id="$1" href="javascript:void(0)" ').'</div>', 'ID_INC');
        return $this->datatables->generate();
     }
     function jsonreklame_detail(){
        $id=$this->session->userdata('sptpdreklame_id');
        $this->datatables->select("ID_INC,BERLAKU_MULAI,B.NAMAKEC,C.NAMAKELURAHAN,D.DESKRIPSI,LOKASI_PASANG, TEKS,P,L,S,JUMLAH,A.MASA,ROKOK,KETINGGIAN,PAJAK_TERUTANG,A.JENIS_WAKTU,CASE WHEN A.JENIS_WAKTU = 1 THEN A.MASA||' th' WHEN A.JENIS_WAKTU = 2 THEN A.MASA||' bln' WHEN A.JENIS_WAKTU = 3 THEN A.MASA||' mgu' ELSE A.MASA||' hr' END AS WAKTU");
        $this->datatables->from('SPTPD_REKLAME_DETAIL A');
        $this->datatables->join('KECAMATAN B', 'A.KECAMATAN = B.KODEKEC');
        $this->datatables->join('KELURAHAN C', 'A.KELURAHAN = C.KODEKELURAHAN AND A.KECAMATAN=C.KODEKEC');
        $this->datatables->join('OBJEK_PAJAK D', 'A.GOLONGAN = D.ID_OP');
        $this->datatables->where("SPTPD_REKLAME_ID='$id'");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdreklame/Reklame2/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="delete" data-id="$1" href="javascript:void(0)"').'</div>', 'ID_INC');
        return $this->datatables->generate();
     }

     function getById($id){
        return $this->db->query("SELECT * FROM SPTPD_REKLAME WHERE ID_INC='$id'")->row();
     }
     function getKec(){
        return $this->db->query("SELECT * FROM KECAMATAN")->result();
     }
     function getGol(){
        return $this->db->query("SELECT * FROM OBJEK_PAJAK where  ID_GOL=207 OR ID_GOL=208 OR ID_GOL=209 OR ID_GOL=206 order by JENIS_REKLAME,DESKRIPSI")->result();
     }


     function getWp(){
        return $this->db->query("SELECT NPWP,NAMA FROM WAJIB_PAJAK")->result();

     }

}

/* End of file Mmenu.php */
/* Location: ./application/models/Mmenu.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-08 05:30:09 */
/* http://harviacode.com */