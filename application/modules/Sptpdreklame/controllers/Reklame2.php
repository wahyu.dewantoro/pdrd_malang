<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reklame2 extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        
        $this->load->model('Mreklame');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->role=$this->session->userdata('MS_ROLE_ID');
        $this->nip=$this->session->userdata('NIP');
        $this->upt=$this->session->userdata('UNIT_UPT_ID');
        
    }

    public function index()
    {
        if(isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $golongan_reklame=$_GET['NPWPD'];
        } else {
                $bulan='';//date('d-m-Y');
                $tahun='';//date('Y');//date('d-m-Y');
                $golongan_reklame='';
        }
            $sess=array(
                    'rek_bulan'=>$bulan,
                    'rek_tahun'=>$tahun,
                    'golongan_reklame'=>$golongan_reklame
                    
            );
          $this->session->set_userdata($sess);
          $data['mp']=$this->Mpokok->listMasapajak();
          $data['GOLONGAN']=$this->Mreklame->getGol();
        $this->template->load('Welcome/halaman','reklame_list',$data);
    }

    public function penetapan_reklame(){
        $this->template->load('Welcome/halaman','penetapan_reklame');
    }
    public function tambah()
    {
       $response = array();
       $exp=explode('|', $this->input->post('KECAMATAN',TRUE));
       $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI');
       //$KECAMATAN=$this->input->post('KECAMATAN');
       $KELURAHAN=$this->input->post('KELURAHAN');
       $GOLONGAN=$this->input->post('GOLONGAN');
       $LOKASI_PASANG=$this->input->post('LOKASI_PASANG');
       $TEKS=$this->input->post('TEKS');
       $P=str_replace(',', '.', str_replace('.', '',$this->input->post('P')));
       $L=str_replace(',', '.', str_replace('.', '',$this->input->post('L')));
       $S=str_replace(',', '.', str_replace('.', '',$this->input->post('S')));
       $JASBONG=$this->input->post('JASBONG');
       $NJOP=str_replace(',', '.', str_replace('.', '',$this->input->post('NJOP')));
       $NILAI_STRATEGIS=str_replace(',', '.', str_replace('.', '',$this->input->post('NILAI_STRATEGIS')));
       $JENIS_WAKTU=$this->input->post('JENIS_WAKTU');
       $JUMLAH=$this->input->post('JUMLAH');
       $MASA=$this->input->post('MASA');
       $ROKOK=$this->input->post('ROKOK');
       $KETINGGIAN=$this->input->post('KETINGGIAN');
       $PAJAK_TERUTANG=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
       $NIP=$this->input->post('NIP');
       $rek=$this->db->query("select getNoRek('$GOLONGAN')REK from dual")->row();
       $kode_rek=$rek->REK;
       if ($JENIS_WAKTU==1) {
           $berlaku=" to_date('$BERLAKU_MULAI','dd/mm/yyyy')+ interval '1' year";
       } else if ($JENIS_WAKTU==2) {
           $berlaku="add_months(to_date('$BERLAKU_MULAI','dd/mm/yyyy'), '$MASA' )";
       } else if ($JENIS_WAKTU==3) {
           $berlaku="to_date('$BERLAKU_MULAI','dd/mm/yyyy') + ('$MASA'*7)";
       } else {
           if ($MASA=='1') {
               $ms=0;
           } else {
               $ms=$MASA;
           }
           $berlaku="to_date('$BERLAKU_MULAI','dd/mm/yyyy')+'$ms'";
       } 
       $sql=$this->db->query("INSERT INTO SPTPD_REKLAME_DETAIL_TEMP (BERLAKU_MULAI, KECAMATAN, KELURAHAN, GOLONGAN, LOKASI_PASANG, TEKS, P, L, S, JUMLAH, MASA, ROKOK, KETINGGIAN, PAJAK_TERUTANG, NIP,JASBONG,NJOP,NILAI_STRATEGIS,JENIS_WAKTU,AKHIR_BERLAKU,UPT_PASANG,NOREK) VALUES (to_date('$BERLAKU_MULAI','dd/mm/yyyy'), '$exp[0]', '$KELURAHAN', 
        '$GOLONGAN', '$LOKASI_PASANG', '$TEKS', '$P', '$L' ,'$S', '$JUMLAH', '$MASA', '$ROKOK', '$KETINGGIAN', '$PAJAK_TERUTANG', '$NIP','$JASBONG','$NJOP','$NILAI_STRATEGIS','$JENIS_WAKTU',$berlaku,'$exp[1]','$kode_rek')");

       if ($sql) {           
        $response['status']  = 'success';
        $response['message'] = 'Data Ditambah';
    } else {
        $response['status']  = 'error';
        $response['message'] = 'Unable to delete product ...';
    }
    echo json_encode($response);
}

public function hapustemp()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $row=$this->db->query("DELETE FROM SPTPD_REKLAME_DETAIL_TEMP WHERE ID_INC='$id'");
        
        if ($row) {
            $response['status']  = 'success';
            $response['message'] = 'Data Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}   
public function hapusdetail()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $row=$this->db->query("DELETE FROM SPTPD_REKLAME_DETAIL WHERE ID_INC='$id'");
        
        if ($row) {
            $response['status']  = 'success';
            $response['message'] = 'Data Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}    
function getkel(){
    $KODEKEC=$_POST['KODEKEC'];
    $exp=explode('|', $KODEKEC);
        //$data['parent']=$parent;
    $data['kc']=$this->db->query("SELECT KODEKELURAHAN,NAMAKELURAHAN FROM KELURAHAN WHERE KODEKEC ='$exp[0]' order by NAMAKELURAHAN asc")->result();
        // print_r($data);
    $this->load->view('sptpdreklame/getkel',$data);
} 
public function templist()
{
    $this->load->view('reklame_detail_temp');
}
public function daftarreklame()
{
    $this->load->view('reklame_detail');
}
function create(){
    $data=array(
        'mp'                        =>$this->Mpokok->listMasapajak(),
        'disable'                   => '',
        'not'                       => '1',
        'update'                    => '0',
        'button'                    => 'Form SPOP Reklame',
        'action'                    => base_url().'sptpdreklame/Reklame2/createaction',
        'ID_INC'                    => set_value('ID_INC'),
        'MASA_PAJAK'                => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'               => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                   => set_value('NAMA_WP'),
        'ALAMAT_WP'                 => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'              => set_value('ALAMAT_USAHA'),
        'NPWPD'                     => set_value('NPWPD'),
        'JENIS_REKLAME'             => set_value('JENIS_REKLAME'),
        'ISI_REKLAME'               => set_value('ISI_REKLAME'),
        'TEMPAT_PEMASANGAN_REKLAME' => set_value('TEMPAT_PEMASANGAN_REKLAME'),
        'KLASIFIKASI_REKLAME'       => set_value('KLASIFIKASI_REKLAME'),
        'UKURAN_REKLAME'            => set_value('UKURAN_REKLAME'),
        'JUMLAH_REKLAME'            => set_value('JUMLAH_REKLAME'),
        'DPP'                       => set_value('DPP'),
        'PAJAK_TERUTANG'            => set_value('PAJAK_TERUTANG'),
        'KECAMATAN'                 => set_value('KECAMATAN'),
        'ID_GOL'                    => set_value('ID_GOL'),
        'KODE_BILING'               => set_value('KODE_BILING'),
        'NIP'                       =>set_value('NIP',$this->nip),
        'BERLAKU_MULAI'             => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'        => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                       =>$this->Mreklame->getKec(),
        'GOLONGAN'                  =>$this->Mreklame->getGol(),
        'param'                     =>'1'
    );
    $this->template->load('Welcome/halaman','reklame_form',$data);
}

function createaction(){
    $penerimaan=$this->input->post('TANGGAL_PENERIMAAN');
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->trans_start();
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK') );
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK') );
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP') );
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP') );
    $this->db->set('NAMA_USAHA', $exp[0] );
    $this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA') );
    $this->db->set('TANGGAL_PENERIMAAN', "to_date('$penerimaan','dd/mm/yyyy')",false);
    $this->db->set('NPWPD', $this->input->post('NPWPD') );
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('NIP_INSERT',$this->nip);
    $this->db->set('STATUS','2');
    $this->db->set('TGL_INSERT',"sysdate",false);
    $this->db->set('KODEKEC', $this->input->post('KODEKEC') );
    $this->db->set('KODEKEL', $this->input->post('KODEKEL') );
    $this->db->set('DPP', $this->input->post('KODEKEL') );
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('UPT_INSERT',$this->upt);
    $this->db->insert('SPTPD_REKLAME');
    $this->db->trans_complete();

    $row=$this->db->query("SELECT MAX(ID_INC) AS ID FROM SPTPD_REKLAME WHERE NIP_INSERT='$this->nip'")->row();  
    $sptpdreklame_id=$row->ID;
    $temp=$this->db->query("SELECT * FROM SPTPD_REKLAME_DETAIL_TEMP WHERE NIP='$this->nip'")->result();
    foreach ($temp as $temp) {
        $this->db->query("INSERT INTO SPTPD_REKLAME_DETAIL (BERLAKU_MULAI, KECAMATAN, KELURAHAN, GOLONGAN, LOKASI_PASANG, TEKS, P, L, S, JUMLAH, MASA, ROKOK, KETINGGIAN, PAJAK_TERUTANG, NIP,SPTPD_REKLAME_ID,
                        JABONG,NJOP,NILAI_STRATEGIS,JENIS_WAKTU,AKHIR_MASA_BERLAKU,UPT_PASANG,NOREK) VALUES
                        ('$temp->BERLAKU_MULAI', '$temp->KECAMATAN', '$temp->KELURAHAN', '$temp->GOLONGAN', '$temp->LOKASI_PASANG',
                         '$temp->TEKS', '$temp->P', '$temp->L' ,'$temp->S', '$temp->JUMLAH', '$temp->MASA', '$temp->ROKOK',
                          '$temp->KETINGGIAN', '$temp->PAJAK_TERUTANG', '$temp->NIP','$sptpdreklame_id','$temp->JASBONG','$temp->NJOP','$temp->NILAI_STRATEGIS','$temp->JENIS_WAKTU','$temp->AKHIR_BERLAKU','$temp->UPT_PASANG','$temp->NOREK')");
    }
    $this->db->query("DELETE FROM SPTPD_REKLAME_DETAIL_TEMP WHERE NIP='$this->nip'");      
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di simpan";
    }else{
            // gagal
        $nt="Gagal di simpan";
    }
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
       swal("'.$nt.'", "", "success")
   });
   </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('sptpdreklame/Reklame2'));
    }
}

function jsonreklame(){
 header('Content-Type: application/json');
 echo $this->Mreklame->jsonreklame();
}

function jsonreklame_penetapan(){
 header('Content-Type: application/json');
 echo $this->Mreklame->jsonreklame_penetapan();
}
function jsonreklame_detail_temp(){
 header('Content-Type: application/json');
 echo $this->Mreklame->jsonreklame_detail_temp();
}
function jsonreklame_detail(){
 header('Content-Type: application/json');
 echo $this->Mreklame->jsonreklame_detail();
}

function update($id){
    $id=rapikan($id);
    $res=$this->Mreklame->getById($id);
    $data['sptpdreklame_id'] =$id;
    $data['status'] ='1';
    $this->session->set_userdata($data);
    if($res){
        $data=array(
            'mp'                        =>$this->Mpokok->listMasapajak(),
            'disable'                   => '',
            'not'                       => '2',
            'button'                    => 'Form SPTPD Reklame',
            'action'                    => base_url().'sptpdreklame/Reklame2/updateaction',
            'ID_INC'                    => set_value('ID_INC',$res->ID_INC),
            'MASA_PAJAK'                => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'               => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                   => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                 => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'              => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                     => set_value('NPWPD',$res->NPWPD),
            'JENIS_REKLAME'             => set_value('JENIS_REKLAME',$res->JENIS_REKLAME),
            'ISI_REKLAME'               => set_value('ISI_REKLAME',$res->ISI_REKLAME),
            'TEMPAT_PEMASANGAN_REKLAME' => set_value('TEMPAT_PEMASANGAN_REKLAME',$res->TEMPAT_PEMASANGAN_REKLAME),
            'KLASIFIKASI_REKLAME'       => set_value('KLASIFIKASI_REKLAME',$res->KLASIFIKASI_REKLAME),
            'UKURAN_REKLAME'            => set_value('UKURAN_REKLAME',number_format($res->UKURAN_REKLAME,'0','','.')),
            'JUMLAH_REKLAME'            => set_value('JUMLAH_REKLAME',number_format($res->JUMLAH_REKLAME,'0','','.')),
            'DPP'                       => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'            => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'BERLAKU_MULAI'             => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
            'KECAMATAN'                 => set_value('KECAMATAN'),
            'ID_GOL'                    => set_value('ID_GOL'),
            'KODE_BILING'               => set_value('KODE_BILING'),
            'NIP'                       =>set_value('NIP',$this->nip),
            'kec'                       =>$this->Mreklame->getKec(),
            'TANGGAL_PENERIMAAN'        => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
            'GOLONGAN'                  =>$this->Mreklame->getGol(),
            'update'                    => '',
            'param'                      =>''
        );
        $data['jenis']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$res->NPWPD."' and jenis_pajak='04'")->result();  
        $this->template->load('Welcome/halaman','reklame_form',$data);
    }else{
     redirect('sptpdreklame/Reklame2');
 }
}

function detail($id){
    $id=rapikan($id);
    $res=$this->Mreklame->getById($id);
    $data['sptpdreklame_id'] =$id;
    $data['status'] ='0';
    $this->session->set_userdata($data);
    if($res){
        $data=array(
            'mp'                        =>$this->Mpokok->listMasapajak(),
            'disable'                   => 'disabled',
            'not'                       => '2',
            'button'                    => 'Form SPTPD Reklame',
            'action'                    => base_url().'sptpdreklame/Reklame2/updateaction',
            'ID_INC'                    => set_value('ID_INC',$res->ID_INC),
            'MASA_PAJAK'                => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'               => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                   => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                 => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'              => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                     => set_value('NPWPD',$res->NPWPD),
            'JENIS_REKLAME'             => set_value('JENIS_REKLAME',$res->JENIS_REKLAME),
            'ISI_REKLAME'               => set_value('ISI_REKLAME',$res->ISI_REKLAME),
            'TEMPAT_PEMASANGAN_REKLAME' => set_value('TEMPAT_PEMASANGAN_REKLAME',$res->TEMPAT_PEMASANGAN_REKLAME),
            'KLASIFIKASI_REKLAME'       => set_value('KLASIFIKASI_REKLAME',$res->KLASIFIKASI_REKLAME),
            'UKURAN_REKLAME'            => set_value('UKURAN_REKLAME',number_format($res->UKURAN_REKLAME,'0','','.')),
            'JUMLAH_REKLAME'            => set_value('JUMLAH_REKLAME',number_format($res->JUMLAH_REKLAME,'0','','.')),
            'DPP'                       => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'            => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'BERLAKU_MULAI'             => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
            'KECAMATAN'                 => set_value('KECAMATAN'),
            'ID_GOL'                    => set_value('ID_GOL'),
            'KODE_BILING'               => set_value('KODE_BILING',$res->KODE_BILING),
            'NIP'                       =>set_value('NIP',$this->nip),
            'kec'                       =>$this->Mreklame->getKec(),
            'GOLONGAN'                  =>$this->Mreklame->getGol(),
            'update'                   => '',
        );
        $this->template->load('Welcome/halaman','reklame_form',$data);
    }else{
     redirect('sptpdreklame/Reklame2');
 }
}

function updateaction(){
    $id=$this->input->post('ID_INC');
    $this->db->trans_start();
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK') );
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK') );
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP') );
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP') );
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA') );
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA') );
    $this->db->set('NPWPD', $this->input->post('NPWPD') );
    $this->db->where('ID_INC',$this->input->post('ID_INC'));
    $this->db->update('SPTPD_REKLAME');
    $this->db->trans_complete();
    $temp=$this->db->query("SELECT * FROM SPTPD_REKLAME_DETAIL_TEMP WHERE NIP='$this->nip'")->result();
    foreach ($temp as $temp) {
        $this->db->query("INSERT INTO SPTPD_REKLAME_DETAIL (BERLAKU_MULAI, KECAMATAN, KELURAHAN, GOLONGAN, LOKASI_PASANG, TEKS, P, L, S, JUMLAH, MASA, ROKOK, KETINGGIAN, PAJAK_TERUTANG, NIP,SPTPD_REKLAME_ID,JABONG,NJOP,NILAI_STRATEGIS,JENIS_WAKTU,AKHIR_MASA_BERLAKU,UPT_PASANG) VALUES (to_date('$temp->BERLAKU_MULAI','dd/mm/yyyy'), '$temp->KECAMATAN', '$temp->KELURAHAN', '$temp->GOLONGAN', '$temp->LOKASI_PASANG', '$temp->TEKS', '$temp->P', '$temp->L' ,'$temp->S', '$temp->JUMLAH', '$temp->MASA', '$temp->ROKOK', '$temp->KETINGGIAN', '$temp->PAJAK_TERUTANG', '$temp->NIP','$id','$temp->JABONG','$temp->NJOP','$temp->NILAI_STRATEGIS','$temp->JENIS_WAKTU','$temp->AKHIR_BERLAKU','$temp->UPT_PASANG')");
    }
    $this->db->query("DELETE FROM SPTPD_REKLAME_DETAIL_TEMP WHERE NIP='$this->nip'");   
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di update";
    }else{
            // gagal
        $nt="Gagal di update";
    }
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
       swal("'.$nt.'", "", "success")
   });
   </script>');
    redirect('sptpdreklame/Reklame2');
}

function delete($id){
    $id=rapikan($id);
 $res=$this->Mreklame->getById($id);
//echo $res->STATUS;
if($res->STATUS=='2'){
    $db=$this->db->query("DELETE SPTPD_REKLAME WHERE ID_INC='$id'");
    if($db){
        $nt="Data berhasil di hapus";    
    }else{
        $nt="Data tidak di hapus";    
    }
}else{
    $nt="Data tidak ada/Sudah Ditetapkan";
}
$this->session->set_flashdata('notif', '<script>
  $(window).load(function(){
   swal("'.$nt.'", "", "success")
});
</script>');
redirect('sptpdreklame/Reklame2'); 
}
function form_penetapan($id){
    $id=rapikan($id);
    $res=$this->Mreklame->getById($id);
    $data['sptpdreklame_id'] =$id;
    $data['status'] ='0';
    $this->session->set_userdata($data);
    if($res){
        $data=array(
            'mp'                        =>$this->Mpokok->listMasapajak(),
            'disable'                   => 'disabled',
            'button'                    => 'Form SPTPD Reklame',
            'action'                    => base_url().'sptpdreklame/Reklame2/updateaction',
            'ID_INC'                    => set_value('ID_INC',$res->ID_INC),
            'MASA_PAJAK'                => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'               => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                   => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                 => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'              => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                     => set_value('NPWPD',$res->NPWPD),
            'JENIS_REKLAME'             => set_value('JENIS_REKLAME',$res->JENIS_REKLAME),
            'ISI_REKLAME'               => set_value('ISI_REKLAME',$res->ISI_REKLAME),
            'TEMPAT_PEMASANGAN_REKLAME' => set_value('TEMPAT_PEMASANGAN_REKLAME',$res->TEMPAT_PEMASANGAN_REKLAME),
            'KLASIFIKASI_REKLAME'       => set_value('KLASIFIKASI_REKLAME',$res->KLASIFIKASI_REKLAME),
            'UKURAN_REKLAME'            => set_value('UKURAN_REKLAME',number_format($res->UKURAN_REKLAME,'0','','.')),
            'JUMLAH_REKLAME'            => set_value('JUMLAH_REKLAME',number_format($res->JUMLAH_REKLAME,'0','','.')),
            'DPP'                       => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'            => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'BERLAKU_MULAI'             => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
            'KECAMATAN'                 => set_value('KECAMATAN'),
            'ID_GOL'                    => set_value('ID_GOL'),
            'STATUS'                    => set_value('STATUS',$res->STATUS),
            'NIP'                       =>set_value('NIP',$this->nip),
            'kec'                       =>$this->Mreklame->getKec(),
            'GOLONGAN'                  =>$this->Mreklame->getGol(),
            'update'                   => '',
        );
        $this->template->load('Welcome/halaman','reklame_penetapan_form',$data);
    }else{
        $nt="Data tidak ada";
        $this->session->set_flashdata('notif', '<script>
          $(window).load(function(){
             swal("'.$nt.'", "", "success")
         });
         </script>');
       redirect('sptpdreklame/Reklame2');
     
    }
   }
   function proses_penetapan(){
          $id=$_POST['del_id'];
          $NIP=$this->nip;
          $row=$this->db->query("SELECT nama_wp,JUMLAH_KETETAPAN,status from SPTPD_REKLAME where ID_INC='$id'")->row();          
          $pt=$row->JUMLAH_KETETAPAN;
          $nm=$row->NAMA_WP;

          if ($row->STATUS=='2') {
              $kb=$this->Mpokok->getSqIdBiling();
              $op=$this->db->query("select golongan from sptpd_reklame_detail where sptpd_reklame_id='$id' and ROWNUM <= 1")->row();
              $rek=$this->db->query("select  getNoRek('$op->GOLONGAN')REK from dual")->row();
              $bank=$this->db->query("select getnorek_bank('$op->GOLONGAN')BANK from dual")->row();
              $KD_BILING="'$bank->BANK'||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'";
              $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
              $ed=$expired->ED;
              $ed_tabel=$expired->NOW;
              $va=$this->Mpokok->getVA($op->GOLONGAN);

                   $this->db->query("UPDATE SPTPD_REKLAME SET STATUS ='0',JUMLAH_KETETAPAN=GETTOTALREKLAME(ID_INC),TGL_KETETAPAN=SYSDATE,KODE_BILING=$KD_BILING,NO_VA='$va',ED_VA='$ed_tabel'
                                WHERE ID_INC ='$id'");
                  $this->db->query("CALL P_INSERT_TAGIHAN_REKLAME('$id','$rek->REK')");
                  $this->Mpokok->registration($va,$nm,$pt,$ed);
              } else {
                  # code...
              }//$this->db->last_query();
          
}
function get_nama_usaha(){
        $data1=$_POST['npwp'];
        
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$data1."' and jenis_pajak='04'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    function get_alamat_usaha(){
        $data1=$_POST['nama'];
        
            //$data['parent']=$parent;
        $data=$this->db->query("SELECT nama_usaha,ALAMAT_USAHA from tempat_usaha where nama_usaha='".$data1."' and jenis_pajak='04'")->row();
            // print_r($data);
        echo $data->ALAMAT_USAHA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    function pdf_kode_biling($id_inc=""){
            $kb=$this->db->query("select KODE_BILING from SPTPD_REKLAME where id_inc='$id_inc'")->row();
            $data['catatan']=$this->Mpokok->getCatatan();
            $data['cara_bayar']=$this->db->query("select * from ms_catatan where keperluan='va' ")->result();
            $kode_biling=$kb->KODE_BILING;
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Pdf_kode_biling',$data,true);
            
            $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */