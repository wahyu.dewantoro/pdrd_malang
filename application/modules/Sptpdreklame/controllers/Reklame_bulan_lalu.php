<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reklame_bulan_lalu extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        
        $this->load->model('Mreklame');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
        $this->upt=$this->session->userdata('UNIT_UPT_ID');
    }

    public function index()
    {
        if(isset($_POST['NPWPD'])){
          /*  $bulan            =$_POST['MASA_PAJAK'];
            $tahun            =$_POST['TAHUN_PAJAK'];*/
            $bulan_berikutnya =$_POST['MASA_PAJAK_SELANJUTNYA'];
            $npwpd            =$_POST['NPWPD'];
        }else{
            /*$bln   =date('Y-m-d');
            $bll   =date('m',strtotime('-1 month',strtotime($bln)));
            $bulan =$bll;
            $tahun =date('Y');*/
            $npwpd =null;
            $bulan_berikutnya=date('m');
        }

      // $this->session->set_userdata($sess);
       /* $data['tahun']=$tahun;
        $data['bulan']=$bulan;*/
        $data['npwpd']=$npwpd;
        $data['bulan_berikutnya']=$bulan_berikutnya;
      $data['mp']=$this->Mpokok->listMasapajak(); 
      $data['mp_s']=$this->Mpokok->listMasapajak();  
      $this->template->load('Welcome/halaman','sptpdreklame/reklame_pilihan_bulan_lalu',$data);
  }


  function jsonreklame_bl(){
     header('Content-Type: application/json');
     $data['tahun']=$_POST['tahun'];
     $data['bulan']=(int)$_POST['bulan'];
     $data['npwpd']=$_POST['npwpd'];
     echo $this->Mreklame->jsonreklamedetail($data);
}

 
    
public function daftarreklame()
{
    $this->load->view('reklame_detail');
}


function jsonreklame(){
 header('Content-Type: application/json');
 echo $this->Mreklame->jsonreklame();
}

function jsonreklame_detail_temp(){
 header('Content-Type: application/json');
 echo $this->Mreklame->jsonreklame_detail_temp();
}

function jsonreklame_detail(){
 header('Content-Type: application/json');
 echo $this->Mreklame->jsonreklame_detail();
}

function data_bulan_lalu($id){
    $res=$this->Mreklame->getById($id);
    $data['sptpdreklame_id'] =$id;
    $data['status'] ='1';
    $this->session->set_userdata($data);
    if($res){
        $data=array(
            'mp'                        =>$this->Mpokok->listMasapajak(),
            'disable'                   => '',
            'button'                    => 'Form SPTPD Reklame',
            'action'                    => base_url().'sptpdreklame/Reklame2/updateaction',
            'ID_INC'                    => set_value('ID_INC',$res->ID_INC),
            'MASA_PAJAK'                => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'               => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                   => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                 => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'              => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                     => set_value('NPWPD',$res->NPWPD),
            'JENIS_REKLAME'             => set_value('JENIS_REKLAME',$res->JENIS_REKLAME),
            'ISI_REKLAME'               => set_value('ISI_REKLAME',$res->ISI_REKLAME),
            'TEMPAT_PEMASANGAN_REKLAME' => set_value('TEMPAT_PEMASANGAN_REKLAME',$res->TEMPAT_PEMASANGAN_REKLAME),
            'KLASIFIKASI_REKLAME'       => set_value('KLASIFIKASI_REKLAME',$res->KLASIFIKASI_REKLAME),
            'UKURAN_REKLAME'            => set_value('UKURAN_REKLAME',number_format($res->UKURAN_REKLAME,'0','','.')),
            'JUMLAH_REKLAME'            => set_value('JUMLAH_REKLAME',number_format($res->JUMLAH_REKLAME,'0','','.')),
            'DPP'                       => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'            => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'BERLAKU_MULAI'             => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
            'KECAMATAN'                 => set_value('KECAMATAN'),
            'ID_GOL'                    => set_value('ID_GOL'),
            'NIP'                       =>set_value('NIP',$this->nip),
            'kec'                       =>$this->Mreklame->getKec(),
            'TANGGAL_PENERIMAAN'        => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
            'GOLONGAN'                  =>$this->Mreklame->getGol(),
            'update'                   => '',
        );
        $this->template->load('Welcome/halaman','reklame_form_bulan_lalu',$data);
    }else{
     redirect('sptpdreklame/Reklame2');
 }
}

function updateaction(){
    $id=$this->input->post('ID_INC');
    $this->db->trans_start();
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK') );
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK') );
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP') );
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP') );
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA') );
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA') );
    $this->db->set('NPWPD', $this->input->post('NPWPD') );
    $this->db->where('ID_INC',$this->input->post('ID_INC'));
    $this->db->update('SPTPD_REKLAME');
    $this->db->trans_complete();
    $temp=$this->db->query("SELECT * FROM SPTPD_REKLAME_DETAIL_TEMP WHERE NIP=$this->nip")->result();
    foreach ($temp as $temp) {
        $this->db->query("INSERT INTO SPTPD_REKLAME_DETAIL (BERLAKU_MULAI, KECAMATAN, KELURAHAN, GOLONGAN, LOKASI_PASANG, TEKS, P, L, S, JUMLAH, MASA, ROKOK, KETINGGIAN, PAJAK_TERUTANG, NIP,SPTPD_REKLAME_ID) VALUES (to_date('$temp->BERLAKU_MULAI','dd/mm/yyyy'), '$temp->KECAMATAN', '$temp->KELURAHAN', '$temp->GOLONGAN', '$temp->LOKASI_PASANG', '$temp->TEKS', '$temp->P', '$temp->L' ,'$temp->S', '$temp->JUMLAH', '$temp->MASA', '$temp->ROKOK', '$temp->KETINGGIAN', '$temp->PAJAK_TERUTANG', '$temp->NIP','$id')");
    }
    $this->db->query("DELETE FROM SPTPD_REKLAME_DETAIL_TEMP WHERE NIP=$this->nip");   
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di update";
    }else{
            // gagal
        $nt="Gagal di update";
    }
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
       swal("'.$nt.'", "", "success")
   });
   </script>');
    redirect('sptpdreklame/Reklame2');
}

    function delete($id){
         $res=$this->Mreklame->getById($id);
         if($res){
            $db=$this->db->query("DELETE SPTPD_REKLAME WHERE ID_INC='$id'");
            if($db){
                $nt="Data berhasil di hapus";    
            }else{
                $nt="Data tidak di hapus";    
            }
        }else{
            $nt="Data tidak ada";
        }
        $this->session->set_flashdata('notif', '<script>
          $(window).load(function(){
           swal("'.$nt.'", "", "success")
        });
        </script>');
        redirect('sptpdreklame/Reklame2');   
    }


    function saveFormBulanLalu(){
        if(!empty($_POST['id_inc'])){
           //$NOW=date('Y-m-d H:i:s');
           $TP=date('Y');
           $MP=$_POST['bulan_berikutnya'];
           //$kb=$this->Mpokok->getSqIdBiling();
           //$KD_BILING='';//"3507400||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'";
           //$DATE="to_date(sysdate,'dd/mm/yyyy')";
           $NIP=$this->nip;
           $IP=$this->Mpokok->getIP();
           $berkas=$this->Mpokok->getNoBerkas();
           $this->db->trans_start();
                $this->db->query("INSERT INTO SPTPD_REKLAME (MASA_PAJAK, TAHUN_PAJAK,
                                      NAMA_WP,
                                       ALAMAT_WP,
                                        NAMA_USAHA, 
                                        ALAMAT_USAHA, 
                                        NPWPD, 
                                        JENIS_REKLAME,
                                         ISI_REKLAME, 
                                         TEMPAT_PEMASANGAN_REKLAME,
                                          KLASIFIKASI_REKLAME, 
                                          UKURAN_REKLAME, 
                                          JUMLAH_REKLAME, 
                                          DPP,
                                           PAJAK_TERUTANG, 
                                             JUMLAH_PAJAK_TERUTANG,
                                              JUMLAH_KETETAPAN,                   
                                               NIP_INSERT,
                                                 IP_INSERT,
                                                  TGL_INSERT,
                                                   STATUS,  
                                                   TANGGAL_PENERIMAAN,
                                                    KODEKEC, 
                                                    KODEKEL,
                                                     NO_REK_BANK,
                                                     NO_FORMULIR, 
                                                     UPT_INSERT, 
                                                     ID_USAHA,AMBIL_DATA_LALU )
                                 SELECT '$MP','$TP', 
                                      NAMA_WP,
                                       ALAMAT_WP,
                                        NAMA_USAHA, 
                                        ALAMAT_USAHA, 
                                        NPWPD, 
                                        JENIS_REKLAME,
                                         ISI_REKLAME, 
                                         TEMPAT_PEMASANGAN_REKLAME,
                                          KLASIFIKASI_REKLAME, 
                                          UKURAN_REKLAME, 
                                          JUMLAH_REKLAME, 
                                          DPP,
                                           PAJAK_TERUTANG, 
                                             JUMLAH_PAJAK_TERUTANG,
                                              JUMLAH_KETETAPAN,                   
                                               '$NIP',
                                                 '$IP',
                                                  sysdate,
                                                   '2',  
                                                   sysdate,
                                                    KODEKEC, 
                                                    KODEKEL,
                                                     NO_REK_BANK,
                                                     '$berkas', 
                                                     '$this->upt',
                                                     ID_USAHA,'1' FROM SPTPD_REKLAME WHERE ID_INC='".$_POST['id_inc']."'");
               // echo $this->db->last_query();
                    $rinsert_id = $this->db->query("select max(id_inc) id_inc   from sptpd_reklame where nip_insert='$NIP' and ip_insert='$IP'")->row();
                    $insert_id=$rinsert_id->ID_INC;
                    $this->db->query("INSERT INTO SPTPD_REKLAME_DETAIL (BERLAKU_MULAI, 
                                        KECAMATAN, 
                                        KELURAHAN, 
                                        GOLONGAN, 
                                        LOKASI_PASANG,
                                         TEKS, P, L, S,
                                         JUMLAH, 
                                         MASA,
                                          ROKOK, 
                                          KETINGGIAN,
                                           PAJAK_TERUTANG,
                                            NIP,
                                             SPTPD_REKLAME_ID, 
                                             JABONG, NJOP, NILAI_STRATEGIS, JENIS_WAKTU,AKHIR_MASA_BERLAKU, UPT_PASANG, NOREK, JENIS_REKLAME)
                                         select AKHIR_MASA_BERLAKU, 
                                        KECAMATAN, 
                                        KELURAHAN, 
                                        GOLONGAN, 
                                        LOKASI_PASANG,
                                         TEKS, P, L, S,
                                         JUMLAH, 
                                         MASA,
                                          ROKOK, 
                                          KETINGGIAN,
                                           PAJAK_TERUTANG,
                                            NIP,
                                             '$insert_id', 
                                             JABONG, NJOP, NILAI_STRATEGIS, JENIS_WAKTU,
                                             CASE WHEN JENIS_WAKTU = 1 THEN TO_DATE(AKHIR_MASA_BERLAKU + interval '1' year)
              WHEN JENIS_WAKTU = 2 THEN TO_DATE(add_months(AKHIR_MASA_BERLAKU, 1 ))
              WHEN JENIS_WAKTU = 3 THEN TO_DATE(AKHIR_MASA_BERLAKU + (MASA*7)) ELSE TO_DATE(AKHIR_MASA_BERLAKU+MASA)  END , UPT_PASANG, NOREK, JENIS_REKLAME
                                         from sptpd_reklame_detail
                                         where SPTPD_REKLAME_ID='".$_POST['id_inc']."'");
                
            //}
               //echo     $this->db->last_query();
            //$this->db->query("CALL P_INSERT_TAGIHAN_REKLAME('$insert_id')");
            $this->db->trans_complete();

                    // sukses
            //echo $jumlah;
                $nt="Berhasil di simpan";
                $this->session->set_flashdata('notif', '<script>
                      $(window).load(function(){
                         swal("'.$nt.'", "", "success")
                     });
                     </script>');
               
           
           redirect('sptpdreklame/Reklame2'); 
        }else{
             // gagal 
                 $nt="Gagal di simpan";
                 $this->session->set_flashdata('notif', '<script>
                      $(window).load(function(){
                         swal("'.$nt.'", "", "warning")
                     });
                     </script>');
            redirect('sptpdreklame/Reklame_bulan_lalu');
        }
        // 
    }


}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */