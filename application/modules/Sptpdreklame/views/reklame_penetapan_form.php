   <?php $namabulan=array(
    '',
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
    ) ?>

   <style>
   th { font-size: 10px; }
   td { font-size: 10px; }
   label { font-size: 11px;}
   textarea { font-size: 11px;}
   .input-group span{ font-size: 11px;}
   input[type='text'] { font-size: 11px; height:30px}
 </style>
 <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>
  <div class="pull-right">
    <?php echo anchor('sptpdreklame/Reklame2','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >  
          <div class="row">
            <div class="col-md-12 col-sm-6">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak </h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" />
                    <?php $NB=$namabulan[$MASA_PAJAK]; ?>
                      <input type="hidden" id="MP" value="<?php echo $NB ?>" />
                      <input type="hidden" id="TP" value="<?php echo $TAHUN_PAJAK; ?>" />
                    <?php if($disable=='' AND $param!=''){?>                                 
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pilihan
                        </label>
                        <div class="col-md-9">
                          <a href="<?php echo site_url('sptpdreklame/Reklame_bulan_lalu') ?>" class="btn btn-danger"><i class="fa fa-edit"></i> Gunakan Data Bulan Lalu</a>
                        </div>
                      </div>  -->
                      <?php } ?>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select style="font-size: 11px" id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($mp as $mp){ ?>
                          <option <?php if(!empty($MASA_PAJAK)){ if($mp==$MASA_PAJAK){echo "selected";}} else {if($mp==date("m")){echo "selected";}}?> value="<?php echo $mp?>"><?php echo $namabulan[$mp] ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <input type="text" <?php echo $disable?> id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun Pajak" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>">                
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD  <sup>*</sup> 
                      </label>
                      <div class="col-md-9">
                        <input type="text" id="NPWPD" <?php echo $disable?> name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>">                  
                      </div>
                    </div>      
                  </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <input type="text" id="NAMA_WP" <?php echo $disable?> name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>">                
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Alamat WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <textarea style="font-size: 11px" id="ALAMAT_WP" rows="3" <?php echo $disable?> class="resizable_textarea form-control" placeholder="Alamat" name="ALAMAT_WP"><?php echo $ALAMAT_WP; ?></textarea>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <input type="text" id="NAMA_USAHA" <?php echo $disable?> name="NAMA_USAHA" required="required" placeholder="Nama Perusahaan" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>">                     
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <textarea style="font-size: 11px" id="ALAMAT_USAHA" rows="3" <?php echo $disable?> class="resizable_textarea form-control" placeholder="Alamat" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                      </div>
                    </div>                      
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-12 col-sm-6">
              <div style="font-size:12px">
                
<?php if($disable==''){?>
               
<?php } ?>
<div class="col-md-12">   
  <div class="x_title">
    <h4><i class="fa fa-desktop"></i> Daftar Pajak Reklame</h4>
    <div class="clearfix"></div>
  </div>
       <div id="templist">
         
       </div> 
       <div id="daftarreklame">
         
       </div>                                
</div>
<div class="col-md-6">   
  <p></p>                
  <?php if($STATUS=='2'){?>
  <div class="form-group">
    <button type="button" id="<?php echo $ID_INC;?>" class="btn btn-warning trash togglee"><i class="fa fa-edit"></i> TETAPKAN</button>
      <div class="badge" id="peringatan" style="visibility: hidden"><p> Berhasil ditetapkan</p></div>
    <a href="javascript:history.back()" id='vis' style="visibility: hidden" class="btn btn-primary"><i class="fa fa-angle-double-left"> Kembali</i></a>
  </div>
  <?php }else {?> <div class="badge"><p> Sudah ditetapkan</p></div>
                           <a href="javascript:history.back()" class="btn btn-primary"><i class="fa fa-angle-double-left"> Kembali</i></a><?php }?>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</form>
</div>



</div>

<script>    
function listreklame(){
    $('#templist').load("<?php echo base_url().'sptpdreklame/Reklame2/templist'?>"); 
  }   
function daftarreklame(){
    $('#daftarreklame').load("<?php echo base_url().'sptpdreklame/Reklame2/daftarreklame'?>"); 
  }
  /*TAMBAH PAJAK REKLAME*/
  $(document).ready(function(){
<?php if($disable==''){?>
   listreklame(); /* it will load products when document loads */
   <?php } ?>
  <?php if($update==''){?>
    daftarreklame();
  <?php } ?>
   $(document).on('click', '#tambah', function(e){
    var kec=document.getElementById("KECAMATAN").value;
    var kel=document.getElementById("KELURAHAN").value;
    var gol=document.getElementById("GOLONGAN").value;
    var lok=document.getElementById("LOKASI_PASANG").value;
    var teks=document.getElementById("TEKS").value;
    var p=document.getElementById("P").value;
    var l=document.getElementById("L").value;
    var s=document.getElementById("S").value;
    var Jumlah=document.getElementById("JUMLAH").value;
    var masa=document.getElementById("MASA").value;
    if (kec==null || kec==""){
      swal("Kecamatan Harus Diisi", "", "warning")
      return false;
    } else if(kel==null || kel==""){
      swal("Kelurahan Harus Diisi", "", "warning")
      return false;
    }  else if(gol==null || gol==""){
      swal("Golongan Harus Diisi", "", "warning")
      return false;
    }  else if(lok==null || lok==""){
      swal("Lokasi Pasang Harus Diisi", "", "warning")
      return false;
    } else if(teks==null || teks==""){
      swal("Teks Harus Diisi", "", "warning")
      return false;
    } else if(p==null || p==""){
      swal("P Harus Diisi", "", "warning")
      return false;
    } else if(l==null || l==""){
      swal("L Harus Diisi", "", "warning")
      return false;
    } else if(s==null || s==""){
      swal("S Harus Diisi", "", "warning")
      return false;
    } else if(Jumlah==null || Jumlah==""){
      swal("Jumlah Harus Diisi", "", "warning")
      return false;
    } else if(masa==null || masa==""){
      swal("Masa Harus Diisi", "", "warning")
      return false;
    } else {
      tambah();
    };
  });

 });
  function tambah(KECAMATAN,BERLAKU_MULAI){
    var data = $('#demo-form2').serialize();
    $.ajax({
     type: "POST",
     url: "<?php echo base_url().'sptpdreklame/Reklame2/tambah'?>",
     data: data
        }).done(function(response){
            swal('Data Ditambah', response.message, response.status);
          listreklame();
           })
           .fail(function(){
            swal('Oops...', 'Something went wrong with ajax !', 'error');
           });; 
     KELURAHAN.value='';
     GOLONGAN.value='';
     LOKASI_PASANG.value='';
     TEKS.value='';
     P.value='';
     L.value='';
     S.value='';
     JUMLAH.value='';
     MASA.value='';
     PAJAK_TERUTANG.value='';  
     $('#ROKOK').prop('checked', false);
     $('#KETINGGIAN').prop('checked', false);    
  }

  /*validasi data reklame*/
function valrek(){
    var kec=document.getElementById("KECAMATAN").value;
    if (kec==null || kec==""){
      swal("Kecamatan Harus Diisi", "", "warning")
      return false;
    };
}  
  /*HITUNG PAJAK TERUTANG*/
  function get(){
    var njop=parseFloat(nominalFormat(document.getElementById("NJOP").value));
    var tarif=parseFloat(nominalFormat(document.getElementById("TARIF").value));
    var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
    var P=parseFloat(nominalFormat(document.getElementById("P").value))||0;
    var L=parseFloat(nominalFormat(document.getElementById("L").value))||0;
    var S=parseFloat(nominalFormat(document.getElementById("S").value))||0;
    var MASA=parseFloat(nominalFormat(document.getElementById("MASA").value))||0;
    var JUMLAH=parseFloat(nominalFormat(document.getElementById("JUMLAH").value))||0;
    var dpp= (njop+tarif)*persen*P*L*S*MASA*JUMLAH;
    DPP.value=getNominal(dpp)  ;
    PAJAK_TERUTANG.value=getNominal(dpp)  ;
  }
  function validate() {
    if ((document.getElementById('ROKOK').checked)&&(document.getElementById('KETINGGIAN').checked)) {
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var pjt=dpp+(dpp*persen)+(dpp*persen);
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else if((document.getElementById('ROKOK').checked)||(document.getElementById('KETINGGIAN').checked)){
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var pjt=dpp+(dpp*persen);
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else if(document.getElementById('ROKOK').checked){
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var pjt=dpp+(dpp*persen);
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else if(document.getElementById('KETINGGIAN').checked){
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var pjt=dpp+(dpp*persen);
      PAJAK_TERUTANG.value=getNominal(pjt);
    } else {

      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      PAJAK_TERUTANG.value=getNominal(dpp);
    }
  }

<?php if($disable==''){?>
  document.getElementById('ROKOK').addEventListener('change', validate);
  document.getElementById('KETINGGIAN').addEventListener('change', validate);
            /* P */
          var P = document.getElementById('P');
          P.addEventListener('keyup', function(e)
          {
            P.value = formatRupiah(this.value);
          }); 
                    /* L */
          var L = document.getElementById('L');
          L.addEventListener('keyup', function(e)
          {
            L.value = formatRupiah(this.value);
          }); 
                    /* S */
          var S = document.getElementById('S');
          S.addEventListener('keyup', function(e)
          {
            S.value = formatRupiah(this.value);
          });           /* JUMLAH */
          var JUMLAH = document.getElementById('JUMLAH');
          JUMLAH.addEventListener('keyup', function(e)
          {
            JUMLAH.value = formatRupiah(this.value);
          }); 
                    /* MASA */
          var MASA = document.getElementById('MASA');
          MASA.addEventListener('keyup', function(e)
          {
            MASA.value = formatRupiah(this.value);
          }); 
  <?php } ?>
  function gantiTitikKoma(angka){
    return angka.toString().replace(/\./g,',');
  }
  function nominalFormat(angka){
    return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
  }
  function getNominal(angka){
    var nominal=gantiTitikKoma(angka);
    var indexKoma=nominal.indexOf(',');
    if (indexKoma==-1) {
      return ribuan(angka);
    } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
    }
  }
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa  = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
  function ribuan (angka)
  {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }      


  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
  }


  $("#NPWPD").keyup(function(){
    var npwpd = $("#NPWPD").val();
    $.ajax({
      url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
      type: 'POST',
      data: "npwpd="+npwpd,
      cache: false,
      success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }

            }
          });
  });

  function getkel(val)
  {
    
    var kode = val;
    alert(kode);
    $.ajax({
     type: "POST",
     url: "<?php echo base_url().'sptpdreklame/Reklame2/getkel'?>",
     data: { kode: kode},
     cache: false,
     success: function(msga){
            //alert(data);
            $("#KELURAHAN").html(msga);
          }
        });    
  }  
</script>
<script type="text/javascript">
  $(function(){
        $('.trash').click(function(){
            var del_id= $(this).attr('id');
            var $ele = $(this).parent().parent();
            var mp=document.getElementById("MP").innerHTML = "<?php echo $NB?>";
             var tp=document.getElementById("TP").innerHTML = "<?php echo $TAHUN_PAJAK?>";
             var fo="Data Berhasil Ditetapkan untuk ";
           //alert(del_id);
            $.ajax({
                  type:'POST',
                  url:'<?php echo base_url('sptpdreklame/Reklame2/proses_penetapan'); ?>',
                  data:{del_id:del_id},
                  success: function(data){
                     alert(fo+" Masa Pajak "+mp+" Tahun Pajak "+tp);                   
                              document.getElementById(del_id).style.visibility = 'hidden';
                              document.getElementById('vis').style.visibility = 'visible';
                              document.getElementById('peringatan').style.visibility = 'visible';
                        
                  }

                   })
              })
    });
</script>

