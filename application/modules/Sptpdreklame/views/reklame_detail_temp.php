  <div style="overflow-x:auto;">
  <table id="example" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th >No</th>
        <th>Berlaku Mulai</th>
        <th>Kecamatan</th>
        <th>Kelurahan</th>
        <th>Golongan</th>
        <th>Lokasi Pasang</th>
        <th>Teks</th>
        <th>P</th>
        <th>L</th>
        <th>S</th>
        <th>Jumlah</th>
        <th>Masa</th>
        <th>Rokok</th>
        <th>Ketinggian</th>
        <th>Pajak Terutang</th>
        <th >Aksi</th>
      </tr>
    </thead>
  </table>
</div>
<?php 
$id=$this->session->userdata('NIP');
$row=$this->db->query("SELECT SUM(PAJAK_TERUTANG) AS TOTAL FROM SPTPD_REKLAME_DETAIL_TEMP WHERE NIP='$id' GROUP BY  NIP")->row(); ?>
<div class="form-group">
  <label class="control-label col-md-3 col-xs-12 col-md-offset-7">Jumlah Total Pajak Terutang </label>
  <div class="col-md-2 ">
    <div class="input-group">
      <span class="input-group-addon">Rp</span>
      <input readonly="" type="text" class="form-control col-md-2 col-xs-12 " style="text-align:right;" value="<?php if(isset($row->TOTAL)) {echo number_format($row->TOTAL,'1',',','.');} else {echo "0";} ?>" >
    </div>
  </div>
</div>
<script type="text/javascript">

    $(document).on('click', '#deletetemp', function(e){
      
      var id = $(this).data('id');
      deltemp(id);
      e.preventDefault();
    });
  function deltemp(id){
    
    swal({
      title: 'Apakah Anda Yakin?',
      text: "Data Akan dihapus Permanen!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!',
      showLoaderOnConfirm: true,
        
      preConfirm: function() {
        return new Promise(function(resolve) {
             
           $.ajax({
            url: '<?php echo base_url()?>sptpdreklame/Reklame2/hapustemp',
            type: 'POST',
              data: 'delete='+id,
              dataType: 'json'
           })
           .done(function(response){
            swal('Deleted!', response.message, response.status);
          listreklame();
           })
           .fail(function(){
            swal('Oops...', 'Something went wrong with ajax !', 'error');
           });
        });
        },
      allowOutsideClick: false        
    }); 
    
  }

  $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };
    $.fn.dataTable.ext.errMode = 'throw';
    var t = $("#example").dataTable({
      initComplete: function() {
        var api = this.api();
        $('#mytable_filter input')
        .off('.DT')
        .on('keyup.DT', function(e) {
          if (e.keyCode == 13) {
            api.search(this.value).draw();
          }
        });
      },



      'oLanguage':
      {
        "sProcessing":   "Sedang memproses...",
        "sLengthMenu":   "Tampilkan _MENU_ entri",
        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
        "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix":  "",
        "sSearch":       "Cari:",
        "sUrl":          "",
        "oPaginate": {
          "sFirst":    "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext":     "Selanjutnya",
          "sLast":     "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      ajax: {"url": "<?php echo base_url()?>sptpdreklame/Reklame2/jsonreklame_detail_temp", "type": "POST"},
      columns: [
      {
        "data":"ID_INC",
        "orderable": false,
        "className" : "text-center"
      },
      {"data":"BERLAKU_MULAI"},
      {"data":"NAMAKEC"},
      {"data":"NAMAKELURAHAN"},
      {"data":"DESKRIPSI"},
      {"data":"LOKASI_PASANG"},
      {"data":"TEKS"},
      {"data":"P"},
      {"data":"L"},
      {"data":"S"},
      {"data":"JUMLAH"},
      {"data":"WAKTU"},
      {"data":"ROKOK"},
      {"data":"KETINGGIAN"},
      {
        "data":"PAJAK_TERUTANG",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      {"data":"action"},
      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>