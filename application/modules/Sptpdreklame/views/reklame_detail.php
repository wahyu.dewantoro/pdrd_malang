 <br>
 <div style="overflow-x:auto;">
  <table id="example2" class="table table-striped table-bordered" width="100%">
    <thead>
      <tr>
        <th class="text-center" width="3%">No</th>
        <th class="text-center">Berlaku Mulai</th>
        <th class="text-center">Kecamatan</th>
        <th class="text-center">Kelurahan</th>
        <th class="text-center" width="30%">Golongan</th>
        <th class="text-center">Lokasi Pasang</th>
        <th class="text-center">Teks</th>
        <th class="text-center">P</th>
        <th class="text-center">L</th>
        <th class="text-center">S</th>
        <th class="text-center">Jumlah</th>
        <th class="text-center">Masa</th>
        <th class="text-center">Rokok</th>
        <th class="text-center">Ketinggian</th>
        <th class="text-center">Pajak Terutang</th>
        <?php if($this->session->userdata('status')=='1') {?>
        <th class="text-center">Aksi</th>
        <?php } ?>
      </tr>
    </thead>
  </table>
</div>
<?php 
$id=$this->session->userdata('sptpdreklame_id');
$row=$this->db->query("SELECT SUM(PAJAK_TERUTANG) AS TOTAL FROM SPTPD_REKLAME_DETAIL WHERE SPTPD_REKLAME_ID='$id' GROUP BY  SPTPD_REKLAME_ID")->row(); ?>
<div class="form-group">
  <label class="control-label col-md-3 col-xs-12 col-md-offset-7">Jumlah Total Pajak Terutang </label>
  <div class="col-md-2 "><?php error_reporting(E_ALL^(E_NOTICE|E_WARNING));?>
    <div class="input-group">
      <span class="input-group-addon">Rp</span>
      <input readonly="" type="text" class="form-control col-md-2 col-xs-12 " style="text-align:right;" value="<?php echo number_format($row->TOTAL,'1',',','.') ?>" >
    </div>
  </div>
</div>
<script type="text/javascript">

    $(document).on('click', '#delete', function(e){
      
      var id = $(this).data('id');
      del(id);
      e.preventDefault();
    });
  function del(id){
    
    swal({
      title: 'Apakah Anda Yakin?',
      text: "Data Akan dihapus Permanen!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!',
      showLoaderOnConfirm: true,
        
      preConfirm: function() {
        return new Promise(function(resolve) {
             
           $.ajax({
            url: '<?php echo base_url()?>sptpdreklame/Reklame2/hapusdetail',
            type: 'POST',
              data: 'delete='+id,
              dataType: 'json'
           })
           .done(function(response){
            swal('Deleted!', response.message, response.status);
          daftarreklame();
           })
           .fail(function(){
            swal('Oops...', 'Something went wrong with ajax !', 'error');
           });
        });
        },
      allowOutsideClick: false        
    }); 
    
  }  
  $(document).ready(function() {

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };
    $.fn.dataTable.ext.errMode = 'throw';
    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        $('#mytable_filter input')
        .off('.DT')
        .on('keyup.DT', function(e) {
          if (e.keyCode == 13) {
            api.search(this.value).draw();
          }
        });
      },



      'oLanguage':
      {
        "sProcessing":   "Sedang memproses...",
        "sLengthMenu":   "Tampilkan _MENU_ entri",
        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
        "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix":  "",
        "sSearch":       "Cari:",
        "sUrl":          "",
        "oPaginate": {
          "sFirst":    "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext":     "Selanjutnya",
          "sLast":     "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      ajax: {"url": "<?php echo base_url()?>sptpdreklame/Reklame2/jsonreklame_detail", "type": "POST"},
      columns: [
      {
        "data":"ID_INC",
        "orderable": false,
        "className" : "text-center"
      },
      {"data":"BERLAKU_MULAI"},
      {"data":"NAMAKEC"},
      {"data":"NAMAKELURAHAN"},
      {"data":"DESKRIPSI","width": "200px",},
      {"data":"LOKASI_PASANG"},
      {"data":"TEKS"},
      {
        "data":"P",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      {
        "data":"L",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      {
        "data":"S",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      {
        "data":"JUMLAH",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 0, '' )
      },
      {"data":"WAKTU"},  
      {"data":"ROKOK",
      "className" : "text-center",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '1' : return "YA"; break;
          default  : return 'TIDAK';
       }}},
       {"data":"KETINGGIAN",
       "className" : "text-center",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '1' : return "YA"; break;
          default  : return 'TIDAK';
       }}}, 
      /*{"data":"JASBONG",
      "render": function ( data, type, row, meta ) {
        switch(data) {
          case '0' : return "YA"; break;
          default  : return 'TIDAK';
       }}},*/
      {
        "data":"PAJAK_TERUTANG",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
      },
      <?php if($this->session->userdata('status')=='1') {?>
      {"data":"action"},
      <?php } ?>
      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>