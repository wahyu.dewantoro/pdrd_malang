   <?php $namabulan=array(
    '',
    'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
    ) ?>

   <style>
   th { font-size: 10px; }
   td { font-size: 10px; }
   label { font-size: 11px;}
   textarea { font-size: 11px;}
   .input-group span{ font-size: 11px;}
   input[type='text'] { font-size: 11px; height:30px}
 </style>
 <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>
  <div class="pull-right">
<a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >  
          <div class="row">
            <div class="col-md-12 col-sm-6">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak </h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" />
                    <?php if($disable=='' AND $param!=''){?>                                 
                      <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pilihan
                        </label>
                        <div class="col-md-9">
                          <a href="<?php echo site_url('sptpdreklame/Reklame_bulan_lalu') ?>" class="btn btn-danger"><i class="fa fa-edit"></i> Gunakan Data Bulan Lalu</a>
                        </div>
                      </div>  -->
                      <?php } ?>   
                   
                         <input type="hidden" <?php echo $disable?>readonly name="MASA_PAJAK" required class="form-control" value="<?php if(!empty($MASA_PAJAK)){echo $MASA_PAJAK; } else { echo date("n");}?>">
                        <input type="hidden" <?php echo $disable?>readonly id="TAHUN_PAJAK" name="TAHUN_PAJAK" required class="form-control" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>">                
                      
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD  <sup>*</sup> 
                      </label>
                      <div class="col-md-9">
                        <input type="text" id="NPWPD" <?php echo $disable?> name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" onchange="NamaUsaha(this.value);">                  
                      </div>
                    </div>
                    <?php if ($disable=='disabled') { ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" id="NAMA_USAHA" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <option  value="<?php echo $NAMA_USAHA?>"><?php echo $NAMA_USAHA ?></option>
                        </select>
                        <!-- <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>> -->               
                      </div>
                    </div>  
<?php } else { ?>                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" id="NAMA_USAHA" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <?php foreach($jenis as $jns){ ?>
                          <option <?php if($jns->NAMA_USAHA==$NAMA_USAHA){echo "selected";}?> value="<?php echo $jns->NAMA_USAHA?>"><?php echo $jns->NAMA_USAHA ?></option>
                          <?php } ?> 
                        </select>
                        <!-- <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>> -->               
                      </div>
                    </div>
<?php } ?>
                    <input type="hidden" id="V_KODEKEC" name="KODEKEC" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="V_KODEKEL" name="KODEKEL" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >  
                    <?php if ($KODE_BILING!=null) {?>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" ><h4><b>KODE BILING</b></h4></label>
                          <div class="col-md-9 col-sm-6 col-xs-12">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" ><h4><b><?php echo $KODE_BILING?></b></h4></label>
                          </div>
                        </div> 
                    <?php } else {?>
                       <?php if ($not=='1') {
                      } else {?>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" ><h4><b>KODE BILING</b></h4></label>
                          <div class="col-md-9 col-sm-6 col-xs-12">
                            <label class="control-label col-md-6 col-sm-3 col-xs-12" for="last-name" ><h4><b>BELUM DITETAPKAN</b></h4></label>
                          </div>
                        </div> 
                        <?php }?>
                    <?php }?> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <textarea style="font-size: 11px" id="ALAMAT_USAHA" rows="3" <?php echo $disable?> class="resizable_textarea form-control" placeholder="Alamat" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                      </div>
                    </div> 
                  </div>
                  <div class="col-md-6">

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <input type="text" id="NAMA_WP" <?php echo $disable?> name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>">                
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Alamat WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <textarea style="font-size: 11px" id="ALAMAT_WP" rows="3" <?php echo $disable?> class="resizable_textarea form-control" placeholder="Alamat" name="ALAMAT_WP"><?php echo $ALAMAT_WP; ?></textarea>
                      </div>
                    </div> 
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <input type="text" id="NAMA_USAHA" <?php echo $disable?> name="NAMA_USAHA" required="required" placeholder="Nama Perusahaan" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>">                     
                      </div>
                    </div> -->                       
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-12 col-sm-6">
              <div style="font-size:12px">
                
<?php if($disable==''){?>
                <div class="x_title">
                  <h4><i class="fa fa-desktop"></i> Data Reklame</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <!-- konten -->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tanggal Penerimaan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" > Berlaku Mulai  <sup>*</sup> 
                      </label>
                      <div class="col-md-9">
                        <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="" required <?php echo $disable?> >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" > Kecamatan  <sup>*</sup> 
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel((this.value));"  disabled style="font-size:11px" id="KECAMATAN" <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                              <option value="">Pilih</option>
                              <?php foreach($kec as $kec){ ?>
                              <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC.'|'.$kec->KODE_UPT?>"><?php echo $kec->NAMAKEC ?></option>
                              <?php } ?>
                        </select>
                      </div>
                    </div>  
                  </div>
                <?php
                    $tarif = "var tarif = new Array();\n";
                    $njop = "var njop = new Array();\n";
                    $persen = "var persen = new Array();\n";
                    $masa = "var masa = new Array();\n";
                    $jenis_reklame = "var jenis_reklame = new Array();\n";
                    $jw = "var jw = new Array();\n";
                    ?>  

                <table class="table table-striped table-bordered" border="1">
                    <thead>
                      <tr>
                        <th>Kelurahan</th>            
                        <th width="160">Golongan</th>
                        <th>Lokasi Pasang</th>
                        <th>Teks</th>
                        <th width="45">P</th>
                        <th width="45">L</th>
                        <th width="45">S</th>
                        <th width="68">Jumlah</th>
                        <th width="50">Masa</th>
                        <th>Rokok</th>
                        <th>Ketinggian</th>
                        <th>Jasbong</th>
                        <th>Pajak Terutang</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><select  style="font-size:11px" disabled id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                                <option value="">Pilih</option>
                            </select></td>
                        <td><select disabled onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN" <?php echo $disable?> name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                                  <option value="">Pilih</option>
                                  <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                                  <option <?php if($GOLONGAN->ID_GOL==$GOLONGAN){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                                  <?php 
                                  $tarif .= "tarif['" . $GOLONGAN->ID_OP . "'] = {tarif:'".addslashes(number_format($GOLONGAN->TARIF,'0','','.'))."'};\n";
                                  $njop .= "njop['" . $GOLONGAN->ID_OP . "'] = {njop:'".addslashes(number_format($GOLONGAN->NJOP,'0','','.'))."'};\n";
                                  $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                                  $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                                  $jw .= "jw['" . $GOLONGAN->ID_OP . "'] = {jw:'".addslashes($GOLONGAN->JENIS_WAKTU)."'};\n";
                                  $jenis_reklame .= "jenis_reklame['" . $GOLONGAN->ID_OP . "'] = {jenis_reklame:'".addslashes(number_format($GOLONGAN->JENIS_REKLAME,'0','','.'))."'};\n";
                                } ?>
                              </select></td>
                        <td><script type="text/javascript">  
                                  <?php echo $tarif; echo $njop;echo $persen;echo $masa;echo $jenis_reklame;echo $jw;?>             
                                  function changeValue(id){
                                    document.getElementById('TARIF').value = tarif[id].tarif;
                                    document.getElementById('NJOP').value = njop[id].njop;
                                    document.getElementById('PERSEN').value = persen[id].persen;
                                    document.getElementById('MASA').value = masa[id].masa;
                                    document.getElementById('JNS').value = jenis_reklame[id].jenis_reklame;
                                    document.getElementById('JENIS_WAKTU').value = jw[id].jw;
                                    document.getElementById('P').value = '';
                                    document.getElementById('L').value = '';
                                    document.getElementById('S').value = '';
                                    document.getElementById('JUMLAH').value = '';
                                    document.getElementById('PAJAK_TERUTANG').value = '';
                                  }
                            </script> 
                          <input type="text" name="LOKASI_PASANG" disabled id="LOKASI_PASANG" class="form-control col-md-7 col-xs-12" value=""  <?php echo $disable?> placeholder="Lokasi Pasang">
                          <input type="hidden" id="TARIF" name="NILAI_STRATEGIS" value="" />   
                          <input type="hidden" id="NJOP"  name="NJOP" value="" />
                          <input type="hidden" id="JENIS_WAKTU"   name="JENIS_WAKTU" value="" />
                          <input type="hidden" id="PERSEN"  value="" />
                          <input type="hidden" id="DPP"  value="" />
                          <input type="hidden" id="JNS"  value="" />
                          <input type="hidden" id="NIP" name="NIP" value="<?php echo $NIP ?>" />
                          </td>
                        <td><input type="text" name="TEKS" disabled id="TEKS" class="form-control col-md-7 col-xs-12" value=""  <?php echo $disable?> placeholder="Teks"></td>
                        <td><input type="text" name="P" disabled id="P" class="form-control col-md-7 col-xs-12" value=""  <?php echo $disable?> placeholder="P" onchange='get(this);'></td>
                        <td><input type="text" name="L" disabled id="L" class="form-control col-md-7 col-xs-12" value=""  <?php echo $disable?> placeholder="L" onchange='get(this);'></td>
                        <td><input type="text" name="S" disabled id="S" class="form-control col-md-7 col-xs-12" value=""  <?php echo $disable?> placeholder="S" onchange='get(this);'></td>
                        <td><input type="text" name="JUMLAH" disabled id="JUMLAH" class="form-control col-md-7 col-xs-12" value=""  <?php echo $disable?> placeholder="Jumlah" onchange='get(this);'></td>
                        <td><input type="text" name="MASA" disabled id="MASA" class="form-control col-md-7 col-xs-12" value=""  <?php echo $disable?> placeholder="Masa" onchange='get(this);'></td>
                        <td><input type="checkbox" value="1" disabled id="ROKOK" name="ROKOK"> </td>
                        <td><input type="checkbox" value="1" disabled id="KETINGGIAN" name="KETINGGIAN"></td>
                        <td><input type="checkbox" value="1" disabled id="JASBONG" name="JASBONG"></td>
                        <td><input type="text" name="PAJAK_TERUTANG" readonly id="PAJAK_TERUTANG" onkeydown="return numbersonly(this, event);"  onkeyup="javascript:tandaPemisahTitik(this);" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG?>"  <?php echo $disable?> ></td>
                      </tr>
                    </tbody>
                </table>                   
                  <div class="col-md-6">   
                    
                  </div>
</div>
<div class="col-md-6">   
 
<div class="form-group">
  <div class="col-md-9 col-sm-6 col-xs-12 col-md-offset-3">
   <a class="btn btn-xs btn-success" id="tambah" ><i class="fa fa-plus"></i> Tambah</a>
 </div>
</div>                    
</div>
<?php } ?>
<div class="col-md-12">   
  <div class="x_title">
    <h4><i class="fa fa-desktop"></i> Daftar Pajak Reklame</h4>
    <div class="clearfix"></div>
  </div>
       <div id="templist">
         
       </div> 
       <div id="daftarreklame">
         
       </div>                                
</div>
<div class="col-md-6">   
  <p></p>                    
  <?php if($disable==''){?>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
      <button type="submit" class="btn btn-primary" onClick="return validasi()"><i class="fa fa-save"></i> Simpan</button>
      <a href="<?php echo site_url('sptpdair/air') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
    </div>
  </div>
  <?php } ?>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</form>
</div>



</div>

<script>    
function listreklame(){
    $('#templist').load("<?php echo base_url().'sptpdreklame/Reklame2/templist'?>"); 
  }   
function daftarreklame(){
    $('#daftarreklame').load("<?php echo base_url().'sptpdreklame/Reklame2/daftarreklame'?>"); 
  }
  /*TAMBAH PAJAK REKLAME*/
  $(document).ready(function(){
<?php if($disable==''){?>
   listreklame(); /* it will load products when document loads */
   <?php } ?>
  <?php if($update==''){?>
    daftarreklame();
  <?php } ?>
   $(document).on('click', '#tambah', function(e){
    var mulai=document.getElementById("BERLAKU_MULAI").value;
    var kec=document.getElementById("KECAMATAN").value;
    var kel=document.getElementById("KELURAHAN").value;
    var gol=document.getElementById("GOLONGAN").value;
    var lok=document.getElementById("LOKASI_PASANG").value;
    var teks=document.getElementById("TEKS").value;
    var p=document.getElementById("P").value;
    var l=document.getElementById("L").value;
    var s=document.getElementById("S").value;
    var Jumlah=document.getElementById("JUMLAH").value;
    var masa=document.getElementById("MASA").value;
    if (kec==null || kec==""){
      swal("Kecamatan Harus Diisi", "", "warning")
      return false;
    } else if(mulai==null || mulai==""){
      swal("Belaku Mulai Harus Diisi", "", "warning")
      return false;
    }else if(kel==null || kel==""){
      swal("Kelurahan Harus Diisi", "", "warning")
      return false;
    }  else if(gol==null || gol==""){
      swal("Golongan Harus Diisi", "", "warning")
      return false;
    }  else if(lok==null || lok==""){
      swal("Lokasi Pasang Harus Diisi", "", "warning")
      return false;
    } else if(teks==null || teks==""){
      swal("Teks Harus Diisi", "", "warning")
      return false;
    } else if(p==null || p==""){
      swal("P Harus Diisi", "", "warning")
      return false;
    } else if(l==null || l==""){
      swal("L Harus Diisi", "", "warning")
      return false;
    } else if(s==null || s==""){
      swal("S Harus Diisi", "", "warning")
      return false;
    } else if(Jumlah==null || Jumlah==""){
      swal("Jumlah Harus Diisi", "", "warning")
      return false;
    } else if(masa==null || masa==""){
      swal("Masa Harus Diisi", "", "warning")
      return false;
    } else {
      tambah();
    };
  });

 });
  function tambah(KECAMATAN,BERLAKU_MULAI){
    var data = $('#demo-form2').serialize();
    $.ajax({
     type: "POST",
     url: "<?php echo base_url().'sptpdreklame/Reklame2/tambah'?>",
     data: data
        }).done(function(response){
            swal('Data Ditambah', response.message, response.status);
          listreklame();
           })
           .fail(function(){
            swal('Oops...', 'Isikan data dengan Benar hapus penggunaan tanda petik satu', 'error');
           });; 
     KELURAHAN.value='';
     GOLONGAN.value='';
     //LOKASI_PASANG.value='';
     //TEKS.value='';
     P.value='';
     L.value='';
     S.value='';
     JUMLAH.value='';
     MASA.value='';
     PAJAK_TERUTANG.value='';  
     $('#ROKOK').prop('checked', false);
     $('#KETINGGIAN').prop('checked', false);  
     $('#JASBONG').prop('checked', false);    
  }

  /*validasi data reklame*/
function valrek(){
    var kec=document.getElementById("KECAMATAN").value;
    if (kec==null || kec==""){
      swal("Kecamatan Harus Diisi", "", "warning")
      return false;
    };
}  
  /*HITUNG PAJAK TERUTANG*/
  function get(){
    var jw=parseFloat(nominalFormat(document.getElementById("JENIS_WAKTU").value))||0;
    var njop=parseFloat(nominalFormat(document.getElementById("NJOP").value));
    var tarif=parseFloat(nominalFormat(document.getElementById("TARIF").value));
    var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
    var P=parseFloat(nominalFormat(document.getElementById("P").value))||0;
    var L=parseFloat(nominalFormat(document.getElementById("L").value))||0;
    var S=parseFloat(nominalFormat(document.getElementById("S").value))||0;
    var MASA=parseFloat(nominalFormat(document.getElementById("MASA").value))||0;
    var JUMLAH=parseFloat(nominalFormat(document.getElementById("JUMLAH").value))||0;
    var JNS=parseFloat(nominalFormat(document.getElementById("JNS").value));
    var dpp= (njop+tarif)*persen*P*L*S*MASA*JUMLAH;
    DPP.value=getNominal(dpp)  ;
    PAJAK_TERUTANG.value=getNominal(Math.round(dpp))  ;
    //alert(JNS);
  }
  function validate() {
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value))/100;
      var jr=parseFloat(nominalFormat(document.getElementById("JNS").value))
     // alert(jr);
    if (document.getElementById('ROKOK').checked) {//jika rokok cek         
          if (document.getElementById('KETINGGIAN').checked) {
               var prt=(dpp*persen)+(dpp*persen);
          } else{
               var prt=dpp*persen;
          };
          if (document.getElementById('JASBONG').checked) {
              if (jr==1) {
                var a=125/100;
                var b=25/100;
                var c=10/100;
                var d=50/100;
                var pjs=dpp/a/b*c;               
              } else{
                var a=125/100;
                var b=25/100;
                var c=10/100;
                var d=50/100;
                var pjs=dpp/a*d;
              };
          } else{
              var pjs=0;
          };
      PAJAK_TERUTANG.value=getNominal(Math.round(dpp+prt+pjs));
    } else if(document.getElementById('KETINGGIAN').checked){//jika ketinggian cek
              if (document.getElementById('JASBONG').checked) {
                    if (jr==1) {
                        var a=125/100;
                        var b=25/100;
                        var c=10/100;
                        var d=50/100;
                        var pjs=dpp/b*c;               
                    } else{
                        var a=125/100;
                        var b=25/100;
                        var c=10/100;
                        var d=50/100;
                        var pjs=dpp*d;
                    };
                  var pjk=(dpp*persen)+pjs;
                } else{
                  var pjk=dpp*persen;
                };
          //var pjk=pkt;
      PAJAK_TERUTANG.value=getNominal(Math.round(dpp+pjk));
    } else if(document.getElementById('JASBONG').checked){//jika jabong cek
     // alert(jr);
          if (jr==1) {
                var a=125/100;
                var b=25/100;
                var c=10/100;
                var d=50/100;
                var pjk=dpp/b*c;               
            } else{
                var a=125/100;
                var b=25/100;
                var c=10/100;
                var d=50/100;
                var pjk=dpp*d;
            };
      var pjt=dpp+pjk;
      PAJAK_TERUTANG.value=getNominal(Math.round(pjt));
    }
    else {
      var dpp=parseFloat(nominalFormat(document.getElementById("DPP").value));
      PAJAK_TERUTANG.value=getNominal(Math.round(dpp));
    }
   
  }

<?php if($disable==''){?>
  document.getElementById('ROKOK').addEventListener('change', validate);
  document.getElementById('KETINGGIAN').addEventListener('change', validate);
  document.getElementById('JASBONG').addEventListener('change', validate);
            /* P */
          var P = document.getElementById('P');
          P.addEventListener('keyup', function(e)
          {
            P.value = formatRupiah(this.value);
          }); 
                    /* L */
          var L = document.getElementById('L');
          L.addEventListener('keyup', function(e)
          {
            L.value = formatRupiah(this.value);
          }); 
                    /* S */
          var S = document.getElementById('S');
          S.addEventListener('keyup', function(e)
          {
            S.value = formatRupiah(this.value);
          });           /* JUMLAH */
          var JUMLAH = document.getElementById('JUMLAH');
          JUMLAH.addEventListener('keyup', function(e)
          {
            JUMLAH.value = formatRupiah(this.value);
          }); 
                    /* MASA */
          var MASA = document.getElementById('MASA');
          MASA.addEventListener('keyup', function(e)
          {
            MASA.value = formatRupiah(this.value);
          }); 
  <?php } ?>
  function gantiTitikKoma(angka){
    return angka.toString().replace(/\./g,',');
  }
  function nominalFormat(angka){
    return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
  }
  function getNominal(angka){
    var nominal=gantiTitikKoma(angka);
    var indexKoma=nominal.indexOf(',');
    if (indexKoma==-1) {
      return ribuan(angka);
    } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
    }
  }
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa  = split[0].length % 3,
    rupiah  = split[0].substr(0, sisa),
    ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
  function ribuan (angka)
  {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }      


  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var usaha=document.forms["demo-form2"]["NAMA_USAHA"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
    if (usaha==null || usaha==""){
      swal("NAMA USAHA Harus di Isi", "", "warning")
      return false;
    };
  }


  function NamaUsaha(id){
                //alert(id);
                var npwpd=id;
                var jp='04';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              //alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
        
        $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_nama_usaha'?>",
           data: { npwp: npwpd,
                   jp: jp   },
           cache: false,
           success: function(msga){
                  //alert(jp);
                  $("#NAMA_USAHA").html(msga);
                }
              }); 
                
    } 
<?php if ($update=='0') {?>
    function alamat(sel)
    {
      if (sel=="") {
        $("#ALAMAT_USAHA").val("");
        document.getElementById("KECAMATAN").disabled=true;
        document.getElementById("KELURAHAN").disabled=true;
        document.getElementById("GOLONGAN").disabled=true;
        document.getElementById("LOKASI_PASANG").disabled=true;
        document.getElementById("TEKS").disabled=true;
        document.getElementById("P").disabled=true;
        document.getElementById("L").disabled=true;
        document.getElementById("S").disabled=true;
        document.getElementById("JUMLAH").disabled=true;
        document.getElementById("MASA").disabled=true;
        document.getElementById("ROKOK").disabled=true;
        document.getElementById("KETINGGIAN").disabled=true;
        document.getElementById("JASBONG").disabled=true;
        document.getElementById("S").disabled=true;

      } else {
        var nama = sel;
        document.getElementById("KECAMATAN").disabled=false;
        document.getElementById("KELURAHAN").disabled=false;
        document.getElementById("GOLONGAN").disabled=false;
        document.getElementById("LOKASI_PASANG").disabled=false;
        document.getElementById("TEKS").disabled=false;
        document.getElementById("P").disabled=false;
        document.getElementById("L").disabled=false;
        document.getElementById("S").disabled=false;
        document.getElementById("JUMLAH").disabled=false;
        document.getElementById("MASA").disabled=false;
        document.getElementById("ROKOK").disabled=false;
        document.getElementById("KETINGGIAN").disabled=false;
        document.getElementById("JASBONG").disabled=false;
        document.getElementById("S").disabled=false;
        //alert(nama);
        $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_alamat_usaha'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              //alert(msga);
              $("#ALAMAT_USAHA").val(msga); 
            }
          });
         $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_detail_usaha_reklame'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              if(msga!=0){
                //alert(nama);
                  var exp = msga.split("|");
                  $("#V_KODEKEL").val(exp[0]);
                  $("#V_KODEKEC").val(exp[2]);                  
                  
                 // alert(msga);
                }else{
                  
                }
            }
          });   
        } 
    }
  <?php
} else {?>
        document.getElementById("KECAMATAN").disabled=false;
        document.getElementById("KELURAHAN").disabled=false;
        document.getElementById("GOLONGAN").disabled=false;
        document.getElementById("LOKASI_PASANG").disabled=false;
        document.getElementById("TEKS").disabled=false;
        document.getElementById("P").disabled=false;
        document.getElementById("L").disabled=false;
        document.getElementById("S").disabled=false;
        document.getElementById("JUMLAH").disabled=false;
        document.getElementById("MASA").disabled=false;
        document.getElementById("ROKOK").disabled=false;
        document.getElementById("KETINGGIAN").disabled=false;
        document.getElementById("JASBONG").disabled=false;
        document.getElementById("S").disabled=false;
  <?php
}?>  

 function getkel(val)
  {
    
    var KODEKEC = val;
    //alert(KODEKEC);
    $.ajax({
     type: "POST",
      url: "<?php echo base_url().'sptpdreklame/Reklame2/getkel'?>",
       data: { KODEKEC: KODEKEC},
     cache: false,
     success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
  } 
</script>


