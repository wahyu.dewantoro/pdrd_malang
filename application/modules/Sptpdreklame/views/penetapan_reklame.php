   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
     <div class="title_left">
        <h3>SPOP Reklame</h3>
      </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
          <?php echo $this->session->flashdata('notif')?>
          
           <table id="example2" class="table table-striped table-bordered table-hover">
              <thead>
                  <tr>
                      <th  class="text-center" width="3%">No</th>
                      <th  class="text-center">NPWPD</th>
                      <th  class="text-center">Nama</th>
                      <th  class="text-center">Alamat</th>
                      <th  class="text-center">Nama Usaha</th>
                      <!-- <th  class="text-center">DPP</th> -->
                      <th  class="text-center">Total Pajak Terutang</th>
                      <th  class="text-center">User Entry</th>
                      <th  class="text-center">Aksi</th>
                      <!-- <th  class="text-center">Keterangan</th> -->
                  </tr>
              </thead>
           </table>
      </div>
    </div>
  </div>
</div>
<?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
 <script type="text/javascript">
          $(document).ready(function() {

                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };
                $.fn.dataTable.ext.errMode = 'throw';
                var t = $("#example2").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    
                        
                    
                    'oLanguage':
                    {
                      "sProcessing":   "Sedang memproses...",
                      "sLengthMenu":   "Tampilkan _MENU_ entri",
                      "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                      "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                      "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                      "sInfoPostFix":  "",
                      "sSearch":       "Cari:",
                      "sUrl":          "",
                      "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                      }
                    },
                    processing: true,
                    serverSide: true,
                    pageLength: 20,
                    ajax: {"url": "<?php echo base_url()?>sptpdreklame/Reklame2/jsonreklame_penetapan", "type": "POST"},
                    columns: [
                        {
                          "data":"ID_INC",
                          "orderable": false,
                          "className" : "text-center"
                        },
                       /* {"data":"MASA_PAJAK",
                          "render": function ( data, type, row, meta ) {
                            switch(data) {
                              case '1' : return "Januari"; break;
                              case '2' : return "Februari"; break;
                              case '3' : return "Maret"; break;
                              case '4' : return "April"; break;
                              case '5' : return "Mei"; break;
                              case '6' : return "Juni"; break;
                              case '7' : return "Juli"; break;
                              case '8' : return "Agustus"; break;
                              case '9' : return "September"; break;
                              case '10' : return "Oktober"; break;
                              case '11' : return "November"; break;
                              case '12' : return "Desember"; break;
                             default  : return 'N/A';
                           }}},*/
                        //"data":"TAHUN_PAJAK"},
                        {"data":"NPWPD"},
                        {"data":"NAMA_WP"},
                        {"data":"ALAMAT_WP"},
                        {"data":"NAMA_USAHA"},
                        {
                          "data":"TOTAL",
                          "className" : "text-right",
                          "render": $.fn.dataTable.render.number( '.', ',', 1, '' )
                        },
                        {"data":"USER_INSERT"},
                  {
                   "data" : "STATUS",
                   "orderable": false,
                   render: function ( data, type, row, meta ) {
                switch(data) {
                  case '1' : return row.actiona; break;
                  case '0' : return row.actiona; break;
                 default  : return row.actionb;
               }}
                 },
            /*     {
                   "data" : "STATUS",
                   "orderable": false,
                   render: function ( data, type, row, meta ) {
                switch(data) {
                  case '0' : return row.actionc; break;
                  case '1' : return row.actionc; break;
                  case '2' : return "Belum Ditetapkan"; break;
               }}
                 },*/
                        // {"data":"action",
                        //       render : function (data,type,row ) {
                        //           this.url='<?php echo base_url()?>';
                        //           var myvar='<?php echo $session_value;?>';
                        //           if (myvar==5){
                        //               if (row.STATUS==1) {
                        //                   return '<a href="'+this.url+'sptpdreklame/Reklame2/detail/'+row.ID_INC+'" class="btn btn-xs btn-info">Detail</a>';
                        //               }
                        //                 else{
                        //                   return '<a href="'+this.url+'sptpdreklame/Reklame2/form_penetapan/'+row.ID_INC+'" class="btn btn-xs btn-success">proses</a>';
                        //               }
                        //           }else{
                        //               if (row.STATUS==1) {
                        //                   return '<a href="'+this.url+'sptpdreklame/Reklame2/detail/'+row.ID_INC+'" class="btn btn-xs btn-info">Detail</a>';
                        //               }
                        //                 else{
                        //                   return '<a href="'+this.url+'sptpdreklame/Reklame2/detail/'+row.ID_INC+'" class="btn btn-xs btn-primary"><i class="fa fa-binoculars"></i></a><a href="'+this.url+'sptpdreklame/Reklame2/update/'+row.ID_INC+'" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a><a href="'+this.url+'sptpdreklame/Reklame2/delete/'+row.ID_INC+'" class="btn btn-xs btn-danger"  onClick="return confirm(\'Apakah anda yakin?\')"><i class="fa fa-trash"></i></a>';
                        //               }
                        //           }
                                  
                                
                        //       }
                        // }
                        /*{"data":"action",
                          "className" : "text-center"}*/
                    ],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>

