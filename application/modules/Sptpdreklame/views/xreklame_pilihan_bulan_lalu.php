   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3>Data SPTPD Reklame</h3>
</div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="col-md-12 col-sm-12">
          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo base_url().'sptpdreklame/Reklame_bulan_lalu/cari_data_bl';?>" method="post" enctype="multipart/form-data" >
          <div class="col-md-6">                         
              <div class="form-group">
                  <label class="control-label col-md-5 col-sm-3 col-xs-12" for="last-name" >Tahun Pajak <sup>*</sup>
                  </label>
                  <div class="col-md-6">
                    <select id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option><?php } ?>
                    </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="control-label col-md-5 col-sm-3 col-xs-12" for="last-name" >Masa Pajak Bulan Lalu<sup>*</sup>
                  </label>
                  <div class="col-md-6">
                      <select id="MASA_PAJAK" name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php /*if (date('m')-1==$mp-1) {echo "selected";} */?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                  </div>
              </div> 
              <div class="form-group">
                  <label class="control-label  col-md-5 col-sm-3 col-xs-12" for="last-name" >Golongan  
                  </label>
                  <div class="col-md-6">
                      <select id="MASA_PAJAK" name="GOLONGAN"  placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Golongan</option>

                          <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                          <option <?php if($GOLONGAN->ID_GOL==$this->session->userdata('rek_bulan')){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                          <?php 
                        } ?>                            
                            
                      </select>
                  </div>
              </div> 
              <div class="form-group">
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-9">
                     <?php echo $this->session->flashdata('notif');?>                   
                  </div>
              </div>     
          </div>
          <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                <button type="submit" class="btn btn-success" onClick="return validasi()"><i class="fa fa-search"></i> Cari</button>
                <a href="<?php echo site_url('sptpdreklame/Reklame2/create') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
              </div>
           </div>
          </form>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <form id="frm-example" action="<?php echo base_url().'sptpdair/Air_bl/entry_airtanah_masa_lalu';?>" method="POST">
                  <?php echo $this->session->flashdata('notif')?>
                  <table id="example2" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th width="3%">No</th>
                        <!-- <th>Masa</th>
                        <th>Tahun</th> -->
                        <th>NPWPD</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <!-- <th>DPP</th> -->
                        <th>Pajak Bulan Lalu</th>
                        <th>Aksi <INPUT type="checkbox" onchange="checkAll(this)" name="chk[]" /></th>
                        <th>STATUS</th>
                      </tr>
                    </thead>
                  </table>
                  <p><button>Submit</button></p>
            </form>      
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function validasi(){
  var npwpd=document.forms["demo-form2"]["NPWPD"].value;
  var masa=document.forms["demo-form2"]["MASA_PAJAK"].value;
  var number=/^[0-9]+$/; 
  if (masa==null || masa==""){
    swal("MASA PAJAK Harus di Isi", "", "warning")
    return false;
  };
  if (npwpd==null || npwpd==""){
    swal("NPWPD Harus di Isi", "", "warning")
    return false;
  };
}
</script>
<script type="text/javascript">

   $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#example2").dataTable({
                  "paging": false,
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    
                        
                    
                    'oLanguage':
                    {
                      "sProcessing":   "Sedang memproses...",
                      "sLengthMenu":   "Tampilkan _MENU_ entri",
                      "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                      "sInfo":         "Menampilkan _TOTAL_ entri",
                      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                      "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                      "sInfoPostFix":  "",
                      "sSearch":       "Cari Nama: ",
                      "sUrl":          "",
                      "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                      }
                    },
      processing: true,
      serverSide: true,
      ajax: {"url": "<?php echo base_url()?>sptpdair/Air_bl/jsonair_bl", "type": "POST"},
      columns: [
      {
        "data":"ID_INC",
        "orderable": false,
        "className" : "text-center"
      },
      
       {"data":"NPWPD"},
       {"data":"NAMA_WP"},
       {"data":"ALAMAT_WP"},
       /*{
        "data":"DPP",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },*/
      {
        "data":"PAJAK_TERUTANG",
        "className" : "text-right",
        "render": $.fn.dataTable.render.number( '.', '.', 0, '' )
      },
      {"data":"action",
      "className" : "text-center",
      render : function (data,type,row ) {
                                  this.url='<?php echo base_url()?>';
                                return '<input type="checkbox" name="id_inc[]" value="'+row.ID_INC+'">';
                              }
                        },
      {"data":"action",render : function (data,type,row ) {
                                  this.url='<?php echo base_url()?>';
                                return 'sdsd';
                              }
                        },
      ],
      rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                        $.fn.dataTable.ext.errMode = 'none';
      }
    });
  });
</script>
<script> 
function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
</script>


