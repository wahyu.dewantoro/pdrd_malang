<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mbayar extends CI_Model
{
  function __construct()
  {
    parent::__construct();
    $this->nip  = $this->session->userdata('NIP');
    // $this->role =$this->session->userdata('MS_ROLE_ID');
  }


  function getData()
  {
    return $this->db->get('SPTPD_PEMBAYARAN');
  }


  // datatables
  function json()
  {
    $date = date('d-m-Y');
    $tgl1 = $this->session->userdata('tgl1');
    $tgl2 = $this->session->userdata('tgl2');
    if ($this->session->userdata('tgl1') != '' and $this->session->userdata('tgl2') != '') {
      $wh = "TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY') AND TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')";
      // $wh ="TO_CHAR(DATE_BAYAR, 'mm')=TO_CHAR(SYSDATE, 'mm') ";
    } else {
      $wh = "TO_DATE(TO_CHAR(DATE_BAYAR,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('$date','DD-MM-YYYY')";
    }
    $this->datatables->select("ID_INC, KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR,PENGESAHAN,DATE_BAYAR, TGL_BAYAR");
    $this->datatables->from("(select ID_INC, KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR,PENGESAHAN,TGL_BAYAR DATE_BAYAR,to_char(TGL_BAYAR,'dd Mon YYYY , HH24: MI: SS') TGL_BAYAR from  SPTPD_PEMBAYARAN order by tgl_bayar desc)");
    $this->datatables->where($wh);
    return $this->datatables->generate();
  }
  function json_pembatalan()
  {
    $tgl1 = $this->session->userdata('tgl1');
    $tgl2 = $this->session->userdata('tgl2');
    if ($this->session->userdata('tgl1') != '' and $this->session->userdata('tgl2') != '') {
      $wh = "TO_DATE(TO_CHAR(TGL_PEMBATALAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY') AND TO_DATE(TO_CHAR(TGL_PEMBATALAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')";
      // $wh ="TO_CHAR(TGL_PEMBATALAN, 'mm')=TO_CHAR(SYSDATE, 'mm') ";
    } else {
      $wh = "TO_CHAR(TGL_PEMBATALAN, 'mm')=TO_CHAR(SYSDATE, 'mm') ";
    }
    $this->datatables->select("ID_INC, ALASAN,KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR, to_char(TGL_PEMBATALAN,'dd Mon YYYY , HH24: MI: SS')AS TGL_PEMBATALAN");

    $this->datatables->from("SPTPD_PEMBATALAN");
    $this->datatables->where($wh);
    return $this->datatables->generate();
  }
  function getIP()
  {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
      $ipaddress = 'UNKNOWN';

    return $ipaddress;
  }
  function getEmail($npwpd = '')
  {
    $ip = $this->db->query("SELECT getEmail('$npwpd')Email FROM DUAL")->row();
    $email = $ip->EMAIL;
    return $email;
  }


  function getKodebiling($kode)
  {
    $this->db->where('KODE_BILING', $kode);
    return $this->db->get('V_GET_TAGIHAN');
  }
  function get_by_id($id)
  {
    return $this->db->query("SELECT NAMA_WP,NPWPD,OBJEK_PAJAK FROM TAGIHAN WHERE KODE_BILING='$id'")->row();
  }

  function addbayar($data = array())
  {
    $this->db->trans_start();
    //  entri ke pembayran
    $KODE_BILING     = $data['KODE_BILING'];
    $MASA_PAJAK      = $data['MASA_PAJAK'];
    $TAHUN_PAJAK     = $data['TAHUN_PAJAK'];
    $TAGIHAN         = $data['TAGIHAN'];
    $DENDA           = $data['DENDA'];
    $TOTAL           = $data['TOTAL'];
    $KODE_BANK       = $data['KODE_BANK'];
    $KODE_PENGESAHAN = KdPengesahan();
    $NIP_INSERT      = $this->nip;
    $IP_INSERT       = $this->getIP();
    $row = $this->get_by_id($KODE_BILING);
    $this->db->query("CALL P_INSERT_SPTPD_PEMBAYARAN('$KODE_BILING','$IP_INSERT','$NIP_INSERT','$TOTAL','$KODE_PENGESAHAN','$KODE_BANK')");
    $this->db->set('STATUS', 1);
    $this->db->set('TGL_BAYAR', 'sysdate', false);
    $this->db->where('KODE_BILING', $data['KODE_BILING']);
    $this->db->update($data['KETERANGAN']);

    $this->db->trans_complete();

    // return $this->db->trans_status();

    if ($this->db->trans_status() === FALSE) {
      return 'false';
    } else {
      return $KODE_PENGESAHAN;
    }
  }
  function addbayar_va($data = array())
  {
    $this->db->trans_start();
    //  entri ke pembayran
    $TANGGAL         = $data['TGL_BAYAR'];
    $VIRTUAL_ACCOUNT = $data['VIRTUAL_ACCOUNT'];
    $TAGIHAN         = $data['TAGIHAN'];
    $KODE_BANK       = '';
    $KODE_PENGESAHAN = $data['REFERENCE'];
    $NIP_INSERT      = $this->nip;
    $IP_INSERT       = $this->getIP();
    $this->db->query("CALL P_INSERT_SPTPD_PEMBAYARAN_VA('$VIRTUAL_ACCOUNT','$IP_INSERT','$NIP_INSERT','$TAGIHAN','$KODE_PENGESAHAN','$TANGGAL','$KODE_BANK')");
    $this->db->set('STATUS', 1);
    $this->db->set('TGL_BAYAR', "TO_DATE('$TANGGAL','yyyy/mm/dd hh24:mi:ss')",false);
    $this->db->where('NO_VA', $data['VIRTUAL_ACCOUNT']);
    $this->db->update($data['KETERANGAN']);

    $this->db->trans_complete();

    // return $this->db->trans_status();

    if ($this->db->trans_status() === FALSE) {
      return 'false';
    } else {
      return $KODE_PENGESAHAN;
    }
  }

  function addbayarbphtb($data = array())
  {
    $this->db->trans_start();
    //  entri ke pembayran
    $KODE_BILING     = $data['KODE_BILING'];
    $MASA_PAJAK      = $data['MASA_PAJAK'];
    $TAHUN_PAJAK     = $data['TAHUN_PAJAK'];
    $TAGIHAN         = $data['TAGIHAN'];
    $DENDA           = $data['DENDA'];
    $TOTAL           = $data['TOTAL'];
    $KODE_BANK       = $data['KODE_BANK'];
    $KODE_PENGESAHAN = KdPengesahan();
    $NIP_INSERT      = $this->nip;
    $IP_INSERT       = $this->getIP();
    // $row = $this->get_by_id($KODE_BILING);
    $date = date('d/m/Y');
    $res = $this->db->query("call bayar_tagihan@tobphtb('$KODE_BILING',$TOTAL,TO_DATE('$date', 'DD/MM/YYYY'),'$KODE_BANK','$KODE_PENGESAHAN','$NIP_INSERT','$IP_INSERT')");

    $this->db->trans_complete();


    if ($this->db->trans_status() === FALSE) {
      return 'false';
    } else {
      return $KODE_PENGESAHAN;
    }
  }

  function addbayar_aplikasi($data = array())
  {
    $this->db->trans_start();
    //  entri ke pembayran
    $KODE_BILING     = $data['KODE_BILING'];
    $MASA_PAJAK      = $data['MASA_PAJAK'];
    $TAHUN_PAJAK     = $data['TAHUN_PAJAK'];
    $TAGIHAN         = $data['TAGIHAN'];
    $DENDA           = $data['DENDA'];
    $TOTAL           = $data['TOTAL'];
    $KODE_BANK       = $data['KODE_BANK'];
    $TGL_BAYAR       = $data['TGL_BAYAR'];
    $KODE_PENGESAHAN = KdPengesahan();
    $NIP_INSERT      = $this->nip;
    $IP_INSERT       = $this->getIP();
    $row = $this->get_by_id($KODE_BILING);
    $this->db->query("CALL P_INSERT_PEMBAYARAN_APLIKASI('$KODE_BILING','$IP_INSERT','$NIP_INSERT','$TOTAL','$KODE_PENGESAHAN','$KODE_BANK','$TGL_BAYAR')");
    $this->db->set('STATUS', 1);
    $this->db->set('TGL_BAYAR', "to_date('$TGL_BAYAR','dd/mm/yyyy')", false);
    $this->db->where('KODE_BILING', $data['KODE_BILING']);
    $this->db->update($data['KETERANGAN']);
    $this->db->trans_complete();

    // return $this->db->trans_status();

    if ($this->db->trans_status() === FALSE) {
      return 'false';
    } else {
      return $KODE_PENGESAHAN;
    }
  }
  function reversal($data = array())
  {
    $this->db->trans_start();
    $KODE_BILING    = $data['KODE_BILING'];
    $MASA_PAJAK        = $data['MASA_PAJAK'];
    $TAHUN_PAJAK      = $data['TAHUN_PAJAK'];
    $TAGIHAN     = $data['TAGIHAN'];
    $DENDA     = $data['DENDA'];
    $TOTAL    = $data['TOTAL'];
    $ALASAN    = $data['ALASAN'];
    $KODE_PENGESAHAN = '';
    $NIP_INSERT = $this->nip;
    $IP_INSERT = $this->getIP();
    $this->db->query("CALL P_REVERSAL('$KODE_BILING','$IP_INSERT','$NIP_INSERT','$TOTAL','$KODE_PENGESAHAN','$ALASAN')");
    $this->db->set('STATUS', 0);
    $this->db->set('TGL_BAYAR', '');
    $this->db->where('KODE_BILING', $data['KODE_BILING']);
    $this->db->update($data['KETERANGAN']);
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return 'false';
    } else {
      return 'true';
    }
  }

  // reversalbphtb
  function reversalbphtb($data = array())
  {
    $this->db->trans_start();
    $KODE_BILING = $data['KODE_BILING'];
    $MASA_PAJAK = $data['MASA_PAJAK'];
    $TAHUN_PAJAK = $data['TAHUN_PAJAK'];
    $TAGIHAN = $data['TAGIHAN'];
    $DENDA = $data['DENDA'];
    $TOTAL = $data['TOTAL'];
    $ALASAN = $data['ALASAN'];
    $KODE_PENGESAHAN = '';
    $NIP_INSERT = $this->nip;
    $IP_INSERT = $this->getIP();

    $this->db->query("call reversal_tagihan@tobphtb('$KODE_BILING', '$TOTAL' , '$NIP_INSERT', '$IP_INSERT' )");

    $this->db->trans_complete();

    // return $this->db->last_query();

      if ($this->db->trans_status() === FALSE) {
        return 'false';
      } else {
        return 'true';
      }
  }

  function pemintaan_reversal($data = array())
  {
    // $this->db->trans_start();
    $KODE_BILING    = $data['KODE_BILING'];
    $MASA_PAJAK        = $data['MASA_PAJAK'];
    $TAHUN_PAJAK      = $data['TAHUN_PAJAK'];
    $TAGIHAN     = $data['TAGIHAN'];
    $DENDA     = $data['DENDA'];
    $TOTAL    = $data['TOTAL'];
    $ALASAN    = $data['ALASAN'];
    $KODE_PENGESAHAN = '';
    $NIP_INSERT = $this->nip;
    $IP_INSERT = $this->getIP();
    $this->db->query("CALL P_PERMINTAAN_REVERSAL('$KODE_BILING','$IP_INSERT','$NIP_INSERT','$TOTAL','$KODE_PENGESAHAN','$ALASAN')");
    //$this->db->update($data['KETERANGAN']);  
    //$this->db->trans_complete();
    if ($this->db->trans_status() === FALSE) {
      return 'false';
    } else {
      return 'true';
    }
  }

  function registration($va='',$nama='',$total='',$exp=''){
      $data = array(
            'VirtualAccount'      => $va,
            'Nama' => $nama,
            'TotalTagihan'    => $total,
            'TanggalExp'    => $exp,
            'Berita1'    => '-',
            'Berita2'    => '-',
            'Berita3'    => '-',
            'Berita4'    => '-',
            'Berita5'    => '-',
            'FlagProses' => 1,
            
    );

    $data_string = json_encode($data);
    $curl = curl_init('http://apps.bankjatim.co.id/Api/Registrasi');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data

    // Send the request
    $result = curl_exec($curl);
    // Free up the resources $curl is using  
    //echo $data_string;
    $content = $data_string;
    $nm_f=$va;
    $filename = $nm_f.".txt"; //the name of our file.
        $strlength = strlen($content); //gets the length of our $content string.
        $create = fopen('log_va/registrasi/'.$filename, "w");
        if( $create == false ){
            //do debugging or logging here
        }else{
            fwrite($create,$content);
            fclose($create);
        }
      curl_close($curl);

       

 /*$data = array(
            'patient_id'      => $username,
            'department_name' => 'ok',
            'patient_type'    => 'b'
    );
    $data_string = json_encode($data);
    $url = 'http://36.89.91.154:8080/pdrd/pembayaran/webservice_ori/posting'; 
    $this->curl->simple_post($url, $data_string, array(CURLOPT_BUFFERSIZE => 10));*/
  }
}
