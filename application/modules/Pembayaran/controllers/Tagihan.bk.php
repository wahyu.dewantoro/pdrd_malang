<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mbayar');
 }
 

  function index(){
        if(isset($_POST['kode_biling'])){
            $data['kode_biling']=$_POST['kode_biling'];
            $data['rk']=$this->Mbayar->getKodebiling($_POST['kode_biling']);
        }else{
            $data['kode_biling']=null;
        }

    // getKodebiling
    $this->template->load('Welcome/halaman','formbayar',$data);
  }

  function prosesbayar(){
    $data['KODE_BILING']=$this->input->post('KODE_BILING');
    $data['NAMA_WP']=$this->input->post('NAMA_WP');
    $data['ALAMAT_WP']=$this->input->post('ALAMAT_WP');
    $data['NAMA_USAHA']=$this->input->post('NAMA_USAHA');
    $data['MASA_PAJAK']=$this->input->post('MASA_PAJAK');
    $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
    $data['PAJAK_TERUTANG']=str_replace('.','',$this->input->post('PAJAK_TERUTANG'));
    $data['KETERANGAN']=$this->input->post('KETERANGAN');
    $data['TGL_BAYAR']=$this->input->post('TGL_BAYAR');
    $data['JUMLAH_BAYAR']= str_replace('.','',$this->input->post('JUMLAH_BAYAR'));
 
    $res=$this->Mbayar->addbayar($data);
    if($res=='true'){
        $pesan='Kode biling '.$this->input->post('KODE_BILING').' Telah di proses.';
    }else{
        $pesan='Kode biling '.$this->input->post('KODE_BILING').' Gagal di proses.';
    }   
     $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$pesan.'", "", "success")
     });
     </script>');
    redirect(site_url('pembayaran/tagihan'));
  }

}