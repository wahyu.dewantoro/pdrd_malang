<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class permintaan_unflag extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Mbayar');
        $this->load->library('datatables');
 }
 

  function index(){
    if(isset($_GET['tgl1'])){
                $tgl1=$_GET['tgl1'];
                $tgl2=$_GET['tgl2'];
                $data['data_permintaan']=$this->db->query("SELECT ID_INC, ALASAN,KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR, to_char(TGL_PEMBATALAN,'dd Mon YYYY , HH24: MI: SS')AS TGL_PEMBATALAN,to_char(TGL_INSERT,'dd Mon YYYY')AS TGL_AJUAN,STATUS_UNFLAGE 
                  FROM SPTPD_PEMBATALAN 
                  WHERE TO_DATE(TO_CHAR(TGL_PEMBATALAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')
                  AND TO_DATE(TO_CHAR(TGL_PEMBATALAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')")->result();
        } else {
                $tgl1=date('d-m-Y');
                $tgl2='';//date('Y');//date('d-m-Y');
                $data['data_permintaan']=$this->db->query("SELECT ID_INC, ALASAN,KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR, to_char(TGL_PEMBATALAN,'dd Mon YYYY , HH24: MI: SS')AS TGL_PEMBATALAN,to_char(TGL_INSERT,'dd Mon YYYY')AS TGL_AJUAN,STATUS_UNFLAGE FROM SPTPD_PEMBATALAN WHERE TO_DATE(TO_CHAR(TGL_PEMBATALAN,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('$tgl1','DD-MM-YYYY')")->result();
        }
        $sess=array(
                'tgl1'=>$tgl1,
                'tgl2'=>$tgl2
                //'bulan'=>$bulan
         );
      $this->session->set_userdata($sess);
    $this->template->load('Welcome/halaman','permintaan_unflag',$data);
  }

  function create(){
        if(isset($_POST['kode_biling'])){
            $data['kode_biling']=$_POST['kode_biling'];
            $data['rk']=$this->Mbayar->getKodebiling($_POST['kode_biling']);
        }else{
            $data['kode_biling']=null;
        }

    // getKodebiling
    $this->template->load('Welcome/halaman','form_permintaan_pembatalan',$data);
  }

  public function add_permintaan()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $content     = file_get_contents("php://input");
            $decoded     = json_decode($content, true);
            

            $KODE_BILING = $this->input->post('Nop');
            $MASA_PAJAK  = (int)$this->input->post('Masa');
            $TAHUN_PAJAK = $this->input->post('Tahun');
            $TAGIHAN     = (int)$this->input->post('Pokok');
            $DENDA       = (int)$this->input->post('Denda');
            $TOTAL       = (int)$this->input->post('Total');
            $ALASAN      = $this->input->post('Alasan');
           

             $q=$this->db->query("SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, NO_SK, TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, REK_BANK, 
                CEIL(TAGIHAN) TAGIHAN,
                CEIL(DENDA) DENDA,
                CEIL(POTONGAN)POTONGAN,
                CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                STATUS, KODE_PENGESAHAN
                FROM TAGIHAN
                WHERE KODE_BILING='$KODE_BILING' AND
                MASA_PAJAK='$MASA_PAJAK' AND
                TAHUN_PAJAK = '$TAHUN_PAJAK'")->row();
            $data = array();
            if ( empty($q) || $q->STATUS==0) {
                // data tidak ditemukan
                $data = array(
                
                    "Status"     =>  array(
                        "IsError"       =>"True",
                        "ResponseCode"  =>'34',
                        "ErrorDesc"     => "Data reversal tidak ditemukan"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }  else {
                // ada data
                if (((int)$q->PAJAK_TERUTANG)  != $TAGIHAN || ((int)$q->DENDA) !=$DENDA  ||  ((int)$q->TOTAL) != $TOTAL ) {
                    $data = array(
                   
                    "Status"     =>  array(
                        "IsError"       =>"True",
                        "ResponseCode"  =>'14',
                        "ErrorDesc"     => "Jumlah tagihan yang dibayarkan tidak sesuai"

                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
                }else{
                     $query=$this->db->query("select keterangan,kode_biling from SPTPD_TAGIHAN where kode_biling='$KODE_BILING'")->row();
                    $data['KETERANGAN']     = $query->KETERANGAN;
                    $data['KODE_BILING']    = $KODE_BILING;
                    $data['MASA_PAJAK']     = $MASA_PAJAK;
                    $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                    $data['TAGIHAN']        = str_replace('.','',$TAGIHAN);
                    $data['DENDA']          = str_replace('.','',$DENDA);
                    $data['TOTAL']          = str_replace('.','',$TOTAL);
                    $data['ALASAN']         = $ALASAN;
                    $res=$this->Mbayar->pemintaan_reversal($data);
                    $data = array();
                    if($res=='true'){
                        $data = array(
                            "Nop"        =>  $q->KODE_BILING,
                            "Masa"       =>  sprintf("%02d",$q->MASA_PAJAK),
                            "Tahun"      =>  $q->TAHUN_PAJAK,
                            "JatuhTempo" =>  $q->JATUH_TEMPO,
                            "KodeRek"    =>  $q->REK_BANK,
                            "Pokok"      =>  (int)$q->PAJAK_TERUTANG,
                            "Denda"      => (int)$q->DENDA,
                            "Total"      =>  (int)$q->TOTAL,
                            "Status"     =>  array(
                                "IsError"      =>"False",
                                "ResponseCode" =>'00',
                                "ErrorDesc"    => "Sukses"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                }  else{
                        $data = array(
                            "Nop"    =>  $KODE_BILING,
                            "Status" =>  array(
                                "IsError"      =>"True",
                                "ResponseCode" =>'99',
                                "ErrorDesc"    => "System Error"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
            }
        }
     }   
    }

    function approve($KODE_BILING=''){
         $this->db->trans_start();
         $query=$this->db->query("select keterangan from SPTPD_pembatalan where kode_biling='$KODE_BILING'")->row();
         $cek=$this->db->query("select kode_pengesahan from tagihan where kode_biling='$KODE_BILING'")->row();
                    $KETERANGAN     = $query->KETERANGAN;
                    $KODE_PENGESAHAN= $cek->KODE_PENGESAHAN;
         $this->db->query("CALL P_APPROVE_REVERSAL('$KODE_BILING','$KODE_PENGESAHAN')");
         $this->db->set('STATUS',0);
         $this->db->set('TGL_BAYAR','');
         $this->db->where('KODE_BILING',$KODE_BILING);
         $this->db->update($KETERANGAN);  
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return 'false';
        } else {
            return 'true';
        }        
      redirect(site_url('Pembayaran/permintaan_unflag'));
    }

}