<?php

class Webservice_ori extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mbayar');
    }
    public function tes()
    {
        echo "tes";
    }

    public function pembayaran()
    {
        $data = array(
      'title'              => 'Pembayaran',
      'action'             => site_url('webservice/create_action'),
            'script'             => 'webservice/script',
      );
        $this->template->load('layout','webservice/view_form',$data);
    }

    public function registration(){
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $VIRTUAL_ACCOUNT = $this->input->get('va',TRUE);
        $query=$this->db->query(
            "SELECT
                KODE_BILING,
                NAMA_WP,
                ALAMAT_WP,
                MASA_PAJAK,
                TAHUN_PAJAK,
                VIRTUAL_ACCOUNT,
                TO_CHAR(AKHIR_BERLAKU, 'ddmmyyyy') JATUH_TEMPO,
                KODE_REK AS REK_BANK,
                CEIL(TAGIHAN) TAGIHAN,
                CEIL(DENDA) DENDA,
                CEIL(POTONGAN)POTONGAN,
                CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                STATUS
            FROM TAGIHAN
            WHERE VIRTUAL_ACCOUNT='$VIRTUAL_ACCOUNT'")->row();

            $data = array();
            if (!empty($query->VIRTUAL_ACCOUNT)) {

                if($query->STATUS==1){
                    $data = array(
                        "Status"        => array(
                            "IsError"=>"True",
                            "ResponseCode"=>'13',
                            "ErrorDesc" => "Data tagihan telah lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data,200);
                }else{

                $data = array(
                    "VirtualAccount"        =>  $query->VIRTUAL_ACCOUNT,
                    "Nama"     => $query->NAMA_WP,
                    "TotalTagihan " => (int)$query->TOTAL,
                    "TanggalExp"=> $query->JATUH_TEMPO,
                    "Berita1"=> "Pembayaran Virtual Account",
                    "FlagProses"=> "1"
                );
                header('Content-Type: application/json');
                echo json_encode($data,200);
                }
            } else {
                $data = array(
                   
                    "Status"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
             
                header('Content-Type: application/json');
                echo json_encode($data);
            }


       }

    }

    public function posting(){
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
        $content = file_get_contents("php://input");
        $decoded = json_decode($content, true);
        $v_va = $decoded["VirtualAccount"];
        $this->db->query("call P_INSERT_LOG('$v_va','$content','post_payment')");
        /*$filename = $v_va.".txt"; //the name of our file.
        $strlength = strlen($content); //gets the length of our $content string.
        $create = fopen('log_va/post/'.$filename, "w");
        if( $create == false ){
            //do debugging or logging here
        }else{
            fwrite($create,$content);
            fclose($create);
        } */  ///uses fopen to create our file.
         
        $VIRTUAL_ACCOUNT = $decoded["VirtualAccount"];
        $TAGIHAN     = (int)$decoded["Amount"];
        $REFERENCE  =$decoded["Reference"];
        $TANGGAL =$decoded["Tanggal"];
        //$KODE_BANK   = '114';
        
        $q=$this->db->query("SELECT KODE_BILING,NPWPD,NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, NO_SK, TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, REK_BANK, 
            CEIL(TAGIHAN) TAGIHAN,
            CEIL(DENDA) DENDA,
            CEIL(POTONGAN)POTONGAN,
            CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
            CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
            STATUS, KODE_PENGESAHAN,VIRTUAL_ACCOUNT
            
            FROM TAGIHAN
                                WHERE
                                    VIRTUAL_ACCOUNT='$VIRTUAL_ACCOUNT'")->row();

        
        if (!empty($q->VIRTUAL_ACCOUNT)) {
            if ($q->STATUS==1) {
                // tagihan telah lunas
                $data = array();
                $data = array(

                    "Status"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'13',
                        "ErrorDesc" => "Data Tagihan Telah Lunas"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
                        $content = $data;
                        $nm_f=$q->VIRTUAL_ACCOUNT;
                        $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_13')");
                        /*$filename = '13'.$nm_f.".txt"; //the name of our file.
                        $strlength = strlen($content); //gets the length of our $content string.
                        $create = fopen('log_va/rc/'.$filename, "w");
                                if( $create == false ){
                                    //do debugging or logging here
                                }else{
                                    fwrite($create,$content);
                                    fclose($create);
                                }*/
            } else {
                // belum lunas
                if (((int)$q->TOTAL) != $TAGIHAN ) {
                    // tagihan tidak sesuai
                    $data = array();
                    $data = array(
                        "Status"     =>  array(
                            "IsError"      =>"True",
                            "ResponseCode" =>'14',
                            "ErrorDesc"    => "Jumlah tagihan yang dibayarkan tidak sesuai"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                                $content = json_encode($data);
                                $nm_f=$q->VIRTUAL_ACCOUNT;
                                $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_14')");
                                /*$filename = '14'.$nm_f.".txt"; //the name of our file.
                                $strlength = strlen($content); //gets the length of our $content string.
                                $create = fopen('log_va/rc/'.$filename, "w");
                                        if( $create == false ){
                                            //do debugging or logging here
                                        }else{
                                            fwrite($create,$content);
                                            fclose($create);
                                        }*/
                } else {
                    // sukses                  
                        $query = $this->db->query("select keterangan,no_va from SPTPD_TAGIHAN where no_va='$VIRTUAL_ACCOUNT'")->row();
                        $data['KETERANGAN']     = $query->KETERANGAN;
                        $data['VIRTUAL_ACCOUNT']= $VIRTUAL_ACCOUNT;
                        $data['TAGIHAN']        = $TAGIHAN;
                        $data['REFERENCE']      = $REFERENCE;
                        $data['TGL_BAYAR']      = $TANGGAL;
                        $res = $this->Mbayar->addbayar_va($data);
                    $data = array();
                    if($res=='false'){
                       $data = array(
                            
                            "Status" =>  array(
                                "IsError"=>"True",
                                "ResponseCode"=>'99',
                                "ErrorDesc" => "System Error"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                                $content = json_encode($data);
                                $nm_f=$q->VIRTUAL_ACCOUNT;
                                $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_99')");
                                /*$filename = '99'.$nm_f.".txt"; //the name of our file.
                                $strlength = strlen($content); //gets the length of our $content string.
                                $create = fopen('log_va/rc/'.$filename, "w");
                                        if( $create == false ){
                                            //do debugging or logging here
                                        }else{
                                            fwrite($create,$content);
                                            fclose($create);
                                        }*/
                    }else{
                            $cek_tujuan=$this->db->query("select GETEMAIL('$q->NPWPD')tujuan from dual")->row();
                            $tujuan=$cek_tujuan->TUJUAN;//'adypabbsi@gmail.com';
                            $ci                  = get_instance();
                            $ci->load->library('email');
                            $config['protocol']  = "smtp";
                            $config['smtp_host'] = "ssl://smtp.gmail.com";
                            $config['smtp_port'] = "465";
                            $config['smtp_user'] = "malangbapendakab@gmail.com"; 
                            $config['smtp_pass'] = "B4p3nd4km";
                            $config['charset']   = "utf-8";
                            $config['mailtype']  = "html";
                            $config['newline']   = "\r\n";          
                            $ci->email->initialize($config);            
                            $ci->email->from('Bapenda Kab. Malang', 'PDRD.KAB.MALANG');
                            $list                = array($tujuan);
                            $ci->email->to($list);
                            $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                            $ci->email->subject('Pembayaran Tagihan');
                            $messageFromHtml = $this->messageHtml($q->VIRTUAL_ACCOUNT,$q->NAMA_WP,$TAGIHAN,$TANGGAL);
                            $ci->email->message($messageFromHtml);
                            //$ci->email->message('Pembayaran Tagihan Nomor virtual Account '.$q->VIRTUAL_ACCOUNT.' dengan nominal Rp. '.number_format($TAGIHAN,0,",",".").' Berhasil');
                            $ci->email->send();                        
                        $data = array(
                            "VirtualAccount"        =>  $q->VIRTUAL_ACCOUNT,
                            "Amount"      =>  (int)$TAGIHAN,
                            "Tanggal"      => $TANGGAL,
                            "Status"     =>  array(
                                "IsError"      =>"False",
                                "ResponseCode" =>'00',
                                "ErrorDesc"    => "Sukses"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                                $content = json_encode($data);
                                $nm_f=$q->VIRTUAL_ACCOUNT;
                                $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_00')");
                                /*$filename = '00'.$nm_f.".txt"; //the name of our file.
                                $strlength = strlen($content); //gets the length of our $content string.
                                $create = fopen('log_va/rc/'.$filename, "w");
                                        if( $create == false ){
                                            //do debugging or logging here
                                        }else{
                                            fwrite($create,$content);
                                            fclose($create);
                                        }*/
                        
                    }
                }
            }
        }else{
            // data tidak di temukan
            $data = array();
            $data = array(
                /*"Nop"           => $KODE_BILING,
                "Masa"          => sprintf("%02d",$MASA_PAJAK),
                "Tahun"         => $TAHUN_PAJAK,
                "Pokok"         => $TAGIHAN,
                "Denda"         => (int)$DENDA,
                "Total"         => $TOTAL,*/
                "Status"        =>  array(
                    "IsError"=>"True",
                    "ResponseCode"=>'10',
                    "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                )
            );
            header('Content-Type: application/json');
            echo json_encode($data);
                                $content = json_encode($data);
                                $nm_f=$q->VIRTUAL_ACCOUNT;
                                $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_10')");
                                $filename = '10'.$nm_f.".txt"; //the name of our file.
                                $strlength = strlen($content); //gets the length of our $content string.
                                $create = fopen('log_va/rc/'.$filename, "w");
                                        if( $create == false ){
                                            //do debugging or logging here
                                        }else{
                                            fwrite($create,$content);
                                            fclose($create);
                                        }
        }

    }
}

function messageHtml($virtual="",$nama="",$tagihan="",$tanggal="") {
        return
        '<table style="border-collapse:collapse;border-spacing:0;margin:0;padding:0" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr>
                          <td style="">
                            <table style="border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                              <tbody>
                                <tr>
                                  <td bgcolor="#ffffff">
                                    <table style="border-collapse:collapse;border-spacing:0;margin:0;padding:0" width="100%" align="left">
                                      <tbody>
                                        <tr>
                                          <table style="border-collapse:collapse;border-spacing:0;margin:0;padding:0" width="100%" align="left">
                                            <tbody>
                                              <tr>
                                                <td style="">
                                                  <div>
                                                      <h1 style="font-family:Arial;color:#808080;line-height:80%;font-size:20px;display:block;font-weight:bold;border-bottom-width:1px;border-bottom-color:#e8e8e8;border-bottom-style:solid;margin:0 0 20px;padding:0" align="left">
                                                        Pembayaran '.$virtual.' Sukses<br>&nbsp;
                                                      </h1>
                                                  </div>
                                                  <div>
                                                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">Hai '.$nama.' ,</p>
                                                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">Selamat! Pembayaran untuk tagihan '.$virtual.' telah berhasil. <br>
                                                        Terima kasih Telah Membayar Pajak</p>
                                                  </div>
                                                  <div>
                                                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0 0 10px;padding:0" align="center">&nbsp;</p>
                                                  </div>
                                                    <div>
                                                      <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0 0 8px">Berikut adalah penjelasan tagihan pembayaran:</p>
                                                      <table style="border-bottom-style:none;border-collapse:collapse;border-spacing:0;font-size:12px;border-bottom-color:#cccccc;border-bottom-width:1px;margin:0 0 25px;padding:0" width="100%" cellspacing="0" cellpadding="5" border="0" bgcolor="#FFFFFF">
                                                        <tbody>
                                                          <tr>
                                                            <th colspan="3" style="border-bottom-style:none;color:#ffffff;padding-left:10px;padding-right:10px" bgcolor="#900135">
                                                              <h2 style="font-family:&quot;Arial&quot;,sans-serif;color:#ffffff;line-height:1.5;font-size:14px;margin:0;padding:5px 0">Tagihan 
                                                              </h2>
                                                            </th>
                                                          </tr>
                                                          <tr>
                                                            <th colspan="3" style="border-bottom-style:none;color:#ffffff;padding-left:10px;padding-right:10px" bgcolor="#900135"></th>
                                                          </tr>
                                                          <tr>
                                                            <td class="" color="#ffffff !important" colspan="2" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;font-size:11px;margin:0;padding:5px 10px" width="40%" valign="top" bgcolor="#F0F0F0" align="left">
                                                            <strong style="color:#555;font-size:14px">Keterangan</strong>
                                                            </td>
                                                            <td class="m_-8029522690150426518headingList" color="#ffffff !important" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;font-size:11px;margin:0;padding:5px 10px" width="60%" valign="top" bgcolor="#F0F0F0" align="right">
                                                            <strong style="color:#555;font-size:14px">Rincian</strong>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" valign="top" bgcolor="#FFFFFF" align="left">
                                                            Tagihan
                                                            </td>
                                                            <td style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" width="100%" valign="top" bgcolor="#FFFFFF" align="right">
                                                            <span class=" " style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">Rp </span><span class="" style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">'.number_format($tagihan,'0','','.').'</span>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" valign="top" bgcolor="#FFFFFF" align="left">
                                                            Tanggal Lunas
                                                            </td>
                                                            <td style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" width="100%" valign="top" bgcolor="#FFFFFF" align="right">'.$tanggal.'</span></td>
                                                          </tr>
                                                          <tr class="">
                                                            <td class="m_-8029522690150426518grand-total-title" colspan="2" style="border-bottom-color:#ffffff!important;border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-width:1px!important;border-bottom-style:solid!important;margin:0;padding:5px 10px" valign="top" bgcolor="#f0f0f0" align="left">
                                                            <strong style="color:#555;font-size:14px">TOTAL PEMBAYARAN</strong>
                                                            </td>
                                                            <td class="m_-8029522690150426518grand-total-amount" style="border-bottom-color:#ffffff!important;border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-width:1px!important;border-bottom-style:solid!important;border-top-color:#ffffff;margin:0;padding:5px 10px" width="25%" valign="top" bgcolor="#67a348" align="right">
                                                            <strong style="color:#555;font-size:14px"><span tyle="font-family:&quot;Arial&quot;,sans-serif;color:#ffffff!important;line-height:1.5;font-size:14px;margin:0;padding:0">Rp '. number_format($tagihan,'0','','.').'</span></strong>
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>

                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td style="">
                    <table id="m_-8029522690150426518footerSeparator" style="border-collapse:collapse;border-spacing:0;table-layout:fixed;border-top-width:2px;border-top-style:solid;border-top-color:#ccc;margin:0;padding:0" width="100%">
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td style="">
                    <table style="border-collapse:collapse;border-spacing:0;font-size:10px;text-align:center;margin:0;padding:0" width="100%">
                    <tbody>
                    <tr>
                    <td style=" 10px">
                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#999;line-height:1.5;margin:0;padding:0">
                    Copyright © 2019 Kab. Malang All Rights Reserved
                    </p>
                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#999;line-height:1.5;margin:0;padding:0">
                    JL. Raden Panji No. 158, Telp. (0341) 390683 Kepanjen 
                    </p>
                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#999;line-height:1.5;margin:0;padding:0">
                    Harap jangan membalas e-mail ini, karena e-mail ini dikirimkan secara otomatis oleh sistem.
                    </p>
                    </td>
                    </tr>
                    <tr>
                    <td style="" height="15"></td>
                    </tr>
                    </tbody></table>

                    </td>
                    </tr>
                    </tbody></table>
                    </td>
                    </tr>
                    </tbody></table>';
    }

}