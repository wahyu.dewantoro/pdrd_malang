<?php

class Webservice extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        /*if(ENVIRONMENT=='development'){
            $pass='1a1dc91c907325c69271ddf0c944bc72';
        }else{
            $pass='3389a711e2494c01896c082bbe07ac28';
        }


        if (!(isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) 
                && $_SERVER['PHP_AUTH_USER'] == 'bankjatim' 
                && md5($_SERVER['PHP_AUTH_PW'] )== $pass)) {
                header('WWW-Authenticate: Basic realm="Restricted area"');
                header('HTTP/1.1 401 Unauthorized');
                exit;
            }*/
        $dbbphtb = $this->load->database('db_bphtb', TRUE);
        $this->load->model('Mbayar');
    }



    public function inquiry()
    {


        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $KODE_BILING = $this->input->get('Nop', TRUE);
            $MASA_PAJAK  =  (int) $this->input->get('MasaPajak', TRUE);
            $TAHUN_PAJAK = $this->input->get('TahunPajak', TRUE);
            $POKOK       = $this->input->get('Pokok', TRUE);

            $sql = "SELECT 
            KODE_BILING,
            NAMA_WP,
            ALAMAT_WP,
            MASA_PAJAK,
            TAHUN_PAJAK,
            NO_SK,
            TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO,
            KODE_REK AS REK_BANK,
            CEIL(TAGIHAN) TAGIHAN,
            CEIL(DENDA) DENDA,
            CEIL(POTONGAN)POTONGAN,
            CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
            CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
            STATUS
        FROM TAGIHAN
        WHERE KODE_BILING='$KODE_BILING' AND MASA_PAJAK='$MASA_PAJAK' AND TAHUN_PAJAK='$TAHUN_PAJAK'
       UNION ALL 
        SELECT ID_BILLING KODE_BILING,initcap(NAMA_WP) nama_wp,initcap(ALAMAT_WP) ALAMAT_WP,SUBSTR(ID_BILLING,10,2) MASA_PAJAK, '20'||SUBSTR(ID_BILLING,8,2) TAHUN_PAJAK,NULL NO_SK,'3112'||TO_CHAR(SYSDATE,'yyyy') JATUH_TEMPO, KODE_REK REF_BANK,
         BPHTB TAGIHAN, DENDA,0 POTONGAN, BPHTB PAJAK_TERUTANG,TOTAL, CASE WHEN STATUS_BAYAR = 1 THEN '1' ELSE '0' END STATUS            
        FROM TAGIHAN_SSPD@TOBPHTB
        WHERE ID_BILLING='$KODE_BILING'";
            $query = $this->db->query($sql)->row();


            /*   $query = $this->db->query(
                "SELECT
                KODE_BILING,
                NAMA_WP,
                ALAMAT_WP,
                MASA_PAJAK,
                TAHUN_PAJAK,
                NO_SK,
                TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO,
                KODE_REK AS REK_BANK,
                CEIL(TAGIHAN) TAGIHAN,
                CEIL(DENDA) DENDA,
                CEIL(POTONGAN)POTONGAN,
                CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                STATUS
            FROM TAGIHAN
            WHERE KODE_BILING='$KODE_BILING' and MASA_PAJAK='$MASA_PAJAK' and TAHUN_PAJAK='$TAHUN_PAJAK'"
            )->row(); */

            /*
           AND TAGIHAN-POTONGAN='$POKOK'*/
            $data = array();
            if (!empty($query->KODE_BILING)) {

                if ($query->STATUS == 1) {
                    $data = array(
                        "Status"        => array(
                            "IsError" => "True",
                            "ResponseCode" => '13',
                            "ErrorDesc" => "Data tagihan telah lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data, 200);
                } else {

                    $data = array(
                        "Nop"        =>  $query->KODE_BILING,
                        "Nama"       =>  $query->NAMA_WP,
                        "Alamat"     =>  $query->ALAMAT_WP,
                        "Masa"       =>  sprintf("%02d", $query->MASA_PAJAK),
                        "Tahun"      => $query->TAHUN_PAJAK,
                        "NoSk"       => $query->NO_SK <> '' ? $query->NO_SK : '-',
                        "JatuhTempo" => $query->JATUH_TEMPO,
                        "KodeRek"    => $query->REK_BANK,
                        "Pokok"      => (int) $query->PAJAK_TERUTANG,
                        "Denda"      => (int) $query->DENDA,
                        "Total"      => (int) $query->TOTAL,
                        "Status"        => array(
                            "IsError" => "False",
                            "ResponseCode" => '00',
                            "ErrorDesc" => "Success"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data, 200);
                }
            } else {
                $data = array(
                    // "Nop"   =>  $KODE_BILING,
                    "Status"        =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
                /*  $data = array(
                    "Nop"        =>  $KODE_BILING,
                    "Nama"       =>  null,
                    "Alamat"     =>  null,
                    "Masa"       =>  sprintf("%02d", $MASA_PAJAK),
                    "Tahun"      => $TAHUN_PAJAK,
                    "NoSk"       => null,
                    "JatuhTempo" => null,
                    "KodeRek"    => null,
                    "Pokok"      => $POKOK,
                    "Denda"      => null,
                    "Total"      => null,
                    "Status"        => array(
                        "IsError"=>"False",
                        "ResponseCode"=>'10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );*/
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        }
    }


    public function payment()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            /*if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
            throw new Exception('Request method must be POST!');
        }*/

            //Make sure that the content type of the POST request has been set to application/json
            /*$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if(strcasecmp($contentType, 'application/json') != 0){
            throw new Exception('Content type must be: application/json');
        }*/

            //Receive the RAW post data.
            $content = file_get_contents("php://input");
            $decoded = json_decode($content, true);
            $v_va = $decoded["Nop"];
            // log pdrd
            $this->db->query("call P_INSERT_LOG('$v_va','$content','post_payment_kb')");
            // log bphtb
            //$db2->query("call P_INSERT_LOG('$v_va','$content','post_payment_kb')");

            $KODE_BILING = $decoded["Nop"];
            $MASA_PAJAK  = (int) $decoded["Masa"];
            $TAHUN_PAJAK = $decoded["Tahun"];
            $TAGIHAN     = (int) $decoded["Pokok"];
            $DENDA       = (int) $decoded["Denda"];
            $TOTAL       = (int) $decoded["Total"];
            $KODE_BANK   = '114';

            $q = $this->db->query("SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, NO_SK, TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, REK_BANK, 
                                CEIL(TAGIHAN) TAGIHAN,
                                CEIL(DENDA) DENDA,
                                CEIL(POTONGAN)POTONGAN,
                                CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                                CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                                STATUS, KODE_PENGESAHAN,'1' JENIS_PAJAK
                                FROM TAGIHAN
                                                    WHERE
                                                        KODE_BILING='$KODE_BILING' AND
                                                        MASA_PAJAK='$MASA_PAJAK' AND
                                                        TAHUN_PAJAK = '$TAHUN_PAJAK'
                                                        union all
                                                        SELECT a.ID_BILLING KODE_BILING,NAMA_WP,ALAMAT_WP,SUBSTR(a.ID_BILLING,10,2) MASA_PAJAK, '20'||SUBSTR(a.ID_BILLING,8,2) TAHUN_PAJAK,NULL NO_SK,'3112'||TO_CHAR(SYSDATE,'yyyy') JATUH_TEMPO, KODE_REK REF_BANK,
                                BPHTB TAGIHAN, a.DENDA,0 POTONGAN, BPHTB PAJAK_TERUTANG,TOTAL, CASE WHEN STATUS_BAYAR = 1 THEN '1' ELSE '0' END STATUS,ntpd kode_pengesahan,'2' jenis_pajak
                                FROM TAGIHAN_SSPD@TOBPHTB a
                                left join bphtb_pembayaran@tobphtb b on a.id_billing=b.id_billing 
                                WHERE a.ID_BILLING='$KODE_BILING'")->row();

            if (!empty($q->KODE_BILING)) {
                if ($q->STATUS == 1) {
                    // tagihan telah lunas
                    $data = array();
                    $data = array(
                        "Status"        =>  array(
                            "IsError" => "True",
                            "ResponseCode" => '13',
                            "ErrorDesc" => "Data Tagihan Telah Lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    $content = json_encode($data);
                    $nm_f = $q->KODE_BILING;
                    // log pdrd
                    $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_13')");
                    // log bphtb
                    //$db2->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_13')");
                } else {
                    // belum lunas
                    if (((int) $q->PAJAK_TERUTANG)  != $TAGIHAN || ((int) $q->DENDA) != $DENDA  || ((int) $q->TOTAL) != $TOTAL) {
                        // tagihan tidak sesuai
                        $data = array();
                        $data = array(
                            "Status"     =>  array(
                                "IsError"      => "True",
                                "ResponseCode" => '14',
                                "ErrorDesc"    => "Jumlah tagihan yang dibayarkan tidak sesuai"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                        $content = json_encode($data);
                        $nm_f = $q->KODE_BILING;
                        // log pdrd
                        $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_14')");
                        // log bphtb
                        //$db2->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_14')");
                    } else {
                        // sukses
                        if ($q->JENIS_PAJAK == '1') {
                            // pdrd
                            $query = $this->db->query("select keterangan,kode_biling from SPTPD_TAGIHAN where kode_biling='$KODE_BILING'")->row();
                            $data['KETERANGAN']     = $query->KETERANGAN;
                            $data['KODE_BILING']    = $KODE_BILING;
                            $data['MASA_PAJAK']     = $MASA_PAJAK;
                            $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                            $data['TAGIHAN']        = (int) str_replace('.', '', $TAGIHAN);
                            $data['DENDA']          = (int) str_replace('.', '', $DENDA);
                            $data['TOTAL']          = (int) str_replace('.', '', $TOTAL);
                            $data['KODE_BANK']      = $KODE_BANK;
                            $res = $this->Mbayar->addbayar($data);
                        } else {
                            // bphtb
                            $data['KETERANGAN']     = 'STPD_BPHTB';
                            $data['KODE_BILING']    = $KODE_BILING;
                            $data['MASA_PAJAK']     = $MASA_PAJAK;
                            $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                            $data['TAGIHAN']        = (int) str_replace('.', '', $TAGIHAN);
                            $data['DENDA']          = (int) str_replace('.', '', $DENDA);
                            $data['TOTAL']          = (int) str_replace('.', '', $TOTAL);
                            $data['KODE_BANK']      = $KODE_BANK;
                            $res = $this->Mbayar->addbayarbphtb($data);
                        }
                        $data = array();
                        if ($res == 'false') {
                            $data = array(

                                "Status" =>  array(
                                    "IsError" => "True",
                                    "ResponseCode" => '99',
                                    "ErrorDesc" => "System Error"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            $content = json_encode($data);
                            $nm_f = $q->KODE_BILING;
                            // pdrd
                            $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_99')");

                            // log bphtb
                            //$db2->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_99')");
                        } else {
                            $data = array(
                                "Nop"        =>  $q->KODE_BILING,
                                "Masa"       =>  sprintf("%02d", $q->MASA_PAJAK),
                                "Tahun"      =>  $q->TAHUN_PAJAK,
                                "JatuhTempo" =>  $q->JATUH_TEMPO,
                                "KodeRek"    =>  $q->REK_BANK,
                                "Pokok"      =>  (int) $q->PAJAK_TERUTANG,
                                "Denda"      => (int) $q->DENDA,
                                "Total"      =>  (int) $q->TOTAL,
                                "Pengesahan" =>  $res,
                                "Status"     =>  array(
                                    "IsError"      => "False",
                                    "ResponseCode" => '00',
                                    "ErrorDesc"    => "Sukses"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            $content = json_encode($data);
                            $nm_f = $q->KODE_BILING;
                            $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_00')");
                            // log bphtb
                            //$db2->query("call P_INSERT_LOG('$nm_f','$content','rc_kb_00')");
                        }
                    }
                }
            } else {
                // data tidak di temukan
                $data = array();
                $data = array(

                    "Status"        =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        }
    }

    public function reversal()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            /*if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
                throw new Exception('Request method must be POST!');
            }*/

            //Make sure that the content type of the POST request has been set to application/json
            /*$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
            if(strcasecmp($contentType, 'application/json') != 0){
                throw new Exception('Content type must be: application/json');
            }*/

            //Receive the RAW post data.
            $content     = file_get_contents("php://input");
            $decoded     = json_decode($content, true);

            $KODE_BILING = $decoded["Nop"];
            $MASA_PAJAK  = (int) $decoded["Masa"];
            $TAHUN_PAJAK = $decoded["Tahun"];
            $TAGIHAN     = (int) $decoded["Pokok"];
            $DENDA       = (int) $decoded["Denda"];
            $TOTAL       = (int) $decoded["Total"];
            $ALASAN      = '';


            $q = $this->db->query("SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, NO_SK, TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, REK_BANK, 
                    CEIL(TAGIHAN) TAGIHAN,
                    CEIL(DENDA) DENDA,
                    CEIL(POTONGAN)POTONGAN,
                    CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                    CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                    STATUS, KODE_PENGESAHAN,'1' jenis_pajak
            FROM TAGIHAN
                                WHERE
                                    KODE_BILING='$KODE_BILING' AND
                                    MASA_PAJAK='$MASA_PAJAK' AND
                                    TAHUN_PAJAK = '$TAHUN_PAJAK'
                                    union all
                                                        SELECT a.ID_BILLING KODE_BILING,NAMA_WP,ALAMAT_WP,SUBSTR(a.ID_BILLING,10,2) MASA_PAJAK, '20'||SUBSTR(a.ID_BILLING,8,2) TAHUN_PAJAK,NULL NO_SK,'3112'||TO_CHAR(SYSDATE,'yyyy') JATUH_TEMPO, KODE_REK REF_BANK,
                                BPHTB TAGIHAN, a.DENDA,0 POTONGAN, TOTAL PAJAK_TERUTANG,TOTAL, CASE WHEN STATUS_BAYAR = 1 THEN '1' ELSE '0' END STATUS,ntpd kode_pengesahan,'2' jenis_pajak
                                FROM TAGIHAN_SSPD@TOBPHTB a
                                left join bphtb_pembayaran@tobphtb b on a.id_billing=b.id_billing 
                                WHERE a.ID_BILLING='$KODE_BILING'")->row();
            $data = array();
            if (empty($q) || $q->STATUS == 0) {
                // data tidak ditemukan
                $data = array(

                    "Status"     =>  array(
                        "IsError"       => "True",
                        "ResponseCode"  => '34',
                        "ErrorDesc"     => "Data reversal tidak ditemukan"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            } else {
                // ada data
                if (((int) $q->PAJAK_TERUTANG)  != $TAGIHAN || ((int) $q->DENDA) != $DENDA  || ((int) $q->TOTAL) != $TOTAL) {
                    $data = array(
                        "Status"     =>  array(
                            "IsError"       => "True",
                            "ResponseCode"  => '14',
                            "ErrorDesc"     => "Jumlah tagihan yang dibayarkan tidak sesuai"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    if ($q->JENIS_PAJAK == '1') {
                        // pdrd
                        $query = $this->db->query("select keterangan,kode_biling from SPTPD_TAGIHAN where kode_biling='$KODE_BILING'")->row();
                        $data['KETERANGAN']     = $query->KETERANGAN;
                        $data['KODE_BILING']    = $KODE_BILING;
                        $data['MASA_PAJAK']     = $MASA_PAJAK;
                        $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                        $data['TAGIHAN']        = (int) str_replace('.', '', $TAGIHAN);
                        $data['DENDA']          = (int) str_replace('.', '', $DENDA);
                        $data['TOTAL']          = (int) str_replace('.', '', $TOTAL);
                        $data['ALASAN']         = $ALASAN;
                        $res = $this->Mbayar->reversal($data);
                    } else {
                        // bphtb
                        $data['KETERANGAN']     = 'STPD_BPHTB';
                        $data['KODE_BILING']    = $KODE_BILING;
                        $data['MASA_PAJAK']     = $MASA_PAJAK;
                        $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                        $data['TAGIHAN']        = (int) str_replace('.', '', $TAGIHAN);
                        $data['DENDA']          = (int) str_replace('.', '', $DENDA);
                        $data['TOTAL']          = (int) str_replace('.', '', $TOTAL);
                        $data['ALASAN']         = $ALASAN;
                        $res = $this->Mbayar->reversalbphtb($data);
                    }

                    $data = array();

                    header('Content-Type: application/json');
                    // echo json_encode($res);
                    // die();
                    if ($res == 'true') {
                        $data = array(
                            "Nop"        =>  $q->KODE_BILING,
                            "Masa"       =>  sprintf("%02d", $q->MASA_PAJAK),
                            "Tahun"      =>  $q->TAHUN_PAJAK,
                            "JatuhTempo" =>  $q->JATUH_TEMPO,
                            "KodeRek"    =>  $q->REK_BANK,
                            "Pokok"      =>  (int) $q->PAJAK_TERUTANG,
                            "Denda"      => (int) $q->DENDA,
                            "Total"      =>  (int) $q->TOTAL,
                            "Status"     =>  array(
                                "IsError"      => "False",
                                "ResponseCode" => '00',
                                "ErrorDesc"    => "Sukses"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    } else {
                        $data = array(
                            "Nop"    =>  $KODE_BILING,
                            "Status" =>  array(
                                "IsError"      => "True",
                                "ResponseCode" => '99',
                                "ErrorDesc"    => "System Error"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
                }
            }
        }
    }
    public function inquiry_aplikasi()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $KODE_BILING = $this->input->get('Nop', TRUE);
            $MASA_PAJAK  =  (int) $this->input->get('MasaPajak', TRUE);
            $TAHUN_PAJAK = $this->input->get('TahunPajak', TRUE);
            $POKOK       = $this->input->get('Pokok', TRUE);
            $query = $this->db->query(
                "SELECT
                KODE_BILING,
                NAMA_WP,
                ALAMAT_WP,
                MASA_PAJAK,
                TAHUN_PAJAK,
                NO_SK,NIP_INSERT,
                TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO,
                KODE_REK AS REK_BANK,
                CEIL(TAGIHAN) TAGIHAN,
                CEIL(DENDA) DENDA,
                CEIL(POTONGAN)POTONGAN,
                CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                STATUS
            FROM TAGIHAN
            WHERE KODE_BILING='$KODE_BILING' "
            )->row();

            /*
           AND TAGIHAN-POTONGAN='$POKOK'*/
            $data = array();
            if (!empty($query->KODE_BILING)) {

                if ($query->STATUS == 1) {
                    $data = array(
                        "Status"        => array(
                            "IsError" => "True",
                            "ResponseCode" => '13',
                            "ErrorDesc" => "Data tagihan telah lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data, 200);
                } else {

                    $data = array(
                        "Nop"        =>  $query->KODE_BILING,
                        "Nip_insert" =>  $query->NIP_INSERT,
                        "Nama"       =>  $query->NAMA_WP,
                        "Alamat"     =>  $query->ALAMAT_WP,
                        "Masa"       =>  sprintf("%02d", $query->MASA_PAJAK),
                        "Tahun"      => $query->TAHUN_PAJAK,
                        "NoSk"       => $query->NO_SK <> '' ? $query->NO_SK : '-',
                        "JatuhTempo" => $query->JATUH_TEMPO,
                        "KodeRek"    => $query->REK_BANK,
                        "Pokok"      => $query->PAJAK_TERUTANG,
                        "Denda"      => $query->DENDA,
                        "Total"      => $query->TOTAL,
                        "Status"        => array(
                            "IsError" => "False",
                            "ResponseCode" => '00',
                            "ErrorDesc" => "Success"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data, 200);
                }
            } else {
                $data = array(
                    // "Nop"   =>  $KODE_BILING,
                    "Status"        =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );

                header('Content-Type: application/json');
                echo json_encode($data);
            }
        }
    }
    public function payment_aplikasi()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $KODE_BILING = $this->input->post('Nop');
            $MASA_PAJAK  = (int) $this->input->post('Masa');
            $TAHUN_PAJAK = $this->input->post('Tahun');
            $TAGIHAN     = (float) $this->input->post('Pokok');
            $DENDA       = (float) $this->input->post('Denda');
            $TOTAL       = (float) $this->input->post('Total');
            $KODE_BANK   = $this->input->post('bank');
            $DATE_BAYAR  = $this->input->post('tglbayar');
            $q = $this->db->query("SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, NO_SK, TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, REK_BANK, 
            CEIL(TAGIHAN) TAGIHAN,
            CEIL(DENDA) DENDA,
            CEIL(POTONGAN)POTONGAN,
            CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
            CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
            STATUS, KODE_PENGESAHAN
            
            FROM TAGIHAN
                                WHERE
                                    KODE_BILING='$KODE_BILING' AND
                                    MASA_PAJAK='$MASA_PAJAK' AND
                                    TAHUN_PAJAK = '$TAHUN_PAJAK'")->row();

            if (!empty($q->KODE_BILING)) {
                if ($q->STATUS == 1) {
                    // tagihan telah lunas
                    $data = array();
                    $data = array(
                        "Status"        =>  array(
                            "IsError" => "True",
                            "ResponseCode" => '13',
                            "ErrorDesc" => "Data Tagihan Telah Lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    // belum lunas
                    if (((float) $q->PAJAK_TERUTANG)  != $TAGIHAN || ((float) $q->DENDA) != $DENDA  || ((float) $q->TOTAL) != $TOTAL) {
                        // tagihan tidak sesuai
                        $data = array();
                        $data = array(
                            "Status"     =>  array(
                                "IsError"      => "True",
                                "ResponseCode" => '14',
                                "ErrorDesc"    => "Jumlah tagihan yang dibayarkan tidak sesuai"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    } else {
                        // sukses
                        $query = $this->db->query("select keterangan,kode_biling from SPTPD_TAGIHAN where kode_biling='$KODE_BILING'")->row();
                        $data['KETERANGAN']     = $query->KETERANGAN;
                        $data['KODE_BILING']    = $KODE_BILING;
                        $data['MASA_PAJAK']     = $MASA_PAJAK;
                        $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                        $data['TAGIHAN']        = str_replace('.', '', $TAGIHAN);
                        $data['DENDA']          = str_replace('.', '', $DENDA);
                        $data['TOTAL']          = str_replace('.', '', $TOTAL);
                        $data['KODE_BANK']      = $KODE_BANK;
                        $data['TGL_BAYAR']      = $DATE_BAYAR;
                        $res = $this->Mbayar->addbayar_aplikasi($data);
                        $data = array();
                        if ($res == 'false') {
                            $data = array(

                                "Status" =>  array(
                                    "IsError" => "True",
                                    "ResponseCode" => '99',
                                    "ErrorDesc" => "System Error"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                        } else {
                            $data = array(
                                "Nop"        =>  $q->KODE_BILING,
                                "Masa"       =>  sprintf("%02d", $q->MASA_PAJAK),
                                "Tahun"      =>  $q->TAHUN_PAJAK,
                                "JatuhTempo" =>  $q->JATUH_TEMPO,
                                "KodeRek"    =>  $q->REK_BANK,
                                "Pokok"      =>  (float) $q->PAJAK_TERUTANG,
                                "Denda"      => (float) $q->DENDA,
                                "Total"      =>  (float) $q->TOTAL,
                                "Pengesahan" =>  $res,
                                "Status"     =>  array(
                                    "IsError"      => "False",
                                    "ResponseCode" => '00',
                                    "ErrorDesc"    => "Sukses"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                        }
                    }
                }
            } else {
                // data tidak di temukan
                $data = array();
                $data = array(
                    "Status"        =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        }
    }

    public function reversal_aplikasi()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $content     = file_get_contents("php://input");
            $decoded     = json_decode($content, true);


            $KODE_BILING = $this->input->post('Nop');
            $MASA_PAJAK  = (int) $this->input->post('Masa');
            $TAHUN_PAJAK = $this->input->post('Tahun');
            $TAGIHAN     = (int) $this->input->post('Pokok');
            $DENDA       = (int) $this->input->post('Denda');
            $TOTAL       = (int) $this->input->post('Total');
            $ALASAN      = $this->input->post('Alasan');


            $q = $this->db->query("SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, NO_SK, TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, REK_BANK, 
                CEIL(TAGIHAN) TAGIHAN,
                CEIL(DENDA) DENDA,
                CEIL(POTONGAN)POTONGAN,
                CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                STATUS, KODE_PENGESAHAN
                FROM TAGIHAN
                WHERE KODE_BILING='$KODE_BILING' AND
                MASA_PAJAK='$MASA_PAJAK' AND
                TAHUN_PAJAK = '$TAHUN_PAJAK'")->row();
            $data = array();
            if (empty($q) || $q->STATUS == 0) {
                // data tidak ditemukan
                $data = array(

                    "Status"     =>  array(
                        "IsError"       => "True",
                        "ResponseCode"  => '34',
                        "ErrorDesc"     => "Data reversal tidak ditemukan"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            } else {
                // ada data
                if (((int) $q->PAJAK_TERUTANG)  != $TAGIHAN || ((int) $q->DENDA) != $DENDA  || ((int) $q->TOTAL) != $TOTAL) {
                    $data = array(
                        /*"Nop"        =>  $q->KODE_BILING,
                    "Masa"       =>  sprintf("%02d",$q->MASA_PAJAK),
                    "Tahun"      =>  $q->TAHUN_PAJAK,
                    "JatuhTempo" =>  $q->JATUH_TEMPO,
                    "KodeRek"    =>  $q->REK_BANK,
                    "Pokok"      => (int)$TAGIHAN,
                    "Denda"      => (int)$DENDA,
                    "Total"      => (int)$TOTAL,*/
                        "Status"     =>  array(
                            "IsError"       => "True",
                            "ResponseCode"  => '14',
                            "ErrorDesc"     => "Jumlah tagihan yang dibayarkan tidak sesuai"

                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    $query = $this->db->query("select keterangan,kode_biling from SPTPD_TAGIHAN where kode_biling='$KODE_BILING'")->row();
                    $data['KETERANGAN']     = $query->KETERANGAN;
                    $data['KODE_BILING']    = $KODE_BILING;
                    $data['MASA_PAJAK']     = $MASA_PAJAK;
                    $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                    $data['TAGIHAN']        = str_replace('.', '', $TAGIHAN);
                    $data['DENDA']          = str_replace('.', '', $DENDA);
                    $data['TOTAL']          = str_replace('.', '', $TOTAL);
                    $data['ALASAN']         = $ALASAN;
                    $res = $this->Mbayar->reversal($data);
                    $data = array();
                    if ($res == 'true') {
                        $data = array(
                            "Nop"        =>  $q->KODE_BILING,
                            "Masa"       =>  sprintf("%02d", $q->MASA_PAJAK),
                            "Tahun"      =>  $q->TAHUN_PAJAK,
                            "JatuhTempo" =>  $q->JATUH_TEMPO,
                            "KodeRek"    =>  $q->REK_BANK,
                            "Pokok"      =>  (int) $q->PAJAK_TERUTANG,
                            "Denda"      => (int) $q->DENDA,
                            "Total"      =>  (int) $q->TOTAL,
                            "Status"     =>  array(
                                "IsError"      => "False",
                                "ResponseCode" => '00',
                                "ErrorDesc"    => "Sukses"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    } else {
                        $data = array(
                            "Nop"    =>  $KODE_BILING,
                            "Status" =>  array(
                                "IsError"      => "True",
                                "ResponseCode" => '99',
                                "ErrorDesc"    => "System Error"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
                }
            }
        }
    }

    public function pembayaran()
    {
        $data = array(
            'title'              => 'Pembayaran',
            'action'             => site_url('webservice/create_action'),
            'script'             => 'webservice/script',
        );
        $this->template->load('layout', 'webservice/view_form', $data);
    }


    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    public function get_va()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $VIRTUAL_ACCOUNT = $this->input->get('va', TRUE);
            $data = array(

                "Status"        =>  array(
                    "IsError" => "True",
                    "ResponseCode" => '10',
                    "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                )
            );

            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    public function posting()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $content = file_get_contents("php://input");
            $decoded = json_decode($content, true);
            $v_va = $decoded["VirtualAccount"];

            $this->db->query("call P_INSERT_LOG('$v_va','$content','post_payment')");
            $filename = $v_va . ".txt"; //the name of our file.
            $strlength = strlen($content); //gets the length of our $content string.
            $create = fopen('log_va/post/' . $filename, "w");
            if ($create == false) {
                //do debugging or logging here
            } else {
                fwrite($create, $content);
                fclose($create);
            }   ///uses fopen to create our file.

            $VIRTUAL_ACCOUNT = $decoded["VirtualAccount"];
            $TAGIHAN     = (int) $decoded["Amount"];
            $REFERENCE  = $decoded["Reference"];
            $TANGGAL = $decoded["Tanggal"];
            //$KODE_BANK   = '114';

            $q = $this->db->query("SELECT KODE_BILING,NPWPD,NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, NO_SK, TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, REK_BANK, 
            CEIL(TAGIHAN) TAGIHAN,
            CEIL(DENDA) DENDA,
            CEIL(POTONGAN)POTONGAN,
            CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
            CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
            STATUS, KODE_PENGESAHAN,VIRTUAL_ACCOUNT
            
            FROM TAGIHAN
                                WHERE
                                    VIRTUAL_ACCOUNT='$VIRTUAL_ACCOUNT'")->row();


            if (!empty($q->VIRTUAL_ACCOUNT)) {
                if ($q->STATUS == 1) {
                    // tagihan telah lunas
                    $data = array();
                    $data = array(

                        "Status"        =>  array(
                            "IsError" => "True",
                            "ResponseCode" => '13',
                            "ErrorDesc" => "Data Tagihan Telah Lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    $content = json_encode($data);
                    $nm_f = $q->VIRTUAL_ACCOUNT;
                    $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_13')");
                    $filename = '13' . $nm_f . ".txt"; //the name of our file.
                    $strlength = strlen($content); //gets the length of our $content string.
                    $create = fopen('log_va/rc/' . $filename, "w");
                    if ($create == false) {
                        //do debugging or logging here
                    } else {
                        fwrite($create, $content);
                        fclose($create);
                    }
                } else {
                    // belum lunas
                    if (((int) $q->TOTAL) != $TAGIHAN) {
                        // tagihan tidak sesuai
                        $data = array();
                        $data = array(
                            "Status"     =>  array(
                                "IsError"      => "True",
                                "ResponseCode" => '14',
                                "ErrorDesc"    => "Jumlah tagihan yang dibayarkan tidak sesuai"
                            )
                        );
                        header('Content-Type: application/json');
                        echo json_encode($data);
                        $content = json_encode($data);
                        $nm_f = $q->VIRTUAL_ACCOUNT;
                        $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_14')");
                        $filename = '14' . $nm_f . ".txt"; //the name of our file.
                        $strlength = strlen($content); //gets the length of our $content string.
                        $create = fopen('log_va/rc/' . $filename, "w");
                        if ($create == false) {
                            //do debugging or logging here
                        } else {
                            fwrite($create, $content);
                            fclose($create);
                        }
                    } else {
                        // sukses                  
                        $query = $this->db->query("select keterangan,no_va from SPTPD_TAGIHAN where no_va='$VIRTUAL_ACCOUNT'")->row();
                        $data['KETERANGAN']     = $query->KETERANGAN;
                        $data['VIRTUAL_ACCOUNT'] = $VIRTUAL_ACCOUNT;
                        $data['TAGIHAN']        = $TAGIHAN;
                        $data['REFERENCE']      = $REFERENCE;
                        $data['TGL_BAYAR']      = $TANGGAL;
                        $res = $this->Mbayar->addbayar_va($data);
                        $data = array();
                        if ($res == 'false') {
                            $data = array(

                                "Status" =>  array(
                                    "IsError" => "True",
                                    "ResponseCode" => '99',
                                    "ErrorDesc" => "System Error"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            $content = json_encode($data);
                            $nm_f = $q->VIRTUAL_ACCOUNT;
                            $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_99')");
                            $filename = '99' . $nm_f . ".txt"; //the name of our file.
                            $strlength = strlen($content); //gets the length of our $content string.
                            $create = fopen('log_va/rc/' . $filename, "w");
                            if ($create == false) {
                                //do debugging or logging here
                            } else {
                                fwrite($create, $content);
                                fclose($create);
                            }
                        } else {
                            $cek_tujuan = $this->db->query("select GETEMAIL('$q->NPWPD')tujuan from dual")->row();
                            $tujuan = $cek_tujuan->TUJUAN; //'adypabbsi@gmail.com';
                            //$tujuan='fadeliss@gmail.com';
                            $ci                  = get_instance();
                            $ci->load->library('email');
                            $config['protocol']  = "smtp";
                            $config['smtp_host'] = "ssl://smtp.gmail.com";
                            $config['smtp_port'] = "465";
                            $config['smtp_user'] = "malangbapendakab@gmail.com";
                            $config['smtp_pass'] = "B4p3nd4km";
                            $config['charset']   = "utf-8";
                            $config['mailtype']  = "html";
                            $config['newline']   = "\r\n";
                            $ci->email->initialize($config);
                            $ci->email->from('Bapenda Kab. Malang', 'PDRD.KAB.MALANG');
                            $list                = array($tujuan);
                            $ci->email->to($list);
                            $this->email->reply_to('no-reply@account-gmail.com', 'no replay');
                            $ci->email->subject('Pembayaran Tagihan');
                            $messageFromHtml = $this->messageHtml($q->VIRTUAL_ACCOUNT, $q->NAMA_WP, $TAGIHAN, $TANGGAL);
                            $ci->email->message($messageFromHtml);
                            //$ci->email->message('Pembayaran Tagihan Nomor virtual Account '.$q->VIRTUAL_ACCOUNT.' dengan nominal Rp. '.number_format($TAGIHAN,0,",",".").' Berhasil');
                            $ci->email->send();
                            $data = array(
                                "VirtualAccount"        =>  $q->VIRTUAL_ACCOUNT,
                                "Amount"      =>  (int) $TAGIHAN,
                                "Tanggal"      => $TANGGAL,
                                "Status"     =>  array(
                                    "IsError"      => "False",
                                    "ResponseCode" => '00',
                                    "ErrorDesc"    => "Sukses"
                                )
                            );
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            $content = json_encode($data);
                            $nm_f = $q->VIRTUAL_ACCOUNT;
                            $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_00')");
                            $filename = '00' . $nm_f . ".txt"; //the name of our file.
                            $strlength = strlen($content); //gets the length of our $content string.
                            $create = fopen('log_va/rc/' . $filename, "w");
                            if ($create == false) {
                                //do debugging or logging here
                            } else {
                                fwrite($create, $content);
                                fclose($create);
                            }
                        }
                    }
                }
            } else {
                // data tidak di temukan
                $data = array();
                $data = array(
                    /*"Nop"           => $KODE_BILING,
                "Masa"          => sprintf("%02d",$MASA_PAJAK),
                "Tahun"         => $TAHUN_PAJAK,
                "Pokok"         => $TAGIHAN,
                "Denda"         => (int)$DENDA,
                "Total"         => $TOTAL,*/
                    "Status"        =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
                $content = json_encode($data);
                $nm_f = $q->VIRTUAL_ACCOUNT;
                $this->db->query("call P_INSERT_LOG('$nm_f','$content','rc_10')");
                $filename = '10' . $nm_f . ".txt"; //the name of our file.
                $strlength = strlen($content); //gets the length of our $content string.
                $create = fopen('log_va/rc/' . $filename, "w");
                if ($create == false) {
                    //do debugging or logging here
                } else {
                    fwrite($create, $content);
                    fclose($create);
                }
            }
        }
    }

    function messageHtml($virtual = "", $nama = "", $tagihan = "", $tanggal = "")
    {
        return
            '<table style="border-collapse:collapse;border-spacing:0;margin:0;padding:0" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                      <tbody>
                        <tr>
                          <td style="">
                            <table style="border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0" width="600" cellspacing="0" cellpadding="0" border="0" align="center">
                              <tbody>
                                <tr>
                                  <td bgcolor="#ffffff">
                                    <table style="border-collapse:collapse;border-spacing:0;margin:0;padding:0" width="100%" align="left">
                                      <tbody>
                                        <tr>
                                          <table style="border-collapse:collapse;border-spacing:0;margin:0;padding:0" width="100%" align="left">
                                            <tbody>
                                              <tr>
                                                <td style="">
                                                  <div>
                                                      <h1 style="font-family:Arial;color:#808080;line-height:80%;font-size:20px;display:block;font-weight:bold;border-bottom-width:1px;border-bottom-color:#e8e8e8;border-bottom-style:solid;margin:0 0 20px;padding:0" align="left">
                                                        Pembayaran ' . $virtual . ' Sukses<br>&nbsp;
                                                      </h1>
                                                  </div>
                                                  <div>
                                                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">Hai ' . $nama . ' ,</p>
                                                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">Selamat! Pembayaran untuk tagihan ' . $virtual . ' telah berhasil. <br>
                                                        Terima kasih Telah Membayar Pajak</p>
                                                  </div>
                                                  <div>
                                                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0 0 10px;padding:0" align="center">&nbsp;</p>
                                                  </div>
                                                    <div>
                                                      <p style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0 0 8px">Berikut adalah penjelasan tagihan pembayaran:</p>
                                                      <table style="border-bottom-style:none;border-collapse:collapse;border-spacing:0;font-size:12px;border-bottom-color:#cccccc;border-bottom-width:1px;margin:0 0 25px;padding:0" width="100%" cellspacing="0" cellpadding="5" border="0" bgcolor="#FFFFFF">
                                                        <tbody>
                                                          <tr>
                                                            <th colspan="3" style="border-bottom-style:none;color:#ffffff;padding-left:10px;padding-right:10px" bgcolor="#900135">
                                                              <h2 style="font-family:&quot;Arial&quot;,sans-serif;color:#ffffff;line-height:1.5;font-size:14px;margin:0;padding:5px 0">Tagihan 
                                                              </h2>
                                                            </th>
                                                          </tr>
                                                          <tr>
                                                            <th colspan="3" style="border-bottom-style:none;color:#ffffff;padding-left:10px;padding-right:10px" bgcolor="#900135"></th>
                                                          </tr>
                                                          <tr>
                                                            <td class="" color="#ffffff !important" colspan="2" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;font-size:11px;margin:0;padding:5px 10px" width="40%" valign="top" bgcolor="#F0F0F0" align="left">
                                                            <strong style="color:#555;font-size:14px">Keterangan</strong>
                                                            </td>
                                                            <td class="m_-8029522690150426518headingList" color="#ffffff !important" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;font-size:11px;margin:0;padding:5px 10px" width="60%" valign="top" bgcolor="#F0F0F0" align="right">
                                                            <strong style="color:#555;font-size:14px">Rincian</strong>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" valign="top" bgcolor="#FFFFFF" align="left">
                                                            Tagihan
                                                            </td>
                                                            <td style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" width="100%" valign="top" bgcolor="#FFFFFF" align="right">
                                                            <span class=" " style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">Rp </span><span class="" style="font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;font-size:14px;margin:0;padding:0">' . number_format($tagihan, '0', '', '.') . '</span>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" valign="top" bgcolor="#FFFFFF" align="left">
                                                            Tanggal Lunas
                                                            </td>
                                                            <td style="border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-color:#cccccc;border-bottom-width:1px;border-bottom-style:solid;margin:0;padding:5px 10px" width="100%" valign="top" bgcolor="#FFFFFF" align="right">' . $tanggal . '</span></td>
                                                          </tr>
                                                          <tr class="">
                                                            <td class="m_-8029522690150426518grand-total-title" colspan="2" style="border-bottom-color:#ffffff!important;border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-width:1px!important;border-bottom-style:solid!important;margin:0;padding:5px 10px" valign="top" bgcolor="#f0f0f0" align="left">
                                                            <strong style="color:#555;font-size:14px">TOTAL PEMBAYARAN</strong>
                                                            </td>
                                                            <td class="m_-8029522690150426518grand-total-amount" style="border-bottom-color:#ffffff!important;border-collapse:collapse;border-spacing:0;font-family:&quot;Arial&quot;,sans-serif;color:#555;line-height:1.5;border-bottom-width:1px!important;border-bottom-style:solid!important;border-top-color:#ffffff;margin:0;padding:5px 10px" width="25%" valign="top" bgcolor="#67a348" align="right">
                                                            <strong style="color:#555;font-size:14px"><span tyle="font-family:&quot;Arial&quot;,sans-serif;color:#ffffff!important;line-height:1.5;font-size:14px;margin:0;padding:0">Rp ' . number_format($tagihan, '0', '', '.') . '</span></strong>
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>

                    </td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td style="">
                    <table id="m_-8029522690150426518footerSeparator" style="border-collapse:collapse;border-spacing:0;table-layout:fixed;border-top-width:2px;border-top-style:solid;border-top-color:#ccc;margin:0;padding:0" width="100%">
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td style="">
                    <table style="border-collapse:collapse;border-spacing:0;font-size:10px;text-align:center;margin:0;padding:0" width="100%">
                    <tbody>
                    <tr>
                    <td style=" 10px">
                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#999;line-height:1.5;margin:0;padding:0">
                    Copyright © 2019 Kab. Malang All Rights Reserved
                    </p>
                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#999;line-height:1.5;margin:0;padding:0">
                    JL. Raden Panji No. 158, Telp. (0341) 390683 Kepanjen 
                    </p>
                    <p style="font-family:&quot;Arial&quot;,sans-serif;color:#999;line-height:1.5;margin:0;padding:0">
                    Harap jangan membalas e-mail ini, karena e-mail ini dikirimkan secara otomatis oleh sistem.
                    </p>
                    </td>
                    </tr>
                    <tr>
                    <td style="" height="15"></td>
                    </tr>
                    </tbody></table>

                    </td>
                    </tr>
                    </tbody></table>
                    </td>
                    </tr>
                    </tbody></table>';
    }
}
