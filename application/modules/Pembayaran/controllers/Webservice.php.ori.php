<?php

class Webservice extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mbayar');
    }
    public function tes()
    {
        echo "tes";
    }
    public function inquiry()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $KODE_BILING= $this->input->get('KODE_BILING',TRUE);
        $query=$this->db->query(
            "SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, OBJEK_PAJAK, TAGIHAN,DENDA,POTONGAN, JENIS_PAJAK, STATUS, TABEL AS KETERANGAN, TO_CHAR (TGL_PENETAPAN, 'dd/mm/yyyy HH24: MI: SS') TGL_PENETAPAN,TO_CHAR (DATE_BAYAR, 'dd/mm/yyyy HH24: MI: SS') DATE_BAYAR,TO_CHAR(JATUH_TEMPO, 'dd/mm/yyyy HH24: MI: SS') JATUH_TEMPO
            FROM TAGIHAN A LEFT JOIN JENIS_PAJAK B ON A.JENIS_PAJAK=B.ID_INC
            WHERE KODE_BILING='$KODE_BILING'"
        )->row(); 
            $data = array();
            if (!empty($query->KODE_BILING)) {
                $data = array(
                    "KODE_BILING"   =>  $query->KODE_BILING,
                    "NAMA_WP"       =>  $query->NAMA_WP,
                    "ALAMAT_WP"     =>  $query->ALAMAT_WP,
                    "OBJEK_PAJAK"     =>  $query->OBJEK_PAJAK,
                    "JENIS_PAJAK"    =>  $query->JENIS_PAJAK,
                    "MASA_PAJAK"    =>  $query->MASA_PAJAK,
                    "TAHUN_PAJAK"   => $query->TAHUN_PAJAK,
                    "TAGIHAN"       =>  $query->TAGIHAN,
                    "DENDA"      => $query->DENDA,
                    "POTONGAN"      => $query->POTONGAN,
                    "KETERANGAN"       =>  $query->KETERANGAN,
                    "STATUS_BAYAR"      => $query->STATUS,
                    "DATE_BAYAR"      => $query->DATE_BAYAR,
                    "TGL_PENETAPAN"      => $query->TGL_PENETAPAN,
                    "JATUH_TEMPO"      => $query->JATUH_TEMPO,
                    "STATUS"        => array(
                        "IsError"=>"False",
                        "ResponseCode"=>'00',
                        "ErrorDesc" => "Success"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data,200);
            } else {
                $data = array(
                    "KODE_BILING"   =>  $KODE_BILING,
                    "STATUS"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            }

            
       }

    }

public function payment()
{
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $KODE_BILING = $this->input->post('KODE_BILING',TRUE);
        $q=$this->db->query(
            "SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, OBJEK_PAJAK, TAGIHAN, JENIS_PAJAK, STATUS, TABEL AS KETERANGAN, KODE_PENGESAHAN, KODE_REK
            FROM TAGIHAN A LEFT JOIN JENIS_PAJAK B ON A.JENIS_PAJAK=B.ID_INC
            WHERE KODE_BILING='$KODE_BILING'"
        )->row();    

        if (!empty($q->KODE_BILING)) {
            if ($q->STATUS==1) {
                $data = array();
                $data = array(
                    "KODE_BILING"   =>  $KODE_BILING,
                    "STATUS"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'13',
                        "ErrorDesc" => "Data Tagihan Telah Lunas"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            } else {
                $data['KODE_BILING']=$this->input->post('KODE_BILING');
                $data['NAMA_WP']=$this->input->post('NAMA_WP');
                $data['ALAMAT_WP']=$this->input->post('ALAMAT_WP');
                $data['OBJEK_PAJAK']=$this->input->post('OBJEK_PAJAK');
                $data['NAMA_USAHA']=$this->input->post('NAMA_USAHA');
                $data['MASA_PAJAK']=$this->input->post('MASA_PAJAK');
                $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
                $data['JENIS_PAJAK']=$this->input->post('JENIS_PAJAK');
                $data['PAJAK_TERUTANG']=str_replace('.','',$this->input->post('TAGIHAN'));
                $data['KETERANGAN']=$this->input->post('KETERANGAN');
                $data['TGL_BAYAR']=$this->input->post('TGL_BAYAR');
                $data['JUMLAH_BAYAR']= str_replace('.','',$this->input->post('JUMLAH_BAYAR'));
                if ($q->TAGIHAN!=str_replace('.','',$this->input->post('JUMLAH_BAYAR'))) {
                    $data = array();
                    $data = array(
                        "KODE_BILING"   =>  $KODE_BILING,
                        "STATUS"        =>  array(
                            "IsError"=>"True",
                            "ResponseCode"=>'14',
                            "ErrorDesc" => "Jumlah tagihan yang dibayarkan tidak sesuai"
                        )
                    );                     
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    $res=$this->Mbayar->addbayar($data); 
                    $data = array();                 
                    if($res=='true'){      
                        $data = array(
                            "KODE_BILING"   =>  $q->KODE_BILING,
                            "NAMA_WP"       =>  $q->NAMA_WP,
                            "ALAMAT_WP"     =>  $q->ALAMAT_WP,
                            "OBJEK_PAJAK"     =>  $q->OBJEK_PAJAK,
                            "JENIS_PAJAK"    =>  $q->JENIS_PAJAK,
                            "MASA_PAJAK"    =>  $q->MASA_PAJAK,
                            "TAHUN_PAJAK"   => $q->TAHUN_PAJAK,
                            "TAGIHAN"       =>  $q->TAGIHAN,
                            "KETERANGAN"       =>  $q->KETERANGAN,
                            "KODEREK"       =>  $q->KODE_REK,
                            "KODE_PENGESAHAN"       =>  $q->KODE_PENGESAHAN,
                            "STATUS"        =>  array(
                                "IsError"=>"False",
                                "ResponseCode"=>'00',
                                "ErrorDesc" => "Success"
                            )
                        ); 
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }else{
                        $data = array(
                            "KODE_BILING"   =>  $KODE_BILING,
                            "STATUS"        =>  array(
                                "IsError"=>"True",
                                "ResponseCode"=>'99',
                                "ErrorDesc" => "System Error"
                            )
                        );                     
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    } 
                } 
            }
        }else{
            $data = array();
            $data = array(
                "KODE_BILING"   =>  $KODE_BILING,
                "STATUS"        =>  array(
                    "IsError"=>"True",
                    "ResponseCode"=>'10',
                    "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                )
            );                     
            header('Content-Type: application/json');
            echo json_encode($data);
        }
        
    } 
}

    public function reversal()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $KODE_BILING = $this->input->post('KODE_BILING',TRUE);
            $query=$this->db->query(
                "SELECT KODE_BILING, NAMA_WP, ALAMAT_WP, MASA_PAJAK, TAHUN_PAJAK, OBJEK_PAJAK, TAGIHAN, JENIS_PAJAK, STATUS, TABEL AS KETERANGAN, TO_CHAR (TGL_PENETAPAN, 'dd/mm/yyyy HH24: MI: SS') TGL_PENETAPAN,TO_CHAR (DATE_BAYAR, 'dd/mm/yyyy HH24: MI: SS') DATE_BAYAR,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') JATUH_TEMPO, KODE_REK, DENDA
                FROM TAGIHAN A LEFT JOIN JENIS_PAJAK B ON A.JENIS_PAJAK=B.ID_INC
                WHERE KODE_BILING='$KODE_BILING'"
            )->row();   
            $data = array();
            if ($query->STATUS!=1) {
                $data = array(
                    "KODE_BILING"   =>  $KODE_BILING,
                    "STATUS"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'34',
                        "ErrorDesc" => "Data reversal tidak ditemukan"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            }  else {
                    $data['KODE_BILING']=$this->input->post('KODE_BILING');
                    $data['MASA_PAJAK']=$this->input->post('MASA_PAJAK');
                    $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
                    $data['POKOK']=str_replace('.','',$this->input->post('POKOK'));
                    $data['DENDA']=str_replace('.','',$this->input->post('DENDA'));
                    $data['TOTAL']=str_replace('.','',$this->input->post('TOTAL'));  
                    $data['KETERANGAN']=$query->KETERANGAN;        
                    $res=$this->Mbayar->reversal($data); 
                    $data = array();                 
                    if($res=='true'){      
                        $data = array(
                            "KODE_BILING"   =>  $query->KODE_BILING,
                            "MASA_PAJAK"       =>  $query->MASA_PAJAK,
                            "TAHUN_PAJAK"     =>  $query->TAHUN_PAJAK,
                            "JATUH_TEMPO"     =>  $query->JATUH_TEMPO,
                            "KODE_REK"    =>  $query->KODE_REK,
                            "POKOK"       =>  $query->TAGIHAN-$query->DENDA,
                            "DENDA"       =>  $query->DENDA,
                            "TOTAL"       =>  $query->TAGIHAN,
                            "STATUS"        =>  array(
                                "IsError"=>"False",
                                "ResponseCode"=>'36',
                                "ErrorDesc" => "Data reversal telah dibatalkan"
                            )
                        ); 
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }else{
                        $data = array(
                            "KODE_BILING"   =>  $KODE_BILING,
                            "STATUS"        =>  array(
                                "IsError"=>"True",
                                "ResponseCode"=>'99',
                                "ErrorDesc" => "System Error"
                            )
                        );                     
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
            }     

        }
    }
    public function updateflag()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $ID= $this->input->post('ID_BILLING',TRUE);
            $this->db->trans_begin();
            $q= $this->db
            ->where('ID_BILLING', $ID)
            ->update("BPHTB_SSPD", array(
                'STATUS_BAYAR' => 1 ,
                'STATUS_VALIDASI_DISPENDA' => 8,
            ));
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
            $result = array();

            if ($this->db->affected_rows()> 0) {
                $ee=$this->db->query("SELECT ID from bphtb_sspd where ID_BILLING='$ID'")->row();

                // sspd_log($ee->ID,'2','Bank Jatim');

                array_push($result,array(
                    "Status_Bayar"=>1,
                    "Deskripsi"=>'Sukses'
                ));
            } else {
                array_push($result,array(
                    "Status_Bayar"=>0,
                    "Deskripsi"=>'Gagal',
                ));
            }


            header('Content-Type: application/json');
            echo json_encode(array('data'=>$result));
       }
    }

    public function pembayaran()
    {
        $data = array(
      'title'              => 'Pembayaran',
      'action'             => site_url('webservice/create_action'),
            'script'             => 'webservice/script',
      );
        $this->template->load('layout','webservice/view_form',$data);
    }

}