<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembatalan extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Mbayar');
 }
 

  function index(){
        if(isset($_POST['kode_biling'])){
            $data['kode_biling']=$_POST['kode_biling'];
            $data['rk']=$this->Mbayar->getKodebiling($_POST['kode_biling']);
        }else{
            $data['kode_biling']=null;
        }

    // getKodebiling
    $this->template->load('Welcome/halaman','formpembatalan',$data);
  }
public function pembatalan()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $KODE_BILING = $this->input->get('Nop',TRUE);
            $MASA_PAJAK  =  (int)$this->input->get('MasaPajak',TRUE);
            $TAHUN_PAJAK = $this->input->get('TahunPajak',TRUE);
            $POKOK       = $this->input->get('Pokok',TRUE);
            $query=$this->db->query(
            "SELECT
                KODE_BILING,
                NAMA_WP,
                ALAMAT_WP,
                MASA_PAJAK,
                TAHUN_PAJAK,
                NO_SK,
                TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO,
                REK_BANK,
                CEIL(TAGIHAN) TAGIHAN,
                CEIL(DENDA) DENDA,
                CEIL(POTONGAN)POTONGAN,
                CEIL(TAGIHAN-POTONGAN) PAJAK_TERUTANG,
                CEIL(TAGIHAN-POTONGAN+DENDA) TOTAL,
                STATUS
            FROM TAGIHAN
            WHERE KODE_BILING='$KODE_BILING'")->row();
            $data = array();
            if (!empty($query->KODE_BILING)) {
                    if($query->STATUS==0){
                         $data = array(
                                "Nop"        =>  $query->KODE_BILING,
                                "Nama"       =>  $query->NAMA_WP,
                                "Alamat"     =>  $query->ALAMAT_WP,
                                "Masa"       =>  sprintf("%02d", $query->MASA_PAJAK),
                                "Tahun"      => $query->TAHUN_PAJAK,
                                "NoSk"       => $query->NO_SK,
                                "JatuhTempo" => $query->JATUH_TEMPO,
                                "KodeRek"    => $query->REK_BANK,
                                "Pokok"      => $query->PAJAK_TERUTANG,
                                "Denda"      => $query->DENDA,
                                "Total"      => $query->TOTAL,
                        "Status"        => array(
                            "IsError"=>"False",
                            "ResponseCode"=>'00',
                            "ErrorDesc" => "Success"
                        )
                    );
                        header('Content-Type: application/json');
                        echo json_encode($data,200);
                    }else{
                    $data = array(
                                "Nop"        =>  $query->KODE_BILING,
                                "Nama"       =>  $query->NAMA_WP,
                                "Alamat"     =>  $query->ALAMAT_WP,
                                "Masa"       =>  sprintf("%02d", $query->MASA_PAJAK),
                                "Tahun"      => $query->TAHUN_PAJAK,
                                "NoSk"       => $query->NO_SK,
                                "JatuhTempo" => $query->JATUH_TEMPO,
                                "KodeRek"    => $query->REK_BANK,
                                "Pokok"      => $query->PAJAK_TERUTANG,
                                "Denda"      => $query->DENDA,
                                "Total"      => $query->TOTAL,
                            "Status"        => array(
                                "IsError"=>"True",
                                "ResponseCode"=>'13',
                                "ErrorDesc" => "Data sudah lunas"
                            )
                        );
                   
                header('Content-Type: application/json');
                echo json_encode($data,200);
                }
            } else {
                $data = array(
                    
                    "Status"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
            
                header('Content-Type: application/json');
                echo json_encode($data);
            }


       }

    }
  function prosesbayar(){
    $data['KODE_BILING']=$this->input->post('KODE_BILING');
    $data['NAMA_WP']=$this->input->post('NAMA_WP');
    $data['ALAMAT_WP']=$this->input->post('ALAMAT_WP');
    $data['NAMA_USAHA']=$this->input->post('NAMA_USAHA');
    $data['MASA_PAJAK']=$this->input->post('MASA_PAJAK');
    $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
    $data['PAJAK_TERUTANG']=str_replace('.','',$this->input->post('PAJAK_TERUTANG'));
    $data['KETERANGAN']=$this->input->post('KETERANGAN');
    $data['TGL_BAYAR']=$this->input->post('TGL_BAYAR');
    $data['JENIS_PAJAK']=$this->input->post('JENIS_PAJAK');
    $data['JUMLAH_BAYAR']= str_replace('.','',$this->input->post('JUMLAH_BAYAR'));
 
    $res=$this->Mbayar->addbayar($data);
    if($res=='true'){
        $pesan='Kode biling '.$this->input->post('KODE_BILING').' Telah di proses.';
    }else{
        $pesan='Kode biling '.$this->input->post('KODE_BILING').' Gagal di proses.';
    }   
     $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$pesan.'", "", "success")
     });
     </script>');
    redirect(site_url('pembayaran/tagihan'));
  }

}