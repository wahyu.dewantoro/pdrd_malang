<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mbayar');
        $this->load->model('Master/Mpokok');
 }
 

  function index(){
        if(isset($_POST['kode_biling'])){
            $data['kode_biling']=$_POST['kode_biling'];
            $data['rk']=$this->Mbayar->getKodebiling($_POST['kode_biling']);
        }else{
            $data['kode_biling']=null;
        }
        $data['bank']=$this->Mpokok->getBank();
    // getKodebiling
    $this->template->load('Welcome/halaman','formbayar',$data);
  }

    public function Tbl_Sptpd()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $KODE_BILING= $this->input->get('Nop',TRUE);
            $query=$this->db->query("select keterangan,kode_biling from SPTPD_TAGIHAN where kode_biling='$KODE_BILING'")->row();
            $data = array();
            if (!empty($query->KODE_BILING)) {
                $data = array(
                    "KETERANGAN"   =>  $query->KETERANGAN,
                    "kb"   =>  $KODE_BILING,
                    "Status"        => array(
                        "IsError"=>"False",
                        "ResponseCode"=>'00',
                        "ErrorDesc" => "Success"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data,200);
            } else {
                $data = array(
                    "kb"   =>  $KODE_BILING,
                    "sts"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            }

            
       }

    }

  function prosesbayar(){
    $data['KODE_BILING']=$this->input->post('KODE_BILING');
    $data['NAMA_WP']=$this->input->post('NAMA_WP');
    $data['ALAMAT_WP']=$this->input->post('ALAMAT_WP');
    $data['NAMA_USAHA']=$this->input->post('NAMA_USAHA');
    $data['MASA_PAJAK']=$this->input->post('MASA_PAJAK');
    $data['TAHUN_PAJAK']=$this->input->post('TAHUN_PAJAK');
    $data['PAJAK_TERUTANG']=str_replace('.','',$this->input->post('PAJAK_TERUTANG'));
    $data['KETERANGAN']=$this->input->post('KETERANGAN');
    $data['TGL_BAYAR']=$this->input->post('TGL_BAYAR');
    $data['JENIS_PAJAK']=$this->input->post('JENIS_PAJAK');
    $data['JUMLAH_BAYAR']= str_replace('.','',$this->input->post('JUMLAH_BAYAR'));
 
    $res=$this->Mbayar->addbayar($data);
    if($res=='true'){
        $pesan='Kode biling '.$this->input->post('KODE_BILING').' Telah di proses.';
    }else{
        $pesan='Kode biling '.$this->input->post('KODE_BILING').' Gagal di proses.';
    }   
     $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$pesan.'", "", "success")
     });
     </script>');
    redirect(site_url('pembayaran/tagihan'));
  }

}