<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pembatalan extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Mbayar');
        $this->load->library('datatables');
 }
 

  function index(){
    if(isset($_GET['tgl1'])){
                $tgl1=$_GET['tgl1'];
                $tgl2=$_GET['tgl2'];
               // $bulan='';
        } else {
                $tgl1='';//date('d-m-Y');
                $tgl2='';//date('Y');//date('d-m-Y');
               // $bulan=date('m');
        }
        $sess=array(
                'tgl1'=>$tgl1,
                'tgl2'=>$tgl2
                //'bulan'=>$bulan
         );
      $this->session->set_userdata($sess);
    $this->template->load('Welcome/halaman','datapembatalan');
  }

  public function json() {
        header('Content-Type: application/json');
        echo $this->Mbayar->json_pembatalan();
    }

}