      <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      
    <div class="page-title">
     <div class="title_left">
      <h3>Form Pembayaran</h3>
    </div>
    <div class="pull-right">
      
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    
      <div class="col-md-10 col-sm-12 col-xs-12">
      <form id="demo-form2" data-parsley-validate class="form-inline"  action="<?php echo base_url().'pembayaran/tagihan.html'?>" method="post" enctype="multipart/form-data" >
        <?php echo $this->session->flashdata('notif')?>
        <div class="x_panel">
          <div class="x_title">
                  <h4><i class="fa fa-search"></i> Pencarian Kode Biling</h4>
                  <div class="clearfix"></div>
                </div>
          <div class="x_content" >
            
              
                <div class="form-group">
                    Kode Biling
                </div>
                <div class="form-group">
                  <input type="text" name="kode_biling" required class="form-control" value="<?= $kode_biling?>" placeholder="xxxxxxx">
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
            </form>
          </div>
        </div>
      </div>

      <?php if(isset($rk)){ ?>
        <div class="col-md-10 col-sm-12 col-xs-12">
            <div class="x_panel">
                  
                <div class="x_content" >
        <?php  if(!empty($rk->row()) ){ $data=$rk->row(); if($data->TGL_BAYAR == null){ ?>
            <form method="post" action="<?php echo base_url().'pembayaran/tagihan/prosesbayar'?>">
                <input type="hidden" value=""> 
                <div class="row">
                  <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                    <div class="col-md-6">
                        <div class="form-group">
                         <label for="KODE_BILLING">Kode Biling</label>
                         <input type="text" name="KODE_BILING" id="KODE_BILING" value="<?=$data->KODE_BILING ?>" readonly class="form-control">
                      </div>
                      <div class="form-group">
                         <label for="NAMA_WP">Nama WP</label>
                         <input type="text" name="NAMA_WP" id="NAMA_WP" value="<?=$data->NAMA_WP ?>" readonly class="form-control">
                      </div>
                    </div>
                  <div class="col-md-6">
                      
                      <div class="form-group">
                         <label for="ALAMAT_WP">Alamat</label>
                         <input type="text" name="ALAMAT_WP" id="ALAMAT_WP" value="<?=$data->ALAMAT_WP ?>" readonly class="form-control">
                      </div>
                      <div class="form-group">
                         <label for="NAMA_USAHA">Nama Usaha</label>
                         <input type="text" name="NAMA_USAHA" id="NAMA_USAHA" value="<?=$data->NAMA_USAHA ?>" readonly class="form-control">
                      </div>  
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12">
                      <div class="x_title">
                        <h4><i class="fa fa-file"></i> Data Pajak</h4>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                         <label for="MASA_PAJAK">Masa Pajak</label>
                         <input type="hidden" name="MASA_PAJAK" id="MASA_PAJAK"  value="<?=$data->MASA_PAJAK ?>" readonly class="form-control">
                         <input type="text"  value="<?= $namabulan[$data->MASA_PAJAK] ?>" readonly class="form-control">
                      </div>
                      <div class="form-group">
                         <label for="TAHUN_PAJAK">Tahun Pajak</label>
                         <input type="text" name="TAHUN_PAJAK" id="TAHUN_PAJAK" value="<?=$data->TAHUN_PAJAK ?>" readonly class="form-control">
                      </div>
                  </div>
                  <div class="col-md-6">
                      
                      <div class="form-group">
                         <label for="PAJAK_TERUTANG">Pajak Terutang</label>
                         <input type="text" name="PAJAK_TERUTANG" id="PAJAK_TERUTANG" value="<?= number_format($data->PAJAK_TERUTANG,'0','','.') ?>" readonly class="form-control">
                      </div>
                      <div class="form-group">
                         <label for="KETERANGAN">Keterangan</label>
                         <input type="text" name="KETERANGAN" id="KETERANGAN" value="<?=$data->KETERANGAN ?>" readonly class="form-control">
                      </div>    
                  </div>

                </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="x_title">
                        <h4><i class="fa fa-money"></i> Data Pembayaran</h4>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                <div class="row">

                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="JUMLAH_BAYAR"> Jumlah Bayar </label>
                          <input type="text" value="<?= number_format($data->PAJAK_TERUTANG,'0','','.') ?>" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="JUMLAH_BAYAR" id="JUMLAH_BAYAR" autofocus class="form-control" required>
                      </div>
                  </div>  
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="TGL_BAYAR"> Tanggal Bayar </label>
                          <input type="text" value="<?php echo date('d/m/Y')?>" name="TGL_BAYAR" id="TGL_BAYAR"  class="tanggal form-control" required>
                      </div>
                  </div>
              </div>
                <div class="pull-right">
                    <button class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                </div>
  <?php } else{?>            
  <div class="alert alert-info">
          <i class="fa fa-close"> <?= $kode_biling ?> sudah lunas bayar tanggal <?= $data->TGL_BAYAR  ?> </i>
        </div>
      <?php } }else{ ?>
        <div class="alert alert-info">
          <i class="fa fa-close"> <?= $kode_biling ?> Tidak di temukan. </i>
        </div>

        <?php }   ?>
                </div>
            </div>
          </div>
       <?php  } ?>
       </form>
    </div>
  
</div>



</div>

<script type="text/javascript">
  
function tandaPemisahTitik(b){
var _minus = false;
if (b<0) _minus = true;
b = b.toString();
b=b.replace(".","");
b=b.replace("-","");
c = "";
panjang = b.length;
j = 0;
for (i = panjang; i > 0; i--){
j = j + 1;
if (((j % 3) == 1) && (j != 1)){
c = b.substr(i-1,1) + "." + c;
} else {
c = b.substr(i-1,1) + c;
}
}
if (_minus) c = "-" + c ;
return c;
}

function numbersonly(ini, e){
if (e.keyCode>=49){
if(e.keyCode<=57){
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
ini.value = tandaPemisahTitik(b);
return false;
}
else if(e.keyCode<=105){
if(e.keyCode>=96){
//e.keycode = e.keycode - 47;
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
ini.value = tandaPemisahTitik(b);
//alert(e.keycode);
return false;
}
else {return false;}
}
else {
return false; }
}else if (e.keyCode==48){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==95){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==8 || e.keycode==46){
a = ini.value.replace(".","");
b = a.replace(/[^\d]/g,"");
b = b.substr(0,b.length -1);
if (tandaPemisahTitik(b)!=""){
ini.value = tandaPemisahTitik(b);
} else {
ini.value = "";
}

return false;
} else if (e.keyCode==9){
return true;
} else if (e.keyCode==17){
return true;
} else {
//alert (e.keyCode);
return false;
}

}
</script>