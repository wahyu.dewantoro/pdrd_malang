      <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      
    <div class="page-title">
    <div class="title_left">
      <h3>Permintaan Pembatalan</h3>
    </div>
    <div class="pull-right">
          <div class="btn-group">
            <?php echo anchor(site_url('Pembayaran/permintaan_unflag/create'), '<i class="fa fa-plus"></i> Tambah Permintaan', 'class="btn btn-primary btn-sm"'); 
            ?>
          </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <div class="x_panel">
          <div class="x_content" >
            <form class="form-inline" method="get" action="<?php echo base_url().'pembayaran/Permintaan_unflag'?>">
                <div class="form-group">
                    <input type="text" name="tgl1"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($this->session->userdata('tgl1')==null) {echo $def1;} else {echo $this->session->userdata('tgl1');}?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($this->session->userdata('tgl2')==null) {echo $def2;} else {echo $this->session->userdata('tgl2');}?>" >
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($this->session->userdata('tgl1') <> '')  { ?>
                                    <a href="<?php echo site_url('pembayaran/Permintaan_unflag'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                   <!--  <a href="<?php echo site_url('Excel/Excel/Excel_laporan_pembayaran'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a> -->
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" width="100%">
                      <thead>
                        <tr>
                          <th width="2%">No</th>
                          <th>Kode Biling</th>
                          <th>Nama WP</th>
                          <th>Usaha</th>
                          <th>Masa Pajak</th>
                          <th>Tahun Pajak</th>
                          <th>Pajak</th>
                          <th width="10%">Jumlah</th>
                          <th>Tanggal Pembatalan</th>
                          <th>Tanggal Pengajuan</th>
                          <th>Alasan</th>                          

                        </tr>
                      </thead> 
                      <tbody>
                        <?php $no=0; $td='0';$pnr='0';foreach ($data_permintaan as $rk)  { ?>
                            <tr>
                              <td align="center"><?php echo ++$no ?></td>
                              <td align="center"><?= $rk->KODE_BILING?></td>
                              <td align=""><?= $rk->NAMA_WP?></td>
                              <td align=""><?= $rk->NAMA_USAHA?></td>
                              <td align=""><?= $namabulan[$rk->MASA_PAJAK]?></td>
                              <td align=""><?= $rk->TAHUN_PAJAK?></td>
                              <td align=""><?= $rk->KETERANGAN?></td>
                              <td align="right">Rp <?=  number_format($rk->JUMLAH_BAYAR,'0','','.')?></td>
                              <td align=""><?php if ($rk->STATUS_UNFLAGE=='1') {
                                echo $rk->TGL_PEMBATALAN;
                              } else {
                                echo "Menugggu Konfirmasi";
                              }
                              ?></td>
                              <td align=""><?= $rk->TGL_AJUAN?></td>
                              <td align=""><?= $rk->ALASAN?></td>
                               <?php if ($this->session->userdata('MS_ROLE_ID')==9) {
                                  if ($rk->STATUS_UNFLAGE==1) {
                                    echo "<td>Disetujui</td>";
                                  } else {?>
                                    <td><a href="<?php echo base_url('Pembayaran/permintaan_unflag/approve/'.$rk->KODE_BILING);?>" onclick="return confirm('konfirmasi Pembatalan Pembayaran dari <?= $rk->NAMA_WP?>');">Aprrove</a></td>
                                  <?php }
                               
                                } else { 

                                }?>
                            </tr>
                            <?php $td +=$qtt;$pnr +=$sum;} ?>
                      </tbody>   
                  </table>
                </div>
          </div>
        </div>
      </div>
</div>
 




