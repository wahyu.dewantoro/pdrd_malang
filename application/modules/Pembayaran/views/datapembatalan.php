      <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      
    <div class="page-title">
     <div class="title_left">
      <h3>Data Pembatalan</h3>
    </div>
    <div class="pull-right">
      
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <?php $def1=date('1/m/Y');$def2=date('d/m/Y');echo $this->session->flashdata('notif')?>
        <div class="x_panel">
          <div class="x_content" >
            <form class="form-inline" method="get" action="<?php echo base_url().'pembayaran/data_pembatalan'?>">
                <div class="form-group">
                    <input type="text" name="tgl1"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($this->session->userdata('tgl1')==null) {echo $def1;} else {echo $this->session->userdata('tgl1');}?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($this->session->userdata('tgl2')==null) {echo $def2;} else {echo $this->session->userdata('tgl2');}?>" >
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($this->session->userdata('tgl1') <> '')  { ?>
                                    <a href="<?php echo site_url('pembayaran/data_pembatalan'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                   <!--  <a href="<?php echo site_url('Excel/Excel/Excel_laporan_pembayaran'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a> -->
                              <?php }   ?>  
        </form>
                  <table id="example2" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <th width="5%">No</th>
                          <th>Kode Biling</th>
                          <th>Nama WP</th>
                          <th>Usaha</th>
                          <th>Masa Pajak</th>
                          <th>Tahun Pajak</th>
                          <th>Pajak</th>
                          <th>Jumlah</th>
                          <th>Tanggal Pembatalan</th>
                          <th>Alasan</th>                          

                        </tr>
                      </thead>    
                  </table>
          </div>
        </div>
      </div>
</div>
 <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#example2").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    
                        
                    
                    'oLanguage':
                    {
                      "sProcessing":   "Sedang memproses...",
                      "sLengthMenu":   "Tampilkan _MENU_ entri",
                      "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                      "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                      "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                      "sInfoPostFix":  "",
                      "sSearch":       "Cari:",
                      "sUrl":          "",
                      "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                      }
                    },
                    processing: true,
                    serverSide: true,
                    pageLength: 20,
                    ajax: {"url": "<?php echo base_url()?>pembayaran/data_pembatalan/json", "type": "POST"},
                    columns: [
                        {
                            "data": "ID_INC",
                            "orderable": false,
                            "className" : "text-center",
                        },
                          { "data": "KODE_BILING"},
                          { "data": "NAMA_WP"},
                           { "data": "NAMA_USAHA"},
                          { "data": "MASA_PAJAK",
                          render: function ( data, type, row, meta ) {
                            switch(data) {
                              case '1' : return "Januari"; break;
                              case '2' : return "Februari"; break;
                              case '3' : return "Maret"; break;
                              case '4' : return "April"; break;
                              case '5' : return "Mei"; break;
                              case '6' : return "Juni"; break;
                              case '7' : return "Juli"; break;
                              case '8' : return "Agustus"; break;
                              case '9' : return "September"; break;
                              case '10' : return "Oktober"; break;
                              case '11' : return "November"; break;
                              case '12' : return "Desember"; break;
                             default  : return 'N/A';
                           }}
                        },
                          { "data": "TAHUN_PAJAK"},
                          { "data": "KETERANGAN"},
                          { "data": "JUMLAH_BAYAR",
                          render: $.fn.dataTable.render.number( '.', ',', 0, '' ),
                          "className" : "text-right"
                          },
                          { "data": "TGL_PEMBATALAN",
                             // render: $.fn.dataTable.render.moment( 'Do MMM YYYYY' )
                          },
                           { "data": "ALASAN",
                             // render: $.fn.dataTable.render.moment( 'Do MMM YYYYY' )
                          },
                        /*{"data": "NAMA_MENU"},
                        {"data": "PARENT_MENU"},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }*/
                    ],
                    order: [[0, 'Desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>




