   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php  $def1=date('1/m/Y');$def2=date('d/m/Y'); echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Upt/laporan_upt_at'?>">
                <div class="form-group">
                    <input type="text" name="tgl1"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($tgl1==null) {echo $def1;} else {echo $tgl1;}?>" >
                </div>
                <div class="form-group">
                     <input type="text" name="tgl2"  required="required" class="form-control col-md-6 col-xs-12 tanggal" value="<?php if ($tgl2==null) {echo $def2;} else {echo $tgl2;}?>" >
                </div>
                <div class="form-group">
                      <select onchange="getkec(this);" name="upt"  placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" >
                            <?= $option?>
                            <?php foreach($list_upt as $rk){ ?>
                            <option  value="<?php echo $rk->ID_INC?>"
                              <?php if ($upt==$rk->ID_INC) {echo "selected";} ?>><?php echo $rk->NAMA_UNIT   ?></option>
                            <?php } ?>  
                      </select>
                </div>
                 <div class="form-group">
                      <select onchange="getkel(this);" name="kecamatan"  id="KECAMATAN" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('h_kecamatan')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kelurahan"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('h_kelurahan')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                <div class="form-group">
                      <select  name="sts" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Status</option>
                          <option <?php if($this->session->userdata('sts')=='1'){echo "selected";}?> value="1">Lunas</option>
                          <option <?php if($this->session->userdata('sts')=='0'){echo "selected";}?> value="0">Belum Lunas</option>
                        </select>
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($tgl1 <> '')  { ?>
                                    <a href="<?php echo site_url('Upt/laporan_upt_at'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <a href="<?php echo site_url('Excel/Excel/Excel_laporan_upt_at'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a>
                              <?php }   ?>  
        </form>
        <div class="table-responsive">
        <table tyle="width: 140%;" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">Masa</th>
              <th class="text-center">Tahun</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">Cara Pengambilan</th>

              <th class="text-center">Peruntukan</th>
              <th class="text-center">Volume Air</th>
              <th class="text-center">Debit Air</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Jatuh tempo</th>
              <th class="text-center">Status</th>
              <th class="text-center">Cetak</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($at as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->KODE_BILING?></td>
                <td><?= $namabulan[$rk->MASA_PAJAK]?></td>
                <td><?= $rk->TAHUN_PAJAK?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td><?= $rk->CARA_PENGAMBILAN?></td>
                <td><?= $rk->JENIS?></td>
                <td align="right"><?=  number_format($rk->VOLUME_AIR_METER,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DEBIT_NON_METER,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DPP,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->TAGIHAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DENDA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH_KETETAPAN,'0','','.')?></td>
                <td><?= $rk->TGL_PENETAPAN?></td>
                <td><?= $rk->JATUH_TEMPO?></td>
                <td><?php if ($rk->STATUS=='1') {
                  echo "Terbayar";} else { echo "Belum Bayar";}?></td>
                <td align="center"><?php if ($rk->KODE_BILING=='') {} else {?>
                  <a href="<?php echo base_url('Pdf/Pdf/pdf_kode_biling/').acak($rk->KODE_BILING);?>"><i class='fa fa-print'></i></a>
                  <?php if ($rk->CARA_PENGAMBILAN=='Meter') {?>
                      <a href="<?php echo base_url('Pdf/Pdf/pdf_skpd_air_meter/').$rk->KODE_BILING;?>">SKPD
                  <?php } else {?>
                      <a href="<?php echo base_url('Pdf/Pdf/pdf_skpd_air_non_meter/').$rk->KODE_BILING;?>">SKPD
                  <?php }?>
                  <?php }?></td>
                  <td><?php if ($rk->CETAK_SKPD=='1') {
                  echo "tercetak";} else { echo "-";}?></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    }
    table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>

<script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
    function getkec(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Upt/Upt/getkec'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KECAMATAN").html(msga);
          }
        });    
    }
  </script>
