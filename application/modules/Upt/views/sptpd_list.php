   
  <?php echo $this->session->flashdata('message')?>  <?php echo $this->session->flashdata('notif')?><br>
  <div class="page-title">
   <div class="title_left">
    <h3><b>
    ENTRY SPTPD</b></h3>
  </div>
</div> 

<div class="clearfix"></div>                  
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <!-- <h4><i class="fa fa-user"></i> Identitas Wajib Pajak </h4> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                        
                          <a href="<?php echo site_url('Sptpd_hotel/sptpd_hotel/create') ?>" class="btn btn-hotel"><i class="fa fa-hotel"></i> PAJAK HOTEL</a>
                          <a href="<?php echo site_url('Sptpd_restoran/create') ?>" class="btn btn-restoran"><i class="fa fa-cutlery"></i> PAJAK RESTORAN</a>
                          <a href="<?php echo site_url('Sptpdhiburan/Hiburan/create') ?>" class="btn btn-hiburan"><i class="fa fa fa-film"></i> Hiburan</a>
                          <a href="<?php echo site_url('sptpdreklame/reklame2/create') ?>" class="btn btn-reklame"><i class="fa fa fa-picture-o"></i> Reklame</a>
                          <a href="<?php echo site_url('Sptpd_ppj/sptpd_ppj/create') ?>" class="btn btn-ppj"><i class="fa fa fa-road"></i> PPJ</a>
                          <a href="<?php echo site_url('Sptpd_mblbm/create') ?>" class="btn btn-galian"><i class="fa fa fa-flag"></i> Galian C</a>
                          <a href="<?php echo site_url('Sptpd_parkir/create') ?>" class="btn btn-parkir"><i class="fa fa fa-car"></i> Parkir</a>
                          <a href="<?php echo site_url('sptpdair/air/create') ?>" class="btn btn-at"><i class="fa fa fa-globe"></i> Air tanah</a>
                          <a href="<?php echo site_url('Sptpd_psb/create') ?>" class="btn btn-sarang"><i class="fa fa fa-twitter"></i> Sarang Burung</a>
                     
            </div>             
          </div>
      </div>
    </div>
  </div>
</div>

  <div class="page-title">
   <div class="title_left">
    <h3><b>
    AMBIL DATA SPTPD BULAN LALU</b></h3>
  </div>
<div class="clearfix"></div>                  
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <!-- <h4><i class="fa fa-user"></i> Identitas Wajib Pajak </h4> -->
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <a href="<?php echo site_url('sptpdair/air_bl/air_bulan_lalu') ?>" class="btn btn-info"><i class="fa fa-edit"></i> Proses Data Bulan Lalu SPTPD Air Tanah</a>
                  <a href="<?php echo site_url('Sptpdreklame/Reklame_bulan_lalu') ?>" class="btn btn-success"><i class="fa fa-edit"></i> Proses Data Bulan Lalu SPTPD Reklame</a> 
            </div>             
          </div>
      </div>
    </div>
  </div>
</div>
  <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 11px;}
    textarea { font-size: 11px;}
    .input-group span{ font-size: 11px;}
    input[type='text'] { font-size: 11px; height:28px}

    .btn-hotel {
      color: #fff;
      background-color: #d5bb4d;
      border-color: #d5bb4d;}
      .btn-restoran {
      color: #fff;
      background-color: #f55643;
      border-color: #f55643;}
      .btn-hiburan {
      color: #fff;
      background-color: #389ede;
      border-color: #389ede;}
      .btn-reklame {
      color: #fff;
      background-color: #8095f0;
      border-color: #8095f0;}
      .btn-ppj {
      color: #fff;
      background-color: #f0ad4e;
      border-color: #eea236;}
      .btn-galian {
      color: #fff;
      background-color: #ba9ea8;
      border-color: #ba9ea8;}
      .btn-parkir {
      color: #fff;
      background-color: #9876f5;
      border-color: #9876f5;}
      .btn-at {
      color: #fff;
      background-color: #66cff8;
      border-color: #66cff8;}
      .btn-sarang {
      color: #fff;
      background-color: #85cb39;
      border-color: #85cb39;}
  </style>

