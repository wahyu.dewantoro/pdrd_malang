<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upt extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Laporan_sptpd/Mlaporan_sptpd');
        $this->load->model('Mupt');
 }
 function index()
 {
       
       $this->template->load('Welcome/halaman','sptpd_list');
    }
 function laporan_upt_hotel()
 {      
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_hotel?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_hotel?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_hotel';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_hotel';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_hotel($tgl1,$tgl2,$upt);
        $hotel                 = $this->Mupt->get_limit_data_rekap_hotel($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak Hotel',
            'hotel'            => $hotel,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_hotel',$data);
    }
    function laporan_upt_restoran(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_restoran?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_restoran?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_restoran';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_restoran';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_restoran($tgl1,$tgl2,$upt);
        $restoran                 = $this->Mupt->get_limit_data_rekap_restoran($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak Restoran',
            'restoran'         => $restoran,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_restoran',$data);
    }
    function laporan_upt_hiburan(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_hiburan?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_hiburan?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_hiburan';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_hiburan';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_hiburan($tgl1,$tgl2,$upt);
        $hiburan                 = $this->Mupt->get_limit_data_rekap_hiburan($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak Hiburan',
            'hiburan'          => $hiburan,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_hiburan',$data);
    }
    function laporan_upt_reklame(){
        $tgl1     = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        //$upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $upt     ='';
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_reklame?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_reklame?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_reklame';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_reklame';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_reklame($tgl1,$tgl2,$upt);
        $reklame                 = $this->Mupt->get_limit_data_rekap_reklame($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak Reklame',
            'reklame'          => $reklame,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_reklame',$data);
    }
    function laporan_upt_ppj(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_ppj?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_ppj?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_ppj';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_ppj';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_ppj($tgl1,$tgl2,$upt);
        $ppj                 = $this->Mupt->get_limit_data_rekap_ppj($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak PPJ',
            'ppj'              => $ppj,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_ppj',$data);
    }
    function laporan_upt_galian(){
        $tgl1     = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_galian?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_galian?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_galian';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_galian';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_galian($tgl1,$tgl2,$upt);
        $galian                 = $this->Mupt->get_limit_data_rekap_galian($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak Galian C',
            'galian'            => $galian,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_galian',$data);
    }
    function laporan_upt_parkir(){
        $tgl1     = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_parkir?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_parkir?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_parkir';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_parkir';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_parkir($tgl1,$tgl2,$upt);
        $parkir                 = $this->Mupt->get_limit_data_rekap_parkir($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak Parkir',
            'parkir'            => $parkir,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_parkir',$data);
    }
    function laporan_upt_at(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $kecamatan= urldecode($this->input->get('kecamatan', TRUE));
        $kelurahan= urldecode($this->input->get('kelurahan', TRUE));
        $sts= urldecode($this->input->get('sts', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
            'h_kecamatan'   => $kecamatan,
            'h_kelurahan'   => $kelurahan,
            'sts'           => $sts
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_at?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&sts='.urlencode($sts);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_at?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt).'&kecamatan='.urlencode($kecamatan).'&kelurahan='.urlencode($kelurahan).'&sts='.urlencode($sts);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_at';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_at';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_at($tgl1,$tgl2,$upt,$kecamatan,$kelurahan,$sts);
        $at                 = $this->Mupt->get_limit_data_rekap_at($config['per_page'], $start, $tgl1,$tgl2,$upt,$kecamatan,$kelurahan,$sts);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap SKPD Air Tanah',
            'at'            => $at,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->db->query("SELECT * FROM KECAMATAN WHERE KODE_UPT='$upt'")->result();
       $this->Mlaporan_sptpd->get_kec();
       $this->template->load('Welcome/halaman','laporan_upt_at',$data);
    }
    function getkec(){
        $KODEKEC=$_POST['KODEKEC'];

        $data['kc']=$this->db->query("SELECT KODEKEC,NAMAKEC FROM KECAMATAN WHERE KODE_UPT ='$KODEKEC' order by NAMAKEC asc")->result();
        $this->load->view('getkec',$data);
    }
    function laporan_upt_sb(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $upt     = urldecode($this->input->get('upt', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'upt'       => $upt,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_sb?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
            $config['first_url'] = base_url() . 'Upt/laporan_upt_sb?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&upt='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Upt/laporan_upt_sb';
            $config['first_url'] = base_url() . 'Upt/laporan_upt_sb';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mupt->total_rows_rekap_sb($tgl1,$tgl2,$upt);
        $sb                 = $this->Mupt->get_limit_data_rekap_sb($config['per_page'], $start, $tgl1,$tgl2,$upt);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="<option value=''>Semua Upt</option>";
        } else {
            $wh="";
        }
        $data = array(
        	'title'			   => 'Laporan Rekap Pajak Sarang Burung',
            'sb'               => $sb,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            'option'           => $wh
        );
       $data['list_upt']=$this->Mupt->get_upt();
       $this->template->load('Welcome/halaman','laporan_upt_sb',$data);
    }
  
}