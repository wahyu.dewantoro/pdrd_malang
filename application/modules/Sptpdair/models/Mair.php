<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mair extends CI_Model
{

    public $table = 'sptpd_at';
    public $id = 'kode_supplier';
    public $order = 'DESC';
    public $KODE_BILING = 'KODE_BILING';

    function __construct()
    {
        parent::__construct();
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }

    // datatables
    function json_supplier() {
        $this->datatables->select('kode_supplier,nama_supplier,email,hp,kecamatan,kabupaten,alamat');
        $this->datatables->from('sptpd_at');
        $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('Pembelian/sptpd_at/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('pembelian/sptpd_at/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" id="delete" data-id="$1" href="javascript:void(0)"').'</div>', 'kode_supplier');
        return $this->datatables->generate();
    }
  
    function jsonair(){
        $t1 =$this->session->userdata('ar_bulan');
        $t2 =$this->session->userdata('ar_tahun');
        $cp =$this->session->userdata('c_pengambilan');
       if ($this->role==5) {
            $ts ="AND STATUS='2'";
       } else {
        $ts =" ";
     }
        if ($this->session->userdata('ar_tahun')!='' AND $this->session->userdata('ar_bulan')!='' AND $this->session->userdata('c_pengambilan')!='') {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND NPWPD LIKE '%$cp%' OR LOWER(NAMA) LIKE '%$cp%' OR LOWER(NAMA_WP) LIKE '%$cp%'";
        } else if ($this->session->userdata('ar_tahun')!='' AND $this->session->userdata('ar_bulan')!='') {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2'";
        } else if ($this->session->userdata('ar_tahun')!='' AND $this->session->userdata('c_pengambilan')!='') {
            $wh ="TAHUN_PAJAK='$t2' AND NPWPD LIKE '%$cp%' OR LOWER(NAMA) LIKE '%$cp%' OR LOWER(NAMA_WP) LIKE '%$cp%'";
        } else if ($this->session->userdata('ar_tahun')!='') {
            $wh ="TAHUN_PAJAK='$t2'";
        } else {
            $wh ="TO_CHAR(TGL_INSERT, 'mm')=TO_CHAR(SYSDATE, 'mm') ";
        }
        $this->datatables->select("NAMA,ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TO_CHAR (TGL_BAYAR, 'dd-mm-yyyy HH24: MI: SS') TGL_BAYAR,CARA_PENGAMBILAN");
        $this->datatables->from('V_LIST_PENETAPAN');
        //$this->datatables->join('MS_PENGGUNA B','A.NPWP_INSERT=B.NIP');
        $this->datatables->where($wh);
        $this->db->order_by("TGL_INSERT DESC, TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");

        $this->datatables->add_column('actionc', '<div class="btn-group">'.anchor(site_url('sptpdair/air/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        if ($this->role==5) {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('sptpdair/air/form_penetapan/$1'),'Proses','class="btn btn-xs btn-success"').'</div>', 'acak(ID_INC)');
        } else {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', ''.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'', 'acak(ID_INC)');     

        }
        return $this->datatables->generate();
    }
    function json_penetapan(){
         $wh ="STATUS='2'";
        $this->datatables->select("NAMA,ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS,TO_CHAR (TGL_BAYAR, 'dd-mm-yyyy HH24: MI: SS') TGL_BAYAR,CARA_PENGAMBILAN");
        $this->datatables->from('V_LIST_PENETAPAN');
       
        $this->datatables->where($wh);
        $this->db->order_by("TGL_INSERT DESC, TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");

        $this->datatables->add_column('actionc', '<div class="btn-group">'.anchor(site_url('sptpdair/air/pdf_kode_biling/$1'),'<i class="fa fa-print"></i>','class="btn btn-xs btn-info"').'</div>', 'ID_INC');
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        if ($this->role==5) {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', '<div class="btn-group">'.anchor(site_url('sptpdair/air/form_penetapan/$1'),'Proses','class="btn btn-xs btn-success"').'</div>', 'acak(ID_INC)');
        } else {
            //status 1
        $this->datatables->add_column('actiona', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'Detail','class="btn btn-xs btn-info"').'</div>', 'acak(ID_INC)');
            //status !=1
        $this->datatables->add_column('actionb', ''.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'', 'acak(ID_INC)');     

        }
        return $this->datatables->generate();
    }
    function jsonair_bl(){
        $t1 =$this->session->userdata('ar_bulan_bl');
        $t2 =$this->session->userdata('ar_tahun_bl');
        $kec =$this->session->userdata('ar_kec_bl');
        $kel =$this->session->userdata('ar_kel_bl');
        if ($this->session->userdata('ar_kel_bl')=='') {
           $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND STATUS!='2' AND KODEKEC='$kec'";
        } else {
            $wh ="MASA_PAJAK='$t1' AND TAHUN_PAJAK='$t2' AND STATUS!='2' AND KODEKEC='$kec' AND KODEKEL='$kel'";
        }
        
        
        $this->datatables->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,ALAMAT_WP, DPP,PAJAK_TERUTANG,STATUS");
        $this->datatables->from('SPTPD_AIR_TANAH');
        $this->datatables->where($wh);
        $this->db->order_by(" TAHUN_PAJAK DESC, MASA_PAJAK DESC,ID_INC DESC");


        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('sptpdair/air/detail/$1'),'<i class="fa fa-binoculars"></i>','class="btn btn-xs btn-primary"').anchor(site_url('sptpdair/air/update/$1'),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"').anchor(site_url('sptpdair/air/delete/$1'),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onClick="return confirm(\'Apakah anda yakin?\')" ').'</div>', 'ID_INC');
        return $this->datatables->generate();
    }

    function getById($id){
        return $this->db->query("SELECT * FROM SPTPD_AIR_TANAH WHERE ID_INC='$id'")->row();
    }
    function Peruntukan(){
        return $this->db->query("SELECT * FROM PERUNTUKAN_AIR_TANAH order by no asc")->result();
    }
    function Gol(){
        return $this->db->query("SELECT * FROM OBJEK_PAJAK where iD_GOL=490 ")->result();
    }
    function PeruntukanbyID($id){
        return $this->db->query("SELECT * FROM PERUNTUKAN_AIR_TANAH WHERE NO='$id'")->row();
    }
}

/* End of file Mmenu.php */
/* Location: ./application/models/Mmenu.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-08 05:30:09 */
/* http://harviacode.com */