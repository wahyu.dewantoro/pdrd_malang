           <div id="FORM_METER">
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Bulan Ini  <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input <?php echo $disable ?> type="text" id="PENGGUNAAN_HARI_INI_METER"  name="PENGGUNAAN_HARI_INI_METER" required="required" placeholder="Hari Ini " class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PENGAMBILAN=='Non Meter'){ echo ''; } else{ echo $PENGGUNAAN_HARI_INI_METER;} ?>" onchange='getvolume(this);'>
                          <span class="input-group-addon">M<sup>3</sup></span>
                        </div>
                      </div>
                    </div>  

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Bulan Lalu <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input <?php echo $disable ?> type="text" id="PENGGUNAAN_BULAN_LALU_METER" name="PENGGUNAAN_BULAN_LALU_METER" required="required" placeholder="Bulan Lalu" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PENGAMBILAN=='Non Meter'){ echo ''; } else{ echo $PENGGUNAAN_BULAN_LALU_METER;} ?>" onchange='getvolume(this);'>
                          <span class="input-group-addon">M<sup>3</sup></span>
                        </div>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Volume Air <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input <?php echo $disable ?> type="text" id="VOLUME" name="VOLUME_AIR_METER"   required="required" placeholder="Volume Air" class="form-control col-md-7 col-xs-12" value="<?php echo $VOLUME_AIR_METER ?>" readonly="">
                          <span class="input-group-addon">M<sup>3</sup></span>
                        </div>
                      </div>
                    </div> 
        </div>
        <script>
if('<?php echo $CARA_PENGAMBILAN ?>'=='Meter'){
        DPP.value=getNominal(<?php echo $DPP ?>);
        PAJAK_TERUTANG.value=getNominal(<?php echo $PAJAK_TERUTANG ?>);

}else{
          DPP.value='';
        PAJAK_TERUTANG.value='';
        VOLUME.value='';
        HARGA.value='';
}
    function getvolume() {
      var H=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_HARI_INI_METER").value))|| 0;
      var B=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_BULAN_LALU_METER").value))|| 0;
      var PERSEN=parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
      if (H<B){
        swal("Nilai meter sekarang tidak boleh lebih kecil dari meter lama", "", "warning");
        VOLUME.value='';
      } else if (H==B){
          DPP.value=0;
          PAJAK_TERUTANG.value=0;
          VOLUME.value=0;
      } else {

              /*if (H=B) {
                 var V=(H-B).toFixed(2);
              } else {
                 var V=(H-B).toFixed(2);
              }*/
      var V=(H-B).toFixed(2);      
      VOLUME.value= getNominal(V);
      if(V>1&&V<=50){
        var HD=parseFloat(nominalFormat(document.getElementById("A").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>50&&V<=500){
        var HD=parseFloat(nominalFormat(document.getElementById("B").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>500&&V<=1000){
        var HD=parseFloat(nominalFormat(document.getElementById("C").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>1000&&V<=2500){
        var HD=parseFloat(nominalFormat(document.getElementById("D").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>2500&&V<=5000){
        var HD=parseFloat(nominalFormat(document.getElementById("E").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>5000&&V<=7500){
        var HD=parseFloat(nominalFormat(document.getElementById("F").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>7500){
        var HD=parseFloat(nominalFormat(document.getElementById("G").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      }

    }
          /* PENGGUNAAN_HARI_INI_METER */
          var PENGGUNAAN_HARI_INI_METER = document.getElementById('PENGGUNAAN_HARI_INI_METER');
          PENGGUNAAN_HARI_INI_METER.addEventListener('keyup', function(e)
          {
            PENGGUNAAN_HARI_INI_METER.value = formatRupiah(this.value);
          });  
          /* PENGGUNAAN_BULAN_LALU_METER */
          var PENGGUNAAN_BULAN_LALU_METER = document.getElementById('PENGGUNAAN_BULAN_LALU_METER');
          PENGGUNAAN_BULAN_LALU_METER.addEventListener('keyup', function(e)
          {
            PENGGUNAAN_BULAN_LALU_METER.value = formatRupiah(this.value);
          });                       
        </script>