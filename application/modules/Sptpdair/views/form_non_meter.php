<div id="FORM_NON_METER"> 
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Debit Air <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="DEBIT_NON_METER" name="DEBIT_NON_METER" required="required" placeholder="Debit air" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PENGAMBILAN=='Meter'){ echo ''; } else{ echo $DEBIT_NON_METER;} ?>" onchange='getv(this);'>
                <span class="input-group-addon">Liter / detik</span>
              </div>
            </div>
          </div>  

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Penggunaan   <br>1 Hari <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="PENGGUNAAN_HARI_NON_METER"  name="PENGGUNAAN_HARI_NON_METER"  required="required" placeholder="Penggunaan 1 Hari" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PENGAMBILAN=='Meter'){ echo ''; } else{ echo $PENGGUNAAN_HARI_NON_METER;} ?>" onchange='getv(this);'>
                <span class="input-group-addon">Jam</span>
              </div>
            </div>
          </div>  
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Penggunaan   <br>1 Bulan <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="PENGGUNAAN_BULAN_NON_METER"  name="PENGGUNAAN_BULAN_NON_METER" required="required" placeholder="Penggunaan 1 Bulan" class="form-control col-md-7 col-xs-12" value="<?php if($CARA_PENGAMBILAN=='Meter'){ echo ''; } else{ echo $PENGGUNAAN_BULAN_NON_METER;} ?>" onchange='getv(this);'>
                <span class="input-group-addon">Hari</span>
              </div>
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Volume Air <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="VOLUME" name="VOLUME_AIR_METER"   required="required" placeholder="Volume Air" class="form-control col-md-7 col-xs-12" value="<?php echo $VOLUME_AIR_METER ?>" readonly="">
                <span class="input-group-addon">M<sup>3</sup></span>
              </div>
            </div>
          </div>

</div>  
<script>

if('<?php echo $CARA_PENGAMBILAN ?>'=='Non Meter'){

        DPP.value=getNominal(<?php echo $DPP ?>);
        PAJAK_TERUTANG.value=getNominal(<?php echo $PAJAK_TERUTANG ?>);
}else{
          DPP.value='';
        PAJAK_TERUTANG.value='';
        VOLUME.value='';
        HARGA.value='';
}
    function getv() {
      var D=parseFloat(nominalFormat(document.getElementById("DEBIT_NON_METER").value))|| 0;
      var H=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_HARI_NON_METER").value))|| 1;
      var B=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_BULAN_NON_METER").value))|| 1;
      var PERSEN=parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
      //alert(getNominal(D*H*B*3600/1000));
      var V=(D*H*B*3600/1000).toFixed(2);
      VOLUME.value= getNominal(V);
      //alert(V);
      if(V==0){
        var HD=parseFloat(nominalFormat(document.getElementById("A").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>1&&V<=50){
        var HD=parseFloat(nominalFormat(document.getElementById("A").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>50&&V<=500){
        var HD=parseFloat(nominalFormat(document.getElementById("B").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>500&&V<=1000){
        var HD=parseFloat(nominalFormat(document.getElementById("C").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>1000&&V<=2500){
        var HD=parseFloat(nominalFormat(document.getElementById("D").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>2500&&V<=5000){
        var HD=parseFloat(nominalFormat(document.getElementById("E").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>5000&&V<=7500){
        var HD=parseFloat(nominalFormat(document.getElementById("F").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
      if(V>7500){
        var HD=parseFloat(nominalFormat(document.getElementById("G").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(Math.round(pt));
        HARGA.value=getNominal(HD);
      }
    }
  /* PENGGUNAAN_HARI_NON_METER */
  var PENGGUNAAN_HARI_NON_METER = document.getElementById('PENGGUNAAN_HARI_NON_METER');
  PENGGUNAAN_HARI_NON_METER.addEventListener('keyup', function(e)
  {
    PENGGUNAAN_HARI_NON_METER.value = formatRupiah(this.value);
  });  
  /* PENGGUNAAN_BULAN_NON_METER */
  var PENGGUNAAN_BULAN_NON_METER = document.getElementById('PENGGUNAAN_BULAN_NON_METER');
  PENGGUNAAN_BULAN_NON_METER.addEventListener('keyup', function(e)
  {
    PENGGUNAAN_BULAN_NON_METER.value = formatRupiah(this.value);
  });
  /* DEBIT_NON_METER */
  var DEBIT_NON_METER = document.getElementById('DEBIT_NON_METER');
  DEBIT_NON_METER.addEventListener('keyup', function(e)
  {
    DEBIT_NON_METER.value = formatRupiah(this.value);
  });  
</script>