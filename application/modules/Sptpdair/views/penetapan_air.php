   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
   <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
      <a href="javascript:history.back()" class='class="btn btn-sm btn-primary'><i class="fa fa-angle-double-left"> Kembali</i></a>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"   method="" enctype="multipart/form-data" >
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content" >  
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="col-md-6">
                      <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" />   
                      <input type="hidden" name="CARA" value="<?php echo $CARA_PENGAMBILAN; ?>" />
                      <?php $NB=$namabulan[$MASA_PAJAK]; ?>
                      <input type="hidden" id="MP" value="<?php echo $NB ?>" />
                      <input type="hidden" id="TP" value="<?php echo $TAHUN_PAJAK; ?>" />
                      <?php if($disable=='' AND $param!=''){?>                                 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pilihan
                        </label>
                        <div class="col-md-9">
                          <a href="<?php echo site_url('sptpdair/air_bl/air_bulan_lalu') ?>" class="btn btn-danger"><i class="fa fa-edit"></i> Gunakan Data Bulan Lalu</a>
                        </div>
                      </div> 
                      <?php } ?> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option <?php if(!empty($MASA_PAJAK)){ if($mp==$MASA_PAJAK){echo "selected";}} else {if($mp==date("m")){echo "selected";}}?> value="<?php echo $mp?>"><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <input type="text" <?php echo $disable?> id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun Pajak" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>">                
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD  <sup>*</sup> 
                        </label>
                        <div class="col-md-9">
                          <input type="text" id="NPWPD" <?php echo $disable?> name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>">                  
                        </div>
                      </div>     
                    </div>
                    <div class="col-md-6">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP <sup>*</sup>
                        </label>
                        <div class="col-md-9 ">
                          <input type="text" id="NAMA_WP" <?php echo $disable?> name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>">                
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Alamat WP <sup>*</sup>
                        </label>
                        <div class="col-md-9 ">
                          <textarea style="font-size: 11px" id="ALAMAT_WP" rows="3" <?php echo $disable?> class="resizable_textarea form-control" placeholder="Alamat" name="ALAMAT_WP"><?php echo $ALAMAT_WP; ?></textarea>
                        </div>
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Nama Usaha <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <input type="text" id="NAMA_USAHA" <?php echo $disable?> name="NAMA_USAHA" required="required" placeholder="Nama Perusahaan" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>">                     
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat Usaha <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <textarea style="font-size: 11px" id="ALAMAT_USAHA" rows="3" <?php echo $disable?> class="resizable_textarea form-control" placeholder="Alamat" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                        </div>
                      </div>                       
                    </div>
                  </div>
                </div>
              </div>

              <div class="x_title">
                <h4><i class="fa fa-ship"></i> Data Pengambilan dan Pemanfaatan Air Tanah</h4>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-6">            
                <div class="col-md-12 col-sm-12">
                  <div style="font-size:12px">
                    <div class="x_content" >
                      <div class="row">

                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > No Berkas  <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <input type="text" id="NO_BERKAS" <?php echo $disable?> name="NO_BERKAS" required="required" placeholder="No Berkas" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_BERKAS; ?>">               
                            </div>
                          </div>                       
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >SIPA / SIPPAT  <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <input type="text" id="NO_SIPA_SIPPAT" <?php echo $disable?> name="NO_SIPA_SIPPAT" required="required" placeholder="No SIPA / SIPPAT" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_SIPA_SIPPAT; ?>">                          
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >TMT <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <input type="text" id="tmt" name="TMT" <?php echo $disable?> required="required" placeholder="TMT" class="form-control col-md-7 col-xs-12" value="<?php echo $TMT; ?>">                  
                            </div>
                          </div> 

                          <?php
                          $persen = "var a = new Array();\n";
                          ?>                
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan Objek Pajak <sup>*</sup>
                            </label>
                            <div class="col-md-9">
                              <select style="font-size: 11px" id="GOLONGAN_OP" <?php echo  $disable?> name="GOLONGAN_OP" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" onchange="changegol(this.value)">
                                <option value="">Pilih</option>
                                <?php foreach ($golongan as $gol) { ?>

                                <option <?php if('834'==$gol->ID_OP){echo "selected";}?> value="<?php echo $gol->ID_OP ?>"><?php echo $gol->DESKRIPSI ?></option>

                      <input type="hidden" name="CARA" id="PERSEN" value="<?php echo $gol->PERSEN; ?>" />                                  

                                <?php 
                                $persen .= "a['" . $gol->ID_OP . "'] = {a:'".addslashes($gol->PERSEN)."'};\n";} ?>
                              </select>

                            </select>  
                            <script type="text/javascript">  
                              <?php echo $persen;?>
                              function changegol(id){
                                document.getElementById('PAJAK').value = a[id].a;
                              }
                            </script>                 
                          </div>
                        </div> 
                        <?php
                        $s1d50 = "var s1 = new Array();\n";
                        $s51d500 = "var s51 = new Array();\n";
                        $s501d1000 = "var s501 = new Array();\n";
                        $s1001d2500 = "var s1001 = new Array();\n";
                        $s2501d5000 = "var s2501 = new Array();\n";
                        $s5001d7500 = "var s5001 = new Array();\n";
                        $s7500 = "var s7500 = new Array();\n";
                        ?>               
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-4 col-xs-12" for="" >Peruntukan  <sup>*</sup>
                          </label>

                          <div class="col-md-9">
                            <select style="font-size: 11px" id="PERUNTUKAN" <?php echo $disable?> name="PERUNTUKAN" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12" onchange="changeValue(this.value)">
                              <option value="">Pilih</option>
                              <?php foreach ($peruntukan as $row) { ?>


                              <option <?php if($PERUNTUKAN==$row->NO){echo "selected";}?> value="<?php echo $row->NO ?>"><?php echo $row->JENIS." - ".$row->JENISPENGAMBILAN; ?></option>
                              <?php 

                              $s1d50 .= "s1['" . $row->NO . "'] = {s1:'".addslashes(number_format($row->S1D50,'0','','.'))."'};\n";
                              $s51d500 .= "s51['" . $row->NO . "'] = {s51:'".addslashes(number_format($row->S51D500,'0','','.'))."'};\n";
                              $s501d1000 .= "s501['" . $row->NO . "'] = {s501:'".addslashes(number_format($row->S501D1000,'0','','.'))."'};\n";
                              $s1001d2500 .= "s1001['" . $row->NO . "'] = {s1001:'".addslashes(number_format($row->S1001D2500,'0','','.'))."'};\n";
                              $s2501d5000 .= "s2501['" . $row->NO . "'] = {s2501:'".addslashes(number_format($row->S2501D5000,'0','','.'))."'};\n";
                              $s5001d7500 .= "s5001['" . $row->NO . "'] = {s5001:'".addslashes(number_format($row->S5001D7500,'0','','.'))."'};\n";
                              $s7500 .= "s7500['" . $row->NO . "'] = {s7500:'".addslashes(number_format($row->S7500D,'0','','.'))."'};\n";
                            } ?>
                          </select>
                        </div>
                      </div> 
                      <script type="text/javascript">  
                        <?php echo $s1d50; echo $s51d500;echo $s501d1000;echo $s1001d2500;echo $s2501d5000;echo $s5001d7500;echo $s7500;?>             
                        function changeValue(id){
                          document.getElementById('A').value = s1[id].s1;
                          document.getElementById('B').value = s51[id].s51;
                          document.getElementById('C').value = s501[id].s501;
                          document.getElementById('D').value = s1001[id].s1001;
                          document.getElementById('E').value = s2501[id].s2501;
                          document.getElementById('F').value = s5001[id].s5001;
                          document.getElementById('G').value = s7500[id].s7500;
                          CARA_PENGAMBILAN.value='';
                          $('#FORM_NON_METER').remove();
                          $('#FORM_METER').remove();
                          DPP.value='';
                          PAJAK_TERUTANG.value='';
                        }
                      </script>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Cara Pengambilan  <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select style="font-size: 11px" id="CARA_PENGAMBILAN" <?php echo $disable?> name="CARA_PENGAMBILAN" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <option <?php if($CARA_PENGAMBILAN=='Meter'){echo "selected";}?> value="Meter">Meter</option>
                            <option <?php if($CARA_PENGAMBILAN=='Non Meter'){echo "selected";}?> value="Non Meter">Non Meter</option>
                          </select>
                        </div>
                      </div> 

          <div id="kontentdiag">
            <!-- edit -->
              <?php if ($CARA_PENGAMBILAN=='Meter') { ?>
           <div id="FORM_METER">
                     <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Bulan Ini  <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input <?php echo $disable ?> type="text" id="PENGGUNAAN_HARI_INI_METER"  name="PENGGUNAAN_HARI_INI_METER" required="required" placeholder="Hari Ini " class="form-control col-md-7 col-xs-12" value="<?php echo $PENGGUNAAN_HARI_INI_METER ?>" onchange='getvolume(this);'>
                          <span class="input-group-addon">M<sup>3</sup></span>
                        </div>
                      </div>
                    </div>  

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Bulan Lalu <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input <?php echo $disable ?> type="text" id="PENGGUNAAN_BULAN_LALU_METER" name="PENGGUNAAN_BULAN_LALU_METER" required="required" placeholder="Bulan Lalu" class="form-control col-md-7 col-xs-12" value="<?php echo $PENGGUNAAN_BULAN_LALU_METER ?>" onchange='getvolume(this);'>
                          <span class="input-group-addon">M<sup>3</sup></span>
                        </div>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Volume Air <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <input <?php echo $disable ?> type="text" id="VOLUME_AIR_METER" name="VOLUME_AIR_METER"   required="required" placeholder="Volume Air" class="form-control col-md-7 col-xs-12" value="<?php echo $VOLUME_AIR_METER ?>" readonly="">
                          <span class="input-group-addon">M<sup>3</sup></span>
                        </div>
                      </div>
                    </div> 
        </div>
        <script>

    function getvolume() {
      var H=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_HARI_INI_METER").value))|| 0;
      var B=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_BULAN_LALU_METER").value))|| 0;
      var PERSEN=parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
      if (H<B){
        swal("Nilai meter sekarang tidak boleh lebih kecil dari meter lama", "", "warning");
        VOLUME_AIR_METER.value='';
      } else {

      var V=(H-B).toFixed(2);
      VOLUME_AIR_METER.value= getNominal(V);
      if(V>1&&V<=50){
        var HD=parseFloat(nominalFormat(document.getElementById("A").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>50&&V<=500){
        var HD=parseFloat(nominalFormat(document.getElementById("B").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>500&&V<=1000){
        var HD=parseFloat(nominalFormat(document.getElementById("C").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>1000&&V<=2500){
        var HD=parseFloat(nominalFormat(document.getElementById("D").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>2500&&V<=5000){
        var HD=parseFloat(nominalFormat(document.getElementById("E").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>5000&&V<=7500){
        var HD=parseFloat(nominalFormat(document.getElementById("F").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>7500){
        var HD=parseFloat(nominalFormat(document.getElementById("G").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      }

    }
          /* PENGGUNAAN_HARI_INI_METER */
          var PENGGUNAAN_HARI_INI_METER = document.getElementById('PENGGUNAAN_HARI_INI_METER');
          PENGGUNAAN_HARI_INI_METER.addEventListener('keyup', function(e)
          {
            PENGGUNAAN_HARI_INI_METER.value = formatRupiah(this.value);
          });  
          /* PENGGUNAAN_BULAN_LALU_METER */
          var PENGGUNAAN_BULAN_LALU_METER = document.getElementById('PENGGUNAAN_BULAN_LALU_METER');
          PENGGUNAAN_BULAN_LALU_METER.addEventListener('keyup', function(e)
          {
            PENGGUNAAN_BULAN_LALU_METER.value = formatRupiah(this.value);
          }); 
          /* VOLUME_AIR_METER */
          var VOLUME_AIR_METER = document.getElementById('VOLUME_AIR_METER');
          VOLUME_AIR_METER.addEventListener('keyup', function(e)
          {
            VOLUME_AIR_METER.value = formatRupiah(this.value);
          });                       
        </script>       
            <?php } elseif($CARA_PENGAMBILAN=='Non Meter') { ?>
                     <div id="FORM_NON_METER"> 
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Debit Air <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="DEBIT_NON_METER" name="DEBIT_NON_METER" required="required" placeholder="Debit air" class="form-control col-md-7 col-xs-12" value="<?php echo $DEBIT_NON_METER ?>" onchange='getv(this);'>
                <span class="input-group-addon">Liter / detik</span>
              </div>
            </div>
          </div>  

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Penggunaan   <br>1 Hari <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="PENGGUNAAN_HARI_NON_METER"  name="PENGGUNAAN_HARI_NON_METER"  required="required" placeholder="Penggunaan 1 Hari" class="form-control col-md-7 col-xs-12" value="<?php echo $PENGGUNAAN_HARI_NON_METER ?>" onchange='getv(this);'>
                <span class="input-group-addon">Jam</span>
              </div>
            </div>
          </div>  
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Penggunaan   <br>1 Bulan <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="PENGGUNAAN_BULAN_NON_METER"  name="PENGGUNAAN_BULAN_NON_METER" required="required" placeholder="Penggunaan 1 Bulan" class="form-control col-md-7 col-xs-12" value="<?php echo $PENGGUNAAN_BULAN_NON_METER ?>" onchange='getv(this);'>
                <span class="input-group-addon">Hari</span>
              </div>
            </div>
          </div> 
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"  >Volume Air <sup>*</sup>
            </label>
            <div class="col-md-9 col-sm-6 col-xs-12">
              <div class="input-group">
                <input <?php echo $disable ?> type="text" id="VOLUME" name="VOLUME_AIR_METER"   required="required" placeholder="Volume Air" class="form-control col-md-7 col-xs-12" value="<?php echo $VOLUME_AIR_METER ?>" readonly="">
                <span class="input-group-addon">M<sup>3</sup></span>
              </div>
            </div>
          </div>

</div>  
<script>

    function getv() {
      var D=parseFloat(nominalFormat(document.getElementById("DEBIT_NON_METER").value))|| 1;
      var H=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_HARI_NON_METER").value))|| 1;
      var B=parseFloat(nominalFormat(document.getElementById("PENGGUNAAN_BULAN_NON_METER").value))|| 1;
      var PERSEN=parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
      //alert(getNominal(D*H*B*3600/1000));
      var V=(D*H*B*3600/1000).toFixed(2);
      VOLUME.value= getNominal(V);
      if(V>1&&V<=50){
        var HD=parseFloat(nominalFormat(document.getElementById("A").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>50&&V<=500){
        var HD=parseFloat(nominalFormat(document.getElementById("B").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>500&&V<=1000){
        var HD=parseFloat(nominalFormat(document.getElementById("C").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>1000&&V<=2500){
        var HD=parseFloat(nominalFormat(document.getElementById("D").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>2500&&V<=5000){
        var HD=parseFloat(nominalFormat(document.getElementById("E").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>5000&&V<=7500){
        var HD=parseFloat(nominalFormat(document.getElementById("F").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
      if(V>7500){
        var HD=parseFloat(nominalFormat(document.getElementById("G").value))|| 0;
        var dpp=V*HD;
        var pt=dpp*PERSEN;
        DPP.value=getNominal(dpp);
        PAJAK_TERUTANG.value=getNominal(pt);
      }
    }
  /* PENGGUNAAN_HARI_NON_METER */
  var PENGGUNAAN_HARI_NON_METER = document.getElementById('PENGGUNAAN_HARI_NON_METER');
  PENGGUNAAN_HARI_NON_METER.addEventListener('keyup', function(e)
  {
    PENGGUNAAN_HARI_NON_METER.value = formatRupiah(this.value);
  });  
  /* PENGGUNAAN_BULAN_NON_METER */
  var PENGGUNAAN_BULAN_NON_METER = document.getElementById('PENGGUNAAN_BULAN_NON_METER');
  PENGGUNAAN_BULAN_NON_METER.addEventListener('keyup', function(e)
  {
    PENGGUNAAN_BULAN_NON_METER.value = formatRupiah(this.value);
  });
  /* DEBIT_NON_METER */
  var DEBIT_NON_METER = document.getElementById('DEBIT_NON_METER');
  DEBIT_NON_METER.addEventListener('keyup', function(e)
  {
    DEBIT_NON_METER.value = formatRupiah(this.value);
  });  
</script>
  <?php } ?>
  <!-- edit -->
          </div>                
                    </div>
                  </div>
                </div>
                <?php if($disable==''){?>      
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                    <button type="submit" class="btn btn-primary" onClick="return validasi()"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?php echo site_url('sptpdair/air') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                  </div>
                </div>
                <?php } ?>      
              </div> 
            </div>
          </div>
              <div class="col-md-6">  

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak (%)
                          </label>
                          <div class="col-md-9">
                            <input type="text" id="PAJAK" name="PAJAK" <?php echo $disable?> required="required" placeholder="Pajak (%)" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK ?>" readonly>                  
                          </div>
                        </div> 
                      <div class="form-group" style="font-size: 12px !important">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Harga Dasar
                        </label>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >1-50</span>
                            <input disabled="" id="A" type="text" placeholder="" class="form-control col-md-2 col-xs-12" value="<?php echo $A ?>">                
                          </div>                
                        </div>
                        <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon" >51-500</span>
                            <input disabled="" id="B" type="text" placeholder="" class="form-control col-md-2 col-xs-12" value="<?php echo $B ?>">                
                          </div>                
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >501-1000</span>
                            <input disabled="" id="C" type="text" placeholder="" class="form-control col-md-2 col-xs-12" value="<?php echo $C ?>">                
                          </div>                
                        </div>
                        <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon" >1001-2500</span>
                            <input disabled="" id="D" type="text" placeholder="" class="form-control col-md-2 col-xs-12" value="<?php echo $D ?>">                
                          </div>                
                        </div>
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >2501-5000</span>
                            <input disabled="" id="E" type="text" placeholder="" class="form-control col-md-2 col-xs-12" value="<?php echo $E ?>">                
                          </div>                
                        </div>
                        <div class="col-md-5">
                          <div class="input-group">
                            <span class="input-group-addon" >5001-7500</span>
                            <input disabled="" id="F" type="text" placeholder="" class="form-control col-md-2 col-xs-12" value="<?php echo $F ?>">                
                          </div>                
                        </div>
                        <div class="col-md-3">         
                        </div>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-addon" >7500 <</span>
                            <input disabled="" id="G" type="text" placeholder="" class="form-control col-md-2 col-xs-12" value="<?php echo $G ?>">                
                          </div>                
                        </div>
                      </div>  
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input <?php echo $disable ?> type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                          </div>
                        </div>  
                      </div> 
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Yang Terutang <sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                          </div>
                        </div> 
                      </div>
                        <?php if ($STATUS=='2') {?>
                            <button type="button" id="<?php echo $ID_INC;?>" class="btn btn-warning trash togglee"><i class="fa fa-edit"></i> TETAPKAN</button>
                            <div class="badge" id="peringatan" style="visibility: hidden"><p> Berhasil ditetapkan</p></div>
                            <a href="javascript:history.back()" id='vis' style="visibility: hidden" class="btn btn-primary"><i class="fa fa-angle-double-left"> Kembali</i></a>
                        <?php 
                        } else {?>
                        <div class="badge"><p> Sudah ditetapkan</p></div>
                           <a href="javascript:history.back()" class="btn btn-primary"><i class="fa fa-angle-double-left"> Kembali</i></a><?php
                        }?>
                     
              </div>
                    

        </div>
      </div>
    </div>
  </div>
</div>      
</form>
</div>


</div>
<style>
label { font-size: 11px;}
textarea { font-size: 11px;}
.input-group span{ font-size: 11px;}
input[type='text'] { font-size: 11px; height:30px}
</style>
<script>
var persen=parseFloat(nominalFormat(document.getElementById("PERSEN").value));
   PAJAK.value=persen;
 $('#CARA_PENGAMBILAN').on('change', function() {
  if(this.value=='Meter'){
    $('#FORM_NON_METER').remove();
    $('#kontentdiag').load('/pdrd/sptpdair/Air/meter/<?php echo $id ?>'); 
  } else if (this.value=='Non Meter') {
    $('#FORM_METER').remove();
    $('#kontentdiag').load('/pdrd/sptpdair/Air/non_meter/<?php echo $id ?>'); 
  } else {
    $('#FORM_METER').remove();
    $('#FORM_NON_METER').remove();
  }
})
 function gantiTitikKoma(angka){
  return angka.toString().replace(/\./g,',');
}
function nominalFormat(angka){
  return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
}
function getNominal(angka){
  var nominal=gantiTitikKoma(angka);
  var indexKoma=nominal.indexOf(',');
  if (indexKoma==-1) {
    return ribuan(angka);
  } else {
    var ribu=nominal.substr(0,indexKoma);
    var koma=nominal.substr(indexKoma,3);
    return ribuan(ribu)+koma;        
  }
}
/* Fungsi */
function formatRupiah(angka, prefix)
{
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split = number_string.split(','),
  sisa  = split[0].length % 3,
  rupiah  = split[0].substr(0, sisa),
  ribuan  = split[0].substr(sisa).match(/\d{3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}
function ribuan (angka)
{
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan  = reverse.match(/\d{1,3}/g);
  ribuan  = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}      

function validasi(){
  var npwpd=document.forms["demo-form2"]["NPWPD"].value;
  var number=/^[0-9]+$/; 
  if (npwpd==null || npwpd==""){
    swal("NPWPD Harus di Isi", "", "warning")
    return false;
  };
}

     /* $( "#NPWPD" ).keyup(function() {
        var npwpd=$('#NPWPD').val();
         $.ajax({
                url: '<?php echo base_url()."Master/pokok/ceknpwpd"?>',
                type: 'POST',
                dataType: 'JSON',
                data: "npwpd="+npwpd,
                success: function (msg) {
                  alert(msg);
                }
            });
          });*/

          $("#NPWPD").keyup(function(){
            var npwpd = $("#NPWPD").val();
            $.ajax({
              url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
              type: 'POST',
              data: "npwpd="+npwpd,
              cache: false,
              success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
          });

        </script>
<script type="text/javascript">
  $(function(){
        $('.trash').click(function(){
            var del_id= $(this).attr('id');
            //alert(del_id);
            var $ele = $(this).parent().parent();
             
             var mp=document.getElementById("MP").innerHTML = "<?php echo $NB?>";
             var tp=document.getElementById("TP").innerHTML = "<?php echo $TAHUN_PAJAK?>";
             var fo="Data Berhasil Ditetapkan untuk ";
            $.ajax({
                  type:'POST',
                  url:'<?php echo base_url('sptpdair/air/proses_penetapan1'); ?>',
                  data:{del_id:del_id},
                  success: function(data){
                     alert(fo+" Masa Pajak "+mp+" Tahun Pajak "+tp);
                      //alert(data);
                     /*setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1);*/
                    
                              document.getElementById(del_id).style.visibility = 'hidden';
                              document.getElementById('vis').style.visibility = 'visible';
                              document.getElementById('peringatan').style.visibility = 'visible';
                        
                  }

                   })
              })
    });
</script>

