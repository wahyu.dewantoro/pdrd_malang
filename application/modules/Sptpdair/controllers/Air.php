<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Air extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mair');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->role=$this->session->userdata('MS_ROLE_ID');
        $this->nip=$this->session->userdata('NIP');
    }

    public function index()
    {
      if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
            $bulan=$_GET['MASA_PAJAK'];
            $tahun=$_GET['TAHUN_PAJAK'];
            $c_pengambilan=$_GET['NPWPD'];
    } else {
            $bulan='';//date('n');//date('d-m-Y');
            $tahun='';//ate('Y');//date('d-m-Y');
            $c_pengambilan='';
        }
        $sess=array(
                'ar_bulan'=>$bulan,
                'ar_tahun'=>$tahun,
                'c_pengambilan'=>$c_pengambilan
                
        );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','sptpdair/vlistair',$data);
  }

  public function penetapan_at(){
      $this->template->load('Welcome/halaman','sptpdair/penetapan_at');
  }
  

  public function json() {
    header('Content-Type: application/json');
    echo $this->Mair->json();
}

public function json_penetapan() {
    header('Content-Type: application/json');
    echo $this->Mair->json_penetapan();
}
public function meter($id="")
{

    $res=$this->Mair->getById($id);
    if($res){
        $data=array(
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER',number_format($res->PENGGUNAAN_HARI_INI_METER,'0','','.')),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER',number_format($res->PENGGUNAAN_BULAN_LALU_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',$res->DPP),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',$res->PAJAK_TERUTANG),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'disable'                     =>''
        );    
    } else{
        $data=array(
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER'),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER'),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER'),
            'DPP'                         => set_value('DPP'),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG'),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN'),
            'disable'                     =>''
        );          
    }
    $this->load->view('sptpdair/form_meter',$data);
}  
public function non_meter($id="")
{
    $res=$this->Mair->getById($id);
    if($res){
        $data=array(
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER',number_format($res->DEBIT_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER',number_format($res->PENGGUNAAN_HARI_NON_METER,'0','','.')),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER',number_format($res->PENGGUNAAN_BULAN_NON_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',$res->DPP),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',$res->PAJAK_TERUTANG),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'disable'                     => '',
        );    
    }
    else {
                $data=array(
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER'),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER'),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER'),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER'),
            'DPP'                         => set_value('DPP'),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG'),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN'),
            'disable'                     => '',
        ); 
    }
   $this->load->view('sptpdair/form_non_meter',$data);
}  

function create(){
    $data=array(
        'button'                      => 'Form SPTPD Air Tanah',
        'action'                      => base_url().'sptpdair/air/createaction',
        'ID_INC'                      => set_value('ID_INC'),
        'MASA_PAJAK'                  => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                 => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                     => set_value('NAMA_WP'),
        'ALAMAT_WP'                   => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                  => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                => set_value('ALAMAT_USAHA'),
        'NPWPD'                       => set_value('NPWPD'),
        'NO_BERKAS'                   => set_value('NO_BERKAS'),
        'NO_SIPA_SIPPAT'              => set_value('NO_SIPA_SIPPAT'),
        'TMT'                         => set_value('TMT'),
        'GOLONGAN_OP'                  => set_value('GOLONGAN_OP'),
        'PAJAK'                       => set_value('PAJAK'),
        'PERUNTUKAN'                  => set_value('PERUNTUKAN'),
        'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN'),
        'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER'),
        'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER'),
        'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER'),
        'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER'),
        'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER'),
        'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER'),
        'DPP'                         => set_value('DPP'),
        'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG'),
        'mp'                          =>$this->Mpokok->listMasapajak(),
        'peruntukan'                  =>$this->Mair->Peruntukan(),
        'golongan'                    =>$this->Mair->Gol(),
        'disable'                     => '',
        'cara'                        => '',
        'A'                           =>'',
        'B'                           =>'',
        'C'                           =>'',
        'D'                           =>'',
        'E'                           =>'',
        'F'                           =>'',
        'G'                           =>'',
        'id'                          =>'0',
        'param'                       =>'1',
        'NAMA_GOLONGAN'                => '',
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => ''
    );
    $data['jenis']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='' and jenis_pajak='08'")->result();
    $this->template->load('Welcome/halaman','sptpdair/vformair',$data);
}

function createaction(){
    $this->db->trans_start();
    $OP=$this->input->post('GOLONGAN_OP');
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
    $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
    $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
    $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
    $this->db->set('NAMA_USAHA',$exp[0]);
    $this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
    $this->db->set('NPWPD',$this->input->post('NPWPD'));
    $this->db->set('NO_BERKAS',$this->Mpokok->getNoBerkas());
    $this->db->set('NO_SIPA_SIPPAT',$this->input->post('NO_SIPA_SIPPAT'));
    $this->db->set('TMT',$this->input->post('TMT'));
    $this->db->set('ID_GOL_OP',$this->input->post('GOLONGAN_OP'));
    $this->db->set('PERUNTUKAN',$this->input->post('PERUNTUKAN'));
    $this->db->set('CARA_PENGAMBILAN',$this->input->post('CARA_PENGAMBILAN'));
    $this->db->set('PAJAK',$this->input->post('PAJAK'));
    $this->db->set('DEBIT_NON_METER',str_replace(',', '.', str_replace('.', '', $this->input->post('DEBIT_NON_METER') ) ));
    $this->db->set('PENGGUNAAN_HARI_NON_METER', str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_NON_METER'))));
    $this->db->set('PENGGUNAAN_BULAN_NON_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_NON_METER'))));
    $this->db->set('PENGGUNAAN_HARI_INI_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_INI_METER'))));
    $this->db->set('PENGGUNAAN_BULAN_LALU_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_LALU_METER'))));
    $this->db->set('VOLUME_AIR_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('VOLUME_AIR_METER'))));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->set('HARGA',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA'))));
    $this->db->set('NIP_INSERT',$this->nip);
    $this->db->set('TGL_INSERT',"sysdate",false);
    $this->db->set('TANGGAL_PENERIMAAN',"sysdate",false);
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('KODEKEC',$this->input->post('KECAMATAN'));
    $this->db->set('KODEKEL',$this->input->post('KELURAHAN'));

    $this->db->insert('SPTPD_AIR_TANAH');
    $this->db->trans_complete();
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di simpan";
    }else{
            // gagal
        $nt="Gagal di simpan";
    }

    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    
        if ($this->role=='10') {
            redirect(site_url('Upt/upt'));
        } else {
            redirect(site_url('sptpdair/air'));
        }
}

function update($id){
    $id=rapikan($id);
    $res=$this->Mair->getById($id);
    $row=$this->Mair->PeruntukanbyID($res->PERUNTUKAN);
    if($res){
        $NAMA_GOLONGAN = $this->Mpokok->getNamaGolongan($res->ID_GOL_OP);
        $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($res->KODEKEC);
        $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($res->KODEKEL,$res->KODEKEC);
        $data=array(
            'button'                      => 'Form SPTPD Air Tanah',
            'action'                      => base_url().'sptpdair/air/upadateaction',
            'cara'                        =>'readonly',
            'ID_INC'                      => set_value('ID_INC',$res->ID_INC),
            'MASA_PAJAK'                  => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'                 => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                     => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                   => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                  => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'                => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                       => set_value('NPWPD',$res->NPWPD),
            'NO_BERKAS'                   => set_value('NO_BERKAS',$res->NO_BERKAS),
            'NO_SIPA_SIPPAT'              => set_value('NO_SIPA_SIPPAT',$res->NO_SIPA_SIPPAT),
            'TMT'                         => set_value('TMT',$res->TMT),
            'GOLONGAN_OP'                  => set_value('GOLONGAN_OP',$res->ID_GOL_OP),
            'PAJAK'                       => set_value('PAJAK',$res->PAJAK),
            'PERUNTUKAN'                  => set_value('PERUNTUKAN',$res->PERUNTUKAN),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER',number_format($res->DEBIT_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER',number_format($res->PENGGUNAAN_HARI_NON_METER,'0','','.')),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER',number_format($res->PENGGUNAAN_BULAN_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER',number_format($res->PENGGUNAAN_HARI_INI_METER,'0','','.')),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER',number_format($res->PENGGUNAAN_BULAN_LALU_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'mp'                          =>$this->Mpokok->listMasapajak(),
            'disable'                     => '',
            'cara'                        =>'disabled',
            'disable1'=> 'true',
            'peruntukan'                  =>$this->Mair->Peruntukan(),
            'golongan'                    =>$this->Mair->Gol(),
            'A'                           =>number_format($row->S1D50,'0','','.'),
            'B'                           =>number_format($row->S51D500,'0','','.'),
            'C'                           =>number_format($row->S501D1000,'0','','.'),
            'D'                           =>number_format($row->S1001D2500,'0','','.'),
            'E'                           =>number_format($row->S2501D5000,'0','','.'),
            'F'                           =>number_format($row->S5001D7500,'0','','.'),
            'G'                           =>number_format($row->S7500D,'0','','.'),
            'id'                          => $id,
            'param'                       => '' ,
            'NAMA_GOLONGAN'       => $NAMA_GOLONGAN->DESKRIPSI,
            'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
            'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN, 
        );
        $this->template->load('Welcome/halaman','sptpdair/vformair',$data);
    }else{
        redirect('sptpdair/air');
    }
}

function upadateaction(){
    $this->db->trans_start();
    $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
    $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
    $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
    $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
    $this->db->set('NAMA_USAHA',$this->input->post('NAMA_USAHA'));
    $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
    $this->db->set('NPWPD',$this->input->post('NPWPD'));
    $this->db->set('NO_BERKAS',$this->input->post('NO_BERKAS'));
    $this->db->set('NO_SIPA_SIPPAT',$this->input->post('NO_SIPA_SIPPAT'));
    $this->db->set('TMT',$this->input->post('TMT'));
    $this->db->set('PERUNTUKAN',$this->input->post('PERUNTUKAN'));
    $this->db->set('CARA_PENGAMBILAN',$this->input->post('CARA_PENGAMBILAN'));
    $this->db->set('PAJAK',$this->input->post('PAJAK'));
    $this->db->set('DEBIT_NON_METER',str_replace(',', '.', str_replace('.', '', $this->input->post('DEBIT_NON_METER') ) ));
    $this->db->set('PENGGUNAAN_HARI_NON_METER', str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_NON_METER'))));
    $this->db->set('PENGGUNAAN_BULAN_NON_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_NON_METER'))));
    $this->db->set('PENGGUNAAN_HARI_INI_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_INI_METER'))));
    $this->db->set('PENGGUNAAN_BULAN_LALU_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_LALU_METER'))));
    $this->db->set('VOLUME_AIR_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('VOLUME_AIR_METER'))));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->where('ID_INC',$this->input->post('ID_INC'));
    $this->db->update('SPTPD_AIR_TANAH');
    $this->db->trans_complete();
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di update";
    }else{
            // gagal
        $nt="Gagal di update";
    }

    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    
    redirect('sptpdair/air');   
}

function delete($id){
    $id=rapikan($id);
    $res=$this->Mair->getById($id);
    if($res){
        $db=$this->db->query("DELETE SPTPD_AIR_TANAH WHERE ID_INC='$id'");
        if($db){
            $nt="Data berhasil di hapus";    
        }else{
            $nt="Data tidak di hapus";    
        }
    }else{
        $nt="Data tidak ada";
    }
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    redirect('sptpdair/air');   
}

function jsonair(){
    header('Content-Type: application/json');
    echo $this->Mair->jsonair();     
}

function detail($id){
    $id=rapikan($id);
    $res=$this->Mair->getById($id);
    $row=$this->Mair->PeruntukanbyID($res->PERUNTUKAN);
    if($res){
        $NAMA_GOLONGAN = $this->Mpokok->getNamaGolongan($res->ID_GOL_OP);
        $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($res->KODEKEC);
        $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($res->KODEKEL,$res->KODEKEC);
        $data=array(
            'button'                      => 'SPTPD Air Tanah',
            'action'                      => base_url().'sptpdair/air/upadateaction',
            'ID_INC'                      => set_value('ID_INC',$res->ID_INC),
            'MASA_PAJAK'                  => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'                 => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                     => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                   => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                  => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'                => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                       => set_value('NPWPD',$res->NPWPD),
            'NO_BERKAS'                   => set_value('NO_BERKAS',$res->NO_BERKAS),
            'NO_SIPA_SIPPAT'              => set_value('NO_SIPA_SIPPAT',$res->NO_SIPA_SIPPAT),
            'TMT'                         => set_value('TMT',$res->TMT),
            'GOLONGAN_OP'                  => set_value('GOLONGAN_OP',$res->ID_GOL_OP),
            'PAJAK'                  => set_value('PAJAK',$res->PAJAK),
            'PERUNTUKAN'                  => set_value('PERUNTUKAN',$res->PERUNTUKAN),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER',number_format($res->DEBIT_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER',number_format($res->PENGGUNAAN_HARI_NON_METER,'0','','.')),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER',number_format($res->PENGGUNAAN_BULAN_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER',number_format($res->PENGGUNAAN_HARI_INI_METER,'0','','.')),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER',number_format($res->PENGGUNAAN_BULAN_LALU_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'mp'                          =>$this->Mpokok->listMasapajak(),
            'disable'=> 'disabled',
            'disable1'=> 'true',
            'cara'=> '',
            'peruntukan'                          =>$this->Mair->Peruntukan(),
            'golongan'                          =>$this->Mair->Gol(),
            'A'                           =>$row->S1D50,
            'B'                           =>$row->S51D500,
            'C'                           =>$row->S501D1000,
            'D'                           =>$row->S1001D2500,
            'E'                           =>$row->S2501D5000,
            'F'                           =>$row->S5001D7500,
            'G'                           =>$row->S7500D,
            'NAMA_GOLONGAN'       => $NAMA_GOLONGAN->DESKRIPSI,
            'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
            'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN,
        );
        $this->template->load('Welcome/halaman','sptpdair/vformair',$data);
    }else{
        $nt="Data tidak ada";
        $this->session->set_flashdata('notif', '<script>
          $(window).load(function(){
             swal("'.$nt.'", "", "success")
         });
         </script>');
        redirect('sptpdair/air');   
    }

}
function form_penetapan($id){
    $id=rapikan($id);
    $res=$this->Mair->getById($id);
    $row=$this->Mair->PeruntukanbyID($res->PERUNTUKAN);
    if($res){
        $data=array(
            'id'                          => $id,
            'button'                      => 'Penetapan SPTPD Air Tanah',
            'action'                      => base_url().'sptpdair/air/upadateaction',
            'ID_INC'                      => set_value('ID_INC',$res->ID_INC),
            'STATUS'                      => set_value('STATUS',$res->STATUS),
            'MASA_PAJAK'                  => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'                 => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                     => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                   => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                  => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'                => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                       => set_value('NPWPD',$res->NPWPD),
            'NO_BERKAS'                   => set_value('NO_BERKAS',$res->NO_BERKAS),
            'NO_SIPA_SIPPAT'              => set_value('NO_SIPA_SIPPAT',$res->NO_SIPA_SIPPAT),
            'TMT'                         => set_value('TMT',$res->TMT),
            'GOLONGAN_OP'                 => set_value('GOLONGAN_OP',$res->ID_GOL_OP),
            'PAJAK'                       => set_value('PAJAK',$res->PAJAK),
            'PERUNTUKAN'                  => set_value('PERUNTUKAN',$res->PERUNTUKAN),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER',number_format($res->DEBIT_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER',number_format($res->PENGGUNAAN_HARI_NON_METER,'0','','.')),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER',number_format($res->PENGGUNAAN_BULAN_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER',number_format($res->PENGGUNAAN_HARI_INI_METER,'0','','.')),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER',number_format($res->PENGGUNAAN_BULAN_LALU_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'mp'                          =>$this->Mpokok->listMasapajak(),
            'disable'=> 'disabled',
            'cara'=> '',
            'peruntukan'                          =>$this->Mair->Peruntukan(),
            'golongan'                          =>$this->Mair->Gol(),
            'A'                           =>$row->S1D50,
            'B'                           =>$row->S51D500,
            'C'                           =>$row->S501D1000,
            'D'                           =>$row->S1001D2500,
            'E'                           =>$row->S2501D5000,
            'F'                           =>$row->S5001D7500,
            'G'                           =>$row->S7500D,
        );
        $this->template->load('Welcome/halaman','sptpdair/penetapan_air',$data);
    }else{
        $nt="Data tidak ada";
        $this->session->set_flashdata('notif', '<script>
          $(window).load(function(){
             swal("'.$nt.'", "", "success")
         });
         </script>');
        redirect('sptpdair/air');   
    }

}
function proses_penetapan1(){
          $id=$_POST['del_id'];
          $NIP=$this->nip;
          $row=$this->db->query("SELECT nama_wp,PAJAK_TERUTANG,STATUS from SPTPD_AIR_TANAH where ID_INC='$id'")->row();
          
          $pt=$row->PAJAK_TERUTANG;
          $nm=$row->NAMA_WP;
          if ($row->STATUS=='2') {
              $kb=$this->Mpokok->getSqIdBiling();
              $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
              $ed=$expired->ED;
              $ed_tabel=$expired->NOW;
              $KD_BILING="3507801||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'";
              $va=$this->Mpokok->getVA('834');
              $this->db->query("UPDATE SPTPD_AIR_TANAH 
                                SET STATUS ='0',TGL_KETETAPAN=SYSDATE,KODE_BILING=$KD_BILING,NO_VA='$va',ED_VA='$ed_tabel' 
                                WHERE ID_INC ='$id'");
              $this->db->query("CALL P_INSERT_TAGIHAN_AIR_TANAH('$id')");
              $this->Mpokok->registration($va,$nm,$pt,$ed);
          } else {
              # code...
          }
          //$this->db->last_query();
          
}

function get_nama_usaha(){
        $data1=$_POST['npwp'];
        
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$data1."' and jenis_pajak='08'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
function get_alamat_usaha(){
        $data1=$_POST['nama'];
        
            //$data['parent']=$parent;
        $data=$this->db->query("SELECT nama_usaha,ALAMAT_USAHA from tempat_usaha where nama_usaha='".$data1."' and jenis_pajak='08'")->row();
            // print_r($data);
        echo $data->ALAMAT_USAHA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    function pdf_kode_biling($id_inc=""){
            $kb=$this->db->query("select KODE_BILING from SPTPD_AIR_TANAH where id_inc='$id_inc'")->row();
            $data['catatan']=$this->Mpokok->getCatatan();
            $data['cara_bayar']=$this->db->query("select * from ms_catatan where keperluan='va' ")->result();
            $kode_biling=$kb->KODE_BILING;
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Pdf_kode_biling',$data,true);
            
            $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
   



}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */