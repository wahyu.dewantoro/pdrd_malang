<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Air_bl extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Mair');
        $this->load->model('Master/Mpokok');
        //$this->load->model('Upt/Mupt');
        $this->load->library('form_validation');        
        $this->load->library('datatables');

        $this->nip=$this->session->userdata('NIP');
    }

    public function air_bulan_lalu()
    { 
      if(isset($_POST['MASA_PAJAK']) && isset($_POST['TAHUN_PAJAK'])){
            $bulan      =$_POST['MASA_PAJAK'];
            $tahun      =$_POST['TAHUN_PAJAK'];
            $bulan_depan=$_POST['MASA_PAJAK_SELANJUTNYA'];
            $upt=$_POST['upt'];
            $kecamatan=$_POST['kecamatan'];
             $kelurahan=$_POST['kelurahan'];

        } else {
            $bulan      ='';//date('d-m-Y');
            $tahun      ='';//date('d-m-Y');
            $bulan_depan=date('m');
            $upt='';
            $kecamatan='';
            $kelurahan='';
           
        }
        $sess=array(
                'ar_bulan_bl'=>$bulan,
                'ar_tahun_bl'=>$tahun,
                'ar_bulan_bl_selanjutnya'=>$bulan_depan,
                'ar_upt_bl'=>$upt,
                'ar_kec_bl'=>$kecamatan,
                'ar_kel_bl'=>$kelurahan
                                
        );
        $this->session->set_userdata($sess);  
      $data['mp']=$this->Mpokok->listMasapajak();
      $data['mp_s']=$this->Mpokok->listMasapajak(); 
      $data['list_upt']=$this->db->query("SELECT * FROM UNIT_UPT WHERE ID_INC !='2'")->result();
       $data['kel']=$this->db->query("SELECT * FROM KELURAHAN WHERE KODEKEC='$kecamatan'")->result();
       $data['kec']=$this->db->query("SELECT * FROM KECAMATAN WHERE KODE_UPT='$upt'")->result(); 
      $this->template->load('Welcome/halaman','sptpdair/air_pilihan_bulan_lalu',$data);
  }
function jsonair_bl(){
    header('Content-Type: application/json');
    echo $this->Mair->jsonair_bl();     
}
 public function cari_data_bl(){
    $TAHUN_PAJAK=$this->input->post('TAHUN_PAJAK');
    $MASA_PAJAK =$this->input->post('MASA_PAJAK');
    //$NPWPD      =$this->input->post('NPWPD');
    error_reporting(E_ALL^(E_NOTICE|E_WARNING));
    $cek_id     =$this->db->query("select id_inc from sptpd_air_tanah where masa_pajak='$MASA_PAJAK' and tahun_pajak='$TAHUN_PAJAK' and npwpd='$NPWPD'")->row();
    if ($cek_id->ID_INC!='') {
        redirect('sptpdair/air_bl/data_bulan_lalu/'.$cek_id->ID_INC);
    } else {
      $this->session->set_flashdata('notif','<div class="badge">
                                                             <p> Data Tidak ditemukan !</p>
                                                            </div>');
     redirect('sptpdair/Air_bl/air_bulan_lalu');
    }
 }

public function meter($id)
{

    $res=$this->Mair->getById($id);
    if($res){
        $data=array(
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER',number_format($res->PENGGUNAAN_HARI_INI_METER,'0','','.')),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER',number_format($res->PENGGUNAAN_BULAN_LALU_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',$res->DPP),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',$res->PAJAK_TERUTANG),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'disable'                     =>''
        );    
    } else{
        $data=array(
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER'),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER'),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER'),
            'DPP'                         => set_value('DPP'),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG'),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN'),
            'disable'                     =>''
        );          
    }
    $this->load->view('sptpdair/form_meter',$data);
}  
public function non_meter($id)
{
    $res=$this->Mair->getById($id);
    if($res){
        $data=array(
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER',number_format($res->DEBIT_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER',number_format($res->PENGGUNAAN_HARI_NON_METER,'0','','.')),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER',number_format($res->PENGGUNAAN_BULAN_NON_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',$res->DPP),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',$res->PAJAK_TERUTANG),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'disable'                     => '',
        );    
    }
    else {
                $data=array(
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER'),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER'),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER'),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER'),
            'DPP'                         => set_value('DPP'),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG'),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN'),
            'disable'                     => '',
        ); 
    }
   $this->load->view('sptpdair/form_non_meter',$data);
}  

function createaction(){
    $this->db->trans_start();
    $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
    $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
    $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
    $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
    $this->db->set('NAMA_USAHA',$this->input->post('NAMA_USAHA'));
    $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
    $this->db->set('NPWPD',$this->input->post('NPWPD'));
    $this->db->set('NO_BERKAS',$this->input->post('NO_BERKAS'));
    $this->db->set('NO_SIPA_SIPPAT',$this->input->post('NO_SIPA_SIPPAT'));
    $this->db->set('TMT',$this->input->post('TMT'));
    $this->db->set('ID_GOL_OP',$this->input->post('GOLONGAN_OP'));
    $this->db->set('PERUNTUKAN',$this->input->post('PERUNTUKAN'));
    $this->db->set('CARA_PENGAMBILAN',$this->input->post('CARA_PENGAMBILAN'));
    $this->db->set('PAJAK',$this->input->post('PAJAK'));
    $this->db->set('DEBIT_NON_METER',str_replace(',', '.', str_replace('.', '', $this->input->post('DEBIT_NON_METER') ) ));
    $this->db->set('PENGGUNAAN_HARI_NON_METER', str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_NON_METER'))));
    $this->db->set('PENGGUNAAN_BULAN_NON_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_NON_METER'))));
    $this->db->set('PENGGUNAAN_HARI_INI_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_HARI_INI_METER'))));
    $this->db->set('PENGGUNAAN_BULAN_LALU_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('PENGGUNAAN_BULAN_LALU_METER'))));
    $this->db->set('VOLUME_AIR_METER',str_replace(',', '.', str_replace('.', '',$this->input->post('VOLUME_AIR_METER'))));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->set('NIP_INSERT',$this->nip);
    $this->db->set('TGL_INSERT',"sysdate",false);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->insert('SPTPD_AIR_TANAH');
    $this->db->trans_complete();
    if ($this->db->trans_status() == TRUE){
            // sukses
        $nt="Berhasil di simpan";
    }else{
            // gagal
        $nt="Gagal di simpan";
    }

    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    
    redirect('sptpdair/air');
}

function data_bulan_lalu($id){
    $res=$this->Mair->getById($id);
    $row=$this->Mair->PeruntukanbyID($res->PERUNTUKAN);
    if($res){
        $data=array(
            'button'                      => 'Form SPTPD Air Tanah',
            'action'                      => base_url().'sptpdair/air_bl/createaction',
            'cara'                        =>'readonly',
            'ID_INC'                      => set_value('ID_INC',$res->ID_INC),
            'MASA_PAJAK'                  => set_value('MASA_PAJAK',$res->MASA_PAJAK),
            'TAHUN_PAJAK'                 => set_value('TAHUN_PAJAK',$res->TAHUN_PAJAK),
            'NAMA_WP'                     => set_value('NAMA_WP',$res->NAMA_WP),
            'ALAMAT_WP'                   => set_value('ALAMAT_WP',$res->ALAMAT_WP),
            'NAMA_USAHA'                  => set_value('NAMA_USAHA',$res->NAMA_USAHA),
            'ALAMAT_USAHA'                => set_value('ALAMAT_USAHA',$res->ALAMAT_USAHA),
            'NPWPD'                       => set_value('NPWPD',$res->NPWPD),
            'NO_BERKAS'                   => set_value('NO_BERKAS',$res->NO_BERKAS),
            'NO_SIPA_SIPPAT'              => set_value('NO_SIPA_SIPPAT',$res->NO_SIPA_SIPPAT),
            'TMT'                         => set_value('TMT',$res->TMT),
            'GOLONGAN_OP'                  => set_value('GOLONGAN_OP',$res->ID_GOL_OP),
            'PAJAK'                       => set_value('PAJAK',$res->PAJAK),
            'PERUNTUKAN'                  => set_value('PERUNTUKAN',$res->PERUNTUKAN),
            'CARA_PENGAMBILAN'            => set_value('CARA_PENGAMBILAN',$res->CARA_PENGAMBILAN),
            'DEBIT_NON_METER'             => set_value('DEBIT_NON_METER',number_format($res->DEBIT_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_NON_METER'   => set_value('PENGGUNAAN_HARI_NON_METER',number_format($res->PENGGUNAAN_HARI_NON_METER,'0','','.')),
            'PENGGUNAAN_BULAN_NON_METER'  => set_value('PENGGUNAAN_BULAN_NON_METER',number_format($res->PENGGUNAAN_BULAN_NON_METER,'0','','.')),
            'PENGGUNAAN_HARI_INI_METER'   => set_value('PENGGUNAAN_HARI_INI_METER',number_format($res->PENGGUNAAN_HARI_INI_METER,'0','','.')),
            'PENGGUNAAN_BULAN_LALU_METER' => set_value('PENGGUNAAN_BULAN_LALU_METER',number_format($res->PENGGUNAAN_BULAN_LALU_METER,'0','','.')),
            'VOLUME_AIR_METER'            => set_value('VOLUME_AIR_METER',number_format($res->VOLUME_AIR_METER,'2',',','.')),
            'DPP'                         => set_value('DPP',number_format($res->DPP,'0','','.')),
            'PAJAK_TERUTANG'              => set_value('PAJAK_TERUTANG',number_format($res->PAJAK_TERUTANG,'0','','.')),
            'mp'                          =>$this->Mpokok->listMasapajak(),
            'disable'                     => '',
            'cara'                        =>'disabled',
            'peruntukan'                  =>$this->Mair->Peruntukan(),
            'golongan'                    =>$this->Mair->Gol(),
            'A'                           =>number_format($row->S1D50,'0','','.'),
            'B'                           =>number_format($row->S51D500,'0','','.'),
            'C'                           =>number_format($row->S501D1000,'0','','.'),
            'D'                           =>number_format($row->S1001D2500,'0','','.'),
            'E'                           =>number_format($row->S2501D5000,'0','','.'),
            'F'                           =>number_format($row->S5001D7500,'0','','.'),
            'G'                           =>number_format($row->S7500D,'0','','.'),
            'id'                          =>'0'
        );
        $this->template->load('Welcome/halaman','sptpdair/vformair_bulan_lalu',$data);
    }else{
        redirect('sptpdair/air');
    }
}
function entry_airtanah_masa_lalu(){
   $NOW=date('Y-m-d H:i:s');
   $TP=date('Y');
   $MP=$_POST['masa_selanjutnya'];
   $kb=$this->Mpokok->getSqIdBiling();
   $KD_BILING="35070||8||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'";
   $NIP=$this->nip;
   $IP=$this->Mpokok->getIP();
   $berkas=$this->Mpokok->getNoBerkas();
   $jumlah= count($_POST['id_inc']);    
   for($i=0; $i < $jumlah; $i++) { 
   $id_i=$_POST['id_inc'][$i];
    $kb=$this->Mpokok->getSqIdBiling();
    $KD_BILING="3507801||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'";
        $this->db->query("INSERT INTO SPTPD_AIR_TANAH (
                         MASA_PAJAK, TAHUN_PAJAK, NAMA_WP, ALAMAT_WP, NAMA_USAHA, ALAMAT_USAHA, NPWPD, NO_BERKAS, NO_SIPA_SIPPAT, TMT, PERUNTUKAN,
                         CARA_PENGAMBILAN, DEBIT_NON_METER, PENGGUNAAN_HARI_NON_METER, PENGGUNAAN_BULAN_NON_METER, PENGGUNAAN_HARI_INI_METER, PENGGUNAAN_BULAN_LALU_METER, 
                         VOLUME_AIR_METER, DPP, PAJAK_TERUTANG, KURANG_BAYAR, SANKSI_ADMINISTRASI, JUMLAH_PAJAK_TERUTANG, JUMLAH_KETETAPAN, TGL_KETETAPAN, JUMLAH_BAYAR, TGL_BAYAR, 
                         NIP_INSERT, NPWP_INSERT, IP_INSERT, TGL_INSERT, ID_GOL_OP, PAJAK, STATUS, KODE_BILING, TANGGAL_PENERIMAAN,NIP_PENETAPAN,KODE_REK,KODEKEC,KODEKEL,HARGA,ID_USAHA,AMBIL_BULAN_LALU)
                         SELECT   '$MP', '$TP', NAMA_WP, ALAMAT_WP, NAMA_USAHA, ALAMAT_USAHA, NPWPD, '$berkas', NO_SIPA_SIPPAT, TMT, PERUNTUKAN,
                         CARA_PENGAMBILAN, DEBIT_NON_METER, PENGGUNAAN_HARI_NON_METER, PENGGUNAAN_BULAN_NON_METER, PENGGUNAAN_HARI_INI_METER, PENGGUNAAN_BULAN_LALU_METER, 
                         VOLUME_AIR_METER, DPP, PAJAK_TERUTANG, KURANG_BAYAR, SANKSI_ADMINISTRASI, JUMLAH_PAJAK_TERUTANG, JUMLAH_KETETAPAN, SYSDATE, '', '', 
                         '$NIP', NPWP_INSERT, '$IP', SYSDATE, ID_GOL_OP, PAJAK, '0', $KD_BILING, SYSDATE,'$NIP',KODE_REK,KODEKEC,KODEKEL,HARGA,ID_USAHA,'1' FROM SPTPD_AIR_TANAH
                         WHERE ID_INC='".$_POST['id_inc'][$i]."'");
             $max=$this->db->query("select max(id_inc)id_inc from SPTPD_AIR_TANAH where nip_insert='$NIP' AND IP_INSERT='$IP' ")->row();
						 $this->db->query("CALL P_INSERT_TAGIHAN_AIR_TANAH('$max->ID_INC')");
    }
            if ($jumlah<=0){
                   // gagal 
                 $nt="Gagal di simpan";
                 $this->session->set_flashdata('notif', '<script>
                      $(window).load(function(){
                         swal("'.$nt.'", "", "warning")
                     });
                     </script>');
                 redirect('sptpdair/Air_bl/air_bulan_lalu');
            }else{
                    // sukses
                $nt="Berhasil di simpan";
                $this->session->set_flashdata('notif', '<script>
                      $(window).load(function(){
                         swal("'.$nt.'", "", "success")
                     });
                     </script>');
               redirect('sptpdair/air');
            } 
    
    
   //echo '<pre>',print_r($a,1),'</pre>';

    }
    function opsi_bulanlalu(){
             $this->template->load('Welcome/halaman','sptpdair/opsi_bulan_lalu');
}
}
/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */