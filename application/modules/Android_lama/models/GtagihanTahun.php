<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class GtagihanTahun extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function gTagihanTahun($npwp,$idOp){
		$sql = "SELECT TAHUN_PAJAK FROM TAGIHAN WHERE NPWPD = '$npwp' AND ID_OP = '$idOp' GROUP BY TAHUN_PAJAK ORDER BY TAHUN_PAJAK DESC";
		$result = $this->db->query($sql)->result_array();
		if (count($result) > 0) {
			$response["success"] = 1;
			$response["message"] = "Berhasil.";
			$response["listTahun"] = array();
			$arrayTahun = array();
			//create an array
			foreach ($result as $row) {
				$tahunPajak = $row["TAHUN_PAJAK"];
				if (!in_array($tahunPajak, $arrayTahun)) {
					$emparray["tahun"] = $tahunPajak;
					array_push($arrayTahun, $tahunPajak);
					array_push($response["listTahun"], $emparray);
				}
			}
			echo json_encode($response);
			
			//close the db connection
			
		} else {
			$response["success"] = 0;
			$response["message"] = "Tidak ada data yang ditemukan";

			echo json_encode($response);
		}
	}
}