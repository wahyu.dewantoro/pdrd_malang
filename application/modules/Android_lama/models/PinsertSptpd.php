<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PinsertSptpd extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function pInsertSptpd($valuesString){
        
        $itemsString = explode('|',$valuesString,31);
        $jnsPajak = $itemsString['0'];
        $noform = $itemsString['1'];
        $msPajak = $itemsString['2'];
        $thnPajak = $itemsString['3'];
        $nmWp = $itemsString['4'];
        $almtWp = $itemsString['5'];
        $idUsaha = $itemsString['6'];
        $nmUsaha = $itemsString['7'];
        $almtUsaha = $itemsString['8'];
        $npwpd = $itemsString['9'];
        $tglPenerimaan = $itemsString['10'];
        $berlakuMulai = $itemsString['11'];
        $kodeKec = $itemsString['12'];
        $kodeKel = $itemsString['13'];
        $idOp = $itemsString['14'];
        $dsrPengenaan = $itemsString['15'];
        $jnsEntrian = $itemsString['16'];
        $dpp = $itemsString['17'];
        $npwpInsert = $itemsString['18'];
        $ipInsert = $itemsString['19'];
        $tglInsert = $itemsString['20'];
        $pjkTerutang = $itemsString['21'];
        $masa = $itemsString['22'];
        $pajak = $itemsString['23'];
        $status = $itemsString['24'];
        $rek = $itemsString['25'];
        $kodeBilling = $itemsString['26'];
        $device = $itemsString['27'];
        $statusPengiriman = $itemsString['28'];
        $uploadedFiles = $itemsString['29'];

        $dataBulan = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        );

        // print("msPajak $msPajak, thnPajak $thnPajak, nmWp $nmWp, almtWp $almtWp, idUsaha $idUsaha, nmUsaha $nmUsaha
        // , almtUsaha $almtUsaha, npwpd $npwpd, tglPenerimaan $tglPenerimaan
        // , berlakuMulai $berlakuMulai, kodeKec $kodeKec, kodeKel $kodeKel, idOp $idOp, dsrPengenaan $dsrPengenaan
        // , jnsEntrian $jnsEntrian, dpp $dpp
        // , npwpInsert $npwpInsert, ipInsert $ipInsert, tglInsert $tglInsert, pjkTerutang $pjkTerutang, masa $masa, pajak $pajak
        // , status $status, rek $rek, kodeBilling $kodeBilling, device $device, statusPengiriman $statusPengiriman, uploadedFiles $uploadedFiles");
        // return;

        if ($statusPengiriman != 2 ) {

            if ($jnsPajak == 1) {
                $sql = "SELECT * FROM SPTPD_HOTEL WHERE MASA_PAJAK = '$msPajak' 
                AND TAHUN_PAJAK = '$thnPajak' AND NPWPD = '$npwpd' AND ID_USAHA = '$idUsaha'";
            } else if ($jnsPajak == 2) {
                $sql = "SELECT * FROM SPTPD_RESTORAN WHERE MASA_PAJAK = '$msPajak' 
                AND TAHUN_PAJAK = '$thnPajak' AND NPWPD = '$npwpd' AND ID_USAHA = '$idUsaha'";
            } else if ($jnsPajak == 3) {
                $sql = "SELECT * FROM SPTPD_HIBURAN WHERE MASA_PAJAK = '$msPajak' 
                AND TAHUN_PAJAK = '$thnPajak' AND NPWPD = '$npwpd' AND ID_TEMPAT_USAHA = '$idUsaha'";
            } else if ($jnsPajak == 7) {
                $sql = "SELECT * FROM SPTPD_PARKIR WHERE MASA_PAJAK = '$msPajak' 
                AND TAHUN_PAJAK = '$thnPajak' AND NPWPD = '$npwpd' AND ID_TEMPAT_USAHA = '$idUsaha'";
            }

            // echo $sql;
            // return;

            $result = $this->db->query($sql)->result_array();
		    if (count($result) > 0) {
                $response["success"] = 1;
                $response["message"] = "Anda sudah membuat laporan bulan $dataBulan[$msPajak]."
                                ."\nApakah anda ingin menambahkan untuk masa pajak bulan $dataBulan[$msPajak]?";
                
                echo json_encode($response);
                return;
            } else {
                $response["success"] = 2;
                $response["message"] = "Berhasil";
                
                echo json_encode($response);
                return;
            }
        }


        $filesPath = "";
        
        if ($uploadedFiles != "") {
            $tags = explode(',' , $uploadedFiles);
            $i = 0;

            foreach ($tags as $item) {
                if (isset($_FILES["$item"]["name"])) {
                    $filePath = $filePathAbsolute . basename( $_FILES["$item"]['name']);
                    if(move_uploaded_file($_FILES["$item"]['tmp_name'], $filePath)) {
                        $sukses += "$filePath";
                        if ($i == 0) {
                            $filesPath .= basename( $_FILES["$item"]['name']);
                        } else {
                            $filesPath .= ",".basename( $_FILES["$item"]['name']);
                        }
                        $i++;
                    } else { $sukses .= "fail";}
                } else { 
                    $sukses .= "fail1";
                    return $sukses;
                }
            }
        }

        if ($jnsPajak == 1) {
            $sql = "INSERT INTO SPTPD_HOTEL(NO_FORMULIR,MASA_PAJAK, TAHUN_PAJAK, NAMA_WP, ALAMAT_WP
                , ID_USAHA, NAMA_USAHA, ALAMAT_USAHA, NPWPD, TANGGAL_PENERIMAAN, BERLAKU_MULAI
                , KODEKEC, KODEKEL, ID_OP, DASAR_PENGENAAN, JENIS_ENTRIAN, DPP, NPWP_INSERT
                , IP_INSERT, TGL_INSERT, PAJAK_TERUTANG, MASA, PAJAK, STATUS, KODE_REK, TGL_KETETAPAN
                , KODE_BILING,DEVICE,FILE_TRANSAKSI)
            VALUES ('$noform','$msPajak', '$thnPajak', '$nmWp', '$almtWp'
                , '$idUsaha', '$nmUsaha', '$almtUsaha'
                , '$npwpd', to_date('$tglPenerimaan','dd/mm/yyyy')
                , to_date('$berlakuMulai','dd/mm/yyyy'), '$kodeKec', '$kodeKel'
                , '$idOp', '$dsrPengenaan', '$jnsEntrian', '$dpp', '$npwpInsert'
                , '$ipInsert', SYSDATE, '$pjkTerutang', '$masa', '$pajak', '$status', '$rek'
                , to_date('$berlakuMulai','dd/mm/yyyy'),$kodeBilling,'$device','$filesPath')";
        } else if ($jnsPajak == 2) {
            $sql = "INSERT INTO SPTPD_RESTORAN(NO_FORMULIR,MASA_PAJAK, TAHUN_PAJAK, NAMA_WP, ALAMAT_WP
                , ID_USAHA, NAMA_USAHA, ALAMAT_USAHA, NPWPD, TANGGAL_PENERIMAAN, BERLAKU_MULAI
                , KODEKEC, KODEKEL, ID_OP, DASAR_PENGENAAN, JENIS_ENTRIAN, DPP, NPWP_INSERT
                , IP_INSERT, TGL_INSERT, PAJAK_TERUTANG, MASA, PAJAK, STATUS, KODE_REK, TGL_KETETAPAN
                , KODE_BILING,DEVICE,FILE_TRANSAKSI)
            VALUES ('$noform','$msPajak', '$thnPajak', '$nmWp', '$almtWp'
                , '$idUsaha', '$nmUsaha', '$almtUsaha'
                , '$npwpd', to_date('$tglPenerimaan','dd/mm/yyyy')
                , to_date('$berlakuMulai','dd/mm/yyyy'), '$kodeKec', '$kodeKel'
                , '$idOp', '$dsrPengenaan', '$jnsEntrian', '$dpp', '$npwpInsert'
                , '$ipInsert', SYSDATE, '$pjkTerutang', '$masa', '$pajak', '$status', '$rek'
                , to_date('$berlakuMulai','dd/mm/yyyy'),$kodeBilling,'$device','$filesPath')";
        } else if ($jnsPajak == 3) {
            $sql = "INSERT INTO SPTPD_HIBURAN(NO_FORMULIR,MASA_PAJAK, TAHUN_PAJAK, NAMA_WP, ALAMAT_WP
                , ID_TEMPAT_USAHA, NAMA_USAHA, ALAMAT_USAHA, NPWPD, TANGGAL_PENERIMAAN, BERLAKU_MULAI
                , KODEKEC, KODEKEL, ID_OP, DASAR_PENGENAAN, JENIS_ENTRIAN, DPP, NPWP_INSERT
                , IP_INSERT, TGL_INSERT, PAJAK_TERUTANG, MASA, PAJAK, STATUS, KODE_REK, TGL_KETETAPAN
                , KODE_BILING,DEVICE,FILE_TRANSAKSI)
            VALUES ('$noform','$msPajak', '$thnPajak', '$nmWp', '$almtWp'
                , '$idUsaha', '$nmUsaha', '$almtUsaha'
                , '$npwpd', to_date('$tglPenerimaan','dd/mm/yyyy')
                , to_date('$berlakuMulai','dd/mm/yyyy'), '$kodeKec', '$kodeKel'
                , '$idOp', '$dsrPengenaan', '$jnsEntrian', '$dpp', '$npwpInsert'
                , '$ipInsert', SYSDATE, '$pjkTerutang', '$masa', '$pajak', '$status', '$rek'
                , to_date('$berlakuMulai','dd/mm/yyyy'),$kodeBilling,'$device','$filesPath')";
        } else if ($jnsPajak == 7) {
            $sql = "INSERT INTO SPTPD_PARKIR(NO_FORMULIR,MASA_PAJAK, TAHUN_PAJAK, NAMA_WP, ALAMAT_WP
                , ID_TEMPAT_USAHA, NAMA_USAHA, ALAMAT_USAHA, NPWPD, TANGGAL_PENERIMAAN
                , KODEKEC, KODEKEL, ID_OP, DASAR_PENGENAAN, DPP, NPWP_INSERT
                , IP_INSERT, TGL_INSERT, PAJAK_TERUTANG, STATUS, KODE_REK, TGL_KETETAPAN
                , KODE_BILING,DEVICE,FILE_TRANSAKSI)
            VALUES ('$noform','$msPajak', '$thnPajak', '$nmWp', '$almtWp'
                , '$idUsaha', '$nmUsaha', '$almtUsaha'
                , '$npwpd', to_date('$tglPenerimaan','dd/mm/yyyy')
                , '$kodeKec', '$kodeKel'
                , '$idOp', '$dsrPengenaan', '$dpp', '$npwpInsert'
                , '$ipInsert', SYSDATE, '$pjkTerutang', '$status', '$rek'
                , to_date('$berlakuMulai','dd/mm/yyyy'),$kodeBilling,'$device','$filesPath')";
        }

        $result = $this->db->query($sql);
        if($result){
            $response["success"] = 3;
            $response["message"] = "Data SPTPD untuk masa $dataBulan[$msPajak] $thnPajak berhasil ditambahkan"
            ." dengan kode Billing $kodeBilling";
            echo json_encode($response);
        }else{
            $response["success"] = 0;
            $response["message"] = "Gagal insert";
            echo json_encode($response);
        }
	}
}