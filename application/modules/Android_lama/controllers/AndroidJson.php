<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class AndroidJson extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('master/Mpokok');
    }

    public function pLogin() {
        $usrnm = $_POST['usrnm'];
        $pwd = $_POST['pwd'];
        
        $this->load->model('Plogin');
        $this->Plogin->pLogin($usrnm,$pwd);
    }

    public function gTmpUsaha() {
        $npwp = $_GET['npwp'];
        $value = $_GET['value'];
        
        $this->load->model('GtmpUsaha');
        $this->GtmpUsaha->gTmpUsaha($npwp,$value);
    }

    public function gGolongan() {
        $value = $_GET['value'];
        
        $this->load->model('Ggolongan');
        $this->Ggolongan->gGolongan($value);
    }

    public function gSptpdTahun() {
        $npwp = $_GET['npwp'];
        $golongan = $_GET['golongan'];
        
        $this->load->model('GsptpdTahun');
        $this->GsptpdTahun->gSptpdTahun($npwp,$golongan);
    }

    public function gSptpd() {
        $npwp = $_GET['npwp'];
        $golongan = $_GET['golongan'];
        $tahun = $_GET['tahun'];
        $tmpUsaha = $_GET['tmpUsaha'];
        
        $this->load->model('Gsptpd');
        $this->Gsptpd->gSptpd($npwp,$golongan,$tahun,$tmpUsaha);
    }

    public function gTagihanTahun() {
        $npwp = $_GET['npwp'];
        $idOp = $_GET['idOp'];
        
        $this->load->model('GtagihanTahun');
        $this->GtagihanTahun->gTagihanTahun($npwp,$idOp);
    }

    public function gTagihan() {
        $npwp = $_GET['npwp'];
        $idOp = $_GET['idOp'];
        $tahun = $_GET['tahun'];
        $tmpUsaha = $_GET['tmpUsaha'];
        
        $this->load->model('Gtagihan');
        $this->Gtagihan->gTagihan($npwp,$idOp,$tahun,$tmpUsaha);
    }

    public function gStatusUser() {
        $npwp = $_GET['npwp'];
        $value = $_GET['value'];
        
        $this->load->model('GstatusUser');
        $this->GstatusUser->gStatusUser($npwp);
    }

    public function gKecKel() {
        $value = $_GET['value'];
        
        $this->load->model('GkecKel');
        $this->GkecKel->gKecKel();
    }

    public function pUTmpUsaha() {
        $idInc = $_POST['idInc'];
        $nmUsaha = $_POST['nmUsaha'];
        $almtUsaha = $_POST['almtUsaha'];
        $kdKel = $_POST['kdKel'];
        $kdKec = $_POST['kdKec'];

        $this->load->model('PuTmpUsaha');
        $this->PuTmpUsaha->pUTmpUsaha($idInc,$nmUsaha,$almtUsaha,$kdKel,$kdKec);
    }

    public function pUEmail() {
        $npwp = $_GET['npwp'];
        $email = $_POST['email'];

        $this->load->model('PuEmail');
        $this->PuEmail->pUEmail($npwp,$email);
    }

    public function pUTelp() {
        $npwp = $_GET['npwp'];
        $telp = $_POST['telp'];

        $this->load->model('PuTelp');
        $this->PuTelp->pUTelp($npwp,$telp);
    }

    public function pUpdateSptpdBerkas() {
        $idSptpd = $_POST['idSptpd'];
        $jmhFile = $_POST['jmlFile'];
        $jnsPajak = $_POST['jnsPajak'];
        $brksFile = $_POST['brksFile'];

        $filePathAbsolute = "";
        $sukses = "";
        $uploadedFiles = "";
        for ($i = 0; $i < $jmhFile; $i++) {
            
            $iterasiUploadFile = $i+1;
            if (isset($_FILES["uploaded_file$iterasiUploadFile"]["name"])) {
                if ($i == 0) {
                    $uploadedFiles .= "uploaded_file$iterasiUploadFile";
                } else {
                    $uploadedFiles .= ","."uploaded_file$iterasiUploadFile";
                }
            } else { 
                $sukses .= "fail1";
                return $sukses;
			}
        }

        $valuesString = "$idSptpd|$uploadedFiles|$jnsPajak|$brksFile";

        $this->load->model('PupdateSptpdBerkas');
        $this->PupdateSptpdBerkas->pUpdateSptpdBerkas($valuesString);
    }

    public function pInsertSptpd() {
        $jnsPajak = $_POST['jnsPajak'];
        $msPajak = $_POST['msPajak'];
        $thnPajak = $_POST['thnPajak'];
        $nmWp = $_POST['nmWp'];
        $idUsaha = $_POST['idUsaha'];
        $almtWp = $_POST['almtWp'];
        $nmUsaha = $_POST['nmUsaha'];
        $almtUsaha = $_POST['almtUsaha'];
        $npwpd = $_POST['npwpd'];
        $tglPenerimaan = $_POST['tglPenerimaan'];
        $berlakuMulai = $_POST['berlakuMulai'];
        $kodeKec = $_POST['kodeKec'];
        $kodeKel = $_POST['kodeKel'];
        $idOp = $_POST['idOp'];
        $dsrPengenaan = $_POST['dsrPengenaan'];
        if (empty($dsrPengenaan)) {
            $dsrPengenaan = "-";
        }
        $jnsEntrian = $_POST['jnsEntrian'];
        $jmhFile = $_POST['jmlFile'];
        $dpp = $_POST['dpp'];
        $npwpInsert = $_POST['npwpInsert'];
        $ipInsert = $_POST['ipInsert'];
        $tglInsert = $_POST['tglInsert'];
        $pjkTerutang = $_POST['pjkTerutang'];
        $masa = $_POST['masa'];
        $pajak = $_POST['pajak'];
        $status = $_POST['status'];
        $device = $_POST['device'];
        $statusPengiriman = $_POST['statusPengiriman'];

        $filePathAbsolute = "";
        $sukses = "";
        $uploadedFiles = "";
        for ($i = 0; $i < $jmhFile; $i++) {
            
            $iterasiUploadFile = $i+1;
            if (isset($_FILES["uploaded_file$iterasiUploadFile"]["name"])) {
                if ($i == 0) {
                    $uploadedFiles .= "uploaded_file$iterasiUploadFile";
                } else {
                    $uploadedFiles .= ","."uploaded_file$iterasiUploadFile";
                }
            } else { 
                $sukses .= "fail1";
                return $sukses;
			}
        }

        // for ($i = 0; $i < $jmhFile; $i++) {
            
        //     $iterasiUploadFile = $i+1;
        //     if (isset($_FILES["uploaded_file$iterasiUploadFile"]["name"])) {
        //         $filePath = $filePathAbsolute . basename( $_FILES["uploaded_file$iterasiUploadFile"]['name']);
        //         if(move_uploaded_file($_FILES["uploaded_file$iterasiUploadFile"]['tmp_name'], $filePath)) {
        //             $sukses += "$filePath";
        //             if ($i == 0) {
        //                 $filesPath .= basename( $_FILES["uploaded_file$iterasiUploadFile"]['name']);
        //             } else {
        //                 $filesPath .= ",".basename( $_FILES["uploaded_file$iterasiUploadFile"]['name']);
        //             }
        //         } else { $sukses .= "fail";}
        //     } else { 
        //         $sukses .= "fail1";
        //         return $sukses;
		// 	}
        // }
        
        $rek=$this->db->query("select  getNoRek('$idOp')REK from dual")->row();
        $rek=$rek->REK;
        
        $kb=$this->Mpokok->getSqIdBiling();
        $bank=$this->db->query("select getnorek_bank('$idOp')BANK from dual")->row();
        $queryKdBilling=$this->db->query("SELECT ('$bank->BANK'||TO_CHAR(SYSDATE, 'yymmdd')||'$kb')ASDF FROM dual")->row();
        $queryKdBilling=$queryKdBilling->ASDF;

        $noform=$this->Mpokok->getFormulir();
        $this->load->model('PinsertSptpd');
        
        $valuesString = "$jnsPajak|$noform|$msPajak|$thnPajak|$nmWp|$almtWp
        |$idUsaha|$nmUsaha|$almtUsaha|$npwpd|$tglPenerimaan|$berlakuMulai
        |$kodeKec|$kodeKel|$idOp|$dsrPengenaan|$jnsEntrian|$dpp|$npwpInsert
        |$ipInsert|$tglInsert|$pjkTerutang|$masa|$pajak|$status|$rek|$queryKdBilling|$device|$statusPengiriman|$uploadedFiles";

        $this->PinsertSptpd->pInsertSptpd($valuesString);
    }
}
