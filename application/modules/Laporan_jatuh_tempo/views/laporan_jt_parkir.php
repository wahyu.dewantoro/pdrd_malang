   <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<div class="page-title">
 <div class="title_left">
  <h3><?php echo $title;?></h3>
</div>
</div><?php $session_value=$this->session->userdata('MS_ROLE_ID');?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <?php echo $this->session->flashdata('notif')?>
        <form class="form-inline" method="get" action="<?php echo base_url().'Laporan_piutang/laporan_piutang_parkir'?>">
                <input type='hidden' value='1' name='cek'>
                <div class="form-group">
                  <select  name="tahun" required="required" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('h_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select  name="bulan"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih Semua Masa</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php if ($this->session->userdata('h_bulan')==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select onchange="getkel(this);" name="kecamatan"  class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua Kecamatan</option>
                            <?php foreach($kec as $row){ ?>
                            <option  value="<?php echo $row->KODEKEC?>"
                              <?php if ($this->session->userdata('h_kecamatan')==$row->KODEKEC) {echo "selected";} ?>><?php echo $row->NAMAKEC ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                      <select  name="kelurahan"  id="KELURAHAN"  class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Semua Kelurahan</option>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($this->session->userdata('h_kelurahan')==$kel->KODEKELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php } ?>                          
                        </select>
                </div>
                <div class="form-group">
                    <input type="text" name="npwpd"  class="form-control col-md-6 col-xs-12" value="<?php echo $this->session->userdata('h_npwpd'); ?>" placeholder="NPWPD">
                </div>
                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
                <?php if ($cek <> '')  { ?>
                                    <a href="<?php echo site_url('Laporan_piutang/laporan_piutang_parkir'); ?>" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</a>
                                    <!-- <a href="<?php echo site_url('Excel/Excel/Excel_laporan_piutang_parkir'); ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a> -->
                              <?php }   ?>  
                              <a href="<?php echo $cetak; ?>" class="btn btn-success"><i class="fa fa-print"></i> Excel</a> 
        </form>
        <table id="example2" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">Kode Biling</th>
              <th class="text-center">NPWPD</th>
              <th class="text-center">Nama WP</th>
              <th class="text-center">Nama Usaha</th>
              <th class="text-center">Alamat Usaha</th>
              <th class="text-center">SSPD</th>
              <th class="text-center">BON</th>
              <th class="text-center">LAINYA</th>
              <th class="text-center">DPP</th>
              <th class="text-center">Ketetapan</th>
              <th class="text-center">Denda</th>
              <th class="text-center">Potongan</th>
              <th class="text-center">Total Pajak Terutang</th>
              <th class="text-center">Tgl Penetapan</th>
              <th class="text-center">Detail</th>
            </tr>
          </thead>
            <tbody>
              <?php if($total_rows>0){  foreach ($parkir as $rk)  { ?>
              <tr>
                <td  align="center"><?php echo ++$start ?></td>
                <td><?= $rk->KODE_BILING?></td>
                <td><?= $rk->NPWPD?></td>
                <td><?= $rk->NAMA_WP?></td>
                <td><?= $rk->OBJEK_PAJAK?></td>
                <td><?= $rk->ALAMAT_OP?></td>
                <td align="right"><?=  number_format($rk->SSPD,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->REKAP_BON,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->LAINYA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DPP,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->TAGIHAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->DENDA,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->POTONGAN,'0','','.')?></td>
                <td align="right"><?=  number_format($rk->JUMLAH_KETETAPAN,'0','','.')?></td>

                <td align="center"><?= $rk->TGL_PENETAPAN?></td>
                <td align="center"><a href="#"  data-toggle="modal" data-target="#myModal<?= $rk->KODE_BILING?>">Detail</a></td>
              </tr>
              <?php  }  }else{ ?>
              <tr>
                <th colspan="9"> Tidak ada data.</th>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <button  class="btn  btn-space btn-info" disabled>Total Record : <?php echo $total_rows ?></button>
          <button  class="btn  btn-space btn-success" disabled>Total Tagihan : <?php echo number_format($total_tagihan->JUMLAH,'0','','.')?></button>
          <div class="float-right">
            <?php echo $pagination ?>
          </div>
      </div>
    </div>
  </div>
</div>
<?php foreach ($parkir as $rk)  { ?>
  <div class="modal fade" id="myModal<?= $rk->KODE_BILING?>" role="dialog">
    <div class="modal-dialog"> 
      <?php $ID=$rk->ID_PARKIR;?>   
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">DETAIL DATA PARKIR</h4>
        </div>
        <div class="modal-body">
          <table id="example2" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th class="text-center" width="3%">No</th>
              <th class="text-center">KODE KARCIS</th>
              <th class="text-center">Jumlah Terjual</th>            
              <th class="text-center">Harga</th>
               <th class="text-center">jumlah Penerimaan</th>
            </tr>
          </thead>
            <tbody>
              <?php $no=1;$tot=0;$det_rek=$this->db->query("SELECT * FROM SPTPD_PARKIR_KARCIS WHERE SPTPD_PARKIR_ID='$ID'")->result();if(count($det_rek)>0){  foreach ($det_rek as $rk_d)  { ?>
              <tr>
                <td><?= $no?></td>
                <td><?= $rk_d->JENIS_PARKIR?></td>
                <td><?= $rk_d->JUMLAH_LEMBAR?></td>
                <td align="right"><?=  number_format($rk_d->NOMINAL,'0','','.')?></td>
                <td align="right"><?=  number_format($rk_d->PENERIMAAN,'0','','.')?></td>
              </tr>
              <?php  $no++;$tot+=$rk_d->PENERIMAAN;}  }else{ ?>
              <tr>
                <th colspan="5"> Tidak ada data.</th>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="4"><b>TOTAL DPP</b></td>
                <td align="right"><?=  number_format($tot,'0','','.')?></td>                                
              </tr>
              <tr>
                <td colspan="4"><b>PAJAK TERUTANG</b></td>
                <td align="right"><?=  number_format(0,'0','','.')?></td>                                
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <?php }?>
  <style type="text/css">
  .modal-dialog {
    width: 877px;
    margin: 30px auto;
    z-index: 9999999;
  }
      table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }
  </style>
    <script type="text/javascript">
function getkel(sel)
    {
      var KODEKEC = sel.value;
      //alert(KODEKEC);
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Laporan_sptpd/Laporan_sptpd/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
  </script>



