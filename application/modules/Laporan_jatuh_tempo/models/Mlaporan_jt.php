<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mlaporan_piutang extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->nip  =$this->session->userdata('NIP');
        // $this->role =$this->session->userdata('MS_ROLE_ID');
    }     
    function total_rows_rekap_hotel($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_HOTEL');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_hotel($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
    					JUMLAH_KAMAR,TARIF_RATA_RATA,KAMAR_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_HOTEL')->result();
    }
        function get_all_rekap_hotel($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                        JUMLAH_KAMAR,TARIF_RATA_RATA,KAMAR_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_HOTEL')->result();
    }
    function sum_tagihan_hotel($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_HOTEL')->row();
    }
    function total_rows_rekap_restoran($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_RESTORAN');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_restoran($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                        JUMLAH_MEJA,TARIF_RATA_RATA,MEJA_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN, (TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_RESTO");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_RESTORAN')->result();
    }
    function get_all_rekap_restoran($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                        JUMLAH_MEJA,TARIF_RATA_RATA,MEJA_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN, (TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_RESTO");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_RESTORAN')->result();
    }    
    function sum_tagihan_restoran($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_RESTORAN')->row();
    }

    function total_rows_rekap_hiburan($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_HIBURAN');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_hiburan($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN, (TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,DASAR_PENGENAAN,HARGA_TANDA_MASUK,JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_HIBURAN')->result();
    }
    function get_all_rekap_hiburan($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN, (TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,DASAR_PENGENAAN,HARGA_TANDA_MASUK,JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_HIBURAN')->result();
    }    
    function sum_tagihan_hiburan($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_HIBURAN')->row();
    }
    function total_rows_rekap_reklame($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_REKLAME');
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_reklame($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,TOTAL");
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_REKLAME')->result();
    }
    function get_all_rekap_reklame($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,TOTAL");
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_REKLAME')->result();
    }    
    function sum_tagihan_reklame($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_REKLAME')->row();
    }
    /*
    function total_rows_rekap_reklame($tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
    $this->db->from('UPT_REKAP_REKLAME');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS ='0'");
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_reklame($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
    				  TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA) AS JUMLAH_KETETAPAN,TOTAL");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS ='0'");
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_REKLAME')->result();
    }*/
    function total_rows_rekap_ppj($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_PPJ');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_ppj($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,KEPERLUAN,TARIF,JUMLAH_PEMAKAIAN,DAYA_TERPASANG,PLN");
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_PPJ')->result();
    }
    function get_all_rekap_ppj($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,KEPERLUAN,TARIF,JUMLAH_PEMAKAIAN,DAYA_TERPASANG,PLN");
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_PPJ')->result();
    }    
    function sum_tagihan_ppj($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_PPJ')->row();
    }

    function total_rows_rekap_galian($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_GALIAN');
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_galian($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,ID_LOGAM");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_GALIAN')->result();
    }
        function get_all_rekap_galian( $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,ID_LOGAM");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_GALIAN')->result();
    }
    function sum_tagihan_galian($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    $this->db->where('MASA_PAJAK', $bulan);
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_GALIAN')->row();
    }
    
    function total_rows_rekap_parkir($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_PARKIR');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_parkir($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,SSPD,REKAP_BON,REKAP_BIL,LAINYA,ID_PARKIR");
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_PARKIR')->result();
    }
        function get_all_rekap_parkir($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,SSPD,REKAP_BON,REKAP_BIL,LAINYA,ID_PARKIR");
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_PARKIR')->result();
    }
    function sum_tagihan_parkir($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_PARKIR')->row();
    }
    function total_rows_rekap_at($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_AT');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_at($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,CARA_PENGAMBILAN,PERUNTUKAN,
    PENGGUNAAN_HARI_NON_METER,PENGGUNAAN_BULAN_NON_METER,PENGGUNAAN_HARI_INI_METER,PENGGUNAAN_BULAN_LALU_METER,VOLUME_AIR_METER,JENIS,DEBIT_NON_METER");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_AT')->result();
    }
    function get_all_rekap_at( $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,CARA_PENGAMBILAN,PERUNTUKAN,
    PENGGUNAAN_HARI_NON_METER,PENGGUNAAN_BULAN_NON_METER,PENGGUNAAN_HARI_INI_METER,PENGGUNAAN_BULAN_LALU_METER,VOLUME_AIR_METER,JENIS,DEBIT_NON_METER");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_AT')->result();
    }    
    function sum_tagihan_at($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_AT')->row();
    }

    function total_rows_rekap_sb($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->from('UPT_REKAP_SB');
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_sb($limit, $start = 0, $tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_PENGELOLAAN,JENIS_BURUNG,KEPEMILIKAN_IJIN,LUAS_AREA,JUMLAH_GALUR,DIPANEN_SETIAP,HASIL_PANEN,HASIL_SETIAP_PANEN,HARGA_RATA_RATA");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_SB')->result();
    }
        function get_all_rekap_sb($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,POTONGAN,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_PENGELOLAAN,JENIS_BURUNG,KEPEMILIKAN_IJIN,LUAS_AREA,JUMLAH_GALUR,DIPANEN_SETIAP,HASIL_PANEN,HASIL_SETIAP_PANEN,HARGA_RATA_RATA");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_SB')->result();
    }
    function sum_tagihan_sb($tahun = NULL,$bulan = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL) {
    $this->db->select("SUM(TAGIHAN+DENDA-POTONGAN)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    if ($bulan!=NULL) {$this->db->where("MASA_PAJAK",$bulan);}
    $this->db->where("STATUS = '0'");
    $this->db->where("JATUH_TEMPO <", "SYSDATE",false);
    if ($kecamatan!=NULL) {$this->db->where("KODEKEC",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KODEKEL",$kelurahan);}
    if ($npwpd!=NULL) {$this->db->where("NPWPD",$npwpd);}
    return $this->db->get('UPT_REKAP_SB')->row();
    }

    public function get_kec(){
        $upt_id=$this->session->userdata('UNIT_UPT_ID');
        if ($upt_id=='2') {
            $wh="";
        } else {
            $wh="where kode_upt='$upt_id'";
        }
          return $this->db->query("select * from KECAMATAN $wh")->result();
     }

    function total_rows_rekap_reklame_detail($tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$habis_masa= NULL,$jns=NULL) {
            $date=date('d-m-Y');
            $this->db->from('V_DET_JT_REKLAME_DETAIL');
            $this->db->where('TAHUN_PAJAK', $tahun);
            
            if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
            if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
            if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
            
            if ($npwpd!=NULL) {
                 $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
            }
            if ($habis_masa!=NULL) {$this->db->where(" AKHIR_MASA < TO_DATE(SYSDATE, 'dd-mm-yyyy')");}
            return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_reklame_detail($limit, $start = 0, $tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$habis_masa= NULL,$jns=NULL) {
    $date=date('d-m-Y');
    $this->db->select("A.*,CASE WHEN JENIS_WAKTU = 1 THEN MASA||' th' WHEN JENIS_WAKTU = 2 THEN MASA||' bln' WHEN JENIS_WAKTU = 3 THEN MASA||' mgu' ELSE MASA||' hr' END AS WAKTU");
    $this->db->where('TAHUN_PAJAK', $tahun);
    
    if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
    if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
    
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
    }
    if ($habis_masa!=NULL) {$this->db->where(" AKHIR_MASA < TO_DATE(SYSDATE, 'dd-mm-yyyy')");}
    $this->db->order_by('ID_INC', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('V_DET_JT_REKLAME_DETAIL A')->result();
    }
    function get_all_data_rekap_reklame_detail($tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$habis_masa= NULL,$jns=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("*");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    
    if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
    if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
    
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
    }
    if ($habis_masa!=NULL) {$this->db->where(" AKHIR_MASA < TO_DATE(SYSDATE, 'dd-mm-yyyy')");}
    return $this->db->get('V_DET_JT_REKLAME_DETAIL')->result();
    }
    function sum_tagihan_reklame_detail($tahun = NULL,$kecamatan=NULL,$kelurahan=NULL,$npwpd=NULL,$habis_masa= NULL,$jns=NULL) {
    $date=date('m-d-Y');
    $this->db->select("SUM(PAJAK_TERUTANG)JUMLAH");    
    $this->db->where('TAHUN_PAJAK', $tahun);
    
    if ($jns!=NULL){$this->db->where("JENIS_REKLAME",$jns);}
    if ($kecamatan!=NULL){$this->db->where("KECAMATAN",$kecamatan);}
    if ($kelurahan!=NULL) {$this->db->where("KELURAHAN",$kelurahan);}
   
    if ($npwpd!=NULL) {
         $this->db->where("(KODE_BILING LIKE '%$npwpd%' OR LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%' OR LOWER(TEKS) LIKE '%$npwpd%')",null,false);
    }
    if ($habis_masa!=NULL) {$this->db->where(" AKHIR_MASA < TO_DATE(SYSDATE, 'dd-mm-yyyy')");}
    return $this->db->get('V_DET_JT_REKLAME_DETAIL')->row();
    }
  

}