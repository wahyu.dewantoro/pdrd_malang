
      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
  </div>  
  <div class="clearfix"></div>   
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_content" >
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">  
                    </div> 
                  </div>
                </div>
              </div> 
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Karcis Non Perforasi</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >         
                <div class="col-md-12">
                  <table class="table table-striped table-bordered table-hover" border="1" with='100%'>
                    <thead>
                      <tr>    
                        <th width="50">KODE</th>
                        <th width="70">Jml Terjual</th>
                        <th width="100">Harga karcis</th>
                        <th width="50">Pajak%</th>
                        <th width="100">Total</th>
                        <th width="100">Nilai Pajak</th>
                        <th width="130">Catatan</th>
                        <th width="10">Aksi</th>
                      </tr>
                    </thead>
                    <tbody id="kontentdiag">
                    <tr>
                        <td><input type="text" id="No_SERI" name="No_SERI[]" required placeholder="Kode" class="form-control col-md-7"></td>
                        <td><input onchange="get(this.value)" type="text" id="JUMLAH_BLOK" name="qtt_terjual[]" required placeholder="Jml Terjual" class="form-control col-md-7" ></td>
                        <td><input type="text" onchange="get(this.value)" id="NILAI" name="NILAI[]" required class="form-control col-md-7"></td>
                        <td><input type="text" onchange="get(this.value)" id="PERSEN_PAJAK" name="PERSEN_PAJAK[]" required class="form-control col-md-7"></td>
                        <td><input type="text" id="TOTAL_BEFORE_TAX" readonly name="dpp_k[]" required placeholder="TOTAL" class="form-control col-md-7"></td>
                        <td><input type="text" id="JML" readonly name="pajak_terutang[]" required placeholder="pajak terutang" class="form-control col-md-7"></td>
                       <td><input type="text" id="CTT" name="catatan[]" placeholder="" class="form-control col-md-7"></td>
                        
                        <td><span class="fa fa-trash"></span></td>
                      </tr>
                    </tbody>
                </table>
                <a class="btn btn-xs btn-success" id="tambahdiag"><i class="fa fa-plus"></i> Tambah</a>                    
                    <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Submit</button>
                      <a href="<?php echo site_url('Sptpdhiburan/hiburan') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                
                </div>
              </div>
            </div>     
          </div>
          </div>
        </div>
      </div>
    </div>
    </div></div></div>
    <script>
    
      function get(sel) {        
        var jml_blok= parseInt(document.getElementById("JUMLAH_BLOK").value.replace(/\./g,''))|| 0;
        var nilai= parseInt(document.getElementById("NILAI").value.replace(/\./g,''))|| 0;
        var pajak= parseInt(document.getElementById("PERSEN_PAJAK").value.replace(/\./g,''))|| 0;
        var total_before_tax= parseInt(document.getElementById("TOTAL_BEFORE_TAX").value.replace(/\./g,''))|| 0;
        var qtt=jml_blok;
        var sum=jml_blok*nilai;
        var pjk=(sum*pajak/100);
        JML.value= getNominal(pjk);
        TOTAL_BEFORE_TAX.value= getNominal(sum);
       
      }
//JUMLAH_BLOK 
var JUMLAH_BLOK = document.getElementById('JUMLAH_BLOK'),
numberPattern = /^[0-9]+$/;

JUMLAH_BLOK.addEventListener('keyup', function(e) {
  JUMLAH_BLOK.value = formatRupiah(JUMLAH_BLOK.value);
});
//LBR_BLOK 

//NILAI 
var NILAI = document.getElementById('NILAI'),
numberPattern = /^[0-9]+$/;
var PERSEN_PAJAK = document.getElementById('PERSEN_PAJAK'),
numberPattern = /^[0-9]+$/;
PERSEN_PAJAK.addEventListener('keyup', function(e) {
  PERSEN_PAJAK.value = formatRupiah(PERSEN_PAJAK.value);
});
NILAI.addEventListener('keyup', function(e) {
  NILAI.value = formatRupiah(NILAI.value);
});
 /* DPP */

    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
  var x=1;
  $("#tambahdiag").click(function(){
    x++;
                $.get("karcis/"+x, function (data) {
                    $("#kontentdiag").append(data);
                });
 });

    $('#kontentdiag').on('click', '.remove_project_file', function(e) {
      x--;
    e.preventDefault();
    $(this).parents(".barisdiag").remove();
  });

   
</script>


