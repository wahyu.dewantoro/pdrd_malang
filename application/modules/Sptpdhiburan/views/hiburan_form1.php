      <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
    <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
<a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="col-md-6">
                      <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option <?php if(!empty($MASA_PAJAK)){ if($mp==$MASA_PAJAK){echo "selected";}} else {if($mp==date("m")){echo "selected";}}?> value="<?php echo $mp?>"><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-7 col-xs-12">
                          <input style="font-size: 11px" type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                
                        </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >No Perforasi<sup>*</sup> 
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input type="text" id="NO_PERFORASI" name="NO_PERFORASI" required="required" placeholder="NO_PERFORASI" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> onchange="no_perforasi(this.value);" >                  
                      </div>
                    </div>                              
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input type="text" id="NPWPD" name="NPWPD" required="required" readonly placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" <?php echo $disable ?> onchange="NamaUsaha(this.value);" >                  
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" readonly class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                        <!-- <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>" <?php echo $disable ?>> -->               
                      </div>
                    </div>  
<?php if ($disable=='disabled') { ?>
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <option  value="<?php echo $NAMA_USAHA?>"><?php echo $NAMA_USAHA ?></option>
                        </select>
                      </div>
                    </div>   -->
<?php } else { ?>                    
                   <!--  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px"  name="NAMA_USAHA"  class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <?php foreach($jenis as $jns){ ?>
                          <option <?php if($jns->NAMA_USAHA==$NAMA_USAHA){echo "selected";}?> value="<?php echo $jns->NAMA_USAHA?>"><?php echo $jns->NAMA_USAHA ?></option>
                          <?php } ?> 
                        </select>
                      </div>
                    </div> -->
<?php } ?>                    
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Golongan</b></td>
                          <td><span id="DESKRIPSI"><?php if ($disable=='disabled') {echo "$NAMA_GOLONGAN";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kecamatan </b></td>
                          <td><span id="NAMAKEC"><?php if ($disable=='disabled') {echo "$NAMA_KECAMATAN";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kelurahan</b></td>
                          <td><span id="NAMAKELURAHAN"><?php if ($disable=='disabled') {echo "$NAMA_KELURAHAN";} ?></span></td>
                        </tr>                       
                      </tbody>
                    </table>
                    <input type="hidden" id="V_KODEKEC" name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="V_KODEKEL" name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >                         
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP<sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <input type="text" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>" <?php echo $disable ?>>                
                        </div>
                      </div> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  WP<sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <textarea style="font-size: 11px" <?php echo $disable ?> id="ALAMAT_WP" rows="3" class="resizable_textarea form-control" placeholder="Alamat Wajib Pajak" name="ALAMAT_WP"><?php echo $ALAMAT_WP; ?></textarea>                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-6 col-xs-12">
                          <textarea style="font-size: 11px" <?php echo $disable ?> id="ALAMAT_USAHA" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                        </div>
                      </div>
                      <table class="table table-bordered">
                        <tr>
                          <th class="text-center">No Seri</th>
                          <th class="text-center">No Urut</th>
                          <th class="text-center">Jml Blok</th>
                          <th class="text-center">Isi /Blok</th>
                          <th class="text-center">Total</th>
                          <th class="text-center">Tgl</th>
                        </tr>
                      <tbody>
                        <tr>
                          <td><span id="V_NO_SERI"></span></td>
                          <td><span id="V_NO_URUT"></span></td>
                          <td align="right"><span id="V_JML_BLOK"></span></td>
                          <td align="right"><span id="V_ISI_PER_BLOK"></span></td>
                          <td align="right"><span id="V_TOTAL"></span></td>
                          <td align="center"><span id="V_TGL_PERFORASI"></span></td>
                          <input id="TOTAL" type="hidden">
                        </tr>            
                      </tbody>
                    </table>
                    </div> 
                  </div>
                </div>
              </div> 

            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">              
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"  <?php echo $disable?> placeholder="No Formulir">
                      </div>
                    </div> -->
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                        </div>
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Berlaku Mulai  <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required <?php echo $disable?> >
                        </div>
                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select  style="font-size:11px" id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                  <?php if($button==('Update SPTPD Hotel'||'Form SPTPD Hiburan')){?>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php }} ?>                          
                        </select>
                      </div>
                    </div>  -->
                    <?php
                    $persen = "var persen = new Array();\n";
                    $masa = "var masa = new Array();\n";
                    ?>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN" <?php echo $disable?> name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                          <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                          <?php 
                          $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                          $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <script type="text/javascript">  
                    <?php echo $persen;echo $masa;?>             
                    function changeValue(id){
                      document.getElementById('PAJAK').value = persen[id].persen;
                      document.getElementById('MASA').value = masa[id].masa;
                    }
                  </script>    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="DASAR_PENGENAAN" <?php echo $disable?> name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <!-- <option value="">Pilih</option> -->
                        <option  <?php if($DASAR_PENGENAAN=='Omzet'){echo "selected";}?> value="Omzet">Omzet</option>
                        <option  <?php if($DASAR_PENGENAAN=='Ketetapan'){echo "selected";}?> value="Ketetapan">Ketetapan</option>
                        <option  <?php if($DASAR_PENGENAAN=='Karcis'){echo "selected";}?> value="Karcis">Karcis</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                        </label>
                      </div>
                    </div>
                  </div>   
                </div>                  
                <div class="col-md-6">        
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Harga Tanda masuk <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                      <input onchange='get(this);' <?php echo $disable ?> type="text" id="HARGA_TANDA_MASUK" name="HARGA_TANDA_MASUK" required="required" placeholder="Harga Tanda masuk" class="form-control col-md-7 col-xs-12" value="<?php echo $HARGA_TANDA_MASUK; ?>">       </div>
                    </div>
                  </div>   
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Terjual<sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                        <input onchange='get(this);'  <?php echo $disable ?> type="text" id="JUMLAH" name="JUMLAH" required="required" placeholder="Jumlah" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH; ?>" >                
                      </div>
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Total <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input readonly="" <?php echo $disable ?> type="text" id="JUMLAH_TOTAL" name="JUMLAH_TOTAL" required="required" placeholder="Jumlah Total" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_TOTAL; ?>" >                
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"  <?php echo $disable?> placeholder="Masa Pajak" >
                    </div>
                    <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"  <?php echo $disable?> placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                      </div>
                    </div> 
                  </div> 
                  <?php if($disable==''){?>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Submit</button>
                      <a href="<?php echo site_url('Sptpdhiburan/hiburan') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  <?php } ?> 
                </div>
              </div>
            </div>     
          </div>              
            </div>


          </div>
        </div>
      </div>
    </div>
  </form>
</div>



</div>

<script>

  function get() {
    var xy = document.getElementById("TOTAL").value;
    var JUMLAH1= parseFloat(nominalFormat(document.getElementById("JUMLAH").value))||0;
    if (JUMLAH1 > xy) {alert("Jumlah Terjual lebih dari yang diperforasi");var JUMLAH= parseFloat(nominalFormat(0))||0;
    } else{
      var JUMLAH= parseFloat(nominalFormat(document.getElementById("JUMLAH").value))||0;
    };
    var HARGA_TANDA_MASUK= parseFloat(nominalFormat(document.getElementById("HARGA_TANDA_MASUK").value))||0;
    var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
    var dpp=HARGA_TANDA_MASUK*JUMLAH;
    JUMLAH_TOTAL.value=getNominal(HARGA_TANDA_MASUK*JUMLAH)  ;
    DPP.value=getNominal(dpp)  ;
    PAJAK_TERUTANG.value=getNominal(dpp*PAJAK)  ;
    //alert(JUMLAH1);
    }
  /* HARGA_TANDA_MASUK */
  var HARGA_TANDA_MASUK = document.getElementById('HARGA_TANDA_MASUK');
  HARGA_TANDA_MASUK.addEventListener('keyup', function(e)
  {
    HARGA_TANDA_MASUK.value = formatRupiah(this.value);
  });
  /* JUMLAH */
  var JUMLAH = document.getElementById('JUMLAH');
  JUMLAH.addEventListener('keyup', function(e)
  {
    JUMLAH.value = formatRupiah(this.value);
  });
    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
  }




    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
    function alamat(sel)
    {
      if (sel=="") {
        $("#ALAMAT_USAHA").val(""); 
        $("#GOLONGAN").val("");
        $("#MASA").val("");
        $("#PAJAK").val("");
        $("#DESKRIPSI").html("");  
        $("#NAMAKELURAHAN").html("");
        $("#NAMAKEC").html("");    

      } else {
        var nama = sel;
        //alert(nama);
        $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_alamat_usaha'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              //alert(msga);
              $("#ALAMAT_USAHA").val(msga); 
            }
          });
         $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_detail_usaha'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              if(msga!=0){
                  var exp = msga.split("|");
                  $("#DESKRIPSI").html(exp[1]);  
                  $("#NAMAKELURAHAN").html(exp[3]);
                  $("#NAMAKEC").html(exp[5]);
                  $("#GOLONGAN").val(exp[0]);
                  changeValue($( "#GOLONGAN option:selected" ).val());
                  $("#V_ID_OP").val(exp[0]);
                  $("#V_KODEKEL").val(exp[2]);
                  $("#V_KODEKEC").val(exp[4]);                  
                  
                 // alert(msga);
                }else{
                  $("#NAMA_WP").val(null);  
                  $("#ALAMAT_WP").val(null); 
                  $("#VNAMA_WP").html("BELUM TERDAFTAR");
                  $("#VALAMAT_WP").html("BELUM TERDAFTAR");
                  
                }
            }
          });   
        } 
    }
    function no_perforasi(id){
          //alert(id);
          var noper=id;
                $.ajax({
                url: "<?php echo base_url().'Master/pokok/ceknoper_hiburan';?>",
                type: 'POST',
                data: "noper="+noper,
                cache: false,
                success: function(msg){
                       // alert(msg);
                       //$("#NPWPD").val(msg);
                       var obj = $.parseJSON(JSON.stringify(msg));
                          $("#NPWPD" ).val(obj['NPWPD']);
                          $("#NAMA_USAHA" ).val(obj['NAMA_USAHA']);
                          $("#NAMA_WP").val(obj['NAMA_WP']);
                          $("#ALAMAT_WP").val(obj['ALAMAT_WP']);
                          $("#ALAMAT_USAHA").val(obj['ALAMAT_USAHA']);

                          $("#GOLONGAN").val(obj['ID_OP']);
                          changeValue($( "#GOLONGAN option:selected" ).val());

                          $("#DESKRIPSI").html(obj['DESKRIPSI']);  
                          $("#NAMAKELURAHAN").html(obj['NAMAKELURAHAN']);  
                          $("#NAMAKEC").html(obj['NAMAKEC']);  

                         /* $("#GOLONGAN").val(exp[0]);
                          $("#V_ID_OP").val(exp[0]);*/
                          $("#V_KODEKEL").val(obj['KODEKEL']);
                          $("#V_KODEKEC").val(obj['KODEKEC']);
                          $("#HARGA_TANDA_MASUK").val(String(obj['NILAI_LEMBAR']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          //$("#JUMLAH_BAYAR" ).val(String(obj['TAGIHAN']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          //data perforasi
                          $("#V_NO_SERI").html(obj['NO_SERI']);
                          $("#V_NO_URUT").html(obj['NO_URUT_KARCIS']);
                          $("#V_JML_BLOK").html(String(obj['JML_BLOK']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#V_ISI_PER_BLOK").html(String(obj['ISI_LEMBAR']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#V_TOTAL").html(String(obj['ISI_LEMBAR']*obj['JML_BLOK']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#TOTAL").val(obj['ISI_LEMBAR']*obj['JML_BLOK']);
                          $("#V_TGL_PERFORASI").html(obj['TGL_PERFORASI']);
                      
                    }
                  });
    }    
    /*function NamaUsaha(id){
                //alert(id);
                var npwpd = document.getElementById("NPWPD").value;
                //var npwpd=id;
                var jp='03';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
        
        $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_nama_usaha'?>",
           data: { npwp: npwpd,
                   jp: jp   },
           cache: false,
           success: function(msga){
                  //alert(jp);
                  $("#NAMA_USAHA").html(msga);
                }
              }); 
                
    } */    
</script>
