      <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
    <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
<a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="" method="post" enctype="multipart/form-data" >
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="col-md-6">
                      <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp){ ?>
                            <option <?php if(!empty($MASA_PAJAK)){ if($mp==$MASA_PAJAK){echo "selected";}} else {if($mp==date("m")){echo "selected";}}?> value="<?php echo $mp?>"><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                        </label>
                        <div class="col-md-9 col-sm-7 col-xs-12">
                          <input style="font-size: 11px" type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                
                        </div>
                      </div>                            
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input type="text" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" <?php echo $disable ?> onchange="NamaUsaha(this.value);" >                  
                      </div>
                    </div> 
                    <?php if ($disable=='disabled') { ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px" required name="NAMA_USAHA" id="NAMA_USAHA" class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <option  value="<?php echo $NAMA_USAHA?>"><?php echo $NAMA_USAHA ?></option>
                        </select>
                      </div>
                    </div>  
<?php } else { ?>                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <select  style="font-size:12px"  required name="NAMA_USAHA" id="NAMA_USAHA" class="form-control select2 col-md-7 col-xs-12" onchange="alamat(this.value);" <?php echo $disable ?>>
                          <?php foreach($jenis as $jns){ ?>
                          <option <?php if($jns->NAMA_USAHA==$NAMA_USAHA){echo "selected";}?> value="<?php echo $jns->NAMA_USAHA?>"><?php echo $jns->NAMA_USAHA ?></option>
                          <?php } ?> 
                        </select>
                      </div>
                    </div>
<?php } ?>    
                    <input type="hidden" id="split_tempat_usaha">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="pengenaan(this.value)" style="font-size:11px" id="DASAR_PENGENAAN" <?php echo $disable?> name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <option  <?php if($DASAR_PENGENAAN=='Omzet'){echo "selected";}?> value="Omzet">Omzet</option>
                          <!-- <option  <?php if($DASAR_PENGENAAN=='Ketetapan'){echo "selected";}?> value="Ketetapan">Ketetapan</option> -->
                          <option  <?php if($DASAR_PENGENAAN=='Karcis Non'){echo "selected";}?> value="Karcis Non">Karcis Non Perforasi</option>
                          <option  <?php if($DASAR_PENGENAAN=='Karcis'){echo "selected";}?> value="Karcis">Karcis</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                          <div class="col-md-8">
                          <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                          </div>
                    </div>
                    <input type="hidden" id="V_KODEKEC" name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="V_KODEKEL" name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="NAMA_WP" name="NAMA_WP" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >                         
                    <input type="hidden" id="ALAMAT_WP" name="ALAMAT_WP" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >                         
                    <input type="hidden" id="ALAMAT_USAHA" name="ALAMAT_USAHA" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >                         
                    <input type="hidden"   id="ID_GOLONGAN" name="GOLONGAN_ID" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    </div>
                    <div class="col-md-6">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="19%"><b>Nama WP</b></td>
                            <td><span id="NAMA_WP1"><?php if ($disable=='disabled') {echo "$NAMA_WP";} ?></span></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Alamat WP</b></td>
                            <td><span id="ALAMAT_WP1"><?php if ($disable=='disabled') {echo "$ALAMAT_WP";} ?></span></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Alamat Usaha</b></td>
                            <td><span id="ALAMAT_USAHA1"><?php if ($disable=='disabled') {echo "$ALAMAT_USAHA";} ?></span></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Golongan</b></td>
                            <td><span id="DESKRIPSI"><?php if ($disable=='disabled') {echo "$NAMA_GOLONGAN";} ?></span></td>
                          </tr>
                          <tr>
                            <td><b>Kecamatan </b></td>
                            <td><span id="NAMAKEC"><?php if ($disable=='disabled') {echo "$NAMA_KECAMATAN";} ?></span></td>
                          </tr>
                          <tr>
                            <td><b>Kelurahan</b></td>
                            <td><span id="NAMAKELURAHAN"><?php if ($disable=='disabled') {echo "$NAMA_KELURAHAN";} ?></span></td>
                          </tr>                       
                        </tbody>
                      </table>                      
                    </div> 
                  </div>
                </div>
              </div>
            <div class="col-md-12 col-sm-12" id="tbl">
              
            </div>
            <div class="col-md-12 col-sm-12" id="non_karcis">
              
            </div>
            <div class="col-md-12 col-sm-12" id="non_perforasi">
              
            </div>
                  
          </div>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>



</div>

<script>    
   function aa(sel){
      var ext = $('#my_file_field').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
          alert('Format File Tidak Didukung!');
          $('#my_file_field').val('');
      }
    }
    var uploadField = document.getElementById("my_file_field");

uploadField.onchange = function() {
    if(this.files[0].size > 2500000){
       alert("File Terlalu Besar!");
       this.value = "";
    };
};
  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
  }
    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
    function alamat(sel)
    {
      if (sel=="") {
        $("#ALAMAT_USAHA").val("");
        $("#ALAMAT_USAHA1").html(""); 

        $("#GOLONGAN").val("");
        $("#MASA").val("");
        $("#PAJAK").val("");
        $("#DESKRIPSI").html("");  
        $("#NAMAKELURAHAN").html("");
        $("#NAMAKEC").html("");
        

      } else {
        var nama = sel;
        //alert(nama);
        $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_alamat_usaha1'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              //alert(msga);
              $("#split_tempat_usaha").val(nama);
              var exp = msga.split("|");
              if (exp[1]=='') {
                alert('Golongan Usaha Tidak Boleh Kosong, Silahkan Edit golongan di menu tempat Usaha');
                $("#NPWPD").val('');
              } else {
                $("#ALAMAT_USAHA").val(exp[0]); 
                $("#ALAMAT_USAHA1").html(exp[0]);
              }
            }
          });
             
          //alert(nama);
           
        }

    }
    function pengenaan(aa){
      var pengenaan = aa;
      var nama = document.getElementById("split_tempat_usaha").value;
      var gol =nama.split("|");
      
          if (pengenaan=='Karcis') {
             $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_data_perforasi'?>",
                     data: { nama: nama},
                     cache: false,
                     success: function(msga){
                            //alert(msga);
                             $("#non_perforasi").html(''); 
                             $("#non_karcis").html('');
                             $('#demo-form2').attr('action', '<?php echo base_url().'Sptpdhiburan/hiburan/proses_karcis';?>');
                             $("#tbl").html(msga);
                          }
                        });
                $.ajax({
                   type: "POST",
                   url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_detail_usaha'?>",
                   data: { nama: nama},
                   cache: false,
                   success: function(msga){
                        if(msga!=0){
                            var exp = msga.split("|");
                            $("#DESKRIPSI").html(exp[1]);  
                            $("#NAMAKELURAHAN").html(exp[3]);
                            $("#NAMAKEC").html(exp[5]);
                            $("#GOLONGAN").val(exp[0]);
                            
                            $("#ID_GOLONGAN").val(exp[0]);
                            $("#V_KODEKEL").val(exp[2]);
                            $("#V_KODEKEC").val(exp[4]);                  
                            changeValue($( "#GOLONGAN option:selected" ).val());
                           // alert(msga);
                          }else{
                            $("#NAMA_WP").val(null);  
                            $("#ALAMAT_WP").val(null); 
                            $("#VNAMA_WP").html("BELUM TERDAFTAR");
                            $("#VALAMAT_WP").html("BELUM TERDAFTAR");
                            
                          }
                      }
                    });                  
          } else if(pengenaan=='Omzet' || pengenaan=='Ketetapan'){
            $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_data_non_karcis'?>",
                     data: { nama: nama},
                     cache: false,
                     success: function(msga){
                            //alert(msga);
                             $("#non_perforasi").html(''); 
                             $("#tbl").html(''); 
                             $('#demo-form2').attr('action', '<?php echo base_url().'Sptpdhiburan/hiburan/preview';?>');
                             $("#non_karcis").html(msga);
                            
                          }
                        }); 
              
          } else if(pengenaan=='Karcis Non'){
            $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_data_karcis_non_perforasi'?>",
                     data: { nama: nama},
                     cache: false,
                     success: function(msga){
                            //alert(msga);
                             $("#tbl").html('');
                             $("#non_karcis").html('');
                             $('#demo-form2').attr('action', '<?php echo base_url().'Sptpdhiburan/hiburan/proses_karcis_non';?>');
                             $("#non_perforasi").html(msga);
                            
                          }
                        }); 
          };
                 $.ajax({
                   type: "POST",
                   url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_detail_usaha'?>",
                   data: { nama: nama},
                   cache: false,
                   success: function(msga){
                    //alert(msga);
                        if(msga!=0){
                            var exp = msga.split("|");
                            
                           /* if (cek==null) {
                                alert('Golongan Usaha Tidak Boleh Kosong, Silahkan Edit di menu tempat Usaha');
                            } else {*/

                            
                            $("#DESKRIPSI").html(exp[1]);  
                            $("#NAMAKELURAHAN").html(exp[3]);
                            $("#NAMAKEC").html(exp[5]);
                            $("#GOLONGAN").val(exp[0]);
                            
                            $("#ID_GOLONGAN").val(exp[0]);
                            $("#V_KODEKEL").val(exp[2]);
                            $("#V_KODEKEC").val(exp[4]);                  
                            changeValue($( "#GOLONGAN option:selected" ).val());
                          //}
                            
                          }else{
                            $("#NAMA_WP").val(null);  
                            $("#ALAMAT_WP").val(null); 
                            $("#VNAMA_WP").html("BELUM TERDAFTAR");
                            $("#VALAMAT_WP").html("BELUM TERDAFTAR");
                            
                          }
                      }
                    });
                
    } 
    function NamaUsaha(id){
                //alert(id);
                var npwpd = document.getElementById("NPWPD").value;
                //var npwpd=id;
                var jp='03';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]); 
                $("#NAMA_WP1").html(exp[0]);  
                $("#ALAMAT_WP1").html(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP1").html("BELUM TERDAFTAR");
                $("#ALAMAT_WP1").html("BELUM TERDAFTAR");
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
        
        $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpdhiburan/hiburan/get_nama_usaha'?>",
           data: { npwp: npwpd,
                   jp: jp   },
           cache: false,
           success: function(msga){
                  //alert(msga);
                    $("#NAMA_USAHA").html(msga); 
                }
              }); 
                
    }     
</script>
