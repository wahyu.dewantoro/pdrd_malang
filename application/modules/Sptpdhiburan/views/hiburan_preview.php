    <?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>

    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 12px;}
    textarea { font-size: 12px;}
    .input-group span{ font-size: 12px;}
    input[type='text'] { font-size: 12px; height:30px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>

<?php if($this->session->userdata('MS_ROLE_ID')==4) {?>  
  <div class="pull-right">
    <a class="btn btn-sm btn-primary" href="javascript:history.back()"><i class="fa fa-angle-double-left"></i> Kembali</a>
  </div>
  <?php } ?>
</div> 

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" />

                    <input type="hidden" name="MASA_PAJAK" value="<?php echo $MASA_PAJAK;?>">  
                    <input style="font-size: 12px" type="hidden" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>" <?php echo $disable ?>>                  
                    <input type="hidden" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>" <?php echo $disable ?> onchange="NamaUsaha(this.value);" >                  
                    <input type="hidden" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>" <?php echo $disable ?>>
                    <input type="hidden" name="ALAMAT_WP" value="<?php echo $ALAMAT_WP; ?>">

                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Masa Pajak</b></td>
                          <td>: <?php echo $namabulan[$MASA_PAJAK] ?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>Tahun</b></td>
                          <td>: <?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>NPWPD</b></td>
                          <td>: <?php echo $NPWPD; ?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>Nama WP</b></td>
                          <td>: <?php echo $NAMA_WP; ?></td>
                        </tr>
                        <tr>
                          <td width="19%"><b>Alamat WP</b></td>
                          <td>: <?php echo $ALAMAT_WP; ?> </td>
                        </tr>                      
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6">
                      <table class="table table-user-information">
                        <tbody>
                          <tr>
                            <td width="19%"><b>Nama Usaha</b></td>
                            <td>: <?php $exp=explode('|', $NAMA_USAHA);echo $exp[0]; ?></td>
                          </tr>
                          <tr>
                            <td width="19%"><b>Almat Usaha</b></td>
                            <td>: <?php echo $ALAMAT_USAHA; ?></td>
                          </tr>
                          <tr>
                            <?php $kec=$this->db->query(" select namakec from kecamatan where kodekec='$KECAMATAN'")->row();?>
                            <td><b>Kecamatan </b></td>
                            <td>: <?php echo $kec->NAMAKEC;?></td>
                          </tr>
                          <tr>
                            <?php $kel=$this->db->query("select namakelurahan from kelurahan where KODEKELURAHAN='$KELURAHAN' and kodekec='$KECAMATAN'")->row();?>
                            <td><b>Kelurahan</b></td>
                            <td>: <?php echo $kel->NAMAKELURAHAN;?></td>
                          </tr>                       
                        </tbody>
                      </table>
                      <input name="NAMA_USAHA"   value="<?php echo $NAMA_USAHA?>" type="hidden">
                      <input name="ALAMAT_USAHA" value="<?php echo $ALAMAT_USAHA?>" type="hidden">
                      <input type="hidden" value="<?php echo $KECAMATAN?>"  name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12">
                      <input type="hidden" value="<?php echo $KELURAHAN?>"  name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12">  
                  </div> 
                </div>
              </div>
            </div> 
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">        
                        <input type="hidden" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"  <?php echo $disable?> placeholder="No Formulir">
                          <input type="hidden" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                          <input type="hidden" name="BERLAKU_MULAI" id="BERLAKU_MULAI" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $BERLAKU_MULAI?>" required <?php echo $disable?> >
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kecamatan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="getkel(this);"  style="font-size:11px" id="KECAMATAN" <?php echo $disable?> name="KECAMATAN" placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($kec as $kec){ ?>
                          <option <?php if($kec->KODEKEC==$KECAMATAN){echo "selected";}?> value="<?php echo $kec->KODEKEC?>"><?php echo $kec->NAMAKEC ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kelurahan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select  style="font-size:11px" id="KELURAHAN" <?php echo $disable?> name="KELURAHAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                  <?php if($button==('Update SPTPD Hotel'||'Form SPTPD Hiburan')){?>
                          <?php foreach($kel as $kel){ ?>
                          <option <?php if($kel->KODEKELURAHAN==$KELURAHAN){echo "selected";}?> value="<?php echo $kel->KODEKELURAHAN?>"><?php echo $kel->NAMAKELURAHAN ?></option>
                          <?php }} ?>                          
                        </select>
                      </div>
                    </div>  -->
                    <?php
                    $persen = "var persen = new Array();\n";
                    $masa = "var masa = new Array();\n";
                    ?>
                    <div class="form-group">
                    <!-- <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >KODE KARCIS <sup>*</sup>
                    </label> -->
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <!-- <div class="input-group">
                        <span class="input-group-addon">KODE</span>
                      <input readonly type="text" id="KODE_KARCIS" name="KODE_KARCIS" required="required" placeholder="Harga Tanda masuk" class="form-control col-md-7 col-xs-12" value="<?php echo $KODE_KARCIS; ?>">       </div> -->
                    </div>
                  </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="changeValue(this.value)" style="font-size:11px" id="GOLONGAN" <?php echo $disable?> name="GOLONGAN"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                          <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                          <?php 
                          $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                          $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                        } ?>
                      </select>
                    </div>
                  </div> 
                  <script type="text/javascript">  
                    <?php echo $persen;echo $masa;?>             
                    function changeValue(id){
                      document.getElementById('PAJAK').value = persen[id].persen;
                      document.getElementById('MASA').value = masa[id].masa;
                      $("#HARGA_TANDA_MASUK").val(null); 
                      $("#DPP").val(null); 
                      $("#PAJAK_TERUTANG").val(null);
                    }
                  </script>    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  style="font-size:11px" id="DASAR_PENGENAAN" <?php echo $disable?> name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>
                        <option  <?php if($DASAR_PENGENAAN=='Omzet'){echo "selected";}?> value="Omzet">Omzet</option>
                        <option  <?php if($DASAR_PENGENAAN=='Ketetapan'){echo "selected";}?> value="Ketetapan">Ketetapan</option>
                        <option  <?php if($DASAR_PENGENAAN=='Karcis'){echo "selected";}?> value="Karcis">Karcis</option>
                      </select>
                    </div>
                    <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                        </label>
                      </div>
                    </div> 
                  </div>   
                  <input onchange='get(this);'  <?php echo $disable ?> type="hidden" id="JUMLAH" name="JUMLAH" required="required" placeholder="Jumlah" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH; ?>" >
                  <input readonly="" <?php echo $disable ?> type="hidden" id="JUMLAH_TOTAL" name="JUMLAH_TOTAL" required="required" placeholder="Jumlah Total" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_TOTAL; ?>" >
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Omzet/Penerimaan <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                      <input onchange='get(this);' <?php echo $disable ?> type="text" id="HARGA_TANDA_MASUK" name="HARGA_TANDA_MASUK" required="required" placeholder="Omzet/Penerimaan" class="form-control col-md-7 col-xs-12" value="<?php echo $HARGA_TANDA_MASUK; ?>">       </div>
                    </div>
                  </div>   
                  <!-- <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                                        
                      </div>
                  </div>  -->
                  <!-- <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Total <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                                        
                      </div>
                    </div>
                  </div> -->                  
                </div>                  
                <div class="col-md-6">
                  <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                        <div class="col-md-8">
                        <input type="hidden" value="<?php echo $FILE;?>" name="FILE"> <?php echo $FILE;?>
                        </div>
                  </div>    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"  <?php echo $disable?> placeholder="Masa Pajak" >
                    </div>
                    <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"  <?php echo $disable?> placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                      </div>
                    </div> 
                  </div> 
                  <?php if($disable==''){?>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Generate ID Biling</button>
                      <a href="<?php echo site_url('Sptpdhiburan/hiburan') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  <?php } ?> 
                </div>
              </div>
            </div>     
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<style type="text/css">
  .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    padding: 6px 6px 6px 6px;
  }
  td{
    font-size:12px;
  }
</style>
<script>

  function get() {
    var HARGA_TANDA_MASUK= parseFloat(nominalFormat(document.getElementById("HARGA_TANDA_MASUK").value))||0;
    var JUMLAH= parseFloat(nominalFormat(document.getElementById("JUMLAH").value))||0;
    var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
    var dpp=HARGA_TANDA_MASUK;//*JUMLAH;
    JUMLAH_TOTAL.value=getNominal(HARGA_TANDA_MASUK*JUMLAH)  ;
    DPP.value=getNominal(dpp)  ;
    PAJAK_TERUTANG.value=getNominal(dpp*PAJAK)  ;
    }
  /* HARGA_TANDA_MASUK */
  var HARGA_TANDA_MASUK = document.getElementById('HARGA_TANDA_MASUK');
  HARGA_TANDA_MASUK.addEventListener('keyup', function(e)
  {
    HARGA_TANDA_MASUK.value = formatRupiah(this.value);
  });
  /* JUMLAH */
  var JUMLAH = document.getElementById('JUMLAH');
  JUMLAH.addEventListener('keyup', function(e)
  {
    JUMLAH.value = formatRupiah(this.value);
  });
    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
  
  
  </script>
