<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hiburan extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        
        $this->load->model('Mhiburan');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');

        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
    public function index()
    {
      if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $dasar_pengenaan=$_GET['NPWPD'];
        } else {
                $bulan='';//date('n');//date('d-m-Y');
                $tahun='';//date('Y');//date('d-m-Y');
                $dasar_pengenaan='';
        }
        $sess=array(
                'hiburan_bulan'=>$bulan,
                'hiburan_tahun'=>$tahun,
                'dasar_pengenaan_hiburan'=>$dasar_pengenaan
         );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','hiburan_list',$data);
  }
  public function jsonhiburan() {
    header('Content-Type: application/json');
    echo $this->Mhiburan->jsonhiburan();
}
public function hapus()
{
    $response = array();
    
    if ($_POST['delete']) {
        
        
        $id = $_POST['delete'];
        $row = $this->Mhiburan->getById($id);
        
        if ($row) {
            $this->Mhiburan->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Supplier Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}
function get_nama_usaha(){
        $data1=$_POST['npwp'];
        $jp=$_POST['jp'];
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT id_inc,nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$data1."' and jenis_pajak='$jp'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Sptpdhiburan/get_nama_usaha',$data);
    }
function get_alamat_usaha(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("SELECT nama_usaha,ALAMAT_USAHA from tempat_usaha where id_inc='$exp[1]' ")->row();
            // print_r($data);
        echo $data->ALAMAT_USAHA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    function get_alamat_usaha1(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("SELECT nama_usaha,ALAMAT_USAHA||'|'||ID_OP ALAMAT_USAHA from tempat_usaha where id_inc='$exp[1]' ")->row();
            // print_r($data);
        echo $data->ALAMAT_USAHA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    function get_detail_usaha(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("select a.id_op||'|'||deskripsi||'|'||a.kodekel||'|'||D.NAMAKELURAHAN||'|'||c.kodekec||'|'||C.NAMAKEC data from tempat_usaha a
                                     join objek_pajak b on a.id_op=b.id_op
                                     join kecamatan c on c.kodekec=A.KODEKEC
                                     join kelurahan d on D.KODEKELURAHAN=A.KODEKEL
                                           and D.KODEKEC=A.KODEKEC
                                     where a.id_inc='$exp[1]'")->row();
            // print_r($data);
        echo $data->DATA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }       
public function table()
{  
   $data['mp']=$this->Mpokok->listMasapajak();
   $this->load->view('Sptpdhiburan/hiburan_table',$data);
}	
public function create() 
{
    if ($this->role==8) {
            $data = array(
        'button'                       => 'Form SPTPD Hiburan',
        'action'                       => site_url('Sptpdhiburan/hiburan/preview'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'tu'                           => $this->Mhiburan->getTu(),

    );           
    $this->template->load('Welcome/halaman','Wp/beranda/beranda_form',$data);
    } else {
    $data = array(
        'button'                       => 'Form SPTPD Hiburan',
        'action'                       => site_url('Sptpdhiburan/hiburan/preview'),
        'mp'                           => $this->Mpokok->listMasapajak(),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'HARGA_TANDA_MASUK'            => set_value('HARGA_TANDA_MASUK'),
        'JUMLAH'                       => set_value('JUMLAH'),
        'JUMLAH_TOTAL'                 => set_value('JUMLAH_TOTAL'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'KODE_KARCIS'                  => set_value('KODE_KARCIS'),
        'ID_TEMPAT_USAHA'              => set_value('ID_TEMPAT_USAHA'),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Mhiburan->getGol(),
        'NAMA_GOLONGAN'                => '',
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => '',
        'KODE_KARCIS'          => set_value('KODE_KARCIS'),
        'ID_TEMPAT_USAHA'      => set_value('ID_TEMPAT_USAHA')
    );           
    $this->template->load('Welcome/halaman','hiburan_form',$data);
  }
}
public function preview()
{
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'.'.$extensi;
                        $nf='upload/file_transaksi/hiburan/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
    $data = array(
        'button'                       => 'Form SPTPD Hiburan',
        'action'                       => site_url('Sptpdhiburan/hiburan/create_action'),
        'mp'                           => $this->Mpokok->listMasapajak(),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'HARGA_TANDA_MASUK'            => set_value('HARGA_TANDA_MASUK'),
        'JUMLAH'                       => set_value('JUMLAH'),
        'JUMLAH_TOTAL'                 => set_value('JUMLAH_TOTAL'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$this->input->post('GOLONGAN',TRUE)),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Mhiburan->getGol(),
        'KELURAHAN'                    => set_value('KELURAHAN'),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'KODE_KARCIS'                  => set_value('KODE_KARCIS'),
        'ID_TEMPAT_USAHA'              => set_value('ID_TEMPAT_USAHA'),
        'FILE'                         => $img_name_ktp,

    );
    $data['jenis']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='' and jenis_pajak='02'")->result();           
    $this->template->load('Welcome/halaman','Sptpdhiburan/hiburan_preview',$data);
}
public function create_action(){


    $m=sprintf("%02d", $this->input->post('MASA_PAJAK',TRUE));
    $BERLAKU_MULAI=date('d/'.$m.'/Y');
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $OP=$this->input->post('GOLONGAN',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $va=$this->Mpokok->getVA($OP);
    $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;
    $catatan=$this->input->post('CATATAN',TRUE);

    $this->db->trans_start();
            //  insert hibran
    //$this->db->set('KODE_KARCIS', $this->input->post('NPWPD',TRUE));
    $jumlah=str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH')));
    $persen= $this->input->post('PAJAK',TRUE);
    $nilai_lembar=str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_TANDA_MASUK')));
    $pajak_terutang=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
    $jumlah_total=str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_TOTAL')));
    $this->db->set('ID_TEMPAT_USAHA', $exp[1]);
    $this->db->set('DEVICE','web');
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    //$this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false);
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    //$this->db->set('HARGA_TANDA_MASUK',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_TANDA_MASUK'))));
    //$this->db->set('JUMLAH',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH'))));
    $this->db->set('JUMLAH_TOTAL',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_TOTAL'))));
    //$this->db->set('MASA', $this->input->post('MASA',TRUE));
    //$this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->set('STATUS', 0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('FILE_TRANSAKSI',$this->input->post('FILE',TRUE));
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
    if ($this->role=='8') {
        $this->db->set('NPWP_INSERT',$this->nip);
    }else {
        $this->db->set('NIP_INSERT',$this->nip);
    }
    $this->db->insert('SPTPD_HIBURAN');
    $this->Mpokok->registration($va,$nm,$pt,$ed);
    $this->db->trans_complete();
           $nomor=$this->db->query("SELECT MAX(ID_INC) NEXT_VAL FROM SPTPD_HIBURAN")->row();
           $NEXT=$nomor->NEXT_VAL;               
                    $this->db->query("INSERT into sptpd_hiburan_detail (kode_karcis,jumlah_terjual,jumlah_masa,persen_pajak,harga_karcis,jumlah_total,pajak_terutang,id_sptpd_hiburan,catatan)
                                      values ('".$this->input->post('NPWPD',TRUE)."','$jumlah','1','$persen','$nilai_lembar','$jumlah_total','$pajak_terutang','$NEXT','$catatan')");
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di simpan";
    }else{
            // gagal
        $nt="Gagal di simpan";
    }
    
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpdhiburan/hiburan'));
    }
}
public function proses_karcis(){
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'.'.$extensi;
                        $nf='upload/file_transaksi/hiburan/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                }
    $ptr=str_replace('.', '',$_POST['pajak_terutang']);
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $OP=$this->input->post('GOLONGAN_ID',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $va=$this->Mpokok->getVA($OP);
    $pt=array_sum($ptr);
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;

    $this->db->trans_start();
    //  insert hibran
    $dpp_k=str_replace('.', '',$_POST['dpp_k']);
    //$this->db->set('KODE_KARCIS', $this->input->post('NPWPD',TRUE));
    $this->db->set('ID_TEMPAT_USAHA', $exp[1]);
    $this->db->set('DEVICE','web');
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    //$this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"SYSDATE",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $OP);
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', "1");
    $this->db->set('HARGA_TANDA_MASUK',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_TANDA_MASUK'))));
    $this->db->set('JUMLAH',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH'))));
    $this->db->set('JUMLAH_TOTAL',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_TOTAL'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',array_sum($dpp_k));
    $this->db->set('PAJAK_TERUTANG',$pt);
    $this->db->set('STATUS', 0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('FILE_TRANSAKSI',$img_name_ktp);
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
    if ($this->role=='8') {
        $this->db->set('NPWP_INSERT',$this->nip);
    } else {
        $this->db->set('NIP_INSERT',$this->nip);
    }
    $this->db->insert('SPTPD_HIBURAN');
    $this->Mpokok->registration($va,$nm,$pt,$ed);
    $this->db->trans_complete();
      if(!empty($_POST['id_inc'])){
           $nomor=$this->db->query("SELECT MAX(ID_INC) NEXT_VAL FROM SPTPD_HIBURAN")->row();
           $NEXT=$nomor->NEXT_VAL;  
           $jumlah= count($_POST['qtt_terjual']); 
                for($i=0; $i < $jumlah; $i++) {
                    $qtt_terjual=str_replace('.', '',$_POST['qtt_terjual'][$i]);
                    $pajak_terutang=str_replace('.', '',$_POST['pajak_terutang'][$i]);
                    $catatan=$_POST['catatan'][$i];

                    $this->db->query("INSERT into sptpd_hiburan_detail (kode_karcis,jumlah_terjual,jumlah_masa,persen_pajak,harga_karcis,jumlah_total,pajak_terutang,id_sptpd_hiburan)
                                         select  nomor_seri,'$qtt_terjual','1',persen,nilai_lembar,nilai_lembar*'$qtt_terjual','$pajak_terutang','$NEXT'
                                         from perforasi
                                    where id_inc='".$_POST['id_inc'][$i]."'");
                
            }               
    }
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di simpan";
    }else{
            // gagal
        $nt="Gagal di simpan";
    }
    
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpdhiburan/hiburan'));
    }
}
public function proses_karcis_non(){
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'.'.$extensi;
                        $nf='upload/file_transaksi/hiburan/'.$img_name_ktp;
                        move_uploaded_file($tmp_file,$nf);
                }
    $pt=str_replace('.', '',$_POST['pajak_terutang']);
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $OP=$this->input->post('GOLONGAN_ID',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $va=$this->Mpokok->getVA($OP);
    $pt=array_sum($pt);
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;

    $this->db->trans_start();
    //  insert hibran
    $dpp_k=str_replace('.', '',$_POST['dpp_k']);
    //$this->db->set('KODE_KARCIS', $this->input->post('NPWPD',TRUE));
    $this->db->set('ID_TEMPAT_USAHA', $exp[1]);
    $this->db->set('DEVICE','web');
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    //$this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"SYSDATE",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $OP);
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', "1");
    $this->db->set('HARGA_TANDA_MASUK',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_TANDA_MASUK'))));
    $this->db->set('JUMLAH',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH'))));
    $this->db->set('JUMLAH_TOTAL',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_TOTAL'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',array_sum($dpp_k));
    $this->db->set('PAJAK_TERUTANG',array_sum($pt));
    $this->db->set('STATUS', 0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('FILE_TRANSAKSI',$img_name_ktp);
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
    if ($this->role=='8') {
        $this->db->set('NPWP_INSERT',$this->nip);
    } else {
        $this->db->set('NIP_INSERT',$this->nip);
    }
    $this->db->insert('SPTPD_HIBURAN');
    $this->Mpokok->registration($va,$nm,$pt,$ed);
    $this->db->trans_complete();
      if(!empty($_POST['id_inc'])){
           $nomor=$this->db->query("SELECT MAX(ID_INC) NEXT_VAL FROM SPTPD_HIBURAN")->row();
           $NEXT=$nomor->NEXT_VAL;  
           $jumlah= count($_POST['qtt_terjual']); 
                for($i=0; $i < $jumlah; $i++) {
                    $noseri=$_POST['No_SERI'][$i];
                    $harga=$_POST['NILAI'][$i];
                    $pajak=$_POST['PERSEN_PAJAK'][$i];
                    $qtt_terjual=str_replace('.', '',$_POST['qtt_terjual'][$i]);
                    $dpp_k=str_replace('.', '',$_POST['dpp_k'][$i]);
                    $pajak_terutang=str_replace('.', '',$_POST['pajak_terutang'][$i]);
                    $catatan=$_POST['catatan'][$i];

                    $this->db->query("INSERT into sptpd_hiburan_detail (kode_karcis,jumlah_terjual,jumlah_masa,persen_pajak,harga_karcis,jumlah_total,pajak_terutang,id_sptpd_hiburan)
                                      values  '$noseri','$qtt_terjual','1','$pajak','$harga','$dpp_k','$pajak_terutang','$NEXT'");
                
            }               
    }
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di simpan";
    }else{
            // gagal
        $nt="Gagal di simpan";
    }
    
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpdhiburan/hiburan'));
    }
}
public function delete($id){
    $row=$this->Mhiburan->getById($id);
    if($row){
        $this->db->trans_start();
            // delete hiburan
        $this->db->where("ID_INC",$id);
        $this->db->delete("SPTPD_HIBURAN");

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE){
            // sukses
            $nt="Berhasil di hapus";
        }else{
            // gagal
            $nt="Gagal di hapus";
        }
        
        $this->session->set_flashdata('notif', '<script>
          $(window).load(function(){
             swal("'.$nt.'", "", "success")
         });
         </script>');
        redirect(site_url('Sptpdhiburan/hiburan'));
    }
} 


function detail($id){
  $id=rapikan($id);
    $row = $this->Mhiburan->getById($id);
    if ($row) {
        $NAMA_GOLONGAN = $this->Mpokok->getNamaGolongan($row->ID_OP);
        $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($row->KODEKEC);
        $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($row->KODEKEL,$row->KODEKEC);      
        $data = array(
           'button'                => 'Form SPTPD Hiburan',
           'action'                => site_url('Sptpdhiburan/hiburan/update_action'),
           'mp'                    => $this->Mpokok->listMasapajak(),
           'disable'               => 'disabled',
           'ID_INC'                => set_value('ID_INC',$row->ID_INC),
           'MASA_PAJAK'            => set_value('MASA_PAJAK',$row->MASA_PAJAK),
           'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
           'NPWPD'                 => set_value('NPWPD',$row->NPWPD),
           'NAMA_WP'               => set_value('NAMA_WP',$row->NAMA_WP),
           'ALAMAT_WP'             => set_value('ALAMAT_WP',$row->ALAMAT_WP),
           'NAMA_USAHA'            => set_value('NAMA_USAHA',$row->NAMA_USAHA),
           'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
           'NO_FORMULIR'           => set_value('NO_FORMULIR',$row->NO_FORMULIR),
           'TANGGAL_PENERIMAAN'    => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
           'BERLAKU_MULAI'         => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
           'KECAMATAN'             => set_value('KECAMATAN',$row->KODEKEC),
           'KELURAHAN'             => set_value('KELURAHAN',$row->KODEKEL),
           'GOLONGAN_ID'           => set_value('GOLONGAN_ID',$row->ID_OP),
           'DASAR_PENGENAAN'       => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
           'JENIS_ENTRIAN'         => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
           'HARGA_TANDA_MASUK'     => set_value('HARGA_TANDA_MASUK',number_format($row->HARGA_TANDA_MASUK,'0','','.')),
           'JUMLAH'                => set_value('JUMLAH',number_format($row->JUMLAH,'0','','.')),
           'JUMLAH_TOTAL'          => set_value('JUMLAH_TOTAL',number_format($row->JUMLAH_TOTAL,'0','','.')),
           'MASA'                  => set_value('MASA',$row->MASA),
           'PAJAK'                 => set_value('PAJAK',$row->PAJAK),
           'DPP'                   => set_value('DPP',number_format($row->DPP,'0','','.')),
           'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
           'kec'                          => $this->Mpokok->getKec(),
           'GOLONGAN'                     => $this->Mhiburan->getGol(),
           'kel'                 => $this->Mhiburan->getkel($row->KODEKEC),
            'NAMA_GOLONGAN'       => $NAMA_GOLONGAN->DESKRIPSI,
            'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
            'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN,
            'KODE_KARCIS'          => set_value('KODE_KARCIS',$row->KODE_KARCIS),
            'ID_TEMPAT_USAHA'      => set_value('ID_TEMPAT_USAHA',$row->ID_TEMPAT_USAHA),
       );

        $this->template->load('Welcome/halaman','hiburan_form',$data);

    } else {

        redirect(site_url('Sptpdhiburan/hiburan'));
    }
}

public function update($id) 
{
    $row = $this->Mhiburan->getById($id);

    if ($row) {
        $data = array(
           'button'                => 'Form SPTPD Hiburan',
           'action'                => site_url('Sptpdhiburan/hiburan/update_action'),
           'mp'                    => $this->Mpokok->listMasapajak(),
           'disable'               => '',
           'ID_INC'                => set_value('ID_INC',$row->ID_INC),
           'MASA_PAJAK'            => set_value('MASA_PAJAK',$row->MASA_PAJAK),
           'TAHUN_PAJAK'           => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
           'NPWPD'                 => set_value('NPWPD',$row->NPWPD),
           'NAMA_WP'               => set_value('NAMA_WP',$row->NAMA_WP),
           'ALAMAT_WP'             => set_value('ALAMAT_WP',$row->ALAMAT_WP),
           'NAMA_USAHA'            => set_value('NAMA_USAHA',$row->NAMA_USAHA),
           'ALAMAT_USAHA'          => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
           'NO_FORMULIR'           => set_value('NO_FORMULIR',$row->NO_FORMULIR),
           'TANGGAL_PENERIMAAN'    => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
           'BERLAKU_MULAI'         => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
           'KECAMATAN'             => set_value('KECAMATAN',$row->KODEKEC),
           'KELURAHAN'             => set_value('KELURAHAN',$row->KODEKEL),
           'GOLONGAN_ID'           => set_value('GOLONGAN_ID',$row->ID_OP),
           'DASAR_PENGENAAN'       => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
           'JENIS_ENTRIAN'         => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
           'HARGA_TANDA_MASUK'     => set_value('HARGA_TANDA_MASUK',number_format($row->HARGA_TANDA_MASUK,'0','','.')),
           'JUMLAH'                => set_value('JUMLAH',number_format($row->JUMLAH,'0','','.')),
           'JUMLAH_TOTAL'          => set_value('JUMLAH_TOTAL',number_format($row->JUMLAH_TOTAL,'0','','.')),
           'MASA'                  => set_value('MASA',$row->MASA),
           'PAJAK'                 => set_value('PAJAK',$row->PAJAK),
           'DPP'                   => set_value('DPP',number_format($row->DPP,'0','','.')),
           'PAJAK_TERUTANG'        => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
           'kec'                   => $this->Mpokok->getKec(),
           'GOLONGAN'              => $this->Mhiburan->getGol(),
           'kel'                   => $this->Mhiburan->getkel($row->KODEKEC),
           'KODE_KARCIS'          => set_value('KODE_KARCIS',$row->KODE_KARCIS),
           'ID_TEMPAT_USAHA'      => set_value('ID_TEMPAT_USAHA',$row->ID_TEMPAT_USAHA),
       );

        $this->template->load('Welcome/halaman','hiburan_form',$data);

    } else {

        redirect(site_url('Sptpdhiburan/hiburan'));
    }
} 

public function update_action(){
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $this->db->trans_start();
            //  insert hibran
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('HARGA_TANDA_MASUK',str_replace(',', '.', str_replace('.', '',$this->input->post('HARGA_TANDA_MASUK'))));
    $this->db->set('JUMLAH',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH'))));
    $this->db->set('JUMLAH_TOTAL',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_TOTAL'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->where('ID_INC',$this->input->post('ID_INC'));
    $this->db->update('SPTPD_HIBURAN');
    
    $this->db->trans_complete();
    if ($this->db->trans_status() === TRUE){
            // sukses
        $nt="Berhasil di simpan";
    }else{
            // gagal
        $nt="Gagal di simpan";
    }
    
    $this->session->set_flashdata('notif', '<script>
      $(window).load(function(){
         swal("'.$nt.'", "", "success")
     });
     </script>');
    redirect(site_url('Sptpdhiburan/hiburan'));      
}
function pdf_kode_biling_hiburan($kode_biling=""){
            $kode_biling=rapikan($kode_biling);
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Sptpdhiburan/sptpd_hiburan_pdf',$data,true);
            //$html     =$this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_pdf');
            $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
 function get_data_perforasi(){
            $nama=$_POST['nama'];
            $exp=explode('|', $nama);
           // echo $exp[1]; 
            error_reporting(E_ALL^(E_NOTICE|E_WARNING)); 
            $data['data']=$this->db->query("SELECT * FROM V_CEK_SPTPD_PERFORASI where ID_TEMPAT_USAHA='$exp[1]' order by tgl_perforasi")->result();           
            $this->load->view('Sptpdhiburan/v_tabel',$data);
    }
 function get_data_non_karcis(){
    $data = array(
        'button'                       => 'Form SPTPD Hiburan',
        'action'                       => site_url('Sptpdhiburan/hiburan/preview'),
        'mp'                           => $this->Mpokok->listMasapajak(),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'HARGA_TANDA_MASUK'            => set_value('HARGA_TANDA_MASUK'),
        'JUMLAH'                       => set_value('JUMLAH'),
        'JUMLAH_TOTAL'                 => set_value('JUMLAH_TOTAL'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'KODE_KARCIS'                  => set_value('KODE_KARCIS'),
        'ID_TEMPAT_USAHA'              => set_value('ID_TEMPAT_USAHA'),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Mhiburan->getGol(),
        'NAMA_GOLONGAN'                => '',
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => '',
        'KODE_KARCIS'          => set_value('KODE_KARCIS'),
        'ID_TEMPAT_USAHA'      => set_value('ID_TEMPAT_USAHA')
    ); 
           /* $nama=$_POST['nama'];
            $exp=explode('|', $nama);
           // echo $exp[1];  
            $data['data']=$this->db->query("SELECT * FROM V_CEK_SPTPD_PERFORASI where ID_TEMPAT_USAHA='$exp[1]'")->result();           
            $data['data1']=$this->db->query("SELECT * FROM V_CEK_SPTPD_PERFORASI where ID_TEMPAT_USAHA='$exp[1]'")->result_array();           */
            $this->load->view('Sptpdhiburan/v_non_karcis',$data);
    }
function get_data_karcis_non_perforasi(){
    $data = array(
        'button'                       => 'Form SPTPD Hiburan',
        'action'                       => site_url('Sptpdhiburan/hiburan/preview'),
        'mp'                           => $this->Mpokok->listMasapajak(),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'HARGA_TANDA_MASUK'            => set_value('HARGA_TANDA_MASUK'),
        'JUMLAH'                       => set_value('JUMLAH'),
        'JUMLAH_TOTAL'                 => set_value('JUMLAH_TOTAL'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'KODE_KARCIS'                  => set_value('KODE_KARCIS'),
        'ID_TEMPAT_USAHA'              => set_value('ID_TEMPAT_USAHA'),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Mhiburan->getGol(),
        'NAMA_GOLONGAN'                => '',
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => '',
        'KODE_KARCIS'          => set_value('KODE_KARCIS'),
        'ID_TEMPAT_USAHA'      => set_value('ID_TEMPAT_USAHA')
    ); 
            $this->load->view('Sptpdhiburan/v_non_perf_karcis',$data);
    }
    public function karcis($x)
    {
         $data['x']=$x;
         $this->load->view('Sptpdhiburan/v_non_perf_append',$data);
    }
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */


/*    
*/