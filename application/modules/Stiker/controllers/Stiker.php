<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stiker extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Master/Mpokok');
        /*$this->load->library('datatables');*/
        //$this->load->model('Mpdf');
 }

  function data_pajak($kode_biling=""){
  		$data['detail_wp']=$this->db->query("SELECT NPWPD,NAMA_WP,ALAMAT_WP,OBJEK_PAJAK,ALAMAT_OP,DPP_TAGIHAN,TAGIHAN+DENDA-POTONGAN PAJAK_TERUTANG FROM TAGIHAN WHERE KODE_BILING='$kode_biling'")->row();
  		$data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select nomor_berkas,A.KODE_BILING,a.npwpd,a.nama_wp,a.alamat_wp,objek_pajak,alamat_op,a.MASA_PAJAK,
                a.TAHUN_PAJAK,namakec,namakelurahan,NOREK,tagihan pajak_terutang, TO_CHAR (e.BERLAKU_MULAI, 'dd-mm-yyyy') BERLAKU_MULAI,
                                                TO_CHAR (akhir_masa_berlaku, 'dd-mm-yyyy') AKHIR_MASA_BERLAKU
                                                from tagihan A
                                                  JOIN SPTPD_reklame b on a.kode_biling=b.kode_biling
                                                   join sptpd_reklame_detail e on E.SPTPD_REKLAME_ID=b.id_inc
                                                  left JOIN KECAMATAN c  ON a.kodekec = c.KODEKEC 
                                                 left JOIN KELURAHAN d ON a.kodekel = d.KODEKELURAHAN AND a.kodekec = d.KODEKEC
                                                where a.kode_biling='$kode_biling'")->row();
            $data['data']=$this->db->query("SELECT * FROM V_DET_REKLAME WHERE KODE_BILING='$kode_biling'")->result();
            $this->db->query("UPDATE TAGIHAN SET CETAK_SKPD='1' WHERE KODE_BILING='$kode_biling'");
    
        //$this->template->load('Welcome/halaman1','detail_data_pajak',$data);
        $this->load->view('Stiker/detail_data_pajak',$data);
  }

  function pdf_skpd_air_meter($kode_biling=""){
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select NOMOR_BERKAS,HARGA,A.KODE_BILING,a.npwpd,a.nama_wp,a.alamat_wp,objek_pajak,alamat_op,a.MASA_PAJAK,a.TAHUN_PAJAK,a.DENDA,a.POTONGAN,PAJAK,
                                            CEIL(tagihan-a.POTONGAN) PAJAK_TERUTANG,namakec,namakelurahan,a.kode_rek,jenis,cara_pengambilan,volume_air_meter,DPP,
                                            penggunaan_bulan_lalu_meter,penggunaan_hari_ini_meter,jenispengambilan
                                                from tagihan A
                                                JOIN SPTPD_air_tanah b on a.kode_biling=b.kode_biling
                                                 JOIN KECAMATAN c  ON A.KODEKEC = c.KODEKEC 
                                                 JOIN KELURAHAN d ON A.KODEKEL = d.KODEKELURAHAN AND A.KODEKEC = d.KODEKEC
                                                 join peruntukan_air_tanah e on e.no=b.peruntukan
                                                where a.kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Pdf/Pdf_skpd_air_meter',$data,true);
              $qr=$this->db->query("select npwpd,nama_wp,MASA_PAJAK,TAHUN_PAJAK,kode_rek from tagihan where kode_biling='$kode_biling'")->row();
              $this->db->query("UPDATE TAGIHAN SET CETAK_SKPD='1' WHERE KODE_BILING='$kode_biling'");
              $jabatan=$this->Mpokok->getJabatan('kabid pdrd');
              $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $qr->KODE_REK.$qr->NPWPD.$qr->MASA_PAJAK.$qr->TAHUN_PAJAK.$jabatan->NIP; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
    function pdf_skpd_air_non_meter($kode_biling=""){
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select NOMOR_BERKAS,HARGA,A.KODE_BILING,a.npwpd,a.nama_wp,a.alamat_wp,objek_pajak,alamat_op,a.MASA_PAJAK,a.TAHUN_PAJAK,a.DENDA,ceil(a.POTONGAN)POTONGAN,PAJAK,
                                            CEIL(tagihan-a.POTONGAN)PAJAK_TERUTANG,namakec,namakelurahan,a.kode_rek,jenis,cara_pengambilan,volume_air_meter,DPP,
                                            jenispengambilan,debit_non_meter,penggunaan_hari_non_meter,penggunaan_bulan_non_meter
                                                from tagihan A
                                                JOIN SPTPD_air_tanah b on a.kode_biling=b.kode_biling
                                                 JOIN KECAMATAN c  ON A.KODEKEC = c.KODEKEC 
                                                 JOIN KELURAHAN d ON A.KODEKEL = d.KODEKELURAHAN AND A.KODEKEC = d.KODEKEC
                                                 join peruntukan_air_tanah e on e.no=b.peruntukan
                                                where a.kode_biling='$kode_biling'")->row();
              $qr=$this->db->query("select npwpd,nama_wp,MASA_PAJAK,TAHUN_PAJAK,kode_rek from tagihan where kode_biling='$kode_biling'")->row();
              $this->db->query("UPDATE TAGIHAN SET CETAK_SKPD='1' WHERE KODE_BILING='$kode_biling'");
              $jabatan=$this->Mpokok->getJabatan('kabid pdrd');
              
              $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $qr->KODE_REK.$qr->NPWPD.$qr->MASA_PAJAK.$qr->TAHUN_PAJAK.$jabatan->NIP; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
            
            $this->load->view('Pdf/Pdf_skpd_air_non_meter_v',$data);
           /* $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);*/
    }
    function pdf_skpd_reklame($kode_biling=""){
            $data['jabatan']=$this->Mpokok->getJabatan('kabid pdrd');
            $data['detail']=$this->db->query("select nomor_berkas,A.KODE_BILING,a.npwpd,a.nama_wp,a.alamat_wp,objek_pajak,alamat_op,a.MASA_PAJAK,
                a.TAHUN_PAJAK,namakec,namakelurahan,NOREK,tagihan pajak_terutang, TO_CHAR (e.BERLAKU_MULAI, 'dd-mm-yyyy') BERLAKU_MULAI,
                                                TO_CHAR (akhir_masa_berlaku, 'dd-mm-yyyy') AKHIR_MASA_BERLAKU
                                                from tagihan A
                                                  JOIN SPTPD_reklame b on a.kode_biling=b.kode_biling
                                                   join sptpd_reklame_detail e on E.SPTPD_REKLAME_ID=b.id_inc
                                                  left JOIN KECAMATAN c  ON a.kodekec = c.KODEKEC 
                                                 left JOIN KELURAHAN d ON a.kodekel = d.KODEKELURAHAN AND a.kodekec = d.KODEKEC
                                                where a.kode_biling='$kode_biling'")->row();
            $data['data']=$this->db->query("SELECT * FROM V_DET_REKLAME WHERE KODE_BILING='$kode_biling'")->result();
            $this->db->query("UPDATE TAGIHAN SET CETAK_SKPD='1' WHERE KODE_BILING='$kode_biling'");
            $html     =$this->load->view('Pdf/Pdf_skpd_reklame',$data,true);
              $qr=$this->db->query("select npwpd,nama_wp,MASA_PAJAK,TAHUN_PAJAK,kode_rek from tagihan where kode_biling='$kode_biling'")->row();

              $jabatan=$this->Mpokok->getJabatan('kabid pdrd');
              $this->load->library('ciqrcode'); //pemanggilan library QR CODE
              $config['cacheable']  = true; //boolean, the default is true
              $config['cachedir']   = '/assets/'; //string, the default is application/cache/
              $config['errorlog']   = '/assets/'; //string, the default is application/logs/
              $config['imagedir']   = '/assets/images/'; //direktori penyimpanan qr code
              $config['quality']    = true; //boolean, the default is true
              $config['size']     = '1024'; //interger, the default is 1024
              $config['black']    = array(224,255,255); // array, default is array(255,255,255)
              $config['white']    = array(70,130,180); // array, default is array(0,0,0)
              $this->ciqrcode->initialize($config);

              $image_name=$kode_biling.'.png'; //buat name dari qr code sesuai dengan nim

              $params['data'] = $qr->KODE_REK.$qr->NPWPD.$qr->MASA_PAJAK.$qr->TAHUN_PAJAK.$jabatan->NIP; //data yang akan di jadikan QR CODE
              $params['level'] = 'H'; //H=High
              $params['size'] = 10;
              $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
              $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
            
            $filename = $kode_biling.".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
  
  
}