<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mskpdkb extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->nip  =$this->session->userdata('NIP');
         $this->upt  =$this->session->userdata('UNIT_UPT_ID');
        // $this->role =$this->session->userdata('MS_ROLE_ID');
    }     

    function total_rows_penerbitan_skpdkb($th=NULL,$npwpd=NULL) {
        $this->db->from('V_SKPDKB');
        //$this->db->where('JENIS_PAJAK',$jp);
        $this->db->where('TAHUN_PAJAK',$th);
            if ($npwpd!=NULL) {
                $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%')",null,false);
            }
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_penerbitan_skpdkb($limit, $start = 0, $th=NULL,$npwpd=NULL) {        
        $this->db->select("*");    
        //$this->db->where('JENIS_PAJAK',$jp);
        $this->db->where('TAHUN_PAJAK',$th);
            if ($npwpd!=NULL) {
                $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(NAMA_USAHA) LIKE '%$npwpd%'  OR NPWPD LIKE '%$npwpd%')",null,false);
            }
        $this->db->order_by('ID_INC');
        $this->db->limit($limit, $start);
    return $this->db->get('V_SKPDKB')->result();
    }


    function total_rows_rekap_hotel($tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
    $this->db->from('UPT_REKAP_HOTEL');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_hotel($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                        JUMLAH_KAMAR,TARIF_RATA_RATA,KAMAR_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_HOTEL')->result();
    }

    function total_rows_rekap_restoran($tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
    $this->db->from('UPT_REKAP_RESTORAN');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
        if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_restoran($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                        JUMLAH_MEJA,TARIF_RATA_RATA,MEJA_TERISI,TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,JENIS_RESTO");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_RESTORAN')->result();
    }

    function total_rows_rekap_hiburan($tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
    $this->db->from('UPT_REKAP_HIBURAN');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
     function get_limit_data_rekap_hiburan($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,DASAR_PENGENAAN,HARGA_TANDA_MASUK,JUMLAH");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_HIBURAN')->result();
    }
    function total_rows_rekap_reklame($tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
    $this->db->from('UPT_REKAP_REKLAME');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    return $this->db->count_all_results();
    }
     function get_limit_data_rekap_reklame($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,TOTAL");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_REKLAME')->result();
    }
    function total_rows_rekap_ppj($tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
    $this->db->from('UPT_REKAP_PPJ');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_ppj($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,KEPERLUAN,TARIF,JUMLAH_PEMAKAIAN,DAYA_TERPASANG,PLN");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_PPJ')->result();
    }

    function total_rows_rekap_galian($tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
    $this->db->from('UPT_REKAP_GALIAN');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_galian($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$upt=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,ID_LOGAM");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_GALIAN')->result();
    }
    function total_rows_rekap_at($tgl1 = NULL,$tgl2 = NULL,$upt=NULL,$kecamatan=NULL,$kelurahan=NULL,$sts=NULL) {
    $this->db->from('UPT_REKAP_AT');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_at($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$upt=NULL,$kecamatan=NULL,$kelurahan=NULL,$sts=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,CEIL(DPP)DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,CEIL(TAGIHAN)TAGIHAN,DENDA,(CEIL(TAGIHAN)+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,CARA_PENGAMBILAN,PERUNTUKAN,
    PENGGUNAAN_HARI_NON_METER,PENGGUNAAN_BULAN_NON_METER,PENGGUNAAN_HARI_INI_METER,PENGGUNAAN_BULAN_LALU_METER,VOLUME_AIR_METER,JENIS,DEBIT_NON_METER");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_AT')->result();
    }

    function total_rows_rekap_parkir($tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
    $this->db->from('UPT_REKAP_PARKIR');
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {
        $this->db->where("(LOWER(NAMA_WP) LIKE '%$npwpd%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
   // $this->db->where('TAHUN_PAJAK', $tgl1);
    return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data_rekap_parkir($limit, $start = 0, $tgl1 = NULL,$tgl2 = NULL,$wp=NULL) {
   // $this->db->order_by('ID_INC', $this->order);
    $this->db->select("ID_INC,MASA_PAJAK,TAHUN_PAJAK,NPWPD,NAMA_WP,OBJEK_PAJAK,ALAMAT_OP,DPP,STATUS ,KODE_BILING,TO_CHAR(TGL_PENETAPAN, 'dd-mm-yyyy') AS TGL_PENETAPAN,
                      TO_CHAR(JATUH_TEMPO, 'dd-mm-yyyy') AS JATUH_TEMPO,TAGIHAN,DENDA,(TAGIHAN+DENDA-POTONGAN) AS JUMLAH_KETETAPAN,SSPD,REKAP_BON,REKAP_BIL,LAINYA,ID_PARKIR");    
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') >= TO_DATE('$tgl1','DD-MM-YYYY')");
    $this->db->where("TO_DATE(TO_CHAR(TGL_PENETAPAN,'dd/mm/yyyy'),'dd/mm/yyyy') <= TO_DATE('$tgl2','DD-MM-YYYY')");
    $this->db->where("STATUS_SKPDKB !='0'");
    if ($wp!=NULL) {$this->db->where("(KODE_BILING LIKE '%$wp%' OR LOWER(NAMA_WP) LIKE '%$wp%' OR LOWER(OBJEK_PAJAK) LIKE '%$wp%'  OR NPWPD LIKE '%$wp%')",null,false);}
    $this->db->order_by('TGL_PENETAPAN', 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get('UPT_REKAP_PARKIR')->result();
    }
    

}