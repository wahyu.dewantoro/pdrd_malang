       <?php $namabulan=array(
        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        ) ?>

      <style>
      th { font-size: 10px; }
      td { font-size: 10px; }
      label { font-size: 11px;}
      textarea { font-size: 11px;}
      .input-group span{ font-size: 11px;}
      input[type='text'] { font-size: 11px; height:30px}
    </style>
      <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
      <a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="" method="post" enctype="multipart/form-data" >
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content" >  
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp1){ ?>
                            <option <?php if($mp1->ID_INC==date("m")){echo "selected";}?> value="<?php echo $mp1->ID_INC?>"><?php echo $namabulan[$mp1->ID_INC] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <input type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo date("Y");?>">                
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD  <sup>*</sup> 
                      </label>
                      <div class="col-md-9">
                        <input type="text" id="NPWPD" name="NPWPD" readonly required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $detail->NPWPD; ?>">                  
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <input type="text" id="NAMA_WP"  readonly name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $detail->NAMA; ?>">                
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Alamat WP <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <input type="text" readonly id="ALAMAT_WP" rows="3"  class="form-control" placeholder="Alamat" name="ALAMAT_WP" value="<?php echo $detail->ALAMAT; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" readonly required="required"  placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $detail->NAMA_USAHA; ?>">               
                      </div>
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="pengenaan(this.value)" style="font-size:11px" id="DASAR_PENGENAAN"  name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <option value="Karcis">Karcis</option>
                          <option value="Omzet">Omzet</option>
                          <option value="Ketetapan">Ketetapan</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Sebab Penerbitan SKPDKB<sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="dasar_penerbitan(this.value)"  style="font-size:11px" id="ALASAN"  name="sebab" class="form-control select2 col-md-7 col-xs-12" required>
                          <option value="">Pilih</option>
                            <?php foreach($sebab as $row){ ?>
                            <option  value="<?php echo $row->KODE_ALASAN?>" ><?= $row->ALASAN  ?></option>
                            <?php } ?>
                         
                        </select>
                      </div>
                    </div>
                    <div id="detail"></div>         
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                  <div class="x_title">
                    <h4><i class="fa fa-desktop"></i> Data Tempat Usaha </h4>
                    <div class="clearfix"></div>
                  </div>
                  <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Golongan</b></td>
                          <td><span id="DESKRIPSI"><?php echo $detail->DESKRIPSI; ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Alamat </b></td>
                          <td><span id="ALAMAT_USAHA"><?php echo $detail->ALAMAT_USAHA;?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kecamatan </b></td>
                          <td><span id="NAMAKEC"><?php echo $detail->NAMAKEC; ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kelurahan</b></td>
                          <td><span id="NAMAKELURAHAN"><?php echo $detail->NAMAKELURAHAN; ?></span></td>
                        </tr>                       
                      </tbody>
                    </table>
                    <input type="hidden" id="V_ALAMAT_USAHA" name="ALAMAT_USAHA" required value="<?= $detail->ALAMAT_USAHA;?>">  
                    <input type="hidden" id="V_KODEKEC" name="KECAMATAN" required value="<?= $detail->KODEKEC;?>">
                    <input type="hidden" id="V_KODEKEL" name="KELURAHAN" required value="<?= $detail->KODEKEL;?>">   
                    <input type="hidden" id="GOLONGAN" name="GOLONGAN" required value="<?= $detail->ID_OP;?>"> 
                    <input type="hidden" id="ID_INC" name="NAMA_USAHA" required value="<?= $detail->NAMA_USAHA.'|'.$detail->ID_INC;?>">   
                    <div id="non_karcis"></div>
                <!-- isiiiiiiiiiiiiiiii -->
              </div>
              <div class="col-md-12 col-sm-12" id="OPSI">
              <?php error_reporting(E_ALL^(E_NOTICE|E_WARNING));?>
              </div>
             <!--  <div class="col-md-6 col-sm-6" >
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>



</div>

<script>
    function get() {
      var L=parseFloat(nominalFormat(document.getElementById("JUMLAH_LEMBAR").value))|| 0;
      var N=parseFloat(nominalFormat(document.getElementById("NOMINAL").value))|| 0;
      var NON=parseFloat(nominalFormat(document.getElementById("PENERIMAAN_NON_KARCIS").value))|| 0;
     
    }
    
    function gantiTitikKoma(angka){
        return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
        return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
      }
    }

  /* JUMLAH_LEMBAR */
  var JUMLAH_LEMBAR = document.getElementById('JUMLAH_LEMBAR');
  JUMLAH_LEMBAR.addEventListener('keyup', function(e)
  {
    JUMLAH_LEMBAR.value = formatRupiah(this.value);
  });
  /* NOMINAL */
  var NOMINAL = document.getElementById('NOMINAL');
  NOMINAL.addEventListener('keyup', function(e)
  {
    NOMINAL.value = formatRupiah(this.value);
  });    
  /* PENERIMAAN_NON_KARCIS */
  var PENERIMAAN_NON_KARCIS = document.getElementById('PENERIMAAN_NON_KARCIS');
  PENERIMAAN_NON_KARCIS.addEventListener('keyup', function(e)
  {
    PENERIMAAN_NON_KARCIS.value = formatRupiah(this.value);
  });
  /* DPP */
  var DPP = document.getElementById('DPP');
  DPP.addEventListener('keyup', function(e)
  {
    DPP.value = formatRupiah(this.value);
  });
  /* PAJAK_TERUTANG */
  var PAJAK_TERUTANG = document.getElementById('PAJAK_TERUTANG');
  PAJAK_TERUTANG.addEventListener('keyup', function(e)
  {
    PAJAK_TERUTANG.value = formatRupiah(this.value);
  });
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
function ribuan (angka)
{
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan  = reverse.match(/\d{1,3}/g);
  ribuan  = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}
  function validasi(){
    var npwpd=document.forms["demo-form2"]["NPWPD"].value;
    var number=/^[0-9]+$/; 
    if (npwpd==null || npwpd==""){
      swal("NPWPD Harus di Isi", "", "warning")
      return false;
    };
    if (NAMA_USAHA==null || NAMA_USAHA==""){
      swal("Nama Usaha Harus di Isi", "", "warning")
      return false;
    };
  }


  function pengenaan(aa){
      var pengenaan = aa;
      var nama =document.getElementById("ID_INC").value;
      //alert(gol);
      if (pengenaan=='Karcis') {
                $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Skpdkb/Skpdkb/get_data_perforasi'?>",
                     data: { nama: nama},
                     cache: false,
                     success: function(msga){
                            //alert(nama);
                             $("#non_karcis").html('');
                             $('#demo-form2').attr('action', '<?php echo base_url().'Skpdkb/create_action_parkir_karcis';?>');
                             $("#OPSI").html(msga); 
                          }
                        });
                
      } else if(pengenaan=='Omzet' || pengenaan=='Ketetapan'){
                $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Skpdkb/Skpdkb/get_data_non_karcis'?>",
                     data: { nama: nama},
                     cache: false,
                     success: function(msga){
                            //alert(msga);
                             $("#OPSI").html(''); 
                             $('#demo-form2').attr('action', '<?php echo base_url().'Skpdkb/create_action_parkir';?>');
                             $("#non_karcis").html(msga);
                            
                          }
                        }); 
                };
                
    }
    function dasar_penerbitan(aa){
      var penerbitan = aa;
     
                $.ajax({
                     type: "POST",
                     url: "<?php echo base_url().'Skpdkb/Skpdkb/get_data_kekurangan'?>",
                     data: { penerbitan: penerbitan},
                     cache: false,
                     success: function(msga){
                            //alert(nama);
                             $("#detail").html(msga); 
                          }
                        });
                
    }
</script>
