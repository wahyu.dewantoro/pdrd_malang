
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >1a. Kompensasi Kelebihan Bayar <sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                        <input type="text" id="KOMPENSASASI"  name="kompensasi" required onchange='get2(this);' placeholder="Kompensasi" class="form-control" >                
                        </div>
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >1b. Setoran Terutang<sup>*</sup>
                      </label>
                      <div class="col-md-9 ">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                        <input type="text" id="SETORAN_TERUTANG"  class="form-control"  onchange='get2(this);' name="setoran_terutang">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >1c. Lain-lain</label>
                      <div class="col-md-9 ">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                        <input type="text" id="LAIN_LAIN"  class="form-control"  onchange='get2(this);' name="lain_lain">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >1d. Jumlah yang dikreditkan (1a+1b+1c)</label>
                      <div class="col-md-9 ">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                        <input type="text" id="POTONGAN"  class="form-control"  readonly onchange='get2(this);' required name="jumlah_potongan">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >2a. Bunga</label>
                      <div class="col-md-9 ">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                        <input type="text" id="BUNGA"  class="form-control" onchange='get1(this);' name="bunga">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >2b. Kenaikan</label>
                      <div class="col-md-9 ">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                        <input type="text" id="KENAIKAN"  class="form-control"  onchange='get1(this);' required name="kenaikan">
                      </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >2c. Jumlah jumlah_denda</label>
                      <div class="col-md-9 ">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                        <input type="text" id="DENDA"  class="form-control" readonly onchange='get1(this);' name="jumlah_denda">
                      </div>
                      </div>
                    </div>
          
              
                <script type="text/javascript">
                  function get2() {
                          var KOM=parseFloat(nominalFormat(document.getElementById("KOMPENSASASI").value))|| 0;
                          var SET=parseFloat(nominalFormat(document.getElementById("SETORAN_TERUTANG").value))|| 0;
                          var LN=parseFloat(nominalFormat(document.getElementById("LAIN_LAIN").value))|| 0;
                          var P=(KOM+SET+LN).toFixed(0);
                          POTONGAN.value= getNominal(P);
                        }

                  function get1() {
                          var B=parseFloat(nominalFormat(document.getElementById("BUNGA").value))|| 0;
                          var K=parseFloat(nominalFormat(document.getElementById("KENAIKAN").value))|| 0;
                          var P=(B+K).toFixed(0);
                          DENDA.value= getNominal(P);
                  }
    
    function gantiTitikKoma(angka){
        return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
        return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
      }
    }

  var KOMPENSASASI = document.getElementById('KOMPENSASASI');
  KOMPENSASASI.addEventListener('keyup', function(e)
  {
    KOMPENSASASI.value = formatRupiah(this.value);
  }); 

  var SETORAN_TERUTANG = document.getElementById('SETORAN_TERUTANG');
  SETORAN_TERUTANG.addEventListener('keyup', function(e)
  {
    SETORAN_TERUTANG.value = formatRupiah(this.value);
  });

   var LAIN_LAIN = document.getElementById('LAIN_LAIN');
  LAIN_LAIN.addEventListener('keyup', function(e)
  {
    LAIN_LAIN.value = formatRupiah(this.value);
  }); 

   var BUNGA = document.getElementById('BUNGA');
  BUNGA.addEventListener('keyup', function(e)
  {
    BUNGA.value = formatRupiah(this.value);
  });

   var KENAIKAN = document.getElementById('KENAIKAN');
  KENAIKAN.addEventListener('keyup', function(e)
  {
    KENAIKAN.value = formatRupiah(this.value);
  }); 
 

  
  /* Fungsi */
  function formatRupiah(angka, prefix)
  {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
function ribuan (angka)
{
  var reverse = angka.toString().split('').reverse().join(''),
  ribuan  = reverse.match(/\d{1,3}/g);
  ribuan  = ribuan.join('.').split('').reverse().join('');
  return ribuan;
}
  
</script>
            
       