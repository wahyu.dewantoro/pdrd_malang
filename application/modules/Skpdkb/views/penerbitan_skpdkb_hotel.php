    <?php $namabulan=array(
      '',
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
      ) ?>

    <style>
    th { font-size: 10px; }
    td { font-size: 10px; }
    label { font-size: 12px;}
    textarea { font-size: 12px;}
    .input-group span{ font-size: 12px;}
    input[type='text'] { font-size: 12px; height:30px}
  </style>
  <div class="page-title">
   <div class="title_left">
    <h3><?php echo $button; ?></h3>
  </div>
 
  <div class="pull-right">
    <!-- <?php echo anchor('Sptpd_hotel/sptpd_hotel','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?> -->
<a href="javascript:history.back()" class="btn btn-sm btn-primary" id="back"><i class="fa fa-angle-double-left"></i> Kembali</a>    
  </div>
</div> 

<div class="clearfix"></div>                  
<div class="row">

  <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >    
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">

        <div class="x_content" >
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >
                  <div class="col-md-6">
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                        </label>
                        <div class="col-md-9">
                          <select id="MASA_PAJAK" <?php echo $disable?> name="MASA_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Pilih</option>
                            <?php foreach($mp as $mp1){ ?>
                            <option <?php if($mp1->ID_INC==date("m")){echo "selected";}?> value="<?php echo $mp1->ID_INC?>"><?php echo $namabulan[$mp1->ID_INC] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                       <input type="text" name="TAHUN_PAJAK" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo date("Y");?>">                              
                      </div>
                    </div>                              
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                      </label>
                      <div class="col-md-9 col-sm-7 col-xs-12">
                        <input type="text" id="NPWPD" name="NPWPD" required="required" readonly class="form-control col-md-7 col-xs-12" value="<?php echo $detail->NPWPD; ?>">                  
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" readonly required="required"  placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $detail->NAMA_USAHA; ?>">               
                      </div>
                    </div>             
                    <!-- <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td width="19%"><b>Golongan</b></td>
                          <td><span id="DESKRIPSI"><?php if ($disable=='disabled') {echo "$NAMA_GOLONGAN";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kecamatan </b></td>
                          <td><span id="NAMAKEC"><?php if ($disable=='disabled') {echo "$NAMA_KECAMATAN";} ?></span></td>
                        </tr>
                        <tr>
                          <td><b>Kelurahan</b></td>
                          <td><span id="NAMAKELURAHAN"><?php if ($disable=='disabled') {echo "$NAMA_KELURAHAN";} ?></span></td>
                        </tr>                       
                      </tbody>
                    </table> -->
                    <input type="hidden" id="V_KODEKEC" name="KECAMATAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >
                    <input type="hidden" id="V_KODEKEL" name="KELURAHAN" required="required" class="form-control col-md-7 col-xs-12" <?php echo $disable ?> >  
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama WP<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_WP"  readonly name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $detail->NAMA; ?>">                  
                      </div>
                    </div> 

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  WP<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                         <input type="text" readonly id="ALAMAT_WP" rows="3"  class="form-control" placeholder="Alamat" name="ALAMAT_WP" value="<?php echo $detail->ALAMAT; ?>">                
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                      </label>
                      <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" readonly id="ALAMAT_WP" rows="3"  class="form-control" placeholder="Alamat" name="ALAMAT_USAHA" value="<?php echo $detail->ALAMAT_USAHA; ?>">                
                      </div>
                    </div>  
                  </div> 
                </div>
              </div>
            </div> 
            <div class="col-md-12 col-sm-12">
              <div style="font-size:12px">
                <div class="x_title">
                  <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content" >     
                  <div class="col-md-6">              
                    <!-- <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> No Formulir  <sup>*</sup></label>
                      <div class="col-md-9">
                        <input type="text" name="NO_FORMULIR" id="NO_FORMULIR" class="form-control col-md-7 col-xs-12" value="<?php echo $NO_FORMULIR?>"  <?php echo $disable?> placeholder="No Formulir">
                      </div>
                    </div> -->
                    <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> Tanggal Penerimaan   <sup>*</sup></label>
                      <div class="col-md-9">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar">
                          </i></span>
                          <input type="text" name="TANGGAL_PENERIMAAN" id="TANGGAL_PENERIMAAN" class="form-control col-md-7 col-xs-12 tanggal" value="<?php echo $TANGGAL_PENERIMAAN?>" required <?php echo $disable?> >
                        </div>
                      </div>
                    </div>  
                   
                    <?php
                    $persen = "var persen = new Array();\n";
                    $masa = "var masa = new Array();\n";
                    ?>   
                    <?php if ($disable!='disabled') { ?>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Golongan <sup>*</sup>
                      </label>
                      <div class="col-md-9">
                        <select onchange="changeValue(this.value)" style="font-size:12px" id="GOLONGAN" <?php echo $disable?> name="GOLONGAN_ID"  placeholder="Kecamatan" class="form-control select2 col-md-7 col-xs-12">
                          <option value="">Pilih</option>
                          <?php foreach($GOLONGAN as $GOLONGAN){ ?>
                          <option <?php if($GOLONGAN->ID_OP==$GOLONGAN_ID){echo "selected";}?> value="<?php echo $GOLONGAN->ID_OP?>"><?php echo $GOLONGAN->DESKRIPSI ?></option>
                          <?php 
                          $persen .= "persen['" . $GOLONGAN->ID_OP . "'] = {persen:'".addslashes(number_format($GOLONGAN->PERSEN,'0','','.'))."'};\n";
                          $masa .= "masa['" . $GOLONGAN->ID_OP . "'] = {masa:'".addslashes(number_format($GOLONGAN->MASA,'0','','.'))."'};\n";
                        } ?>
                      </select>
                    </div>
                  </div> 
                    <?php } ?>
                  <script type="text/javascript">  
                    <?php echo $persen;echo $masa;?>             
                    function changeValue(id){
                      document.getElementById('PAJAK').value = persen[id].persen;
                      document.getElementById('MASA').value = masa[id].masa;
                    }
                  </script>    
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan <sup>*</sup>
                    </label>
                    <div class="col-md-3">
                      <select  required style="font-size:12px" id="DASAR_PENGENAAN" <?php echo $disable?> name="DASAR_PENGENAAN"  placeholder="Kelurahan" class="form-control select2 col-md-7 col-xs-12">
                        <option value="">Pilih</option>  
                        <option  <?php if($DASAR_PENGENAAN=='Omzet'){echo "selected";}?> value="Omzet">Omzet</option>
                        <!-- <option  <?php if($DASAR_PENGENAAN=='Ketetapan'){echo "selected";}?> value="Ketetapan">Ketetapan</option> -->
                       <!--    -->
                      </select>
                    </div>
                    <!-- <label class="control-label col-md-3 col-xs-12"> Jenis Entrian </label>
                    <div class="col-md-3">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="1" id="JENIS_ENTRIAN" checked name="JENIS_ENTRIAN" <?php if($JENIS_ENTRIAN=='1'){echo "checked=''";}?> > Entrian Sederhana
                        </label>
                      </div>
                    </div> -->
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                        <div class="col-md-8">
                        <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                        </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                        <div class="col-md-8">
                        <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE1" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                        </div>
                  </div>
                  <div class="form-group">
                      <label class="control-label col-md-3 col-xs-12"> File Transaksi <font color="red" style='font-size: 9px;'>*Max(2MB)</font></label>
                        <div class="col-md-8">
                        <input type="file" accept="image/x-png,image/gif,image/jpeg" name="FILE2" class="form-control col-md-7 col-xs-12" id="my_file_field" onchange="aa(this);">
                        </div>
                  </div>   
                </div>                  
                <div class="col-md-6"> 
                    <input onchange='get(this);' <?php echo $disable ?> type="hidden" id="KAMAR_TERISI" name="KAMAR_TERISI" required="required" placeholder="Kamar Terisi" class="form-control col-md-7 col-xs-12" value="0" >
                    <input <?php echo $disable ?> type="hidden" id="JUMLAH_KAMAR" name="JUMLAH_KAMAR" required="required" placeholder="Jumlah Kamar" class="form-control col-md-7 col-xs-12" value="0">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Omzet <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input onchange='get(this);' <?php echo $disable ?> type="text" id="TARIF_RATA_RATA" name="TARIF_RATA_RATA" required="required" placeholder="Omzet" class="form-control col-md-7 col-xs-12" value="<?php echo $TARIF_RATA_RATA; ?>" >                
                      </div>
                    </div>  
                  </div>  
                  <div class="form-group">
                    <label class="control-label col-md-3 col-xs-12" >Masa Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="MASA" id="MASA" class="form-control col-md-7 col-xs-12" value="<?php echo $MASA; ?>"  <?php echo $disable?> placeholder="Masa Pajak" >
                    </div>
                    <label class="control-label col-md-1 col-xs-12" > Pajak </label>
                    <div class="col-md-4">
                      <input readonly="" type="text" name="PAJAK" id="PAJAK" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK; ?>"  <?php echo $disable?> placeholder="Pajak" onchange='get(this);'>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="DPP" name="DPP" required="required" placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                      </div>
                    </div>  
                  </div> 
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Terutang <sup>*</sup>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="input-group">
                        <span class="input-group-addon">Rp</span>
                        <input <?php echo $disable ?> type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                      </div>
                    </div> 
                  </div> 
                  <?php if($disable==''){?>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                      <button type="submit" class="btn btn-primary" onClick="return validasi()">Submit</button>
                      <a href="<?php echo site_url('Sptpd_hotel/sptpd_hotel') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                  </div>
                  <?php } ?> 
                </div>
              </div>
            </div>     
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



</div>
<script>
function aa(sel){
      var ext = $('#my_file_field').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
          alert('Format File Tidak Didukung!');
          $('#my_file_field').val('');
      }
    }
    var uploadField = document.getElementById("my_file_field");

uploadField.onchange = function() {
    if(this.files[0].size > 2500000){
       alert("File Terlalu Besar!");
       this.value = "";
    };
};
  function get() {
    var TARIF_RATA_RATA= parseFloat(nominalFormat(document.getElementById("TARIF_RATA_RATA").value))||0;
    var KAMAR_TERISI= parseFloat(nominalFormat(document.getElementById("KAMAR_TERISI").value))||0;
    var PAJAK= parseFloat(nominalFormat(document.getElementById("PAJAK").value))/100;
    var dpp=TARIF_RATA_RATA;
    DPP.value=getNominal(dpp)  ;
    //PAJAK_TERUTANG.value=getNominal(dpp*PAJAK);

     PAJAK_TERUTANG.value=getNominal(Math.round(dpp*PAJAK));
    }
    function validasi(){
      var MASA_PAJAK=document.forms["demo-form2"]["MASA_PAJAK"].value;
      var TAHUN_PAJAK=document.forms["demo-form2"]["TAHUN_PAJAK"].value;
      var NPWPD=document.forms["demo-form2"]["NPWPD"].value;
      var PT=document.forms["demo-form2"]["PAJAK_TERUTANG"].value;
      if (MASA_PAJAK==null || MASA_PAJAK==""){
        swal("Masa Pajak Harus di Isi", "", "warning")
        return false;
      };
      if (TAHUN_PAJAK==null || TAHUN_PAJAK==""){
        swal("Tahun Pajak Harus di Isi", "", "warning")
        return false;
      }; 
      if (NPWPD==null || NPWPD==""){
        swal("NPWPD Harus di Isi", "", "warning")
        return false;
      };
       if (PT==null || PT==""){
        swal("PAJAK TERUTANG Tidak boleh kosong", "", "warning")
        return false;
      };
      if (!NPWPD.match(number)){
        swal("NPWPD Harus Angka", "", "warning")
        return false;
      };

    }  

    $("#NPWPD").keyup(function(){
      var npwpd = $("#NPWPD").val();
      
    }); 
    function NamaUsaha(id){
                //alert(id);
                var npwpd=id;
                var jp='01';
                $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
        
        $.ajax({
           type: "POST",
           url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_nama_usaha'?>",
           data: { npwp: npwpd,
                   jp: jp   },
           cache: false,
           success: function(msga){
                  //alert(jp);
                  $("#NAMA_USAHA").html(msga);
                }
              }); 
                
    }     
    /* Tanpa Rupiah */
    var JUMLAH_KAMAR = document.getElementById('JUMLAH_KAMAR');
    JUMLAH_KAMAR.addEventListener('keyup', function(e)
    {
      JUMLAH_KAMAR.value = formatRupiah(this.value);
    });
    /* Tanpa Rupiah */
    var TARIF_RATA_RATA = document.getElementById('TARIF_RATA_RATA');
    TARIF_RATA_RATA.addEventListener('keyup', function(e)
    {
      TARIF_RATA_RATA.value = formatRupiah(this.value);
    });     
    /* Tanpa Rupiah */
    var KAMAR_TERISI = document.getElementById('KAMAR_TERISI');
    KAMAR_TERISI.addEventListener('keyup', function(e)
    {
      KAMAR_TERISI.value = formatRupiah(this.value);
    });   
    /* Fungsi */
    function formatRupiah(angka, prefix)
    {
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa  = split[0].length % 3,
      rupiah  = split[0].substr(0, sisa),
      ribuan  = split[0].substr(sisa).match(/\d{3}/gi);
      
      if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    function ribuan (angka)
    {
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan  = reverse.match(/\d{1,3}/g);
      ribuan  = ribuan.join('.').split('').reverse().join('');
      return ribuan;
    }

    function gantiTitikKoma(angka){
      return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
      return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
        var ribu=nominal.substr(0,indexKoma);
        var koma=nominal.substr(indexKoma,3);
        return ribuan(ribu)+koma;        
      }
    }
    function getkel(sel)
    {
      var KODEKEC = sel.value;
      $.ajax({
       type: "POST",
       url: "<?php echo base_url().'Master/pokok/getkel'?>",
       data: { KODEKEC: KODEKEC},
       cache: false,
       success: function(msga){
            //alert(msga);
            $("#KELURAHAN").html(msga);
          }
        });    
    }
    function alamat(sel)
    {
      if (sel=="") {
        $("#ALAMAT_USAHA").val(""); 
        $("#GOLONGAN").val("");
        $("#MASA").val("");
        $("#PAJAK").val("");
        $("#DESKRIPSI").html("");  
        $("#NAMAKELURAHAN").html("");
        $("#NAMAKEC").html("");    

      } else {
        var nama = sel;
        //alert(nama);
        $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_alamat_usaha'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              //alert(msga);
              $("#ALAMAT_USAHA").val(msga); 
            }
          });
         $.ajax({
         type: "POST",
         url: "<?php echo base_url().'Sptpd_hotel/sptpd_hotel/get_detail_usaha'?>",
         data: { nama: nama},
         cache: false,
         success: function(msga){
              if(msga!=0){
                  var exp = msga.split("|");
                  $("#DESKRIPSI").html(exp[1]);  
                  $("#NAMAKELURAHAN").html(exp[3]);
                  $("#NAMAKEC").html(exp[5]);
                  $("#GOLONGAN").val(exp[0]);
                  changeValue($( "#GOLONGAN option:selected" ).val());
                  $("#V_ID_OP").val(exp[0]);
                  $("#V_KODEKEL").val(exp[2]);
                  $("#V_KODEKEC").val(exp[4]);                  
                  
                 // alert(msga);
                }else{
                  $("#NAMA_WP").val(null);  
                  $("#ALAMAT_WP").val(null); 
                  $("#VNAMA_WP").html("BELUM TERDAFTAR");
                  $("#VALAMAT_WP").html("BELUM TERDAFTAR");
                  
                }
            }
          });   
        } 
    }
  
  </script>

