<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skpdkb extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mskpdkb');
        $this->load->model('Master/Mpokok');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
 }
 function penerbitan_skpdkb(){         
        $th     = urldecode($this->input->get('th', TRUE));    
       // $jp     = urldecode($this->input->get('jp', TRUE));
        $npwpd   = urldecode($this->input->get('npwpd', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'th'       => $th,
            //'jp'       => $jp,
            'npwpd'       => $npwpd
        );
        $this->session->set_userdata($sess);
        if ($npwpd <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/penerbitan_skpdkb?&th='.urlencode($th).'&npwpd='.urlencode($npwpd);
            $config['first_url'] = base_url() . 'Skpdkb/penerbitan_skpdkb?&th='.urlencode($th).'&npwpd='.urlencode($npwpd);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/penerbitan_skpdkb';
            $config['first_url'] = base_url() . 'Skpdkb/penerbitan_skpdkb';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_penerbitan_skpdkb($th,$npwpd);
        $realisasi                 = $this->Mskpdkb->get_limit_data_penerbitan_skpdkb($config['per_page'], $start,$th,$npwpd);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        
        $data = array(
          'title'        => 'Penerbitan SKPDKB ',
            'realisasi'            => $realisasi,
            'npwpd'               => $npwpd,
            //'kec'               => $kec,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
            //'option'           => $wh
        );
       
       $this->template->load('Welcome/halaman','penerbitan_skpdkb',$data);
    }

    function form_penerbitan_skpdkb($npwpd="",$kd_usaha=""){
     $data1=$this->db->query("select jenis_pajak from tempat_usaha where id_inc='$kd_usaha'")->row(); 
     $jp=$data1->JENIS_PAJAK;
     $data['sebab']=$this->db->query("select * from ms_skpdkb_alasan where kode_alasan !='0'")->result();
     $data['detail']=$this->db->query("select * from v_detail_wp_usaha where id_inc='$kd_usaha' and npwpd='$npwpd'")->row();
     $data['mp']=$this->db->query("select * from masa_pajak order by id_inc")->result();
     if ($jp=='01') {
       $this->template->load('Welcome/halaman','penerbitan_skpdkb_hotel',$data);
     } else if ($jp=='02') {
       $this->template->load('Welcome/halaman','penerbitan_skpdkb_resto',$data);
     } else if ($jp=='03') {
       $this->template->load('Welcome/halaman','tes',$data);
     } else if ($jp=='04') {
       $data['reklame']=$this->db->query("select * from v_detail_wp_usaha_rekl_minerba where id_inc='$kd_usaha' and npwpd='$npwpd'")->row();
       $this->template->load('Welcome/halaman','penerbitan_skpdkb_reklame',$data);
     } else if ($jp=='05') {
       $this->template->load('Welcome/halaman','penerbitan_skpdkb_ppj',$data);
     } else if ($jp=='06') {
       $data['galian']=$this->db->query("select * from v_detail_wp_usaha_rekl_minerba where id_inc='$kd_usaha' and npwpd='$npwpd'")->row();
       $this->template->load('Welcome/halaman','penerbitan_skpdkb_minerba',$data);
     } else if ($jp=='07') {
       $this->template->load('Welcome/halaman','penerbitan_skpdkb_parkir',$data);
     } else if ($jp=='08') {
       $this->template->load('Welcome/halaman','penerbitan_skpdkb_air_tanah',$data);
     } else {
       echo 'tempat Usaha Tidak Ditemukan';
     }     
  }
    
     function get_data_kekurangan(){           
            $this->load->view('Skpdkb/ajax_detail_kekurangan');
    }
    public function karcis($x)
    {
         $data['x']=$x;
         $this->load->view('Skpdkb/sptpd_parkir_karcis',$data);
    }
    function get_data_non_karcis(){
    $data = array(
            'button'                => 'Form SPTPD Parkir',
            'action'                => site_url('Sptpd_parkir/create_action')
        );
            $this->load->view('Skpdkb/v_ketetapan_parkir',$data);
    }
     function get_data_perforasi(){
            $nama=$_POST['nama'];
            $exp=explode('|', $nama);
           // echo $exp[1]; 
           error_reporting(E_ALL^(E_NOTICE|E_WARNING)); 
            $data['data']=$this->db->query("SELECT * FROM V_CEK_SPTPD_PERFORASI_PARKIR where ID_TEMPAT_USAHA='$exp[1]'")->result();           
            $this->load->view('Skpdkb/v_tabel',$data);
    } 
    public function create_action_parkir(){
        $OP=$this->input->post('GOLONGAN');
        $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
        $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
        $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
        $this->db->trans_start();
        $kb=$this->Mpokok->getSqIdBiling();
        $kompensasi=$this->input->post('kompensasi');
        $setoran_terutang=$this->input->post('setoran_terutang');
        $lain_lain=$this->input->post('lain_lain');
        $bunga=$this->input->post('bunga');
        $kenaikan=$this->input->post('kenaikan');
            //  insert hibran
            $this->db->set('ID_TEMPAT_USAHA', $exp[1]);
            $this->db->set('DEVICE','web');
            $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
            $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
            $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
            $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
            $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
            $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
            $this->db->set('NAMA_USAHA',$exp[0]);
            $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
            $this->db->set('NPWPD',$this->input->post('NPWPD'));
            $this->db->set('PENERIMAAN_NON_KARCIS',str_replace('.', '', $this->input->post('PENERIMAAN_NON_KARCIS')));
            $this->db->set('DPP',str_replace('.', '', $this->input->post('DPP')));
            $this->db->set('PAJAK_TERUTANG',str_replace('.', '', $this->input->post('PAJAK_TERUTANG')));
            $this->db->set('SSPD',$this->input->post('SSPD'));
            $this->db->set('REKAP_BON',$this->input->post('REKAP_BON'));
            $this->db->set('LAINYA',$this->input->post('LAINYA'));
            $this->db->set('NIP_INSERT',$this->nip);
            $this->db->set('TGL_INSERT',"sysdate",false);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
            $this->db->set('KODE_REK',$rek->REK);
            $this->db->set('STATUS','0');
            $this->db->set('ID_OP',$OP);
            $this->db->set('TANGGAL_PENERIMAAN',"SYSDATE",false);
            $this->db->set('KODEKEL',$this->input->post('KELURAHAN'));
            $this->db->set('KODEKEC',$this->input->post('KECAMATAN'));
            $this->db->set('SKPDKB_STATUS',$this->input->post('sebab'));
            $this->db->set('FILE_TRANSAKSI',substr_replace($img_name_ktp.$img_name_ktp1.$img_name_ktp2,'','-1'));
              if ($this->role=='8') {
                  $this->db->set('NPWP_INSERT',$this->nip);
              } else {
                  $this->db->set('NIP_INSERT',$this->nip);
              }
            //$this->db->insert('SPTPD_PARKIR');
            

            $rk=$this->db->query("SELECT MAX(ID_INC) SPTPD_PARKIR_ID
                                    FROM SPTPD_PARKIR
                                    WHERE NIP_INSERT='".$this->nip."' AND IP_INSERT='".$this->Mpokok->getIP()."'")->row();
            // $cek=count();
            if(!empty($_POST['JENIS_PARKIR'])){
                $count=count($_POST['JENIS_PARKIR']);
                for($i=0;$i<$count;$i++){
                    $this->db->set('SPTPD_PARKIR_ID',$rk->SPTPD_PARKIR_ID);
                    $this->db->set('JENIS_PARKIR',$_POST['JENIS_PARKIR'][$i]);
                    $this->db->set('JUMLAH_LEMBAR',str_replace(',', '.', str_replace('.', '',$_POST['JUMLAH_LEMBAR'][$i])));
                    $this->db->set('NOMINAL',str_replace('.', '', $_POST['NOMINAL'][$i]));
                    $this->db->set('PENERIMAAN',str_replace('.', '', $_POST['PENERIMAAN'][$i]));
                    $this->db->set('CATATAN',str_replace('.', '', $_POST['CATATAN'][$i]));
                    //$this->db->insert('SPTPD_PARKIR_KARCIS');
                }
            }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE){
            // sukses
            $nt="Berhasil di simpan";
        }else{
            // gagal
            $nt="Gagal di simpan";
        }
        $this->db->query("CALL P_SKPDKB_DETAIL('$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||''),'$kompensasi','$setoran_terutang','$lain_lain','$bunga','$kenaikan'");
        $this->session->set_flashdata('message', '<script>
                                              $(window).load(function(){
                                               swal("'.$nt.'", "", "success")
                                              });
                                            </script>');
        
            redirect(site_url('Skpdkb/penerbitan_skpdkb'));
        
    }
    public function create_action_parkir_karcis(){
        $OP=$this->input->post('GOLONGAN');
        $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
        $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
        $kb=$this->Mpokok->getSqIdBiling();
        $this->db->trans_start();
        //  insert hibran
            $pt=str_replace('.', '',$_POST['pajak_terutang']);
            $dpp_k=str_replace('.', '',$_POST['dpp_k']);
        //$this->db->set('KODE_KARCIS', $this->input->post('NPWPD',TRUE));
            $this->db->set('ID_TEMPAT_USAHA', $exp[1]);
            $this->db->set('DEVICE','web');
            $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
            $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
            $this->db->set('MASA_PAJAK',$this->input->post('MASA_PAJAK'));
            $this->db->set('TAHUN_PAJAK',$this->input->post('TAHUN_PAJAK'));
            $this->db->set('NAMA_WP',$this->input->post('NAMA_WP'));
            $this->db->set('ALAMAT_WP',$this->input->post('ALAMAT_WP'));
            $this->db->set('NAMA_USAHA',$exp[0]);
            $this->db->set('ALAMAT_USAHA',$this->input->post('ALAMAT_USAHA'));
            $this->db->set('NPWPD',$this->input->post('NPWPD'));
            $this->db->set('PENERIMAAN_NON_KARCIS',str_replace('.', '', $this->input->post('PENERIMAAN_NON_KARCIS')));
            $this->db->set('DPP',array_sum($dpp_k));
            $this->db->set('PAJAK_TERUTANG',array_sum($pt));
            $this->db->set('SSPD',$this->input->post('SSPD'));
            $this->db->set('REKAP_BON',$this->input->post('REKAP_BON'));
            $this->db->set('LAINYA',$this->input->post('LAINYA'));
            $this->db->set('NIP_INSERT',$this->nip);
            $this->db->set('TGL_INSERT',"sysdate",false);
            $this->db->set('IP_INSERT',$this->Mpokok->getIP());
            $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
            $this->db->set('KODE_REK',$rek->REK);
            $this->db->set('STATUS','0');
            $this->db->set('ID_OP',$OP);
            $this->db->set('TANGGAL_PENERIMAAN',"SYSDATE",false);
            $this->db->set('KODEKEL',$this->input->post('KELURAHAN'));
            $this->db->set('KODEKEC',$this->input->post('KECAMATAN'));
            $this->db->set('FILE_TRANSAKSI',substr_replace($img_name_ktp.$img_name_ktp1.$img_name_ktp2,'','-1'));
              if ($this->role=='8') {
                  $this->db->set('NPWP_INSERT',$this->nip);
              } else {
                  $this->db->set('NIP_INSERT',$this->nip);
              }
            $this->db->insert('SPTPD_PARKIR');
           if(!empty($_POST['id_inc'])){
           $nomor=$this->db->query("SELECT MAX(ID_INC) NEXT_VAL FROM SPTPD_PARKIR")->row();
           $NEXT=$nomor->NEXT_VAL;  
           $jumlah= count($_POST['qtt_terjual']); 
                for($i=0; $i < $jumlah; $i++) {
                    $qtt_terjual=str_replace('.', '',$_POST['qtt_terjual'][$i]);
                    $pajak_terutang=str_replace('.', '',$_POST['pajak_terutang'][$i]);

                    $this->db->query("INSERT into SPTPD_PARKIR_KARCIS (sptpd_parkir_id,jenis_parkir,jumlah_lembar,nominal,penerimaan,persen,tagihan_pajak)
                                         select  '$NEXT',nomor_seri,'$qtt_terjual',nilai_lembar,nilai_lembar*'$qtt_terjual',persen,'$pajak_terutang'
                                         from perforasi
                                    where id_inc='".$_POST['id_inc'][$i]."'");
                
            }               
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE){
            // sukses
            $nt="Berhasil di simpan";
        }else{
            // gagal
            $nt="Gagal di simpan";
        }
        //echo $this->db->last_query();
        $this->session->set_flashdata('message', '<script>
                                              $(window).load(function(){
                                               swal("'.$nt.'", "", "success")
                                              });
                                            </script>');
        
            redirect(site_url('Skpdkb/penerbitan_skpdkb'));
       
    
}

function skpdkb_hotel()
 {      
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_hotel?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($upt);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_hotel?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_hotel';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_hotel';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_hotel($tgl1,$tgl2,$wp);
        $hotel                 = $this->Mskpdkb->get_limit_data_rekap_hotel($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data = array(
          'title'        => 'Laporan Rekap SKPDKB Pajak Hotel',
            'hotel'            => $hotel,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'wp'               => $wp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       $this->template->load('Welcome/halaman','lap_skpdkb_hotel',$data);
    }
    function skpdkb_restoran(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_restoran?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_restoran?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_restoran';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_restoran';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_restoran($tgl1,$tgl2,$wp);
        $restoran                 = $this->Mskpdkb->get_limit_data_rekap_restoran($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data = array(
          'title'        => 'Laporan Rekap SKPDKB Pajak Restoran',
            'restoran'         => $restoran,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'wp'               => $wp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       $this->template->load('Welcome/halaman','lap_skpdkb_restoran',$data);
    }
    function skpdkb_hiburan(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_hiburan?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_hiburan?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_hiburan';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_hiburan';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_hiburan($tgl1,$tgl2,$wp);
        $hiburan                 = $this->Mskpdkb->get_limit_data_rekap_hiburan($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Laporan Rekap SKPDKB Pajak Hiburan',
            'hiburan'          => $hiburan,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'wp'              => $wp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
        );
       $this->template->load('Welcome/halaman','lap_skpdkb_hiburan',$data);
    }
function skpdkb_reklame(){
        $tgl1     = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_reklame?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_reklame?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_reklame';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_reklame';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_reklame($tgl1,$tgl2,$wp);
        $reklame                 = $this->Mskpdkb->get_limit_data_rekap_reklame($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Laporan Rekap SKPDKB Pajak Reklame',
            'reklame'          => $reklame,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'upt'              => $upt,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
           
        );
       $this->template->load('Welcome/halaman','lap_skpdkb_reklame',$data);
    }
    function skpdkb_ppj(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_ppj?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_ppj?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_ppj';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_ppj';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_ppj($tgl1,$tgl2,$wp);
        $ppj                 = $this->Mskpdkb->get_limit_data_rekap_ppj($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Laporan Rekap SKPDKB Pajak PPJ',
            'ppj'              => $ppj,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'wp'               => $wp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       
       $this->template->load('Welcome/halaman','lap_skpdkb_ppj',$data);
    }
    function skpdkb_galian(){
        $tgl1     = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_galian?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_galian?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_galian';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_galian';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_galian($tgl1,$tgl2,$wp);
        $galian                 = $this->Mskpdkb->get_limit_data_rekap_galian($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
       
        $data = array(
          'title'        => 'Laporan Rekap SKPDKB Pajak Galian C',
            'galian'            => $galian,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'wp'              => $wp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       $this->template->load('Welcome/halaman','lap_skpdkb_galian',$data);
    }

function skpdkb_parkir(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp,
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_parkir?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_parkir?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_parkir';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_parkir';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_parkir($tgl1,$tgl2,$wp);
        $parkir                 = $this->Mskpdkb->get_limit_data_rekap_parkir($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
       
        $data = array(
          'title'        => 'Laporan SKPDKB Pajak Parkir',
            'parkir'            => $parkir,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'wp'              => $wp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start,
        );
       
       $this->template->load('Welcome/halaman','lap_skpdkb_parkir',$data);
    }

    function skpdkb_at(){
        $tgl1    = urldecode($this->input->get('tgl1', TRUE));
        $tgl2    = urldecode($this->input->get('tgl2', TRUE));
        $wp     = urldecode($this->input->get('wp', TRUE));
        $start   = intval($this->input->get('start'));
        $sess=array(
            'tgl1'       => $tgl1,
            'tgl2'       => $tgl2,
            'wp'       => $wp
        );
        $this->session->set_userdata($sess);
        if ($tgl1 <> '') {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_at?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($wp);
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_at?tgl1=' . urlencode($tgl1).'&tgl2='.urlencode($tgl2).'&wp='.urlencode($upt);
        } else {
            $config['base_url']  = base_url() . 'Skpdkb/skpdkb_at';
            $config['first_url'] = base_url() . 'Skpdkb/skpdkb_at';
        }

        $config['per_page']          = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mskpdkb->total_rows_rekap_at($tgl1,$tgl2,$wp);
        $at                 = $this->Mskpdkb->get_limit_data_rekap_at($config['per_page'], $start, $tgl1,$tgl2,$wp);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
          'title'        => 'Laporan Rekap SKPDKB Air Tanah',
            'at'            => $at,
            'tgl1'             => $tgl1,
            'tgl2'             => $tgl2,
            'wp'              => $wp,
            'pagination'       => $this->pagination->create_links(),
            'total_rows'       => $config['total_rows'],
            'start'            => $start
        );
       $this->template->load('Welcome/halaman','lap_skpdkb_at',$data);
    }
  
}