<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_hotel extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Msptpd_hotel');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
        $this->role=$this->session->userdata('MS_ROLE_ID');
    }
    public function index(){

        if(isset($_GET['TAHUN_PAJAK']) OR isset($_GET['NPWPD'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $dasar_pengenaan=$_GET['NPWPD'];
        } else {
                $bulan='';//date('d-m-Y');
                $tahun='';//date('Y');//date('d-m-Y');
                $dasar_pengenaan='';
        }
        $sess=array(
                'hotel_bulan'=>$bulan,
                'hotel_tahun'=>$tahun,
                'dasar_pengenaan_hotel'=>$dasar_pengenaan
         );
      $this->session->set_userdata($sess);
      $data['mp']=$this->Mpokok->listMasapajak();
      $this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_list',$data);
  }
  public function json() {
    header('Content-Type: application/json');
    echo $this->Msptpd_hotel->json();
}
public function hapus()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $row = $this->Msptpd_hotel->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_hotel->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Sptpd Hotel Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}
public function table()
{
    $data['mp']=$this->Mpokok->listMasapajak();
    $this->load->view('Sptpd_hotel/sptpd_hotel_table',$data);
}	
public function create() 
{
    
    if ($this->role==8) {
            $data = array(
        'button'                       => 'Form SPTPD Hotel',
        'action'                       => site_url('Sptpd_hotel/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'tu'                           => $this->Msptpd_hotel->getTu(),

    );           
    $this->template->load('Welcome/halaman','Wp/beranda/beranda_form',$data);
    } else {
    $data = array(
        'button'                       => 'Form SPTPD Hotel',
        'action'                       => site_url('Sptpd_hotel/preview'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_KAMAR'                 => set_value('JUMLAH_KAMAR'),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'jenis_hotel'                  => $this->Msptpd_hotel->ListJenisHotel(),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID'),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_hotel->getGol(),
        'NAMA_GOLONGAN'                => '',
        'NAMA_KECAMATAN'               => '',
        'NAMA_KELURAHAN'               => '',

    );
    $data['jenis']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='' and jenis_pajak='02'")->result();           
    $this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_form',$data);
}
}

public function preview() 
{
                $tmp_file=$_FILES['FILE']['tmp_name'];
                $name_file =$_FILES['FILE']['name'];
                if($name_file!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_1'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/hotel/'.str_replace(',', '', $img_name_ktp);
                        move_uploaded_file($tmp_file,$nf);
                        
                    }
                $tmp_file1=$_FILES['FILE1']['tmp_name'];
                $name_file1 =$_FILES['FILE1']['name'];
                if($name_file1!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file1);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp1=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_2'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/hotel/'.str_replace(',', '', $img_name_ktp1);
                        move_uploaded_file($tmp_file1,$nf);
                        
                    }
                $tmp_file2=$_FILES['FILE2']['tmp_name'];
                $name_file2 =$_FILES['FILE2']['name'];
                if($name_file2!=''){
                        $exp=explode('|', set_value('NAMA_USAHA'));
                        //upload file
                        $explode       = explode('.',$name_file2);
                        $extensi       = $explode[count($explode)-1];
                        $img_name_ktp2=set_value('MASA_PAJAK').'_'.set_value('TAHUN_PAJAK').'_'.$exp[1].'_'.set_value('NPWPD').'_3'.'.'.$extensi.',';
                        $nf='upload/file_transaksi/hotel/'.str_replace(',', '', $img_name_ktp2);
                        move_uploaded_file($tmp_file2,$nf);
                        
                    }
    $data = array(
        'button'                       => 'Preview SPTPD Hotel',
        'action'                       => site_url('Sptpd_hotel/create_action'),
        'disable'                      => '',
        'ID_INC'                       => set_value('ID_INC'),
        'MASA_PAJAK'                   => set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'                  => set_value('TAHUN_PAJAK'),
        'NAMA_WP'                      => set_value('NAMA_WP'),
        'ALAMAT_WP'                    => set_value('ALAMAT_WP'),
        'NAMA_USAHA'                   => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'                 => set_value('ALAMAT_USAHA'),
        'NPWPD'                        => set_value('NPWPD'),
        'JUMLAH_KAMAR'                 => set_value('JUMLAH_KAMAR'),
        'TARIF_RATA_RATA'              => set_value('TARIF_RATA_RATA'),
        'KAMAR_TERISI'                 => set_value('KAMAR_TERISI'),
        'DPP'                          => set_value('DPP'),
        'PAJAK_TERUTANG'               => set_value('PAJAK_TERUTANG'),
        'MASA'                         => set_value('MASA'),
        'JENIS_ENTRIAN'                => set_value('JENIS_ENTRIAN'),
        'PAJAK'                        => set_value('PAJAK'),
        'jenis_hotel'                  => $this->Msptpd_hotel->ListJenisHotel(),
        'list_masa_pajak'              => $this->Mpokok->listMasapajak(),
        'KELURAHAN'                    => set_value('KELURAHAN'),
        'KECAMATAN'                    => set_value('KECAMATAN'),
        'GOLONGAN_ID'                  => set_value('GOLONGAN_ID',$this->input->post('GOLONGAN',TRUE)),
        'DASAR_PENGENAAN'              => set_value('DASAR_PENGENAAN'),
        'ID_OP'                        => set_value('ID_OP'),
        'NO_FORMULIR'                  => set_value('NO_FORMULIR'),
        'NIP'                          => set_value('NIP',$this->nip),
        'BERLAKU_MULAI'                => set_value('BERLAKU_MULAI',DATE('d/m/Y')),
        'TANGGAL_PENERIMAAN'           => set_value('TANGGAL_PENERIMAAN',DATE('d/m/Y')),
        'kec'                          => $this->Mpokok->getKec(),
        'GOLONGAN'                     => $this->Msptpd_hotel->getGol(),
        'FILE'                         => $img_name_ktp.$img_name_ktp1.$img_name_ktp2,
        //'kb'                           =>$this->Mpokok->getSqIdBiling(),

    );
    $data['jenis']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='' and jenis_pajak='02'")->result();           
    $this->template->load('Welcome/halaman','sptpd_hotel/sptpd_hotel_preview',$data);
}


public function create_action() 
{
    $m=sprintf("%02d", $this->input->post('MASA_PAJAK',TRUE));
    $BERLAKU_MULAI=date('d/'.$m.'/Y');
    $exp=explode('|', $this->input->post('NAMA_USAHA',TRUE));
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $OP=$this->input->post('GOLONGAN_ID',TRUE);
    $rek=$this->db->query("select  getNoRek('$OP')REK from dual")->row();
    $bank=$this->db->query("select getnorek_bank('$OP')BANK from dual")->row();
    $kb=$this->Mpokok->getSqIdBiling();
    $expired=$this->db->query("select TO_CHAR(sysdate+30, 'yyyymmdd')ed,sysdate+30 now from dual")->row();
    $va=$this->Mpokok->getVA($OP);
    $pt=str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG')));
    $nm=$this->input->post('NAMA_WP',TRUE);
    $ed=$expired->ED;

    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $exp[0]);
    $this->db->set('ID_USAHA', $exp[1]);
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->Mpokok->getNoBerkas());
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "SYSDATE",false);
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN_ID',TRUE));
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('JUMLAH_KAMAR',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_KAMAR'))));
    $this->db->set('TARIF_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF_RATA_RATA'))));
    $this->db->set('KAMAR_TERISI',str_replace(',', '.', str_replace('.', '',$this->input->post('KAMAR_TERISI'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));
    $this->db->set('STATUS', 0);
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('TGL_KETETAPAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('KODE_REK',$rek->REK);
    $this->db->set('DEVICE','web');
    $this->db->set('FILE_TRANSAKSI',$this->input->post('FILE',TRUE));
    //$this->db->set('KODE_PENGESAHAN',KdPengesahan());
    $this->db->set('KODE_BILING', "'$bank->BANK' ||TO_CHAR(SYSDATE, 'yymmdd')||'$kb'",false);
    $this->db->set('NO_VA',$va);
    $this->db->set('ED_VA',$expired->NOW);
if ($this->role=='8') {
    $this->db->set('NPWP_INSERT',$this->nip);
} else {
    $this->db->set('NIP_INSERT',$this->nip);
}

    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->insert('SPTPD_HOTEL');
    $this->Mpokok->registration($va,$nm,$pt,$ed);
    /*$noform=$this->Mpokok->getFormulir();
    $this->db->query("CALL INSERTFORMULIR('$noform','01')");*/
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Tambah ", "", "success")
     });

     </script>');
    if ($this->role=='10') {
        redirect(site_url('Upt/upt'));
    } else {
        redirect(site_url('Sptpd_hotel/sptpd_hotel'));
    }
}
public function delete($id) 
{
    $row = $this->Msptpd_hotel->get_by_id($id);

    if ($row) {
        $this->Msptpd_hotel->delete($id);
        $this->db->query("commit");
        $this->session->set_flashdata('message', '<script>
          $(window).load(function(){
             swal("Berhasil Hapus Supplier", "", "success")
         });

         </script>');
        redirect(site_url('Sptpd/sptpd_hotel'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('Sptpd/sptpd_hotel'));
    }
} 
public function update($id) 
{

    $row = $this->Msptpd_hotel->get_by_id($id);

    if ($row) {
        $data = array(
            'button'              => 'Update SPTPD Hotel',
            'action'              => site_url('Sptpd_hotel/sptpd_hotel/update_action'),
            'disable'             => '',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'DASAR_PENGENAAN'     => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'JUMLAH_KAMAR'        => set_value('JUMLAH_KAMAR',number_format($row->JUMLAH_KAMAR,'0','','.')),
            'TARIF_RATA_RATA'     => set_value('TARIF_RATA_RATA',number_format($row->TARIF_RATA_RATA,'0','','.')),
            'KAMAR_TERISI'        => set_value('KAMAR_TERISI',number_format($row->KAMAR_TERISI,'0','','.')),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'jenis_hotel'         => $this->Msptpd_hotel->ListJenisHotel(),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_hotel->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_hotel->getGol(),

        );
        $data['jenis']=$this->db->query("SELECT nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$row->NPWPD."' and jenis_pajak='02'")->result();  
        $this->template->load('Welcome/halaman','sptpd_hotel/sptpd_hotel_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
} 
public function update_action() 
{
    $BERLAKU_MULAI=$this->input->post('BERLAKU_MULAI',TRUE);
    $TANGGAL_PENERIMAAN=$this->input->post('TANGGAL_PENERIMAAN',TRUE);
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('NO_FORMULIR', $this->input->post('NO_FORMULIR',TRUE));
    $this->db->set('TANGGAL_PENERIMAAN',"to_date('$TANGGAL_PENERIMAAN','dd/mm/yyyy')",false );
    $this->db->set('BERLAKU_MULAI', "to_date('$BERLAKU_MULAI','dd/mm/yyyy')",false );
    $this->db->set('KODEKEC', $this->input->post('KECAMATAN',TRUE));
    $this->db->set('KODEKEL', $this->input->post('KELURAHAN',TRUE));
    $this->db->set('ID_OP', $this->input->post('GOLONGAN',TRUE));
    $this->db->set('DASAR_PENGENAAN', $this->input->post('DASAR_PENGENAAN',TRUE));
    $this->db->set('JENIS_ENTRIAN', $this->input->post('JENIS_ENTRIAN',TRUE));
    $this->db->set('JUMLAH_KAMAR',str_replace(',', '.', str_replace('.', '',$this->input->post('JUMLAH_KAMAR'))));
    $this->db->set('TARIF_RATA_RATA',str_replace(',', '.', str_replace('.', '',$this->input->post('TARIF_RATA_RATA'))));
    $this->db->set('KAMAR_TERISI',str_replace(',', '.', str_replace('.', '',$this->input->post('KAMAR_TERISI'))));
    $this->db->set('MASA', $this->input->post('MASA',TRUE));
    $this->db->set('PAJAK', $this->input->post('PAJAK',TRUE));
    $this->db->set('DPP',str_replace(',', '.', str_replace('.', '',$this->input->post('DPP'))));
    $this->db->set('PAJAK_TERUTANG',str_replace(',', '.', str_replace('.', '',$this->input->post('PAJAK_TERUTANG'))));

    $this->db->where('ID_INC',$this->input->post('ID_INC', TRUE));
    $this->db->update('SPTPD_HOTEL');
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
         swal("Berhasil Update SPTPD Hotel", "", "success")
     });

     </script>');
    redirect(site_url('Sptpd_hotel/sptpd_hotel'));
}   
function detail($id){
    $id=rapikan($id);
    $row = $this->Msptpd_hotel->get_by_id($id);
    if ($row) {
        $NAMA_GOLONGAN = $this->Mpokok->getNamaGolongan($row->ID_OP);
        $NAMA_KECAMATAN = $this->Mpokok->getNamaKec($row->KODEKEC);
        $NAMA_KELURAHAN = $this->Mpokok->getNamaKel($row->KODEKEL,$row->KODEKEC);
        $data = array(
           'button'                => 'Detail SPTPD Hotel',
           'action'                => site_url('Sptpdhiburan/hiburan/update_action'),
           'mp'                    => $this->Mpokok->listMasapajak(),
           'disable'               => 'disabled',
            'ID_INC'              => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK'          => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK'         => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NPWPD'               => set_value('NPWPD',$row->NPWPD),
            'NAMA_WP'             => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP'           => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA'          => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA'        => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NO_FORMULIR'         => set_value('NO_FORMULIR',$row->NO_FORMULIR),
            'TANGGAL_PENERIMAAN'  => set_value('TANGGAL_PENERIMAAN',$row->TANGGAL_PENERIMAAN),
            'BERLAKU_MULAI'       => set_value('BERLAKU_MULAI',$row->BERLAKU_MULAI),
            'KECAMATAN'           => set_value('KECAMATAN',$row->KODEKEC),
            'KELURAHAN'           => set_value('KELURAHAN',$row->KODEKEL),
            'GOLONGAN_ID'         => set_value('GOLONGAN_ID',$row->ID_OP),
            'DASAR_PENGENAAN'     => set_value('DASAR_PENGENAAN',$row->DASAR_PENGENAAN),
            'JENIS_ENTRIAN'       => set_value('JENIS_ENTRIAN',$row->JENIS_ENTRIAN),
            'JUMLAH_KAMAR'        => set_value('JUMLAH_KAMAR',number_format($row->JUMLAH_KAMAR,'0','','.')),
            'TARIF_RATA_RATA'     => set_value('TARIF_RATA_RATA',number_format($row->TARIF_RATA_RATA,'0','','.')),
            'KAMAR_TERISI'        => set_value('KAMAR_TERISI',number_format($row->KAMAR_TERISI,'0','','.')),
            'MASA'                => set_value('MASA',$row->MASA),
            'PAJAK'               => set_value('PAJAK',$row->PAJAK),
            'DPP'                 => set_value('DPP',number_format($row->DPP,'0','','.')),
            'PAJAK_TERUTANG'      => set_value('PAJAK_TERUTANG',number_format($row->PAJAK_TERUTANG,'0','','.')),
            'jenis_hotel'         => $this->Msptpd_hotel->ListJenisHotel(),
            'list_masa_pajak'     => $this->Mpokok->listMasapajak(),
            'kel'                 => $this->Msptpd_hotel->getkel($row->KODEKEC),
            'kec'                 => $this->Mpokok->getKec(),
            'GOLONGAN'            => $this->Msptpd_hotel->getGol(),
            'NAMA_GOLONGAN'       => $NAMA_GOLONGAN->DESKRIPSI,
            'NAMA_KECAMATAN'      => $NAMA_KECAMATAN->NAMAKEC,
            'NAMA_KELURAHAN'      => $NAMA_KELURAHAN->NAMAKELURAHAN,
       );

        $this->template->load('Welcome/halaman','sptpd_hotel_form',$data);

    } else {

        redirect(site_url('Sptpd_hotel/sptpd_hotel'));
    }
}   
function pdf_kode_biling_hotel($kode_biling=""){
            $kode_biling=rapikan($kode_biling);
            $data['wp']=$this->db->query("select * from V_GET_TAGIHAN where kode_biling='$kode_biling'")->row();
            $html     =$this->load->view('Sptpd_hotel/sptpd_hotel_pdf',$data,true);
            
           $filename = time().".pdf";
            $this->load->library('PdfGenerator');
            $this->pdfgenerator->generate($html);
    }
function get_nama_usaha(){
        $data1=$_POST['npwp'];
        $jp=$_POST['jp'];
            //$data['parent']=$parent;
        $data['kc']=$this->db->query("SELECT id_inc,nama_usaha,jenis_pajak from tempat_usaha where npwpd='".$data1."' and jenis_pajak='$jp'")->result();
            // print_r($data);
        //echo "string";
        $this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
function get_alamat_usaha(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("SELECT nama_usaha,ALAMAT_USAHA from tempat_usaha where id_inc='$exp[1]' ")->row();
            // print_r($data);
        echo $data->ALAMAT_USAHA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }
    function get_detail_usaha(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("select a.id_op||'|'||deskripsi||'|'||a.kodekel||'|'||D.NAMAKELURAHAN||'|'||c.kodekec||'|'||C.NAMAKEC data from tempat_usaha a
                                     join objek_pajak b on a.id_op=b.id_op
                                     join kecamatan c on c.kodekec=A.KODEKEC
                                     join kelurahan d on D.KODEKELURAHAN=A.KODEKEL
                                           and D.KODEKEC=A.KODEKEC
                                     where a.id_inc='$exp[1]'")->row();
            // print_r($data);
        echo $data->DATA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    } 
    function get_detail_usaha_reklame(){
        $data1=$_POST['nama'];
        $exp=explode('|', $data1);  
            //$data['parent']=$parent;
        $data=$this->db->query("select a.kodekel||'|'||D.NAMAKELURAHAN||'|'||c.kodekec||'|'||C.NAMAKEC data from tempat_usaha a
                                     join kecamatan c on c.kodekec=A.KODEKEC
                                     join kelurahan d on D.KODEKELURAHAN=A.KODEKEL
                                           and D.KODEKEC=A.KODEKEC
                                     where a.id_inc='$exp[1]'")->row();
            // print_r($data);
        echo $data->DATA;
        //$this->load->view('Sptpd_hotel/get_nama_usaha',$data);
    }       
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */