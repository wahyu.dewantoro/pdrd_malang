<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sptpd_hotel extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Msptpd_hotel');
        $this->load->model('Master/Mpokok');
        $this->load->library('form_validation');        
        $this->load->library('datatables');
        $this->nip=$this->session->userdata('NIP');
    }
    public function index()
    {
      if(isset($_GET['MASA_PAJAK']) && isset($_GET['TAHUN_PAJAK']) OR isset($_GET['DASAR_PENGENAAN'])){
                $bulan=$_GET['MASA_PAJAK'];
                $tahun=$_GET['TAHUN_PAJAK'];
                $c_pengambilan=$_GET['DASAR_PENGENAAN'];
        } else {
                $bulan='';//date('d-m-Y');
                $tahun='';//date('d-m-Y');
                $dasar_pengenaan='';
        }
        $sess=array(
                'hotel_bulan'=>$bulan,
                'hotel_tahun'=>$tahun,
                'dasar_pengenaan_hotel'=>$dasar_pengenaan
         );
      $this->session->set_userdata($sess);
      //$data1['mp']=$this->Mpokok->listMasapajak();
      $this->template->load('Welcome/halaman','Sptpd_hotel/sptpd_hotel_list');
  }
  public function json() {
    header('Content-Type: application/json');
    echo $this->Msptpd_hotel->json();
}
public function hapus()
{
    $response = array();
    
    if ($_POST['delete']) {


        $id = $_POST['delete'];
        $row = $this->Msptpd_hotel->get_by_id($id);
        
        if ($row) {
            $this->Msptpd_hotel->delete($id);
            $response['status']  = 'success';
            $response['message'] = 'Data Sptpd Hotel Sudah Dihapus ...';
        } else {
            $response['status']  = 'error';
            $response['message'] = 'Unable to delete product ...';
        }
        echo json_encode($response);
    }
}
public function table()
{
    $this->load->view('Sptpd_hotel/sptpd_hotel_table');
}	
public function create() 
{
    $data = array(
        'button'     => 'Form SPTPD Hotel',
        'action'     => site_url('Sptpd_hotel/create_action'),
        'ID_INC' => set_value('ID_INC'),
        'MASA_PAJAK'=> set_value('MASA_PAJAK'),
        'TAHUN_PAJAK'=> set_value('TAHUN_PAJAK'),
        'NAMA_WP'=> set_value('NAMA_WP'),
        'ALAMAT_WP'=> set_value('ALAMAT_WP'),
        'NAMA_USAHA' => set_value('NAMA_USAHA'),
        'ALAMAT_USAHA'=> set_value('ALAMAT_USAHA'),
        'NPWPD' => set_value('NPWPD'),
        'JENIS_HOTEL_ID'=> set_value('JENIS_HOTEL_ID'),
        'PENERIMAAN_KAMAR'=> set_value('PENERIMAAN_KAMAR'),
        'PENERIMAAN_FASILITAS'=> set_value('PENERIMAAN_FASILITAS'),
        'DPP'=> set_value('DPP'),
        'PAJAK_TERUTANG'=> set_value('PAJAK_TERUTANG'),
        'KURANG_BAYAR'=> set_value('KURANG_BAYAR'),
        'SANKSI_ADMINISTRASI'=> set_value('SANKSI_ADMINISTRASI'),
        'JUMLAH_PAJAK_TERUTANG'=> set_value('JUMLAH_PAJAK_TERUTANG'),
        'SSPD'=> set_value('SSPD'),
        'REKAP_OMSET'=> set_value('REKAP_OMSET'),
        'REKAP_BON'=> set_value('REKAP_BON'),
        'LAINYA'=> set_value('LAINYA'),
        'jenis_hotel'=>$this->Msptpd_hotel->ListJenisHotel(),
        'list_masa_pajak'=>$this->Mpokok->listMasapajak()

    );           
    $this->template->load('Welcome/halaman','sptpd_hotel/sptpd_hotel_form',$data);
}
public function create_action() 
{
    $this->db->set('MASA_PAJAK', $this->input->post('MASA_PAJAK',TRUE));
    $this->db->set('TAHUN_PAJAK', $this->input->post('TAHUN_PAJAK',TRUE));
    $this->db->set('NAMA_WP', $this->input->post('NAMA_WP',TRUE));
    $this->db->set('ALAMAT_WP', $this->input->post('ALAMAT_WP',TRUE));
    $this->db->set('NAMA_USAHA', $this->input->post('NAMA_USAHA',TRUE));
    $this->db->set('ALAMAT_USAHA', $this->input->post('ALAMAT_USAHA',TRUE));
    $this->db->set('NPWPD', $this->input->post('NPWPD',TRUE));
    $this->db->set('JENIS_HOTEL_ID', $this->input->post('JENIS_HOTEL_ID',TRUE));
    $this->db->set('PENERIMAAN_KAMAR', $this->input->post('PENERIMAAN_KAMAR',TRUE));
    $this->db->set('PENERIMAAN_FASILITAS', $this->input->post('PENERIMAAN_FASILITAS',TRUE));
    $this->db->set('DPP', $this->input->post('DPP',TRUE));
    $this->db->set('PAJAK_TERUTANG', $this->input->post('PAJAK_TERUTANG',TRUE));
    $this->db->set('KURANG_BAYAR', $this->input->post('KURANG_BAYAR',TRUE));
    $this->db->set('SANKSI_ADMINISTRASI', $this->input->post('SANKSI_ADMINISTRASI',TRUE));
    $this->db->set('JUMLAH_PAJAK_TERUTANG', $this->input->post('JUMLAH_PAJAK_TERUTANG',TRUE));
    $this->db->set('SSPD', $this->input->post('SSPD',TRUE));
    $this->db->set('REKAP_OMSET', $this->input->post('REKAP_OMSET',TRUE));
    $this->db->set('REKAP_BON', $this->input->post('REKAP_BON',TRUE));
    $this->db->set('LAINYA', $this->input->post('LAINYA',TRUE));
    $this->db->set('IP_INSERT',$this->Mpokok->getIP());
    $this->db->set('NIP_INSERT',$this->nip);
    $this->db->set('TGL_INSERT',"SYSDATE",false);
    $this->db->insert('SPTPD_HOTEL'); 
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
       swal("Berhasil Tambah SPTPD Hotel", "", "success")
   });

   </script>');
    redirect(site_url('Sptpd_hotel/sptpd_hotel'));
}
public function delete($id) 
{
    $row = $this->Msptpd_hotel->get_by_id($id);

    if ($row) {
        $this->Msptpd_hotel->delete($id);
        $this->db->query("commit");
        $this->session->set_flashdata('message', '<script>
          $(window).load(function(){
           swal("Berhasil Hapus Supplier", "", "success")
       });

       </script>');
        redirect(site_url('Sptpd/sptpd_hotel'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('Sptpd/sptpd_hotel'));
    }
} 
public function update($id) 
{

    $row = $this->Msptpd_hotel->get_by_id($id);

    if ($row) {
        $data = array(
            'button'     => 'Update SPTPD Hotel',
            'action'     => site_url('Sptpd_hotel/sptpd_hotel/update_action'),
            'ID_INC' => set_value('ID_INC',$row->ID_INC),
            'MASA_PAJAK' => set_value('MASA_PAJAK',$row->MASA_PAJAK),
            'TAHUN_PAJAK' => set_value('TAHUN_PAJAK',$row->TAHUN_PAJAK),
            'NAMA_WP' => set_value('NAMA_WP',$row->NAMA_WP),
            'ALAMAT_WP' => set_value('ALAMAT_WP',$row->ALAMAT_WP),
            'NAMA_USAHA' => set_value('NAMA_USAHA',$row->NAMA_USAHA),
            'ALAMAT_USAHA' => set_value('ALAMAT_USAHA',$row->ALAMAT_USAHA),
            'NPWPD' => set_value('NPWPD',$row->NPWPD),
            'JENIS_HOTEL_ID' => set_value('JENIS_HOTEL_ID',$row->JENIS_HOTEL_ID),
            'PENERIMAAN_KAMAR' => set_value('PENERIMAAN_KAMAR',$row->PENERIMAAN_KAMAR),
            'PENERIMAAN_FASILITAS' => set_value('PENERIMAAN_FASILITAS',$row->PENERIMAAN_FASILITAS),
            'DPP' => set_value('DPP',$row->DPP),
            'PAJAK_TERUTANG' => set_value('PAJAK_TERUTANG',$row->PAJAK_TERUTANG),
            'KURANG_BAYAR' => set_value('KURANG_BAYAR',$row->KURANG_BAYAR),
            'SANKSI_ADMINISTRASI' => set_value('SANKSI_ADMINISTRASI',$row->SANKSI_ADMINISTRASI),
            'JUMLAH_PAJAK_TERUTANG' => set_value('JUMLAH_PAJAK_TERUTANG',$row->JUMLAH_PAJAK_TERUTANG),
            'SSPD' => set_value('SSPD',$row->SSPD),
            'REKAP_OMSET' => set_value('REKAP_OMSET',$row->REKAP_OMSET),
            'REKAP_BON' => set_value('REKAP_BON',$row->REKAP_BON),
            'LAINYA' => set_value('LAINYA',$row->LAINYA),
            'jenis_hotel'=>$this->Msptpd_hotel->ListJenisHotel(),
            'list_masa_pajak'=>$this->Mpokok->listMasapajak()
        );
        $this->template->load('Welcome/halaman','sptpd_hotel/sptpd_hotel_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('setting/group'));
    }
} 
public function update_action() 
{
    $data = array(
        'MASA_PAJAK' => $this->input->post('MASA_PAJAK',TRUE),
        'TAHUN_PAJAK' => $this->input->post('TAHUN_PAJAK',TRUE),
        'NAMA_WP' => $this->input->post('NAMA_WP',TRUE),
        'ALAMAT_WP' => $this->input->post('ALAMAT_WP',TRUE),
        'NAMA_USAHA' => $this->input->post('NAMA_USAHA',TRUE),
        'ALAMAT_USAHA' => $this->input->post('ALAMAT_USAHA',TRUE),
        'NPWPD' => $this->input->post('NPWPD',TRUE),
        'JENIS_HOTEL_ID' => $this->input->post('JENIS_HOTEL_ID',TRUE),
        'PENERIMAAN_KAMAR' => $this->input->post('PENERIMAAN_KAMAR',TRUE),
        'PENERIMAAN_FASILITAS' => $this->input->post('PENERIMAAN_FASILITAS',TRUE),
        'DPP' => $this->input->post('DPP',TRUE),
        'PAJAK_TERUTANG' => $this->input->post('PAJAK_TERUTANG',TRUE),
        'KURANG_BAYAR' => $this->input->post('KURANG_BAYAR',TRUE),
        'SANKSI_ADMINISTRASI' => $this->input->post('SANKSI_ADMINISTRASI',TRUE),
        'JUMLAH_PAJAK_TERUTANG' => $this->input->post('JUMLAH_PAJAK_TERUTANG',TRUE),
        'SSPD' => $this->input->post('SSPD',TRUE),
        'REKAP_OMSET' => $this->input->post('REKAP_OMSET',TRUE),
        'REKAP_BON' => $this->input->post('REKAP_BON',TRUE),
        'LAINYA' => $this->input->post('LAINYA',TRUE),
    );

    $this->Msptpd_hotel->update($this->input->post('ID_INC', TRUE), $data);
    $this->db->query("commit");
    $this->session->set_flashdata('message', '<script>
      $(window).load(function(){
       swal("Berhasil Update SPTPD Hotel", "", "success")
   });

   </script>');
    redirect(site_url('Sptpd_hotel/sptpd_hotel'));
}          
}

/* End of file Sptpd.php */
/* Location: ./application/modules/Sptpd/controllers/Sptpd.php */