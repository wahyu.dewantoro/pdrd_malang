
    <div class="page-title">
     <div class="title_left">
      <h3><?php echo $button; ?></h3>
    </div>
    <div class="pull-right">
      <?php echo anchor('Sptpd_hotel/sptpd_hotel','<i class="fa fa-angle-double-left"></i> Kembali','class="btn btn-sm btn-primary"')?>
    </div>
  </div> 

  <div class="clearfix"></div>                  
  <div class="row">

    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >    
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">

          <div class="x_content" >
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <input type="hidden" name="ID_INC" value="<?php echo $ID_INC; ?>" /> 

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Masa Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="select2_single form-control" name="MASA_PAJAK" id="MASA_PAJAK">
                          <option></option>  
                          <?php  foreach ($list_masa_pajak as $list_masa_pajak) {?>
                          <option <?php if($MASA_PAJAK==$list_masa_pajak){echo "selected";}?> value="<?php echo $list_masa_pajak?>"><?php echo  $list_masa_pajak?></option>
                          <?php }?>                                                                
                        </select>                
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Tahun <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Tahun" class="form-control col-md-7 col-xs-12" value="<?php if(!empty($TAHUN_PAJAK)){echo $TAHUN_PAJAK; } else { echo date("Y");}?>">                
                      </div>
                    </div>                              
                    <div class="form-group">
                      <label class="control-label  col-md-3 col-sm-3 col-xs-12" for="last-name" >NPWPD <sup>*</sup> 
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="NPWPD" name="NPWPD" required="required" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?php echo $NPWPD; ?>">                  
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Nama Wajib Pajak<sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_WP" name="NAMA_WP" required="required" placeholder="Nama Wajib Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_WP; ?>">                
                      </div>
                    </div> 

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Wajib Pajak<sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="ALAMAT_WP" rows="3" class="resizable_textarea form-control" placeholder="Alamat Wajib Pajak" name="ALAMAT_WP"><?php echo $ALAMAT_WP; ?></textarea>                
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" > Nama Tempat Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="NAMA_USAHA" name="NAMA_USAHA" required="required" placeholder="Nama Tempat Usaha" class="form-control col-md-7 col-xs-12" value="<?php echo $NAMA_USAHA; ?>">               
                      </div>
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" > Alamat  Usaha<sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="ALAMAT_USAHA" rows="3" class="resizable_textarea form-control" placeholder="Alamat Usaha" name="ALAMAT_USAHA"><?php echo $ALAMAT_USAHA; ?></textarea>                
                      </div>
                    </div>   
                  </div>
                </div>
              </div> 
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-ship"></i>  Diisi Oleh Wajib Pajak</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >                         
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="" >Klasifikasi Usaha <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">   
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <?php foreach ($jenis_hotel as $jenis_hotel) { ?>


                          <div class="radio">
                            <input <?php if($JENIS_HOTEL_ID==$jenis_hotel->ID_INC){echo "checked='checked'";}?> type="radio" class="flat" value="<?php echo $jenis_hotel->ID_INC ?>" name="JENIS_HOTEL_ID"> <?php echo $jenis_hotel->HOTEL ?>
                          </div>
                          <?php } ?>                               
                        </div>
                      </div> 
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Penerimaan Kamar <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="PENERIMAAN_KAMAR" name="PENERIMAAN_KAMAR" required="required" placeholder="Jumlah Penerimaan Kamar" class="form-control col-md-7 col-xs-12" value="<?php echo $PENERIMAAN_KAMAR; ?>" onchange='get(this);'>                
                        </div>
                      </div>
                    </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Penerimaa Fasilitas Hotel <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="PENERIMAAN_FASILITAS" name="PENERIMAAN_FASILITAS" required="required" placeholder="Jumlah Penerimaa Fasilitas Hotel" class="form-control col-md-7 col-xs-12" value="<?php echo $PENERIMAAN_FASILITAS; ?>" onchange='get(this);'>                
                        </div>
                      </div>  
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Dasar Pengenaan Pajak <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="DPP" name="DPP" required="required" readonly placeholder="Dasar Pengenaan Pajak" class="form-control col-md-7 col-xs-12" value="<?php echo $DPP; ?>">                
                        </div>
                      </div>  
                    </div> 
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Pajak Yang Terutang <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="PAJAK_TERUTANG" readonly name="PAJAK_TERUTANG" required="required" placeholder="Pajak Yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $PAJAK_TERUTANG; ?>">                
                        </div>
                      </div> 
                    </div>  
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Kurang Bayar / Lebih bayar Masa Pajak Sebelumnya <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="KURANG_BAYAR" name="KURANG_BAYAR" required="required" placeholder="Kurang Bayar / Lebih bayar Masa Pajak Sebelumnya" class="form-control col-md-7 col-xs-12" value="<?php echo $KURANG_BAYAR; ?>" onchange='getkb(this);'>                
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Sanksi Administrasi <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="SANKSI_ADMINISTRASI" name="SANKSI_ADMINISTRASI" required="required" placeholder="Sanksi Administrasi" class="form-control col-md-7 col-xs-12" value="<?php echo $SANKSI_ADMINISTRASI; ?>" onchange='getsa(this);'>                
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Jumlah Pajak yang Terutang <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="input-group">
                          <span class="input-group-addon">Rp</span>
                          <input type="text" id="JUMLAH_PAJAK_TERUTANG" readonly="" name="JUMLAH_PAJAK_TERUTANG" required="required" placeholder="Jumlah Pajak yang Terutang" class="form-control col-md-7 col-xs-12" value="<?php echo $JUMLAH_PAJAK_TERUTANG; ?>">                
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>                           
              <div class="col-md-12 col-sm-12">
                <div style="font-size:12px">
                  <div class="x_title">
                    <h4><i class="fa fa-paperclip"></i> Data Pendukung Dilampirkan</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Surat Setoran Pajak Daerah <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="SSPD" name="SSPD" required="required" placeholder="Surat Setoran Pajak Daerah" class="form-control col-md-7 col-xs-12" value="<?php echo $SSPD; ?>">                
                      </div>
                    </div>   
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Rekapitulasi Penjualan /Omset <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="REKAP_OMSET" name="REKAP_OMSET" required="required" placeholder="Rekapitulasi Penjualan /Omzet" class="form-control col-md-7 col-xs-12" value="<?php echo $REKAP_OMSET; ?>">                
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Rekapitulasi Penggunaan Bon / Bill <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="REKAP_BON" name="REKAP_BON" required="required" placeholder="Rekapitulasi Penggunaan Bon / Bill" class="form-control col-md-7 col-xs-12" value="<?php echo $REKAP_BON; ?>">                
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name" >Lainnya <sup>*</sup>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="LAINYA" name="LAINYA" required="required" placeholder="Lainnya" class="form-control col-md-7 col-xs-12" value="<?php echo $LAINYA; ?>">                
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-primary" onClick="return validasi()">Simpan</button>
                        <a href="<?php echo site_url('Sptpd_hotel/sptpd_hotel') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>



  </div>
  <script>
    function get() {
      var pk= parseInt(document.getElementById("PENERIMAAN_KAMAR").value.replace(/\./g,''))|| 1;
      var pf= parseInt(document.getElementById("PENERIMAAN_FASILITAS").value.replace(/\./g,''))|| 1;
      var kb= parseInt(document.getElementById("KURANG_BAYAR").value.replace(/\./g,''))|| 0;
      var sa= parseInt(document.getElementById("SANKSI_ADMINISTRASI").value.replace(/\./g,''))|| 0;
      var dpp=(pk*pf).toFixed(2);
      var pt=(dpp*0.1).toFixed(2);
      var pjt=((dpp*0.1)+kb+sa).toFixed(2);
      DPP.value = getNominal(dpp);
      PAJAK_TERUTANG.value = getNominal(pt);
      JUMLAH_PAJAK_TERUTANG.value= getNominal(pjt);
    }
    function gantiTitikKoma(angka){
        return angka.toString().replace(/\./g,',');
    }
    function nominalFormat(angka){
        return angka.toString().replace(/\./g,'').replace(/\,/g,'.');
    }
    function getNominal(angka){
      var nominal=gantiTitikKoma(angka);
      var indexKoma=nominal.indexOf(',');
      if (indexKoma==-1) {
        return ribuan(angka);
      } else {
      var ribu=nominal.substr(0,indexKoma);
      var koma=nominal.substr(indexKoma,3);
      return ribuan(ribu)+koma;        
      }
    }
    function getkb() {
      var pt=parseFloat(nominalFormat(document.getElementById("PAJAK_TERUTANG").value))|| 0;
      var kb=parseFloat(nominalFormat(document.getElementById("KURANG_BAYAR").value))|| 0;
      var sa=parseFloat(nominalFormat(document.getElementById("SANKSI_ADMINISTRASI").value))|| 0;
      var jpt=(pt+kb+sa).toFixed(2);
      JUMLAH_PAJAK_TERUTANG.value= getNominal(jpt);
    }
    function getsa() {
      var pt=parseFloat(nominalFormat(document.getElementById("PAJAK_TERUTANG").value))|| 0;
      var kb=parseFloat(nominalFormat(document.getElementById("KURANG_BAYAR").value))|| 0;
      var sa=parseFloat(nominalFormat(document.getElementById("SANKSI_ADMINISTRASI").value))|| 0;
      var jpt=(pt+kb+sa).toFixed(2);
      JUMLAH_PAJAK_TERUTANG.value= getNominal(jpt);
    }
    function validasi(){
      var MASA_PAJAK=document.forms["demo-form2"]["MASA_PAJAK"].value;
      var TAHUN_PAJAK=document.forms["demo-form2"]["TAHUN_PAJAK"].value;
      var NPWPD=document.forms["demo-form2"]["NPWPD"].value;
      var NAMA_WP=document.forms["demo-form2"]["NAMA_WP"].value;
      var ALAMAT_WP=document.forms["demo-form2"]["ALAMAT_WP"].value;
      var NAMA_USAHA=document.forms["demo-form2"]["NAMA_USAHA"].value;
      var ALAMAT_USAHA=document.forms["demo-form2"]["ALAMAT_USAHA"].value;
      var JENIS_HOTEL_ID=document.forms["demo-form2"]["JENIS_HOTEL_ID"].value;
      var PENERIMAAN_KAMAR=document.forms["demo-form2"]["PENERIMAAN_KAMAR"].value;
      var PENERIMAAN_FASILITAS=document.forms["demo-form2"]["PENERIMAAN_FASILITAS"].value;
      var DPP=document.forms["demo-form2"]["DPP"].value;
      var PAJAK_TERUTANG=document.forms["demo-form2"]["PAJAK_TERUTANG"].value;
      var KURANG_BAYAR=document.forms["demo-form2"]["KURANG_BAYAR"].value;
      var SANKSI_ADMINISTRASI=document.forms["demo-form2"]["SANKSI_ADMINISTRASI"].value;
      var JUMLAH_PAJAK_TERUTANG=document.forms["demo-form2"]["JUMLAH_PAJAK_TERUTANG"].value;
      var SSPD=document.forms["demo-form2"]["SSPD"].value;
      var REKAP_OMSET=document.forms["demo-form2"]["REKAP_OMSET"].value;
      var REKAP_BON=document.forms["demo-form2"]["REKAP_BON"].value;
      var LAINYA=document.forms["demo-form2"]["LAINYA"].value;
      var number=/^[0-9]+$/;     
      if (MASA_PAJAK==null || MASA_PAJAK==""){
        swal("Masa Pajak Harus di Isi", "", "warning")
        return false;
      };
      if (TAHUN_PAJAK==null || TAHUN_PAJAK==""){
        swal("Tahun Pajak Harus di Isi", "", "warning")
        return false;
      }; 
      if (NPWPD==null || NPWPD==""){
        swal("NPWPD Harus di Isi", "", "warning")
        return false;
      };
      if (!NPWPD.match(number)){
        swal("NPWPD Harus Angka", "", "warning")
        return false;
      };
      if (NAMA_WP==null || NAMA_WP==""){
        swal("Nama Wajib Pajak Harus di Isi", "", "warning")
        return false;
      };
      if (ALAMAT_WP==null || ALAMAT_WP==""){
        swal("Alamat Wajib Pajak Harus di Isi", "", "warning")
        return false;
      };

      if (NAMA_USAHA==null || NAMA_USAHA==""){
        swal("Nama Tempat Usaha Harus di Isi", "", "warning")
        return false;
      };
      if (ALAMAT_USAHA==null || ALAMAT_USAHA==""){
        swal("Alamat Usaha Harus di Isi", "", "warning")
        return false;
      };
      if (JENIS_HOTEL_ID==null || JENIS_HOTEL_ID==""){
        swal("Klasifikasi Usaha  Harus di Isi", "", "warning")
        return false;
      };
      if (PENERIMAAN_KAMAR==null || PENERIMAAN_KAMAR==""){
        swal("Jumlah Penerimaan Kamar Harus di Isi", "", "warning")
        return false;
      };
      if (PENERIMAAN_FASILITAS==null || PENERIMAAN_FASILITAS==""){
        swal("Jumlah Penerimaa Fasilitas Hotel Harus di Isi", "", "warning")
        return false;
      };
      if (DPP==null || DPP==""){
        swal("Dasar Pengenaan Pajak Harus di Isi", "", "warning")
        return false;
      };
      if (PAJAK_TERUTANG==null || PAJAK_TERUTANG==""){
        swal("Pajak Yang Terutang Harus di Isi", "", "warning")
        return false;
      };
      if (KURANG_BAYAR==null || KURANG_BAYAR==""){
        swal("Kurang Bayar / Lebih bayar Masa Pajak Sebelumnya  Harus di Isi", "", "warning")
        return false;
      };
      if (SANKSI_ADMINISTRASI==null || SANKSI_ADMINISTRASI==""){
        swal("Sanksi Administrasi  Harus di Isi", "", "warning")
        return false;
      };
      if (JUMLAH_PAJAK_TERUTANG==null || JUMLAH_PAJAK_TERUTANG==""){
        swal("Jumlah Pajak yang Terutang Harus di Isi", "", "warning")
        return false;
      };
      if (SSPD==null || SSPD==""){
        swal("Surat Setoran Pajak Daerah Harus di Isi", "", "warning")
        return false;
      };
      if (REKAP_OMSET==null || REKAP_OMSET==""){
        swal("Rekapitulasi Penjualan /Omset Harus di Isi", "", "warning")
        return false;
      };
      if (REKAP_BON==null || REKAP_BON==""){
        swal("Rekapitulasi Penggunaan Bon / Bill Harus di Isi", "", "warning")
        return false;
      };
      if (LAINYA==null || LAINYA==""){
        swal("Lainnya Harus di Isi", "", "warning")
        return false;
      };

    }  

    $("#NPWPD").keyup(function(){
      var npwpd = $("#NPWPD").val();
      $.ajax({
        url: "<?php echo base_url().'Master/pokok/ceknpwpd';?>",
        type: 'POST',
        data: "npwpd="+npwpd,
        cache: false,
        success: function(msg){
              // alert(msg);
              if(msg!=0){
                var exp = msg.split("|");
                $("#NAMA_WP").val(exp[0]);  
                $("#ALAMAT_WP").val(exp[1]);  
                // alert(msg);
              }else{
                $("#NAMA_WP").val(null);  
                $("#ALAMAT_WP").val(null);  
              }
              
            }
          });
    });    
//PENERIMAAN_KAMAR      
var msg = document.getElementById('PENERIMAAN_KAMAR'),
numberPattern = /^[0-9]+$/;

msg.addEventListener('keyup', function(e) {
  msg.value = getFormattedData(msg.value);
});
//PENERIMAAN_FASILITAS 
var PENERIMAAN_FASILITAS = document.getElementById('PENERIMAAN_FASILITAS'),
numberPattern = /^[0-9]+$/;

PENERIMAAN_FASILITAS.addEventListener('keyup', function(e) {
  PENERIMAAN_FASILITAS.value = getFormattedData(PENERIMAAN_FASILITAS.value);
});
//KURANG_BAYAR 
var KURANG_BAYAR = document.getElementById('KURANG_BAYAR'),
numberPattern = /^[0-9]+$/;

KURANG_BAYAR.addEventListener('keyup', function(e) {
  KURANG_BAYAR.value = getFormattedData(KURANG_BAYAR.value);
});
//SANKSI_ADMINISTRASI 
var SANKSI_ADMINISTRASI = document.getElementById('SANKSI_ADMINISTRASI'),
numberPattern = /^[0-9]+$/;

SANKSI_ADMINISTRASI.addEventListener('keyup', function(e) {
  SANKSI_ADMINISTRASI.value = getFormattedData(SANKSI_ADMINISTRASI.value);
});

function checkNumeric(str) {
  return str.replace(/\./g, '');
}
Number.prototype.format = function() {
  return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

function getFormattedData(num) {
  var i = checkNumeric(num),isNegative = checkNumeric(num) < 0,val;

  if (num.indexOf(',') > -1) {
    return num;
  }
  else if (num[num.length - 1] === '.') {
    return num;
  }
  else if(i.length < 3) {
   return i;
 }else {
  val = Number(i.replace(/[^\d]+/g, ''));
}

return (isNegative ? '-' : '') + val.format();
}    
function ribuan (angka)
{
  if(angka< 0)
  {
    var str = angka.toString();
    var res = str.substr(1);
    var reverse = res.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return "-"+ribuan;    
    return "-"+res;
  } else {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }
}
</script>