
        <?php $namabulan=array(
  '',
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
) ?>
<?php echo $this->session->flashdata('message')?><br>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title"> 
        <h2>SPTPD Hotel</h2>
        <div class="pull-right">
          <div class="btn-group">
            <?php echo anchor(site_url('Sptpd_hotel/sptpd_hotel/create'), '<i class="fa fa-plus"></i> Tambah', 'class="btn btn-primary btn-sm"'); 
            ?>
          </div>
        </div>                  
        <div class="clearfix"></div>
      </div>
        <form class="form-inline" method="get" action="<?php echo base_url().'Sptpd_hotel/sptpd_hotel'?>">
                <div class="form-group">
                  <select id="TAHUN_PAJAK" name="TAHUN_PAJAK" required="required" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                          <?php $thnskg = date('Y');
                          for($i=$thnskg; $i>=$thnskg-2; $i--){ ?>
                          <option value="<?php echo $i; ?>"
                          <?php if ($this->session->userdata('hotel_tahun')==$i) {echo "selected";} ?>><?php echo $i; ?></option><?php } ?>
                    </select>
                </div>
                <div class="form-group">
                      <select id="MASA_PAJAK" name="MASA_PAJAK" placeholder="Masa Pajak" class="form-control select2 col-md-7 col-xs-12">
                            <option value="">Semua masa</option>
                            <?php foreach($mp as $mp){ ?>
                            <option  value="<?php echo $mp?>"
                              <?php if ($this->session->userdata('hotel_bulan')==$mp) {echo "selected";} ?>><?php echo $namabulan[$mp] ?></option>
                            <?php } ?>      
                      </select>
                </div>
                <div class="form-group">
                     <input type="text" name="NPWPD" placeholder="NPWPD" class="form-control col-md-7 col-xs-12" value="<?= $this->session->userdata('dasar_pengenaan_hotel')?>">
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
        </form>
      <div class="x_content">
      
      </div>
    </div>
  </div>



</div> 
<script>
  $(document).ready(function(){

   readProducts(); 

   $(document).on('click', '#delete', function(e){

    var productId = $(this).data('id');
    SwalDelete(productId);
    e.preventDefault();
  });

 });
  
  function SwalDelete(productId){

    swal({
      title: 'Apakah Anda Yakin?',
      text: "Data Akan dihapus Permanen!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!',
      showLoaderOnConfirm: true,

      preConfirm: function() {
        return new Promise(function(resolve) {

         $.ajax({
          url: '<?php echo base_url()?>Sptpd_hotel/sptpd_hotel/hapus',
          type: 'POST',
          data: 'delete='+productId,
          dataType: 'json'
        })
         .done(function(response){
          swal('Deleted!', response.message, response.status);
          readProducts();
        })
         .fail(function(){
          swal('Oops...', 'Something went wrong with ajax !', 'error');
        });
       });
      },
      allowOutsideClick: false        
    }); 
    
  }
  function readProducts(){
    $('.x_content').load('sptpd_hotel/table'); 
  }
</script>