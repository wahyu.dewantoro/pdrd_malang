<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mbayar extends CI_Model {
    function __construct()
    {
        parent::__construct();
        $this->nip  =$this->session->userdata('NIP');
        // $this->role =$this->session->userdata('MS_ROLE_ID');
    }
	 

     function getData(){
        return $this->db->get('SPTPD_PEMBAYARAN');
     }


     // datatables
    function json() {
        
        $this->datatables->select("ID_INC, KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR,  TGL_BAYAR" );
        $this->datatables->from("(select ID_INC, KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR, to_char(TGL_BAYAR,'dd Mon YYYY , HH24: MI: SS') TGL_BAYAR from  SPTPD_PEMBAYARAN order by tgl_bayar desc)");
        return $this->datatables->generate();
    }
    function json_pembatalan() {
        
        $this->datatables->select("ID_INC, KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR,  TGL_PEMBATALAN" );
        $this->datatables->from("(select ID_INC, KODE_BILING, NAMA_WP, ALAMAT_WP, NAMA_USAHA, MASA_PAJAK, TAHUN_PAJAK, KETERANGAN, JUMLAH_BAYAR, to_char(TGL_PEMBATALAN,'dd Mon YYYY , HH24: MI: SS') TGL_PEMBATALAN from  SPTPD_PEMBATALAN order by tgl_bayar desc)");
        return $this->datatables->generate();
    }
     function getIP(){
        $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
    
        return $ipaddress;
    }


    function getKodebiling($kode){
        $this->db->where('KODE_BILING',$kode);
        return $this->db->get('V_GET_TAGIHAN');
    }

    function addbayar($data=array()){
        $this->db->trans_start();
        //  entri ke pembayran
         $KODE_BILING    =$data['KODE_BILING'];
         $MASA_PAJAK        =$data['MASA_PAJAK'];
         $TAHUN_PAJAK      =$data['TAHUN_PAJAK'];
         $TAGIHAN     =$data['TAGIHAN'];
         $DENDA     =$data['DENDA'];
         $TOTAL    =$data['TOTAL'];
         $NIP_INSERT=$this->nip;
         $IP_INSERT=$this->getIP();
         $this->db->query("CALL P_INSERT_SPTPD_PEMBAYARAN('$KODE_BILING','$IP_INSERT','$NIP_INSERT','$TOTAL')");
         $this->db->set('STATUS',1);
         $this->db->set('TGL_BAYAR','sysdate',false);
         $this->db->where('KODE_BILING',$data['KODE_BILING']);
         $this->db->update($data['KETERANGAN']);       
         $this->db->trans_complete();


        if ($this->db->trans_status() === FALSE)
            {
                return 'false';
            }else{
                return 'true';
            }
    }   
    function reversal($data=array()){
        $this->db->trans_start();
         $KODE_BILING    =$data['KODE_BILING'];
         $MASA_PAJAK        =$data['MASA_PAJAK'];
         $TAHUN_PAJAK      =$data['TAHUN_PAJAK'];
         $TAGIHAN     =$data['TAGIHAN'];
         $DENDA     =$data['DENDA'];
         $TOTAL    =$data['TOTAL'];
         $NIP_INSERT=$this->nip;
         $IP_INSERT=$this->getIP();
         $this->db->query("CALL P_REVERSAL('$KODE_BILING','$IP_INSERT','$NIP_INSERT','$TOTAL')");  
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return 'false';
        } else {
            return 'true';
        }        
    }

}