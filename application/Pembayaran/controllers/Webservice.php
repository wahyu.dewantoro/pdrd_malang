<?php

class Webservice extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        $this->load->model('Mbayar');
    }
    public function tes()
    {
        echo "tes";
    }
    public function inquiry()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $KODE_BILING= $this->input->get('Nop',TRUE);
        $query=$this->db->query(
            "SELECT 
                KODE_BILING, 
                NAMA_WP, 
                ALAMAT_WP, 
                MASA_PAJAK, 
                TAHUN_PAJAK, 
                NO_SK, 
                TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, 
                KODE_REK,
                TAGIHAN,
                DENDA, 
                STATUS,
                POTONGAN
            FROM TAGIHAN
            WHERE KODE_BILING='$KODE_BILING'"
        )->row(); 
            $data = array();
            if (!empty($query->KODE_BILING)) {
                $data = array(
                    "Nop"   =>  $query->KODE_BILING,
                    "Nama"       =>  $query->NAMA_WP,
                    "Alamat"     =>  $query->ALAMAT_WP,
                    "Masa"    =>  $query->MASA_PAJAK,
                    "Tahun"   => $query->TAHUN_PAJAK,
                    "NoSk"   => $query->NO_SK,
                    "JatuhTempo"      => $query->JATUH_TEMPO,
                    "KodeRek"      => $query->KODE_REK,
                    "Pokok"       =>  $query->TAGIHAN-$query->POTONGAN,
                    "Denda"      => $query->DENDA,
                    "Total"      => $query->DENDA+$query->TAGIHAN-$query->POTONGAN,
                    "Status"        => array(
                        "IsError"=>"False",
                        "ResponseCode"=>'00',
                        "ErrorDesc" => "Success"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data,200);
            } else {
                $data = array(
                    "Nop"   =>  $KODE_BILING,
                    "Status"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            }

            
       }

    }

public function payment()
{
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $KODE_BILING = $this->input->post('Nop',TRUE);
        $MASA_PAJAK = $this->input->post('Masa',TRUE);
        $TAHUN_PAJAK = $this->input->post('Tahun',TRUE);
        $TAGIHAN = $this->input->post('Pokok',TRUE);
        $DENDA = $this->input->post('Denda',TRUE);
        $TOTAL = $this->input->post('Total',TRUE);
        $KETERANGAN = $this->input->post('KETERANGAN',TRUE);
        $q=$this->db->query(
            "SELECT 
                KODE_BILING, 
                NAMA_WP, 
                ALAMAT_WP, 
                MASA_PAJAK, 
                TAHUN_PAJAK, 
                NO_SK, 
                TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, 
                KODE_REK,
                TAGIHAN,
                DENDA, 
                STATUS,
                KODE_PENGESAHAN,
                POTONGAN
            FROM TAGIHAN
            WHERE 
                KODE_BILING='$KODE_BILING' AND 
                MASA_PAJAK='$MASA_PAJAK' AND 
                TAHUN_PAJAK = '$TAHUN_PAJAK' AND 
                TAGIHAN-POTONGAN='$TAGIHAN' AND
                DENDA='$DENDA' AND
                TAGIHAN+DENDA-POTONGAN='$TOTAL'"
        )->row();    

        if (!empty($q->KODE_BILING)) {
            if ($q->STATUS==1) {
                $data = array();
                $data = array(
                    "Nop"           =>  $KODE_BILING,
                    "Masa"          =>  $MASA_PAJAK,
                    "Tahun"         =>  $TAHUN_PAJAK,
                    "Pokok"         => $TAGIHAN,
                    "Denda"         => $DENDA,
                    "Total"         => $TOTAL,
                    "Status"        =>  array(
                        "IsError"=>"True",
                        "ResponseCode"=>'13',
                        "ErrorDesc" => "Data Tagihan Telah Lunas"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            } else {
                if ($q->TAGIHAN+$q->DENDA-$q->POTONGAN!=str_replace('.','',$TOTAL)) {
                    $data = array();
                    $data = array(                    
                        "Nop"           =>  $KODE_BILING,
                        "Masa"          =>  $MASA_PAJAK,
                        "Tahun"         =>  $TAHUN_PAJAK,
                        "Pokok"         => $TAGIHAN,
                        "Denda"         => $DENDA,
                        "Total"         => $TOTAL,
                        "Status"        =>  array(
                            "IsError"=>"True",
                            "ResponseCode"=>'14',
                            "ErrorDesc" => "Jumlah tagihan yang dibayarkan tidak sesuai"
                        )
                    );                     
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    $data['KETERANGAN']     = $KETERANGAN;
                    $data['KODE_BILING']    = $KODE_BILING;
                    $data['MASA_PAJAK']     = $MASA_PAJAK;
                    $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                    $data['TAGIHAN']        = str_replace('.','',$TAGIHAN);
                    $data['DENDA']          = str_replace('.','',$DENDA);
                    $data['TOTAL']          = str_replace('.','',$TOTAL);
                    $res=$this->Mbayar->addbayar($data); 
                    $data = array();                 
                    if($res=='true'){      
                        $data = array(
                            "Nop"   =>  $q->KODE_BILING,
                            "Masa"       =>  $q->MASA_PAJAK,
                            "Tahun"     =>  $q->TAHUN_PAJAK,
                            "JatuhTempo"     =>  $q->JATUH_TEMPO,
                            "KodeRek"    =>  $q->KODE_REK,
                            "Pokok"    =>  $q->TAGIHAN,
                            "Denda"   => $q->DENDA,
                            "Total"       =>  $q->TAGIHAN+$q->DENDA,
                            "Pengesahan"       =>  $q->KODE_PENGESAHAN,
                            "Status"        =>  array(
                                "IsError"=>"False",
                                "ResponseCode"=>'00',
                                "ErrorDesc" => "Success"
                            )
                        ); 
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }else{
                        $data = array(
                            "Nop"           =>  $KODE_BILING,
                            "Nop"           =>  $KODE_BILING,
                            "Masa"          =>  $MASA_PAJAK,
                            "Tahun"         =>  $TAHUN_PAJAK,
                            "Pokok"         => $TAGIHAN,
                            "Denda"         => $DENDA,
                            "Total"         => $TOTAL,                            
                            "Status"        =>  array(
                                "IsError"=>"True",
                                "ResponseCode"=>'99',
                                "ErrorDesc" => "System Error"
                            )
                        );                     
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    } 
                } 
            }
        }else{
            $data = array();
            $data = array(
                "Nop"           => $KODE_BILING,
                "Masa"          => $MASA_PAJAK,
                "Tahun"         => $TAHUN_PAJAK,
                "Pokok"         => $TAGIHAN,
                "Denda"         => $DENDA,
                "Total"         => $TOTAL,
                "Status"        =>  array(
                    "IsError"=>"True",
                    "ResponseCode"=>'10',
                    "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                )
            );                     
            header('Content-Type: application/json');
            echo json_encode($data);
        }
        
    } 
}

    public function reversal()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $KODE_BILING = $this->input->post('Nop',TRUE);
            $MASA_PAJAK = $this->input->post('Masa',TRUE);
            $TAHUN_PAJAK = $this->input->post('Tahun',TRUE);
            $TAGIHAN = $this->input->post('Pokok',TRUE);
            $DENDA = $this->input->post('Denda',TRUE);
            $TOTAL = $this->input->post('Total',TRUE);
            $q=$this->db->query(
                "SELECT 
                KODE_BILING, 
                NAMA_WP, 
                ALAMAT_WP, 
                MASA_PAJAK, 
                TAHUN_PAJAK, 
                NO_SK, 
                TO_CHAR(JATUH_TEMPO, 'ddmmyyyy') JATUH_TEMPO, 
                KODE_REK,
                TAGIHAN,
                DENDA, 
                STATUS,
                KODE_PENGESAHAN,
                POTONGAN
            FROM TAGIHAN
            WHERE 
                KODE_BILING='$KODE_BILING' AND 
                MASA_PAJAK='$MASA_PAJAK' AND 
                TAHUN_PAJAK = '$TAHUN_PAJAK' AND 
                TAGIHAN-POTONGAN='$TAGIHAN' AND
                DENDA='$DENDA' AND
                TAGIHAN+DENDA-POTONGAN='$TOTAL'"
        )->row();   
            $data = array();
            if ($q->STATUS==0) {
                $data = array(
                    "Nop"           =>  $KODE_BILING,
                    "Masa"          =>  $MASA_PAJAK,
                    "Tahun"         =>  $TAHUN_PAJAK,
                    "Pokok"         => $TAGIHAN,
                    "Denda"         => $DENDA,
                    "Totals"         => $TOTAL,
                    "Status"        =>  array(
                        "IsError"       =>"True",
                        "ResponseCode"  =>'34',
                        "ErrorDesc"     => "Data reversal tidak ditemukan"
                    )
                );                     
                header('Content-Type: application/json');
                echo json_encode($data);
            }  else { 
                    $data['KODE_BILING']    = $KODE_BILING;
                    $data['MASA_PAJAK']     = $MASA_PAJAK;
                    $data['TAHUN_PAJAK']    = $TAHUN_PAJAK;
                    $data['TAGIHAN']        = str_replace('.','',$TAGIHAN);
                    $data['DENDA']          = str_replace('.','',$DENDA);
                    $data['TOTAL']          = str_replace('.','',$TOTAL);
                    $res=$this->Mbayar->reversal($data); 
                    $data = array();                 
                    if($res=='true'){      
                        $data = array(
                            "Nop"   =>  $q->KODE_BILING,
                            "Masa"       =>  $q->MASA_PAJAK,
                            "Tahun"     =>  $q->TAHUN_PAJAK,
                            "JatuhTempo"     =>  $q->JATUH_TEMPO,
                            "KodeRek"    =>  $q->KODE_REK,
                            "Pokok"    =>  $q->TAGIHAN-$q->POTONGAN,
                            "Denda"   => $q->DENDA,
                            "Total"       =>  $q->TAGIHAN+$q->DENDA-$q->POTONGAN,
                            "Status"        =>  array(
                                "IsError"=>"False",
                                "ResponseCode"=>'36',
                                "ErrorDesc" => "Data reversal telah dibatalkan"
                            )
                        ); 
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }else{
                        $data = array(
                            "Nop"   =>  $KODE_BILING,
                            "Status"        =>  array(
                                "IsError"=>"True",
                                "ResponseCode"=>'99',
                                "ErrorDesc" => "System Error"
                            )
                        );                     
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
            }     

        }
    }
    public function pembayaran()
    {
        $data = array(
      'title'              => 'Pembayaran',
      'action'             => site_url('webservice/create_action'),
            'script'             => 'webservice/script',
      );
        $this->template->load('layout','webservice/view_form',$data);
    }

}