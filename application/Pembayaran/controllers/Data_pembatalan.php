<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pembatalan extends CI_Controller {
	   public function __construct(){
        parent::__construct();
        $this->load->model('Mbayar');
        $this->load->library('datatables');
 }
 

  function index(){
    $this->template->load('Welcome/halaman','datapembatalan');
  }

  public function json() {
        header('Content-Type: application/json');
        echo $this->Mbayar->json_pembatalan();
    }

}