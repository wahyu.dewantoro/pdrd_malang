    <div class="page-title">
     <div class="title_left">
      <h3>Form Pembayaran</h3>
    </div>
    <div class="pull-right">
      
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    
      <div class="col-md-10 col-sm-12 col-xs-12">
      <form id="demo-form2" data-parsley-validate class="form-inline"  method="post" enctype="multipart/form-data" >
        <?php echo $this->session->flashdata('notif')?>
        <div class="x_panel">
          <div class="x_title">
                  <h4><i class="fa fa-search"></i> Pencarian Kode Biling</h4>
                  <div class="clearfix"></div>
                </div>
          <div class="x_content" >
            
              
                <div class="form-group">
                    Kode Biling
                </div>
                <div class="form-group">
                  <input id="KODE_BILING" type="text" name="kode_biling" required class="form-control" value="<?= $kode_biling?>" placeholder="xxxxxxx">
                </div>
                <a class="tampil btn btn-primary"><i class="fa fa-search"></i> Cari</a>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-10 col-sm-12 col-xs-12" id="KOSONG"> 
        <div class="x_panel">
        <div class="alert alert-info" >
          <i class="fa fa-close kosong"></i>
        </div>
        </div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12" id="TERBAYAR"> 
        <div class="x_panel">
        <div class="alert alert-info" >
          <i class="fa fa-close terbayar"></i>
        </div>
        </div>
      </div>
        <div class="col-md-10 col-sm-12 col-xs-12" id="DATA">
            <div class="x_panel">
                  
                <div class="x_content" >
            <form method="post" id="forms">
                <input type="hidden" value=""> 
                <div class="row">
                  <div class="x_title">
                  <h4><i class="fa fa-user"></i> Identitas Wajib Pajak</h4>
                  <div class="clearfix"></div>
                </div>
                    <div class="col-md-6">
                        <div class="form-group">
                         <label for="Nop">Kode Biling</label>
                         <input type="text" name="Nop" id="Nop" value="" readonly class="form-control">
                      </div>
                      <div class="form-group">
                         <label for="Nama">Nama WP</label>
                         <input type="text" name="Nama" id="Nama" value="" readonly class="form-control">
                      </div>
                    </div>
                  <div class="col-md-6">
                      
                      <div class="form-group">
                         <label for="Alamat">Alamat</label>
                         <input type="text" name="Alamat" id="Alamat" value="" readonly class="form-control">
                      </div> 
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12">
                      <div class="x_title">
                        <h4><i class="fa fa-file"></i> Data Pajak</h4>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                         <label for="MASA_PAJAK">Masa Pajak</label>
                         <input type="hidden" name="Masa" id="Masa"  value="" readonly class="form-control">
                         <input type="text" id="MASA_PAJAK" readonly class="form-control">
                      </div>
                      <div class="form-group">
                         <label for="Tahun">Tahun Pajak</label>
                         <input type="text" name="Tahun" id="Tahun" value="" readonly class="form-control">
                      </div>
                      <div class="form-group">
                         <label for="JatuhTempo">Jatuh Tempo</label>
                         <input type="text" name="JatuhTempo" id="JatuhTempo" value="" readonly class="form-control">
                      </div>                      
                  </div>
                  <div class="col-md-6">
                      
                      <div class="form-group">
                         <label for="NoSk">No SK</label>
                         <input type="text" name="NoSk" id="NoSk" value="" readonly class="form-control">
                      </div>  
                      <div class="form-group">
                         <label for="KodeRek">Kode Rekening</label>
                         <input type="text" name="KodeRek" id="KodeRek" value="" readonly class="form-control">
                      </div>  
                      <div class="col-md-4">
                        <div class="form-group">
                           <label for="Pokok">Pokok</label>
                           <input type="text" name="Pokok" id="Pokok" value="" readonly class="form-control">
                        </div>                
                      </div>    
                      <div class="col-md-4">
                        <div class="form-group">
                           <label for="Denda">Denda</label>
                           <input type="text" name="Denda" id="Denda" value="" readonly class="form-control">
                        </div>                
                      </div> 
                      <div class="col-md-4">
                        <div class="form-group">
                           <label for="Total">Total</label>
                           <input type="text" name="Total" id="Total" value="" readonly class="form-control">
                            <input type="hidden" name="KETERANGAN" id="KETERANGAN" value="">
                        </div>                
                      </div>                                                
                  </div>

                </div>
<!--                   <div class="row">
                    <div class="col-md-12">
                      <div class="x_title">
                        <h4><i class="fa fa-money"></i> Data Pembayaran</h4>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                <div class="row">

                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="JUMLAH_BAYAR"> Jumlah Bayar </label>
                          <input type="text" value="" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="JUMLAH_BAYAR" id="JUMLAH_BAYAR" autofocus class="form-control" required>
                      </div>
                  </div>  
              </div> -->
                <div class="pull-right">
                    <input type="hidden" name="JENIS_PAJAK" id="JENIS_PAJAK" value="">
                    <i class="btn btn-success kirim"><i class="fa fa-save"></i> Submit</i>
                </div>
              </div>    
            </div>
          </div>
       </form>
    </div>
  
</div>



</div>

<script type="text/javascript">
  $('#DATA').hide();
  $('#TERBAYAR').hide();
  $('#KOSONG').hide();
var bulan = [        '',
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'];
      $(function(){
            $(document).on('click','.tampil',function(e){
                e.preventDefault();
                // $("#myModal").modal('show');
                $.get("<?= base_url().'pembayaran/webservice/inquiry' ?>",
                    {Nop:$("#KODE_BILING").val() },
                    function(html){
                      var obj = $.parseJSON(JSON.stringify(html));
                      if (obj['Status']['ResponseCode']=='00')    {
                        if (obj['STATUS_BAYAR']==1) {
                          $('#TERBAYAR').show();
                          $('#DATA').hide();
                          $( ".terbayar" ).text(obj['KODE_BILING']+" Sudah Lunas, Bayar Tanggal "+ obj['DATE_BAYAR']);
                        } else {
                          var masa_pajak= obj['Masa'];
                          $('#DATA').show();
                          $('#TERBAYAR').hide();
                          $("#Nop" ).val(obj['Nop']);
                          $("#Nama" ).val(obj['Nama']);
                          $("#Alamat" ).val(obj['Alamat']);
                          $("#MASA_PAJAK" ).val(bulan[obj['Masa']]);
                          $("#Masa" ).val(obj['Masa']);   
                          $("#Tahun" ).val(obj['Tahun']);  
                          $("#NoSk" ).val(obj['NoSk']);             
                          $("#JatuhTempo" ).val(obj['JatuhTempo']);
                          $("#KodeRek" ).val(obj['KodeRek']);        
                          $("#Pokok" ).val(obj['Pokok']);        
                          $("#Denda" ).val(obj['Denda']);        
                          $("#Total" ).val(obj['Total']);        
                          $("#PAJAK_TERUTANG" ).val(String(obj['Total']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                          $("#JUMLAH_BAYAR" ).val(String(obj['Total']).replace(/(.)(?=(\d{3})+$)/g,'$1.'));
                        }
                        $('#KOSONG').hide();
                      } else {

                        $('#KOSONG').show();
                        $('#TERBAYAR').hide();
                        $('#DATA').hide();
                        $( ".kosong" ).text(obj['Nop']+"Tidak di temukan" );
                      }
                    }
                );
            });
            $(document).on('click','.kirim',function(e){
                e.preventDefault();
                // $("#myModal").modal('show');
                $.ajax({
                 type: "POST",
                 url: "<?= base_url().'pembayaran/webservice/payment' ?>",
                 data: $('#forms').serialize(),
                 cache: false,
                 success: function(html){
                  // alert(JSON.stringify(html));
                      var obj = $.parseJSON(JSON.stringify(html));
                      if (obj['Status']['ResponseCode']=='14')    {                  
                         swal("Gagal",obj['Status']['ErrorDesc'],"error");
                      } else if(obj['Status']['ResponseCode']=='13') {
                        swal("Gagal",obj['Status']['ErrorDesc'],"error");
                      }else{
                        swal("Berhasil",obj['Status']['ErrorDesc'],"success")
                        .then((value) => {
                          window.location = "<?php echo base_url().'pembayaran/data'?>";
                        });
                        
                      }
                }
              });    
            }); 
              $(document).on('click','.tampil',function(e){
                e.preventDefault();
                 $.get("<?= base_url().'pembayaran/Tagihan/Tbl_Sptpd'?>",
                    {Nop:$("#KODE_BILING").val() },
                    function(html){
                      var obj = $.parseJSON(JSON.stringify(html));
                      if (obj['Status']['ResponseCode']=='00')    {
                          $("#KETERANGAN" ).val(obj['KETERANGAN']);
                      } else {

                      }
                    }
                );
            });            
        });
function tandaPemisahTitik(b){
var _minus = false;
if (b<0) _minus = true;
b = b.toString();
b=b.replace(".","");
b=b.replace("-","");
c = "";
panjang = b.length;
j = 0;
for (i = panjang; i > 0; i--){
j = j + 1;
if (((j % 3) == 1) && (j != 1)){
c = b.substr(i-1,1) + "." + c;
} else {
c = b.substr(i-1,1) + c;
}
}
if (_minus) c = "-" + c ;
return c;
}

function numbersonly(ini, e){
if (e.keyCode>=49){
if(e.keyCode<=57){
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
ini.value = tandaPemisahTitik(b);
return false;
}
else if(e.keyCode<=105){
if(e.keyCode>=96){
//e.keycode = e.keycode - 47;
a = ini.value.toString().replace(".","");
b = a.replace(/[^\d]/g,"");
b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
ini.value = tandaPemisahTitik(b);
//alert(e.keycode);
return false;
}
else {return false;}
}
else {
return false; }
}else if (e.keyCode==48){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==95){
a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
b = a.replace(/[^\d]/g,"");
if (parseFloat(b)!=0){
ini.value = tandaPemisahTitik(b);
return false;
} else {
return false;
}
}else if (e.keyCode==8 || e.keycode==46){
a = ini.value.replace(".","");
b = a.replace(/[^\d]/g,"");
b = b.substr(0,b.length -1);
if (tandaPemisahTitik(b)!=""){
ini.value = tandaPemisahTitik(b);
} else {
ini.value = "";
}

return false;
} else if (e.keyCode==9){
return true;
} else if (e.keyCode==17){
return true;
} else {
//alert (e.keyCode);
return false;
}

}

</script>